import random
from collections import OrderedDict

from gensim.models import Doc2Vec
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec

from task2.HelperFunctions import accuracy_system, RR_splitting
from task2.Doc2VecModels import create_corpuses
from task2.Doc2VecSVM import train_svm_doc2vec, predict_svm_doc2vec
from task2.Tests import SVM_freq_unigrams, SVM_freq_bigrams, SVM_freq_unigramsbigrams, SVM_pres_unigrams, \
    SVM_pres_bigrams, SVM_pres_unigramsbigrams


def monte_carlo_permutation_test(true_sentiments, predictions_A, predictions_B, R=5000):
    s = 0
    orig_mean_diff = abs(accuracy_system(true_sentiments, predictions_A) - accuracy_system(true_sentiments, predictions_B))

    for i in range(R):
        new_predictions_modelA = predictions_A
        new_predictions_modelB = predictions_B

        for j in new_predictions_modelA:
            if (random.randint(0, 1) == 1):
                # Swap the predictions between models
                new_predictions_modelA[j], new_predictions_modelB[j] = new_predictions_modelB[j], new_predictions_modelA[j]
            # else do nothing

        new_mean_diff = abs(accuracy_system(true_sentiments, new_predictions_modelA) - accuracy_system(true_sentiments, new_predictions_modelB))
        if (new_mean_diff >= orig_mean_diff): s += 1

    return (s + 1) / (R + 1)


def compare_systems(folds, systemA, systemB, systemAName, systemBName, model_1=None, model_2=None):
    scoresA = []
    scoresB = []
    allTrueSentiments = {}
    allPredictionsA = {}
    allPredictionsB = {}

    # We have only 9 folds because we don't reuse validation => 1800 movie reviews
    for i in range(len(folds)):
        train_set_POS = []
        train_set_NEG = []

        test_set_POS = []
        test_set_NEG = []
        for j in range(len(folds)):
            if (i == j):
                test_set_POS.extend(folds[i]["POS"])
                test_set_NEG.extend(folds[i]["NEG"])
            else:
                train_set_POS.extend(folds[j]["POS"])
                train_set_NEG.extend(folds[j]["NEG"])

        # Assign true sentiments
        trueSentiments = {}

        for file in test_set_POS: trueSentiments[file] = "POS"
        for file in test_set_NEG: trueSentiments[file] = "NEG"

        if (model_1 != None):
            predictionA = systemA(model_1, train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)
        else:
            predictionA = systemA(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)
        if (model_2 != None):
            predictionB = systemB(model_2, train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)
        else:
            predictionB = systemB(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)

        allTrueSentiments.update(trueSentiments)
        allPredictionsA.update(predictionA)
        allPredictionsB.update(predictionB)

        # Compute accuracies
        accA = accuracy_system(trueSentiments, predictionA)
        accB = accuracy_system(trueSentiments, predictionB)
        scoresA.append(accA)
        scoresB.append(accB)

    p = monte_carlo_permutation_test(allTrueSentiments, allPredictionsA, allPredictionsB)

    print("RESULTS MONTE CARLO PERMUTATION TEST")
    print("System A name", systemAName, "Accuracy:", sum(scoresA) / len(folds))
    print("System B name", systemBName, "Accuracy:", sum(scoresB) / len(folds))
    print("p value:", p)


def Doc2Vec_model(model, train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model_from_train_svm_with_doc2vec = train_svm_doc2vec(train_set_POS, train_set_NEG, model)
    prediction_svm_with_doc2vec = predict_svm_doc2vec(model_from_train_svm_with_doc2vec, test_set_POS, model, 'POS')
    prediction_svm_with_doc2vec.update(predict_svm_doc2vec(model_from_train_svm_with_doc2vec, test_set_NEG, model, 'NEG'))

    return prediction_svm_with_doc2vec


# Load the already trained models
model0 = Doc2Vec.load("models/" + str(0))
model1 = Doc2Vec.load("models/" + str(1))
model2 = Doc2Vec.load("models/" + str(2))
model3 = Doc2Vec.load("models/" + str(3))
model4 = Doc2Vec.load("models/" + str(4))
model5 = Doc2Vec.load("models/" + str(5))
model6 = Doc2Vec.load("models/" + str(6))
simple_models = [model0, model1, model2, model3, model4, model5, model6]

train_corpus, test_corpus = create_corpuses()

# Create the PV-DMM + PV-DBOW and PV-DMC + PV-DBOW
models_by_name = OrderedDict((str(model), model) for model in simple_models)
models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[5]])
models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[6]])
# Train concatenated models
models_by_name['dbow+dmm'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmm'].epochs)
models_by_name['dbow+dmc'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmc'].epochs)
# Append concatenated models to simple models
simple_models.append(models_by_name['dbow+dmm'])
simple_models.append(models_by_name['dbow+dmc'])


folds = RR_splitting()
folds_without_validation = []
for i in range(1, 10):
    folds_without_validation.append(folds[i])

# # Check if number of epocs (10->20) significantly increases accuracy
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 0", "Doc2Vec model 1", model0, model1)
# # Check if number of window size (100->200) significantly increases accuracy
compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 0", "Doc2Vec model 2", model0, model2)
# # Check if major increase in window size (100->400) significantly increases accuracy
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 0", "Doc2Vec model 3", model0, model3)
#
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 4", "Doc2Vec model 5", model4, model5)
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 4", "Doc2Vec model 6", model4, model6)
#
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model dbow+dmm ", "Doc2Vec model dbow+dmc", models_by_name['dbow+dmm'], models_by_name['dbow+dmc'])
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model 4 DBOW", "Doc2Vec model dbow+dmm", model4, models_by_name['dbow+dmc'])
#
#
# compare_systems(folds_without_validation, Doc2Vec_model, Doc2Vec_model, "Doc2Vec model0", "Doc2Vec model4", model0, model4)
#
# # Best accuracy model doc2vec, dbow+dmc
# compare_systems(folds_without_validation, SVM_freq_unigrams, Doc2Vec_model, "SVM frequency unigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
# compare_systems(folds_without_validation, SVM_freq_bigrams, Doc2Vec_model, "SVM frequency bigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
# compare_systems(folds_without_validation, SVM_freq_unigramsbigrams, Doc2Vec_model, "SVM frequency unigrams+bigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
#
# compare_systems(folds_without_validation, SVM_pres_unigrams, Doc2Vec_model, "SVM presence unigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
# compare_systems(folds_without_validation, SVM_pres_bigrams, Doc2Vec_model, "SVM presence bigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
# compare_systems(folds_without_validation, SVM_pres_unigramsbigrams, Doc2Vec_model, "SVM presence unigrams+bigrams", "Doc2Vec dbow+dmc", model_1=None, model_2=models_by_name['dbow+dmc'])
