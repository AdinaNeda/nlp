import os, multiprocessing, gensim.models.doc2vec
from task2.Doc2VecSVM import train_svm_doc2vec, predict_svm_doc2vec
# from task2.Doc2VecModels import modelFile_1, modelFile_2, modelFile_3, modelFile_4, modelFile_5

cores = multiprocessing.cpu_count()
data_dir = "./aclImdb/"

def RR_splitting(N=10):
    # Get list of files in POS and NEG directories
    pos_files_list = os.listdir("../task1/data/POS")
    neg_files_list = os.listdir("../task1/data/NEG")

    # Sort the lists; we do RR splits on the first part of the file name
    pos_files_list.sort()
    neg_files_list.sort()

    # Split the files in N folds
    folds = []
    for i in range(N):
        # cv000, cv010, cv020,. . . = Split 1
        # cv001, cv011, cv021,. . . = Split 2
        split = {"POS": [], "NEG": []}

        for index_split in range(i, len(pos_files_list), N):
            split['POS'].append(pos_files_list[index_split])
            split['NEG'].append(neg_files_list[index_split])

        folds.append(split)

    return folds

def validation_train_true_sentiments():
    folds = RR_splitting()

    print('folds:', len(folds))

    # Validation set (from data task 1)
    validation = {}
    validation['POS'] = folds[0]["POS"]
    validation['NEG'] = folds[0]["NEG"]

    # Train set (from data task 1)
    train = {}
    train_set_pos = []
    train_set_neg = []
    for i in range(1, 10):
        train_set_pos.extend(folds[i]["POS"])
        train_set_neg.extend(folds[i]["NEG"])

    train['POS'] = train_set_pos
    train['NEG'] = train_set_neg

    # True sentiments validation
    true_sentiments = {}
    for file in validation["POS"]: true_sentiments[file] = "POS"
    for file in validation["NEG"]: true_sentiments[file] = "NEG"

    return validation, train, true_sentiments

def accuracy(predictions_pos, predictions_neg, test_set_POS, test_set_NEG):
    total_correct_pos = 0
    total_correct_neg = 0

    for prediction in predictions_pos.values():
        if(prediction == "POS") : total_correct_pos += 1
        else: total_correct_neg += 0

    for prediction in predictions_neg.values():
        if(prediction == "POS") : total_correct_pos += 0
        else: total_correct_neg += 1

    accuracy = (total_correct_pos + total_correct_neg) / (len(test_set_POS) + len(test_set_NEG))

    return accuracy

def accuracy_system(trueSentiments, predictions):
    correct = 0
    for file, sentiment in trueSentiments.items():
        if (predictions[file] == sentiment): correct += 1
    return correct / len(trueSentiments)

def test_model_on_validation(model, validation, train, true_sentiments):
    model_from_train_svm_with_doc2vec = train_svm_doc2vec(train['POS'], train['NEG'], model)
    prediction_svm_with_doc2vec = predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['POS'], model, 'POS')
    prediction_svm_with_doc2vec.update(predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['NEG'], model, 'NEG'))

    acc = accuracy_system(true_sentiments, prediction_svm_with_doc2vec)

    print("The accuracy for model ", model, "is: ", acc)
    return acc
