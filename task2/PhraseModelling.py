import multiprocessing, os
from collections import OrderedDict
from os.path import isdir
import numpy as np
from os.path import isdir
from gensim.models.doc2vec import TaggedDocument
import datetime, gensim, smart_open
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
from gensim.models import Doc2Vec

from task2.Doc2VecSVM import train_svm_doc2vec, predict_svm_doc2vec
from task2.HelperFunctions import validation_train_true_sentiments, accuracy_system
from task2.ROCCurve import draw_ROC

from gensim.models.phrases import Phrases, Phraser

cores = multiprocessing.cpu_count()
data_dir = "./aclImdb/"

def read_document_text(path_to_file):
    with open(path_to_file, 'r') as f:
        # extract the text for the file
        content = [token.strip() for token in f.readlines()]
    return content

def read_from_list_files_PhraseModelling(list_files, dir_path, bigram, start_index=0, tokens_only=False):
    i = start_index
    for file in list_files:
        with smart_open.open(dir_path + file, encoding="iso-8859-1") as f:
            for index, line in enumerate(f):
                tokens = gensim.utils.simple_preprocess(line)
            if tokens_only:
                yield bigram[tokens]
            else:
                # For training data, add tags
                yield TaggedDocument(bigram[tokens], [i])
                i += 1

def create_corpuses_PhraseModelling():
    # The list of file names in every directory of the data
    # Create train and test dictionaries
    all_train = {}
    all_train[data_dir + 'train/pos/'] = os.listdir(os.path.abspath(data_dir) + "/train/pos")
    all_train[data_dir + 'train/neg/'] = os.listdir(os.path.abspath(data_dir) + "/train/neg")
    all_train[data_dir + 'train/unsup/'] = os.listdir(os.path.abspath(data_dir) + "/train/unsup")
    all_train[data_dir + 'test/pos/'] = os.listdir(os.path.abspath(data_dir) + "/test/pos")
    all_train[data_dir + 'test/neg/'] = os.listdir(os.path.abspath(data_dir) + "/test/neg")

    all_test = {}
    all_test['../task1/data/POS/'] = os.listdir(os.path.abspath("../task1/data/POS"))
    all_test['../task1/data/NEG/'] = os.listdir(os.path.abspath("../task1/data/NEG"))

    test_corpus = {}
    pos_files_tokens = []
    neg_files_tokens = []
    for key in all_test.keys():
        for file in all_test.get(key):
            content = read_document_text(key + file)

            if (key == '../task1/data/POS/'):
                pos_files_tokens.append(content)
            elif (key == '../task1/data/NEG/'):
                neg_files_tokens.append(content)

    test_corpus['POS'] = pos_files_tokens
    test_corpus['NEG'] = neg_files_tokens

    tokenized_train = []
    for key in all_train.keys():
        for file in all_train.get(key):
            with smart_open.open(key + file, encoding="iso-8859-1") as f:
                content = [token.strip() for token in (f.readlines())[0].split()]
                tokenized_train.append(content)

    bigram = Phraser(Phrases(tokenized_train))

    train_corpus = []
    index = 0
    for key in all_train.keys():
        train_corpus += list(read_from_list_files_PhraseModelling(all_train.get(key), key, bigram, index))
        index += len(all_train.get(key))

    return train_corpus, test_corpus


def Doc2Vec_models_PhraseModelling(train_corpus):
    common_kwargs = dict(
        vector_size=100, epochs=20, min_count=2,
        sample=0, workers=1, negative=5, hs=0, seed=0
    )

    simple_models = [
        # Models 0-3 (included) are for testing if there's a significant difference in changing
        # the window size and the number of epochs
        # PV-DBOW plain - 10 epocs, window size 100
        Doc2Vec(dm=0, vector_size=100, min_count=2, epochs=10, workers=1, seed=0),
        # PV-DBOW plain - 20 epocs, window size 100
        Doc2Vec(dm=0, vector_size=100, min_count=2, epochs=20, workers=1, seed=0),
        # PV-DBOW plain - 10 epocs, window size 200
        Doc2Vec(dm=0, vector_size=200, min_count=2, epochs=10, workers=1, seed=0),
        # A much bigger window size! 400 is what the paper used
        Doc2Vec(dm=0, vector_size=400, min_count=2, epochs=10, workers=1, seed=0),

        # Models 4, 5, 6 are the PV-DBOW, PV-DM (with sum) and PV-DM (with concatenation)
        # PV-DBOW plain
        Doc2Vec(dm=0, **common_kwargs),
        # PV-DM w/ default averaging; a higher starting alpha may improve CBOW/PV-DM modes
        Doc2Vec(dm=1, window=10, alpha=0.05, **common_kwargs),
        # PV-DM w/ concatenation - big, slow, experimental mode
        # window=5 (both sides) approximates paper's apparent 10-word total window size
        Doc2Vec(dm=1, dm_concat=1, window=5, **common_kwargs),
    ]

    for model in simple_models:
        model.build_vocab(train_corpus)
        print("%s vocabulary scanned & state initialized" % model)

    if not isdir("models/phrasemodelling"): os.mkdir("models/phrasemodelling")
    index = 0
    print("Start training model:", datetime.datetime.now())
    for model in simple_models:
        model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)
        # Save model for persistence
        model.save("models/phrasemodelling/" + str(index))
        index += 1
    print("Finish training model:", datetime.datetime.now())

    return simple_models

def test_model_on_validation(model, validation, train, true_sentiments):
    model_from_train_svm_with_doc2vec = train_svm_doc2vec(train['POS'], train['NEG'], model)
    prediction_svm_with_doc2vec = predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['POS'], model, 'POS')
    prediction_svm_with_doc2vec.update(predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['NEG'], model, 'NEG'))

    acc = accuracy_system(true_sentiments, prediction_svm_with_doc2vec)

    print("The accuracy for model ", model, "is: ", acc)
    return acc


# ************
# # UNCOMMENT TO TEST
# train_corpus, test_corpus = create_corpuses_PhraseModelling()
# simple_models = Doc2Vec_models_PhraseModelling(train_corpus)
# validation, train, true_sentiments = validation_train_true_sentiments()
#
# # Create the PV-DMM + PV-DBOW and PV-DMC + PV-DBOW
# models_by_name = OrderedDict((str(model), model) for model in simple_models)
# models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[5]])
# models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[6]])
# # Train concatenated models
# models_by_name['dbow+dmm'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmm'].epochs)
# models_by_name['dbow+dmc'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmc'].epochs)
# # Append concatenated models to simple models
# simple_models.append(models_by_name['dbow+dmm'])
# simple_models.append(models_by_name['dbow+dmc'])
# # calculate accuracies all models
# print("Start calculating accuracies:", datetime.datetime.now())
# for model in simple_models:
#     test_model_on_validation(model, validation, train, true_sentiments)
# print("Finish calculating accuracies:", datetime.datetime.now())
# # Draw ROC curve for all models
# print("Start plotting ROC:", datetime.datetime.now())
# draw_ROC(simple_models[4:], validation, train, true_sentiments, 4)
# print("Finish plotting ROC:", datetime.datetime.now())
