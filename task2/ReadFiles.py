import smart_open, gensim
from gensim.models.doc2vec import TaggedDocument

def read_document_text(path_to_file):
    with open(path_to_file, 'r') as f:
        # extract the text for the file
        content = [token.strip() for token in f.readlines()] # .lower().replace('-lrb-', '').replace('-rrb-', '')
    return content


def read_from_list_files(list_files, dir_path, start_index=0, tokens_only=False):
    i = start_index
    for file in list_files:
        with smart_open.open(dir_path + file, encoding="iso-8859-1") as f:
            for index, line in enumerate(f):
                tokens = gensim.utils.simple_preprocess(line)
            if tokens_only:
                yield tokens
            else:
                # For training data, add tags
                yield TaggedDocument(tokens, [i])
                i += 1

# def normalize_text(text):
#     norm_text = text.lower()
#     # Replace breaks with spaces
#     norm_text = norm_text.replace('<br />', ' ')
#     # Pad punctuation with spaces on both sides
#     for char in ['.', '"', ',', '(', ')', '!', '?', ';', ':']:
#         norm_text = norm_text.replace(char, ' ' + char + ' ')
#
#     return norm_text
