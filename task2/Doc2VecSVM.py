import svmlight
from task2.ReadFiles import read_document_text, read_from_list_files


def train_svm_doc2vec(trainingPOS, trainingNEG, doc2vec_model):
    doc_vec_pos = []
    doc_vec_neg = []
    training_data = []
    for file in trainingPOS:
        path_to_file = '../task1/data/POS/' + file

        # Create Doc2Vec SVM vector positive files
        content = read_document_text(path_to_file)  # a list of tokens
        vector = doc2vec_model.infer_vector(content)  # infer the vector
        doc_vec = doc2vec_to_svmvec(vector)
        doc_vec_pos.extend(vector)
        training_data.append((1, doc_vec))

    for file in trainingNEG:
        path_to_file = '../task1/data/NEG/' + file

        # Create Doc2Vec SVM vector negative files
        content = read_document_text(path_to_file)
        vector = doc2vec_model.infer_vector(content)
        doc_vec = doc2vec_to_svmvec(vector)
        doc_vec_neg.extend(vector)
        training_data.append((-1, doc_vec))

    model = svmlight.learn(training_data, type='classification')

    return model


def predict_svm_doc2vec(model, test_set, doc2vec_model, sentiment):
    results = {}

    doc_vec_pred = []
    test_data = []
    for file in test_set:
        path_to_file = '../task1/data/' + sentiment + '/' + file

        # Create Doc2Vec SVM vector test files
        content = read_document_text(path_to_file)
        vector = doc2vec_model.infer_vector(content)
        doc_vec = doc2vec_to_svmvec(vector)
        doc_vec_pred.extend(vector)
        test_data.append((0, doc_vec))

    predictions = svmlight.classify(model, test_data)

    # Classify the document as positive or negative
    for i in range(len(predictions)):
        if predictions[i] > 0:
            results[test_set[i]] = "POS"
        else:
            results[test_set[i]] = "NEG"

    return results


def doc2vec_to_svmvec(vector):
    formatted_vector = []
    for index, value in enumerate(vector):
        formatted_vector.append((index + 1, value))

    return formatted_vector
