import random
from collections import OrderedDict
from IPython.display import HTML
from gensim.models import Doc2Vec
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec

from task2.Doc2VecModels import create_corpuses, validation_train_true_sentiments

# Create train and test corpuses
train_corpus, test_corpus = create_corpuses()
# Create validation and train from the 2000 documents and get the true accuracies for the validation corpus
validation, train, true_sentiments = validation_train_true_sentiments()

# Load the already trained models
model0 = Doc2Vec.load("models/" + str(0))
model1 = Doc2Vec.load("models/" + str(1))
model2 = Doc2Vec.load("models/" + str(2))
model3 = Doc2Vec.load("models/" + str(3))
model4 = Doc2Vec.load("models/" + str(4))
model5 = Doc2Vec.load("models/" + str(5))
model6 = Doc2Vec.load("models/" + str(6))

simple_models = [model0, model1, model2, model3, model4, model5, model6]

# pick a random word with a suitable number of occurences
while True:
    word = random.choice(simple_models[0].wv.index2word)
    if simple_models[0].wv.vocab[word].count > 100:
        break

# or uncomment below line, to just pick a word from the relevant domain:
#word = 'your-word'
similars_per_model = [str(model.wv.most_similar(word, topn=20)) for model in simple_models]

with open("similar-words.txt", 'w') as out:
    m = ' '.join([str(model) for model in simple_models])
    similars = ' '.join(similars_per_model)
    print("most similar words for '%s' (%d occurences)" % (word, simple_models[0].wv.vocab[word].count), file=out)
    print("Models: \n", m, file=out)
    print("Similar words: \n", similars, file=out)
