from collections import OrderedDict
import numpy as np
from os.path import isdir
import datetime, gensim
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
from gensim.models import Doc2Vec

from task2.ReadFiles import read_document_text, read_from_list_files
from task2.HelperFunctions import *
from task2.ROCCurve import draw_ROC

cores = multiprocessing.cpu_count()
data_dir = "./aclImdb/"

def create_corpuses():
    # The list of file names in every directory of the data
    # Create train and test dictionaries
    all_train = {}
    all_train[data_dir + 'train/pos/'] = os.listdir(os.path.abspath(data_dir) + "/train/pos")
    all_train[data_dir + 'train/neg/'] = os.listdir(os.path.abspath(data_dir) + "/train/neg")
    all_train[data_dir + 'train/unsup/'] = os.listdir(os.path.abspath(data_dir) + "/train/unsup")
    all_train[data_dir + 'test/pos/'] = os.listdir(os.path.abspath(data_dir) + "/test/pos")
    all_train[data_dir + 'test/neg/'] = os.listdir(os.path.abspath(data_dir) + "/test/neg")

    all_test = {}
    all_test['../task1/data/POS/'] = os.listdir(os.path.abspath("../task1/data/POS"))
    all_test['../task1/data/NEG/'] = os.listdir(os.path.abspath("../task1/data/NEG"))

    test_corpus = {}
    pos_files_tokens = []
    neg_files_tokens = []
    for key in all_test.keys():
        for file in all_test.get(key):
            content = read_document_text(key + file)

            if (key == '../task1/data/POS/'):
                pos_files_tokens.append(content)
            elif (key == '../task1/data/NEG/'):
                neg_files_tokens.append(content)

    test_corpus['POS'] = pos_files_tokens
    test_corpus['NEG'] = neg_files_tokens

    train_corpus = []
    index = 0
    for key in all_train.keys():
        train_corpus += list(read_from_list_files(all_train.get(key), key, index))
        index += len(all_train.get(key))
    #
    # print("Tokenised dataset task 1 correct length?", len(test_corpus))
    # print("len of pos (training): ", len(test_corpus['POS']), "len of neg (training): ", len(test_corpus['NEG']))
    # print("training data length", len(train_corpus))

    return train_corpus, test_corpus


def Doc2Vec_models(train_corpus):
    common_kwargs = dict(
        vector_size=100, epochs=20, min_count=2,
        sample=0, workers=1, negative=5, hs=0, seed=0
    )

    simple_models = [
        # Models 0-3 (included) are for testing if there's a significant difference in changing
        # the window size and the number of epochs
        # PV-DBOW plain - 10 epocs, window size 100
        Doc2Vec(dm=0, vector_size=100, min_count=2, epochs=10, workers=1, seed=0),
        # PV-DBOW plain - 20 epocs, window size 100
        Doc2Vec(dm=0, vector_size=100, min_count=2, epochs=20, workers=1, seed=0),
        # PV-DBOW plain - 10 epocs, window size 200
        Doc2Vec(dm=0, vector_size=200, min_count=2, epochs=10, workers=1, seed=0),
        # A much bigger window size! 400 is what the paper used
        Doc2Vec(dm=0, vector_size=400, min_count=2, epochs=10, workers=1, seed=0),

        # Models 4, 5, 6 are the PV-DBOW, PV-DM (with sum) and PV-DM (with concatenation)
        # PV-DBOW plain
        Doc2Vec(dm=0, **common_kwargs),
        # PV-DM w/ default averaging; a higher starting alpha may improve CBOW/PV-DM modes
        Doc2Vec(dm=1, window=10, alpha=0.05, **common_kwargs),
        # PV-DM w/ concatenation - big, slow, experimental mode
        # window=5 (both sides) approximates paper's apparent 10-word total window size
        Doc2Vec(dm=1, dm_concat=1, window=5, **common_kwargs),
    ]

    for model in simple_models:
        model.build_vocab(train_corpus)
        print("%s vocabulary scanned & state initialized" % model)

    if not isdir("models/"): os.mkdir("models/")
    index = 0
    print("Start training model:", datetime.datetime.now())
    for model in simple_models:
        model.train(train_corpus, total_examples=model.corpus_count, epochs=model.epochs)
        # Save model for persistence
        model.save("models/" + str(index))
        index += 1
    print("Finish training model:", datetime.datetime.now())

    return simple_models


# # TEST 1 - UNCOMMENT TO USE
# # ************
# # Create train and test corpuses
# train_corpus, test_corpus = create_corpuses()
# # Create validation and train from the 2000 documents and get the true accuracies for the validation corpus
# validation, train, true_sentiments = validation_train_true_sentiments()
# # Load the already trained models
# model0 = Doc2Vec.load("models/" + str(0))
# model1 = Doc2Vec.load("models/" + str(1))
# model2 = Doc2Vec.load("models/" + str(2))
# model3 = Doc2Vec.load("models/" + str(3))
# model4 = Doc2Vec.load("models/" + str(4))
# model5 = Doc2Vec.load("models/" + str(5))
# model6 = Doc2Vec.load("models/" + str(6))
#
# simple_models = [model0, model1, model2, model3, model4, model5, model6]
#
# # Create the PV-DMM + PV-DBOW and PV-DMC + PV-DBOW
# models_by_name = OrderedDict((str(model), model) for model in simple_models)
# models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[5]])
# models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[6]])
# # Train concatenated models
# models_by_name['dbow+dmm'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmm'].epochs)
# models_by_name['dbow+dmc'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmc'].epochs)
# # Append concatenated models to simple models
# simple_models.append(models_by_name['dbow+dmm'])
# simple_models.append(models_by_name['dbow+dmc'])
# # calculate accuracies all models
# print("Start calculating accuracies:", datetime.datetime.now())
# for model in simple_models:
#     test_model_on_validation(model, validation, train, true_sentiments)
# print("Finish calculating accuracies:", datetime.datetime.now())
# # Draw ROC curve for all models
# print("Start plotting ROC:", datetime.datetime.now())
# draw_ROC(simple_models[4:], validation, train, true_sentiments, 4)
# print("Finish plotting ROC:", datetime.datetime.now())
# # ************



# # TEST 2 - UNCOMMENT TO USE
# # ************
# train_corpus, test_corpus = create_corpuses()
# simple_models = Doc2Vec_models(train_corpus)
# validation, train, true_sentiments = validation_train_true_sentiments()
#
# # Create the PV-DMM + PV-DBOW and PV-DMC + PV-DBOW
# models_by_name = OrderedDict((str(model), model) for model in simple_models)
# models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[5]])
# models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[6]])
# # Train concatenated models
# models_by_name['dbow+dmm'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmm'].epochs)
# models_by_name['dbow+dmc'].train(train_corpus, total_examples=len(train_corpus), epochs=models_by_name['dbow+dmc'].epochs)
# # Append concatenated models to simple models
# simple_models.append(models_by_name['dbow+dmm'])
# simple_models.append(models_by_name['dbow+dmc'])
# # calculate accuracies all models
# print("Start calculating accuracies:", datetime.datetime.now())
# for model in simple_models:
#     test_model_on_validation(model, validation, train, true_sentiments)
# print("Finish calculating accuracies:", datetime.datetime.now())
# # Draw ROC curve for all models
# print("Start plotting ROC:", datetime.datetime.now())
# draw_ROC(simple_models, validation, train, true_sentiments)
# print("Finish plotting ROC:", datetime.datetime.now())
# # ************