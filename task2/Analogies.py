from collections import OrderedDict
import random, smart_open, os
from gensim.models import Doc2Vec
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
import numpy as np


from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
from task2.Doc2VecModels import create_corpuses
from task2.HelperFunctions import validation_train_true_sentiments

# Load the already trained models
model0 = Doc2Vec.load("models/" + str(0))
model1 = Doc2Vec.load("models/" + str(1))
model2 = Doc2Vec.load("models/" + str(2))
model3 = Doc2Vec.load("models/" + str(3))
model4 = Doc2Vec.load("models/" + str(4))
model5 = Doc2Vec.load("models/" + str(5))
model6 = Doc2Vec.load("models/" + str(6))

simple_models = [model0, model1, model2, model3, model4, model5, model6]

train_corpus, test_corpus = create_corpuses()
validation, train, true_sentiments = validation_train_true_sentiments()

test_set_list = []
test_set_list.extend(test_corpus["POS"])
test_set_list.extend(test_corpus["NEG"])

# print(len(test_set_list))

def analogies():
    # grab the file if not already local
    questions_filename = 'questions-words.txt'
    if not os.path.isfile(questions_filename):
        # Download IMDB archive
        print("Downloading analogy questions file...")
        url = u'https://raw.githubusercontent.com/tmikolov/word2vec/master/questions-words.txt'
        with smart_open.open(url, 'rb') as fin:
            with smart_open.open(questions_filename, 'wb') as fout:
                fout.write(fin.read())
    assert os.path.isfile(questions_filename), "questions-words.txt unavailable"
    print("Success, questions-words.txt is available for next steps.")

    # we can notice that the BOW-based Doc2Vec is not able to do any word analogies
    # the other models still have limited capabilities i.e ranging between 18.44% and 22.99% correctness
    # we use a domain-specific, reasonably small dataset, it might not be the most representative
    for model in simple_models:
        score, sections = model.wv.evaluate_word_analogies('questions-words.txt')
        correct, incorrect = len(sections[-1]['correct']), len(sections[-1]['incorrect'])
        print(model, "correct: ", float(correct * 100) / (correct + incorrect), correct, correct + incorrect)


def similarities_test_set():
    model = model6
    doc_id = 0

    most = 0
    median = 0
    least = 0
    for doc_test_set in test_set_list:

        inferred_vector = model.infer_vector(doc_test_set)
        sims = model.docvecs.most_similar([inferred_vector], topn=len(model.docvecs))

        print("Most Similar", len(sims))
        print("TEST:", [('MOST', 0), ('MEDIAN', len(sims) // 2), ('LEAST', len(sims) - 1)])

        min_diff = pow(10, 10)
        label_res = ''
        for label, index in [('MOST', 0), ('MEDIAN', len(sims) // 2), ('LEAST', len(sims) - 1)]:
            if (abs(doc_id - sims[index][0]) < min_diff):
                min_diff = abs(doc_id - sims[index][0])
                label_res = label
            else:
                continue
        if (label_res == 'MOST'): most += 1
        elif (label_res == 'MEDIAN'): median += 1
        elif (label_res == 'LEAST'): least += 1
        doc_id += 1

        print("most, median, least", most, median, least)


def heatmap():
    sentences = ["terrible plot", "fabulous plot", "good plot", "bad plot"]
    # sentences = ["fabulous plot", "amazing plot", "extraordinary plot", "formidable plot"]

    sentence_split = [sentence.split(" ") for sentence in sentences]

    model_index = 0
    for model in simple_models:
        sentence_vec = []
        for s in sentence_split:
            vec = model.infer_vector(s)
            sentence_vec.append(np.array([vec]))

        # df = pd.DataFrame(np.concatenate(sentenceVectors))

        fig, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1, figsize=(4, 4), constrained_layout=True, sharey=True, sharex=True)
        # [left, bottom, width, height]
        # cbar_ax = fig.add_axes([.91, .3, .01, .4]) "cbar_ax": cbar_ax

        pc_kwargs = {"cmap": "coolwarm", "xticklabels": False, "yticklabels": False}
        im = sns.heatmap(pd.DataFrame(sentence_vec[0]), ax=ax1, cbar=False, **pc_kwargs)
        ax1.set_title(sentences[0])

        sns.heatmap(pd.DataFrame(sentence_vec[1]), ax=ax2, cbar=False, **pc_kwargs)
        ax2.set_title(sentences[1])

        sns.heatmap(pd.DataFrame(sentence_vec[2]), ax=ax3, cbar=False, **pc_kwargs)
        ax3.set_title(sentences[2])

        sns.heatmap(pd.DataFrame(sentence_vec[3]), ax=ax4, cbar=False, **pc_kwargs)
        ax4.set_title(sentences[3])

        mappable = im.get_children()[0]
        plt.colorbar(mappable, ax=[ax1, ax2, ax3, ax4], orientation='vertical', shrink=0.8)

        plt.savefig('./heatmaps/model'+ str(model_index)+'.jpg')
        model_index += 1


# # UNCOMMENT TO TEST:
# analogies()
# similarities_test_set()
heatmap()