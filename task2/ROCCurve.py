from sklearn.metrics import roc_curve, auc
import matplotlib.pyplot as plt
from gensim.models.word2vec import Word2Vec

from task2.Doc2VecSVM import train_svm_doc2vec, predict_svm_doc2vec
from task2.HelperFunctions import validation_train_true_sentiments

def draw_ROC(models, validation, train, true_sentiments, start_index):
    list_name_models = [
        'PV-DBOW (window size=100, epochs=10)',
        'PV-DBOW (window size=100, epochs=20)',
        'PV-DBOW (window size=200, epochs=10)',
        'PV-DBOW (window size=400, epochs=10)',

        'PV-DBOW (window size=100, epochs=20)',
        'PV-DM (with sum)',
        'PV-DM (with concatenation)',

        'PV-DM (with sum) + PV-DBOW',
        'PV-DM (with concatenation) + PV-DBOW',
    ]
    for model in models:
        model_from_train_svm_with_doc2vec = train_svm_doc2vec(train['POS'], train['NEG'], model)
        prediction_svm_with_doc2vec = predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['POS'], model, 'POS')
        prediction_svm_with_doc2vec.update(predict_svm_doc2vec(model_from_train_svm_with_doc2vec, validation['NEG'], model, 'NEG'))

        correct_vector = []
        predictions_vector = []

        for file, sentiment in true_sentiments.items():
            if (sentiment == 'POS'): correct_vector.append(1.0)
            else: correct_vector.append(0.0)

        for file, sentiment in prediction_svm_with_doc2vec.items():
            if (sentiment == 'POS'): predictions_vector.append(1.0)
            else: predictions_vector.append(0.0)

        fpr, tpr, _ = roc_curve(correct_vector, predictions_vector)
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, label='Area ' + list_name_models[start_index] + '= %.3f' % roc_auc)
        start_index += 1

    plt.plot([0, 1], [0, 1], 'k--', label='Random chance')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate', fontsize=15)
    plt.ylabel('True Positive Rate', fontsize=15)
    plt.legend(loc="lower right", frameon=True).get_frame().set_edgecolor('black')
    plt.grid(True, linestyle='dotted')
    plt.title('Receiver Operating Characteristic (ROC) curve for Doc2Vec models', fontsize=12)
    plt.show()

