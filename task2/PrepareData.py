# TO BE RUN ONLY ONCE!!!
import os

# Convert text to lower-case and strip punctuation/symbols from words
def lowercase_and_remove_punctuation(dict_of_files):
    punctuation = ['.', '"', ',', '(', ')', '!', '?', ';', ':']
    for key in dict_of_files.keys():
        dir_path = key
        list_files = dict_of_files.get(key)
        for file in list_files:
            with open(dir_path + file, 'r+') as read_file:
                lowercase_text_no_breaks = read_file.readlines()[0].lower().replace('<br />', ' ')
                # Add space for punctuation
                for char in punctuation:
                    lowercase_text_no_breaks = lowercase_text_no_breaks.replace(char, ' ' + char + ' ')

            with open(dir_path + file, 'w') as write_file:
                write_file.write(lowercase_text_no_breaks)

    return;


data_dir = "./aclImdb/"
# The list of file names in every directory of th data
test_data_pos = os.listdir(os.path.abspath(data_dir) + "/test/pos")
test_data_neg = os.listdir(os.path.abspath(data_dir) + "/test/neg")
train_data_pos = os.listdir(os.path.abspath(data_dir) + "/train/pos")
train_data_neg = os.listdir(os.path.abspath(data_dir) + "/train/neg")
train_data_unsup = os.listdir(os.path.abspath(data_dir) + "/train/unsup")

all_train = {}
all_train[data_dir + 'train/pos/'] = train_data_pos
all_train[data_dir + 'train/neg/'] = train_data_neg
all_train[data_dir + 'train/unsup/'] = train_data_unsup

all_test = {}
all_test[data_dir + 'test/pos/'] = test_data_pos
all_test[data_dir + 'test/neg/'] = test_data_neg

# Call the function to make lowercase and add padding for punctuation
lowercase_and_remove_punctuation(all_train)
lowercase_and_remove_punctuation(all_test)