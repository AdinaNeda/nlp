from collections import OrderedDict
from os.path import isdir

# from MulticoreTSNE import MulticoreTSNE
from sklearn.manifold import TSNE as TSNE_F
import gensim
import nltk
from gensim.models.word2vec import Word2Vec
from nltk.corpus import stopwords
import numpy as np
import os
import matplotlib.pyplot as plt
from gensim.test.test_doc2vec import ConcatenatedDoc2Vec
from gensim.models import Doc2Vec
from task2.Doc2VecModels import create_corpuses
from gensim.test.utils import get_tmpfile

stop_words = set(stopwords.words('english'))

# Load the already trained models
model0 = Doc2Vec.load("models/" + str(0))
model1 = Doc2Vec.load("models/" + str(1))
model2 = Doc2Vec.load("models/" + str(2))
model3 = Doc2Vec.load("models/" + str(3))
model4 = Doc2Vec.load("models/" + str(4))
model5 = Doc2Vec.load("models/" + str(5))
model6 = Doc2Vec.load("models/" + str(6))

simple_models = [model0, model1, model2, model3, model4, model5, model6]

train_corpus, test_corpus = create_corpuses()

# Create the PV-DMM + PV-DBOW and PV-DMC + PV-DBOW
models_by_name = OrderedDict((str(model), model) for model in simple_models)
models_by_name['dbow+dmm'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[5]])
models_by_name['dbow+dmc'] = ConcatenatedDoc2Vec([simple_models[4], simple_models[6]])
# Train concatenated models
models_by_name['dbow+dmm'].train(train_corpus, total_examples=len(train_corpus),
                                 epochs=models_by_name['dbow+dmm'].epochs)
models_by_name['dbow+dmc'].train(train_corpus, total_examples=len(train_corpus),
                                 epochs=models_by_name['dbow+dmc'].epochs)
# Append concatenated models to simple models
simple_models.append(models_by_name['dbow+dmm'])
simple_models.append(models_by_name['dbow+dmc'])


def getWordVecs(words, model):
    vecs = []
    labels = []
    for word, tag in words:
        word = word.replace('\n', '')
        try:
            vecs.append(model[word][:100].reshape((1, 100)))
            labels.append(word)
        except KeyError:
            continue
    vecs = np.concatenate(vecs)
    return np.array(vecs, dtype='float'), labels  # TSNE expects float type values


def read_files():
    all_test = {}
    all_test['../task1/data/POS/'] = os.listdir(os.path.abspath("../task1/data/POS/"))
    all_test['../task1/data/NEG/'] = os.listdir(os.path.abspath("../task1/data/NEG/"))

    ''' 
    FOR POSITIVE REVIWS
    1. Read all POS files and store ALL the text in a list
    '''
    text_pos = []
    for file in all_test['../task1/data/POS/']:
        with open('../task1/data/POS/' + file, 'r') as infile:
            text_pos.extend([word.replace('\n', '') for word in infile.readlines()])

    # Remove duplictates (for speedup)
    text_pos = list(dict.fromkeys(text_pos))

    # remove stop words and empty words from wordList
    wordsListPOS = []
    for word in text_pos:
        if (word not in stop_words and word != ''): wordsListPOS.append(word)

    # tag words using part-of-speech (POS) tagger
    pos_tagged = nltk.pos_tag(wordsListPOS)

    ''' 
    FOR NEGATIVE REVIWS
    1. Read all NEG files and store ALL the text in a list
    '''
    text_neg = []
    for file in all_test['../task1/data/NEG/']:
        with open('../task1/data/NEG/' + file, 'r') as infile:
            text_neg.extend([word.replace('\n', '') for word in infile.readlines()])

    # Remove duplictates (for speedup)
    text_neg = list(dict.fromkeys(text_neg))

    # remove stop words and empty words from wordList
    wordsListNEG = []
    for word in text_neg:
        if (word not in stop_words and word != ''): wordsListNEG.append(word)

    # tag words using part-of-speech (POS) tagger
    neg_tagged = nltk.pos_tag(wordsListNEG)

    return pos_tagged, neg_tagged


def TSNE(pos, neg, pos_labels, neg_labels, model_doc2vec, index_name_model):
    list_name_models = [
        'PV-DBOW (window size=100, epochs=10)',
        'PV-DBOW (window size=100, epochs=20)',
        'PV-DBOW (window size=200, epochs=10)',
        'PV-DBOW (window size=400, epochs=10)',

        'PV-DBOW (window size=100, epochs=20)',
        'PV-DM (with sum)',
        'PV-DM (with concatenation)',

        'PV-DM (with sum) + PV-DBOW',
        'PV-DM (with concatenation) + PV-DBOW',
    ]
    ts = TSNE_F(n_components=2, perplexity=7)
    plt.figure()

    reduced_vecs = ts.fit_transform(np.concatenate((pos, neg)))
    print(len(reduced_vecs))

    all_lables = np.concatenate((pos_labels, neg_labels))

    x_coords = reduced_vecs[:, 0]
    y_coords = reduced_vecs[:, 1]

    for i in range(len(x_coords)):
        if i < len(pos):
            # pos words colored blue
            color = 'b'
        elif i >= len(pos) and i < (len(pos) + len(neg)):
            # neg words colored red
            color = 'r'

        if i == len(pos) - 1:
            plt.plot(reduced_vecs[i, 0], reduced_vecs[i, 1], marker='x', color='blue', markersize=4,
                     label='POS Adjectives')
        elif i == len(pos) + len(neg) - 1:
            plt.plot(reduced_vecs[i, 0], reduced_vecs[i, 1], marker='x', color='red', markersize=4,
                     label='NEG Adjectives')
        else:
            plt.plot(reduced_vecs[i, 0], reduced_vecs[i, 1], marker='x', color=color, markersize=4)

    for label, x, y in zip(all_lables, x_coords, y_coords):
        plt.annotate(label, xy=(x, y), xytext=(0, 0), textcoords='offset points', fontsize=10)

    plt.title(list_name_models[index_name_model], fontsize=12, y=1.02)
    plt.legend(loc="lower right", frameon=True).get_frame().set_edgecolor('black')

    if not isdir("./tsneperplexity7/"): os.mkdir("./tsneperplexity7/")
    plt.savefig('./tsneperplexity7/model' + str(index_name_model) + ".jpg")
    # plt.show()


pos_words = [
    'goodness', 'good', 'better', 'best', 'elysian',
    'best', 'superb', 'amiable', 'gainly', 'pretty',
    'charming', 'gorgeous', 'worthy', 'formidable', 'ideal',
    'sensational', 'nice', 'legendary', 'perfect', 'golden'
]
neg_words = [
    'abject', 'deplorable', 'distressing', 'lamentable', 'pitiful',
    'sad', 'sorry', 'bad', 'disgusting', 'unsound',
    'scrimy', 'cheapjack', 'unfortunate', 'inauspicious', 'miserable',
    'pathetic', 'misfortunate', 'wretched', 'terrible', 'uncool'
]

print("Pos words length:", len(pos_words))
print("Neg words length:", len(neg_words))

pos_tagged, neg_tagged = read_files()
pos_adjectives = []
for word, part_of_speech in pos_tagged:
    if ((part_of_speech == "JJ" or
         part_of_speech == "JJR" or
         part_of_speech == "JJS") and
            word.lower() in pos_words): pos_adjectives.append((word.lower(), part_of_speech))

neg_adjectives = []
for word, part_of_speech in neg_tagged:
    if ((part_of_speech == "JJ" or
         part_of_speech == "JJR" or
         part_of_speech == "JJS") and
            word.lower() in neg_words): neg_adjectives.append((word.lower(), part_of_speech))

pos_adjectives = list(dict.fromkeys(pos_adjectives))
neg_adjectives = list(dict.fromkeys(neg_adjectives))

# print("POS ajectives:", pos_adjectives)
# print("NEG ajectives:", neg_adjectives)

index = 0
for model_doc2vec in simple_models:
    pos, pos_labels = getWordVecs(pos_adjectives, model_doc2vec)
    neg, neg_labels = getWordVecs(neg_adjectives, model_doc2vec)

    TSNE(pos, neg, pos_labels, neg_labels, model_doc2vec, index)
    index += 1
