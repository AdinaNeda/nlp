from task1.SVM import train_svm, predict_svm

def SVM_freq_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS")
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG"))

    return prediction

def SVM_freq_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=False, bigrams=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=False, bigrams=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=False, bigrams=True))

    return prediction


def SVM_freq_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=True, bigrams=True, presence=False)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=True, bigrams=True, presence=False)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=True, bigrams=True, presence=False))
    return prediction

def SVM_pres_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", presence=True))
    return prediction

def SVM_pres_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=False, bigrams=True, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=False, bigrams=True, presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=False, bigrams=True, presence=True))
    return prediction

def SVM_pres_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, bigrams=True, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", bigrams=True, presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", bigrams=True, presence=True))
    return prediction
