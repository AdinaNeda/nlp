# from task1 import SignTest, NaiveBayes, SVM, CrossValidation

from task1.NaiveBayes import smoothed_log_probs, naive_bayes
from task1.SVM import train_svm,predict_svm
from task1.SignTest import compare_systems_task1
from task1.CrossValidation import RR_splitting

current_dir = "./data/"

class_probabilities = {"POS" : 0.5, "NEG" : 0.5}

# Bayes and SVM with Frequency and Unigrams
def NB_freq_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS")
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG"))

    return prediction

def SVM_freq_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS")
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG"))

    return prediction

# Bayes and SVM with Frequency and Bigrams
def NB_freq_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG, unigrams=False, bigrams=True)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS", unigrams=False, bigrams=True)
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG", unigrams=False, bigrams=True))

    return prediction

def SVM_freq_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=False, bigrams=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=False, bigrams=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=False, bigrams=True))

    return prediction

# Bayes and SVM with Frequency and Unigrams+Bigrams
def NB_freq_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG, unigrams=True, bigrams=True)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS", unigrams=True, bigrams=True, presence=False)
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG", unigrams=True, bigrams=True, presence=False))

    return prediction

def SVM_freq_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=True, bigrams=True, presence=False)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=True, bigrams=True, presence=False)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=True, bigrams=True, presence=False))
    return prediction


# Bayes and SVM with Presence and Unigrams
def NB_pres_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS", presence=True)
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG", presence=True))

    return prediction

def SVM_pres_unigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", presence=True))
    return prediction

# Bayes and SVM with Presence and Bigrams
def NB_pres_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG, unigrams=False, bigrams=True)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS", unigrams=False, bigrams=True, presence=True)
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG", unigrams=False, bigrams=True, presence=True))
    return prediction

def SVM_pres_bigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, unigrams=False, bigrams=True, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", unigrams=False, bigrams=True, presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", unigrams=False, bigrams=True, presence=True))
    return prediction

# Bayes and SVM with Presence and Unigrams + Bigrams
def NB_pres_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    logProbs = smoothed_log_probs(train_set_POS, train_set_NEG, bigrams=True)
    prediction = naive_bayes(test_set_POS, logProbs, class_probabilities, "POS", bigrams=True, presence=True)
    prediction.update(naive_bayes(test_set_NEG, logProbs, class_probabilities, "NEG", bigrams=True, presence=True))
    return prediction

def SVM_pres_unigramsbigrams(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG):
    model, wordMap = train_svm(train_set_POS, train_set_NEG, bigrams=True, presence=True)
    prediction = predict_svm(model, test_set_POS, wordMap, "POS", bigrams=True, presence=True)
    prediction.update(predict_svm(model, test_set_NEG, wordMap, "NEG", bigrams=True, presence=True))
    return prediction


# FOR 3 FOLD CROSS VALIDATION
# Unigrams freq and pres
compare_systems_task1(RR_splitting(current_dir,3), NB_freq_unigrams, SVM_freq_unigrams, "NB_freq_unigrams, 3 folds", "SVM_freq_unigrams, 3 folds")
compare_systems_task1(RR_splitting(current_dir,3), NB_pres_unigrams, SVM_pres_unigrams, "NB_pres_unigrams, 3 folds", "SVM_pres_unigrams, 3 folds")

# Bigrams freq and pres
compare_systems_task1(RR_splitting(current_dir,3), NB_freq_bigrams, SVM_freq_bigrams, "NB_freq_bigrams, 3 folds", "SVM_freq_bigrams, 3 folds")
compare_systems_task1(RR_splitting(current_dir,3), NB_pres_bigrams, SVM_pres_bigrams, "NB_pres_bigrams, 3 folds", "SVM_pres_bigrams, 3 folds")

# Unigrams + Bigrams freq and presence
compare_systems_task1(RR_splitting(current_dir,3), NB_freq_unigramsbigrams, SVM_freq_unigramsbigrams, "NB_freq_unigramsbigrams, 3 folds", "SVM_freq_unigramsbigrams, 3 folds")
compare_systems_task1(RR_splitting(current_dir,3), NB_pres_unigramsbigrams, SVM_pres_unigramsbigrams, "NB_pres_unigramsbigrams, 3 folds", "SVM_pres_unigramsbigrams, 3 folds")

#
# #  FOR 10 FOLD CROSS VALIDATION
# # Unigrams freq and pres
# compare_systems(RR_splitting(current_dir), NB_freq_unigrams, SVM_freq_unigrams, "NB_freq_unigrams, 10 folds", "SVM_freq_unigrams, 10 folds")
# compare_systems(RR_splitting(current_dir), NB_pres_unigrams, SVM_pres_unigrams, "NB_pres_unigrams, 10 folds", "SVM_pres_unigrams, 10 folds")
#
# # Bigrams freq and pres
# compare_systems(RR_splitting(current_dir), NB_freq_bigrams, SVM_freq_bigrams, "NB_freq_bigrams, 10 folds", "SVM_freq_bigrams, 10 folds")
# compare_systems(RR_splitting(current_dir), NB_pres_bigrams, SVM_pres_bigrams, "NB_pres_bigrams, 10 folds", "SVM_pres_bigrams, 10 folds")
#
# # Unigrams + Bigrams freq and presence
# compare_systems(RR_splitting(current_dir), NB_freq_unigramsbigrams, SVM_freq_unigramsbigrams, "NB_freq_unigramsbigrams, 10 folds", "SVM_freq_unigramsbigrams, 10 folds")
# compare_systems(RR_splitting(current_dir), NB_pres_unigramsbigrams, SVM_pres_unigramsbigrams, "NB_pres_unigramsbigrams, 10 folds", "SVM_pres_unigramsbigrams, 10 folds")

