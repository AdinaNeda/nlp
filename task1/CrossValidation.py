import os
from task1 import NaiveBayes, SVM

current_dir = "./data/"
dataDirectoryPOS = "./data/POS/*"
dataDirectoryNEG = "./data/NEG/*"

def accuracy(predictions_pos, predictions_neg, test_set_POS, test_set_NEG):
    total_correct_pos = 0
    total_correct_neg = 0

    for prediction in predictions_pos.values():
        if(prediction == "POS") : total_correct_pos += 1
        else: total_correct_neg += 0

    for prediction in predictions_neg.values():
        if(prediction == "POS") : total_correct_pos += 0
        else: total_correct_neg += 1

    # print('total_correct_pos', total_correct_pos, "total_correct_neg", total_correct_neg)
    accuracy = (total_correct_pos + total_correct_neg) / (len(test_set_POS) + len(test_set_NEG))

    return accuracy;

def class_probabilities(dir_path):
    # Get list of files in POS and NEG directories
    pos_files_list = os.listdir(os.path.abspath(dir_path) + "/POS")
    neg_files_list = os.listdir(os.path.abspath(dir_path) + "/NEG")

    number_pos_files = len(pos_files_list)
    number_neg_files = len(neg_files_list)

    total_number_of_files = number_pos_files + number_neg_files

    return {"POS": number_pos_files / total_number_of_files , "NEG": number_neg_files / total_number_of_files}


def RR_splitting(dir_path, N=10):
    # Get list of files in POS and NEG directories
    pos_files_list = os.listdir(os.path.abspath(dir_path) + "/POS")
    neg_files_list = os.listdir(os.path.abspath(dir_path) + "/NEG")

    # Sort the lists; we do RR splits on the first part of the file name
    pos_files_list.sort()
    neg_files_list.sort()

    # Split the files in N folds
    folds = []
    for i in range(N):
        # cv000, cv010, cv020,. . . = Split 1
        # cv001, cv011, cv021,. . . = Split 2
        split = {"POS": [], "NEG": []}

        for index_split in range(i, len(pos_files_list), N):
            split['POS'].append(pos_files_list[index_split])
            split['NEG'].append(neg_files_list[index_split])

        folds.append(split)

    # print("FOLDS SIZE", len(folds))
    return folds;

def NaiveBayes_CrossValidation(folds):
    scores1 = [0.0] * len(folds)
    scores2 = [0.0] * len(folds)
    scores3 = [0.0] * len(folds)

    # print("len(folds)", len(folds))

    for i in range(len(folds)):
        train_set_POS = []
        train_set_NEG = []

        test_set_POS = []
        test_set_NEG = []
        for j in range(len(folds)):
            if(i == j):
                test_set_POS.extend(folds[i]["POS"])
                test_set_NEG.extend(folds[i]["NEG"])
            else:
                train_set_POS.extend(folds[j]["POS"])
                train_set_NEG.extend(folds[j]["NEG"])

        classProbabilities = class_probabilities(current_dir)

        # Experiment 1 - NB with unigrams
        logProbs = NaiveBayes.smoothed_log_probs(train_set_POS, train_set_NEG, True, False)
        predictionsPOS = NaiveBayes.naive_bayes(test_set_POS, logProbs, classProbabilities, 'POS', True, False)
        predictionsNEG = NaiveBayes.naive_bayes(test_set_NEG, logProbs, classProbabilities, 'NEG', True, False)
        # Calculate accuracy
        scores1[i] = accuracy(predictionsPOS, predictionsNEG, test_set_POS, test_set_NEG)

        # # Experiment 2 - NB with bigrams
        # logProbs = NaiveBayes.smoothed_log_probs(train_set_POS, train_set_NEG, False, True)
        # predictionsPOS = NaiveBayes.naive_bayes(test_set_POS, logProbs, classProbabilities, 'POS', False, True)
        # predictionsNEG = NaiveBayes.naive_bayes(test_set_NEG, logProbs, classProbabilities, 'NEG', False, True)
        # # Calculate accuracy
        # scores2[i] = accuracy(predictionsPOS, predictionsNEG, test_set_POS, test_set_NEG)
        #
        # # Experiment 3 - NB with unigrams + bigrams
        # logProbs = NaiveBayes.smoothed_log_probs(train_set_POS, train_set_NEG, True, True)
        # predictionsPOS = NaiveBayes.naive_bayes(test_set_POS, logProbs, classProbabilities, 'POS', True, True)
        # predictionsNEG = NaiveBayes.naive_bayes(test_set_NEG, logProbs, classProbabilities, 'NEG', True, True)
        # # Calculate accuracy
        # scores3[i] = accuracy(predictionsPOS, predictionsNEG, test_set_POS, test_set_NEG)

    # ("NB with unigrams", sum(scores1) / len(folds), "NB with bigrams", sum(scores2) / len(folds), "NB with uni + bi ", sum(scores3) / len(folds))
    return ("NB with unigrams", sum(scores1) / len(folds))

def SVM_CrossValidation(folds):
    # print("folds mine", folds)
    scores1 = [0.0] * len(folds)
    scores2 = [0.0] * len(folds)

    for i in range(len(folds)):
        train_set_POS = []
        train_set_NEG = []

        test_set_POS = []
        test_set_NEG = []
        for j in range(len(folds)):
            if (i == j):
                test_set_POS.extend(folds[i]["POS"])
                test_set_NEG.extend(folds[i]["NEG"])
            else:
                train_set_POS.extend(folds[j]["POS"])
                train_set_NEG.extend(folds[j]["NEG"])

        # Experiment - SVM
        # model, wordMap = SVM.train(train_set_POS, train_set_NEG)
        model1, wordMap2 = SVM.train_svm(train_set_POS, train_set_NEG)

        # print("THE MODEL 1", model1)

        pos_predictions = SVM.predict_svm(model1, test_set_POS, wordMap2, "POS")
        neg_predictions = SVM.predict_svm(model1, test_set_NEG, wordMap2, "NEG")

        # Calculate accuracy
        scores1[i] = accuracy(pos_predictions, neg_predictions, test_set_POS, test_set_NEG)

        # # Experiment - SVM with bigrams
        # model2, wordMap2 = SVM.train_svm(train_set_POS, train_set_NEG, False, True)
        #
        # pos_predictions = SVM.predict_svm(model2, test_set_POS, wordMap2, "POS", False, True)
        # neg_predictions = SVM.predict_svm(model2, test_set_NEG, wordMap2, "NEG", False, True)
        #
        # # # Calculate accuracy
        # scores2[i] = accuracy(pos_predictions, neg_predictions, test_set_POS, test_set_NEG)

    # ("with unigrams", sum(scores1) / len(folds), "with bigrams", sum(scores2) / len(folds))

    return ("with unigrams", sum(scores1) / len(folds))

#
# print("beautiful RR splitting: ")
# print(RR_splitting(current_dir, 3))
# print(RR_splitting(current_dir, 1))



# print("--------------------")
# print("Naive Bayes with cross validation 3 splits")
# print(NaiveBayes_CrossValidation(RR_splitting(current_dir, 3)))
# print("--------------------")
# print("Naive Bayes with cross validation 10 splits")
# print(NaiveBayes_CrossValidation(RR_splitting(current_dir, 10)))
#
# print("--------------------")
# print("SVM with cross validation 3 splits")
# print(SVM_CrossValidation(RR_splitting(current_dir, 3)))
# print("--------------------")
# print("SVM with cross validation 10 splits")
# print(SVM_CrossValidation(RR_splitting(current_dir, 10)))




