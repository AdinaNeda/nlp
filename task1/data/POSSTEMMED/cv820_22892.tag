``
The
Spanish
Prison
''
From
my
first
view
,
I
notic
that
David
Mamet
's
film
wa
intens
and
clever
at
time
but
bore
and
clich
at
other
time
especi
at
the
begin
.

From
my
second
view
howev
,
I
notic
that
Mamet
's
script
wa
actual
a
masterpiec
.

The
Spanish
Prison
,
like
The
Usual
Suspect
or
Chinatown
,
beg
for
the
viewer
's
complet
attent
.

Miss
a
scene
or
even
a
small
detail
,
and
you
could
miss
the
mean
of
the
entir
film
.

Also
like
the
aforement
film
,
it
reward
it
viewer
for
pai
attent
by
deliv
a
surpris
and
memor
end
.

Mamet
is
success
do
again
what
he
onc
tackl
in
1987
when
he
wrote
and
direct
Hous
of
Game
.

Like
hi
past
gem
,
The
Spanish
Prison
fool
the
audienc
as
much
-LRB-
or
even
more
-RRB-
than
the
victim
.

Becaus
we
were
deceiv
just
like
the
victim
,
we
feel
a
strong
attach
to
the
main
charact
and
consequ
ar
more
interest
in
the
film
.

Mamet
ha
the
talent
of
not
complet
end
a
film
gift
wrap
,
but
rather
leav
some
ambigu
toward
the
end
.

The
power
that
he
ha
in
forc
the
viewer
to
determin
who
the
good
gui
and
bad
gui
ar
onc
the
film
is
over
,
without
give
a
right
answer
,
is
uniqu
and
ingeni
.

The
Spanish
Prison
will
leav
you
serious
puzzl
and
intrigu
.

After
see
the
film
again
,
I
notic
that
the
detail
Mamet
us
ar
so
incred
accur
it
is
frighten
.

Hi
direct
is
veri
subtl
;
as
he
leav
so
mani
clue
explain
what
is
go
on
and
who
is
who
but
it
is
still
veri
difficult
to
tell
what
exactli
is
happen
even
if
you
see
all
of
them
,
or
think
you
see
all
of
them
.

The
stori
follow
a
young
up
and
come
businessman
,
Joe
Ross
,
-LRB-
Campbel
Scott
-RRB-
who
ha
just
creat
a
mysteri
and
lucr
``
process
''
that
everyon
is
try
to
get
their
hand
on
.

Soon
Ross
doe
not
know
who
he
can
trust
or
who
is
after
hi
``
process
.
''

Through
hi
journei
Ross
meet
a
number
of
mysteri
charact
:
The
rich
and
popular
Jimmi
Dell
,
a
secretari
who
ha
a
huge
crush
on
Ross
,
and
Ross
'
boss
Mr.
Klein
.

Plai
by
Steve
Martin
,
Rebecca
Pidgeon
,
and
Ben
Gazarra
respect
,
the
support
cast
is
veri
solid
especi
Martin
,
who
deserv
an
Oscar
nomin
for
hi
command
and
manipul
role
.

The
most
interest
charact
in
the
film
,
though
,
wa
Campbel
Scott
's
Joe
Ross
.

The
charact
's
transform
from
an
ingenu
and
polit
rooki
to
a
paranoid
veteran
is
veri
appar
and
justifi
.

David
Mamet
's
script
is
so
interest
becaus
we
know
peopl
do
n't
actual
talk
like
the
charact
in
the
film
,
yet
we
feel
so
connect
to
some
of
them
.

Hi
beguil
style
could
fool
ani
viewer
up
for
the
challeng
.

Thi
is
a
must
see
,
for
those
interest
in
solv
a
complex
puzzl
of
a
movi
.

