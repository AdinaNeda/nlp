Chad
'
z
rate
:
****
-LRB-
out
of
=
excel
-RRB-
1995
,
R
,
125
minut
-LSB-
2
hour
,
minut
-RSB-
-LSB-
thriller/mysteri
-RSB-
star
:
Brad
Pitt
-LRB-
Detect
David
Mill
-RRB-
,
Morgan
Freeman
-LRB-
Detect
William
Sommerset
-RRB-
,
Gwenyth
Paltrow
-LRB-
Traci
Mill
-RRB-
,
written
by
Andrew
K.
Walker
,
produc
by
Arnold
Kopelson
,
Phylli
Carlyl
,
direct
by
David
Fincher
.

``
Seven
''
is
on
of
the
best
mysteri
movi
I
've
ever
seen
.

It
's
extrem
intrigu
and
suspens
,
but
it
's
also
quit
fun
.

It
's
a
serial
killer
mysteri
,
but
you
do
n't
care
so
much
about
make
the
killer
pai
as
much
as
hope
thei
just
catch
the
killer
.

The
stori
is
a
cops-on-the-trail-of-serial-kil
mysteri
.

Someon
is
murder
peopl
who
ar
offend
of
``
The
Seven
Deadli
Sin
.
''

A
fat
man
-LRB-
gluttoni
-RRB-
wa
forc
to
eat
himself
to
death
,
a
lawyer
-LRB-
greed
-RRB-
is
slaughter
by
hi
own
rich
possess
...
I
could
go
on
but
reveal
ani
more
would
give
awai
entir
too
much
.

What
make
thi
film
so
uniqu
is
the
character
of
the
``
good
gui
''
which
make
the
unseen
villain
seem
so
vile
.

Brad
Pitt
star
is
quit
excel
as
Detect
David
Mill
,
the
cocki
rooki
.

Howev
,
Morgan
Freeman
is
even
better
as
Detect
William
Sommerset
,
the
wise
veteran
on
the
verg
of
retir
-LRB-
and
he
doe
n't
get
kill
by
the
end
-RRB-
.

There
is
a
great
sens
of
camaraderi
here
which
often
provid
for
a
breath
of
comic
relief
.

Thi
is
impress
becaus
the
mood
is
so
tens
,
and
the
fact
that
it
can
be
down-plai
as
well
as
it
is
here
-LRB-
let
alon
at
all
-RRB-
is
quit
an
accomplish
.

The
set
take
place
in
present
dai
New
York
Citi
but
the
art
direct
is
abl
to
give
the
citi
a
feel
of
the
evil
,
scari
place
mani
believ
it
to
be
.

The
product
design
is
superb
,
in
the
spirit
of
``
Batman
''
and
``
The
Crow
,
''
thi
film
embodi
the
Gothic
mood
.

The
wai
the
word
fade
in
and
out
,
along
with
the
freaki
Nine
Inch
Nail
music
realli
add
a
lot
to
the
stori
on
a
subconsci
level-even
the
credit
ar
scari
!

The
killer
's
victim
have
no
connect
at
all
,
and
thu
Mill
and
Sommerset
do
n't
have
much
to
go
on
to
solv
the
case
.

Thei
can
onli
wait
for
the
next
murder
to
occur
which
make
for
tremend
suspens
.

We
becom
just
as
tens
and
worri
as
the
detect
becaus
of
thi
thick
atmospher
of
the
unknown
.

Not
that
mani
film
have
such
an
interest
and
intrigu
screenplai
as
thi
.

The
onli
problem
I
had
with
the
film
is
the
wai
in
which
Mill
and
Sommerset
ar
lead
to
a
suspect
.

Let
's
just
sai
it
seem
a
littl
too
``
mysteri
movi
,
''
or
too
conveni
in
other
word
.

When
thei
ar
led
to
a
suspect
by
the
name
of
John
Doe
,
a
terrif
chase
scene
ensu
.

Thi
is
a
typic
thrill
element
,
but
it
work
perfectli
here
becaus
of
the
process
of
the
stori
.

If
anyth
,
thi
film
is
the
epitom
of
twist
end
.

I
do
n't
have
to
tell
you
Mill
and
Sommerset
final
catch
the
crimin
,
but
the
wai
in
which
thi
happen
is
surpris
.

There
is
a
scene
of
intrigu
philosophi
between
the
killer
and
the
detect
,
and
even
though
he
's
obvious
insan
,
hi
charisma
make
for
some
good
point
.

What
's
even
more
surpris
is
the
last
few
scene
themselv
as
the
suspens
come
to
a
terrif
climax
as
the
film
's
resolut
come
down
to
a
question
of
what
justic
realli
is
.

``
Seven
''
is
not
just
a
film
about
crime
,
but
about
the
evil
within
man
.

John
Doe
felt
he
wa
do
societi
a
favor
by
rid
it
of
``
scum
.
''

But
as
justifi
as
he
make
himself
seem
,
we
must
never
be
tempt
by
such
twist
ideal
of
justic
.

Pleas
visit
Chad
'
z
Movi
Page
@
-LSB-
1
-RSB-
http://members.aol.com/ChadPolenz
E-mail
:
-LSB-
2
-RSB-
ChadPolenz@aol.com

