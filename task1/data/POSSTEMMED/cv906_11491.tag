Bob
Goss
's
Niagara
Niagara
follow
a
blueprint
not
unlik
a
lot
of
young-lovers-on-the-road
movi
.

Wild
Marci
-LRB-
Robin
Tunnei
-RRB-
and
calm
Seth
-LRB-
Henri
Thoma
-RRB-
meet
cute
,
liter
run
into
each
other
while
shoplift
at
a
local
store
.

A
mere
coupl
of
scene
later
,
the
two
embark
on
a
journei
to
Toronto
from
their
small
,
unnam
American
town
in
pursuit
of
a
rare
doll
that
Marci
desper
want
.

Along
the
wai
,
true
love
inevit
blossom
.

What
set
Niagara
Niagara
apart
,
though
,
is
that
Marci
is
afflict
with
Tourett
's
Syndrom
,
a
neurolog
disord
that
caus
sudden
muscl
and
vocal
tic
.

Tunnei
,
displai
an
act
rang
not
hint
at
in
the
teenag
witch
thriller
The
Craft
,
deliv
an
astonish
perform
that
won
her
the
Best
Actress
prize
at
last
year
's
Venic
Film
Festiv
.

To
term
her
work
a
tour-de-forc
is
not
to
impli
that
she
attack
the
sceneri
;
Tunnei
's
effect
li
in
her
modul
and
vulner
,
which
make
her
depict
of
Marci
's
ill
--
which
often
caus
her
to
act
violent
--
that
much
more
convinc
and
tragic
.

She
and
the
nice
subtl
Thoma
develop
a
sweetli
innoc
and
beguilingli
off-kilt
chemistri
.

Their
journei
hit
a
few
rough
spot
creativ
along
the
wai
,
mostli
the
fault
of
writer
Matthew
Weiss
.

A
detour
involv
a
kindli
widow
-LRB-
Michael
Park
-RRB-
who
take
the
coupl
in
bring
the
stori
to
a
screech
halt
,
and
the
kei
charact
of
a
trigger-happi
pharmacist
-LRB-
Stephen
Lang
-RRB-
is
highli
unbeliev
.

But
these
misstep
do
not
blunt
the
power
of
Tunnei
's
bravura
turn
,
which
carri
Niagara
Niagara
to
a
level
of
poignanc
it
would
not
have
otherwis
achiev
.

-LRB-
open
March
20
-RRB-

