Back
in
1998
Dreamwork
unveil
their
first
comput
anim
movi
``
Antz
''
.

The
film
wa
critic
acclaim
,
and
went
on
to
gross
almost
$
100m
dollar
at
the
domest
box
offic
.

Again
in
1998
thei
releas
their
tradit
anim
film
``
The
Princ
of
Egypt
''
and
it
name
an
instant
classic
,
and
becam
a
huge
hit
as
well
.

On
a
win
streak
,
thei
releas
``
The
Road
to
El
Dorado
''
and
it
wa
n't
as
big
as
thei
hope
,
but
still
did
ok
,
then
thei
had
the
fantast
``
Chicken
Run
''
in
the
summer
.

Now
in
2001
Dreamwork
ha
releas
what
I
can
safe
name
,
on
of
the
top
five
best
anim
film
.

Shrek
-LRB-
Mike
Myer
-RRB-
is
about
a
love
,
green
Ogre
who
live
in
the
wood
of
Duloc
by
himself
.

Surround
by
fairi
tale
charact
,
he
hope
somehow
he
would
escap
from
everyon
.

The
ruler
of
Duloc
is
the
short
,
heartless
Lord
Farquaad
-LRB-
John
Lithgow
-RRB-
,
who
is
offer
a
reward
to
all
the
fairi
tale
creatur
,
if
thei
ar
captur
and
arrest
.

Of
cours
mani
of
the
owner
turn
them
in
for
gold
and
silver
,
but
on
of
those
creatur
is
Donkei
-LRB-
Eddie
Murphi
-RRB-
.

A
regular
donkei
,
except
he
talk
and
ha
a
definit
attitud
,
after
hi
owner
tri
to
disown
him
,
he
escap
into
the
wood
,
and
run
into
Shrek
.

Lord
Farquaad
howev
,
ha
differ
plan
and
make
all
the
fairi
tale
creatur
in
the
wood
near
Shrek
's
hous
.

To
get
hi
properti
back
,
and
hi
life
back
Shrek
must
rescu
Princess
Fiona
-LRB-
Cameron
Diaz
-RRB-
so
Lord
Farquaad
can
be
a
king
.

Shrek
want
hi
properti
back
,
but
he
never
thought
he
would
fall
in
love
with
Princess
Fiona
.

``
Shrek
''
is
such
an
amaz
film
,
that
while
watch
it
you
ca
n't
help
but
just
smile
the
entir
time
and
wonder
why
that
other
product
compani
ca
n't
make
movi
like
thi
.

The
anim
graphic
ar
so
great
,
that
thei
look
everi
bit
of
realist
.

I
saw
preview
for
thi
film
all
last
year
,
and
I
thought
to
myself
wow
,
what
a
great
movi
thi
will
be
.

I
must
sai
I
wa
not
disappoint
at
all
,
and
the
film
is
the
best
of
2001
so
far
,
and
will
go
down
on
my
best
of
2001
list
at
the
end
of
the
year
.

The
voic
talent
ar
brilliant
as
well
,
with
the
comic
geniu
Mike
Myer
.

In
the
past
he
ha
been
a
Wayn
,
an
Austin
,
and
a
person
marri
to
an
ax
murder
.

Now
he
is
Shrek
,
not
onli
doe
he
do
the
voic
talent
...
but
he
IS
Shrek
.

With
hi
Irish
accent
,
and
he
lovabl
person
,
Shrek
becom
a
friend
to
the
audienc
and
warm
your
heart
.

Eddie
Murphi
who
show
hi
voic
talent
in
1998
``
Mulan
''
doe
anoth
bang
up
job
here
,
and
he
is
the
comic
relief
in
the
film
.

The
tone
of
hi
voic
,
the
line
he
utter
,
and
the
joke
he
give
,
he
even
outdo
the
comic
geniu
of
Robin
William
in
``
Aladdin
''
.

The
alwai
beauti
Cameron
Diaz
give
the
voic
of
Princess
Fiona
and
she
doe
a
great
job
as
well
.

She
is
veri
funni
,
and
her
charact
fit
her
to
a
T.
Of
cours
the
best
support
voic
is
John
Lithgow
,
who
is
person
on
of
my
favorit
actor
.

He
as
the
bad
gui
Lord
Farquaad
doe
a
great
job
in
be
both
sinist
and
funni
at
the
same
time
.

At
time
I
wa
so
amaz
by
the
comput
graphic
,
that
you
ca
n't
help
but
just
start
look
at
background
.

There
is
mani
hidden
joke
in
the
film
,
and
even
reflect
of
light
on
the
screen
like
a
camera
.

``
Shrek
''
is
a
definit
kid
film
,
but
not
just
a
kid
film
for
it
is
a
film
for
the
entir
famili
to
enjoi
.

There
ar
mani
adult
joke
in
the
film
that
the
kid
wo
n't
get
,
but
the
parent
will
be
laugh
.

Believ
me
though
,
everyon
will
be
laugh
the
entir
wai
through
.

In
the
end
,
``
Shrek
''
give
a
veri
import
messag
to
it
's
audienc
to
enjoi
and
be
who
you
ar
.

Everybodi
out
there
is
beauti
in
their
own
wai
,
even
though
it
mai
be
insid
.

There
's
not
mani
word
that
can
prais
``
Shrek
''
enough
,
but
my
word
to
you
is
to
stop
what
you
ar
do
and
see
thi
movi
now
.

