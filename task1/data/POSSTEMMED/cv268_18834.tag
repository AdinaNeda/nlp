How
doe
on
do
a
comedi
focus
on
the
mob
?

Well
,
a
few
year
ago
,
there
wa
a
sly
littl
comedi
call
The
Freshman
,
which
had
Marlon
Brando
do
a
romp
of
a
send-up
of
hi
famou
Don
Corleon
charact
and
,
recent
,
the
utterli
stupid
-LRB-
in
a
bad
wai
-RRB-
movi
Mafia
!

fail
to
realli
get
the
joke
.

Thi
time
around
,
howev
,
Warner
Bros.
mai
have
gotten
it
right
with
Analyz
Thi
,
a
movi
that
hit
it
target
more
than
it
miss
...
and
for
plenti
of
reason
.

The
stori
goe
like
thi
.

A
mobster
-LRB-
Robert
DeNiro
-RRB-
find
himself
receiv
sever
panic
attack
as
of
late
,
fuel
by
the
stress
of
an
upcom
mobster
meet
and
a
near-death
follow
a
drive-bi
shoot
.

It
's
these
attack
that
prompt
him
to
hire
a
psychiatrist
-LRB-
Billi
Crystal
-RRB-
who
's
mostli
reluct
to
take
the
mobster
's
case
for
two
reason
.

First
,
of
cours
,
he
's
a
mobster
,
but
secondli
,
he
's
try
to
get
marri
and
enjoi
a
quiet
honeymoon
with
hi
wife
-LRB-
Lisa
Kudrow
-RRB-
and
kid
.

And
,
of
cours
,
he
ca
n't
rest
worth
for
a
moment
becaus
the
mobster
's
goon
ar
alwai
need
the
shrink
for
someth
,
be
it
a
quick
consult
or
dream
analyz
.

Fortun
,
the
movi
is
plai
out
for
plenti
of
laugh
.

Director
Harold
Rami
-LRB-
who
also
helm
the
comedi
Multipl
,
which
wa
n't
half
bad
-RRB-
keep
the
tone
peppi
and
light
,
even
if
it
's
sometim
sprinkl
by
slight
dramat
moment
-LRB-
shoot
ca
n't
alwai
be
taken
for
laugh
,
you
know
-RRB-
.

DeNiro
doe
some
of
hi
finest
comic
work
sinc
The
King
of
Comedi
,
give
off
a
somewhat
similar
perform
to
hi
role
in
Goodfella
,
but
with
a
bit
of
a
lighter
heart
.

-LRB-
A
scene
where
he
tri
to
displai
hi
anger
over
the
phone
and
fail
miser
is
hilari
.
-RRB-

Crystal
plai
an
excel
straight
man
to
DeNiro
's
charact
,
uplift
him
from
such
bomb
as
Father
's
Dai
and
My
Giant
.

Kudrow
is
also
a
hoot
as
Crystal
's
wife-to-b
,
who
's
on
the
verg
of
a
breakdown
thank
to
DeNiro
's
presenc
.

Is
the
movi
perfect
?

Not
realli
,
thank
to
a
simplist
end
and
some
slightli
wast
moment
with
a
charact
portrai
by
Chazz
Palminteri
-LRB-
he
's
a
comic
actor
,
too
,
come
on
,
give
him
some
more
!
-RRB-

,
but
it
is
an
enjoy
romp
that
let
DeNiro
do
someth
differ
but
same
for
a
chang
,
take
hi
dramat
act
to
a
new
field
of
comedi
.

Kudo
to
Crystal
and
Kudrow
for
not
just
make
it
hi
show
,
either
.

Line
of
the
movi
:
Crystal
:
``
When
you
said
you
need
famili
therapi
,
thi
is
NOT
the
famili
I
had
in
mind
!
''

