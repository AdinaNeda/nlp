EARTH
is
a
harsh
,
unconsol
drama
about
the
time
when
India
gain
independ
from
Britain
and
the
ensu
turmoil
that
engulf
the
subcontin
.

Peopl
who
supposedli
love
the
same
land
and
the
same
God
found
themselv
in
bitter
conflict
,
as
the
countri
divid
into
faction
fight
in
the
street
:
Indian
and
Pakistani
;
Muslim
,
Hindu
,
Sikh
,
Parse
.

The
stori
take
place
in
Lahor
,
which
wa
Indian
befor
Partit
,
and
Pakistani
afterward
,
regardless
of
what
it
citizen
might
call
themselv
.

The
action
is
seen
through
the
ey
of
Lenni
-LRB-
Maaia
Sethna
-RRB-
,
a
young
Parse
girl
,
but
it
is
realli
about
her
nanni
,
Shanta
.

Shanta
ha
a
small
circl
of
male
admir
.

Two
of
the
men
ar
suitor
:
Hasan
-LRB-
Rahul
Khanna
-RRB-
,
a
Hindu
,
and
Dil
Navaz
-LRB-
Aamir
Khan
-RRB-
,
a
Muslim
.

The
other
--
Sikh
,
Hindu
,
Muslim
--
ar
older
;
some
ar
marri
;
but
all
appreci
Shanta
,
who
is
plai
by
the
stirringli
beauti
Nandita
Da
.

-LRB-
She
also
star
in
Deepa
Mehta
's
previou
film
,
FIRE
,
the
first
part
of
a
themat
trilogi
which
will
conclud
with
WATER
.
-RRB-

In
an
earli
scene
,
the
men
ar
sit
in
a
park
talk
with
Shanta
and
Lenni
;
polit
tension
in
the
countri
is
grow
as
the
dai
of
independ
near
.

One
of
the
men
remark
,
jokingli
,
that
their
littl
group
is
probabl
on
of
the
last
place
in
the
citi
where
the
differ
religion
can
still
get
along
.

He
is
wrong
:
the
men
make
barb
littl
comment
to
each
other
,
spoken
like
joke
,
but
with
an
undercurr
of
fanatic
.

The
tenor
of
these
exchang
grow
darker
and
more
bitter
as
the
movi
progress
.

It
is
easi
to
perceiv
,
in
these
squabbl
,
the
trajectori
which
end
in
mass
violenc
and
slaughter
.

The
group
around
Shanta
repres
,
in
a
sens
,
a
unit
India
,
and
Shanta
herself
the
ideal
motherland
--
on
which
all
love
,
which
inspir
Hindu
,
Muslim
,
and
Sikh
to
live
peacabl
togeth
.

But
the
circl
around
Shanta
is
sunder
,
as
is
the
State
.

A
Sikh
is
persecut
,
hide
,
then
flee
.

Another
man
is
murder
.

The
group
of
friend
will
not
meet
again
.

The
movi
is
rife
with
imag
of
breakag
and
destruct
:
a
plate
shatter
on
the
floor
,
a
stuf
toi
torn
apart
by
an
upset
child
,
and
,
most
brutal
,
when
the
tension
escal
into
violenc
,
a
man
is
held
down
,
ti
between
two
car
,
and
rip
in
half
by
the
oppos
movement
of
the
vehicl
.

It
is
a
viscer
and
effect
metaphor
for
a
countri
which
is
be
destroi
just
as
painfulli
.

The
crux
of
EARTH
's
plot
is
the
transform
undergon
by
Dil
Navaz
.

Hi
sister
ar
kill
in
brutal
fashion
,
by
Hindu
,
becaus
thei
ar
Muslim
.

Dil
Navaz
seek
solac
from
Shanta
,
and
ask
in
desper
that
she
marri
him
.

But
she
love
Hasan
,
and
tenderli
refus
Dil
Navaz
's
offer
.

The
combin
of
famili
anguish
and
romant
reject
twist
insid
him
,
and
it
is
easi
to
see
how
he
is
seduc
by
the
grow
mob
mental
:
the
Muslim
caus
give
him
a
motiv
to
vent
hi
anger
and
hate
.

Despit
the
thing
he
later
doe
,
it
is
to
the
credit
of
EARTH
that
it
doe
not
simpli
denounc
Dil
Navaz
and
those
like
him
,
but
show
how
circumst
can
make
decent
enough
men
do
monstrou
thing
.

The
conclus
--
the
consequ
of
Dil
Navaz
's
transform
--
is
rather
abrupt
.

The
stori
EARTH
tell
doe
not
end
.

It
just
stop
.

It
is
an
appropri
finish
:
give
us
closur
would
have
impart
,
at
least
on
an
aesthet
level
,
a
sens
of
resolut
,
of
satisfact
.

But
there
wa
no
resolut
for
India
and
Pakistan
--
thei
have
been
at
war
,
on
a
small
or
larg
scale
,
for
decad
.

Sinc
the
person
drama
in
EARTH
mirror
the
polit
struggl
of
a
nation
,
it
is
onli
right
that
those
drama
should
have
no
terminu
.

There
is
no
heal
to
be
had
,
no
closur
,
on
ani
level
-LRB-
despit
the
needless
,
tacked-on
end
,
with
voice-ov
narrat
from
Lenni
as
an
adult
,
frame
the
stori
-RRB-
.

EARTH
is
,
in
some
wai
,
an
unexcept
movi
.

It
doe
not
reinvent
the
wheel
,
follow
instead
the
standard
pattern
of
histor
drama
.

But
Deepa
Mehta
's
directon
is
assur
,
and
the
issu
she
address
run
deep
.

EARTH
doe
for
India
what
movi
like
Zhang
Yimou
's
TO
LIVE
and
Tian
Zhuangzhuang
's
THE
BLUE
KITE
did
for
China
:
thei
show
us
the
agoni
endur
by
unremark
peopl
who
had
the
bad
luck
to
be
caught
up
and
smash
in
the
crucibl
of
histori
.

Such
film
strike
me
as
inher
valuabl
,
even
when
flaw
.

Thei
mai
be
bias
or
inaccur
,
but
thei
bear
the
weight
of
human
hardship
,
and
thi
confer
on
them
a
nobil
and
graviti
which
other
,
more
invent
film
,
often
lack
.

It
is
notabl
that
all
three
movi
were
ban
in
their
nativ
countri
:
the
event
shown
mai
have
happen
decad
ago
,
but
thei
still
touch
a
nerv
.

The
issu
tackl
in
EARTH
have
a
particular
urgenc
in
light
of
the
nuclear
test
bomb
that
took
place
last
year
.

In
the
near
futur
,
India
and
Pakistan
mai
launch
nuclear
missil
at
each
other
.

At
the
time
of
the
test
I
had
some
understand
of
the
reason
why
thi
might
happen
;
after
see
EARTH
,
I
have
a
much
,
much
better
understand
.

Subject
Camera
-LRB-
subjective.freeservers.com
-RRB-
Movi
Review
by
David
Dalgleish
-LRB-
-LSB-
1
-RSB-
daviddalgleish@yahoo.com
-RRB-

