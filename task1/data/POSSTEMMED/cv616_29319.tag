A
welcom
cinemat
trend
over
the
past
coupl
of
year
ha
been
to
make
the
work
of
Shakespear
more
access
to
the
younger
audienc
who
ar
the
vast
major
of
moviego
by
contempor
the
Bard
and
alter
the
set
to
familiar
surround
.

A
prime
exampl
of
thi
is
1999
's
10
Thing
I
Hate
About
You
,
which
wa
basic
The
Tame
of
the
Shrew
in
a
high
school
.

Less
success
wa
last
year
's
adapt
of
Hamlet
with
Ethan
Hawk
as
the
heir
appar
of
the
Denmark
Corp.
.

Here
the
set
wa
the
corpor
world
of
New
York
.

Now
come
O
,
a
faith
retel
of
on
of
the
most
tragic
of
Shakespear
's
tragedi
.

Like
10
Thing
I
Hate
About
You
,
the
stage
is
a
high
school-a
privat
prep
school
where
Odin
Jame
-LRB-
Mekhi
Phifer
-RRB-
is
the
on-court
gener
,
the
basketbal
team
's
standout
player
.

Julia
Stile
,
featur
in
10
Thing
I
Hate
About
You
as
well
as
perform
Ophelia
in
Hawk
's
Hamlet
,
plai
Desi
Brabl
,
Odin
's
girlfriend
,
whose
father
also
happen
to
be
dean
of
the
institut
.

Josh
Hartnett
-LRB-
Pearl
Harbor
-RRB-
is
on
hand
for
the
villaini
,
plai
the
scheme
Hugo
,
who
set
the
tragic
wheel
in
motion
.

O
wa
actual
film
a
coupl
of
year
ago
,
but
wa
shelv
becaus
of
the
Columbin
High
School
tragedi
.

The
movi
,
direct
by
Tim
Blake
Nelson-best
known
for
hi
co-star
role
in
O
Brother
,
Where
Art
Thou
?

-
and
written
by
Brad
Kaaya
,
is
veri
grim
.

An
air
of
tragedi
weigh
it
down
from
the
open
scene
to
the
close
credit
.

And
that
is
how
it
should
be
.

Of
all
of
Shakespear
's
tragedi
,
Othello
is
the
most
harrow
,
not
becaus
of
ani
bodi
count
,
but
becaus
of
the
psycholog
havoc
wreak
by
Iago
,
whose
motiv
in
the
sheer
pleasur
of
hi
treacheri
.

Kaaya
's
profane-fil
script-aft
all
,
these
ar
high
school
kids-do
offer
Hugo
a
subtl
motiv
of
sort
.

The
young
man
consid
himself
the
basketbal
team
's
prime
util
man
who
doe
a
littl
bit
of
everything-pass
,
shoot
,
rebound-but
is
constantli
in
the
shadow
of
Odin
.

Plu
hi
father
,
the
team
's
coach-plai
with
a
feroci
intens
by
Martin
Sheen-show
more
love
and
concern
on
hi
star
player
than
on
hi
own
flesh
and
blood
.

The
script
follow
the
familiar
line
of
the
classic
.

Hugo
begin
drop
hint
to
Odin
concern
Desi
's
fidel
,
erod
the
star
's
confid
in
those
he
consid
closest
to
him
,
affect
both
hi
relationship
and
hi
game
.

It
all
end
in
blood
,
of
cours
,
which
is
why
the
movi
wa
origin
shelv
.

The
perform
in
O
vari
.

Phifer
is
at
first
cocki
and
self-assur
,
emot
he
handl
with
eas
.

He
also
doe
quit
well
displai
Odin
's
darker
side
;
hi
defens
sensit
about
be
the
onli
black
at
the
institut
,
hi
grow
distrust
of
those
he
love
,
hi
rage
and
the
eros
of
hi
belief
system
.

Hartnett
is
appropri
cool
and
Machiavellian
as
he
weav
hi
deadli
web
of
li
and
deceit
,
turn
friend
against
friend
,
and
lover
against
lover
.

Stile
seem
a
bit
stilt
,
but
occasion
also
rise
to
the
occas
,
move
from
ador
to
confus
to
fear
of
her
young
knight
.

The
film
doe
contain
a
few
misstep
:
A
subplot
involv
Hugo
's
theft
of
the
school
mascot
is
left
hang
.

Also
Desi
's
action
concern
Michael
-LRB-
Andrew
Keegan
-RRB-
,
Odin
's
former
teammat
and
best
friend
who
wa
kick
off
the
team
becaus
of
on
of
Hugo
's
machin
,
is
puzzl
.

Why
she
would
continu
be
seen
with
Michael-howev
innoc
the
circumstances-when
she
know
her
man
suspect
the
two
of
cheat
behind
hi
back
.

It
is
irrat
.

But
these
ar
minor
carp
.

Overal
,
O
is
a
stylish
and
faith
adapt
.

If
noth
els
,
it
mai
encourag
some
peopl
to
read
the
origin
text
,
and
that
will
counterbal
the
sever
of
thi
featur
.

