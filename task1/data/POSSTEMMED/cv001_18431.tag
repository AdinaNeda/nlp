Everi
now
and
then
a
movi
come
along
from
a
suspect
studio
,
with
everi
indic
that
it
will
be
a
stinker
,
and
to
everybodi
's
surpris
-LRB-
perhap
even
the
studio
-RRB-
the
film
becom
a
critic
darl
.

MTV
Film
'
_
Election
,
a
high
school
comedi
star
Matthew
Broderick
and
Rees
Witherspoon
,
is
a
current
exampl
.

Did
anybodi
know
thi
film
exist
a
week
befor
it
open
?

The
plot
is
decept
simpl
.

Georg
Washington
Carver
High
School
is
have
student
elect
.

Traci
Flick
-LRB-
Rees
Witherspoon
-RRB-
is
an
over-achiev
with
her
hand
rais
at
nearli
everi
question
,
wai
,
wai
,
high
.

Mr.
``
M
''
-LRB-
Matthew
Broderick
-RRB-
,
sick
of
the
megalomaniac
student
,
encourag
Paul
,
a
popular-but-slow
jock
to
run
.

And
Paul
's
nihilist
sister
jump
in
the
race
as
well
,
for
person
reason
.

The
dark
side
of
such
sleeper
success
is
that
,
becaus
expect
were
so
low
go
in
,
the
fact
that
thi
wa
qualiti
stuff
made
the
review
even
more
enthusiast
than
thei
have
ani
right
to
be
.

You
ca
n't
help
go
in
with
the
baggag
of
glow
review
,
which
is
in
contrast
to
the
neg
baggag
that
the
review
were
like
to
have
.

_
Election
,
a
good
film
,
doe
not
live
up
to
it
hype
.

What
make
_
Election
_
so
disappoint
is
that
it
contain
signific
plot
detail
lift
directli
from
_
Rushmor
_
,
releas
a
few
month
earlier
.

The
similar
ar
stagger
:
Traci
Flick
-LRB-
_
Election
_
-RRB-
is
the
presid
of
an
extraordinari
number
of
club
,
and
is
involv
with
the
school
plai
.

Max
Fischer
-LRB-
_
Rushmor
_
-RRB-
is
the
presid
of
an
extraordinari
number
of
club
,
and
is
involv
with
the
school
plai
.

The
most
signific
tension
of
_
Election
_
is
the
potenti
relationship
between
a
teacher
and
hi
student
.

The
most
signific
tension
of
_
Rushmor
_
is
the
potenti
relationship
between
a
teacher
and
hi
student
.

Traci
Flick
is
from
a
singl
parent
home
,
which
ha
contribut
to
her
drive
.

Max
Fischer
is
from
a
singl
parent
home
,
which
ha
contribut
to
hi
drive
.

The
male
bumbl
adult
in
_
Election
_
-LRB-
Matthew
Broderick
-RRB-
pursu
an
extramarit
affair
,
get
caught
,
and
hi
whole
life
is
ruin
.

He
even
get
a
bee
sting
.

The
male
bumbl
adult
in
_
Rushmor
_
-LRB-
Bill
Murrai
-RRB-
pursu
an
extramarit
affair
,
get
caught
,
and
hi
whole
life
is
ruin
.

He
get
sever
bee
sting
.

And
so
on
.

What
happen
?

How
is
it
that
an
individu
screenplai
-LRB-
_
Rushmor
_
-RRB-
and
a
novel
-LRB-
_
Election
_
-RRB-
contain
so
mani
signific
plot
point
,
and
yet
both
film
were
probabl
not
even
awar
of
each
other
,
made
from
two
differ
studio
,
from
a
genr
-LRB-
the
high
school
geek
reveng
movi
-RRB-
that
had
n't
been
fulli
form
yet
?

Even
so
,
the
strength
of
_
Election
_
reli
upon
it
fantast
perform
from
Broderick
,
Witherspoon
,
and
newcom
Jessica
Campbel
,
as
Paul
's
anti-soci
sister
,
Tammi
.

Broderick
here
is
plai
the
Mr.
Roonei
role
from
_
Ferri
Bueller
_
,
and
he
seem
to
be
have
the
most
fun
he
's
had
sinc
then
.

Witherspoon
is
a
revel
.

It
's
earli
in
the
year
,
it
's
a
comedi
,
and
teenag
have
littl
clout
,
but
for
my
monei
,
Witherspoon
deserv
an
Oscar
nomin
.

And
onc
Campbel
's
charact
get
go
,
like
in
her
fantast
speech
in
the
gymnasium
,
then
you
're
won
over
.

One
thing
that
's
been
bother
me
sinc
I
've
seen
it
.

There
is
an
extraordinari
amount
of
sexual
in
thi
film
.

I
suppos
that
,
come
from
MTV
film
,
I
should
expect
no
less
...
but
the
film
start
off
light
and
airi
,
like
a
sitcom
.

As
the
screw
tighten
,
and
the
tension
mount
,
Alexand
Payn
decid
to
add
element
that
,
frankli
,
distract
from
the
stori
.

It
is
bad
enough
that
Mr.
M
doe
n't
like
Traci
's
determin
to
win
at
all
cost
,
but
did
thei
have
to
throw
in
the
student/teach
relationship
?

Even
so
,
there
's
no
logic
reason
why
Mr.
M
ha
an
affair
when
he
doe
.

There
's
a
lot
to
like
in
_
Election
_
,
but
the
plot
similar
to
_
Rushmor
_
,
and
the
tonal
nosed
it
take
as
it
get
explicitli
sex-driven
,
mark
thi
as
a
disappoint
.

