AIRPLANE
!

is
consid
among
mani
to
be
the
epitom
of
satir
film-mak
.

After
all
,
it
's
brought
to
us
by
on
of
the
best
known
satir
writing/direct
team
.

Even
if
most
peopl
do
n't
recogn
the
name
behind
the
film
,
thei
ar
bound
to
recogn
the
titl
:
AIRPLANE
!

,
TOP
SECRET
,
THE
NAKED
GUN
,
and
HOT
SHOTS
to
name
a
few
.

But
although
the
Zucker/Abrahams/Zuck
team
wa
first
introduc
with
THE
KENTUCKY
FRIED
MOVIE
in
1977
,
AIRPLANE
!

remain
the
true
cornerston
of
their
work
,
and
their
directori
debut
.

In
the
seventi
,
disast
film
seem
to
be
at
an
all
time
high
.

Film
like
EARTHQUAKE
,
THE
TOWERING
INFERNO
,
and
THE
POSEIDON
ADVENTURE
were
big
hit
.

There
wa
also
a
seri
about
the
disast
that
can
aris
when
travel
by
plane-a
seri
that
span
the
entir
decad
.

And
so
,
in
1980
,
we
were
introduc
to
a
new
airplan
disast
film
.

Thi
time
the
disast
had
noth
to
do
with
a
maniac
hijack
or
crash
into
the
ocean
...
it
had
to
do
with
bad
fish
.

AIRPLANE
!

is
the
stori
of
Ted
Striker
-LRB-
Robert
Hai
-RRB-
-
an
ex-fighter-pilot
who
ha
never
gotten
over
the
fact
that
a
decis
he
had
to
make
in
the
midst
of
war
led
to
the
death
of
six
,
er
,
seven
comrad
.

Unabl
to
stop
live
in
the
past
,
Ted
's
world
fell
apart
.

He
spent
hi
time
move
from
citi
to
citi
without
ever
have
a
stabl
job
,
and
eventu
,
as
we
begin
thi
film
,
is
get
dump
by
hi
lover
,
flight
attend
Elain
Dickinson
-LRB-
Juli
Hagerti
-RRB-
.

In
a
desper
attempt
not
to
lose
her
,
Ted
bui
a
ticket
for
the
same
flight
Elain
is
work
.

Unfortun
,
she
is
unsympathet
and
even
critic
Ted
for
follow
her
,
which
leav
him
wallow
in
self-piti
.

In
the
midst
of
it
all
,
someth
happen
.

Someon
get
sick
.

Then
anoth
.

Then
anoth
.

Soon
,
the
whole
plane
is
full
of
deathly-il
Chicago-bound
peopl
...
and
when
the
pilot
get
sick
-LRB-
plai
by
Peter
Grave
,
Kareem
Abdul-Jabbar
,
and
Frank
Ashmore
-RRB-
,
who
will
save
the
dai
?

-LRB-
Gee
,
I
wonder
-RRB-
Of
cours
the
main
draw
here
is
the
non-stop
``
whiz-bang
''
comedi
,
with
a
hefti
dose
of
both
visual
and
spoken
gag
.

Although
I
had
seen
thi
movi
a
few
time
befor
,
I
wa
still
laugh
thru
the
entir
thing
.

The
most
fun
charact
by
far
is
the
doctor
,
plai
by
Lesli
Nielsen
.

He
's
a
dry
,
subtl
,
sterotyp
straight-fac
doctor
that
end
up
sai
some
of
the
funniest
line
of
the
film
.

In
a
film
like
thi
,
you
're
not
realli
expect
an
elabor
product
,
so
on
ca
n't
complain
about
the
amateurish
qualiti
.

What
you
can
complain
about
is
the
absolut
annoi
Johnni
,
plai
by
Stephen
Stucker
.

Thi
charact
is
pointless
,
nauseat
,
and
veri
unfunni
.

I
do
n't
know
why
he
wa
put
in
there
,
and
I
do
n't
think
anybodi
will
find
a
good
excus
.

And
when
the
film
near
an
end
,
it
slow
down
quit
a
bit
,
almost
get
old
.

You
got
ta
give
it
to
a
movi
that
onli
run
88
minut
and
can
feel
two
hour
long
.

Aside
from
that
,
AIRPLANE
!

is
realli
a
top-notch
spoof
that
will
like
forev
be
known
as
a
spoof
classic
.

It
would
be
a
good
choic
to
pop
in
the
VCR
with
a
group
of
friend
,
especi
if
thei
have
n't
seen
it
.

And
if
thei
ask
what
the
plot
is
,
just
tell
'
em
``
It
's
a
synopsi
of
the
basic
storylin
of
the
film
,
but
that
's
not
import
right
now
.
''

If
you
've
seen
the
film
,
you
'll
understand
;--RRB-

