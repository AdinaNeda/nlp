John
Cusack
is
the
kind
of
actor
who
seem
to
effortlessli
slide
into
hi
respect
film
role
.

So
effortlessli
that
peopl
tend
to
forget
he
's
there
,
much
in
the
wai
peopl
rare
recal
mani
of
the
great
charact
actor
-LRB-
anyon
who
can
put
the
name
Jame
Rebhorn
with
that
actor
's
face
is
invit
to
treat
themselv
to
a
product
from
on
of
my
sponsor
-RRB-
.

Exampl
:
The
other
dai
my
mother
ask
me
-LRB-
the
expert
,
of
cours
-RRB-
if
there
were
ani
movi
out
worth
see
,
never
mind
that
our
tast
could
n't
be
more
diverg
--
I
'll
never
forget
the
dai
she
recommend
that
I
go
see
A
NIGHT
AT
THE
ROXBURY
for
god
sake
,
and
to
be
fair
she
wa
mightili
piss
at
me
for
tell
her
that
GO
!

wa
a
lot
of
fun
-LRB-
which
it
is
-RRB-
,
so
I
did
n't
see
thi
as
anyth
more
than
a
futil
attempt
at
convers
.

I
mutter
,
with
a
littl
trepid
,
that
she
might
enjoi
HIGH
FIDELITY
.

She
respond
with
her
usual
queri
,
``
Whose
in
that
on
?
''

If
the
repli
is
Julia
Robert
you
probabl
assum
it
wa
someth
wispi
and
light
,
a
Sylvest
Stallon
movi
and
you
pictur
a
bloodbath
with
few
word
and
much
involuntari
bicep
flex
,
and
a
Woodi
Allen
movi
mean
hyper
articul
white
peopl
-LRB-
usual
of
the
Jewish
faith
-RRB-
fret
about
in
some
Upper
East
Side
palac
.

Anywai
,
when
I
answer
``
John
Cusack
''
,
she
repli
with
a
rather
stun
``
Who
?
''

,
as
if
it
wa
unreason
for
her
to
know
whom
I
wa
refer
to
.

Now
I
know
for
a
fact
that
my
mother
ha
seen
sever
Cusack
movi
,
most
recent
BEING
JOHN
MALKOVICH
and
PUSHING
TIN
,
but
I
suppos
it
's
understand
that
she
would
not
recogn
the
name
,
after
all
,
for
most
,
name
ar
tougher
than
face
.

I
show
her
a
pictur
of
Cusack
's
mug
in
the
paper
hope
for
an
``
Oh-him
,
ye
he
's
quit
good
''
instead
I
got
a
``
No-I
do
n't
know
him
''
.

At
thi
point
I
wa
wai
beyond
frustrat
into
a
realm
of
nose
hair
yank
anger
-LRB-
ye
,
filmic
ignor
is
all
it
take
to
get
me
into
the
nose
hair
pull
zone
-RRB-
,
till
I
realiz
someth
:
I
doubt
that
much
of
America
would
recogn
Cusack
's
rel
bland
visag
,
or
even
the
name
which
is
rather
bland
compar
to
the
cool
of
a
Brad
Pitt
or
Tom
Cruis
,
one-syl
name
that
glide
off
the
tongu
with
the
veloc
of
a
speed
car
.

Cu-sack
.

Cu-sack-no
,
not
quit
as
cool
.

I
'm
sure
that
twenti
year
from
now
Cusack
will
forev
be
refer
to
as
``
the
gui
from
-LRB-
add
in
the
Cusack
film
of
your
choic
-RRB-
''
.

He
's
not
a
brand
name
,
and
hi
movi
usual
ar
n't
big
moneymak
,
but
in
hi
own
unassum
wai
he
's
kind
of
a
geniu
.

With
hi
pleasant
,
common
man
look
,
and
charm
semi-articul
blather
he
ha
becom
on
of
the
most
likeabl
romant
lead
men
of
the
90
's
.

From
THE
SURE
THING
to
SAY
ANYTHING
Cusack
ha
merg
the
talent
of
earli
Tom
Hank
and
earli
Jack
Lemmon
.

Despit
the
fact
that
nearli
everi
charact
he
plai
is
a
variat
of
the
charact
befor
it
-LRB-
the
schlumpi
everi
man
-RRB-
,
he
's
a
constantli
enjoy
screen
persona
,
like
Albert
Brook
or
Woodi
Allen
.

And
he
know
how
to
chose
script
,
while
the
Stephan
Baldwin
's
and
Val
Kilmer
's
seem
to
put
them
selv
in
everyth
-LRB-
the
former
actual
had
the
insight
to
follow
up
Oscar
winner
USUAL
SUSPECTS
with
a
Pauli
Shore
vehicl
,
and
judg
from
hi
latest
project
it
look
like
he
still
ha
yet
to
fire
hi
agent
-RRB-
,
Cusack
gener
pick
reward
project
-LRB-
save
for
the
unwatch
HOT
PURSUIT
,
a
1988
film
also
featur
a
young
Ben
Stiller
-RRB-
.

HIGH
FIDELITY
is
hi
second
collabor
with
hit
or
miss
English
director
Stephan
Fear
-LRB-
THE
GRIFTERS
,
THE
HI-LO
COUNTRY
-RRB-
,
and
it
's
a
hit
.

It
wa
also
co-written
by
Cusack
,
and
some
of
hi
collabor
on
the
terrif
comedi
,
GROSSE
POINT
BLANK
.

It
's
not
quit
as
entertain
or
as
funni
as
that
film
,
but
as
it
progress
in
it
meander
fashion
,
the
movi
cast
someth
of
a
spell
over
me
.

HIGH
FIDELITY
begin
as
a
hipster
od
to
the
non-committ
Rob
,
a
vinyl
record
store
owner
,
who
open
the
film
by
break
down
the
fourth
wall
with
much
abandon
and
educ
us
in
hi
``
topbreak
up
list
''
.

Thi
is
someth
Cusack
doe
the
entir
film
a
la
FERRIS
BUELLER
'S
DAY
OFF
.

At
first
the
talking-directly-into-the-camera-schtick
had
me
vagu
annoi
,
mostli
becaus
it
remind
me
so
much
of
the
film
BODY
SHOTS
which
us
a
similar
conceit
,
but
it
began
to
grow
on
me
due
almost
entir
to
Cusack
's
witti
deliveri
.

He
's
the
kind
of
gui
we
do
n't
mind
talk
directli
to
us
.

Gradual
the
film
settl
into
a
SHAMPOO-like
tragi-com
charact
studi
,
of
a
gui
who
must
confront
himself
and
hi
person
failur
in
order
to
figur
out
the
true
person
he
is
-LRB-
ye
it
's
all
veri
existenti
-RRB-
.

Though
that
plot-lin
is
mostli
enjoy
,
the
thing
I
like
the
most
about
the
film
is
how
much
pleasur
it
offer
in
introduc
us
to
minor
charact
,
all
of
whom
-LRB-
now
thi
is
realli
someth
-RRB-
ar
terrif
enough
to
warrant
their
own
film
.

The
best
of
which
is
Jack
Black
-LRB-
of
the
veri
funni
show
TENACIOUS
D
,
a
program
that
sadli
went
the
wai
of
Bruce
Willi
'
hairâ
¦
unfortun
that
annoi
smirk
remain
-RRB-
,
an
ardent
record
store
employe
who
bulli
custom
into
bui
exactli
what
he
want
them
to
bui
.

He
's
the
kind
of
gui
I
'm
sure
we
've
all
met
befor
,
a
blowhard
who
think
he
's
alwai
right
and
will
do
anyth
in
hi
power
-LRB-
be
it
yell
,
argu
,
insult
-RRB-
to
hoist
hi
opinion
onto
other
.

But
I
like
thi
gui
,
becaus
of
how
Black
plai
him
.

The
actor
is
portli
with
a
round
babi
face
and
crazi
ey
,
but
despit
hi
size
he
leap
to
and
fro
like
a
manic
speed
freak
-LRB-
which
might
be
a
bit
of
a
redund
descript
,
see
as
how
all
the
speed
freak
I
know
ar
manic
-RRB-
.

He
's
the
kind
of
support
charact
who
would
be
imposs
annoi
comic
relief
in
most
other
film
.

-LRB-
Look
at
what
that
hack
Jan
de
Bont
did
to
poor
Phillip
Seymour
Hoffman
in
TWISTER
-RRB-
.

But
Fear
and
Black
never
let
that
happen
.

The
charact
,
for
all
hi
shuck
and
jive
is
steep
in
realiti
.

He
's
over
the
top
,
but
in
a
wai
we
can
all
probabl
relat
to
.

Catherin
Zeta-Jon
,
in
her
brief
role
,
give
a
vivid
portray
of
a
women
complet
enamor
with
her
own
``
charm
''
.

Tim
Robbin
regist
in
an
even
tinier
part
as
a
New
Age-typ
,
a
charact
plai
entir
for
cartoon
yuk
,
but
still
manag
to
work
it
desir
comic
effect
.

The
on
except
in
the
act
depart
is
a
bit
of
a
doozi
,
the
main
love
interest
plai
by
Danish
actress
Iben
Hjejl
-LRB-
MIFUNE
-RRB-
,
who
complet
obliter
ani
trace
of
a
Danish
accent
,
replac
it
with
a
stilt
American
phonat
.

I
give
her
kudo
for
attempt
such
a
contradictori
role
,
but
instead
of
be
likeabl
,
she
come
across
as
rather
robot
,
and
as
such
it
's
hard
to
see
why
Cusack
's
charact
would
be
so
obsess
with
her
especi
when
he
ha
a
beauti
,
intellig
writer
-LRB-
Natasha
Gregson
Wagner
-RRB-
potenti
wait
in
the
wing
.

Thi
remind
me
of
SAY
ANYTHING
,
where
the
charm
Cusack
wa
pair
with
the
charmless
Ione
Skye
,
and
in
MIDNIGHT
IN
THE
GARDEN
OF
GOOD
AND
EVIL
where
the
poor
bastard
had
to
put
up
with
Alison
Eastwood
-LRB-
whose
unbeliev
more
wooden
then
father
,
Clint
for
those
``
special
peopl
''
whose
wheel
turn
a
littl
slower
-RRB-
.

In
fact
Cusack
is
rare
pair
up
with
a
person
that
match
hi
,
though
I
'd
bet
that
if
Laura
and
Natasha
switch
role
,
the
result
would
be
more
effect
.

HIGH
FIDELITY
work
almost
entir
on
the
strength
of
it
charact
and
perform
.

Fear
'
direct
is
somewhat
stilt
,
and
the
script
is
sometim
a
littl
too
-LRB-
and
thi
is
a
word
I
'm
begin
to
hate
to
us
but
ala
I
must
-RRB-
quirki
for
it
own
good
-LRB-
I
think
I
'll
blame
that
on
Scott
Rosenberg
,
who
judg
from
what
he
did
with
BEAUTIFUL
GIRLS
,
CON
AIR
,
and
THINGS
TO
DO
IN
DENVER
WHILE
YOUR
DEAD
,
ha
hi
fingerprint
all
over
the
most
irrit
bit
of
thi
movi
-RRB-
.

But
Cusack
realli
doe
deserv
an
award
,
for
be
so
damnâ
¦
Cusackian
.

I
'm
afraid
that
hi
talent
ar
so
underst
that
he
mai
have
to
wait
till
hi
hip
need
replac
to
be
offer
such
an
award
worthi
of
hi
consider
talent
.

Hi
charact
,
Rob
-LRB-
whose
sort
of
like
an
older
,
more
bitter
Lloyd
from
SAY
ANYTHING
-RRB-
is
funni
and
tragic
without
be
pathet
,
and
that
Cusack
can
do
all
thi
,
and
still
not
imprint
himself
onto
the
mind
of
most
audienc
,
is
someth
of
an
achiev
.

