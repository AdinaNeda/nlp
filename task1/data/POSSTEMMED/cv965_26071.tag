In
mani
wai
,
``
TWOTG
''
doe
for
tough-gui
movi
what
LA
Confidenti
did
for
polic
stori
.

There
's
savvi
in
it
write
and
a
matur
patienc
in
allow
the
materi
to
unfold
.

But
,
I
suppos
,
we
would
n't
expect
less
from
Christoph
McQuarri
,
who
wa
respons
for
give
us
``
The
Usual
Suspect
''
.

Hi
write
talent
ar
still
in
tact
as
he
bring
to
us
a
bloodi
yet
intellectu
tale
of
two
unabash
but
dimwit
thug
and
a
plan
gone
awri
.

The
two
goon
ar
Parker
and
Longbaugh
-LRB-
Ryan
Phillip
and
Benicio
del
Toro
-RRB-
.

These
two
men
show
the
harden
wear
of
those
who
have
alwai
had
it
tough
.

Their
troubl
youth
have
forg
two
nihilist
soul
who
us
their
amaz
gunplai
skill
to
surviv
.

Given
the
choic
of
minimum
wage
or
petti
crime
,
thei
'll
alwai
choos
the
latter
.

Yet
,
thei
ar
incred
foolish
planner
.

Actualli
,
these
two
never
realli
have
a
plan
.

Thei
just
improvis
along
the
wai
,
hope
that
their
bravado
and
arsen
of
gun
will
take
care
of
anyth
that
thei
might
have
overlook
.

Their
next
scheme
,
which
thei
cook
up
about
as
fast
as
a
McDonald
's
burger
,
is
to
kidnap
some
surrog
mother
name
Robin
-LRB-
Juliett
Lewi
-RRB-
that
wa
hire
by
some
well-to-do
famili
.

If
all
goe
well
,
thei
'll
receiv
a
nice
bit
of
ransom
monei
and
life
goe
on
.

What
Parker
and
Longbaugh
do
not
realiz
is
that
Robin
is
carri
a
babi
for
the
Chidduck
famili
,
whose
patriarch
is
a
fear
crime
boss
.

Moreov
,
two
bodyguard
-LRB-
Nicki
Katt
and
Tay
Digg
-RRB-
constantli
escort
her
.

These
two
protector
find
an
eeri
sens
of
pleasur
when
be
confront
and
test
in
life
and
death
situat
.

Thei
would
rather
die
than
fail
;
thu
their
bravado
level
is
equal
high
.

But
it
seem
that
the
testosteron
level
of
Parker
and
Longbaugh
ar
a
bit
higher
,
and
thei
emploi
some
unusu
but
fun-to-watch
tactic
to
pull
off
the
kidnap
and
to
outrun
the
pursuer
in
a
sort
of
slow-mot
car
chase
down
a
seri
of
alleywai
.

The
next
hour
or
so
,
we
watch
the
Chidduck
camp
regroup
and
assess
their
situat
.

Dure
thi
time
,
addit
plot
line
come
into
plai
and
more
and
more
detail
ar
reveal
.

Becaus
the
Chidduck
's
can
not
go
to
the
polic
and
becaus
of
the
special
circumst
involv
the
kidnap
,
we
ar
introduc
to
more
charact
that
will
help
to
influenc
the
outcom
.

We
discov
that
not
everyon
in
the
Chidduck
camp
is
loyal
to
the
caus
.

As
a
result
,
there
ar
undercurr
of
a
conspiraci
,
and
element
of
betray
and
subterfug
.

The
sepia-ton
atmospher
is
made
all
the
more
fascin
thank
to
a
terrif
soundtrack
which
emit
velvet
forebod
,
shrill
of
intrigu
and
crescendo
that
tell
us
of
life
and
death
situat
that
ar
about
to
occur
.

The
momentum
and
the
cool
of
the
film
,
howev
,
begin
to
fizzl
in
the
lastminut
,
which
featur
an
overli
extend
shootout
where
our
two
desperado
try
to
make
their
wai
off
with
the
ransom
monei
.

You
can
expect
lot
of
bloodlet
and
an
odd
denouement
.

In
fact
,
you
mai
admir
thi
film
more
than
you
like
it
.

Yet
,
for
the
most
part
,
``
The
Wai
of
the
Gun
''
stai
on
target
.

It
feel
cool
with
it
element
of
conspiraci
and
gunplai
,
and
smart
for
it
invect
dialogu
and
tough
gui
poetri
.

