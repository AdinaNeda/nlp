Bobbi
Garfield
-LRB-
Yelchin
-RRB-
live
in
a
small
town
with
hi
mirthless
widow
mother
-LRB-
Hope
Davi
-RRB-
.

Bobbi
's
world
revolv
around
hi
friend
,
especi
the
sprite
Carol
-LRB-
Boorem
-RRB-
.

Then
on
dai
,
a
new
boarder
arriv
at
Bobbi
's
hous
.

Ted
Brautigan
-LRB-
Hopkin
-RRB-
is
an
enigmat
man
to
whom
Bobbi
take
an
immedi
like
.

As
the
bond
between
Bobbi
and
Ted
deepen
,
Bobbi
becom
privi
to
Ted
's
great
secret
,
an
event
which
will
chang
both
of
their
live
forev
.

A
small
but
enchant
movi
,
``
Heart
In
Atlanti
''
easili
recal
anoth
King-inspir
coming-of-ag
film
,
``
Stand
By
Me
''
,
both
in
term
of
it
set
and
the
sentiment
it
convei
.

``
Heart
''
is
a
tribut
to
the
magic
of
childhood
,
to
those
summer
when
the
dai
seem
neverend
,
and
noth
mean
more
than
your
closest
friend
.

Unlike
``
Stand
By
Me
''
,
there
is
a
supernatur
element
to
``
Heart
''
,
but
although
it
is
kei
to
the
plot
,
it
is
not
promin
.

Like
``
Stand
By
Me
''
,
thi
is
a
mostli
character-driven
film
,
and
as
such
it
benefit
greatli
from
superb
cast
.

Yelchin
is
veri
good
as
Bobbi
,
find
a
good
mix
of
innoc
and
resign
.

More
splendid
still
is
Boorem
,
whom
I
prais
highli
for
her
work
in
``
Along
Came
A
Spider
''
and
who
is
simpli
radiant
here
as
Carol
.

And
then
there
is
Hopkin
,
who
despit
plai
such
a
quiet
,
introspect
charact
as
Ted
,
nonetheless
command
our
attent
everi
time
he
is
onscreen
.

Less
success
is
Davi
,
whose
strident
Elizabeth
come
across
as
overli
cartoonish
.

I
also
found
it
odd
that
Bobbi
and
Carol
's
other
friend
,
Sulli
-LRB-
whose
death
as
an
adult
set
up
the
movi
's
flashback
frame
devic
-RRB-
,
is
paid
virtual
no
attent
.

But
Hick
'
direct
is
love
without
be
cloi
,
and
despit
the
movi
's
gener
lack
of
incid
,
it
never
ceas
to
weav
it
spell
over
the
audienc
.

