Almost
a
full
decad
befor
Steven
Spielberg
's
SAVING
PRIVATE
RYAN
ask
whether
a
film
could
be
both
``
anti
war
''
and
``
pro-soldi
''
,
John
Irvin
's
HAMBURGER
HILL
prove
it
could
.

Lost
in
the
inund
of
critic
acclaim
that
greet
Oliver
Stone
's
PLATOON
,
thi
excel
film
wa
dismiss
as
``
too
militarist
''
.

It
's
hard
to
understand
exactli
why
--
unless
Irvin
,
in
assembl
hi
motlei
collect
of
young
men
who
for
predict
-LRB-
and
often
naiv
-RRB-
reason
``
chose
to
show
up
''
for
the
Vietnam
debacl
,
--
ha
refus
to
present
us
with
the
stone
killer
,
drug-stok
psycho
and
ruthless
opportunist
who
have
becom
to
Vietnam
war
epic
what
``
the
Polack
,
the
Hillbilli
and
the
Kid
from
Brooklyn
''
becam
to
WWII
movi
.

HAMBURGER
HILL
,
base
on
a
true
stori
,
is
not
an
easi
film
to
watch
.

There
is
a
scene
that
will
have
grai
anti-war
activist
squirm
in
their
seat
,
or
move
to
genuin
tear
.

And
the
climact
final
assault
on
the
``
Hill
''
in
question
is
visual
confus
.

Gristli
realiti
ar
present
in
brief
flash
,
as
if
the
brain
dare
not
acknowledg
what
it
had
encount
.

And
in
the
mud
and
smoke
offic
and
enliste
,
veteran
and
``
newbi
''
,
black
soldier
and
white
,
becom
almost
indistinguish
from
each
other
,
as
thei
do
in
the
chao
of
actual
combat
.

The
act
throughout
is
solid
with
an
absolut
stellar
perform
render
by
Courtnei
B.
Vanc
as
Doc
--
in
a
role
that
will
have
mani
flatli
disbeliev
that
thi
is
same
actor
thei
cheer
as
``
Seaman
Jone
''
in
McTiernan
's
RED
OCTOBER
.

If
you
've
seen
PRIVATE
RYAN
,
you
ow
it
to
yourself
to
see
HAMBURGER
HILL
--
if
onli
to
determin
that
the
all
the
valour
and
horror
of
Spielberg
's
vision
wa
as
present
in
the
Ashau
Vallei
as
it
wa
at
Omaha
Beach
.

