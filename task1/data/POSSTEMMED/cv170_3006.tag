I
do
n't
know
what
movi
the
critic
saw
,
but
it
wa
n't
thi
on
.

The
popular
consensu
among
newspap
critic
wa
that
thi
movi
is
unfunni
and
dreadfulli
bore
.

In
my
person
opinion
,
thei
could
n't
be
more
wrong
.

If
you
were
expect
AIRPLANE
!

-
like
laugh
and
Agatha
Christie-intens
mysteri
,
then
ye
,
thi
movi
would
be
a
disappoint
.

Howev
,
If
you
're
just
look
for
an
enjoy
movi
and
a
good
time
,
thi
is
on
to
see
.

Honest
,
it
is
.

Thi
stori
is
about
a
4th
network
,
WBN
,
take
to
America
's
airwav
in
1939
.

Penni
Henderson
-LRB-
Mari
Stuart
Masterson
-RRB-
,
the
station
owner
's
secretari
,
must
deal
with
her
overbear
boss
,
an
unimpress
sponsor
and
writer
readi
to
quit
on
a
moment
's
notic
,
partli
due
to
the
fact
that
thei
have
n't
been
paid
in
week
.

Among
the
mayhem
,
she
must
also
deal
with
her
soon-to-b
ex-husband
,
Roger
-LRB-
Brian
Benben
-RRB-
who
desper
want
her
back
.

Unfortun
,
her
problem
onli
get
wors
as
the
night
goe
on
and
the
bodi
count
rise
without
explan
.

While
try
to
woo
Penni
back
,
Roger
must
deal
with
the
polic
and
try
to
find
the
killer
that
lurk
at
WBN
.

Mari
Stuart
Masterson
doe
well
in
her
role
as
Penni
-LRB-
I
'm
try
to
be
unbias
so
I
wo
n't
tell
you
how
amaz
she
look
-RRB-
,
although
Brian
Benben
get
most
of
the
screen
time
.

Along
with
the
two
lead
,
ar
sever
recogniz
face
,
includ
Corbin
Bernsen
-LRB-
L.A.
Law
-RRB-
,
Michael
McKean
-LRB-
Lavern
&
Shirlei
-RRB-
and
Bobcat
Goldthwait
.

The
special
effect
ar
amaz
,
and
fool
me
-LRB-
which
some
mai
argu
is
n't
veri
difficult
to
do
-RRB-
in
mani
scene
.

Although
the
movi
sometim
goe
overboard
with
the
physic
comedi
,
it
more
than
make
up
for
those
mistak
throughout
the
movi
.

