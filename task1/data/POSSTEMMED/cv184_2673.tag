Star
Julia
Robert
and
Nick
Nolt
.

I
tri
hard
not
like
thi
movi
without
succeed
.

It
contain
part
of
BRINGING
UP
BABY
,
ani
Hepburn/Traci
film
,
part
of
the
plot
of
WAR
GAMES
,
Cari
Grant
and
Rosalind
Russel
in
HIS
GIRL
FRIDAY
,
elev
shaft
action
,
train
action
,
and
murder
.

It
is
cute
,
funni
,
suspens
-LRB-
a
littl
bit
-RRB-
,
sexi
-LRB-
a
littl
bit
-RRB-
.

One
of
the
bad
gui
wa
a
surpris
.

The
pace
is
fast
enough
,
there
ar
some
``
action
''
scene
.

I
think
that
thi
is
a
reason
famili
summer
movi
for
famili
with
older
kid
who
have
alreadi
seen
MAVERICK
.

Julia
Robert
and
Nick
Nolt
ar
fine
separ
and
togeth
.

I
realli
identifi
with
the
Julia
Robert
charact
-LRB-
wish
think
,
I
know
!
-RRB-

,
but
I
do
have
better
hair
!

Her
hair
,
a
veri
dull
dark
brown
,
ha
no
color
.

She
could
us
some
highlight
.

Thei
both
do
fine
with
their
role
:
he
's
a
bore
columnist
with
the
Chicago
Chronicl
and
she
's
a
hotshot
``
cub
''
report
with
the
Chicago
Globe
.

Thei
have
great
fun
try
to
out
scoop
each
other
to
solv
on
case
throughout
thi
movi
.

I
like
the
on
stori
focu
.

Other
actor
includ
Charl
Martin
Smith
as
Robert
'
boss
,
Olympia
Dukaki
as
Nolt
's
cowork
,
Nora
Dunn
also
with
the
Chronicl
,
and
Marsha
Mason
as
a
congresswoman
.

Basic
thi
movi
is
reason
fun
.

There
is
a
seriou
mysteri
to
solv
,
and
I
enjoi
follow
a
fun
coupl
solv
the
mysteri
.

It
is
a
pleasant
chang
of
pace
from
movi
that
take
themselv
serious
.

I
do
give
success
comedi
high
mark
.

