As
I
write
the
review
for
the
new
Hanks/Ryan
romant
comedi
YOU
'VE
GOT
MAIL
,
I
am
acut
awar
that
I
am
type
it
on
a
comput
and
send
it
a
billion
mile
awai
on
the
Internet
.

I
am
also
awar
that
I
have
just
spent
the
last
2
hour
watch
the
world
's
biggest
paid
commerci
for
America
Online
.

And
I
wonder
:
is
that
so
bad
?

Well
,
the
commerci
part
is
.

As
for
the
movi
,
well
,
as
long
as
I
can
watch
Tom
Hank
and
Meg
Ryan
,
I
think
I
'll
be
okai
.

To
paraphras
Jame
Berardinelli
,
whose
review
I
admir
veri
much
,
Tom
Hank
and
Meg
Ryan
can
act
.

Thei
ar
both
wonder
,
but
for
all
of
Hank
'
gloriou
work
in
seriou
film
,
such
as
hi
magnific
perform
in
SAVING
PRIVATE
RYAN
,
and
hi
gloriou
triumph
in
PHILADELPHIA
,
I
like
him
best
when
he
's
suitabl
obnoxi
.

Tom
Hank
is
wonder
when
he
is
obnoxi
in
a
romant
comedi
when
he
's
go
to
get
the
girl
:
the
onli
question
is
how
?

Meg
Ryan
,
America
's
high
school
sweetheart
,
can
act
.

She
's
had
a
hand
of
memor
perform
,
includ
the
great
scene
in
the
diner
in
WHEN
HARRY
MET
SALLY
-LRB-
I
must
not
mention
what
she
doe
,
due
to
the
,
well
,
um
,
you
get
the
idea
,
and
if
you
do
n't
,
get
the
movi
--
fantast
-RRB-
.

But
,
I
like
her
best
when
she
's
,
well
,
perki
.

She
is
so
ador
perky/cut
that
togeth
with
Hank
,
thei
ar
the
most
amaz
onscreen
duo
in
my
short
lifetim
.

Some
peopl
have
them
go
back
to
realli
old
classic
romant
coupl
,
but
I
do
n't
realli
know
.

All
I
know
is
that
I
just
like
them
both
.

Thei
have
such
wonderfulli
kinet
chemistri
that
's
realli
hard
to
resist
,
and
I
'm
not
go
to
even
try
.

Thei
're
just
realli
,
realli
cute
,
sweet
,
and
charm
.

Hank
and
Ryan
collabor
onc
again
with
Nora
Ephron
,
who
direct
them
in
the
megahit
SLEEPLESS
IN
SEATTLE
.

Here
,
the
setup
is
a
littl
differ
.

Kathleen
Kelli
-LRB-
Ryan
-RRB-
own
a
small
children
's
bookstor
which
her
mother
found
42
year
ago
and
pass
down
to
her
.

She
is
success
,
beauti
,
and
date
a
well-known
columnist
with
veri
radic
idea
-LRB-
Kinnear
-RRB-
.

She
is
also
have
an
affair
,
of
sort
.

You
see
,
Kathleen
,
with
the
handl
SHOPGIRL
,
is
secretli
email
NY152
.

Thei
have
a
strictli
un-detail
relationship
:
there
ar
no
specif
,
and
thei
have
no
idea
who
each
other
is
.

NY152
happen
to
be
Joe
Fox
-LRB-
Hank
-RRB-
,
a
multimillionair
booksel
who
is
the
heir
to
the
fortun
of
the
Fox
chain
of
mega-bookstor
,
run
by
hi
father
-LRB-
Coleman
-RRB-
.

Fox
Book
ha
decid
to
open
a
store
on
the
West
Side
,
right
across
from
a
littl
children
's
booksel
name
``
Shop
Around
the
Corner
''
,
run
by
a
perki
girl
name
Kathleen
Kelli
.

Everi
morn
,
these
two
email
each
other
silli
,
and
everi
dai
,
thei
fight
``
to
the
death
!!
''
.

It
's
a
charm
premis
,
and
on
that
work
nice
,
balanc
the
immens
troubl
Kathleen
is
in
with
the
romanc
she
want
to
have
.

It
also
ha
the
great
sens
of
be
a
classic
romanc
:
there
is
no
``
let
's
have
sex
tonight
''
mantra
,
if
you
'll
excus
my
blunt
.

Thi
is
a
solid
romanc
built
around
that
vagu
concept
of
love
.

Hmm
.

Well
,
the
movi
goe
through
twist
and
turn
,
have
some
sad
moment
and
happi
on
,
until
at
on
moment
Tom
Hank
and
Meg
Ryan
``
hook
up
''
,
to
us
modern
terminolog
,
and
kiss
their
wai
into
happi
.

You
know
it
's
go
to
happen
,
the
question
is
now
?

Thankfulli
,
Ephron
ha
a
wonder
support
cast
with
Posei
,
Kinnear
,
Chappel
,
and
a
host
of
other
,
includ
a
great
scene
involv
brother
,
aunt
,
and
two
sweet
kid
.

It
's
all
cute
and
wonder
,
and
for
all
of
the
ideal
,
it
make
me
feel
good
.

Hank
is
as
obnoxi
as
ever
on
the
outsid
and
as
warm
on
the
insid
as
he
alwai
is
.

He
is
,
I
believ
,
America
's
greatest
actor
.

Doe
he
show
it
here
?

Nah
.

But
he
's
still
Tom
Hank
,
and
she
's
still
Meg
Ryan
.

Which
is
what
thi
movi
boil
down
to
.

It
's
pure
confect
.

All
the
bit
about
THE
GODFATHER
,
the
joke
,
the
superb
script
,
it
's
all
realli
sweet
.

There
ar
some
immens
flaw
,
like
pace
.

The
last
third
of
the
movi
realli
doe
n't
work
as
well
as
I
would
have
like
,
and
for
a
romant
comedi
,
it
's
slow
.

Once
Hank
know
the
secret
,
it
get
realli
slow
.

I
even
look
at
my
watch
a
coupl
time
,
to
make
sure
Ephron
wa
go
to
deliv
the
big
onscreen
kiss
I
wa
wait
for
,
so
that
I
could
get
home
at
a
reason
hour
.

The
other
problem
is
that
thi
movi
is
shape
up
to
be
realli
date
.

E-mail
?

When
my
kid
ar
my
ag
,
I
do
n't
know
what
we
'll
be
us
.

But
Hank
can
do
more
with
on
eyerol
than
anybodi
,
and
Meg
Ryan
just
love
to
dive
into
a
pillow
better
than
anyon
in
movi
histori
.

Thei
just
work
togeth
,
and
it
's
nice
to
see
.

It
give
you
,
well
,
a
smile
.

The
script
help
with
some
great
line
that
ar
absolut
hilari
,
and
that
alwai
come
at
the
exact
right
time
to
keep
the
audienc
awak
.

I
wa
argu
with
my
father
on
the
drive
back
home
on
what
the
purpos
of
movi
wa
.

I
've
alwai
believ
that
movi
ar
veri
power
,
veri
power
inde
.

You
see
,
mankind
ha
three
abil
he
need
to
surviv
:
the
abil
to
think
,
entertain
,
and
procreat
.

Movi
can
definit
do
the
first
two
,
and
as
for
the
third
,
well
,
I
'd
rather
not
think
about
that
.

I
alwai
thought
film
that
made
you
think
enlighten
you
and
made
you
see
someth
from
anoth
viewpoint
,
and
that
film
that
entertain
you
were
good
,
becaus
you
forgot
about
your
troubl
and
thought
about
someth
nice
for
a
night
.

And
I
wa
think
about
how
incred
a
year
Tom
Hank
ha
had
.

Hank
wa
the
drive
forc
behind
on
of
the
year
's
best
film
in
SAVING
PRIVATE
RYAN
,
which
is
on
of
those
think
film
that
truli
made
someon
wonder
about
the
world
.

He
's
also
half
of
the
glue
behind
the
year
's
best
exampl
of
pure
entertain
.

Tom
Hank
and
Meg
Ryan
ar
magic
,
as
is
thi
movi
.

It
's
a
sweet
,
love
affair
with
a
technolog
twist
.

It
mai
be
a
long
commerci
,
but
it
gave
me
a
pretti
big
smile
on
my
face
.

