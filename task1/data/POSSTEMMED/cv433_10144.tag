Quentin
Tarantino
seem
to
have
a
knack
for
give
hi
star
big
career
.

After
hi
PULP
FICTION
,
mani
actor
began
receiv
mani
offer
for
job
.

John
Travolta
made
hi
come-back
and
is
now
on
of
todai
's
most
bankabl
star
.

Bruce
Willi
prove
hi
act
chop
and
is
now
consid
an
actual
actor
.

JACKIE
BROWN
,
on
the
other
hand
,
boast
quit
a
well
known
cast
,
except
for
the
main
lead
:
Pam
Grier
.

Most
like
you
have
heard
of
her
and
seen
her
in
film
,
but
she
's
never
realli
had
a
breakthrough
perform
-LRB-
her
first
film
wa
in
the
Roger
Ebert-written
BEYOND
THE
VALLEY
OF
THE
DOLLS
-RRB-
.

That
is
,
until
now
.

JACKIE
BROWN
is
a
highli
anticip
featur
film
from
Quentin
Tarantino
,
whose
last
film
wa
the
huge
hit
PULP
FICTION
.

After
mediocr
act
job
,
Tarantino
return
to
what
made
hi
name
a
household
on
:
direct
and
write
.

JACKIE
BROWN
is
the
result
,
and
it
is
fantast
entertain
.

Despit
excel
direct
from
Tarantino
,
the
cast
steal
the
film
,
give
on
good
perform
after
anoth
.

Pam
Grier
give
a
stun
perform
as
Jacki
Brown
,
and
she
is
support
by
an
incred
mix
of
talent
--
Samuel
L.
Jackson
,
Bridget
Fonda
,
Michael
Keaton
,
Robert
De
Niro
,
and
Robert
Forster
.

More
than
like
,
you
will
see
a
few
of
these
name
in
the
Oscar
pool
come
Oscar
nomin
.

Describ
a
Quentin
Tarantino
film
is
veri
difficult
,
as
you
realli
ca
n't
sai
much
or
you
spoil
it
for
everyon
els
.

I
will
tread
lightli
.

JACKIE
BROWN
open
with
an
impress
shot
of
Jacki
Brown
-LRB-
Grier
-RRB-
in
a
blue
outfit
walk
through
the
airport
.

The
camera
track
along
with
her
,
and
end
up
reveal
her
occup
.

We
ar
also
introduc
to
Ordell
Robbi
-LRB-
Jackson
-RRB-
,
who
sell
illeg
gun
to
interest
buyer
...
for
a
hefti
price
.

Ms.
Brown
carri
the
monei
from
buyer
to
Ordell
in
order
to
keep
the
cop
awai
.

Howev
,
an
FBI
agent
-LRB-
Keaton
-RRB-
and
a
local
L.A.
cop
-LRB-
Michael
Bowen
-RRB-
ar
out
to
find
Ordell
and
catch
him
while
take
the
monei
.

Thei
try
to
reach
him
through
Beaumont
Livingston
-LRB-
Chri
Tucker
-RRB-
,
on
of
Ordell
's
buyer
,
but
he
mysteri
end
up
dead
.

Thei
try
again
through
Brown
,
but
she
is
hesit
to
admit
to
ani
wrong-do
.

But
after
unknowingli
carri
some
drug
for
on
of
Ordell
's
friend
-LRB-
Fonda
-RRB-
,
she
is
caught
and
sent
to
jail
.

Ordell
pai
her
bail
through
a
bail
bond
agent
,
Max
Cherri
-LRB-
Forster
-RRB-
,
and
then
want
to
find
out
what
she
told
the
cop
.

After
she
claim
to
have
said
noth
,
she
reveal
the
fact
that
she
will
tell
them
about
Ordell
in
order
to
stai
out
of
prison
.

Ordell
doe
n't
like
thi
,
but
thei
both
come
up
with
a
scheme
to
throw
the
Fed
and
cop
off
their
track
.

Thi
scheme
seem
to
be
go
as
plan
,
but
unexpect
occur
mai
or
mai
not
foul
up
their
goal
.

Reveal
more
would
spoil
the
fun
of
JACKIE
BROWN
,
and
that
's
the
biggest
reason
to
go
see
a
Tarantino
film
.

Tarantino
seem
to
have
a
fascin
with
hitmen
and
unusu
predica
-LRB-
and
slang
term
-RRB-
,
but
thei
ar
alwai
done
veri
well
and
believ
.

Hi
hitmen
ar
normal
cruel
,
but
pleasant
,
in
that
nasti
sort
of
wai
.

Thei
talk
like
normal
peopl
,
except
for
when
do
busi
,
and
thei
ar
rude
to
their
girlfriend
.

Of
cours
,
thi
is
a
Tarantino
film
,
and
you
know
you
can
expect
some
awkward
situat
to
aris
.

And
perhap
the
biggest
flaw
with
JACKIE
BROWN
is
the
slow
middl
section
in
which
the
pace
begin
to
drop
.

Of
cours
,
Tarantino
fix
thi
by
move
onto
the
climax
of
the
film
,
ad
a
lot
of
hi
RESERVOIR
DOGS-style
storytel
to
it
.

Thing
ar
told
from
on
person
's
perspect
,
and
then
from
anoth
,
and
then
from
anoth
,
each
time
learn
more
and
more
.

Thi
all
lead
up
to
a
veri
interest
conclus
,
which
decid
who
is
on
whose
side
.

On
the
technic
side
of
JACKIE
BROWN
,
Quentin
Tarantino
direct
it
with
the
same
style
as
he
did
with
hi
1994
film
.

Howev
,
thi
time
around
it
is
n't
as
nearli
as
impress
or
origin
.

The
write
,
on
the
other
hand
,
is
quit
good
,
although
I
do
find
Tarantino
's
us
of
slang
term
offens
.

The
stori
jump
back
and
forth
in
time
as
in
PULP
FICTION
,
but
it
's
much
more
easi
to
understand
.

Hi
previou
film
need
a
second
view
in
order
to
understand
the
time
differenti
,
but
JACKIE
BROWN
is
veri
simpli
to
follow
,
especi
consid
that
the
time
is
given
dure
the
``
leap
''
in
time
.

Perhap
thi
is
a
result
of
the
screenplai
be
written
from
the
novel
``
Rum
Punch
''
by
Elmore
Leonard
.

The
dialogu
is
pretti
much
intellig
,
and
all
the
charact
ar
fulli
realiz
.

The
cinematographi
is
veri
well
done
by
Guillermo
Navarro
-LRB-
who
ha
work
with
Tarantino
on
previou
film
-RRB-
and
the
edit
is
veri
good
.

And
as
with
PULP
FICTION
,
the
music
is
a
highlight
,
bring
back
memor
song
from
the
70
and
80
.

The
act
side
of
JACKIE
BROWN
is
unforgett
.

The
perform
ar
incred
rich
,
with
hidden
mean
behind
everi
charact
'
action
.

Noth
is
quit
as
it
seem
,
and
you
ca
n't
realli
tell
which
side
on
person
is
on
.

The
real
treat
,
of
cours
,
is
Pam
Grier
who
give
an
astonish
perform
as
Jacki
Brown
.

Her
poor
live
condit
ar
superced
by
her
superior
wit
,
which
plai
an
import
role
in
the
film
.

Watch
Grieg
,
I
could
tell
what
wa
go
through
her
mind
,
even
if
I
did
n't
know
what
she
wa
think
.

Her
face
portrai
a
lot
of
emot
that
you
can
tell
when
she
is
sad
,
happi
,
or
in
deep
thought
.

Bridget
Fonda
give
a
veri
good
perform
as
a
druggi
couch
potato
.

I
did
n't
even
realiz
it
wa
Fonda
until
I
saw
her
name
in
the
final
credit
-LRB-
her
perform
actual
remind
me
of
Heather
Graham
's
in
BOOGIE
NIGHTS
-RRB-
.

Michael
Keaton
come
across
veri
well
,
as
doe
Michael
Bowen
.

Chri
Tucker
ha
a
small
,
but
effect
perform
.

Samuel
L.
Jackson
is
veri
good
,
but
it
seem
to
me
that
he
wa
replai
hi
Jule
Winnfield
charact
from
PULP
FICTION
-LRB-
although
much
less
cynic
-RRB-
.

But
no
matter
...
Jackson
is
veri
strong
in
hi
role
.

Robert
De
Niro
is
actual
a
littl
annoi
at
time
,
but
overal
he
doe
a
good
job
as
on
of
Ordell
's
perspect
client
and
friend
.

Robert
Forster
give
on
of
the
best
perform
in
the
film
,
as
he
ha
on
of
the
most
develop
charact
.

He
is
present
throughout
most
of
the
film
,
and
he
hold
hi
own
against
Jackson
and
Grier
.

JACKIE
BROWN
is
rate
R
for
languag
,
sex
,
violenc
,
drug
us
,
and
some
offens
remark
.

As
a
film
by
itself
,
JACKIE
BROWN
is
a
terrif
piec
of
entertain
,
with
a
complex
plot
to
draw
viewer
in
.

Howev
,
on
can
not
help
but
compar
it
to
Tarantino
's
PULP
FICTION
,
and
expect
great
result
.

When
compar
,
it
come
up
short
,
but
not
that
short
.

It
's
a
worthi
effort
,
with
terrif
act
and
some
impress
write
from
Elmore
Leonard
and
Tarantino
.

Perhap
the
best
thing
about
JACKIE
BROWN
is
the
discoveri
of
Pam
Grier
as
a
major
Hollywood
actress
.

I
ca
n't
rememb
see
her
in
anyth
-LRB-
except
for
a
small
role
in
MARS
ATTACKS
!
-RRB-

,
but
hopefulli
she
will
get
mani
more
role
from
thi
on
.

Expect
to
see
her
name
float
around
the
Oscar
nomin
-LRB-
and
hopefulli
she
will
even
be
abl
to
nab
on
-RRB-
.

