Have
you
ever
wonder
if
death
is
someth
that
simpli
happen
natur
,
at
ani
given
moment
,
in
life
?

Or
if
it
is
an
occurr
that
is
predetermin
by
some
much
larger
forc
within
the
world
,
or
outsid
of
it
?

These
thought-provok
question
ar
the
subject
of
``
Final
Destin
,
''
which
is
the
most
rivet
horror-drama
sinc
1999
's
``
The
Sixth
Sens
.
''

Stylishli
film
and
tautli
direct
by
``
X-File
''
alum
Jame
Wong
,
the
film
deal
with
sever
fairli
heavi
topic
that
ar
discuss
truthfulli
by
teenag
charact
-LRB-
in
accur
written
``
teen-speak
,
''
rather
than
overwrought
,
highli
intellectu
dialogu
that
even
Harvard
scholar
would
n't
us
-RRB-
,
all
the
while
develop
into
a
darkli
forebod
,
sever
eeri
thriller
that
successfulli
leav
you
uneasi
from
the
horrifyingli
real
open
twenti
minut
to
the
less-success
,
but
still
suspens
epilogu
.

At
the
start
of
the
film
,
17-year-old
high
school
senior
Alex
Brown
-LRB-
Devon
Sawa
-RRB-
is
about
to
leav
with
hi
40-student
French
class
for
a
field
trip
to
Pari
.

Progress
get
a
case
of
preflight
jitter
,
he
spot
sever
mysteri
``
coincid
,
''
includ
the
departur
time
be
9:25
,
the
same
as
hi
Septemb
25
birthdai
,
as
well
as
hear
the
John
Denver
song
``
Rocki
Mountain
High
''
over
the
sound
system
in
the
airport
bathroom
.

``
John
Denver
di
in
a
plane
crash
,
''
Alex
grudgingli
mutter
to
himself
.

Once
seat
,
Alex
suddenli
get
a
horrif
premonit
about
the
plane
explod
upon
takeoff
,
and
after
caus
a
rucku
onboard
,
he
,
as
well
as
six
other
student
and
a
teacher
,
ar
thrown
off
.

Argu
in
the
airport
lobbi
,
Alex
's
suspicion
come
true
when
the
plane
realli
doe
explod
in
flight
,
kill
all
300
+
passeng
,
includ
the
rest
of
their
classmat
.

As
two
investig
interrog
Alex
about
how
he
knew
the
plane
wa
go
to
explod
,
the
fellow
survivor
,
whom
he
ha
save
,
treat
him
in
vari
manner
.

Carter
-LRB-
Kerr
Smith
-RRB-
,
the
hotshot
jock
,
hold
him
in
contempt
becaus
he
believ
that
it
is
onli
he
himself
who
can
decid
hi
fate
.

Alex
's
best
friend
,
Tod
-LRB-
Chad
E.
Donella
-RRB-
,
want
to
stand
by
him
but
ca
n't
becaus
of
hi
fear
parent
.

The
teacher
,
Mrs.
Lewton
-LRB-
Kristen
Cloke
-RRB-
,
is
frighten
by
him
,
believ
that
it
wa
he
who
somehow
caus
the
disast
,
as
well
as
in
a
state
of
guilt
for
give
up
her
seat
on
the
plane
to
a
fellow
cowork
.

Only
outsid
Clear
River
-LRB-
Ali
Larter
-RRB-
,
who
unboard
the
plane
due
to
a
sudden
connect
to
Alex
's
premonit
even
though
she
had
never
even
spoken
to
him
befor
,
is
sympathet
and
thank
to
him
for
save
her
life
.

That
is
onli
the
setup
of
``
Final
Destin
,
''
and
the
less
said
about
what
follow
,
the
better
.

Suffic
to
sai
,
the
film
doe
,
inde
,
turn
into
a
slasher
film
,
albeit
on
that
is
more
thought
than
most
,
and
replac
a
knife-wield
maniac
for
the
never-seen
granddaddi
killer
of
them
all
,
the
Grim
Reaper
itself
.

The
setpiec
in
ani
horror
movi
ar
the
death
scene
,
and
``
Final
Destin
''
sure
boost
some
of
the
most
invent
on
to
grace
the
silver
screen
in
year
.

Graphic
and
brilliantli
orchestr
in
a
Rube
Goldbergesqu
manner
,
thei
manag
to
occasion
be
so
intens
you
do
n't
know
whether
to
shrink
down
in
your
seat
,
tap
your
feet
in
nervous
,
or
turn
awai
.

Devon
Sawa
,
a
rise
star
who
put
hi
physic
comedi
skill
to
good
us
in
1999
's
underseen
slasher-comedi
,
``
Idle
Hand
,
''
is
even
more
of
a
charismat
presenc
here
.

The
conflict
emot
he
feel
for
hi
surviv
,
which
he
come
to
believ
he
wa
n't
meant
to
do
,
as
well
as
the
loss
of
the
other
passeng
,
is
superbl
and
subtli
act
on
hi
part
.

One
scene
,
in
which
he
is
watch
a
new
report
on
the
crash
and
slowli
begin
to
break
down
is
especi
realist
and
power
.

Ali
Larter
-LRB-
1999
's
``
Varsiti
Blue
''
-RRB-
,
in
the
other
central
role
,
is
also
effect
,
as
a
girl
whose
life
wa
go
well
until
her
father
di
in
a
conveni
store
shoot
year
befor
,
leav
her
stuck
with
an
increasingli
uncar
mother
who
marri
a
loutish
man
--
the
exact
opposit
of
her
now-deceas
father
.

The
other
role
ar
not
as
fulli
written
,
and
most
remain
rather
one-dimension
.

The
movi
is
mainli
Sawa
's
,
howev
,
and
the
rest
of
the
actor
equip
themselv
well
in
limit
role
.

Toni
Todd
-LRB-
1992
's
``
Candyman
''
-RRB-
,
as
an
arcan
morgu
attend
,
pop
up
for
a
five-minut
cameo
,
but
hi
appear
is
rather
supererogatori
.

And
the
aforement
dialogu
occasion
hit
the
bullsey
,
while
at
other
time
it
hover
over
be
just
a
littl
too
stilt
and
campi
.

Ultimat
,
what
is
so
good
about
``
Final
Destin
''
is
that
,
within
the
confin
of
the
slasher
genr
,
writer-director
Wong
and
screenwrit
Glen
Morgan
and
Jeffrei
Reddick
have
creat
a
premis
that
ha
never
been
seen
befor
in
thi
manner
,
and
a
film
that
ha
the
abil
to
both
surpris
and
frighten
--
two
thing
that
ar
rare
found
in
todai
's
horror
film
.

If
anyth
,
it
's
safe
to
sai
you
will
never
ever
look
at
fly
in
airplan
the
same
wai
again
.

