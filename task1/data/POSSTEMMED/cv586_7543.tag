-LSB-
Note
:
After
claim
otherwis
,
my
appetit
wa
inde
whet
by
Kenneth
Branagh
's
HAMLET
to
search
out
other
attempt
to
translat
Shakespear
to
film
in
hope
of
find
a
better
mousetrap
.

As
it
happen
,
it
did
n't
take
long
.
-RSB-

Is
there
a
more
romant
director
than
Franco
Zeffirelli
?

Known
mainli
to
American
audienc
as
the
man
behind
1968
's
ROMEO
AND
JULIET
-LRB-
requir
view
for
mani
high
schooler
,
and
as
much
as
most
ever
learn
of
Shakespear
-RRB-
,
he
is
also
a
favorit
at
the
Metropolitan
Opera
,
where
hi
achingli
beauti
design
further
the
alreadi
lush
emot
of
mani
a
Puccini
aria
.

Last
year
he
gave
us
hi
beautifulli
realiz
version
of
JANE
EYRE
,
on
of
the
twin
peak
-LRB-
with
WUTHERING
HEIGHTS
-RRB-
of
romant
literatur
.

It
might
seem
peculiar
,
then
,
that
Zeffirelli
ever
had
an
interest
in
translat
HAMLET
for
the
screen
.

At
first
blush
,
HAMLET
is
far
remov
from
romanc
.

The
hero
,
though
a
princ
,
is
depress
,
conflict
and
confus
.

The
ostens
love
interest
get
littl
screen
time
,
and
her
scene
with
her
princ
ar
usual
as
an
object
of
mockeri
.

The
plot
is
full
of
polit
intrigu
and
the
player
ar
as
curs
as
the
Hous
of
Atreu
.

It
is
Zeffirelli
's
geniu
,
howev
,
that
he
is
abl
to
expos
the
romant
core
of
HAMLET
and
give
the
sometim
dry
tale
an
emot
embrac
.

In
thi
Italian
master
's
hand
,
romanc
is
everywher
--
in
a
son
's
worship
of
hi
father
,
in
the
bond
of
deepest
friendship
,
in
the
privat
thought
of
young
lover
,
even
in
the
mysteri
of
castl
by
the
sea
.

After
all
,
the
stori
hing
on
a
ghost
,
and
what
could
be
more
romant
than
that
?

What
seem
like
a
cheap
wai
to
sell
ticket
at
the
time
turn
out
to
be
on
of
the
most
romant
gestur
of
all
,
name
,
hire
then-reign
Hollywood
hunk
Mel
Gibson
to
plai
the
lead
.

Who
could
believ
that
the
man
who
made
hi
mark
plai
Mad
Max
and
a
``
lethal
weapon
''
-LRB-
in
the
seri
of
the
same
name
-RRB-
would
have
the
skill
to
succe
in
the
most
famou
role
in
the
English
languag
?

Franco
Zeffirelli
,
that
's
who
.

And
it
is
appar
in
everi
frame
that
hi
instinct
wa
right
on
the
monei
.

What
Gibson
bring
to
the
role
is
a
natur
and
eas
which
make
the
whole
stori
meaning
,
not
to
mention
comprehens
.

Hi
line
read
sound
spontan
rather
than
rehears
.

Hi
movement
and
gestur
-LRB-
except
hi
sometim
too
activ
ey
-RRB-
ar
total
in
keep
with
the
charact
.

Thi
is
a
Hamlet
whose
pain
we
feel
,
whose
struggl
we
empath
with
,
and
whose
death
we
mourn
as
sincer
as
Horatio
at
film
's
end
.

Glenn
Close
,
the
other
big
name
in
the
product
,
also
doe
well
by
the
script
and
satisfi
as
Hamlet
's
mother
,
Gertrud
--
though
I
must
confess
I
'm
still
try
to
understand
the
charact
as
written
.

-LRB-
Zeffirelli
's
incestu
interpret
of
Hamlet
's
relationship
with
hi
mother
doe
n't
help
.
-RRB-

Alan
Bate
make
a
believ
evil
Claudiu
without
resort
to
the
manner
of
a
Hollywood
heavi
.

By
and
larg
the
rest
of
the
cast
perform
admir
and
comfort
.

Of
special
note
ar
the
cinematographi
by
David
Watkin
and
the
score
by
Ennio
Morricon
.

Both
support
the
realist
mood
marvel
-LRB-
as
do
the
set
and
costum
-RRB-
,
without
ani
overst
effect
that
declar
``
thi
is
import
;
thi
is
Shakespear
!
''

I
particularli
appreci
that
the
word
ar
often
spoken
without
ani
music
at
all
:
the
melodi
of
a
wonderfulli
wrought
phrase
is
given
it
due
.

Perhap
it
is
difficult
to
call
edit
Shakespear
``
definit
.
''

And
there
ar
certainli
quit
a
few
other
film
version
of
HAMLET
that
I
have
not
seen
.

But
if
you
've
ever
felt
the
urg
to
overcom
your
fear
of
the
Bard
,
thi
HAMLET
is
an
excel
place
to
start
.

