Direct
by
David
Lean
;
Written
by
Michael
Wilson
,
Carl
Foreman
,
base
on
Pierr
Boull
's
novel
;
Cinematographi
by
Jack
Hildyard
;
Music
by
Malcolm
Arnold
;
Edite
by
Peter
Taylor
;
Produc
by
Sam
Spiegel
;
With
Alec
Guin
,
William
Holden
,
Jack
Hawkin
,
Sessu
Hayakawa
,
Jame
Donald
,
Geoffrei
Horn
I
want
to
correct
what
I
wrote
in
a
former
retrospect
of
David
Lean
's
war
pictur
.

I
still
think
that
it
doe
n't
deserv
be
the
number
in
the
American
Film
Institut
's
list
of
the
100
greatest
American
movi
.

And
I
think
that
Lumet
's
``
12
Angry
Men
''
,
Wilder
's
``
Wit
for
the
Prosecut
''
and
Kubrick
's
``
Path
of
Glori
''
would
have
been
better
choic
for
the
Best
Pictur
Oscar
in
1958
.

But
I
ca
n't
deni
the
import
of
``
The
Bridg
on
the
River
Kwai
''
-
cinemat
and
in
it
content
.

The
film
is
set
in
Burma
in
1943
.

A
bataillon
of
British
soldier
in
Japanes
war
captiv
is
forc
by
the
Japanes
to
build
a
strateg
moment
railwai
bridg
over
the
River
Kwai
.

But
the
British
command
offic
,
Colonel
Nicholson
-LRB-
Alec
Guin
-RRB-
,
insists-correspond
to
the
Geneva
Conventions-that
hi
offic
work
as
simpl
workmen
.

Struggl
toughli
,
Col.
Nicholson
forc
the
Japanes
command
,
Col.
Saito
-LRB-
Sessu
Hayakawa
-RRB-
,
to
give
wai
in
thi
respect
.

Afterward
Col.
Nicholson
assidu
commit
himself
for
the
build
of
the
bridg
.

He
consid
it
an
opportun
to
rais
hi
men
's
moral
,
and
he
want
to
prove
superior
British
capabl
to
the
Japanes
.

But
the
British
High
Command
send
a
few
soldier
who
shall
destroi
the
bridg
,
among
them
the
American
Shear
-LRB-
William
Holden
-RRB-
-
an
escape
from
the
Japanes
prison
camp-and
the
British
Major
Warden
-LRB-
Jack
Hawkin
-RRB-
...
A
flaw
of
the
pictur
is
the
clichéd
character
of
the
Japanes
peopl
.

Thei
ar
present
as
if
thei
were
intellectu
inferior
to
the
British-a
if
the
Japanes
were
incap
of
build
a
bridg
.

And
the
film
doe
n't
consist
question
the
militari
spirit
as
Kubrick
doe
in
``
Path
of
Glori
''
.

Lean
seem
rather
fascin
by
the
militari
hierarchi
.

Thi
is
also
percept
in
the
convers
between
Col.
Nicholson
and
Col.
Saito
.

In
thi
regard
it
is
symptomat
that
Shear
,
who
doubt
the
militari
logic
,
is
a
somehow
unpleas
person
in
the
film
.

The
audienc
is
suppos
to
applaud
Col.
Nicholson
's
persever
concern
the
question
if
hi
offic
shall
work
on
the
bridg
or
not
.

The
spectat
ar
suppos
to
neglect
the
risk
Col.
Nicholson
take
for
hi
men
.

-LRB-
The
plot
by-pass
these
risk
.
-RRB-

That
mean
,
the
pictur
is
n't
perfect
.

But
it
ha
a
lot
of
virtu
as
well
.

It
show
the
``
mad
''
of
war
and
what
war
can
produc
in
peopl
's
mind
.

It
show
how
Col.
Nicholson
becom
possess
by
the
idea
of
be
a
hero
and
that
other
-LRB-
like
Shear
-RRB-
get
cynic
.

And
``
The
Bridg
on
the
River
Kwai
''
is
an
interest
studi
of
charact
with
clash
interest
.

These
point
and
the
sometim
iron
dialogu
make
thi
film
an
anti-war
film
-LRB-
despit
the
inconsist
in
the
treatment
of
thi
theme
-RRB-
.

David
Lean
's
effect
and
atmospher
perfect
direct
creat
a
high
suspens
,
especi
in
the
dramat
-LRB-
though
not
wholli
plausibl
-RRB-
showdown
.

Alec
Guin
doe
a
magnific
job
of
bring
Col.
Nicholson
to
life
and
make
him
such
an
interest
charact
.

The
other
actor
deliv
veri
good
perform
as
well
.

Jack
Hildyard
's
fine
color
cinematographi
and
the
apt
score
ar
also
help
.

I
like
thi
extraordinari
film
despit
it
weak
.

-LRB-
C
-RRB-
Karl
Rackwitz
-LRB-
Klein
Köri
,
Germani
,
1999
-RRB-

