Do
you
want
to
know
the
truth
about
cat
and
dog
?

Do
you
?

Well
,
as
thi
movi
show
us
,
thei
make
good
companion
but
pale
in
comparison
to
the
joi
and
fulfil
romant
relationship
can
bring
.

Unfortun
,
there
's
a
giant
roadblock
set
up
in
our
cultur
that
onli
certain
peopl
can
get
through
while
the
rest
of
us
mill
around
outsid
,
feel
sorri
for
ourselv
.

A
Checkpoint
Beauti
kind
of
thing
,
and
Janean
Garafolo
's
charact
Abby
relat
to
it
well
.

Like
a
certain
overs
teenag
movi
critic
,
she
is
intellig
and
ha
a
great
sens
of
humor
but
strike
out
with
the
opposit
sex
becaus
she
see
herself
as
physic
unattract
,
and
the
men
seem
to
be
back
her
up
on
that
.

Meanwhil
,
her
gorgeou
neighbor
Nora
-LRB-
Uma
Thurman
...
Uma
be
the
onli
woman
's
name
wors
than
Nora
-RRB-
attract
men
like
fli
.

As
Abby
tell
her
,
``
You
burp
and
thei
think
it
's
cute
.
''

You
barf
and
thei
line
up
to
hold
your
hair
back
.
''
''

But
Nora
's
down
on
herself
for
be
too
shallow
and
superfici
.

We
all
have
our
prwhen
a
man
call
in
to
veterinarian
Abby
's
talk
radio
show
want
to
know
how
to
get
the
roller
skate
off
hi
basset
hound
.

-LRB-
Oh
come
on
,
ani
idiot
know
how
to
handl
that
problem
!
-RRB-

He
like
her
person
and
profession
demeanor
and
ask
to
meet
her
in
person
.

Abby
,
though
,
know
her
bodi
doe
n't
match
her
person
and
that
she
probabl
wo
n't
have
a
chanc
with
him
onc
he
see
her
.

So
she
give
him
Nora
's
bodi
descript
and
stand
him
up
.

Nora
happen
to
be
in
the
studio
the
next
dai
when
the
dog
man
come
by
and
Abby
ask
her
for
a
big
favor
.

She
goe
along
with
the
masquerad
and
the
three
of
them
head
out
for
a
night
of
fun
,
Abby
assum
the
role
of
a
goat
chees
farmer
name
Donna
.

The
dog
man
find
out
even
though
thi
girl
is
beauti
,
she
's
not
quit
as
intellectu
or
confid
about
herself
in
person
,
but
he
doe
n't
suspect
anyth
,
realiz
he
's
not
live
in
a
``
Three
's
Compani
''
episod
.

The
charad
continu
,
with
Abby
further
the
relationship
over
the
phone
-LRB-
includ
a
decidedli
unnecessari
phone
sex/masturb
sequenc
-RRB-
and
Nora
further
it
in
person
,
while
eventu
find
herself
attract
to
the
dog
man
.

Like
SLEEPLESS
IN
SEATTLE
,
thi
comedy/rom
is
predict
all
the
wai
through
,
but
we
care
enough
about
the
charact
that
we
root
them
on
and
will
the
romanc
to
blossom
.

THE
TRUTH
ABOUT
CATS
AND
DOGS
is
n't
quit
as
entertain
as
I
wa
expect
it
to
be
,
but
it
handl
well
a
theme
I
could
identifi
with
.

The
thing
is
,
I
find
Janean
Garafolo
rather
attract
.

She
's
no
Uma
but
I
'm
sure
in
real
life
she
'd
have
no
problem
attract
men
.

I
,
on
the
other
hand
,
could
n't
attract
a
man
to
save
my
life
...
and
am
rather
proud
of
that
fact
.

