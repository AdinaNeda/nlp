A
movi
that
's
been
as
highli
built
up
as
the
Truman
Show
,
with
review
boast
,
``
The
film
of
the
decad
!
''

and
``
A
breakthrough
!
''

can
onli
be
lead
up
to
letdown
.

That
's
no
doubt
--
it
seem
ani
movi
with
critic
acclaim
make
you
think
you
're
go
in
for
the
ride
of
your
life
,
that
you
'll
end
up
chang
on
the
other
side
,
and
you
come
out
of
the
theater
go
,
``
Eh
...
*
that
's
*
what
all
the
fuss
wa
about
?
''

So
,
natur
,
The
Truman
Show
wa
build
up
to
what
wa
go
to
be
a
dissapoint
.

And
I
convinc
myself
,
as
I
nestl
into
my
uncomfort
movi
theater
chair
,
actual
to
TRY
not
to
enjoi
it
.

Let
me
tell
you
,
that
is
an
impossibilti
.

The
Truman
Show
is
truli
``
the
film
of
the
decad
''
and
``
a
breakthrough
''
and
MORE
.

And
you
will
come
out
chang
on
the
other
side
.

Such
a
movi
come
around
onli
onc
in
a
lifetim
,
where
you
find
yourself
feel
everyth
that
the
charact
is
feel
,
and
thi
,
friend
,
is
IT
.

Jim
Carrei
show
that
he
doe
n't
need
to
talk
out
of
hi
butt
to
entertain
us
;
that
he
can
be
as
dramat
as
ani
of
Hollywood
's
lead
men
.

The
film
ha
a
difficult
premis
to
tackl
,
try
to
set
us
up
thirti
year
into
the
``
Truman
Show
's
''
run
,
but
tackl
it
it
doe
,
and
perfectli
.

With
flashback
actual
be
flashback
on
the
televis
show
,
you
get
the
sens
that
you
ar
view
a
*
real
*
prime-tim
hit
.

And
the
wai
the
camera
constantli
take
the
form
it
would
in
the
``
real
''
Truman
Show
is
clever
and
well-don
.

Peopl
,
rejoic
!

Thi
is
the
FIRST
Oscar-worthi
film
of
not
onli
thi
year
,
but
of
the
last
five
year
.

Not
sinc
``
Schindler
's
List
''
ha
a
movi
captur
human
spirit
and
true
despair
so
well
.

Thi
is
a
classic
in
the
make
,
and
-LRB-
to
borrow
a
line
from
Esquir
's
review
-RRB-
it
star
Jim
Carrei
.

Noth
in
hi
career
to
date
and
noth
he
can
do
in
the
futur
will
ever
be
abl
to
top
hi
role
as
Truman
Burbank
;
mark
my
word
.

For
,
as
I
said
,
thi
is
the
movi
of
a
lifetim
and
the
role
is
of
the
same
calibr
.

Forget
special-effect
ladden
summer
fodder
.

Drop
your
romant
comedi
in
the
trash
.

Toss
your
thriller
and
teen
flick
on
the
pile
.

You
need
noth
more
than
``
The
Truman
Show
''
to
carri
you
through
the
summer
,
or
for
that
matter
,
the
year
.

Oh
!

And
in
case
I
do
n't
see
you
,
good
afternoon
,
good
even
,
and
goodnight
.

