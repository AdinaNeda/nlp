John
Carpent
direct
thi
stylish
and
gori
vampir
flick
to
which
ha
it
's
good
side
and
it
's
bad
.

It
's
held
up
by
some
good
perform
and
dazzl
special
effect
lead
up
to
an
end
that
's
pretti
satisfi
.

Jame
Wood
give
an
outstand
perform
that
almost
cover
up
the
movi
.

The
good
side
:
some
neat
scene
of
vampir
and
violenc
.

The
bad
:
No
scare
.

Ye
that
's
right
,
even
though
be
direct
by
on
of
horror
's
most
cherish
director
,
`
Vampir
'
is
the
least
bit
scari
,
if
not
at
all
.

Jame
Wood
star
as
Jack
a
vampir
hunter
out
to
stop
the
plan
of
the
`
master
'
vampir
Valek
plai
by
Thoma
Ian
Griffith
whom
want
vampir
to
walk
in
the
daylight
.

Daniel
Baldwin
plai
hi
sidekick
Montoya
,
a
wise-crack
gui
who
realli
is
n't
that
funni
.

In
the
open
scene
Jack
along
with
Montoya
and
the
team
of
vampir
hunter
,
find
an
abandon
hous
to
which
vampir
occupi
.

Of
cours
we
all
know
sunshin
kill
vampir
,
and
that
is
just
how
thei
kill
these
vampir
:
hook
them
to
rope
and
drag
them
outsid
let
them
burn
to
death
.

After
throw
a
parti
for
what
happen
,
the
vampir
that
did
not
die
come
back
to
trash
it
.

Thei
kill
almost
all
of
Jack
's
team
,
leav
onli
a
few
aliv
.

Sheryl
Lee
plai
a
prostitut
Katrina
who
wa
bitten
by
Valek
,
but
not
yet
turn
into
a
vampir
.

She
is
rescu
by
Jack
and
flee
with
them
on
their
search
for
Valek
.

Thei
have
to
fight
thei
're
wai
against
vampir
,
lead
up
to
a
final
that
is
well-don
with
great
special
effect
.

`
Vampir
'
defin
could
have
been
done
better
.

The
make-up
,
special
effect
and
stori
ar
first
rate
,
but
there
is
no
scare
or
terror
that
the
film
could
have
had
.

It
to
me
had
more
humor
than
horror
,
almost
be
compar
to
`
Fright
Night
'
with
the
mix
of
violenc
and
comedi
.

John
Carpent
doe
a
great
job
of
direct
thi
film
.

He
eas
through
some
tough
time
and
save
the
flaw
,
which
ar
notic
but
forgett
.

The
script
is
clever
and
funni
,
with
mani
line
that
make
us
laugh
out
loud
.

I
much
enjoi
`
Vampir
'
with
a
few
except
.

It
's
on
of
those
film
you
see
but
do
n't
take
to
heart
.

You
enjoi
the
time
you
spent
and
go
on
.

The
recent
releas
`
Psycho
'
is
the
same
wai
,
it
's
not
as
good
as
you
'd
hope
but
it
is
fun
and
entertain
.

`
Vampir
'
is
a
good
horror
flick
with
no
scare
but
plenti
of
other
content
to
pleas
the
viewer
.

