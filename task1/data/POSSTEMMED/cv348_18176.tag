What
's
wors
than
Y2K
?

How
about
a
fulli
digit
futur
where
NOTHING
is
real
?

That
's
what
comput
programm
Neo
-LRB-
Keanu
Reev
-RRB-
slowli
learn
as
he
enter
the
danger
world
that
is
The
Matrix
,
a
movi
direct
by
The
Wachowski
Brother
-LRB-
the
gui
behind
the
hip
thriller
Bound
-RRB-
.

Befor
you
groan
at
the
presenc
of
Reev
in
the
lead
role
,
hold
on
.

There
's
actual
a
stori
with
depth
here
.

It
seem
that
a
group
of
hacker
is
out
to
stop
the
system
that
is
The
Matrix
,
led
by
the
bald
but
brilliant
Laurenc
Fishburn
and
assist
by
Carri
Ann
Moss
,
who
look
quit
cool
in
leather
,
I
must
sai
.

Neo
-LRB-
Reev
-RRB-
is
pretti
much
a
kei
player
in
thi
fight
,
although
he
realli
doe
n't
look
it
at
first
.

Could
thi
amateur
be
``
the
on
''
as
Fisburn
think
he
is
?

Credit
the
Wachowski
for
put
togeth
a
great
stori
that
allow
suspens
to
build
,
someth
that
few
special
effects-laden
film
seem
to
do
these
dai
.

There
's
plenti
of
effect
to
go
around
,
of
cours
,
but
at
least
there
's
a
backbon
to
build
them
on
as
well
.

Reev
is
surprisingli
good
in
hi
role
as
Neo
,
particularli
in
a
fight
scene
with
Fishburn
that
come
across
as
goofi
,
yet
great
.

I
wish
the
villain
were
n't
quit
so
``
gener
''
-LRB-
leftov
G-Men
from
look
for
a
sweet
sci-fi
action
with
plenti
of
stop-mot
swoop
camera
angl
and
,
of
cours
,
a
motherlod
of
a
helicopt
explos
.

Now
the
Wachowski
ar
head
into
horror
film
with
their
next
project
.

Bring
it
on
.

