GETTING
IT
RIGHT
is
a
far
far
cry
from
the
teenag
sex
comedi
you
might
expect
from
a
summer
movi
about
male
virgin
.

The
film
wa
direct
by
Randal
Kleiser
,
who
brought
us
such
bubble-gum
classic
as
GREASE
and
THE
BLUE
LAGOON
.

But
thank
to
Kleiser
's
surprisingli
good
direct
and
an
intellig
script
,
GETTING
IT
RIGHT
is
a
pleasantli
quiet
,
move
,
and
memor
film
--
on
gear
strictli
toward
a
sophist
,
adult
audienc
.

GETTING
IT
RIGHT
star
Jess
Birdsal
-LRB-
Emili
Lloyd
's
boyfriend
in
WISH
YOU
WERE
HERE
-RRB-
as
Gavin
,
a
painfulli
shy
31-year-old
hairdress
who
live
at
home
with
hi
parent
and
who
--
gasp
--
is
still
a
virgin
.

Gavin
ha
no
problem
whatsoev
make
small
talk
with
the
elderli
women
who
clamor
for
hi
servic
at
the
hair
salon
,
but
expos
him
to
a
singl
30-year-old
beauti
and
he
's
a
mess
.

Much
to
Gavin
's
surpris
,
the
woman
who
final
bring
him
out
of
hi
shell
and
rob
him
of
hi
virgin
is
Joan
-LRB-
Lynn
Redgrav
-RRB-
,
a
rich
,
marri
,
and
lone
45-year-old
.

GETTING
IT
RIGHT
is
the
simpl
stori
of
Gavin
's
awkward
and
haphazard
effort
to
get
it
right
;
that
is
,
to
find
the
right
woman
to
fulfil
hi
life
.

GETTING
IT
RIGHT
is
on
of
those
rare
movi
that
devot
itself
entir
to
in-depth
charact
develop
.

Gavin
's
self-consci
voice-ov
narrat
of
hi
experi
firmli
thrust
us
into
hi
psych
.

We
observ
hi
fascin
metamorphosi
from
shy
and
passiv
introvers
to
assert
and
self-confid
matur
.

Birdsal
's
superb
,
underst
perform
as
Gavin
is
the
real
backbon
of
the
film
;
he
seem
convinc
and
authent
throughout
.

The
charm
of
GETTING
IT
RIGHT
also
li
in
the
wide
arrai
of
peopl
who
inhabit
or
invad
Gavin
's
life
.

Despit
her
limit
screen
time
,
Redgrav
make
quit
a
splash
in
the
movi
as
Gavin
's
middle-ag
seduc
.

For
a
support
charact
,
Joan
is
unusu
and
impress
complex
,
thank
in
larg
part
to
Redgrav
's
magnific
perform
.

Gavin
is
also
pursu
,
or
perhap
plagu
,
by
Minni
,
the
total
neurot
daughter
of
a
rich
aristocrat
.

After
Minni
and
Gavin
meet
at
a
parti
,
she
take
great
pleasur
in
disrupt
hi
safe
and
simpl
life
.

Helena
Bonham
Carter
is
delightfulli
pathet
and
kooki
in
the
role
,
and
Sir
John
Gielgud
,
with
hi
tongu
in
hi
cheek
,
embodi
the
height
of
arrog
as
her
father
.

Gavin
's
eventu
love-interest
is
Jenni
,
the
ador
twenty-year-old
singl
mother
who
assist
him
at
the
beauti
salon
;
London
stage
actress
Jane
Horrock
'
fine
perform
yield
a
touchingli
sweet
charact
.

Peter
Cook
,
the
great
British
satirist
,
appear
briefli
in
the
film
as
Gavin
's
uptight
employ
.

The
scene
of
Gavin
's
home
life
with
hi
parent
ar
also
a
treat
.

Gavin
's
over-protect
mother
-LRB-
Pat
Heywood
-RRB-
,
whom
even
Woodi
Allen
would
find
overbear
,
almost
steal
the
movi
.

Her
life
seem
limit
to
just
two
activ
:
treat
Gavin
like
a
10-year-old
and
prepar
exot
but
ined
meal
,
such
as
scald
hot
curri
and
bake
chicken
with
chocol
sauc
.

Gavin
and
hi
father
's
attempt
to
avoid
eat
her
meal
ar
extrem
amus
.

GETTING
IT
RIGHT
,
for
the
most
part
,
is
veri
well-craft
,
except
for
two
minor
shortcom
.

First
,
the
movi
is
about
15
minut
too
long
,
and
consequ
it
lose
momentum
at
time
.

Second
,
the
film
includ
an
altogeth
extran
subplot
about
the
domest
problem
between
Gavin
's
best
friend
,
Harri
,
and
hi
unfaith
lover
,
Winthrop
.

Overal
,
howev
,
GETTING
IT
RIGHT
is
quit
memor
for
it
humor
,
depth
,
sophist
,
and
outstand
act
.

Director
Kleiser
and
screenwrit
Elizabeth
Jane
Howard
-LRB-
adapt
her
own
highli
acclaim
novel
-RRB-
deserv
prais
for
find
just
the
right
tone
for
thi
tale
of
growth
,
matur
,
and
self-discoveri
.

