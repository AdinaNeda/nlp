TIME
BANDITS
,
from
Director
Terri
Gilliam
,
is
a
veri
differ
fantasi
Action/Adventur
movi
about
a
group
of
time-travel
dwarv
,
led
by
Randal
-LRB-
David
Rappaport
-RRB-
,
who
have
stolen
a
map
of
the
Univers
.

Thi
map
contain
time
hole
,
that
,
if
exploit
,
enabl
the
men
to
travel
back
and
forth
through
time
.

The
Suprem
Be
of
the
Univers
-LRB-
Ralph
Richardson
-RRB-
is
the
former
employ
of
these
treasure-seek
bandit
,
and
he
wish
desper
to
regain
the
map
.
.

Dure
the
travel
,
he
appear
as
a
ghostli
face
,
demand
that
thei
return
the
map
that
thei
have
stolen
.

But
,
accord
to
Randal
,
thei
ar
just
``
borrow
''
it
.

Kevin
,
a
young
boi
who
seem
quit
bore
with
hi
life
,
is
unexpectedli
brought
into
the
scheme
of
the
littl
men
when
thei
appear
in
hi
room
,
which
ha
a
portal
for
time
travel
.

He
join
up
with
the
men
and
becom
part
of
their
gang
,
follow
them
on
their
robberi
.

Their
first
victim
is
Napoleon
-LRB-
Ian
Holm
-RRB-
,
whom
thei
rob
dure
a
battl
that
he
is
command
.

From
here
,
the
group
travel
to
the
Middl
Age
,
meet
up
with
Robin
Hood
himself
.

Eventual
,
Kevin
is
separ
from
the
group
and
travel
to
an
Egyptian
time
where
he
is
taken
in
by
King
Agamemnon
,
plai
by
Sean
Conneri
.

Kevin
accident
save
the
King
's
life
,
and
the
King
wish
to
have
Kevin
as
hi
son
.

But
the
group
of
bandit
find
Kevin
and
transport
onto
the
deck
of
the
Titan
.

Meanwhil
,
the
Evil
Geniu
-LRB-
David
Warner
-RRB-
is
watch
the
group
from
Fortress
of
Ultimat
Dark
,
attempt
to
find
a
wai
in
order
to
bring
the
men
,
and
the
map
,
to
him
.

When
Randal
learn
of
the
Fortress
of
Ultimat
Dark
,
which
supposedli
contain
``
the
most
fabul
object
in
the
world
''
,
the
dollar
sign
seemingli
appear
on
hi
ey
,
as
he
is
convinc
that
thei
must
travel
there
.

Once
insid
the
Fortress
of
Ultimat
Dark
,
it
is
unclear
as
to
if
ani
of
the
men
will
make
it
out
aliv
.

TIME
BANDITS
is
a
fantast
made
film
that
cater
to
the
imagin
of
anyon
.

With
a
terrif
soundtrack
,
courtesi
of
Georg
Harrison
,
whom
also
wa
an
Execut
Produc
,
TIME
BANDITS
is
sure
to
be
a
veri
surrealist
,
time-travel
adventur
with
unforgett
charact
that
is
sure
to
entertain
anyon
.

Terri
Gilliam
,
howev
,
doe
not
util
that
greatli
hi
trademark
abil
in
moviemak
dure
thi
film
,
although
thi
doe
not
affect
the
film
that
greatli
.

And
final
,
if
you
like
LABYRINTH
,
you
'll
love
TIME
BANDITS
.

