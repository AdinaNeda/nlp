The
premis
is
simpl
,
if
not
bizarr
.

A
mad
scientist
-LRB-
Trace
Beaulieu
as
Dr.
Clayton
Forrest
-RRB-
launch
an
average-jo
-LRB-
Michael
J.
Nelson
as
Mike
Nelson
-RRB-
into
space
where
he
forc
hi
subject
to
watch
the
most
horrend
movi
ever
made
.

Why
?

It
's
tortur
,
it
's
maniac
,
and
it
's
just
plain
fun
.

Base
on
the
cult-favorit
cabl
televis
seri
,
MYSTERY
SCIENCE
THEATER
3000
:
THE
MOVIE
is
n't
tortur
,
but
as
for
be
maniac
and
just
plain
fun
,
it
foot
the
bill
quit
nice
.

Mike
Nelson
,
on
a
dog-bon
shape
spacecraft
,
spend
hi
dai
as
ani
young
man
would
dream-indulg
in
sarcasm
and
horseplai
,
and
see
quit
a
few
movi
.

The
catch
is
,
these
movi
ar
n't
the
movi
he
choos
,
but
retch
exampl
of
film-mak
chosen
specif
by
Dr.
Forrest
in
an
attempt
to
break
Mike
's
will
to
live
.

Dr.
Forrest
is
convinc
that
on
too
mani
b-movi
is
all
it
will
take
to
ruin
a
man
,
but
with
a
coupl
of
wise-crack
puppet
buddi
-LRB-
Tom
Servo
,
voic
by
Kevin
Murphi
,
and
Crow
T.
Robot
,
voic
by
Beaulieu
-RRB-
,
the
tortur
becom
somewhat
of
a
honorari
parti
for
all
that
is
wrong
in
the
world
of
cheezi
cinema
.

The
purpos
behind
MST3K
is
to
exploit
some
of
the
worst
film
known
-LRB-
even
if
by
veri
few
of
us
-RRB-
to
man
.

We
watch
as
the
silhouett
of
Mike
,
Tom
,
and
Crow
sit
in
a
darken
theater
poke
fun
at
the
movi
go
on
befor
them
.

In
thi
case
,
it
's
the
1954
sci-fi
film
THIS
ISLAND
EARTH
.

We
basic
watch
as
our
three
lead
watch
,
onli
we
get
the
pleasur
of
eavesdrop
on
their
hilari
commentari
.

The
plot
behind
the
movi
rip
apart
ar
realli
quit
irrelev
,
but
for
the
sake
of
those
who
might
want
to
know
,
I
'll
explain
thi
on
.

THIS
ISLAND
EARTH
is
the
tale
of
two
scientist
,
a
man
and
a
woman
,
who
wind
up
aboard
a
spaceship
whose
crew
intend
to
destroi
the
Earth
.

Togeth
the
two
fight
to
surviv
as
well
as
save
their
home
planet
.

To
make
MST3K
work
,
the
film-within-the-film
natur
ha
to
be
as
horribl
as
possibl
,
and
although
the
TV-seri
introduc
us
to
sever
wors
film
than
THIS
ISLAND
EARTH
,
it
's
a
bad
enough
flick
to
bring
about
some
hyster
cruelti
.

MST3K
,
which
doe
n't
actual
contain
the
complet
THIS
ISLAND
EARTH
,
is
a
short
73
minut
,
but
thi
is
a
step
veri
wise
taken
.

As
funni
as
some
of
their
observ
ar
,
it
can
onli
go
so
long
.

Occasion
break
from
THIS
ISLAND
EARTH
also
help
the
film
tremend
.

Although
it
take
a
second
to
get
back
into
the
right
mode
after
thi
premis
ha
been
left
for
a
moment
,
it
's
better
than
overkil
the
whole
concept
post
hast
.

MST3K
lover
will
like
hail
the
film
greatli
,
but
if
you
do
n't
know
what
you
're
in
for
,
it
could
be
a
jar
disappoint
.

Although
I
thought
the
sharp
wit
of
thi
film
wa
worth
three
star
,
it
is
a
movi
to
be
seen
on
home
video
,
late
at
night
when
your
brain
is
not
function
to
full
capac
anywai
,
and
with
a
larg
,
saracast
crowd-New
Year
's
at
midnight
for
exampl
,
which
is
when
I
saw
it
.

Warn
:
Although
MST3K
ha
more
to
it
end
credit
than
most
-LRB-
the
three
lead
us
the
credit
to
poke
some
more
fun
-RRB-
,
thei
ar
actual
more
annoi
than
most
.

The
name
slander
and
asinin
one-lin
were
extrem
unfunni
,
and
after
laugh
for
about
70
minut
straight
,
it
put
a
heavi
damper
on
the
overal
experi
.

Most
peopl
will
like
stai
to
see
what
the
smart-alec
's
have
to
sai
,
but
for
me
,
it
almost
ruin
an
otherwis
good
film
.

