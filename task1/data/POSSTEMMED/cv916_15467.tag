Martin
Scorses
's
triumphant
adapt
of
Edith
Wharton
's
THE
AGE
OF
INNOCENCE
is
a
stun
film
for
the
quintessenti
New
York
filmmak
,
the
man
who
brought
the
street
of
TAXI
DRIVER
and
MEAN
STREETS
to
life
.

It
seem
like
an
odd
choic
for
Scorses
to
do
a
period
piec
in
the
earli
1900
's
,
but
the
fact
that
he
pull
it
off
so
brilliantli
is
a
wonder
,
and
a
testament
to
the
great
of
Scorses
as
a
filmmak
.

Thi
is
a
gorgeou
visual
experi
that
it
sure
on
of
Scorses
's
finest
.

Newland
Archer
-LRB-
Day-Lewi
-RRB-
is
a
prestigi
lawyer
who
is
engag
to
Mai
Welland
-LRB-
Ryder
-RRB-
,
a
somewhat
empti
and
shallow
New
Yorker
,
who
belong
to
a
prestigi
famili
and
is
quit
beauti
.

The
marriag
is
on
which
will
unit
two
veri
prestigi
famili
,
in
a
societi
where
noth
is
more
import
than
the
opinion
of
other
.

On
the
dai
that
Archer
is
to
announc
hi
engag
to
Mai
,
Countess
Ellen
Olenska
-LRB-
Pfeiffer
-RRB-
,
cousin
of
Mai
,
walk
into
Archer
's
life
.

Archer
is
immedi
captiv
,
and
find
himself
in
love
with
Ellen
.

Archer
is
also
bound
by
the
limit
of
New
York
societi
,
which
is
an
intrus
as
ani
other
in
the
world
.

Archer
find
himself
have
a
secret
love
affair
in
hi
mind
with
Countess
Olenska
,
attempt
to
keep
her
in
hi
mind
while
try
not
to
lose
hi
social
statu
.

The
film
's
subject
matter
mai
seem
alien
to
Scorses
,
but
the
theme
is
definit
not
.

It
is
a
theme
of
forbidden
romanc
,
guilti
pleasur
,
and
the
consequ
caus
becaus
of
those
action
.

There
is
a
painstakingli
flaw
hero
,
and
hi
choic
between
the
life
he
want
,
and
the
life
he
is
destin
for
.

In
truth
,
it
is
a
film
about
a
societi
the
audienc
doe
n't
know
about
,
but
want
to
find
out
more
,
much
like
the
societi
of
GOODFELLAS
or
even
KUNDUN
.

The
perform
ar
absolut
breathtak
.

Day-Lewi
portrai
more
mental
anguish
in
hi
face
than
on
man
should
be
forc
to
take
.

Pfeiffer
is
marvel
as
Countess
Olenska
,
a
mix
of
passion
and
beauti
that
the
audienc
would
die
for
as
well
.

Ryder
is
probabl
the
gem
of
the
group
,
for
it
is
her
quiet
presenc
that
overwhelm
the
plot
,
and
slowli
push
Day-Lewi
closer
and
closer
to
hi
eventu
end
.

The
support
cast
is
also
wonder
,
with
sever
charact
so
singular
that
thei
ar
indel
in
on
's
memori
.

Scorses
definit
ha
a
passion
for
filmmak
.

Hi
lavish
and
sumptuou
set
design
and
marvel
recreat
of
New
York
is
a
wondrou
sight
.

He
liter
transport
the
viewer
to
anoth
world
with
incred
imageri
.

Hi
script
is
also
excel
,
slow
in
buildup
,
with
a
rapid
conclus
and
a
fantast
end
that
ha
to
be
seen
to
be
believ
.

It
is
difficult
to
make
a
period
piec
grip
:
Scorses
,
howev
,
doe
it
beautifulli
.

The
famou
camera
of
the
legendari
director
ar
also
everywher
.

He
is
patient
,
but
he
film
everyth
and
anyth
remot
import
.

The
camera
sweep
,
pan
,
track
,
and
do
more
than
thei
've
ever
done
,
but
thei
ar
so
subtl
,
on
doe
n't
realiz
he
's
watch
all
the
Scorses
hallmark
until
a
2nd
view
.

The
central
track
shot
is
probabl
longer
and
more
complex
than
the
famou
GOODFELLAS
shot
,
but
the
viewer
doe
n't
notic
it
,
becaus
we
want
to
see
more
of
thi
gorgeou
world
.

There
ar
a
few
deft
touch
of
filmmak
that
ar
simpli
outstand
,
and
Joann
Woodward
'
narrat
is
exquisit
.

Not
a
fast
film
like
GOODFELLAS
,
thi
share
more
in
common
with
KUNDUN
than
anyth
els
.

And
like
KUNDUN
,
thi
is
a
slow-start
film
that
truli
shine
,
when
given
the
chanc
to
fulli
breath
and
bloom
in
the
end
.

A
beauti
film
by
a
director
continu
to
challeng
himself
year
after
year
.

