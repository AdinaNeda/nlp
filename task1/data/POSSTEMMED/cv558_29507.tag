In
1914
,
as
the
Great
War
descend
upon
Europ
,
Sir
Ernest
Shackleton
began
hi
third
expedit
to
Antarctica
,
thi
time
to
be
the
first
to
cross
the
ice-bound
contin
on
foot.men
set
sail
on
the
brigantin
Enduranc
and
thei
would
not
be
heard
from
again
for
nearli
two
year
.

Director
Georg
Butler
,
us
film
footag
and
photograph
from
the
histor
journei
,
tell
their
remark
stori
in
``
The
Enduranc
:
Shackleton
's
Legendari
Antarctic
Expedit
.
''

The
expedit
start
out
as
a
mere
incred
difficult
quest
to
travers
,
on
foot
,
the
vast
wasteland
of
Antarctica
.

As
the
intrepid
ship
and
crew
enter
the
Weddel
Sea
,
the
entri
to
the
cold
,
cruel
contin
,
thei
did
not
plan
on
the
dens
pack
ic
bring
the
voyag
to
an
abrupt
halt
as
the
Enduranc
becam
trap
within
sight
of
land
.

For
month
the
ic
thicken
and
compress
until
the
ship
wa
crush
,
strand
the
men
on
the
flow
,
propel
them
awai
from
their
goal
and
,
eventu
,
into
their
lifeboat
for
safeti
.

Shackleton
,
face
with
a
desol
situat
,
made
the
decis
to
lead
the
three
tini
boat
to
the
rel
safeti
of
uninhabit
Elephant
Island
.

Once
on
thi
``
safe
''
haven
,
Shackleton
next
turn
hi
sight
to
save
hi
crew
and
return
them
to
civil
.

He
and
five
of
hi
men
leav
the
island
in
an
open
boat
to
make
the
800
mile
journei
on
the
world
's
roughest
sea
to
get
help
from
the
nearest
inhabit
island
of
South
Georgia
.

Miracul
,
the
six
land
,
afterdai
at
sea
,
on
the
uninhabit
side
of
the
target
island
and
ar
forc
to
trek
across
treacher
,
snow
cover
mountain
,
coveringmil
in
justhour
.

Thi
ordeal
is
follow
by
sever
attempt
by
Shackleton
to
get
back
to
Elephant
Island
to
rescu
the
remain
crewmen
.

In
the
end
,
he
is
success
and
everyon
made
it
back
home
aliv
,
onli
to
be
swallow
up
by
the
ravag
of
the
war
.

The
titl
of
thi
fascin
documentari
,
``
The
Enduranc
,
''
is
more
than
just
the
name
of
ship
that
carri
the
crew
of
brave
men
to
the
edg
of
the
world
.

It
is
also
the
descript
of
the
stamina
and
abil
of
everi
on
of
the
men
who
surviv
19-month
of
the
toughest
,
most
gruel
condit
on
the
face
of
the
earth
.

The
name
of
the
film
is
a
tribut
to
everi
member
of
the
crew
and
to
the
ship
that
shelter
them
for
mani
month
of
frigid
Antarctic
weather
.

It
also
describ
the
focus
mental
state
of
Shackleton
who
never
gave
up
on
himself
or
hi
men
.

With
astonish
foresight
,
Shackleton
brought
Australian
cinematograph
and
photograph
Frank
Hurlei
along
on
the
journei
.

The
film
footag
and
photo
ar
remark
as
thei
captur
the
ordeal
of
the
men
,
dai
to
dai
life
,
the
affect
all
felt
for
their
loyal
sled
dog
,
the
slow
destruct
of
the
Enduranc
and
the
prepar
to
get
off
of
the
pack
ic
flow
.

After
the
sink
of
the
ship
,
the
remain
film
stock
,
with
the
except
of
a
still
camera
and
three
roll
of
film
,
had
to
be
discard
so
the
continu
chronicl
of
the
incred
journei
get
less
photo
document
as
the
ordeal
plod
on
.

Consid
Hurlei
wa
us
rel
primit
photo
technolog
in
on
of
the
world
's
harshest
environ
and
captur
so
much
on
film
is
a
feat
unto
itself
.

-LRB-
Hurlei
creat
hi
own
account
of
the
Shackleton
expedit
in
hi
1919
documentari
,
``
South
,
''
an
interest
companion
piec
for
``
The
Enduranc
.
''

If
Shackleton
's
origin
idea
,
document
hi
trek
across
the
frigid
southern
contin
,
had
been
success
,
it
would
have
been
consid
a
super
human
feat
.

As
you
watch
the
stori
of
``
The
Enduranc
''
unfold
,
you
realiz
that
the
disastr
obstacl
the
Sir
Ernest
and
hi
men
face
and
overcam
were
even
greater
accomplish
than
the
first
intent
.

-LRB-
Shackleton
's
amaz
800-mile
journei
in
an
open
boat
is
fodder
for
a
rous
adventur
stori
all
by
itself
.
-RRB-

There
ar
a
coupl
of
minor
nitpick
with
thi
fine
work
,
though
.

Actor
Liam
Neeson
narrat
the
stori
,
but
hi
voic
is
too
soft
spoken
and
lilt
for
the
dramat
tale
.

The
natur
of
the
ordeal
also
concentr
most
of
the
visual
materi
,
both
film
and
photo
,
in
the
documentari
's
first
half
.

As
the
crew
's
adventur
take
more
drastic
path
,
Hurlei
had
less
and
less
film
stock
avail
to
chronicl
the
awesom
event
.

Helmer
Butler
and
hi
team
fill
in
the
blank
,
but
the
treasur
of
``
The
Enduranc
,
''
it
film
footag
,
is
mostli
releg
to
the
earli
month
of
the
expedit
.

I
ca
n't
think
of
a
true-lif
adventur
that
show
,
better
,
the
triumph
of
man
over
seemingli
insurmount
odd
.

``
The
Enduranc
,
''
base
on
Carolin
Alexand
's
book
,
is
an
eye-open
documentari
that
goe
where
few
men
have
gone
befor
and
,
even
more
remark
,
allsurvivor
live
to
tell
about
it
.

