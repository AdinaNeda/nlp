Call
me
crazi
,
but
I
do
n't
see
SAVING
PRIVATE
RYAN
as
the
film
of
the
summer
.

A
good
movi
,
ye
,
with
chillingli
realist
battl
scene
and
emot
to
spare
.

An
utterli
rivit
movi
on
par
with
Steven
Spielberg
's
best
work
,
no
.

Person
,
if
I
wa
Spielberg
,
I
would
n't
go
back
to
the
World
War
II
era
on
more
time
after
SCHINDLER
'S
LIST
and
the
Indiana
Jone
movi
.

I
'm
guess
Steve
ha
a
thing
for
Nazi
;
I
realli
would
n't
be
surpris
if
a
velociraptor
at
on
of
the
swastika-wear
dude
in
the
next
JURASSIC
PARK
movi
.

All
lofti
pretens
asid
,
SAVING
PRIVATE
RYAN
is
the
goriest
movi
thi
side
of
a
1980
slasher
flick
.

The
differ
is
,
it
's
easi
dure
FRIDAY
THE
13TH
to
laugh
off
a
spear
stick
out
of
Kevin
Bacon
's
chest
while
blood
spurt
like
crazi
,
but
it
's
damn
hard
to
sit
and
eat
Rees
's
Piec
while
soldier
suffer
machin
gun
bullet
to
the
head
and
have
their
intestin
spill
out
onto
the
battlefield
.

And
believ
me
,
there
's
plenti
of
it
.

A
30-minut
sequenc
at
the
begin
of
the
movi
ha
an
Army
captain
-LRB-
Tom
Hank
-RRB-
and
hi
soldier
land
at
Omaha
Beach
to
join
countless
other
American
who
ar
alreadi
under
fire
.

Live
ar
lost
in
second
as
the
purposefulli
confus
and
jar
scene
goe
on
and
on
,
and
all
the
young
men
in
the
audienc
find
themselv
never
,
ever
want
to
be
draft
.

Cut
to
some
bureaucrat
defens
offic
,
where
a
hundr
women
pound
out
sympathi
form
letter
to
the
famili
of
the
casualti
.

One
woman
happen
upon
an
interest
detail
--
three
brother
in
differ
platoon
were
kill
in
combat
,
and
their
mother
is
get
the
telegram
todai
.

I
guess
it
's
an
interest
convers
piec
to
everyon
but
the
mother
,
so
the
Army
chief
-LRB-
Harv
Presnel
-RRB-
send
Hank
on
what
is
essenti
a
public
relat
mission
,
to
find
the
fourth
Ryan
brother
and
send
him
home
.

That
wai
,
the
Army
save
the
postag
on
yet
anoth
telegram
to
Mrs.
Ryan
.

One
good
thing
about
SAVING
PRIVATE
RYAN
is
that
the
soldier
who
ar
head
to
rescu
Ryan
know
it
's
a
mission
design
to
make
the
Army
look
good
.

Thei
question
the
worth
of
risk
eight
soldier
'
live
to
save
on
,
and
Hank
'
charact
admit
he
doe
n't
give
a
damn
about
Ryan
;
he
's
just
follow
order
.

If
thi
wa
a
John
Wayn
movi
,
thing
would
be
differ
.

There
'd
be
a
phoni
,
``
Let
's
go
get
that
boi
,
gosh
darn
it
!
''

attitud
that
would
sugarcoat
the
realiti
of
war
.

By
do
thing
thi
wai
,
Spielberg
admit
the
instinct
of
self-preserv
and
complac
that
everi
normal
person
ha
.

It
make
SAVING
PRIVATE
RYAN
a
lot
more
power
than
an
testosterone-driven
Stallon
movi
mission
.

The
movi
's
pattern
is
to
have
long
battl
scene
follow
by
quiet
scene
of
semi-introspect
convers
among
the
soldier
.

Hank
is
paint
first
as
a
nails-tough
Army
man
care
to
hide
hi
true
self
from
the
other
men
.

It
's
not
that
wai
for
long
.

Edward
Burn
,
the
gui
you
get
for
your
movi
when
Ben
Affleck
is
n't
avail
,
is
the
impuls
on
.

Jeremi
Davi
plai
the
translat
who
is
see
combat
for
the
first
time
,
and
so
on
.

None
of
the
charact
ar
fascin
or
natur
born
hero
,
but
wa
Spielberg
's
obviou
intent
.

I
wonder
when
I
heard
SAVING
PRIVATE
RYAN
wa
three
hour
long
,
how
were
thei
go
to
fill
three
hour
'
time
search
for
on
person
and
still
make
it
interest
?

There
ar
a
few
fals
start
;
Hank
find
on
Privat
Ryan
in
Ted
Danson
's
compani
-LRB-
Apparent
,
Danson
join
the
Army
after
``
Ink
''
wa
cancel
.
-RRB-

and
break
the
bad
new
befor
learn
it
's
the
wrong
Ryan
.

And
of
cours
the
movi
's
far
from
over
when
Hank
doe
locat
Ryan
-LRB-
Matt
Damon
,
the
other
gui
you
get
when
Affleck
is
n't
avail
-RRB-
.

That
's
when
the
movi
turn
into
more
of
a
convent
war
flick
,
although
bodi
part
and
limb
still
fly
like
never
befor
.

SAVING
PRIVATE
RYAN
is
worth
your
time
,
but
it
's
definit
no
SCHINDLER
'S
LIST
.

The
battl
scene
ar
intens
and
realist
,
but
some
of
the
attempt
at
sincer
emot
ar
n't
.

The
movi
's
bookend
ar
particularli
cheesi
and
out
of
place
,
when
an
old
Privat
Ryan
goe
to
the
cemetari
with
hi
famili
and
bawl
hi
ey
out
,
whine
,
``
Tell
me
I
've
led
a
good
life
.
''

Tell
me
I
'm
a
good
man
.
''
''

Spielberg
wa
appar
follow
the
Jame
Cameron
line
of
thought
that
ani
three-hour
movi
about
the
past
should
be
frame
by
a
self-contain
prologu
and
epilogu
that
take
place
in
the
present
.

Much
more
effect
ar
subtler
scene
,
like
the
on
where
sever
of
Hank
'
men
ar
rifl
thrugh
a
bag
of
dogtag
from
dead
soldier
,
and
thei
make
light
of
what
thei
're
do
by
pretend
thei
're
plai
poker
.

Thi
while
an
airborn
divis
march
by
,
their
ey
full
of
that
mound
of
dogtag
.

By
not
explicitli
point
out
that
it
symbol
the
random
poker
game
that
life
and
death
can
be
in
war
,
the
audienc
get
the
messag
.

And
even
if
it
's
not
a
classic
,
SAVING
PRIVATE
RYAN
is
definit
on
of
the
better
war
movi
out
there
.

