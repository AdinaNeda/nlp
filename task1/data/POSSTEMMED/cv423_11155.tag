If
you
do
n't
care
how
and
what
the
food
you
bui
goe
through
in
your
local
supermarket
,
then
thi
film
is
not
for
you
...
.

mayb
it
is
better
to
ignor
it
,
as
thi
strong
and
bite
satir
,
as
onli
Juzo
Itami
can
deliv
,
will
give
you
an
insight
as
to
what
some
food
go
through
.

Have
,
in
the
past
,
made
film
that
pull
everi
punch
imagin
,
from
the
famili
tradit
-LRB-
THE
FUNERAL
-RRB-
,
to
tax
-LRB-
A
TAXING
WOMAN
&
A
TAXING
WOMAN
RETURNS
-RRB-
,
to
the
diabol
gangster
-LRB-
MINBO
-RRB-
,
to
hospit
-LRB-
THE
SERIOUSLY
ILL
-RRB-
,
Juzo
Itami
,
thi
time
take
on
the
supermarket
'
own
war
,
to
get
the
consum
.

And
while
thi
film
ha
it
funni
side
,
in
the
crazi
situat
that
it
creat
,
it
also
show
anoth
side
,
which
is
veri
much
the
attitud
of
a
corpor
structur
that
is
more
concern
with
it
profit
than
it
is
with
ani
custom
.

Goro
ha
a
supermarket
.

And
a
competitor
come
into
the
neighborhood
,
try
to
take
him
out
of
busi
by
provid
less
expens
price
for
everyth
that
is
sold
in
the
supermarket
.

Bargain
Galor
,
it
name
,
ha
in
mind
the
closur
of
Goro
's
humbler
venu
,
so
that
in
the
end
it
can
mark
up
it
price
in
ani
wai
it
can
.

Goro
hire
Hanako
,
a
housewif
whose
talent
seem
to
be
better
suit
to
manag
,
than
thei
ar
in
anyth
els
.

Hanako
,
as
she
get
familiar
with
the
oper
,
discov
mani
of
the
trick
of
the
trade
,
done
by
mani
of
the
elder
in
charg
of
each
section
of
the
market
.

And
the
procedur
,
ar
not
alwai
``
fresh
''
food
,
but
a
new
packag
and
a
new
date
,
to
try
and
convinc
the
custom
that
qualiti
is
on
the
shelv
.

Hanako
's
idea
take
hold
,
start
with
a
few
of
the
women
that
work
in
the
supermarket
,
be
that
thei
do
not
even
shop
there
at
all
.

And
Hanako
discov
along
the
wai
that
a
few
giveawai
here
and
there
make
a
bit
of
a
differ
.

If
not
caus
some
seriou
problem
in
the
run
of
the
store
.

A
price
error
,
forc
them
to
give
awai
Egg
for
veri
low
price
,
and
caus
the
store
a
few
hassl
,
but
,
it
brought
the
peopl
in
,
and
everyon
know
that
these
custom
will
bui
a
few
other
thing
.

As
the
oper
get
stronger
,
the
competit
also
ha
trickeri
up
it
sleev
,
and
it
ha
been
undermin
much
of
the
oper
by
payback
to
on
of
the
store
manag
,
as
well
as
the
meat
and
fish
elder
supervisor
.

Eventual
these
peopl
ar
found
out
,
and
Hanako
ha
to
tread
slowli
and
carefulli
,
until
in
the
end
thei
all
have
a
showdown
.

And
here
,
the
truth
and
honesti
win
out
,
and
even
on
of
the
elder
decid
to
stai
rather
than
go
to
work
for
the
corrupt
oppon
.

In
the
process
,
thei
also
find
that
``
fresh
''
-LRB-
with
some
veri
funni
joke
on
thi
word
as
well
-RRB-
becom
a
much
more
import
tool
than
thei
imagin
.

With
some
veri
nice
perform
,
in
thi
hectic
,
and
fast
pace
film
,
thi
film
give
us
a
sit
of
two
hour
that
is
satisfi
in
mani
wai
,
although
a
fail
love
stori
between
Goro
and
Hanako
doe
not
come
off
much
,
even
though
toward
the
end
it
is
suggest
.

But
thi
would
take
awai
from
the
satir
and
it
strength
.

With
food
gag
all
over
the
place
,
the
film
let
you
have
it
.

And
what
fun
it
is
.

-LRB-
Editori
-RRB-
The
director
of
thi
film
,
Juzo
Itami
,
di
shortli
after
the
film
wa
finish
,
and
he
left
behind
a
legaci
of
comedi
and
satir
that
is
rare
,
and
uncompromis
.

We
would
wish
that
he
had
been
abl
to
make
on
about
film
make
,
of
which
he
must
have
had
a
few
stori
to
tell
,
but
we
will
have
to
do
with
much
of
the
Japanes
societi
,
and
it
mani
errant
wai
...
.

not
to
sai
that
these
thing
do
not
happen
anywher
els
...
.

America
is
no
less
guilti
of
ani
of
these
subject
at
all
,
should
anyon
have
the
gut
to
tackl
it
...
we
could
learn
someth
along
the
wai
,
too
...
.

worth
see
,
special
,
is
A
TAXING
WOMAN
and
it
follow
up
A
TAXING
WOMAN
RETURNS
.

THE
FUNERAL
is
also
good
,
although
not
as
funni
as
the
later
film
.

TAMPOPO
is
also
veri
enjoy
.

All
four
of
these
film
ar
avail
in
video
,
which
mean
your
best
local
site
for
foreign
film
mai
have
them
...
ask
for
it
.

