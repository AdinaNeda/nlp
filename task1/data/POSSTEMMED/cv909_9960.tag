GERE
,
WILLIS
,
POITIER
CHASE
EACH
OTHER
AROUND
THE
WORLD
When
the
Soviet
Union
implod
,
the
western
countri
lost
their
shadow
.

With
the
Unite
State
friendli
with
the
Russian
,
we
no
longer
had
an
entiti
to
blame
for
the
world
's
problem
.

Thi
show
up
in
Hollywood
film
as
the
communist
govern
wa
no
longer
the
easi
bad
gui
.

It
's
time
to
rejoic
becaus
we
've
found
our
new
villain
.

Now
it
's
no
longer
the
Russian
govern
who
send
killer
out
into
foreign
land
,
it
's
the
Russian
_
Mafia
_
.

A
perfect
solut
,
it
combin
the
dread
of
organ
crime
and
the
still-pres
uneasi
with
the
former
Eastern
Block
countri
.

Best
of
all
,
the
villain
ar
still
foreign
:
fear
of
the
other
alwai
plai
best
.

So
it
is
a
crime
lord
in
Moscow
that
send
legendari
hitman
the
Jackal
-LRB-
Bruce
Willi
-RRB-
to
assassin
a
highli
place
US
govern
figur
in
retali
for
the
death
of
hi
brother
dure
a
nightclub
raid
.

The
FBI
is
at
a
loss
as
to
how
to
protect
the
target
from
someon
thei
're
not
sure
even
exist
.

Come
to
their
rescu
is
former
IRA
oper
Declan
Mulqueen
-LRB-
Richard
Gere
-RRB-
who
is
temporarili
releas
from
prison
to
assist
FBI
agent
Carter
Preston
-LRB-
Sidnei
Poitier
-RRB-
and
Russian
Major
Valentina
Koslova
-LRB-
Dian
Venora
-RRB-
.

Mulqueen
's
ex-girlfriend
Basqu
terrorist
Isabella
-LRB-
Mathilda
Mai
-RRB-
is
the
onli
person
who
ha
seen
the
elus
Jackal
.

-LRB-
Presum
there
is
an
exclus
intern
terrorist
club
somewher
where
the
three
met
.
-RRB-

The
film
follow
two
parallel
track
as
the
Jackal
prepar
for
hi
$
70
million
hit
and
Mulqueen
attempt
to
locat
him
while
Preston
make
sure
that
the
Irishman
doe
n't
slip
awai
.

Cross
numer
border
and
don
variou
disguis
for
both
himself
and
hi
mini-van
,
the
killer
is
alwai
on
step
ahead
of
hi
pursuer
.

Be
veri
loos
base
on
the
same
book
the
1973
thriller
``
The
Dai
Of
The
Jackal
''
,
comparison
between
the
two
film
is
inevit
.

There
is
no
doubt
that
the
origin
is
the
better
movi
,
plai
the
stori
for
suspens
rather
than
the
current
action/adventur
.

As
a
mysteri
,
``
The
Jackal
''
ha
enough
hole
in
it
to
ruin
the
tale
,
but
if
you
can
accept
it
for
what
it
is
,
there
's
entertain
to
be
had
.

Hole
?

Let
's
seeA
pivot
clue
for
Mulqueen
is
so
obscur
that
he
must
possess
psychic
power
to
pick
it
up
.

For
a
20-year
veteran
that
can
command
the
big
buck
,
the
Jackal
is
an
incred
poor
shot
.

The
final
scene
between
Gere
and
Willi
occur
in
a
locat
that
should
be
mob
with
polic
,
but
it
's
just
the
two
of
them
.

Willi
'
disguis
usual
look
like
Bruce
Willi
and
ar
just
as
interest
as
Val
Kilmer
's
in
``
The
Saint
''
.

-LRB-
And
lest
you
misunderstand
,
that
's
not
a
compliment
.
-RRB-

But
the
three
star
ar
fun
to
watch
.

It
's
good
to
see
Gere
in
someth
other
than
a
busi
suit
.

Willi
ha
a
mix
histori
in
pick
project
,
but
hi
charact
ar
alwai
watchabl
.

Poitier
is
by
far
the
superior
actor
,
but
ha
limit
screen
time
.

The
problem
in
logic
ar
flaw
,
but
do
n't
ruin
the
experi
.

Occasion
there
ar
movi
that
transcend
their
blemish
.

Thi
is
on
of
them
.

-LSB-
The
appear
in
the
11/20/97
``
Bloomington
Voic
''
,
Bloomington
,
Indiana
-RSB-

