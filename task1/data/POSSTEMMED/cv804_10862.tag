Satir
film
usual
fall
into
on
of
two
categori
:
1
-RRB-
long-term
satir
where
everyth
,
includ
the
joke
,
is
somehow
relat
on
a
larg
scale
-LRB-
i.e.
``
Dr.
Strangelov
''
;
2
-RRB-
situat
satir
in
which
the
comedi
and
theme
ar
deriv
moment
by
moment
and
scene
by
scene
-LRB-
i.e.
``
Men
In
Black
''
-RRB-
.

What
's
uniqu
about
``
Wag
The
Dog
''
is
that
it
doe
n't
necessarili
fit
either
of
these
descript
,
and
yet
is
both
at
the
same
time
.

The
result
is
a
clever
comedi
base
on
an
origin
,
smart
premis
,
even
if
the
focu
doe
tend
to
wander
a
bit
.

If
there
's
anyth
that
affect
American
the
most
it
's
polit
and
showbusi
.

In
thi
ag
of
inform
and
technolog
,
we
've
been
so
condit
by
the
media
and
the
entertain
industri
in
our
wai
of
perceiv
thing
,
it
's
gotten
to
the
point
that
we
ar
practic
control
by
it
.

We
need
the
media
to
bring
us
the
most
up-to-d
new
,
but
we
also
need
the
escap
of
Hollywood
to
counter
all
that
think
.

Thi
film
start
off
well
by
quickli
establish
these
idea
,
and
then
present
an
interest
scenario
:
if
polit
,
the
media
coverag
of
polit
,
and
the
entertain
industri
ar
so
close
relat
,
could
n't
someon
with
the
power
and
author
to
manipul
on
of
the
aspect
manipul
them
all
?

That
's
the
idea
Conrad
Brean
-LRB-
De
Niro
-RRB-
,
a
profession
Washington
spin
doctor
,
come
up
with
in
order
to
restor
the
presid
's
good
name
who
ha
been
accus
of
child
molest
just
week
befor
the
elect
.

Brean
,
along
with
the
presid
's
top
advisor
,
Winifr
Ame
-LRB-
Hech
-RRB-
,
fly
to
California
to
meet
up
with
hot-shot
movi
produc
Stanlei
Motss
-LRB-
Hoffman
-RRB-
.

Thei
pitch
him
an
idea
to
``
produc
''
the
imag
of
a
war
with
Albania
-LRB-
it
's
a
countri
no
on
's
heard
of
so
thei
peopl
will
bui
it
-RRB-
.

Soon
the
wheel
ar
in
motion
and
the
film
's
rif
satir
come
into
plai
.

We
're
inund
with
the
Hollywood
mind
set
as
Motss
bring
in
a
number
of
expert
to
help
manag
the
situat
,
such
as
a
folk
singer
,
visual
and
sound
effect
technician
,
and
a
modern
style
expert
known
as
the
Fad
King
-LRB-
Leari
-RRB-
.

The
initi
atmospher
is
rich
with
numer
pop
cultur
refer
where
everyth
somehow
relat
to
someth
on
TV
,
in
the
movi
,
or
in
the
new
.

The
charact
'
dialogu
is
witti
and
funni
,
although
some
of
the
actual
joke
and
gag
seem
rather
sitcom-esqu
.

For
exampl
,
on
scene
show
Motss
and
Brean
's
product
crew
film
an
actress
portrai
a
poor
Albanian
refuge
.

Motss
order
a
kitten
for
the
girl
to
carri
,
but
instead
wound
up
with
a
dozen
differ
dog
.

Sinc
it
's
all
computer-enhanc
anywai
,
she
end
up
cradl
a
bag
of
Tostito
with
the
kitten
to
be
insert
later
.

Thi
demonstr
the
film
's
abil
to
take
a
rel
ordinari
situat
and
make
it
smart
and
funni
just
through
the
premis
.

But
then
there
's
the
gag
and
one-lin
that
,
although
ar
humor
,
seem
a
littl
gratuiti
in
the
long
run
such
as
Motss
not
be
abl
to
rememb
the
actress
's
name
,
or
,
whenev
a
problem
emerg
,
alwai
respond
with
,
``
Thi
is
noth
!
''

By
the
time
the
film
make
it
to
the
midpoint
,
everyth
that
is
go
to
happen
in
term
of
plot
happen
.

The
first
act
is
quickli
pace
,
while
the
second
act
contain
most
of
the
joke
and
is
the
core
of
the
film
.

Everi
ten
minut
or
so
a
new
problem
crop
up
that
Motss
and
Brean
must
deal
with
,
and
it
's
alwai
fascin
to
see
what
thei
do
and
how
thei
do
it
.

The
screenplai
provid
good
character
and
actual
deal
with
most
of
the
problem
head-on
in
realist
manner
.

Howev
,
it
doe
have
a
tendenc
to
avoid
thing
and
shift
it
focu
from
the
elect
campaign
and
fals
war
smokescreen
to
someth
a
bit
off
the
subject
.

As
the
third
act
roll
around
the
satir
natur
becom
more
concern
with
the
situat
and
less
so
with
the
big
pictur
.

What
had
start
off
as
a
brilliant
,
somewhat
harmless
conspiraci
to
fool
the
gener
public
begin
to
seem
more
like
someth
that
would
happen
in
a
movi
.

Still
,
the
situat
comedi
is
quit
funni
,
especi
a
montag
show
the
entir
countri
fling
old
shoe
into
tree
to
honor
Sgt.
Willi
Schumann-a
war
hero
Motss
and
Brean
concoct
.

Truli
great
satir
manag
to
creat
for
an
entertain
stori
that
make
you
laugh
at
the
time
,
but
think
about
afterward
and
``
Wag
The
Dog
''
at
least
succe
in
thi
respect
.

Howev
,
in
retrospect
it
seem
like
so
much
more
could
have
been
done
,
name
by
extend
the
run
time
and
fill
in
the
plot
hole
.

Then
again
,
it
's
a
valiant
effort
.

