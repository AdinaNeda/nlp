There
is
noth
like
AMERICAN
HISTORY
X
in
theater
or
on
video
.

No
other
featur
film
take
such
a
cold
hard
look
at
the
lure
,
the
cultur
,
and
the
brotherhood
of
white
supremaci
.

Nice
gui
Ed
Norton
Jr.
-LRB-
who
sang
in
EVERYONE
SAYS
I
LOVE
YOU
-RRB-
plai
Derek
,
a
twenty-year
old
skinhead
.

Dad
's
subtl
racism
grew
larg
in
Derek
,
after
gang
member
kill
hi
father
.

Dad
wa
fight
a
fire
when
thei
shot
him
.

Now
Derek
keep
hi
head
shave
and
ha
a
giant
swastika
tattoo
over
hi
heart
.

Derek
is
more
interest
in
the
IDEAS
of
white
supremaci
than
in
it
cultur
of
violenc
.

At
a
basketbal
court
,
black
and
white
temper
flare
.

Derek
channel
the
aggress
into
a
game
,
black
versu
white
,
for
ownership
of
the
court
.

When
the
choic
present
itself
,
Derek
goe
for
game
point
instead
of
the
sucker-punch
.

Cameron
-LRB-
Staci
Keach
-RRB-
step
in
to
Derek
's
life
as
a
surrog
father
.

He
take
Derek
under
hi
wing
and
nurtur
hi
racist
feel
.

Keep
hi
own
crimin
record
spotless
,
he
us
Derek
as
a
leader
and
organ
for
high-vis
racial
intimid
.

Derek
oblig
by
lead
hi
younger
and
dumber
friend
in
race-motiv
mob
crime
.

At
the
bottom
of
the
chain
,
Derek
's
younger
brother
Danni
-LRB-
Edward
Furlong
,
made
famou
in
TERMINATOR
2
-RRB-
join
the
skinhead
not
for
ideolog
or
intellectu
reason
,
but
becaus
he
admir
hi
brother
and
he
want
to
belong
.

One
night
three
black
youth
break
into
Derek
's
truck
,
which
is
exactli
what
Derek
ha
been
wait
for
.

Outsid
in
hi
short
and
hi
tattoo
,
he
shoot
them
all
.

The
third
would-b
thief
,
unarm
,
is
onli
wound
.

In
the
kei
scene
of
the
film
,
Derek
command
the
kid
into
a
posit
where
he
can
be
kill
with
on
gloriou
,
enraptur
,
aw
stomp
.

-LRB-
The
fun-spoil
NC-17
of
ORGAZMO
seem
even
more
inappropri
,
consid
AMERICAN
HISTORY
X
wa
rate
R
.

What
sort
of
countri
is
thi
that
sai
sex
comedi
ar
a
bigger
threat
to
our
youth
than
brutal
,
ecstat
violenc
?
-RRB-

The
polic
arriv
just
as
Derek
kill
the
last
thief
.

Derek
doe
not
resist
the
cop
,
and
as
thei
spin
him
around
to
cuff
him
,
the
film
slow
down
.

Derek
rais
hi
eyebrow
and
smile
at
hi
littl
brother
in
a
chill
,
sadist
,
satisfi
grin
.

Now
in
prison
,
Derek
face
new
challeng
.

As
the
black
man
in
the
laundri
tell
him
,
``
in
the
joint
,
you
the
nigra
,
not
me
.
''

There
is
a
cliqu
of
swastika-wear
skinhead
,
but
thei
ar
not
interest
in
the
ideolog
of
white
supremaci
.

Thei
onli
us
the
symbol
as
a
mean
of
intimid
.

Derek
find
himself
truli
alon
,
truli
in
danger
,
and
truli
afraid
.

When
Derek
final
get
out
of
prison
,
he
find
that
hi
friend
from
the
gang
have
also
chang
.

Without
Derek
's
leadership
,
thei
have
shun
the
white
supremacist
ideolog
for
the
white
supremacist
cultur
.

It
is
the
final
factor
that
make
him
realiz
how
badli
he
's
screw
up
.

In
the
end
,
he
spend
qualiti
time
with
is
brother
try
to
undo
the
respect
and
admir
he
had
earlier
inspir
in
Danni
.

The
film
end
a
littl
too
deliber
,
too
neatli
after
the
unchain
emot
and
violent
glee
of
the
rest
of
the
film
,
but
it
bare
detract
from
the
overal
experi
.

Edward
Norton
give
an
Oscar-worthi
perform
.

Although
some
of
hi
dialogu
seem
to
be
written
without
enough
convict
,
Norton
's
perform
compens
.

-LRB-
An
exampl
that
come
to
mind
is
hi
pep
talk
befor
loot
the
store
.
-RRB-

He
also
captur
the
essenc
of
an
older
brother
.

He
took
hi
respons
as
a
role
model
to
hi
younger
brother
veri
serious
,
veri
lovingli
,
both
befor
and
after
hi
chang
of
heart
.

Though
clearli
not
for
all
tast
,
thi
film
is
bold
and
dare
.

The
subject
matter
is
ugli
,
cruel
,
and
at
time
hard
to
look
at
.

Nevertheless
it
subject
ar
part
of
human
's
great
face
.

Kay
give
us
a
good
look
at
thi
fascin
,
if
distast
,
American
subcultur
.

