Star
Peter
Roach
,
Meri
Steven
,
Joe
Kaczkowski
,
Robb
Sherman
,
and
Kevin
Flower
I
do
n't
box
with
kid
glove
.

I
do
n't
plai
nice
,
I
'm
not
a
nice
gui
,
and
I
never
,
ever
,
go
easi
on
a
film
.

I
consid
it
to
be
a
breech
of
some
sort
of
code
of
ethic
for
a
movi
critic
.

Howev
,
I
do
some
favor
,
and
these
often
come
in
the
form
of
point
that
I
hand
to
certain
group
due
to
the
artist
braveri
.

Rigormorti
,
the
product
compani
that
ha
been
my
prime
exampl
of
how
monei
doe
not
need
to
motiv
filmmak
,
get
sever
of
these
point
each
time
.

Thei
recent
sent
me
a
VHS
copi
of
their
Down
with
America
trilogi
-LRB-
which
begin
,
quit
wittili
,
with
a
disclaim
that
thei
ar
not
try
to
undermin
America
with
the
make
of
thi
film
.
-RRB-

and
I
decid
to
spend
an
hour
of
my
dai
watch
it
.

Well
,
I
do
have
some
regret
,
but
that
is
not
the
point
in
the
previou
sentenc
.

The
point
of
it
wa
that
Down
with
America
wa
a
film
that
,
from
a
critic
standpoint
,
did
not
entir
disappoint
me
.

Sure
,
the
riski
us
of
VHS
instead
of
Super
8mm
or
16mm
wa
a
pain
,
and
the
natur
light
wa
on
of
the
most
annoi
thing
about
public
access
film
,
but
the
movi
itself
wa
fairli
enjoy
.

Down
with
America
concern
a
govern
agent
,
needless
murder
,
and
a
book
contain
everyth
from
the
Unabomb
's
Manifesto
to
the
1995
Apple
Comput
Profit
Report
.

Like
the
previou
film
of
Rigormorti
that
I
have
review
,
it
displai
an
off-kilt
humor
and
intellig
it
succe
in
make
me
laugh
where
countless
studio
film
fail
.

The
best
wai
to
see
thi
film
would
be
as
a
parodi
of
the
countless
conspiraci
film
that
we
have
been
drown
in
sinc
the
paranoia
of
the
80
.

A
dy
movement
from
the
dai
thei
start
,
these
paranoid
`
thriller
'
had
the
govern
alwai
cover
up
someth
and
had
the
same
favorit
word
:
`
Roswel
.
'

In
Down
with
America
,
the
line
`
Roswel
'
is
highli
absent
.

With
an
about
ten
minut
run
time
,
Down
with
America
effortlessli
sidestep
everi
cliché
that
the
conspiraci
film
fell
into
,
make
joke
at
them
at
the
same
time
,
and
provid
us
with
funni
and
memor
charact
.

Again
,
I
have
seen
movi
that
have
gone
on
two
hour
with
charact
I
could
n't
care
less
about
.

The
film
,
as
previous
state
,
concern
a
Feder
Agent
-LRB-
Peter
Roach
-RRB-
,
an
obsess
librarian
-LRB-
Meri
Steven
-RRB-
,
a
Mysteri
Man
-LRB-
Joe
Kaczkowski
-RRB-
,
and
two
peopl
obsess
with
silenc
in
the
librari
-LRB-
Robb
Sherman
,
Kevin
Flower
-RRB-
.

The
plot
:
a
book
contain
the
secret
of
all
anarchist
is
hidden
in
A
PUBLIC
LIBRARY
where
it
can
be
view
by
all
.

From
there
we
go
into
a
delight
parodi
.

The
Feder
Agent
claim
hi
sovereign
right
to
alter
the
truth
,
the
librarian
goe
on
a
diatrib
about
the
sanctiti
of
book
.

We
spend
our
time
laugh
at
fairli
idiot
joke
that
ar
perform
much
too
well
consid
the
lack
of
coach
of
the
cast
.

Although
the
actor
and
actress
ar
in
small
role
and
give
a
whole
new
mean
to
`
no-nam
'
,
it
end
up
be
the
no-nam
peopl
who
do
a
good
job
,
deliv
better
perform
as
comic
villain
than
half
the
crap
that
Hollywood
turn
out
.

For
onc
,
I
do
n't
have
a
URL
that
I
know
offhand
to
give
you
as
to
where
to
locat
the
film
onlin
.

I
can
onli
sai
that
you
should
find
my
previou
review
of
L'Auto
and
Le
X-File
and
look
up
the
Rigormorti
Product
site
in
and
of
itself
.

It
's
almost
as
much
fun
as
the
film
.

