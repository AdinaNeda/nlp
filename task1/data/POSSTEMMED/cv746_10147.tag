-LRB-
Dimens
Film
,
``
Scream
2
''
's
distributor
,
ha
ask
press
to
sai
extrem
littl
--
if
anyth
--
about
the
film
's
twisti
plot
.

That
's
no
easi
task
consid
the
wit
that
deserv
to
be
mention
here
,
but
I
will
do
my
best
to
be
vagu
-LRB-
now
,
there
's
a
first
-RRB-
.
-RRB-

``
The
first
on
wa
-LSB-
good
-RSB-
,
but
all
the
rest
suck
,
''
said
a
cinematically-savvi
teen
in
last
winter
's
We
Craven
thriller
``
Scream
,
''
her
statement
refer
to
the
film
of
the
``
Nightmar
on
Elm
Street
''
seri
but
realli
put
down
franchis
overkil
in
gener
.

The
comment
certainli
carri
clout
:
For
everi
truli
great
sequel
,
there
appear
to
be
a
coupl
of
dud
,
make
on
wonder
if
writer
ar
better
off
stick
sole
with
fresh
idea
.

But
like
it
or
not
,
along
come
``
Scream
2
.
''

And
believ
it
or
not
,
it
's
a
doozi
--
a
slick
,
sinist
,
madli
subvers
good
time
at
the
movi
,
as
intent
on
send
up
Hollywood
's
sequel
syndrom
as
much
as
it
prequel
poke
fun
at
slasher
convent
.

``
Scream
2
''
is
definit
that
rare
movi
thing
--
a
follow-up
that
can
stand
along
side
it
origin
with
pride
.

It
's
been
two
year
sinc
a
pair
of
overzeal
horror
movi
fan
clad
in
Edvard
Munch-esqu
get-up
carv
their
wai
through
the
young
populac
of
Woodsboro
,
California
.

Those
surviv
the
ordeal
have
gotten
on
with
their
live
.

Plucki
heroin
Sidnei
Prescott
-LRB-
Neve
Campbel
-RRB-
is
a
drama
student
at
the
Midwestern
Windsor
Colleg
;
her
pop
culture-whiz
pal
Randi
Meek
-LRB-
Jami
Kennedi
-RRB-
ha
tag
along
.

Trash
tabloid
report
Gale
Weather
-LRB-
Courtenei
Cox
-RRB-
ha
written
a
best-sel
base
on
their
ordeal
,
the
basi
of
which
ha
been
turn
into
a
-LRB-
veri
bad
-RRB-
movi
call
``
Stab
.
''

And
Dewei
Rilei
-LRB-
David
Arquett
-RRB-
,
still
suffer
from
wound
inflict
dure
``
Scream
,
''
ha
left
hi
job
as
a
polic
offic
for
a
while
.

Life
is
tranquil
...
at
least
for
a
while
.

Sever
sudden
murder
bring
Sidnei
,
Randi
,
Gale
and
Dewei
togeth
again
,
but
with
suspici
ey
cast
on
each
other
and
most
of
those
in
their
surround
--
if
these
four
peopl
learn
anyth
from
the
past
,
it
's
to
trust
no
on
.

Thu
,
the
possibl
victim/potenti
killer
list
includ
said
quartet
,
as
well
as
:
Derek
-LRB-
Jerri
O'Connel
-RRB-
,
Sidnei
's
new
beau
;
Cici
-LRB-
Sara
Michel
Gellar
-RRB-
,
a
chatti
soror
gal
;
Joel
-LRB-
Duan
Martin
-RRB-
,
Gale
's
cameraman
who
's
not
too
thrill
with
her
blood-soak
past
;
Halli
-LRB-
Elise
Neal
-RRB-
,
Sidnei
's
sassi
roommat
;
Debbi
-LRB-
Lauri
Metcalf
-RRB-
,
a
local
report
who
give
Gale
some
not-too-friendli
competit
;
and
Mickei
-LRB-
Timothi
Olyphant
-RRB-
,
Randi
's
good
friend
and
fellow
film
student
.

Cotton
Weari
-LRB-
Liev
Schreiber
-RRB-
,
the
man
Sidnei
wrongli
accus
of
her
mother
's
murder
in
Scream
,
also
show
up
on
campu
--
but
why
?

Like
the
first
Scream
,
Craven
and
screenwrit
Kevin
Williamson
inaugur
thing
with
a
bang
.

Thi
time
,
it
's
an
extend
sequenc
that
find
an
African-American
coupl
-LRB-
Jada
Pinkett
and
Omar
Epp
-RRB-
attend
a
sneak
preview
of
``
Stab
,
''
make
sly
refer
about
everyth
from
Sandra
Bullock
to
the
lunchmeat
non-rol
of
black
in
slasher
cinema
.

Befor
tragedi
ensu
at
the
screen
,
the
movie-within-a-movi
setup
allow
for
plenti
of
jokei
moment
,
includ
a
scene
from
``
Stab
''
which
turn
Scream
's
now-fam
Drew
Barrymor
prologu
on
it
ear
.

-LRB-
``
B
--
,
hang
up
the
phone
and
Star-69
hi
a
--
!
''

Pinkett
yell
at
the
screen
.
-RRB-

It
's
funni
,
creepi
stuff
-LRB-
I
do
n't
think
I
'll
ever
feel
safe
in
a
movi
theater
again
-RRB-
that
effect
foreshadow
the
ratio
of
smart
scare
and
spoofi
laugh
down
the
road
.

Another
flawless
exampl
of
thi
is
a
riotou
cell
phone-assault
of
the
killer
on
Randi
,
Dewei
and
Gale
in
broad
daylight
;
it
build
comed
and
end
with
genuin
terror
becaus
of
the
emot
invest
made
on
these
delici
charact
.

There
ar
other
superb
set
piec
,
but
explain
them
risk
ruin
their
effect
.

There
ar
more
player
thi
time
around
as
to
provid
both
a
higher
number
suspect
and
a
stagger
bodi
count
.

It
's
someth
of
a
trade-in
--
the
new
charact
ar
too
larg
in
number
to
be
as
fully-develop
as
the
origin
``
Scream
''
gang
,
but
thi
movi
pack
in
know
perform
-LRB-
especi
by
Kennedi
and
Cox
-RRB-
and
more
death
for
your
dollar
.

It
can
be
argu
that
,
while
the
origin
might
outclass
it
by
a
tad
,
Scream
2
is
both
scarier
and
funnier
.

It
certainli
doe
n't
skimp
with
shock
--
a
major
on
be
the
kill
of
on
cast
member
veri
near
and
dear
to
my
heart
-LRB-
I
actual
question
Craven
and
Williamson
's
judgment
here
,
consid
how
much
thi
person
ha
ad
to
these
film
-RRB-
.

Sometim
you
wish
that
more
could
have
been
done
with
Stab
--
the
two
scene
we
're
shown
ar
dead-on
--
but
for
film
fan
,
there
ar
plenti
of
subtl
nod
to
movi
like
``
The
Usual
Suspect
,
''
``
Alien
''
and
``
The
Empire
Strike
Back
.
''

Still
,
``
Scream
2
''
doe
so
mani
thing
right
,
it
's
petti
to
quibbl
.

It
penchant
for
parodi
is
irresist
,
discuss
of
the
merit
of
film
sequel
and
all
.

The
attract
,
solid
cast
is
a
definit
bonu
.

And
the
denouement
-LRB-
while
not
as
shock
as
``
Scream
''
's
-RRB-
is
priceless
;
the
film
's
final
revel
incorpor
bit
of
a
particular
horror
film
promin
refer
to
in
``
Scream
''
with
on
of
the
best
bad
gui
motiv
ever
put
on
paper
.

All
thi
and
a
``
Scream
3
''
is
in
the
work
.

Know
the
seri
'
tendenc
for
self-refer
,
perhap
on
of
it
charact
will
again
discuss
the
concept
of
sequel
justic
.

