``
The
Deep
End
''
us
a
color
palett
of
rich
earth
tone
surround
by
a
vast
arrai
of
blue
.

Allure
,
attract
blue
,
but
also
ici
and
forebod
.

After
take
in
their
beauti
,
the
ey
seek
out
the
reassur
brown
and
green
,
like
a
tire
swimmer
search
for
the
safeti
of
land
.

The
product
work
that
wai
as
well
it
pull
you
in
initi
as
a
crisp
,
thoroughly-modern
thriller
,
with
clip
exchang
between
defiantli
non-commun
charact
.

But
the
real
draw
of
the
film
is
the
submerg
human
of
two
kei
player
as
``
The
Deep
End
''
gradual
reveal
the
melodramat
core
beneath
it
iron
candi
coat
.

SPOILER
ALERT
:
The
follow
reveal
the
basic
storylin
.

I
believ
the
plot
point
ar
incident
,
becaus
the
real
reward
come
in
explor
the
mindset
of
the
main
charact
,
but
still
,
you
should
proce
at
your
own
risk
.

With
her
husband
Tom
,
a
Naval
offic
,
often
awai
at
sea
,
Margaret
Hall
-LRB-
Tilda
Swinton
-RRB-
hold
down
the
fort
,
take
care
of
the
children
,
Beau
-LRB-
Jonathan
Tucker
-RRB-
,
Paig
-LRB-
Tamara
Hope
-RRB-
,
and
Dylan
-LRB-
Jordon
Dorranc
-RRB-
,
while
her
father-in-law
Jack
-LRB-
Peter
Donat
-RRB-
hover
in
the
background
,
alwai
watch
.

The
stori
open
with
Margaret
leav
the
famili
home
in
Lake
Taho
to
storm
a
male
strip
club
in
Reno
.

It
seem
that
Beau
,
her
eldest
,
is
have
an
affair
with
Darbi
Rees
-LRB-
Josh
Luca
-RRB-
,
a
disreput
sort
12
year
hi
senior
.

Margaret
demand
that
Darbi
leav
her
son
alon
,
onli
to
have
the
man
smirk
at
her
and
announc
he
'll
be
glad
to
steer
clear
of
Beau
for
$
5000
.

Back
at
home
,
she
tri
to
talk
about
it
all
with
her
son
,
but
the
kid
wo
n't
even
confirm
that
he
's
gai
,
let
alon
discuss
the
issu
she
ha
with
hi
boyfriend
.

He
doe
,
howev
,
get
attent
when
she
reveal
Darbi
's
$
5,000
offer
,
even
as
he
maintain
hi
sullen
demeanor
.

Late
in
the
even
,
Darbi
show
up
outsid
,
toss
pebbl
at
Beau
's
window
like
a
schoolchild
.

The
two
meet
by
the
water
at
the
boathous
,
where
someth
import
happen
.

I
wo
n't
reveal
what
;
suffic
to
sai
we
becom
awar
of
a
signific
fact
that
Margaret
doe
not
.

All
of
thi
happen
in
the
first
few
minut
of
the
movi
,
by
the
wai
.

Within
a
dai
or
so
,
Margaret
's
life
becom
even
more
complic
when
a
suav
man
name
Alec
Goran
Visnjic
-RRB-
turn
up
with
an
explicit
videotap
document
the
sex
life
of
Beau
and
Darbi
.

He
and
hi
associ
ar
will
to
destroi
the
tape
,
onc
Margaret
cough
up
$
50,000
.

END
SPOILERS
Written
,
direct
and
produc
by
Scott
McGehe
and
David
Siegel
-LRB-
``
Sutur
''
-RRB-
,
``
The
Deep
End
''
is
base
on
Elisabeth
Sanxai
Hold
's
1940
's
novel
,
``
The
Blank
Wall
,
''
which
wa
also
the
sourc
of
the
1949
film
,
``
The
Reckless
Moment
.
''

I
've
neither
read
the
book
nor
seen
the
movi
,
but
I
understand
that
the
filmmak
chang
some
detail
.

In
Hold
's
stori
,
the
``
scandal
''
relationship
is
between
an
older
man
and
Margaret
's
teen-ag
daughter
.

Switch
the
gender
make
the
tale
seem
more
contemporari
,
and
it
also
rais
question
about
Margaret
's
motiv
.

Mani
review
of
``
The
Deep
End
''
describ
Margaret
as
a
mother
feroci
protect
her
son
,
but
I
wonder
.

Consid
thi
:
Margaret
live
in
a
meticul
kept
upscal
home
.

Her
husband
is
absent
most
of
the
time
and
seem
to
be
regard
as
a
larger-than-lif
figur
.

Her
father-in-law
is
constantli
present
,
almost
as
if
he
is
monitor
for
hi
son
.

And
when
her
husband
doe
call
,
Margaret
is
care
not
to
tell
him
about
anyth
disturb
that
is
go
on
with
the
famili
.

So
is
Margaret
a
mother
who
accept
her
son
's
sexual
orient
and
is
simpli
try
to
shield
the
young
man
from
a
lover
she
fear
will
take
advantag
of
him
?

Or
is
she
caretak
of
a
museum
of
upper
middl
class
complac
,
desper
to
maintain
the
statu
quo
so
that
everyth
will
be
postcard
perfect
when
the
old
man
return
from
the
sea
?

You
sort
it
out
.

Regardless
,
the
reson
of
``
The
Deep
End
''
come
from
two
actor
.

Goran
Visnjic
,
best
known
from
the
TV
drama
``
ER
,
''
is
strike
as
a
blackmail
unlik
ani
you
've
seen
befor
.

He
engag
Margaret
in
a
mesmer
danc
of
protocol
,
chivalri
and
simmer
erotic
.

But
the
star
of
thi
show
,
without
a
doubt
,
is
the
remark
Tilda
Swinton
.

Watch
the
shade
,
depth
,
resourc
,
vital
and
underst
sexual
she
give
Margaret
and
try
to
imagin
the
film
without
her
.

Dai
after
my
second
view
of
``
The
Deep
End
,
''
I
can
still
vividli
pictur
her
negoti
with
villain
,
try
to
connect
with
her
son
and
dive
into
ici
blue
water
.

``
The
Deep
End
''
is
an
except
film
and
Tilda
Swinton
is
the
best
part
of
it
.

