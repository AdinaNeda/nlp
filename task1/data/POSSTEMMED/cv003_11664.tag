``
Jaw
''
is
a
rare
film
that
grab
your
attent
befor
it
show
you
a
singl
imag
on
screen
.

The
movi
open
with
black
,
and
onli
distant
,
alien-lik
underwat
sound
.

Then
it
come
,
the
first
omin
bar
of
compos
John
William
'
now
infam
score
.

Dah-dum
.

From
there
,
director
Steven
Spielberg
wast
no
time
,
take
us
into
the
water
on
a
midnight
swim
with
a
beauti
girl
that
turn
deadli
.

Right
awai
he
let
us
know
how
vulner
we
all
ar
float
in
the
ocean
,
and
onc
``
Jaw
''
ha
attack
,
it
never
relinquish
it
grip
.

Perhap
what
is
most
outstand
about
``
Jaw
''
is
how
Spielberg
build
the
movi
.

He
work
it
like
a
theatric
product
,
with
a
first
act
and
a
second
act
.

Unlike
so
mani
modern
filmmak
,
he
ha
a
great
deal
of
restraint
,
and
refus
to
show
us
the
shark
until
the
middl
of
the
second
act
.

Until
then
,
he
mere
suggest
it
presenc
with
creepi
,
subject
underwat
shot
and
William
'
music
.

He
's
build
the
tension
bit
by
bit
,
so
when
it
come
time
for
the
climax
,
the
shark
's
arriv
is
truli
terrifi
.

He
doe
n't
let
us
get
bore
with
the
imageri
.

The
first
act
open
with
Polic
Chief
Martin
Brodi
-LRB-
Roi
Scheider
-RRB-
,
a
New
York
cop
who
ha
taken
an
easi
,
peac
job
run
the
polic
station
on
Amiti
Island
,
a
fictiti
New
England
resort
town
where
there
ha
n't
been
a
murder
or
a
gun
fire
inyear
.

The
island
is
shaken
up
by
sever
viciou
great
white
shark
attack
right
befor
the
Fourth
of
Juli
,
and
the
mayor
,
Larri
Vaughn
-LRB-
Murrai
Hamilton
-RRB-
,
doe
n't
want
to
shut
down
the
beach
becaus
the
island
is
reliant
on
summer
tourist
monei
.

Brodi
is
join
by
Matt
Hooper
-LRB-
Richard
Dreyfuss
-RRB-
,
a
young
,
ambiti
shark
expert
from
the
Marin
Institut
.

Hooper
is
as
fascin
by
the
shark
as
he
is
determin
to
help
Brodi
stop
it
--
hi
knowledg
about
the
exact
work
of
the
shark
-LRB-
``
It
's
a
perfect
engin
,
an
eat
machin
''
-RRB-
make
it
that
much
more
terrifi
.

When
Vaughn
final
relent
,
Hooper
and
Brodi
join
a
crusti
old
shark
killer
name
Quint
-LRB-
Robert
Shaw
-RRB-
on
hi
decrepit
boat
,
the
Orca
,
to
search
for
the
shark
.

The
entir
second
act
take
place
on
the
Orca
as
the
three
men
hunt
the
shark
,
and
inevit
,
ar
hunt
by
it
.

``
Jaw
''
is
a
thriller
with
a
keen
sens
of
humor
and
an
incred
sens
of
pace
,
tension
,
and
horror
.

It
is
like
ten
movi
all
roll
into
on
,
and
it
's
no
wonder
it
took
America
by
storm
in
the
summer
of
1975
,
take
in
enough
monei
to
crown
it
the
box
offic
champ
of
all
time
-LRB-
until
it
wa
unceremoni
dethron
in
1977
by
``
Star
War
''
-RRB-
.

Even
todai
,
fascin
with
thi
film
is
on
par
with
Hitchcock
's
``
Psycho
,
''
and
it
never
seem
to
ag
.

Although
grand
new
technolog
exist
that
make
the
technic
sequenc
,
includ
sever
mechan
shark
,
obsolet
,
none
of
it
could
improv
the
film
becaus
it
onli
would
lead
to
overkil
.

The
technic
limit
face
by
Spielberg
in
1975
mai
have
actual
produc
a
better
film
becaus
it
forc
him
to
reli
on
tradit
cinemat
element
like
pace
,
character
,
sharp
edit
,
and
creativ
photographi
,
instead
of
simpli
dous
the
audienc
with
digit
shark
effect
.

Scheider
,
Dreyfuss
,
and
Shaw
were
known
actor
at
the
time
``
Jaw
''
wa
made
,
but
none
of
them
had
the
draw
of
a
Robert
Redford
or
Paul
Newman
.

Nevertheless
,
thi
film
guarante
them
all
success
career
becaus
each
gave
an
outstand
perform
and
refus
to
be
overshadow
by
the
shark
.

Scheider
hit
just
the
right
note
as
a
sympathet
husband
and
father
caught
in
the
polit
quagmir
of
do
what
's
right
and
go
against
the
entir
town
.

``
It
's
your
first
summer
here
,
you
know
,
''
Mayor
Vaughn
warn
him
.

Dreyfuss
,
who
had
previous
been
seen
in
``
American
Graffiti
''
-LRB-
1973
-RRB-
and
``
The
Apprenticeship
of
Duddi
Kravitz
''
-LRB-
1974
-RRB-
give
a
surprisingli
matur
,
complex
perform
for
someon
who
had
liter
onli
plai
kid
and
teenag
.

Howev
,
most
outstand
is
the
gnarl
perform
by
Robert
Shaw
as
the
movi
's
Captain
Ahab
,
a
perform
sore
overlook
by
the
Academi
Award
.

Border
of
parodi
,
Shaw
plai
Quint
as
a
grizzl
old
loner
whose
machismo
border
on
masoch
.

He
's
slightli
derang
,
and
Shaw
's
perform
is
almost
a
caricatur
.

Howev
,
there
is
on
scene
late
in
the
film
,
when
he
and
Brodi
and
Hooper
ar
below
deck
on
the
Orca
compar
scar
.

Quint
is
drawn
into
tell
the
stori
of
hi
experi
aboard
the
U.S.S.
Indianapoli
,
a
Navi
ship
in
World
War
II
that
wa
sunk
by
the
Japanes
.

Hi
tale
of
float
in
the
water
for
more
than
a
week
with
over
1,000
other
men
while
swarm
of
shark
slowli
devour
them
them
is
actual
more
hair-rais
than
anyth
Spielberg
put
on
screen
.

Shaw
deliv
the
stori
in
on
long
take
,
and
it
is
the
best
act
in
the
film
.

Of
cours
,
we
ca
n't
leav
out
the
shark
itself
;
with
it
black
ey
,
endless
row
of
teeth
,
and
insati
urg
to
eat
,
it
is
basic
the
epitom
of
all
mankind
's
fear
about
what
is
unknown
and
threaten
in
natur
.

A
shark
is
such
a
perfect
nemesi
it
is
real
--
have
surviv
sinch
the
dinosaur
,
great
white
do
exist
,
thei
can
be
as
larg
as
the
shark
in
``
Jaw
,
''
and
thei
ar
a
threat
.

Everi
on
of
Spielberg
's
subject
underwat
shot
make
us
feel
queasi
becaus
let
us
see
how
we
look
to
the
shark
:
a
bunch
of
writih
,
dangl
,
complet
unprotect
leg
just
readi
to
be
chomp
into
.

The
shark
in
``
Jaw
''
wa
actual
a
combin
of
actual
footag
and
five
differ
mechan
shark
-LRB-
all
nicknam
``
Bruce
''
by
the
crew
-RRB-
built
to
be
shot
from
differ
angl
.

Mani
have
forgotten
,
but
``
Jaw
''
wa
a
sort
of
precursor
to
``
Waterworld
''
-LRB-
1995
-RRB-
,
a
movi
's
who
soggi
product
and
cost
overrun
had
Univers
Studio
worri
about
a
bomb
.

But
,
as
we
can
see
now
,
Spielberg
overcam
all
the
obstacl
,
and
deliv
on
of
the
finest
primal
scare-thril
ever
to
come
out
of
Hollywood
.

