Rate
R
-LRB-
some
sexual
and
relat
dialogu
,
some
graphic
languag
-RRB-
-LRB-
Note
:
There
ar
spoiler
regard
the
film
's
climax
;
the
elect
,
of
cours
-RRB-
We
see
Matthew
Broderick
,
a
man
torn
to
a
primal
state
;
he
's
been
unfaith
to
hi
wife
,
li
to
and
manipul
hi
student
,
and
by
the
same
token
thei
've
demean
hi
masculin
,
hi
self-respect
,
hi
desper
attempt
at
chang
the
world
.

And
yet
,
he
equat
the
caus
of
hi
pain
,
hi
torment
,
with
Traci
Flick
-LRB-
Rees
Witherspoon
-RRB-
.

No
matter
how
mani
student
have
come
and
gone
,
and
disappoint
him
as
an
educ
,
she
's
the
real
threat
.

About
to
give
in
,
and
divulg
that
she
's
won
by
onli
a
lone
vote
,
Broderick
's
McCalist
turn
in
defeat
,
see
Traci
's
euphor
celebr
in
the
outsid
corridor
and
sai
,
'
No
'
.

The
fact
that
he
simultan
lust
after
her
ideolog
further
illustr
that
Freudian
foundat
of
entitl
which
all
men
,
no
matter
how
obscur
,
have
in
their
relationship
;
a
tradit
expect
of
success
,
to
usurp
and
surpass
women
as
a
proverbi
industri
.

She
ca
n't
go
higher
than
him
.

He
wo
n't
allow
it
.

And
what
's
amaz
about
``
Election
''
is
that
everi
word
of
that
critic
is
drawn
from
a
rather
opaqu
metaphor
.

Earli
in
the
film
we
learn
that
Traci
wa
romant
,
and
then
sexual
,
involv
with
a
now
depart
teacher
.

It
's
like
Payn
and
Taylor
,
hi
co-screenwrit
,
have
taken
Traci
,
a
girl
desper
for
friendship
,
loyalti
,
and
almost
pervers
drawn
the
mytholog
pattern
of
kid
who
were
so
utterli
reject
by
their
peer
,
that
thei
found
sit
at
the
teacher
'
lunch
tabl
more
fit
,
to
an
unlik
extrem
with
her
as
the
protagonist
.

And
it
's
amaz
the
compass
that
we
have
;
it
seem
so
real
to
us
,
and
not
mere
becaus
it
's
happen
befor
,
splash
all
over
the
front
page
,
but
,
becaus
of
the
all
around
``
nice
''
persona
of
these
peopl
,
we
easili
dismiss
the
truli
wayward
deed
of
the
charact
.

It
's
with
Mandi
Barnett
's
``
If
You
'll
be
The
Teacher
''
plai
winsom
over
the
close
credit
that
Payn
skewer
the
tenet
of
hi
detractor
most
;
a
final
,
viscou
inject
of
bittersweet
ironi
.

Sexual
,
though
,
is
not
the
onli
basi
for
Payn
's
satir
,
or
it
success
.

It
also
make
a
tell
point
about
polit
,
friendship
,
and
class
boundari
.

Satir
is
requir
-LRB-
or
ought
to
be
,
anywai
-RRB-
to
take
itself
serious
,
or
act
so
,
while
the
audienc
doe
not
.

It
's
a
fine
line
to
straddl
,
but
some
film
ignor
it
altogeth
-LRB-
``
Drop
Dead
Gorgeou
''
leap
infuriatingli
to
mind
-RRB-
.

Payn
and
Taylor
hit
the
right
chord
;
their
charact
ar
real
peopl
:
neither
is
perfect
,
but
rather
both
of
the
lead
ar
flaw
,
misguid
individu
who
retain
,
somewhat
,
nobl
intent
at
heart
.

We
sympath
with
them
,
but
still
,
as
a
satir
,
their
idiosyncrat
behavior
,
and
their
wrongdo
,
is
taken
to
an
extrem
for
the
audienc
to
knowingli
chuckl
,
but
also
reflect
and
medit
,
about
.

Minu
half
a
point
,
though
,
for
Payn
admit
not
to
have
seen
``
Ferri
Bueller
's
Dai
Off
''
.

Whatev
.

