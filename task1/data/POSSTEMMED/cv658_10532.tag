Find
the
courag
to
face
life
full-on
is
a
difficult
task
.

Summon
up
the
audac
to
fall
in
love
is
a
harder
job
.

If
you
're
a
total
jerk
like
Melvin
Udall
is
,
the
odd
ar
even
more
against
you
.

Melvin
-LRB-
Jack
Nicholson
-RRB-
is
a
success
romanc
author
who
is
just
the
opposit
of
what
you
would
expect
someon
in
that
profess
to
be
.

A
solitari
man
hidden
awai
in
hi
New
York
apart
with
no
human
interact
and
not
even
a
hint
of
ever
have
been
in
a
passion
relationship
,
he
avoid
the
outsid
world
as
much
as
possibl
.

Hi
obsessive-compuls
disord
rule
hi
life
as
he
us
sever
differ
bar
of
soap
to
wash
hi
hand
and
refus
to
step
on
a
crack
on
the
sidewalk
.

-LRB-
There
's
a
great
scene
when
he
ha
to
cross
a
floor
made
of
mosaic
tile
--
_
very_smal
_
mosaic
tile
.
-RRB-

Melvin
ha
some
great
line
.

When
hi
publish
's
receptionist
ask
him
how
he
write
women
so
well
in
hi
book
,
he
respond
``
I
think
of
men
and
I
take
awai
reason
and
account
.
''

Do
n't
get
him
wrong
,
he
's
not
mere
a
misogynist
.

Set
hi
sight
higher
than
that
,
he
's
also
a
homophob
,
racist
,
anti-Semit
,
xenophob
,
animal-hat
and
intens
dislik
peopl
who
talk
in
metaphor
.

When
we
first
meet
him
,
he
's
busi
dump
a
neighbor
's
dog
down
the
garbag
chute
.

He
's
an
equal
opportun
bigot
.

He
doe
ventur
outsid
hi
apart
everi
dai
to
eat
breakfast
at
a
local
cafe
.

Melvin
is
such
a
grouchi
curmudgeon
that
waitress
Carol
Connelli
-LRB-
Helen
Hunt
-RRB-
is
the
onli
on
will
to
wait
on
him
.

And
she
's
none
too
happi
about
it
.

When
Carol
doe
n't
show
up
for
work
,
Melvin
's
routin
is
shot
and
he
's
determin
to
do
someth
about
it
.

Track
down
her
address
,
he
show
up
at
her
place
beg
her
to
come
back
to
work
so
that
he
can
eat
breakfast
.

The
fact
that
she
's
a
poor
singl
mother
care
for
a
chronic
ill
son
doe
n't
appear
to
phase
him
,
he
want
hi
food
.

Much
more
upset
is
that
hi
gai
artist
neighbor
Simon
Bishop
-LRB-
Greg
Kinnear
-RRB-
is
brutal
attack
dure
a
robberi
.

Thi
doe
n't
upset
Melvin
much
-LRB-
``
Do
n't
worri
,
you
'll
be
back
on
your
knee
in
no
time
''
-RRB-
but
when
he
is
bulli
into
take
care
of
Simon
's
dog
,
hi
life
is
turn
topsy-turvei
.

Melvin
is
sequest
becaus
he
's
afraid
of
what
's
out
there
on
the
other
side
of
hi
apart
door
.

He
ha
to
make
sure
it
's
ritualist
lock
three
time
to
keep
the
world
on
it
proper
side
.

When
he
bring
the
dog
home
he
reluctantli
grow
fond
of
it
,
talk
to
it
and
take
it
everywher
he
goe
.

Thi
littl
crack
in
hi
emot
armor
open
him
up
.

Hide
behind
hi
need
for
Carol
to
return
to
work
so
she
can
serv
him
,
he
pai
for
a
specialist
to
treat
her
son
.

He
even
develop
a
friendship
with
Simon
.

All
three
peopl
ar
heavili
damag
.

Melvin
ha
retreat
from
life
in
hi
apart
.

Carol
ha
devot
herself
to
her
son
,
convinc
that
she
ha
to
give
up
her
life
for
him
.

Simon
is
estrang
from
hi
parent
and
hi
friend
disappear
when
he
is
hospit
and
hi
monei
run
out
.

A
more
unlik
trio
to
form
relationship
,
you
'd
rare
see
.

And
in
the
midst
of
it
all
,
there
ar
some
wonder
moment
.

Melvin
is
hilari
in
hi
grouchi
and
touch
as
he
attempt
to
be
nice
.

``
You
make
me
want
to
be
a
better
man
,
''
he
tell
her
.

One
of
the
best
aspect
is
that
even
as
he
open
up
,
he
is
still
a
curmudgeon
.

Carol
,
afraid
that
hi
monetari
contribut
to
the
well
of
her
son
hide
a
hidden
agenda
rush
over
to
hi
apart
in
the
rain
to
explain
in
no
uncertain
term
that
she
will
never
sleep
with
him
.

It
onli
take
her
a
second
to
realiz
that
she
is
deliv
that
statement
while
look
as
if
she
just
left
a
wet
t-shirt
contest
.

There
's
hardli
anyth
left
to
sai
about
Jack
Nicholson
.

I
do
n't
rememb
ever
see
him
in
anyth
less
than
a
stellar
perform
and
hi
over-the-edg
Melvin
is
no
disappoint
.

Nicholson
mai
be
the
best
actor
work
todai
.

Hunt
doe
a
commend
job
match
Nicholson
's
energi
.

She
is
n't
afraid
to
be
seen
as
someth
less
than
a
babe
and
her
portray
of
exhaust
run-down
Carol
struggl
with
seemingli
overwhelm
oblig
is
top-notch
.

Their
on-screen
chemistri
is
a
bit
odd
.

Each
doe
a
wonder
job
with
the
charact
,
but
togeth
,
there
's
someth
a
littl
off
.

It
's
a
minor
problem
:
thei
do
work
well
togeth
,
but
that
miss
element
is
what
prevent
thi
from
be
a
four-star
film
.

Director
Jame
L.
Brook
-LRB-
``
Term
Of
Endearment
''
,
``
Broadcast
New
''
-RRB-
ha
a
movi
that
is
pick
up
award
by
the
buckets
-LRB-
nomine
for
all
the
top
Golden
Globe
,
winner
from
the
Nation
Board
Of
Review
,
inevit
Oscar
-RRB-
and
ha
a
winner
on
hi
hand
.

``
As
Good
As
It
Get
''
is
n't
exactli
the
qualiti
that
the
titl
sai
,
but
it
's
pretti
darn
close
.

