Star
Sylvest
Stallon
;
Amy
Brenneman
;
Viggo
Mortensen
;
Dan
Hedaya
&
Jai
O.
Sander
At
first
glanc
,
Daylight
would
seem
like
your
typic
disast
movi
.

On
second
glanc
it
look
that
wai
too
.

Although
,
as
typic
disast
movi
go
,
Daylight
is
n't
that
bad
.

Admittedli
,
it
seem
to
rip
off
a
great
deal
of
it
plot
devic
from
disast
flick
of
dai
gone
by
-LRB-
think
Poseidon
Adventur
-RRB-
.

But
the
decent
dialogu
and
surprisingli
good
act
make
up
for
the
``
have
n't
I
seen
thi
scene
somewher
befor
?
''

feel
that
creep
up
all
too
often
dure
the
film
.

Sylvest
Stallon
star
as
the
recent
fire
head
of
New
York
's
Emergenc
Respons
team
.

By
sheer
luck
,
he
is
in
the
right
place
at
the
right
time
when
disast
strike
the
unawar
inhabit
of
thi
littl
film
.

``
What
disast
is
that
?
''

you
ask
,
as
you
inch
ever
closer
to
the
edg
of
your
seat
.

Glad
you
ask
.

Through
a
whole
sequenc
of
entir
probabl
event
-LRB-
about
as
probabl
as
hit
the
lotteri
-RRB-
a
larg
explos
take
place
in
the
Lincoln
tunnel
run
under
the
Hudson
River
in
New
York
Citi
.

The
explos
result
in
the
tunnel
collaps
at
both
end
,
which
caus
not
onli
massiv
loss
of
life
,
but
trap
a
dozen
peopl
insid
the
wreckag
of
the
tunnel
.

Good
old
Sly
,
who
is
onli
feet
awai
from
enter
the
tunnel
when
it
collaps
,
offer
hi
servic
to
the
rescuer
,
who
gladli
accept
it
.

Sly
then
enter
the
tunnel
through
a
vent
shaft
-LRB-
think
Judg
Dredd
-RRB-
which
he
can
onli
go
in
,
and
not
out
off
,
and
proce
to
try
to
rescu
those
trap
insid
.

So
begin
the
fun
,
but
the
fun
ha
to
be
quick
,
becaus
thi
tunnel
ha
start
to
spring
a
leak
.

The
act
is
about
what
you
would
expect
of
a
Stallon
movi
.

Not
superb
by
ani
mean
,
but
far
abov
some
of
hi
other
film
-LRB-
think
Judg
Dredd
again
-RRB-
.

Daylight
's
act
is
probabl
averag
,
or
a
littl
abov
,
in
term
of
thi
sort
of
genr
,
star
thi
particular
star
.

Amy
Brenneman
is
the
onli
actor
of
ani
note
,
and
she
doe
a
pretti
good
job
as
Stallon
's
reluct
helper
.

Oh
yeah
,
Stallon
's
real
life
son
,
Sage
,
is
in
thi
too
.

Sage
--
nice
name
.

I
guess
it
could
be
wors
,
he
could
have
been
name
nutmeg
or
parslei
.

Note
to
Sage
--
you
ar
still
young
.

Go
to
school
and
get
a
good
educ
--
you
do
n't
have
a
futur
in
act
.

The
plot
run
along
the
same
line
as
the
act
--
interest
,
but
predict
.

Mostli
due
to
the
fact
that
it
wa
pretti
well
recycl
from
a
dozen
other
action
flick
.

The
biggest
problem
with
a
film
of
thi
natur
is
that
thei
try
to
cram
too
mani
thing
into
the
movi
.

As
soon
as
the
cast
clear
on
seemingli
imposs
hurdl
,
anoth
on
seem
to
pop
right
up
,
even
wors
than
the
last
on
.

I
realiz
that
it
would
n't
be
much
of
a
movi
otherwis
,
but
it
doe
make
the
movi
a
bit
predict
.

Adding
to
the
predict
is
the
cast
of
charact
,
which
of
cours
,
ha
to
includ
at
least
on
or
two
difficult
peopl
who
want
to
do
thing
their
own
wai
and
think
the
qualifi
gui
-LRB-
in
thi
case
Stallon
-RRB-
doe
n't
know
what
he
is
do
.

Why
is
it
in
disast
movi
we
can
never
have
everyon
agre
with
each
other
and
all
work
togeth
?

Would
thi
be
too
much
to
ask
?

Ca
n't
we
all
just
get
along
?

Special
effect
were
n't
bad
--
but
I
've
certainli
seen
better
.

Some
of
the
scene
,
while
fun
to
watch
,
were
such
blatant
rip-off
that
you
had
to
wonder
if
script
from
old
movi
were
just
copi
verbatim
.

Especial
all
of
the
scene
that
involv
anyth
underwat
-LRB-
the
tunnel
,
of
cours
,
start
to
flood
-RRB-
.

I
realli
expect
Gene
Hackman
,
Roddi
McDowel
and
Ernest
Borgnin
to
appear
at
ani
minut
--
I
wa
n't
sure
if
I
wa
watch
Daylight
or
The
Poseidon
Adventur
.

I
suppos
if
you
have
to
rip
off
a
movi
for
good
idea
,
The
Poseidon
Adventur
is
a
good
on
to
us
.

The
underwat
sequenc
-LRB-
asid
from
the
deja
vu
-RRB-
were
veri
well
shot
and
work
fairli
well
.

Thei
were
the
best
part
of
the
movi
by
a
long
shot
.

Better
than
averag
Stallon
flick
.

I
realiz
that
's
not
sai
much
when
the
man
ha
Judg
Dredd
on
hi
resum
.

But
certainli
not
up
to
the
standard
of
Cliffhang
.

Daylight
is
a
fun
movi
,
and
as
long
as
you
ar
n't
expect
a
whole
lot
you
might
even
be
pleasantli
surpris
.

