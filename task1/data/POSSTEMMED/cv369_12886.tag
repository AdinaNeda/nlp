Sometim
you
just
have
to
tip
your
hat
to
a
film
.

Sometim
you
just
have
to
jump
on
the
bandwagon
and
enjoi
the
ride
.

I
saw
THE
TRUMAN
SHOW
with
an
audienc
full
of
teenag
,
most
of
whom
were
no
doubt
drawn
by
Ace
Ventura
himself
and
hope
to
see
hi
latest
take
on
fart
joke
.

Surpris
,
thei
mai
have
been
,
when
thei
realiz
that
thi
pictur
actual
had
someth
to
sai
.

But
by
the
attent
of
the
crowd
I
'd
sai
thei
were
won
over-a
wa
I.
I
ca
n't
tell
yet
if
Truman
realli
is
that
good
,
or
if
it
onli
seem
that
good
in
comparison
to
the
aw
mountain
of
crap
spew
forth
from
the
bowl
of
Hollywood
so
far
thi
year
.

Time
alon
will
make
that
call
.

But
by
the
end
of
the
film
we
were
all
on
Truman
Burbank
's
side
.

We
were
concern
for
hi
well-b
,
and
want
him
to
win
.

That
we
felt
as
we
did
is
a
tribut
both
to
the
film
and
to
Jim
Carrei
,
who
ha
achiev
legitimaci
at
last
.

THE
TRUMAN
SHOW
is
a
film
best
view
cold
,
with
as
littl
foreknowledg
as
possibl
about
the
plot
.

Unless
you
're
under
a
media
blackout
,
howev
,
you
probabl
know
the
basic
.

Carrei
star
as
Truman
Burbank
,
a
Capra-esqu
Everyman
insur
agent
live
with
hi
beauti
wife
Meryl
-LRB-
Laura
Linnei
-RRB-
in
the
South
Florida
island
town
of
Seahaven
,
which
look
like
Michael
Eisner
's
idea
of
the
perfect
American
small
town
.

Truman
would
seem
to
live
the
perfect
middl
class
lifestyl
,
complet
with
a
work
wife
,
a
best
bud
,
friendli
neighbor
and
interest
co-work
.

There
is
tragedi
in
hi
past-hi
father
drown
in
a
horribl
boat
accid
,
leav
Truman
with
a
dread
fear
of
water
,
and
of
travel
in
general-but
overal
,
life
is
good
.

But
then
on
dai
as
Truman
leav
hi
hous
for
work
,
a
klieg
light
fall
magic
from
the
sky
.

Thi
curiou
event
mai
lead
Truman
to
discov
what
the
rest
of
the
world
alreadi
know
:
that
he
is
a
prison
on
the
world
's
biggest
soundstag
,
that
hi
wife
,
friend
and
rel
ar
all
actor
paid
to
interact
with
him
,
and
that
hi
life
ha
,
for
over
10,000
dai
,
been
broadcast
as
the
world
's
most
popular
televis
program
.

What
make
the
first
hour
of
TRUMAN
so
enjoy
to
watch
is
the
magic
verisimilitud
painstakingli
construct
by
Niccol
and
Weir
.

The
soundstag
,
we
're
told
,
is
``
onli
the
second
man-mad
structur
visibl
from
space
.
''

Truman
's
movement
ar
track
ceaselessli
by
5,000
camera
scatter
throughout
the
town
:
button
cam
,
dashboard
cam
,
mirror
cam
,
wife
cam
,
big
cam
and
small
cam
.

Weir
cut
the
film
so
we
're
never
quit
sure
if
we
're
watch
Truman
through
the
camera
of
director
Weir
or
through
the
camera
of
the
show
's
omnipot
creator
Christof
-LRB-
Ed
Harri
-RRB-
who
overse
the
show
from
a
control
room
built
into
the
man-mad
moon
in
Seahaven
's
sky
.

The
second
act
offer
a
plethora
of
clue
for
Truman
as
to
the
truth
of
hi
exist
.

Hi
best
friend
Marlon
-LRB-
Noah
Emmerich
-RRB-
alwai
ha
a
six
pack
of
beer
with
him
.

Hi
wife
seem
to
exist
in
an
etern
commerci
in
which
she
is
alwai
endors
the
latest
hot
household
product
.

He
see
the
same
peopl
walk
in
the
same
set
pattern
all
over
town
.

One
wonder
why
he
did
n't
pick
up
on
ani
of
thi
earlier
,
but
Christof
ha
an
answer
for
thi
:
``
Peopl
tend
to
accept
the
realiti
with
which
thei
're
present
.
''

Nevertheless
,
Truman
begin
to
grow
restless
,
and
dream
of
escap
to
Fiji
,
where
an
old
colleg
sweetheart
-LRB-
Natasha
McElhone
-RRB-
supposedli
live
.

Thi
convinc
realiti
of
televis
as
progenitor
of
realiti
is
a
magic
and
provoc
concept
;
the
best
thing
Jim
Carrei
doe
is
stai
out
of
it
wai
.

By
underplai
Truman
,
he
allow
the
subtl
manipul
of
the
film
to
take
over
.

There
is
a
particularli
poignant
scene
in
which
Truman
confid
hi
fear
to
Marlon
,
and
Marlon
answer
with
best-friend
sincer
,
``
I
'd
gladli
step
in
front
of
a
bu
for
you
.
''

But
that
line
wa
fed
to
Marlon
through
an
earpiec
by
Christof
.

The
abject
cruelti
to
which
Truman
is
subject
to
hit
home-and
from
that
moment
,
we
're
on
hi
side
.

THE
TRUMAN
SHOW
deriv
much
of
it
success
from
plai
to
our
own
secret
paranoid
fantasies-haven
'
t
we
all
,
at
least
onc
,
doubt
our
place
in
the
world
,
doubt
the
sincer
of
those
closest
to
us
?

Ultimat
,
howev
,
the
pictur
rise
abov
it
own
artific
to
rais
some
real
question
about
the
relationship
of
humankind
and
our
creator
.

What
doe
God
realli
think
of
us
?

Doe
He
resent
our
abandon
of
paradis
?

Who
exactli
is
watch
us
,
up
there
in
the
sky
?

If
you
enjoi
the
pictur
as
much
as
I
did
,
credit
Weir
for
allow
the
magic
of
the
screenplai
to
work
.

As
for
Carrey-wel
,
Truman
Burbank
is
the
kind
of
role
that
Jimmi
Stewart
wa
born
to
plai
.

Carrei
is
no
Jimmi
Stewart
;
to
hi
credit
,
he
doe
n't
try
to
be
.

He
just
tri
to
feel
the
wai
you
or
I
would
,
if
we
suddenli
found
out
that
the
whole
world
wa
watch
us
.

