A
bunch
of
bad
gui
dress
up
as
Elvi
imperson
rob
a
Vega
casino
dure
a
Preslei
convent
.

The
boi
eventu
get
togeth
to
split
the
monei
,
but
as
plan
chang
,
double-cross
occur
,
deal
and
wheel
goe
down
and
the
crew
set
up
for
the
road
.

Who
's
on
the
up
and
up
,
who
's
the
real
bad
gui
and
who
's
gon
na
get
to
bang
Courtenei
Cox
ar
just
a
few
of
the
question
which
will
be
answer
by
the
rest
of
thi
movi
.

The
funnest
movi
that
I
've
seen
so
far
thi
year
!

It
's
got
style
,
it
's
got
tough
gui
talkin
'
shite
and
it
's
got
plenti
of
bullet
to
go
around
.

A
definit
hunk-a-hunk-a
burnin
'
adventur
!

A
gui
's
gui
movi
through
and
through
,
and
on
that
'll
set
your
jolli
to
high
if
you
dig
on
the
rough
stuff
.

It
's
not
veri
origin
,
it
goe
on
for
a
littl
too
long
,
but
Costner
and
Russel
make
it
happen
,
as
two
shite-kickin
'
Elvi
lover
,
who
do
n't
stop
till
thei
,
more
or
less
,
drop
.

Thi
movi
's
got
some
pretti
intens
shoot-out
scene
,
on
of
the
coolest
robberi
scenario
that
I
've
ever
wit
,
featur
sever
Elvise
with
gun
a
blazin
'
and
the
King
's
music
plai
in
the
background
,
and
a
decent
help
of
double-cross
,
sharp
dialogu
and
funni
moment
.

In
fact
,
even
though
the
cast
is
fill
with
mani
name
actor
-LRB-
B-actor
as
thei
might
be
for
the
most
part
-RRB-
,
most
of
them
do
their
bit
and
succe
well
in
pass
the
torch
onto
the
next
victim
.

But
the
true
glue
that
hold
thi
film
togeth
,
other
than
the
spirit
of
Elvi
himself
,
is
Costner
and
Russel
,
who
plai
their
badass
charact
to
the
T.
Sure
,
Russel
's
been
down
thi
road
befor
,
but
Costner
surpris
me
with
hi
nasti
.

I
guess
he
's
piss
at
all
the
box-offic
bomb
that
he
's
been
in
of
late
,
but
he
sure
plai
a
great
unstabl
Elvi
imperson
robber
here
.

In
the
end
,
the
bottom
line
with
thi
movi
is
have
fun
,
kickin
'
ass
and
make
sure
there
's
enough
bubblegum
around
to
chew
afterward
.

And
smoke
'em
if
you
got
'em
,
folk
!

There
is
also
thi
on
realli
great
show-down
scene
between
Costner
and
a
cop
,
that
'll
have
you
crackin
'
up
and
lovin
'
it
all
at
the
same
time
.

The
violent
scene
in
thi
movi
ar
violent
and
the
entir
film
is
wrap
in
a
soundtrack
that
'll
kick
your
ars
from
thi
end
of
the
theatr
to
the
next
.

It
should
actual
come
to
no
surpris
to
anyon
that
the
director
of
thi
movi
come
from
a
music
video
background
,
with
plenti
,
and
I
mean
plenti
,
of
camera
trick
,
cut
and
edit
,
but
for
thi
kind
of
flick
,
it
actual
work
.

The
film
is
not
perfect
though
,
it
start
off
with
a
pretti
big
bang
,
but
then
pull
a
WAY
OF
THE
GUN
on
us
,
and
slow
the
pace
down
somewhat
in
the
middl
,
while
charact
intermingl
and
dispers
.

I
wa
person
kept
in
the
game
despit
some
soft
spot
,
and
enjoi
the
overal
ride
for
what
it
wa
as
well
.

And
did
I
mention
that
Courtenei
Cox
look
mega-hot
in
the
movi
?

No
...
okai
,
well
I
just
did
!

I
coulda
done
without
her
whole
``
romanc
''
angl
,
but
to
be
honest
,
it
doe
n't
take
up
much
of
the
film
,
which
is
gener
cover
in
blood
,
explos
,
gut
and
Elvi
nod
.

Try
to
see
how
mani
``
insid
''
connect
to
the
King
you
can
guess
.

Of
cours
,
it
's
no
secret
that
I
am
quit
a
big
Elvi
fan
myself
so
pleas
take
thi
whole
review
with
that
grain
of
salt
in
mind
.

I
also
like
these
type
of
``
gui
''
movi
a
lot
,
and
even
though
the
film
doe
n't
bring
much
origin
to
the
tabl
,
it
doe
offer
a
pretti
coher
stori
,
some
nice
twist
and
turn
,
humor
and
a
big
ol'
help
of
whoop-ass
come
straight
from
Costner
and
Russel
's
respect
boot
.

Probabl
not
a
film
for
everyon
,
but
definit
for
those
who
enjoi
the
gun
,
the
charismat
bad
gui
and
a
fun
,
if
entir
dispos
,
adventur
.

Now
see
how
mani
Elvi
song
I
've
us
to
pun
my
wai
through
thi
phoni
review
below
.

THE
ELVIS
SONGS
ALSO
RECOMMEND
THIS
MOVIE
!

So
whether
you
're
lonesom
tonight
or
ani
other
night
,
rememb
to
pack
the
teddi
bear
with
bullet
,
leav
your
littl
sister
at
home
,
bring
your
bossa
nova
babi
and
rock
out
of
that
jailhous
,
caus
thi
movi
ai
n't
on
to
return
to
the
sender
.

I
realli
beg
of
you
,
whether
you
're
stuck
in
the
ghetto
or
cry
in
the
chapel
,
to
slap
thi
cinemat
ring
around
your
neck
and
love
it
tender
.

And
no
suspici
mind
either
,
folk
,
caus
I
got
stung
by
thi
movi
,
I
mean
it
realli
shook
me
up
,
and
unless
you
're
the
devil
in
disguis
himself
,
I
think
you
wo
n't
be
abl
to
help
fall
in
love
with
thi
lucki
charm
either
.

So
do
n't
be
cruel
,
do
n't
turn
awai
,
do
n't
curl
up
with
your
latest
flame
tonit
-LRB-
whether
she
be
a
hard-head
woman
or
not
-RRB-
and
surrend
to
the
hound
dog
of
movi
that
is
thi
film
.

It
's
now
or
never
,
folk
.

Trust
me
,
I
did
it
my
wai
and
the
heartbreak
hotel
of
crappi
movi
that
I
'd
seen
so
far
thi
year
is
far
behind
me
.

JoBlo
ha
now
offici
left
the
saniti
of
hi
mind
and
thi
build
.

Thank
you
...
thank
you
veri
much
!!

Where
's
JoBlo
come
from
?

