It
wa
a
raini
Fridai
afternoon
in
Columbu
when
I
persuad
a
friend
to
see
a
matine
perform
of
MST3K
:
TM
.

He
had
never
seen
ani
episod
of
the
show
,
and
I
have
watch
a
scant
few
,
due
to
it
unsoci
airtim
on
Comedi
Central
and
the
uneven
natur
of
mani
of
the
episod
.

For
those
of
you
not
familiar
with
the
premis
,
Dr
Clayton
Forrest
-LRB-
Beaulieu
-RRB-
wish
to
take
over
the
world
by
find
the
worst
film
ever
made
and
unleash
it
upon
an
unsuspect
public
.

To
achiev
thi
,
he
-LRB-
in
the
word
of
the
TV
seri
'
theme
,
which
is
miss
in
the
movi
-RRB-
``
bump
-LSB-
Mike
Nelson
-LRB-
Nelson
-RRB-
-RSB-
on
the
noggin
and
then
shot
him
into
space
''
,
and
is
monitor
Nelson
's
reaction
to
the
movi
he
is
forc
to
endur
.

Rather
than
succumb
to
the
sheer
aw
of
mani
of
the
movi
,
Nelson
spend
hi
time
make
wisecrack
with
the
help
of
hi
two
robot
companion
,
Tom
Servo
and
Crow
T.
Robot
.

The
format
of
the
show
consist
of
Nelson
,
Servo
and
Crow
make
their
comment
while
silhouet
against
the
movi
be
watch
,
and
break
everi
minut
or
so
for
segment
set
on
the
Satellit
of
Love
,
the
ship
on
which
our
hero
ar
maroon
.

Only
two
thing
ar
differ
in
the
movi
:
the
absenc
of
Forrest
's
sidekick
,
TV
's
Frank
,
and
the
slower
pace
of
the
joke
.

Thi
latter
chang
is
presum
deliber
to
avoid
the
view
audienc
miss
some
of
the
best
line
while
laugh
from
the
previou
joke
.

For
their
big
screen
out
,
the
produc
have
chosen
``
Thi
Island
Earth
''
,
a
1954
classic
,
and
on
of
the
first
SF
film
to
have
a
special
effect
budget
larger
than
the
averag
groceri
bill
.

Unfortun
for
that
film
-LRB-
but
make
it
ideal
MST3K
fodder
-RRB-
,
act
and
dialogu
appear
to
have
taken
a
back
seat
to
the
effect
which
,
by
todai
's
standard
,
ar
less
than
impress
.

Nelson
&
Co.
make
joke
about
everyth
from
Japan
's
domin
in
the
world
market
,
to
Star
Trek
,
to
the
state
of
disrepair
of
Seattl
's
Kingdom
,
and
most
of
them
work
.

Unfortun
,
the
segment
set
outsid
the
satellit
's
movi
theater
seem
out
of
place
and
ar
n't
particularli
funni
,
but
at
least
thei
're
fairli
short
.

The
big
question
about
thi
movi
though
is
:
Why
?

I
presum
it
wa
an
attempt
to
gain
a
larger
follow
to
keep
support
behind
the
seri
-LRB-
rumour
of
it
impend
demis
circul
for
some
time
befor
the
plug
wa
eventu
pull
a
few
month
ago
-RRB-
,
but
the
format
gain
noth
from
it
transit
to
the
big
screen
--
there
ar
no
special
effect
to
dazzl
you
,
no
action
sequenc
to
keep
you
on
the
edg
of
your
seat
,
and
no
us
of
digit
surround
sound
.

So
,
it
seem
pointless
to
spend
$
8
per
person
to
see
thi
movi
when
in
a
few
month
it
will
be
out
on
video
and
you
can
watch
it
for
$
3
,
and
not
have
to
sit
in
a
room
full
of
popcorn
addict
.

Nevertheless
,
MST3K
:
TM
provid
more
laugh-out-loud
opportun
than
ani
film
you
're
go
to
see
thi
year
,
and
I
thoroughli
recommend
it
to
anyon
with
a
puls
.

Given
it
uniqu
,
I
hesit
to
grade
it
against
other
film
,
but
it
fulfil
it
claim
and
so
in
the
class
of
``
unsubtl
comedi
film
whose
laugh
come
at
the
expens
of
bad
B-movi
''
it
doe
well
.

