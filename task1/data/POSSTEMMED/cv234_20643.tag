When
I
saw
the
trailer
for
``
The
Sixth
Sens
,
''
I
did
n't
expect
much
from
it
.

My
snap
judgment
wa
that
it
wa
a
watered-down
version
of
``
The
Shine
''
or
possibl
a
late-entri
in
the
glut
of
angel-rel
movies/TV
show
of
a
coupl
year
ago
.

But
all
the
buzz
motiv
me
to
give
it
a
shot
.

It
turn
out
to
be
on
of
the
most
satisfi
movi
experi
of
the
year
.

Bruce
Willi
plai
child
psychologist
Malcolm
Crow
.

At
the
begin
of
the
movi
,
he
is
admir
an
award
the
citi
ha
given
him
for
hi
work
,
but
he
is
soon
confront
by
Vincent
,
a
patient
Malcolm
fail
year
earlier
-LRB-
Donni
Wahlberg-form
New
Kid-in
an
impress
cameo
-RRB-
.

A
year
pass
,
and
Malcolm
is
treat
anoth
child
with
the
same
symptom
Vincent
displai
.

Cole
Sear
-LRB-
Halei
Joel
Osment
-RRB-
is
a
withdrawn
kid
who
is
call
``
Freak
''
by
the
other
boi
in
the
neighborhood
.

Weird
thing
seem
to
happen
around
Cole-hi
mother
-LRB-
Olivia
William
-RRB-
leav
the
kitchen
for
a
moment
and
return
to
find
everi
cabinet
and
drawer
open
even
though
Cole
ha
n't
moved-and
Cole
seem
to
know
thing
that
a
kid
hi
ag
should
n't
-
for
exampl
,
that
there
wa
onc
a
gallow
in
hi
school
.

He
steal
religi
icon
from
church
to
build
a
chapel
in
a
pup
tent
in
hi
bedroom
.

The
trailer
gave
awai
the
movi
's
first
major
plot
twist
.

If
it
had
n't
,
the
first
hour
of
the
movi
would
be
more
engross
becaus
we
would
n't
know
what
's
wrong
with
Cole
.

If
you
're
lucki
enough
to
have
not
seen
the
trailer
,
stop
read
thi
review
right
now
and
go
see
``
The
Sixth
Sens
.
''

Anywai
,
sinc
the
trailer
gave
it
awai
,
I
figur
it
's
fair
game
to
discuss
here
.

Once
Malcolm
gain
Cole
's
trust
,
Cole
reveal
hi
secret
:
he
see
ghost
everywher
.

Dead
peopl
wander
around
not
know
that
thei
're
dead
,
invis
to
everyon
except
Cole
.

Malcolm
gradual
begin
to
realiz
that
Cole
is
tell
the
truth
.

The
box
offic
success
of
``
The
Sixth
Sens
''
ha
gener
a
lot
of
press
about
how
horror
movi
rule
the
box
offic
thi
year
and
lot
of
theori
about
why
that
's
so
.

I
hate
to
burst
anybodi
's
bubbl
,
but
``
The
Sixth
Sens
''
is
n't
realli
a
horror
film
.

At
it
core
,
``
The
Sixth
Sens
''
is
a
touch
stori
of
how
a
troubl
kid
and
an
emotionally-scar
adult
help
each
other
to
move
past
their
problem
.

Some
scene
with
the
ghost
ar
wonderfulli
creepi
,
though
.

One
that
realli
got
to
me
:
the
ghost
of
a
boi
sai
to
Cole
,
``
I
'll
show
you
where
my
dad
keep
hi
gun
''
;
when
the
ghost
turn
,
we
see
the
bullet
wound
in
the
back
of
hi
head
.

Osment
give
the
best
perform
I
've
ever
seen
from
a
child
actor
.

It
's
hard
enough
to
find
a
grown-up
in
Hollywood
who
can
give
a
subtl
,
realist
portray
!

Although
Willi
get
top
bill
,
Cole
is
realli
the
protagonist
of
the
stori
,
and
Osment
truli
bring
the
complex
kid
to
life
.

If
onli
Willi
'
work
were
as
impress
.

Accustom
to
deliv
catchphras
rather
than
dialogu
,
Willi
is
stiff
and
awkward
as
Malcolm
,
hi
most
challeng
role
to
date
.

-LRB-
Thi
is
a
bad
omen
for
hi
next
movi
,
``
The
Stori
of
Us
,
''
which
chronicl
the
up
and
down
of
a
marriag
-RRB-
.

Writer/Director
M.
Night
Shyamalan
ha
a
light
,
poetic
touch
that
goe
to
the
heart
of
the
viewer
without
seem
sappi
or
trite
.

He
's
also
a
skill
storytel
,
as
the
movi
's
end
demonstr
.

Only
when
the
final
secret
is
reveal
do
you
realiz
that
Shyamalan
ha
been
hint
at
it
throughout
the
movi
.

Look
for
a
cameo
by
the
director
as
a
doctor
who
treat
Cole
.

Bottom
line
:
Osment
should
get
an
Oscar
nomin
,
and
you
should
go
see
thi
movi
.

