You
've
heard
all
the
hype
.

You
've
seen
all
their
faces-Natali
Portman
-LRB-
The
Profession
-RRB-
as
Queen
Amidala
,
Liam
Neeson
-LRB-
Schindler
's
List
-RRB-
as
Qui-Gon
Jinn
,
Ewan
McGregor
-LRB-
Trainspot
-RRB-
as
Obi-Wan
Kenobi
,
and
Jake
Lloyd
-LRB-
Jingl
all
the
Wai
-RRB-
as
young
Anakin
Skywalk
.

If
you
've
read
ani
review
,
you
've
also
probabl
heard
that
thi
movi
fail
to
live
up
to
the
magic
and
human
of
the
first
trilogi
.

You
also
mai
have
heard
that
thi
on
's
too
kiddie-friendli
,
and
doe
n't
have
enough
content
for
adult
.

The
effect
ar
stun
,
the
digit
creatur
ar
amazingli
realist
,
the
lightsab
duel
ar
amaz
,
and
Queen
Amidala
's
sumptuou
robe
ar
fit
to
be
worn
by
Queen
Elizabeth
.

But
there
's
someth
miss
here
,
and
it
is
n't
budget
or
effects-it
's
everyth
monei
*
ca
n't
*
bui
.

The
actor
struggl
as
best
thei
can
to
flesh
out
broad-strok
and
flat
charact
.

The
most
success
at
thi
is
Liam
Neeson
who
,
as
Qui-Gon
Jinn
,
a
Jedi
master
to
young
Obi-Wan
Kenobi
,
ha
quiet
digniti
and
a
wise
,
command
presenc
.

He
is
the
anchor
to
thi
movi
,
as
he
is
the
on
charact
who
Georg
Luca
appar
spent
some
time
flesh
out
.

Portman
's
Queen
Amidala
and
McGregor
's
Obi-Wan
Kenobi
do
n't
fare
nearli
so
well
.

Amidala
,
the
futur
mother
of
Luke
and
Leia
and
the
queen
of
a
peac
planet
be
invad
by
the
Trade
Feder
-LRB-
?!?
-RRB-
,
come
off
as
stoic
,
stilt
and
caricatur
-LRB-
it
appear
as
though
she
wa
a
Vulcan
Geisha
in
a
former
life
-RRB-
.

And
Obi-Wan
,
although
McGregor
make
him
veri
endear
,
ha
so
littl
screen
time
that
he
's
noth
but
a
Robin
to
Qui-Gon
Jinn
's
Batman
.

Howev
,
McGregor
doe
struggl
manfulli
to
infus
thi
surprisingli
small
support
role
with
a
spark
of
genuin
insight
and
human
,
and
he
doe
a
good
job
.

He
also
absolut
NAILS
Alec
Guiness
'
-LRB-
Obi-Wan
in
Episod
4-6
-RRB-
Scottish
accent
,
and
that
realli
make
hi
presenc
in
the
film
more
profound
than
it
might
have
been
otherwis
.

In
fact
,
Luca
seem
to
bank
on
that
preexist
knowledg
quit
a
bit
,
and
that
's
on
of
the
problem
with
thi
film
.

I
would
never
call
myself
a
``
Star
War
''
fan-especi
not
consid
what
it
mean
to
be
a
fanat
these
days-but
I
've
alwai
enjoi
the
film
along
with
everyon
els
in
America
.

Howev
,
if
you
ar
the
on
person
in
America
who
*
ha
n't
*
seen
the
film
at
all
or
even
recent
,
or
who
doe
n't
bother
to
brush-up
on
the
name
of
obscur
charact
,
you
mai
be
hopelessli
lost
.

For
exampl
,
on
of
the
film
's
main
-LRB-
nefari
-RRB-
charact
is
Senat
Palpatin
.

For
those
of
you
not
recent
steep
in
``
Star
War
''
trivia
,
Emperor
Palpatin
is
that
scari
hood
apparit
who
wa
Darth
Vader
's
master
in
``
Return
of
the
Jedi
''
and
``
Empire
Strike
Back
.
''

So
,
obvious
,
hi
appear
in
``
Phantom
Menac
''
is
meant
to
stir
some
echo
of
hi
later
role
in
the
seri
,
thu
make
hi
role
here
more
meaning
.

A
lot
of
the
movi
is
like
that
.

R2-D2
and
C-3PO
make
token
appear
,
and
it
's
obviou
that
Luca
is
bank
on
the
audienc
's
pre-exist
fond
for
them
...
becaus
he
doe
n't
do
much
to
add
to
it
in
ani
wai
.

Same
goe
for
Jabba
the
Hutt
,
who
doe
n't
look
as
much
mean
here
as
he
doe
corpul
and
lazi
.

Even
*
planet
*
make
foreshadow
guest
appear
in
thi
film-Tatooin
-LRB-
the
desert
world
where
Luke
grew
up
-RRB-
,
Corusc
-LRB-
the
cloud
citi
from
``
Empire
Strike
Back
''
-RRB-
,
and
Alderaan
-LRB-
Leia
's
home
planet-which
we
never
actual
see-that
get
blown
up
in
``
Star
War
''
-RRB-
show
up
or
ar
mention
briefli
in
pass
.

What
's
the
result
of
ty
so
mani
plot
point
and
charact
to
futur
film
?

The
answer
is
that
thi
on
seem
surprisingli
empti
.

There
's
no
meat-no
substance-that
make
you
want
to
love
the
charact
as
thei
ar
,
not
as
thei
will
be
.

Another
problem
is
that
there
ar
actual
*
too
mani
*
alien
charact
in
the
film
.

I
wa
distract
by
the
fact
that
two
of
the
main
evil
characters-strang
look
alien
who
look
like
Gila
monsters-had
mouth
that
bare
move
,
make
them
look
more
like
Muppet
than
actual
charact
.

And
Jar
Jar
Bink
,
a
silli
Roger
Rabbitish
amphibi
charact
,
is
intend
as
comic
relief
,
but
what
he
realli
becom
is
annoying-and
fast
.

Unlike
Chewbacca
and
C-3PO
,
who
got
their
human
-LRB-
if
you
'll
pardon
the
express
-RRB-
from
their
interact
with
the
human
charact
,
Jar
Jar
is
often
left
to
interact
mostli
with
other
of
hi
kind
,
at
time
make
the
movi
seem
like
noth
more
than
a
veri
expens
``
Teenag
Mutant
Ninja
Turtl
.
''

Furthermor
,
the
dialogu
is
often
stilt
and
corni
,
and
sometim
downright
infantil
,
therebi
render
some
charact
into
noth
more
than
bystand
to
the
plot
.

Oddly
,
thi
is
n't
just
*
my
*
judgment-Luca
ha
said
that
he
design
thi
film
for
kid
.

Well
,
he
's
done
a
good
job
.

The
hero
of
thi
film
,
of
cours
,
is
young
Anakin
,
but
it
wa
veri
hard
for
me
to
feel
ani
kinship
or
identifi
with
an
eight-year-old
boi
.

That
's
not
Jake
Lloyd
's
fault
,
though-h
doe
a
good
job
of
be
a
cute
kid
,
which
is
appar
all
Luca
ask
of
him
.

That
's
anoth
mistak
,
of
cours
,
becaus
the
cute
tyke
becom
Darth
Vader
.

In
my
opinion
,
there
wa
not
nearli
enough
foreshadow
of
Anakin
's
futur
evil
in
the
film
.

The
boi
is
all
blond
flow
hair
and
rosi
cheek
,
and
there
's
noth
more
than
a
spark
of
aggress
in
him
throughout
the
entir
movi
.

He
ha
a
dote
mother
-LRB-
Pernilla
August
,
make
her
first
English-languag
film
-RRB-
and
is
a
slave
to
a
gross
fly
gnome
on
Tatooin
.

But
the
onli
indic
the
audienc
get
that
thi
kid
is
n't
all
heart
and
rose
is
Yoda
's
hesit
in
allow
him
to
train
as
a
Jedi
under
Qui-Gon
Jinn
.

Hi
explan
?

``
Hi
futur
is
cloud
.
''

-LRB-
Warn
:
MAJOR
spoiler
ahead
.

Enter
at
your
own
risk
.
-RRB-

Of
cours
,
the
nobl
Qui-Gon
di
at
the
hand
of
Darth
Maul
,
a
scary-look
Sith
lord
who
excel
at
the
Jedi
art
,
but
ha
turn
to
the
Dark
Side
.

Thi
dude
ha
mayb
two
line
in
the
entir
movi
,
but
he
establish
hi
presenc
through
hi
amaz
move
with
hi
double-sid
lightsab
,
and
hi
scari
facial
makeup
.

Becaus
Qui-Gon
never
get
a
chanc
to
train
the
young
Anakin
in
the
wai
of
the
Forc
,
Obi-Wan
,
Qui-Gon
's
young
Padawan
apprentic
-LRB-
on
level
below
Knightdom
-RRB-
,
must
take
over
the
train
himself
.

So
,
at
the
end
of
the
movi
,
we
end
where
the
saga
actual
begins-with
Obi-Wan
Kenobi
and
hi
young
apprentic
,
Anakin
``
Darth
Vader
''
Skywalk
.

Did
thi
stori
need
to
be
told
?

I
would
sai
no
.

But
is
it
a
worthwhil
movi
to
see
?

Absolut
.

If
you
do
n't
enter
the
theater
with
Jedi-s
expect
,
and
you
simpli
want
to
be
treat
to
an
enjoy
visual
spectacl
,
then
thi
is
your
movi
.

The
Tatooin
pod
race
ar
a
triumph
of
effect
and
comput
anim
.

The
digit
background
on
some
of
the
planet
ar
an
astonish
sight
.

And
Yoda
and
young
Obi-Wan
ar
worth
see
for
their
origin
in
a
simpler
,
happier
time
.

Above
all
,
thi
is
a
fun
movi
.

Not
deep
,
not
meaning
,
and
not
profound
.

But
fun
.

Mayb
next
time
,
Luca
will
hire
Lawrenc
Kasdan
to
co-writ
the
script
,
and
the
gui
who
direct
``
Empire
''
will
direct
.

Becaus
if
Luca
doe
the
next
on
himself
,
it
will
be
lack
the
on
thing
it
need
the
most-potenti
.

