PULP
FICTION
,
Quentin
Tarantino
's
anxious
await
and
superb
follow-up
to
RESERVOIR
DOGS
,
is
absolut
and
without
a
doubt
progress
as
on
of
the
most
talk
about
,
love
,
and
hate
film
of
all-tim
.

In
all
fair
,
it
should
be
note
that
those
who
love
thi
film
greatli
outnumb
those
who
hate
it
.

As
if
RESERVOIR
DOGS
wa
n't
perfect
,
Tarantino
went
ahead
and
improv
on
perfect
by
creat
PULP
FICTION
,
a
fast-pac
,
hard
talk
,
roller
coaster
ride
of
a
movi
that
combin
violenc
and
extraordinari
dialogu
in
a
seemingli
perfect
wai
.

Everi
major
charact
in
thi
film
is
repres
by
a
terrif
act
perform
by
the
actor
.

To
begin
with
,
the
two
hit
men
:
Vincent
Vega
-LRB-
John
Travolta
-RRB-
and
Jule
Winnfield
-LRB-
Samuel
L.
Jackson
-RRB-
.

These
two
charact
work
togeth
perfectli
,
and
the
perform
by
each
actor
ar
nearli
perfect
.

Travolta
,
in
fact
,
is
still
try
to
get
out
of
the
Vincent
Vega
role
.

If
these
two
charact
were
not
enough
,
there
's
the
struggl
boxer
Butch
Coolidg
-LRB-
Bruce
Willi
-RRB-
,
the
two
small-tim
crimin
Pumpkin
-LRB-
Tim
Roth
-RRB-
and
Honei
Bunni
-LRB-
Amanda
Plummer
-RRB-
,
the
big
man
in
charg
Marsellu
Wallac
-LRB-
Ving
Rhame
-RRB-
and
hi
attract
young
wife
Mia
-LRB-
Uma
Thurman
-RRB-
,
and
the
``
problem
solver
''
himself
,
The
Wolf
-LRB-
Harvei
Keitel
-RRB-
.

These
ar
just
the
major
charact
.

Smaller
role
,
but
equal
terrif
,
ar
hand
to
Jimmi
-LRB-
Quentin
Tarantino
-RRB-
,
a
young
marri
man
who
get
caught
up
in
some
seriou
gangster
busi
,
Lanc
-LRB-
Eric
Stoltz
-RRB-
,
the
far-out
drug
dealer
,
as
well
as
Maynard
-LRB-
Duan
Whitak
-RRB-
and
Zed
-LRB-
Peter
Green
-RRB-
,
two
hillbilli
who
ar
the
men
``
behind
''
-LRB-
no
pun
intend
for
those
of
you
who
have
seen
thi
-RRB-
a
veri
disturb
scene
toward
the
end
of
the
film
.

The
film
interweav
three
stori
that
,
at
first
,
seem
to
be
noth
alik
but
later
prove
to
be
veri
critic
to
each
other
.

The
film
begin
with
Pumpkin
and
Honei
Bunni
in
a
small
coffe
shop
discuss
their
recent
robberi
and
hold-up
.

The
two
quickli
decid
to
hold
up
the
coffe
shop
,
unknow
of
what
li
ahead
.

After
thi
,
the
film
focus
for
some
time
on
Vincent
and
Jule
and
their
hit
man
lifestyl
.

Utiliz
the
film
unforgett
dialogu
,
the
two
men
discuss
such
thing
as
hamburg
and
foot
massag
befor
thei
bust
into
an
apart
to
terror
the
occup
.

The
two
men
were
sent
to
the
apart
to
recov
a
mysteri
briefcas
for
their
boss
,
Marsellu
Wallac
.

After
basic
toi
with
the
men
,
especi
the
unfortun
Brett
-LRB-
Frank
Whalei
-RRB-
,
Vincent
and
Jule
complet
their
mission
and
recov
the
briefcas
.

Thi
is
not
befor
a
man
jump
out
from
a
bathroom
with
a.
357
``
hand
cannon
''
,
shoot
at
the
two
gangster
.

He
somehow
complet
miss
them
,
lead
Jule
to
cite
divin
intervent
.

Thi
is
also
the
basi
for
more
of
the
now-fam
Tarantino
dialogu
.

The
film
then
shift
to
focu
on
boxer
Butch
Coolidg
and
hi
box
escapad
.

Butch
wa
order
to
take
a
fall
in
hi
latest
fight
,
but
when
he
doe
not
,
Marsellu
vow
to
find
him
,
wherev
he
is
,
and
make
sure
that
Butch
is
not
abl
to
enjoi
hi
new
fortun
.

Vincent
,
meanwhil
,
ha
the
honor
of
take
Marsellu
'
wife
,
Mia
,
out
on
a
date
while
the
big
man
is
out
of
town
.

Vincent
,
obvious
,
is
rather
nervou
about
thi
,
especi
after
hear
the
tale
about
a
man
who
wa
thrown
out
of
a
window
for
supposedli
give
Mia
a
foot
massag
.

After
their
date
,
which
seem
to
rather
smoothli
,
thing
take
a
turn
for
the
worst
at
the
hand
of
Vincent
's
high-pric
heroin
,
dub
a
``
monster
''
by
Lanc
.

Each
stori
continu
progress
,
and
new
and
amaz
event
occur
.

Eventual
,
the
stori
all
meet
each
other
in
the
end
,
with
Pumpkin
and
Honei
Bunni
hold
up
the
restaur
,
which
happen
to
contain
both
Vincent
and
Jule
.

PULP
FICTION
is
definit
on
of
the
best
film
of
the
decad
,
and
possibl
of
all-tim
.

Along
with
it
superb
dialogu
,
action
,
write
,
direct
,
and
act
,
thi
film
will
not
onli
latch
onto
you
,
but
it
prove
that
it
doe
not
want
to
releas
you
until
it
ha
sunk
it
claw
complet
into
you
.

And
with
most
fan
of
thi
movi
,
thi
is
prove
to
be
a
rather
easi
task
,
although
at
time
the
graphic
imag
,
violenc
,
and
languag
mai
seem
a
bit
overpow
.

Sure
,
Quentin
Tarantino
's
fame
wa
greatli
escal
by
thi
film
,
and
he
prove
not
to
be
on
who
will
fade
from
exist
veri
soon
.

