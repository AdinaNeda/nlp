I
rent
thi
movi
with
veri
high
hope
.

Thi
movi
got
prais
as
on
of
the
best
film
of
1998
,
and
unfortun
,
wa
not
as
good
as
I
hope
,
but
wa
still
veri
intrigu
and
thought
provok
.

First
of
all
,
the
cast
for
thi
film
is
great
.

Tobei
Maguir
and
Rees
Witherspoon
ar
the
futur
of
Hollywood
.

Thei
plai
their
role
as
the
Parker
brother
and
sister
perfectli
.

With
a
plot
as
unbeliev
as
thi
film
,
you
need
chemistri
and
honest
act
from
your
cast
.

Tobei
and
Rees
-LRB-
Bud
and
Mari
Sue
Parker
-RRB-
plai
their
role
perfectli
and
displai
their
charact
with
honesti
and
believ
.

William
H.
Maci
,
an
actor
I
have
grown
to
love
over
the
past
year
,
turn
in
anoth
great
perform
as
the
TV
dad
who
is
torn
over
hi
wife
,
a
``
color
''
person
and
hi
friend
,
the
``
non-color
''
on
.

He
also
displai
believ
and
must
displai
a
certain
lack
of
chemistri
with
hi
wife
.

He
doe
thi
perfectli
and
yet
again
show
why
he
should
be
on
of
the
top
actor
in
Hollywood
.

Joan
Allen
is
equal
great
as
Betti
Parker
,
the
ideal
TV
Mom
.

Her
charact
is
the
most
ambiti
.

She
is
veri
nave
,
and
must
displai
thi
well
to
make
her
charact
believ
and
she
doe
just
that
.

Her
lack
of
chemistri
with
Maci
,
which
the
role
call
for
,
and
her
develop
chemistri
with
Jeff
Daniel
is
terrif
,
and
if
Daniel
would
have
been
as
stellar
as
the
rest
of
the
cast
,
then
thi
film
would
even
been
even
better
.

As
said
befor
,
Jeff
Daniel
give
a
bad
perform
.

Hi
charact
displai
the
chang
go
on
throughout
the
town
,
yet
he
doe
not
displai
it
with
enough
convict
.

Hi
charact
is
veri
confus
and
he
is
not
veri
believ
.

Thi
film
could
have
been
elev
to
a
whole
new
level
had
Daniel
been
abl
to
give
a
better
perform
.

The
movi
is
veri
provoc
.

It
challeng
the
issu
of
racism
that
exist
in
the
past
and
that
still
exist
todai
.

I
realli
wa
not
expect
all
of
the
element
thei
brought
up
,
but
thei
pull
them
off
with
such
flair
that
it
wa
mind
blow
.

Thei
demonstr
the
racism
issu
without
a
hitch
,
but
do
it
in
a
slow
pace
.

The
onli
major
flaw
in
the
film
besid
the
perform
by
Daniel
is
length
.

Some
film
ar
not
meant
to
be
that
long
.

Pleasantvil
is
on
of
those
film
.

Thi
film
could
have
been
so
much
better
had
thei
not
move
so
slowli
to
it
.

First
,
thei
ar
amaz
by
the
implement
of
color
.

Then
thei
want
to
have
it
.

Then
thei
do
not
want
to
.

Then
the
war
ensu
between
the
color
and
non-color
.

Thi
is
all
strung
out
over
two
hour
,
which
wa
wai
too
long
.

Some
movi
need
a
lot
of
time
.

Save
Privat
Ryan
wa
a
littl
bit
under
three
hour
long
and
I
thought
it
could
have
us
more
time
.

Thi
film
is
onli
two
hour
long
,
and
seem
much
longer
.

Length
is
everyth
in
film
.

If
you
can
nail
down
the
perfect
time
and
pace
,
you
can
win
half
the
battl
.

Pleasantvil
can
move
at
a
good
pace
at
time
,
but
it
is
not
consist
enough
.

Overal
,
Pleasantvil
is
a
veri
pleasant
and
provoc
departur
from
the
predict
of
most
film
these
dai
.

I
went
in
expect
a
light-heart
film
about
chang
.

I
left
learn
an
import
lesson
about
racism
and
the
wai
thing
chang
.

Luckili
,
Pleasantvil
teach
thi
lesson
in
such
a
wai
to
make
it
appeal
to
all
audienc
.

Only
if
it
could
have
done
in
a
shorter
amount
of
time
.

