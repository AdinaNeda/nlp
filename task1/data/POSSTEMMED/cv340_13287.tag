Why
do
peopl
hate
the
Spice
Girl
?

What
exactli
have
thei
done
wrong
,
other
than
perhap
offend
the
Fashion
Polic
?

So
what
if
thei
ar
not
``
great
''
singer
--
thei
ai
n't
sing
opera
,
folk
,
it
pop
music
!

Who
exactli
IS
a
``
great
''
singer
?

Judg
by
that
open
paragraph
it
is
obviou
that
I
like
the
Spice
Girl
.

Thei
do
what
thei
do
--
sing
some
catchi
tune
--
and
thei
do
it
pretti
well
.

Thei
ar
abl
to
carri
that
into
their
first
film
,
``
Spice
World
''
.

``
Spice
World
''
certainli
is
n't
a
great
filmgo
experi
...
what
it
is
what
the
Spice
Girl
ar
all
about
:
fun
,
song
,
and
a
lot
of
``
Girl
Power
!
''

Draw
from
a
number
of
differ
film
-LRB-
like
the
Beatl
'
``
Hard
Dai
's
Night
''
-RRB-
``
Spice
World
''
chronicl
the
week
head
up
to
the
Girl
'
first
concert
at
London
's
Albert
Hall
.

Thei
get
to
tool
around
London
in
a
big
ol'
bu
-LRB-
driven
by
Meat
Loaf
!
-RRB-

and
gener
have
fun
.

In
order
to
through
a
semblenc
of
plot
into
the
film
,
there
is
an
evil
tabloid
publish
plai
by
Barri
Humphri
,
better
known
to
American
,
for
aboutminut
,
as
Dame
Edna
-LRB-
rememb
her
?
-RRB-

He
want
to
sabotag
the
Girl
'
concert
so
he
can
sell
paper
.

Of
cours
,
the
point
in
a
movi
like
``
Spice
World
''
is
n't
plot
.

The
center
of
the
film
is
the
Spice
Girl
themselv
:
Ginger
-LRB-
the
recent
depart
Geri
Haliwel
-RRB-
,
Posh
-LRB-
Victoria
Adam
-RRB-
,
Sporti
-LRB-
Melani
Chisolm
-RRB-
,
Scari
-LRB-
Melani
Brown
-RRB-
,
and
Babi
-LRB-
Emma
Bunton
-RRB-
.

Each
on
ha
their
own
person
and
,
perhap
the
best
part
of
the
film
,
look
like
thei
ar
have
some
honest-to-good
fun
,
someth
that
ca
n't
be
said
for
a
lot
of
peopl
in
film
todai
.

Along
for
the
ride
ar
Richard
E.
Grant
as
their
manag
,
Clifford
,
and
variou
other
charact
make
quick
cameo
appear
-LRB-
includ
teriff
British
actor
like
Richard
Brier
and
Stephen
Fry
-RRB-
.

And
,
everi
onc
and
a
while
,
Roger
Moor
pop
up
as
The
Chief
to
spout
odd
koan
-LRB-
ad
bonu
:
he
get
to
,
in
the
word
of
Craig
Kilborn
,
danc
,
danc
,
danc
.
-RRB-

Watch
thi
film
wa
fun
,
for
the
most
part
.

There
ar
a
few
slow
segment
,
but
overli
,
director
Bob
Spier
keep
thing
move
at
a
quick
pace
,
with
plenti
of
hummabl
Spice
Girl
song
in
the
background
.

Hi
camerawork
is
fairli
pedestrian
,
though
the
final
concert
number
is
done
quit
well
with
effect
us
of
edit
.

The
look
of
the
film
promot
the
high
fun
,
with
the
design
of
the
SpiceBu
be
quit
origin
and
eye-catch
.

Add
in
wall-to-wal
Spice
Girl
music
and
thi
film
is
lovabl
romp
.

If
you
do
n't
like
the
Spice
Girl
,
you
wo
n't
chang
your
mind
with
thi
film
.

But
,
if
you
find
them
at
least
toler
,
get
readi
to
sit
back
and
chill
becaus
the
Spice
Girl
ar
here
to
entertain
you
...
and
do
it
QUITE
well
.

``
Spice
World
''
-LRB-
1998
-RRB-
.

Direct
by
Bob
Spier
.

Written
by
Kim
Fuller
.

Music
by
the
Spice
Girl
.

With
Geri
Halliwel
,
Victoria
Adam
,
Melani
Chisolm
,
Melani
Brown
,
Emma
Bunton
,
Richard
E.
Grant
,
Clair
Rushbrook
,
Roger
Moor
.

Distribut
by
Columbia
Pictur
.

Run
time
:
minut
.

Rate
:
PG
.

Availabl
on
Home
Video
.

--
29
June
1998
Visit
my
Film
Review
Multiplex
:
-LSB-
1
-RSB-
http://pages.prodigy.com/XWKZ01B/index.htm

