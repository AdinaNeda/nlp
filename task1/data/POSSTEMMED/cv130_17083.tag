Star
:
Jason
Schwartzman
,
Olivia
William
and
Bill
Murrai
Written
by
:
We
Anderson
&
Owen
Wilson
Direct
by
:
We
Anderson
While
watch
We
Anderson
's
Rushmor
,
it
mai
be
surpris
to
think
that
the
role
of
Max
Fischer
,
the
film
's
anti-hero
,
wa
not
written
with
Jason
Schwartzman
in
mind
.

The
young
actor
,
make
hi
film
debut
as
ayear
old
student
at
the
exclus
Rushmor
Academi
is
so
perfect
in
the
role
that
it
come
as
a
shock
that
Anderson
audit
some
two
thousand
other
prospect
star
befor
discov
Schwarztman
.

Schwartzman
plai
Max
,
a
student
who
is
more
interest
in
hi
extra-curricular
activ
-LRB-
such
as
write
edgi
plai
like
``
Serpico
''
and
be
presid
of
the
Rushmor
Beekeep
-RRB-
but
see
hi
grade
suffer
as
a
result
.

On
the
brink
of
be
thrown
out
,
he
land
himself
in
even
more
hot
water
when
he
fall
for
first
grade
teacher
Rosemari
Cross
-LRB-
Olivia
William
-RRB-
and
enlist
the
help
of
millionair
acquaint
Herman
Blume
-LRB-
the
great
Bill
Murrai
-RRB-
to
build
an
aquarium
-LRB-
on
Rushmor
's
basebal
field
-RRB-
in
her
honour
.

Unfortun
for
everyon
involv
,
Blume
find
that
he
ha
feel
for
Miss
Cross
as
well
,
which
get
in
the
wai
of
Max
's
plan
to
woo
her
.

Max
is
on
of
the
best-written
charact
we
've
yet
seen
in
the
90
,
and
Schwartzman
plai
him
to
perfect
in
on
of
the
finest
comedi
perform
ever
given
by
someon
of
hi
ag
.

As
everyon
els
ha
probabl
alreadi
point
out
,
he
is
reminisc
of
a
young
Dustin
Hoffman
-LRB-
although
I
wa
remind
more
of
Paul
from
``
The
Wonder
Year
''
,
from
a
visual
standpoint
,
at
least
-RRB-
.

Hopefulli
,
if
he
avoid
typecast
as
weird
,
quirki
loner-typ
he
should
earn
himself
a
good
career
.

Bill
Murrai
is
hi
equal
as
Blume
,
displai
a
depth
we
have
n't
yet
seen
from
him
.

While
thi
is
n't
hi
best
perform
-LRB-
that
honour
goe
to
Groundhog
Dai
-RRB-
he
is
still
terrif
here
.

William
is
n't
given
much
to
do
except
react
to
Schwartzman
and
Murrai
,
but
still
doe
fine
work
.

Rushmor
seem
to
go
just
a
littl
too
long
but
,
while
it
is
n't
a
perfect
film
,
it
is
a
defin
must-se
for
anyon
who
want
to
see
that
rariti
,
an
American
coming-of-ag
film
that
actual
work
well
without
an
excess
amount
of
sentiment
.

Anderson
ha
creat
a
modern
classic
with
a
star
who
should
,
with
ani
luck
,
rise
to
even
greater
success
.

