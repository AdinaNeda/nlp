OBSESSION
-LRB-
director/writ
:
Brian
De
Palma
;
screenwrit
:
from
an
origin
stori
by
De
Palma
&
Paul
Schrader/Paul
Schrader
;
cinematograph
:
Vilmo
Zsigmond
;
editor
:
Paul
Hirsch
;
cast
:
Cliff
Robertson
-LRB-
Michael
Courtland
-RRB-
,
Genevièv
Bujold
-LRB-
Sandra
Portinari
/
Elizabeth
Courtland
-RRB-
,
John
Lithgow
-LRB-
Robert
La
Sall
-RRB-
,
Sylvia
``
Kuumba
''
William
-LRB-
Maid
-RRB-
,
Wanda
Blackman
-LRB-
Amy
Courtland
-RRB-
,
J.
Patrick
McNamara
-LRB-
3rd
Kidnap
-RRB-
,
Stanlei
Rey
-LRB-
Inspector
Brie
-RRB-
,
Stocker
Fontelieu
-LRB-
Dr.
Ellman
-RRB-
;
Runtim
:
98
;
Columbia
;
1976
-RRB-
Thi
is
the
movi
not
the
perfum
.

A
slow
move
,
stylish
psycholog
thriller
in
imit
of
Hitchcock
's
Vertigo
.

It
is
adapt
from
an
origin
stori
by
Brian
De
Palma
and
Paul
Schrader
,
and
is
direct
by
De
Palma
and
script
by
Schrader
.

Set
in
New
Orlean
,
1959
,
it
show
the
wealthi
Michael
Courtland
-LRB-
Cliff
Robertson
-RRB-
sit
on
top
of
the
world
,
celebr
hi
tenth
wed
anniversari
with
a
parti
in
hi
mansion
,
as
hi
love
wife
Elizabeth
-LRB-
Genevièv
Bujold
-RRB-
and
young
daughter
Amy
-LRB-
Blackman
-RRB-
,
both
look
up
to
him
beam
with
joi
.

Hi
real-est
develop
partner
Bob
La
Sall
-LRB-
John
Lithgow
-RRB-
propos
a
toast
to
him
and
to
their
prosper
busi
,
while
hi
high
societi
friend
give
him
their
best
wish
.

Upon
retir
for
the
night
,
hi
daughter
and
wife
ar
kidnap
in
their
bedroom
and
he
is
left
with
a
ransom
note
to
deliv
$
500,000
tomorrow
or
thei
will
be
kill
.

Inspector
Brie
-LRB-
Stanlei
Rey
-RRB-
come
up
with
a
plan
to
put
in
phoni
monei
and
a
transmitt
in
the
briefcas
exchang
,
as
that
will
lead
them
to
the
kidnapp
.

But
the
kidnapp
burst
out
of
the
hous
surround
by
the
polic
,
take
the
two
victim
with
them
and
in
the
pursu
polic
chase
,
the
kidnapp
's
car
explod
and
goe
over
the
bridg
,
where
none
of
the
bodi
were
found
.

Griev
the
loss
of
hi
wife
and
child
,
the
guilt-ridden
Michael
build
them
a
tomb
on
the
expens
land
thei
were
to
develop
.

Sixteen
year
later
he
goe
with
hi
partner
Bob
on
a
vacat
and
he
revisit
Florenc
,
Itali
,
where
he
met
hi
wife
.

When
he
goe
to
the
church
where
thei
met
,
he
see
a
young
girl
who
look
exactli
like
hi
wife
.

Her
name
is
Sandra
Portinari
-LRB-
Genevièv
Bujold
-RRB-
and
she
's
work
there
restor
art
that
is
decai
.

He
fall
instantli
in
love
with
her
and
doe
n't
heed
Bob
's
warn
that
she
might
be
a
gold
digger
and
decid
to
take
her
back
to
New
Orlean
and
marri
her
.

Sandra
becom
hi
second
chanc
to
prove
hi
love
,
as
he
think
he
can
final
put
the
past
behind
him
.

The
surpris
to
come
were
n't
realli
all
that
surpris
,
as
the
beauti
in
the
storytel
is
almost
exclus
in
the
underst
act
by
the
soul
stricken
Cliff
Robertson
,
the
emotion
impact
perform
by
Genevièv
Bujold
,
and
the
compet
act
of
John
Lithgow
.

It
plai
too
much
like
a
rehash
of
the
Master
's
work
,
to
break
ani
new
ground
,
yet
it
is
still
thrill
in
it
own
wai
.

Denni
Schwartz
:
``
Ozu
'
World
Movi
Review
''
©
ALL
RIGHTS
RESERVED
DENNIS
SCHWARTZ

