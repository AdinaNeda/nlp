Carla
Gugino
graduat
from
high
school
and
instead
of
stai
in
her
small
farm
town
,
she
goe
to
colleg
in
California
.

There
she
meet
the
residenti
advisor
Pauli
Shore
,
who
is
a
citi
boi
.

When
Gugino
goe
back
home
for
Thanksgiv
,
she
bring
Shore
with
her
,
and
her
parent
-LRB-
Lane
Smith
and
Cindi
Pickett
-RRB-
ar
less
than
thrill
.

Dan
Gauthier
,
Gugino
's
boyfriend
from
high
school
,
propos
to
Gugino
.

Gugino
is
n't
readi
for
anyth
like
thi
so
she
make
up
the
stori
that
she
is
engag
to
Shore
.

She
and
Shore
have
to
make
it
look
like
thei
ar
engag
while
her
parent
try
to
make
it
end
.

Forget
JURASSIC
PARK
,
CLIFFHANGER
,
THE
FIRM
and
LAST
ACTION
HERO
,
SON-IN-LAW
is
the
film
to
see
thi
summer
.

SON-IN-LAW
is
a
great
romant
comedi
that
should
pleas
the
viewer
,
especi
fan
of
the
Wiez
.

When
SON-IN-LAW
is
long
gone
and
forgotten
,
the
blockbust
summer
film
will
still
be
plai
and
you
will
have
a
chanc
to
see
them
then
.

Thi
film
could
also
make
great
video
fare
for
those
that
ar
leeri
of
spend
seven
dollar
on
it
.

Noth
will
be
lost
by
watch
it
on
a
televis
screen
.

Although
the
basic
stori
line
ha
been
done
mani
,
mani
time
befor
,
the
film
is
still
fun
to
watch
.

The
laugh
ar
plenti
,
especi
when
the
Wiez
is
around
.

The
wai
some
of
the
joke
ar
done
ar
new
,
even
though
some
of
the
joke
ar
old
.

There
ar
also
sever
new
joke
that
,
at
least
,
I
have
n't
seen
befor
.

Still
,
when
present
right
,
even
old
materi
can
be
funni
still
.

Thi
film
is
just
basic
a
light
comedi
that
is
great
to
see
if
you
ar
in
the
right
mood
that
doe
n't
contain
much
violenc
or
ani
nuditi
that
is
definit
worth
full
ticket
price
.

The
cast
for
thi
film
doe
an
excel
job
.

Pauli
Shore
-LRB-
ENCINO
MAN
-RRB-
is
the
star
attract
of
thi
film
.

He
is
great
,
and
know
how
to
entertain
the
audienc
.

Pauli
Shore
just
seem
to
take
over
the
entir
screen
when
he
is
on
with
express
and
comment
that
ar
hilari
.

Thi
film
would
definit
not
have
been
as
good
with
anyon
els
in
thi
role
.

Carla
Gugino
doe
a
veri
good
job
as
the
farm
girl
gone
citi
girl
.

She
make
her
charact
believ
,
on
some
level
,
yet
interest
at
the
same
time
.

The
person
who
plai
the
grandfath
-LRB-
Mason
Adam
,
I
believ
-RRB-
also
doe
a
great
job
.

The
wai
he
deliv
hi
line
kept
me
laugh
throughout
the
time
that
he
wa
on
the
screen
.

Patrick
Renna
,
who
plai
Gugino
's
littl
brother
,
wa
a
littl
annoi
,
but
no
where
as
near
as
Macaulai
Culkin
,
and
for
onli
part
of
the
time
,
so
I
can
forgiv
him
.

Tiffani-Amb
Thiessen
-LRB-
A
KILLER
AMONG
FRIENDS
,
``
Save
by
the
Bell
''
-RRB-
doe
a
reason
good
job
for
the
time
that
she
is
on
screen
.

She
show
that
she
mai
have
a
potenti
in
movi
if
she
can
get
out
of
the
corni
young
adult
program
on
televis
.

She
wa
n't
given
overli
much
to
do
in
the
film
,
but
what
she
had
,
she
doe
a
good
job
with
.

