Cinemat
speak
,
Gordon
Park
'
origin
1971
_
Shaft
_
is
not
a
great
film
.

A
bit
slow
at
time
and
more
than
a
littl
rough
around
the
edg
as
it
build
to
it
climact
explos
of
violent
action
,
thi
spirit
but
formula
yarn
that
initi
brought
Ernest
Tidyman
's
``
black
privat
dick
that
's
a
sex
machin
to
all
the
chick
''
to
the
big
screen
hardli
qualifi
as
groundbreak
filmmak
.

Yet
nearlyyear
after
it
origin
releas
--
and
long
after
the
genr
it
spawn
,
``
blaxploit
,
''
di
with
that
decad
--
the
1971
_
Shaft
_
remain
an
extrem
entertain
watch
,
never
have
complet
escap
pop
cultur
conscious
.

The
reason
for
thi
is
the
same
on
that
explain
the
film
's
connect
with
moviego
far
beyond
the
target
African-American
audienc
:
the
titl
charact
of
John
Shaft
.

While
the
fact
that
the
strong
,
smart
,
viril
,
and
superbl
suav
Shaft
is
black
is
the
primari
factor
for
hi
histor
and
cultur
signific
,
hi
broad-bas
appeal
stem
from
an
idea
that
transcend
race
:
he
is
comfort
,
confid
,
and
proud
about
who
he
is
,
and
anyon
who
had
a
problem
with
that
could
simpli
kiss
hi
ass
.

Thi
fact
also
explain
why
John
Singleton
's
Y2G
reviv
of
John
Shaft
is
as
enjoy
as
it
is
.

Much
like
the
film
that
start
the
franchis
,
thi
_
Shaft
_
's
plot
doe
n't
score
point
in
the
origin
depart
,
but
the
energi
level
and
smooth
attitud
distinguish
it
from
standard
crime
thriller
.

Contrari
to
what
ha
been
report
over
the
past
few
month
,
thi
_
Shaft
_
is
not
a
remak
of
the
origin
film
,
but
more
of
a
sequel/spinoff
.

The
star
audienc
know
and
love
from
the
origin
film
and
it
first
two
sequel
-LRB-
1972
's
_
Shaft
's
_
Big_Scor
!

_
and
1973
's
_
Shaft_in_Africa
_
-RRB-
,
Richard
Roundtre
,
onc
again
plai
John
Shaft
,
who
still
run
a
privat
investig
firm
in
New
York
Citi
.

Howev
,
the
focu
of
the
film
li
on
hi
same-nam
nephew
-LRB-
Samuel
L.
Jackson
-RRB-
,
who
,
as
the
film
begin
,
is
a
cop
whose
take-no-crap
demeanor
constantli
leav
him
at
odd
with
hi
superior
.

When
a
privileg
young
man
name
Walter
Wade
Jr.
-LRB-
Christian
Bale
-RRB-
accus
of
a
brutal
,
racially-motiv
murder
is
allow
to
be
releas
on
bail
,
a
disgust
Shaft
leav
the
forc
and
decid
to
take
matter
into
hi
own
hand
as
a
P.I.
.

But
that
's
easier
said
than
done
,
for
also
stand
in
the
wai
of
Shaft
and
hi
wai
of
justic
is
Peopl
Hernandez
-LRB-
Jeffrei
Wright
-RRB-
,
a
Dominican
gangster
who
is
hire
by
Walter
to
rub
out
waitress
Dian
Palmieri
-LRB-
Toni
Collett
-RRB-
,
the
onli
eyewit
to
hi
crime
.

Peopl
--
or
,
rather
,
Wright
is
also
the
big
obstacl
in
Jackson
's
wai
toward
command
thi
film
.

Peopl
is
more
of
an
outrag
comic
charact
for
most
of
the
run
time
,
and
Wright
is
insan
funni
dure
these
stage
.

Howev
,
he
is
n't
so
funni
as
to
make
the
charact
come
off
as
goofi
and
buffoonish
,
and
Peopl
'
eventu
turn
to
more
seriou
villaini
is
seamless
and
believ
-LRB-
which
probabl
would
not
have
been
the
case
had
he
been
plai
by
origin
cast
choic
John
Leguizamo
,
who
bow
out
befor
film
-RRB-
.

It
's
no
easi
task
to
steal
a
film
from
the
Jackson
-LRB-
who
is
hi
usual
captiv
,
charismat
self
here
-RRB-
,
but
that
's
exactli
what
Wright
doe
--
and
make
it
seem
effortless
.

Then
again
,
with
such
a
talent
ensembl
surround
him
,
it
is
not
too
surpris
that
Jackson
's
impress
star
turn
doe
n't
quit
tower
over
the
rest
;
he
is
strongli
complement
not
onli
by
Wright
but
all
hi
other
co-star
.

Bale
ha
alreadi
proven
hi
abil
to
plai
an
uppiti
killer
in
_
American_Psycho
_
,
so
it
onli
follow
that
hi
perform
as
a
similar
,
less
exagger
charact
would
be
spot-on
.

Collett
lend
the
film
some
convinc
and
welcom
dramat
weight
as
the
frighten
,
conflict
Dian
.

Busta
Rhyme
bring
some
good
laugh
as
Shaft
's
sidekick
Rasaan
.

Regist
not
as
strongli
--
but
through
no
fault
of
their
own
--
ar
Vanessa
William
-LRB-
as
tough
narcot
cop
Carmen
Vasquez
-RRB-
and
a
dismayingli
underus
Roundtre
;
thei
simpli
ar
given
littl
to
do
in
the
script
credit
to
Richard
Price
,
Singleton
,
and
Shane
Salerno
.

-LRB-
Jackson
also
ha
littl
to
``
do
''
in
a
sens
;
some
throwawai
footag
dure
the
open
credit
asid
,
hi
Shaft
doe
n't
even
have
on
sex
scene
.
-RRB-

That
Singleton
onc
again
prove
hi
abil
with
actor
is
an
especi
good
thing
sinc
he
's
not
realli
an
action
director
.

Thi
is
not
to
sai
that
he
doe
a
bad
job
with
the
numer
gunfight
and
the
requisit
foot
and
car
chase
.

Thei
move
well
-LRB-
as
doe
the
film
as
a
whole
-RRB-
and
ar
reason
excit
;
it
's
just
that
there
's
noth
terribl
invent
about
them
.

These
set
piec
ar
function
in
the
wai
the
script
is
:
thei
work
well
enough
,
but
thei
're
unsurpris
and
convent
.

But
if
there
's
anyth
that
a
_
Shaft
_
movi
doe
well
,
it
's
make
the
familiar
look
cool
--
and
thi
_
Shaft
_
keep
that
tradit
aliv
.

From
the
slick
titl
sequenc
--
score
,
of
cours
,
to
Isaac
Hay
'
ever-infecti
Oscar-win
theme
song
,
which
Singleton
wise
sprinkl
throughout
the
film
--
on
,
the
film
look
great
and
easili
sweep
the
viewer
into
it
world
with
it
energet
bravado
.

Even
a
common
visual
trick
such
as
emploi
fanci
wipe
for
scene
transit
not
onli
feel
unforc
,
it
feel
necessari
.

Style
doe
n't
exactli
make
for
a
great
film
,
but
when
it
come
to
_
Shaft
_
,
that
's
of
littl
consequ
.

What
matter
abov
all
els
is
have
a
good
time
,
and
the
latest
_
Shaft
_
should
be
just
the
first
of
mani
fun
ride
to
be
had
with
thi
bad
mutha
--
shut
your
mouth
.

