CAPSULE
:
Trippi
,
hyperspe
action
machin
from
Hong
Kong
's
accomplish
Tsui
Hark
.

Van
Damm
and
Rodman
have
nice
chemistri
,
the
stunt
ar
eyepop
,
and
stuff
get
blow
up
real
good
;
what
more
do
you
want
?

I
admit
I
wa
all
set
to
loath
DOUBLE
TEAM
;
it
reek
of
cheapjack
timing-ori
market
-LRB-
stick
Denni
Rodman
in
a
movi
,
quick
,
while
he
's
hot
,
and
do
someth
about
Jean-Claud
Van
Damm
's
flag
career
while
we
're
at
it
!
-RRB-
.

Surpris
--
DOUBLE
TEAM
transcend
it
dumb
root
and
turn
out
to
be
a
mess
of
fun
.

Bring
some
friend
,
get
some
pretzel
,
and
have
a
blast
.

Van
Damm
is
Jack
Quinn
,
an
ex-ag
who
is
brought
back
in
for
``
on
last
mission
''
--
you
'd
think
ani
spy
worth
hi
shoe
phone
would
run
like
hell
when
he
hear
those
word
.

But
Van
Damm
's
charact
ha
a
pregnant
wife
who
's
also
a
sculptor
,
and
some
unpleas
pressur
get
us
to
get
him
to
come
through
on
thi
mission
.

He
's
been
assign
to
take
down
an
old
enemi
,
a
terrorist
name
Stavro
-LRB-
Mickei
Rourk
,
look
oddli
subdu
-RRB-
,
who
mai
be
back
up
to
hi
old
trick
.

In
the
first
showdown
between
Quinn
and
Stavro
,
the
movi
wear
it
ambit
proudli
on
it
sleev
:
nonstop
action
.

An
amus
park
,
a
hospit
,
a
privat
``
retir
spy
''
's
retreat
,
most
of
Rome
,
variou
hous
,
plane
,
car
and
other
mode
of
transport
,
and
the
Coliseum
-LRB-
!
-RRB-

all
becom
arena
for
some
of
the
most
bone-rattl
shoot-out
and
punch-out
film
.

Thei
ar
better
seen
than
describ
and
ar
reason
enough
to
see
the
film
:
on
jaw-drop
scene
ha
Van
Damm
take
on
a
man
who
us
a
switchblad
with
hi
*
feet
*
.

-LRB-
Van
Damm
is
a
good
,
dextrou
athlet
and
fighter
,
but
he
is
often
upstag
.
-RRB-

There
ar
other
nice
touch
.

Stavro
and
Quinn
both
want
the
same
thing
:
to
retir
in
peac
with
their
famili
.

That
ambit
ti
them
togeth
in
variou
wai
,
and
also
human
them
a
bit
.

One
of
the
thing
about
HK
action
movi
is
that
there
's
alwai
some
form
of
human
element
,
and
that
's
carri
over
into
thi
movi
as
well
.

It
give
weight
to
scene
that
would
otherwis
be
forgett
.

Also
good
is
Denni
Rodman
,
plai
a
weapon
dealer
name
Yaz
--
a
charact
who
stick
out
like
a
fist
of
broken
finger
and
who
is
funni
just
stand
there
.

Rodman
is
natur
on
screen
;
he
's
fun
to
watch
,
especi
when
sling
bad
gui
like
basketbal
,
and
deserv
to
get
a
movi
of
hi
own
,
base
on
what
's
seen
here
:
a
lightweight
,
fast-mov
entertain
that
showcas
all
of
it
piec
excel
.

