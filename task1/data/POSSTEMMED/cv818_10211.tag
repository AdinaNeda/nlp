It
ha
been
three
long
year
sinc
Quentin
Tarantino
stun
the
cinema
world
by
claim
the
Cann
Film
Festiv
's
Palm
D'Or
,
a
$
100
million-plu
box
offic
gross
,
and
an
Oscar
nomin
all
for
hi
sophomor
out
,
PULP
FICTION
.

Sinc
then
,
the
talent
film
maker
ha
been
virtual
invis
,
surfac
briefli
as
a
co-director
of
the
wildli
uneven
FOUR
ROOMS
and
the
screenwrit
of
the
gori
vampire-fest
,
FROM
DUSK
TILL
DAWN
.

In
between
,
he
ha
moonlight
as
an
``
actor
''
with
sever
decidedli
unmemor
perform
.

Now
,
with
much
fanfar
and
anticip
,
Tarantino
ha
return
with
hi
third
directori
effort
,
JACKIE
BROWN
.

And
,
while
thi
motion
pictur
,
adapt
from
Elmore
Leonard
's
novel
,
RUM
PUNCH
,
offer
solid
entertain
,
those
expect
anoth
bravura
out
from
Tarantino
will
leav
theater
disappoint
.

For
the
most
part
,
JACKIE
BROWN
is
a
pretti
ordinari
crime
movi
.

The
stori
,
which
start
out
slowli
,
develop
into
a
twisti
affair
,
with
double-cross
and
triple-cross
.

And
the
movi
is
litter
with
occasion
Tarantino
trademark
:
witti
dialogu
,
unexpect
gunfir
,
'
70
pop
tune
,
and
close-up
of
women
'
bare
feet
.

Yet
,
for
all
of
that
,
the
product
is
someth
of
a
letdown
.

The
sheer
,
in-your-fac
exuber
that
mark
RESERVOIR
DOGS
and
especi
PULP
FICTION
is
absent
.

The
mostly-straightforward
chronolog
of
JACKIE
BROWN
doe
n't
match
up
favor
to
the
non-linear
style
of
Tarantino
's
previou
effort
an
approach
that
ad
tension
and
edgi
to
the
narr
.

And
there
ar
n't
nearli
as
mani
delici
offbeat
convers
thi
time
around
.

There
's
a
Samuel
L.
Jackson
monologu
about
gun
,
a
Jackson/Chri
Tucker
argument
regard
the
merit
of
hide
in
a
car
trunk
,
and
a
Jackson/Robert
De
Niro
exchang
that
recal
some
of
the
Jackson/Travolta
materi
from
PULP
FICTION
,
but
that
's
about
it
.

JACKIE
BROWN
's
lone
``
innov
''
is
it
present
of
a
crucial
sequenc
from
three
differ
perspect
.

Thi
is
n't
exactli
an
origin
techniqu
it
ha
been
done
numer
time
befor
,
most
famous
in
Akira
Kurosawa
's
RASHOMON
and
most
recent
in
Edward
Zwick
's
COURAGE
UNDER
FIRE
.

Howev
,
while
in
those
two
movi
-LRB-
and
other
-RRB-
,
there
wa
a
legitim
plot
reason
for
the
multipl
points-of-view
,
Tarantino
's
sole
purpos
for
us
it
appear
to
be
becaus
it
's
unconvent
.

Had
the
scene
in
question
been
shown
from
onli
on
of
the
three
vantag
,
noth
would
have
been
lost
.

As
a
result
,
thi
aspect
of
the
film
is
littl
more
than
a
curios
.

For
the
second
pictur
in
a
row
,
Tarantino
is
attempt
to
reviv
the
career
of
a
'
70
icon
.

Thi
time
around
,
instead
of
John
Travolta
,
it
's
Blaxploit
queen
Pam
Grier
-LRB-
Foxi
Brown
ha
becom
Jacki
Brown
-RRB-
.

In
on
of
mani
nod
to
the
most
famou
segment
of
Grier
's
career
,
Tarantino
us
a
'
70
song
to
accompani
her
first
appear
dure
the
open
credit
.

There
ar
also
sever
instanc
dure
JACKIE
BROWN
when
the
director
offer
a
sly
wink
toward
certain
convent
of
the
Blaxploit
genr
-LRB-
although
Grier
never
doe
ani
butt-kick
-RRB-
.

Grier
is
Jacki
Brown
,
a
flight
attend
who
get
caught
transport
drug
and
monei
into
the
Unite
State
.

She
's
work
for
gun
dealer
Ordell
Robbi
-LRB-
Samuel
L.
Jackson
-RRB-
,
but
she
keep
her
mouth
shut
under
question
,
despit
pressur
from
Rai
Nicolet
-LRB-
Michael
Keaton
-RRB-
,
a
Feder
offici
.

No
longer
sure
whether
or
not
he
can
trust
Jacki
,
Ordell
arrang
for
a
bail
bondsman
,
Max
Cherri
-LRB-
Robert
Forster
,
TV
's
``
Banyon
''
-RRB-
to
post
the
necessari
$
10,000
,
then
plan
to
shoot
Jacki
if
she
prove
disloy
.

Jacki
pass
Ordell
's
test
,
howev
,
and
soon
the
two
of
them
ar
plot
a
wai
to
smuggl
$
500,000
of
Ordell
's
monei
into
the
Unite
State
without
tip
off
the
Fed
.

Soon
,
just
about
everyon
is
after
that
monei
,
includ
Jacki
,
Max
,
Rai
,
Ordell
's
perpetu
oversex
and
drugged-out
girlfriend
,
Melani
-LRB-
Bridget
Fonda
-RRB-
,
and
hi
right-hand
man
,
Loui
-LRB-
Robert
De
Niro
-RRB-
.

Unsurprisingli
,
the
most
memor
perform
is
turn
in
by
Samuel
L.
Jackson
,
but
Ordell
is
n't
nearli
as
invigor
or
compel
a
charact
as
Jule
from
PULP
FICTION
.

In
addit
to
look
fantast
,
Pam
Grier
is
also
quit
good
,
although
her
is
not
an
Oscar-calib
perform
-LRB-
although
she
might
get
a
nomin
-RRB-
.

Robert
Forster
and
Michael
Keaton
ar
solid
in
their
tough-guys-who-rarely-smil
role
.

Bridget
Fonda
is
around
for
three
discern
reason
:
to
look
sexi
in
a
bikini
,
to
provid
a
littl
twist
comedi
,
and
to
satisfi
Tarantino
's
foot
fetish
.

Robert
De
Niro
is
crimin
underus
in
a
part
that
could
have
been
plai
equal
well
by
ani
grungy-look
,
middle-ag
actor
.

The
film
,
which
clock
in
at
sever
minut
over
the
two-and-a-half
hour
mark
,
is
probabl
too
long
for
the
materi
,
but
the
plot
is
convolut
enough
to
keep
us
guess
throughout
-LRB-
although
the
payoff
is
a
letdown
-RRB-
.

Tarantino
keep
thing
move
along
nice
,
with
a
heavier
dose
of
humor
and
less
violenc
than
in
PULP
FICTION
,
but
,
on
the
whole
,
thi
movi
seem
more
like
the
work
of
on
of
hi
wannab
than
someth
from
the
director
himself
.

When
it
come
to
recent
caper
film
-LRB-
like
THE
GRIFTERS
and
BOUND
-RRB-
,
JACKIE
BROWN
is
a
second-ti
effort
.

It
's
an
entertain
divers
,
but
not
a
masterpiec
,
and
certainli
not
an
Oscar
contend
.

