In
1940
,
Walt
Disnei
releas
a
film
experi
.

FANTASIA
wa
a
blend
of
two
art
form
:
classic
music
and
anim
.

While
it
wa
not
immedi
popular
with
the
audienc
of
it
dai
,
it
ha
sinc
becom
recogn
as
someth
of
a
classic
.

Accord
to
the
program
note
,
it
wa
Mr.
Disnei
's
intent
to
have
FANTASIA
be
a
continu
work
in
progress
.

Sixti
year
later
,
thank
to
the
ongo
effort
of
Walt
's
nephew
,
a
new
FANTASIA
is
be
releas
.

It
wa
not
onli
worth
the
wait
,
it
is
evid
to
just
how
far
anim
ha
come
dure
the
past
half-decad
.

With
seven
new
sequenc
and
on
old
favorit
,
FANTASIA
2000
exhibit
an
imagin
and
respect
for
it
art
form
that
is
absent
from
most
other
featur
film
.

Like
the
origin
,
FANTASIA
2000
will
more
than
like
bore
the
veri
young
,
although
there
is
more
narr
appeal
in
thi
newer
version
and
the
segment
appear
to
be
substanti
shorter
,
perhap
a
nod
to
the
shorter
attent
span
of
the
studio
's
primari
audienc
.

Rang
from
the
silli
to
the
sublim
,
each
segment
is
preced
by
a
short
,
humor
introduct
or
interstiti
by
a
guest
celebr
host
,
Steve
Martin
-LRB-
BOWFINGER
-RRB-
be
the
first
and
most
effect
.

These
ar
quickli
forgotten
as
conductor
Jame
Levin
lead
the
Chicago
Symphoni
Orchestra
through
the
music
program
which
,
along
with
the
imag
drawn
by
the
Disnei
team
of
anim
,
is
thoughtfulli
and
beautifulli
brought
to
a
stun
synthesi
.

FANTASIA
2000
is
be
releas
exclus
in
the
giant-screen
IMAX
theater
,
make
for
a
uniqu
view
experi
.

There
is
enough
varieti
in
the
program
to
provid
someth
enjoy
for
everyon
and
sure
,
peopl
will
exit
the
theater
with
their
own
person
favorit
.

For
me
,
that
would
have
to
be
the
jazz
ag
stori
of
four
unconnect
and
discont
peopl
live
in
NYC
who
,
unknowingli
,
help
each
other
realiz
their
dream
.

Drawn
in
the
style
of
caricaturist
Al
Hirschfeld
and
inspir
by
Georg
Gershwin
's
RHAPSODY
IN
BLUE
,
the
music
and
imag
ar
blend
perfectli
result
in
a
product
greater
than
the
sum
of
it
two
part
.

Children
will
like
the
short
piec
set
to
CARNIVAL
OF
THE
ANIMALS
by
Camil
Saint-Saen
.

Here
,
a
goofi
look
flamingo
irrit
the
rest
of
the
flock
with
hi
yo-yo
plai
antic
.

Thei
will
also
giggl
as
Donald
Duck
,
plai
Noah
's
assist
,
tri
to
load
the
anim
into
the
ark
to
the
familiar
strain
of
POMP
AND
CIRCUMSTANCE
by
Sir
Edward
Elgar
.

-LRB-
While
the
depict
is
not
biblic
accur
,
it
is
entertain
and
might
be
excus
enough
to
familiar
ourselv
with
the
true
record
by
read
what
is
written
in
Genesi
7
.
-RRB-

A
posit
note
is
that
the
studio
ha
avoid
ani
segment
which
is
as
pervers
dark
as
NIHGT
ON
BALD
MOUNTAIN
upon
which
the
origin
film
end
.

There
ar
some
imag
base
upon
fantasi
such
as
the
fly
whale
in
Respighi
's
PINES
OF
ROME
or
imag
which
have
their
root
in
mytholog
such
as
the
ether
creatur
which
tell
the
stori
of
natur
's
cycl
of
life
,
death
,
and
rebirth
in
Igor
Stravinski
's
FIREBIRD
SUITE
.

But
the
stori
thei
tell
ar
uplift
,
contain
noth
too
frighten
for
our
younger
children
.

The
darkest
segment
,
spiritu
speak
,
actual
belong
to
the
onli
holdov
from
the
origin
FANTASIA
,
Mickei
Mous
in
THE
SORCERER
'S
APPRENTICE
by
Paul
Duka
.

But
overal
,
the
emot
evok
from
FANTASIA
2000
ar
posit
on
,
be
full
of
hope
and
promis
.

Music
ha
an
impact
which
can
be
undeni
felt
,
capabl
of
lead
the
listen
to
emot
peak
and
inspir
high
simpli
not
attain
through
other
medium
.

It
is
no
wonder
that
the
Levit
,
as
the
priestli
tribe
of
the
Old
Testament
,
were
also
the
appoint
musician
.

If
Fantasia
2000
is
abl
to
introduc
a
new
audienc
to
music
which
inspir
and
exalt
,
it
will
have
done
an
incred
servic
.

