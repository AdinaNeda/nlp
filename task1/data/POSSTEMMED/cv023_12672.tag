When
Bulworth
end
,
I
allow
myself
a
sigh
of
relief
:
it
is
possibl
for
me
to
enjoi
polit
satir
.

There
have
been
sever
recent
polit
film
that
did
n't
do
a
whole
lot
for
me
--
Wag
the
Dog
,
for
instanc
,
I
found
to
be
an
intellig
but
heartless
film
,
while
Primari
Color
is
unbear
and
unwatch
in
it
aw
.

Bulworth
is
a
far
better
film
than
these
,
both
in
it
execut
and
last
impress
.

It
's
a
tremend
funni
and
intellig
pictur
,
but
it
also
ha
an
emot
center
--
writer/director/star
Warren
Beatti
allow
the
audienc
to
identifi
with
hi
charact
,
and
,
in
turn
,
we
like
him
and
actual
care
about
the
stori
.

Beatti
plai
incumb
Democrat
Senat
Jai
Billington
Bulworth
;
the
film
open
and
we
see
Bulworth
sob
as
he
watch
hi
televis
spot
over
and
over
again
.

He
ha
n't
slept
in
dai
,
nor
ha
he
eaten
,
and
,
almost
instantan
,
he
goe
insan
.

He
's
not
stark
rave
mad
-LRB-
not
outright
,
anywai
-RRB-
,
but
he
's
definit
bonker
.

It
's
the
final
weekend
of
hi
campaign
,
and
hi
assist
,
Murphi
-LRB-
Oliver
Platt
-RRB-
ha
written
him
a
speech
to
feed
to
a
group
of
Black
peopl
in
a
church
in
the
ghetto
.

Bulworth
begin
the
speech
,
but
suddenli
goe
off
track
and
just
start
be
honest
.

``
You
mean
,
''
on
ladi
ask
,
``
the
Democrat
parti
doe
n't
care
about
the
African
American
commun
?
''

``
Well
,
''
he
shout
,
laugh
,
``
is
n't
that
obviou
?
''

The
stori
is
propel
by
a
devic
in
which
Bulworth
put
out
a
contract
on
hi
own
life
-LRB-
he
muse
later
that
it
's
a
bad
thing
to
make
decis
when
you
're
suicid
-RRB-
;
he
keep
see
a
man
in
sunglass
-LRB-
Graham
Beckel
-RRB-
whom
he
believ
to
be
the
hitman
.

Bulworth
also
meet
Nina
-LRB-
Hall
Berri
-RRB-
with
whom
he
becom
infatu
.

Suddenli
,
in
the
short
span
of
thi
weekend
,
Bulworth
's
no-nonsens
``
truth
in
polit
''
method
becom
a
nation
sensat
,
and
he
shoot
ahead
in
the
poll
despit
the
fact
that
he
's
appear
on
televis
wear
gang
cloth
and
spew
more
profan
than
coher
sentenc
.

Beatti
's
work
on
both
side
of
the
camera
is
excel
.

Hi
script
and
direct
ar
both
extrem
sharp
,
as
is
the
surprisingli
strong
character
of
Bulworth
himself
.

Here
is
a
man
who
's
reach
hi
limit
,
and
hi
wai
of
lash
back
is
by
screw
over
the
entir
polit
structur
.

I
like
that
the
film
doe
n't
take
a
clear
shot
at
ani
of
the
polit
parti
or
affili
,
but
at
polit
in
gener
and
how
everyth
is
run
by
the
rich
-LRB-
although
the
anti-rich
sentiment
do
get
a
bit
tiresom
by
the
end
of
the
film
-RRB-
.

Some
of
the
segment
ar
simpli
hilari
,
such
as
a
lengthi
rap
that
he
deliv
at
a
luncheon
dedic
to
him
.

Much
of
the
film
is
complet
absurd
,
but
that
's
the
fun
part
about
it
.

It
's
an
angri
,
seriou
film
at
the
core
,
but
the
packag
that
Beatti
ha
creat
is
so
much
more
access
than
recent
attempt
in
the
genr
.

What
add
to
thi
feel
is
Beatti
's
portray
of
Bulworth
;
thi
is
a
perform
that
deserv
recognit
on
a
higher
scale
.

Beatti
is
so
much
fun
to
watch
here
--
he
's
alwai
funni
,
but
he
's
also
subtl
in
wai
that
flesh
out
hi
charact
without
dialogu
or
huge
notic
action
.

Beatti
is
so
good
that
Bulworth
,
despit
hi
shortcom
as
a
human
be
,
is
an
entir
sympathet
and
likabl
charact
almost
from
the
begin
.

The
support
cast
is
vast
and
color
.

Berri
is
lumin
,
as
alwai
,
and
add
to
her
repertoir
of
solid
support
role
.

Don
Cheadl
ha
a
good
role
as
a
drug
dealer
who
us
gun-tot
toddler
to
do
hi
dirti
work
.

Oliver
Platt
is
an
actor
who
should
be
care
,
for
somedai
I
fear
he
mai
induc
a
heart
attack
with
the
intens
of
hi
act
-LRB-
though
he
's
veri
funni
to
watch
-RRB-
.

Paul
Sorvino
add
the
Southern
accent
to
hi
list
of
master
inflect
.

Is
Bulworth
offens
?

I
suppos
some
mai
see
it
that
wai
.

I
wa
n't
offend
by
the
film
,
but
then
again
,
I
hardli
care
about
polit
.

I
think
that
peopl
who
find
thi
film
offens
will
just
be
blindsid
by
the
honesti
of
the
stori
.

The
film
is
n't
perfect
,
of
cours
--
the
end
did
n't
quit
work
for
me
,
and
the
a
few
of
the
scene
between
Bulworth
and
Nina
feel
forc
-LRB-
although
the
eclect
danc
sequenc
is
fantast
-RRB-
.

But
these
ar
minor
quibbl
about
an
otherwis
brilliant
film
.

Bulworth
is
a
smart
,
uproari
funni
pictur
that
prove
to
me
that
polit
satir
can
scratch
far
deeper
than
the
surfac
.

