AMERICAN
PIE
American
Pie
acknowledg
a
cold
,
hard
fact
that
most
movi
do
n't
:
it
is
veri
difficult
to
get
laid
.

It
four
virgin
hero
ar
Jim
-LRB-
Bigg
-RRB-
,
a
chronic
masturb
,
Kev
-LRB-
Nichola
-RRB-
,
who
desper
want
to
deflow
hi
girlfriend
-LRB-
Reid
-RRB-
,
Oz
-LRB-
Klein
-RRB-
,
a
lacross
player
whose
approach
requir
some
fine
tune
,
and
Finch
-LRB-
Eddie
Kay
Thoma
-RRB-
,
a
germphob
-LRB-
hi
crass
nicknam
is
hilari
,
but
I
wo
n't
spoil
it
here
-RRB-
who
pai
a
classmat
to
spread
rumour
about
the
size
of
hi
member
.

Jim
,
Kev
,
Oz
and
Finch
attend
a
high
school
in
suburban
Michigan
full
of
so
mani
imposs
beauti
women
it
's
no
wonder
thei
're
horni
all
the
time
.

After
a
parti
at
their
studli
friend
Stifler
's
-LRB-
Seann
William
Scott
,
in
the
film
's
sharpest
perform
-RRB-
hous
leav
them
sexual
unsatisfi
,
the
quartet
make
a
pact
:
to
``
lose
it
''
by
graduation-specif
,
prom
night
,
which
is
onli
a
few
week
awai
.

American
Pie
is
in
the
tradit
of
Bachelor
Parti
or
Reveng
of
the
Nerd
.

Almost
everi
convers
these
charact
have
revolv
around
makin
'
whoope
.

All
women
ar
there
to
be
ogl
.

-LRB-
Alyson
Hannigan
's
perki
music
student
is
the
onli
memor
femal
of
the
bunch
.
-RRB-

Situat
abound
that
defi
logic
onli
to
arous
and/or
amus
it
core
audienc
,
adolesc
boi
.

-LRB-
To
arous
:
Nadia
's
-LRB-
Shannon
Elizabeth
-RRB-
Internet
striptease-an
unexpectedli
raucou
-LRB-
and
guiltili
pleasur
-RRB-
sequenc
.

To
amus
:
Jim
's
lusti
encount
with
a
hot
appl
pie
.
-RRB-

What
differenti
American
Pie
from
those
dirti
eighti
comedi
,
asid
from
a
veri
nineti
obsess
with
bodili
fluid
*
,
is
a
cast
that
's
light
year
more
appeal
than
that
of
,
sai
,
Just
One
of
the
Gui
.

Two
more
standout
:
Klein
and
``
SCTV
''
's
Eugen
Levi
.

Klein
plai
a
kind-heart
athlet
for
the
second
time
in
a
row
,
after
Alexand
Payn
's
underappreci
Election
.

I
hope
to
see
more
of
thi
warm
actor
with
the
disarmingli
honest
face
veri
soon
.

Levi
's
is
the
most
crowd-pleas
perform
,
and
inde
,
it
's
nice
to
see
him
back
on
the
big
screen
in
a
role
that
make
wonder
us
of
that
express
brow
.

American
Pie
also
want
to
emul
the
teen
flick
your
parent
_
would
_
let
you
watch
grow
up
.

It
skillfulli
emploi
a
broad
mix
of
pop
tune
,
much
like
John
Hugh
'
movi
did
,
and
even
pai
homag
to
The
Breakfast
Club
a
coupl
of
time
,
most
evid
when
the
prom
band
cover
Simpl
Mind
'
``
Do
n't
You
-LRB-
Forget
About
Me
-RRB-
.
''

The
pictur
ultim
ha
a
healthi
,
if
obligatori
,
attitud
toward
safe
sex-even
at
their
most
libidin
,
American
Pie
's
protagonist
first
whip
out
a
condom
.

The
filmmak
prove
that
social
respons
raunch
is
possibl
.

For
it
familiar
and
for
it
charm
lead
,
I
recommend
American
Pie
,
but
the
buzz
on
thi
film
had
me
expect
someth
...
fresher
.

As
far
as
the
New
Teen
Cinema
goe
,
it
's
at
the
top
of
the
heap
.

*
Do
n't
sai
I
did
n't
warn
you
about
the
parti
scene
.

