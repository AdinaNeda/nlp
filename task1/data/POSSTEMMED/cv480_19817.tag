The
trailer
and
the
begin
of
the
move
sum
up
thi
plot
veri
easili
.

Three
filmmak
ventur
into
the
wood
of
Maryland
to
track
down
the
legend
of
the
Blair
Witch
for
a
documentari
thei
ar
make
.

Thei
disappear
and
their
footag
is
discov
a
year
later
buri
underneath
the
foundat
of
a
on
hundr
year
old
hous
.

From
there
,
thing
becom
a
littl
more
complic
.

I
can
sai
,
with
absolut
assur
,
that
no
film
ha
ever
had
quit
the
impact
that
thi
littl
movi
ha
had
on
me
.

Although
I
know
it
wa
all
a
stage
``
mockumentari
''
,
there
's
no
deni
the
psycholog
impact
thi
film
will
leav
on
you
.

I
did
n't
feel
much
upon
initi
watch
it
.

I
just
thought
it
wa
an
interest
film
with
some
pretti
creepi
part
.

I
never
even
dream
that
thirti
minut
after
the
film
had
end
I
would
be
frighten
out
of
my
mind
just
discuss
it
with
a
friend
.

I
've
gotten
chill
everi
time
I
've
brought
it
up
sinc
then
.

Thi
is
not
a
movi
that
hit
you
right
awai
.

Instead
thi
is
a
movi
that
sit
at
the
base
of
your
spine
,
wait
until
you
think
everyth
is
okai
,
then
it
slowli
start
to
creep
it
's
wai
up
into
your
mind
.

Thi
is
truli
the
most
frighten
movi
I
have
ever
had
the
pleasur
to
watch
.

The
film
is
compris
of
home
video
and
16mm
camera
footag
that
the
documentarian
supposedli
shot
for
their
Blair
Witch
film
,
so
do
n't
expect
ani
steadi-cam
shot
or
fanci
camera
work
.

Thi
is
as
real
as
it
get
folk
.

I
do
n't
recommend
sit
too
close
to
the
theater
screen
though
,
becaus
I
guarante
you
will
get
motion
sick
from
the
constant
movement
of
the
camera
.

The
perform
of
the
three
lead
actor
in
the
film
ar
so
real
,
you
mai
find
yourself
question
whether
all
of
thi
ha
realli
happen
or
not
.

Thi
is
definit
a
movi
that
warrant
home
video
view
,
especi
due
to
the
fact
that
the
bulk
of
the
movi
wa
shot
with
a
standard
home
video
camera
.

The
realiti
of
the
imag
will
strike
frighteningli
close
to
home
when
view
as
actual
video
instead
of
the
tape-to-film
transfer
done
for
the
big
screen
.

Word
ha
it
that
a
special
edit
DVD
with
boatload
of
extra
-LRB-
hour
of
delet
scene
,
altern
end
,
commentari
,
the
Sci-Fi
Channel
special
,
and
other
-RRB-
is
be
plan
for
releas
.

I
urg
Artisan
Entertain
to
forg
ahead
with
thi
plan
and
give
us
what
could
be
the
DVD
to
rival
all
DVD
's
.

I
know
I
'll
be
first
in
line
to
bui
on
.

