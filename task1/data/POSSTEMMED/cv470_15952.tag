``
Love
is
the
Devil
''
is
a
challeng
film
,
munund
it
audienc
with
wild
imageri
and
a
plot
structur
that
disallow
a
plot
,
perhap
in
an
attempt
to
get
us
to
know
the
artist
's
psych
rather
than
the
artist
's
lifelin
.

Watch
it
,
I
wa
enthral
with
the
look
of
the
film
,
the
wai
the
director
shot
everyth
like
it
wa
look
through
a
bizarr
,
person
filter
.

Everyth
look
like
it
is
not
how
life
look
like
but
how
painter
Franci
Bacon
,
the
film
's
subject
,
look
at
it
person
.

But
while
I
wa
engross
,
I
stumbl
upon
my
thought
halfwai
through
the
film
,
awaken
from
my
tranc
by
some
inner
distract
,
and
began
to
try
and
follow
what
's
go
on
.

Exactli
what
wa
I
look
at
?

Watch
thi
film
,
I
wa
n't
sure
if
it
wa
the
most
insight
film
I
had
ever
seen
or
the
most
vacuou
.

Direct
-LRB-
and
written
-RRB-
by
John
Mayburi
,
``
Love
is
the
Devil
''
is
stylish
masterpiec
for
the
sens
.

Everyth
look
origin
bizarr
and
perplex
.

The
camera
angl
ar
feroci
askew
,
and
the
close-up
ar
uncomfort
too
close
.

The
edit
is
deft
,
occasion
cut
awai
to
someth
bizarr
everi
coupl
second
,
but
other
time
hold
on
a
shot
for
so
long
that
you
wonder
if
the
audienc
is
not
suppos
to
be
voyeur
,
or
rather
,
intrud
to
Bacon
's
world
and
psych
.

As
such
,
there
is
no
real
stori
.

I
've
not
heard
much
about
Franci
Bacon
that
I
did
n't
read
prior
to
the
film
,
but
what
I
learn
wa
thi
:
he
wa
a
painter
in
England
who
reach
hi
peak
dure
the
60
and
70
,
draw
hideous
bizarr
draw
of
carnag
and
the
like
.

He
wa
on
of
the
first
to
realli
come
out
of
the
closet
,
and
in
interview
,
he
wa
notori
drunk
yet
incred
witti
.

As
plai
by
great
Shakesperean
actor
Derek
Jacobi
,
he
's
like
a
foppish
and
self-absorb
cross
between
Oscar
Wild
and
Nero
.

He
live
life
the
wai
he
want
to
live
it
,
to
the
disatisfact
of
those
who
have
the
priveleg
of
be
realli
close
to
him
.

It
's
as
if
he
were
take
delight
in
the
destruct
of
other
and
mayb
himself
-LRB-
``
Champagn
for
my
real
friend
,
and
pain
for
my
sham
friend
,
''
he
sai
on
night
at
the
bar
he
frequent
-RRB-
.

``
Love
is
the
Devil
''
chronicl
the
latter
part
of
hi
life
.

Bacon
,
well-known
in
hi
mid-lif
,
awaken
on
night
when
he
hear
a
man
tumbl
through
hi
ceil
window
and
land
on
the
floor
.

He
walk
in
,
unafraid
of
what
he
find
,
and
discov
a
thief
,
Georg
Dyer
-LRB-
Daniel
Craig
-RRB-
.

Bacon
give
him
an
offer
:
if
he
spend
the
night
with
him
,
he
can
take
anyth
he
want
in
the
morn
.

Georg
agre
to
thi
,
and
off
to
bed
he
goe
,
but
end
up
stai
with
him
,
for
whatev
reason
.

The
film
show
their
live
togeth
,
at
least
in
refer
to
on
anoth
.

While
creat
Bacon
's
world
of
friend
-LRB-
like
``
High
Art
,
''
thi
film
minor
in
show
a
certain
group
of
peopl
who
radiat
a
connect
that
is
not
shown
but
is
understood
-RRB-
,
the
film
show
the
relationship
of
Bacon
and
Georg
as
it
remain
rather
stagnant
.

Bacon
is
haughti
and
bizarr
;
Georg
is
simpl
and
doe
n't
understand
Bacon
in
the
least
,
especi
not
hi
paint
.

While
watch
thi
,
that
's
basic
all
I
thought
wa
there
.

The
film
ha
a
hypnot
feel
,
free
of
ani
restraint
of
form
,
and
is
shot
so
uniqu
that
I
felt
my
attent
wa
almost
entir
on
the
wai
thi
film
wa
made
rather
than
what
it
is
about
.

Once
the
film
is
over
,
it
's
easier
to
piec
it
togeth
.

I
kept
on
think
about
thi
film
,
wonder
what
the
point
to
all
of
it
wa
.

Someon
doe
n't
mere
make
a
film
of
all
style
and
no
substanc
at
all
,
and
if
thei
do
,
thei
do
it
by
accid
,
but
still
allow
some
substanc
to
creep
in
.

Think
about
it
,
I
rememb
how
the
two
fed
off
of
eachoth
.

I
thought
about
how
Bacon
wa
a
masochist
,
in
love
with
cruelti
-LRB-
in
on
scene
,
he
watch
a
box
match
with
a
orgiast
delight
,
and
let
out
a
squeal
of
pleasur
when
blood
from
the
on
boxer
's
head
splash
across
hi
face
;
in
anoth
scene
,
he
masturb
to
the
Odessa
Step
sequenc
of
Eisenstein
's
``
Battleship
Potemkin
''
-RRB-
,
and
perhap
he
drove
Georg
too
far
in
hi
delight
for
pain
.

He
drove
him
over
the
edg
,
and
for
him
that
wa
love
,
even
if
it
wa
n't
for
Georg
.

I
guess
that
explain
the
titl
.

A
bit
.

That
's
great
and
all
,
but
I
almost
wish
the
film
wa
devoid
of
ani
mean
.

I
wish
it
had
n't
reduc
itself
to
make
some
point
about
human
,
about
how
love
is
the
most
selfish
thing
in
the
world
-LRB-
and
it
is
,
if
you
look
at
it
a
certain
wai
-RRB-
.

Or
mayb
if
it
had
avoid
ani
mean
about
human
and
mere
drove
itself
into
be
the
on
film
that
wa
truli
insid
on
man
's
twist
pysch
.

It
's
almost
alwai
best
to
obtain
insight
from
look
at
on
man
's
uniqu
than
it
is
forc
univers
down
an
audienc
's
throat
.

That
wai
you
do
n't
reduc
your
film
to
someth
it
's
just
not
.

I
ca
n't
sai
I
total
enjoi
``
Love
is
the
Devil
,
''
though
.

Despit
all
of
the
thing
I
respect
about
thi
movi
,
it
's
still
rather
uncomfort
to
sit
through
.

Even
at
a
normal
trite
length
of
nineti
minut
,
the
film
still
seem
like
an
arduou
task
to
sit
through
,
especi
after
an
exhaust
first
hour
.

Though
hypnot
,
it
still
almost
seem
gimmicki
and
even
redund
at
time
,
as
if
it
were
take
advantag
of
on
man
's
truli
bizarr
natur
but
not
do
anyth
deeper
with
it
.

As
such
,
I
respect
the
wai
the
film
look
.

It
's
beauti
and
painstakingli
craft
so
that
,
along
with
``
What
Dream
Mai
Come
''
and
``
Dark
Citi
,
''
it
's
the
year
's
most
visual
stun
film
.

In
fact
,
if
a
better
script
had
been
forg
,
I
'd
almost
compar
it
in
visual
power
to
a
Peter
Greenawai
film
,
complet
with
similar
haunt
imag
that
stick
in
the
mind
forev
.

And
with
a
dymanit
perform
by
Derek
Jacobi
,
it
ha
the
comic
and
distanc
tone
that
it
need
.

I
just
wish
it
had
been
more
than
mostli
style
and
just
a
hair
bit
of
substanc
.

Then
I
would
have
had
someth
to
hold
me
over
even
now
.

