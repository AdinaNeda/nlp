****
-LRB-
out
of
=
excel
-RRB-
1993
,
PG-13
,
133
minut
-LSB-
2
hour
,
minut
-RSB-
-LSB-
thriller
-RSB-
star
:
Harrison
Ford
-LRB-
Dr.
Richard
Kimbl
-RRB-
,
Tommi
Lee
Jone
-LRB-
Deputi
Sam
Gerard
,
U.S.
Marshal
-RRB-
,
Joe
Pantoliano
-LRB-
Cosmo
Renfo
-RRB-
,
Daniel
Roebuck
-LRB-
Bigg
-RRB-
,
written
by
Jeb
Stuart
,
David
Twohi
,
produc
by
Arnold
Kopelson
,
direct
by
Andrew
Davi
.

``
The
Fugit
''
is
probabl
on
of
the
greatest
thriller
ever
made
.

It
take
realist
,
believ
charact
and
tell
an
excit
stori
that
's
total
believ
throughout
.

Thi
is
n't
an
over-the-top
action
flick
for
the
sake
of
action
,
thi
is
an
intellig
adventur
stori
with
a
real
sens
of
mysteri
to
it
and
it
work
perfectli
.

Harrison
Ford
star
as
Dr.
Richard
Kimbl
,
a
man
who
is
wrongli
accus
and
convict
of
murder
hi
wife
.

The
entir
premis
of
the
stori
revolv
around
thi
murder
,
but
is
not
told
in
a
straightforward
manner
.

We
constantli
see
flashback
to
the
murder
from
Kimbl
's
perspect
.

Hint
ar
drop
from
variou
time
of
the
night
Kimbl
's
wife
,
Helen
-LRB-
Sela
Ward
-RRB-
wa
murder
.

We
do
n't
get
to
see
who
the
killer
is
until
much
further
into
the
stori
.

In
fact
,
we
might
even
have
a
littl
doubt
about
Kimbl
's
innoc
ourselv
.

The
film
doe
n't
spend
much
time
on
the
detail
of
Kimbl
's
trial
and
convict
as
it
is
not
the
point
of
the
stori
.

One
of
the
greatest
action
scene
ensu
as
we
get
a
botch
escap
attempt
by
hi
fellow
inmat
which
lead
to
a
fantast
train
wreck
.

But
it
doe
not
at
all
seem
gratuit
or
formul
,
it
is
actual
suspens
to
watch
.

Thi
movi
is
so
incred
becaus
it
's
abl
to
take
element
like
these
and
make
them
wholli
origin
and
believ
.

Tommi
Lee
Jone
almost
steal
the
show
as
Deputi
Sam
Gerard
,
a
U.S.
Marshal
who
is
an
expert
in
hunt
fugit
.

From
the
moment
he
appear
we
know
a
huge
game
of
cat
and
mous
is
about
to
occur
.

Kimbl
and
Gerard
ar
equal
match
as
there
is
no
wai
to
tell
how
on
is
go
to
outsmart
the
other
.

Most
of
the
film
deal
with
Richard
's
complex
investig
as
he
tri
to
find
the
one-arm
man
who
kill
hi
wife
.

Ford
doe
not
have
a
lot
of
line
here
,
so
it
is
Stuart
and
Twohi
's
script
and
Davi
's
outstand
direct
that
make
the
film
as
great
as
it
is
.

Just
watch
Richard
us
hi
keen
intellig
and
wit
,
disguis
himself
,
and
do
hi
own
detect
work
is
veri
interest
.

We
know
just
as
littl
as
he
doe
as
to
why
hi
wife
wa
kill
,
so
as
he
learn
more
so
do
we
.

Meanwhil
Gerard
and
hi
staff
ar
us
all
their
skill
to
hunt
him
down
.

At
on
point
thei
run
into
each
other
and
a
great
chase
ensu
,
but
Richard
is
just
too
smart
for
them
.

He
leav
clue
for
Gerard
to
show
hi
innoc
and
to
help
him
solv
the
mysteri
.

One
of
the
greatest
charm
to
the
film
is
it
sens
of
humor
.

There
is
a
great
sens
of
comradeship
between
Gerard
and
hi
two
closest
deputi
Cosmo
Renfo
-LRB-
Pantoliano
-RRB-
and
Bigg
-LRB-
Roebuck
-RRB-
.

Thei
ar
try
to
catch
what
thei
believ
to
be
a
danger
murder
,
yet
thei
constantli
start
up
small
talk
with
each
other
,
make
funni
wisecrack
and
one-lin
,
but
not
in
ani
kind
of
distract
manner
.

The
final
act
conclud
with
anoth
terrif
chase
.

Richard
figur
out
who
the
killer
is
and
confront
him
,
and
although
we
get
a
typic
fight
scene
,
the
case
is
far
from
solv
.

The
man
who
order
the
murder
ha
been
under
our
nose
all
along
,
and
we
do
n't
realiz
thi
until
the
end
when
both
Richard
and
Gerard
put
the
piec
togeth
.

Mayb
the
final
scene
is
a
bit
much
,
but
it
still
ha
a
believ
atmospher
to
it
.

We
're
so
reliev
when
justic
is
serv
.

It
's
not
the
plot
is
that
make
``
The
Fugit
''
great
,
it
's
all
the
motion
the
charact
go
through
to
make
for
an
intrigu
stori
.

Watch
a
seemingli
averag
man
go
through
as
much
as
thi
is
what
make
the
movi
so
adventur
.

If
onli
Hollywood
could
produc
more
movi
like
thi
.

-LRB-
10/31/96
-RRB-
-LRB-
1/29/97
-RRB-
-LRB-
6/13/97
-RRB-
-LSB-
see
also
:
``
Nick
Of
Time
''
-RSB-
pleas
visit
Chad
'
z
Movi
Page
@
-LSB-
1
-RSB-
http://members.aol.com/ChadPolenz/index.html

