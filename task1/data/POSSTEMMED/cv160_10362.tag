Corei
Yuen
's
latest
film
,
``
HERO
''
is
notabl
on
two
front
.

First
thi
is
the
film
to
bring
back
the
Shaw
Brother
Studio
back
to
the
forefront
of
film
product
.

Secondli
,
the
film
is
the
re-discoveri
of
Yuen
Biao
.

The
film
open
around
the
end
of
the
Qin
dynasti
,
when
mani
immigr
were
make
their
wai
to
Shanghai
.

Poverti
and
crime
rule
most
of
China
.

A
young
man
,
Ma
Wing
Jing
and
hi
brother
enter
the
fabl
citi
and
becom
labor
at
the
pier
.

Tam
See
is
the
most
power
gangster
in
town
,
have
control
of
the
center
of
the
citi
and
have
an
allianc
with
the
British
Army
.

Wing
Jing
and
See
becom
friend
dure
a
confront
.

Their
friendship
grow
as
both
men
start
to
realiz
their
dream
,
Wing
Jing
's
to
be
a
power
and
wealthi
man
,
and
See
to
settl
down
with
a
woman
he
can
truli
love
.

Wing
Jing
meet
the
singer
at
the
club
-LRB-
Jessica
Hester
-RRB-
and
fall
in
love
with
her
,
not
realiz
that
she
is
the
star
attract
.

After
steal
her
pictur
from
a
displai
,
Wing
Jing
and
hi
brother
plot
to
impress
her
.

First
thei
rob
two
foreign
's
of
their
monei
and
cloth
,
and
set
up
a
midnight
rendezv
.

Unfortun
,
Wing
Jing
ha
run
afoul
of
a
rival
gangster
,
Yeung
Seung
who
ha
bribe
the
polic
in
an
effort
to
gain
control
of
See
's
night
club
.

The
two
bother
ar
arrest
and
held
until
thei
manag
to
escap
just
befor
dawn
.

Jessica
,
have
wait
outsid
in
the
cold
with
her
manag
all
night
,
give
up
and
return
home
.

See
decid
to
retir
,
he
give
the
night
club
to
Wing
Jing
,
as
repay
for
save
hi
life
dure
an
attempt
murder
.

See
arrang
the
trade
and
ask
hi
lover
-LRB-
Valeri
Chow
-RRB-
to
take
care
of
Wing
Jing
.

Unfortun
,
she
ha
side
with
Yeung
Seung
.

The
perform
in
the
film
ar
all
top
notch
,
with
fine
turn
by
Takeshi
Kaneshiro
as
Ma
Wing
Jing
and
Jessica
Hester
and
Valeri
Chow
.

The
comic
turn
by
Yuen
Wah
,
previous
known
as
the
mad
Vietnames
in
Samo
Hung
's
``
EASTERN
CONDORS
''
,
is
a
marvel
surpris
.

Hi
gift
for
comedi
is
marvel
underst
and
hilari
.

The
stand-out
is
by
Yuen
Biao
who
return
to
the
screen
after
a
string
of
disappoint
,
with
a
stellar
perform
.

Hi
Tam
See
is
a
Triad
boss
who
know
that
hi
time
is
come
to
an
end
but
still
ha
the
cun
and
craft
to
uphold
hi
pride
.

Hi
gangster
is
almost
non-viol
,
in
that
he
onli
retali
,
never
instig
violenc
.

But
when
the
time
come
to
fight
,
Yuen
Biao
ha
never
look
better
.

Corei
Yuen
direct
the
film
with
a
matur
and
a
sens
of
pace
that
ha
been
lack
in
Hong
Kong
movi
as
of
late
.

The
light
,
cinematographi
and
stage
of
the
film
ar
beauti
to
watch
.

The
action
scene
do
not
disappoint
either
.

Thi
is
the
most
aggress
and
invent
martial
art
choreographi
I
've
seen
in
some
time
,
actual
surpass
Jacki
Chan
's
work
in
both
'
Rumbl
in
the
Bronx
'
and
'
First
Strike
'
.

The
special
effect
team
deserv
prais
for
a
fantast
job
of
make
the
imposs
look
possibl
.

An
earli
fight
between
Takeshi
Kaneshiro
and
Yuen
Biao
on
the
back
of
a
hors
is
to
be
seen
to
be
believ
.

Also
noteworthi
is
the
score
for
the
film
,
done
in
a
marvel
lush
orchestr
style
,
not
repres
of
most
Hong
Kong
fare
.

I
would
dare
to
sai
it
rank
as
high
as
the
score
for
both
``
Titan
''
and
``
Rosewood
''
as
best
of
the
year
.

All
in
all
,
``
HERO
''
mark
a
grand
return
to
filmmak
for
Shaw
Brother
.

Thi
is
the
film
that
give
us
faith
that
Hong
Kong
cinema
is
aliv
and
flourish
.

