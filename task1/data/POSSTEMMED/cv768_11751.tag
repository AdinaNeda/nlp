Everi
onc
in
a
while
you
see
a
film
that
is
so
effect
in
deliv
the
good
that
it
is
easi
to
forget
,
and
forgiv
,
it
glare
imperfect
.

Such
is
the
case
with
³
Good
Will
Hunt
²
,
a
subtl
charact
studi
about
a
social
inept
mathemat
geniu
who
struggl
to
find
hi
path
in
life
.

Despit
some
seriou
charact
problem
,
thi
is
still
a
veri
good
film
.

You
probabl
know
about
the
plot
so
I
¹
ll
make
it
quick
.

Will
Hunt
-LRB-
Damon
-RRB-
is
a
janitor
at
M.I.T.
,
he
¹
s
realli
smarter
than
Einstein
but
nobodi
know
it
.

He
like
to
go
out
with
hi
friend
Chucki
-LRB-
Affleck
-RRB-
and
their
other
working-class
buddi
and
drink
beer
.

He
¹
s
good-look
,
charismat
,
and
witti
but
ha
a
terribl
time
with
author
and
stiff
colleg
folk
.

After
get
into
a
tiff
with
the
law
,
a
distinguish
professor
-LRB-
Skarsgård
-RRB-
discov
Will
¹
s
geniu
and
offer
him
a
chanc
to
clean
up
hi
record
and
avoid
jail
time
,
at
a
price
:
He
must
attend
weekli
session
with
a
therapist
and
work
on
variou
mathemat
problem
-LRB-
that
have
stump
the
academ
elit
-RRB-
with
the
professor
.

After
outsmart
and
scare
the
hell
out
of
a
coupl
of
differ
psychologist
he
meet
hi
match
when
hook
up
with
a
once-promis
therapist
name
Sean
Maguir
-LRB-
William
-RRB-
who
ha
hi
own
problem
.

In
the
meantim
Will
meet
a
British
medic
school
student
-LRB-
Driver
-RRB-
and
thei
begin
to
fall
in
love
.

The
stori
start
out
well
enough
and
is
a
pretti
origin
basi
for
a
film
.

Even
though
we
¹
ve
seen
movi
about
misunderstood
,
errat
prodigi
befor
-LRB-
³
Shine
²
ring
a
bell
?
-RRB-

,
the
script
here
creat
a
complex
narr
that
doesn
¹
t
just
focu
sole
on
on
charact
.

Ala
though
,
thi
is
not
a
perfect
film
,
as
much
as
you
feel
like
it
could
¹
ve
been
while
watch
it
.

The
on
real
problem
I
had
with
it
is
the
unrealist
natur
of
the
main
charact
.

Is
it
possibl
for
a
lowli
janitor
to
be
thi
intellig
?

Of
cours
.

Is
it
possibl
for
him
to
be
estrang
from
ani
deep
,
human
relationship
?

Usualli
,
ye
.

But
,
is
it
possibl
for
him
to
also
be
so
handsom
,
funni
,
quick
with
the
tongu
,
and
city-street
tough
?

Not
veri
like
.

Come
on
,
usual
these
gui
ar
total
nerd
who
can
¹
t
even
bui
their
own
shirt
,
much
less
talk
down
a
Harvard
student
in
a
hip
pub
while
pick
up
phone
number
from
pretti
med.-school
girl
.

Will
is
just
a
littl
too
perfect
,
and
in
order
to
accept
the
charact
your
disbelief
suspens
need
to
be
in
excel
work
condit
.

The
heavy-hand
,
anti-war
statement
made
by
Will
at
a
govern
job
interview
late
in
the
film
is
also
boorish
,
overlong
,
pompou
,
and
complet
unnecessari
.

All
thi
sound
pretti
bad
,
but
the
film
somehow
make
up
for
it
in
other
wai
.

Damon
¹
s
act
overshadow
the
fact
that
the
charact
is
slightli
unbeliev
,
hi
perform
is
truli
extraordinari
.

Which
lead
me
to
the
realli
good
part
of
the
review
.

The
strength
of
thi
movi
can
be
sum
up
in
on
singl
word
:
act
.

I
can
¹
t
recal
see
a
film
recent
that
wa
so
well-act
from
top
to
bottom
.

From
Minni
Driver
¹
s
frustrat
lover
to
Ben
Affleck
¹
s
laid-back
best
friend
,
and
all
the
small
role
in
between
,
the
perform
ar
magnific
.

Robin
William
¹
skill
is
a
given
as
a
bereav
psychologist
who
could
¹
ve
had
a
legendari
career
but
wa
knock
off
the
path
somewher
down
the
line
.

The
real
gem
though
is
Stellan
Skarsgård
¹
s
turn
as
Professor
Lambeau
,
an
award-win
mathematician
who
feel
reduc
in
comparison
to
a
younger
,
smarter
Will
Hunt
.

The
scene
between
William
and
Skarsgård
,
as
two
old
colleg
pal
who
¹
ve
been
brought
back
togeth
by
thi
enigmat
kid
,
displai
some
of
the
best
act
I
¹
ve
ever
seen
.

When
I
sai
deliv
the
good
,
thi
is
what
I
¹
m
talk
about
.

Watch
these
two
work
is
what
go
to
see
movi
is
all
about
.

Gu
Van
Sant
¹
s
-LRB-
To
Die
For
,
Drugstor
Cowboi
-RRB-
cold
,
urban
direct
is
right
on
,
as
well
as
Danni
Elfman
¹
s
saunter
music
score
.

I
highli
recommend
³
Good
Will
Hunt
²
.

Despit
it
fault
,
it
is
still
an
intrigu
and
fascin
film
and
you
ar
not
like
to
see
a
better
act
on
thi
year
.

