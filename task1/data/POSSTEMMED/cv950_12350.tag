From
the
man
who
present
us
with
HENRY
:
THE
PORTRAIT
OF
A
SERIAL
KILLER
come
a
wild
tale
set
within
the
elit
and
white
trash
of
Florida
's
South
Coast
.

Guidanc
Counselor
-LRB-
Dillon
-RRB-
is
accus
of
rape
on
of
hi
student
.

She
happen
to
be
the
daughter
of
on
of
the
wealthiest
women
in
Florida
.

The
counselor
is
brought
to
court
on
the
charg
,
as
anoth
white
trash
girl
-LRB-
Campbel
-RRB-
,
also
join
the
rape
finger-point
club
.

As
the
stori
unfold
,
we
find
that
mani
thing
ar
n't
exactli
as
thei
seem
-LRB-
that
's
as
much
as
I
could
sai
without
ruin
anyth
-RRB-
.

Good
littl
flick
that
slow
down
at
certain
point
,
and
run
a
tad
long
,
but
gener
enough
twist
and
turn
to
keep
most
peopl
interest
throughout
.

Mind
you
,
I
could
see
mani
peopl
not
like
it
becaus
it
take
``
on
too
mani
''
twist
and
turn
,
but
hei
,
I
happen
to
like
that
kind
of
thing
.

And
besid
,
the
twist
and
turn
in
thi
movi
manag
to
remain
within
the
realm
of
believ
-LRB-
or
scathingli
close
to
-RRB-
,
and
even
``
explain
''
mani
of
it
turn
with
a
showcas
of
miss
scene
dure
the
credit
crawl
at
the
end
of
the
film
-LRB-
DO
NOT
leav
the
theatr
befor
you
see
the
credit
...
it
realli
doe
clear
up
some
stuff
!!
-RRB-
.

Other
than
that
,
the
act
wa
good
,
with
Campbel
transform
her
``
good
girl
stereotyp
''
role
into
a
pot-smok
,
goth-look
white
trash
chick
out
for
a
good
time
.

Denis
Richard
-LRB-
from
STARSHIP
TROOPERS
fame
-RRB-
solidifi
herself
as
Hollywood
's
jelly-donut
pinup
girl
of
the
moment
,
and
prove
that
all
breast
implant
lead
to
lawsuit
.

Bacon
is
solid
,
and
so
is
Dillon
-LRB-
while
continu
to
prove
hi
inabl
to
ag
to
the
whole
wide
wonder
world
!!
-RRB-
.

Sprinkl
the
stori
with
a
bunch
o
'
shot
of
allig
peek
through
the
swamp
water
,
Bacon
show
the
world
the
size
of
hi
ding-dong
-LRB-
do
we
realli
need
to
see
thi
??
-RRB-

,
a
coupl
of
lesbian
kiss
scene
,
and
a
menage-a-troi
,
and
you
've
got
yourself
a
decent
time
at
the
movi
theatr
.

On
the
down
side
,
the
soundtrack
wa
not
as
promin
-LRB-
or
slick
-RRB-
as
I
thought
it
would
be
,
and
Theresa
Russel
-LRB-
real
name
:
Theresa
Paup
-RRB-
seem
to
have
lost
her
wai
in
everi
which
wai
possibl
...
.

ooooh
,
I
almost
forgot
Bill
Murrai
's
exquisit
role
as
the
down-trodden
lawyer
who
agre
to
defend
Dillon
in
court
.

Murrai
actual
ad
that
extra
littl
spice
of
humour
that
allow
thi
film
to
affirm
it
posit
as
a
quirki
littl
noirish
tale
of
sex
,
greed
and
mysteri
.

Littl
Known
Fact
:
Neve
Campbel
specifi
a
non-nud
claus
in
her
contract
-LRB-
she
's
also
born
in
Guelph
,
Ontario
...
go
hoser
!
-RRB-

Director
John
McNaughton
sai
he
delet
a
scene
that
would
have
shown
Matt
Dillon
and
Kevin
Bacon
shower
togeth
,
as
it
wa
gratuit
.

Kevin
Bacon
plai
in
a
band
with
hi
brother
,
call
The
Bacon
Brother
,
is
marri
to
actress
Kyra
Sedgwick
,
and
ha
a
game
base
on
him
call
``
Six
Degre
of
Kevin
Bacon
''
.

