There
ar
certain
peopl
in
the
world
who
have
some
talent
that
veri
few
peopl
have
,
but
thei
chose
not
to
take
advantag
of
thi
for
some
person
reason
.

``
Good
Will
Hunt
''
is
about
on
of
those
peopl
.

The
Will
Hunt
of
the
-LRB-
horribl
-RRB-
titl
-LRB-
let
's
face
it
,
thi
film
would
be
a
bitch
to
titl
-RRB-
,
plai
by
co-writ
Matt
Damon
,
is
a
mathemat
geniu
.

He
can
do
almost
ani
mathemat
connundrum
in
about
the
time
it
take
to
brew
a
cup
of
Irish
Cream
Cappuccino
.

And
he
work
at
MIT
...
as
the
janitor
.

One
dai
,
the
professor
of
on
of
the
most
challeng
cours
,
Proffessor
Lambeau
-LRB-
``
Break
the
Wave
''
's
Stellan
Skarsgrd
-RRB-
,
put
a
big
problem
on
the
board
for
hi
student
to
attempt
to
complet
.

The
next
dai
,
it
's
alreadi
done
on
the
board
,
but
no
on
in
hi
class
claim
to
have
done
it
.

So
Lambeau
put
anoth
problem
on
the
board
,
thi
time
on
that
took
he
and
hi
colleagu
over
two
year
to
prove
.

And
,
duh
,
it
's
done
again
.

Of
cours
,
thei
catch
the
gui
who
did
it
red
handed-Wil
.

Befor
Lambeau
can
find
Will
again
-LRB-
he
ran
awai
when
he
caught
him
-RRB-
,
Will
ha
gotten
into
a
fight
with
a
bunch
of
punk
on
a
basketbal
court
,
and
struck
a
polic
offic
,
so
Lambeau
get
him
out
on
probat
with
two
agreement
:
a
-RRB-
he
work
with
him
on
math
,
and
b
-RRB-
he
get
some
therapi
.

After
go
through
some
therapist
whom
he
psych
out
in
on
wai
or
anoth
-LRB-
for
a
hypnotist
,
he
fake
be
under
hi
spell
,
then
launch
into
an
impromptu
perform
of
the
70
classic
,
``
Afternoon
Delight
''
-RRB-
.

Final
,
Lambeau
's
old
colleg
roomat
,
Sean
McGuir
-LRB-
Robin
William
-RRB-
,
a
psych
professor
,
agre
to
treat
him
,
and
the
two
begin
a
rocki
relationship
,
as
Sean
tri
to
get
Will
to
open
up
.

Turn
out
Will
had
a
rough
childhood
,
be
abandon
,
and
place
in
foster
home
and
what
not
.

It
also
turn
out
that
Will
doe
not
want
to
do
simpl
mathemat
all
hi
life
;
he
want
a
challeng
.

And
that
is
to
hang
out
with
hi
bud
-LRB-
includ
the
other
co-writ
,
Ben
Affleck
-RRB-
,
and
do
honor
job
,
like
construct
.

Is
it
right
to
make
thi
geniu
follow
in
the
footstep
of
Einstein
and
Kasinski
?

Should
thi
gui
be
forc
to
do
someth
he
doe
n't
want
to
do
?

Thi
appear
to
be
on
of
the
million
question
in
thi
film
,
which
is
jam-pack
with
great
,
but
doe
not
have
the
total
brilliant
whole
it
should
.

I
'm
not
sai
``
Good
Will
Hunt
''
is
a
bad
film
at
all
.

It
's
a
veri
enjoy
film
with
ton
of
great
moment
,
and
lot
of
great
thing
about
it
.

The
act
is
amaz
,
with
two
Oscar-nomination-worthi
perform
from
Damon
and
William
,
who
have
amaz
chemistri
togeth
.

We
also
get
some
great
support
from
Stellan
Skarsgrd
-LRB-
great
name
-RRB-
,
Ben
Affleck
,
and
from
Minni
Driver
,
who
plai
Will
's
love
interest
,
Skylar
.

There
ar
ton
of
individu
moment
that
I
ador
.

I
love
all
the
comedi
in
the
film
,
especi
a
scene
where
Damon
prove
a
schmuck
out
of
a
colleg
student
who
is
quot
a
historian
to
impress
some
chick
,
hi
variou
attempt
at
therapi
,
and
of
cours
,
my
favorit
scene
in
the
entir
film
,
the
on
where
he
deliv
a
long
,
exstens
rant
to
some
NSA
agent
about
the
downsid
of
work
for
them
.

I
like
the
relationship
between
some
of
the
peopl
,
like
Will
and
Skylar
,
Will
and
Sean
,
Sean
and
Lambeau
,
and
Will
and
hi
friend
.

There
ar
scene
between
them
which
ar
just
amaz
to
watch
.

And
there
ar
even
moment
in
the
film
which
ar
so
honest
in
their
portray
,
that
I
felt
rightfulli
uncomfort
.

Like
the
break-up
scene
between
Will
and
Skylar
,
Will
and
Sean
's
first
meet
,
a
bar
scene
between
Sean
and
Lambeau
,
and
a
scene
where
Sean
tell
in
detail
how
he
doe
n't
regret
meet
hi
first
wife
,
who
would
later
suffer
a
slow
,
pain
death
,
and
leav
him
lone
and
slightli
bitter
.

Howev
,
the
main
flaw
of
the
film
is
there
's
too
much
of
everyth
,
and
not
enough
of
it
either
.

The
film
tri
for
deeper
relat
between
the
charact
,
but
thei
're
sometim
either
overwritten
or
underwritten
.

Take
for
exampl
the
relationship
between
Sean
and
Will
:
the
film
get
an
interest
father/son
as
well
as
doctor/pati
relationship
go
with
out
ani
problem
,
but
when
it
tri
to
show
them
as
equal
,
it
fall
on
it
face
.

We
hear
how
thei
went
to
the
same
town
,
and
there
ar
even
scene
where
the
film
show
how
hypocrit
both
of
them
ar
at
time
.

But
thi
never
take
off
like
the
other
part
of
their
relationship
do
.

Also
,
Will
's
relationship
with
hi
best
friend
,
Chucki
-LRB-
Affleck
-RRB-
,
ha
a
nice
climax
,
but
not
enough
rise
action
.

We
see
them
joke
around
,
but
when
Chucki
arriv
at
a
decis
between
the
two
of
them
,
it
seem
more
like
a
superfici
reason
than
a
more
select
on
.

Their
relationship
never
goe
beyond
``
just
best
friend
,
''
and
hi
decis
just
seem
to
be
a
gener
of
ani
two
best
friend
instead
of
someth
more
...
human
.

Mayb
it
wa
just
me
.

And
it
also
goe
for
a
parallel
with
the
relationship
between
Sean
and
Lambeau
,
but
that
,
again
,
is
underwritten
.

And
,
of
cours
,
as
in
most
film
like
thi
,
the
girl/boi
relationship
is
pretti
underwritten
.

We
never
understand
what
thei
see
in
each
other
,
so
when
Skylar
confess
that
she
love
him
,
it
seem
more
like
it
's
there
for
plot
detail
that
anyth
els
.

Minni
Driver
is
interest
,
though
,
and
breath
life
into
her
charact
.

The
write
is
a
bit
at
fault
here
,
but
what
make
all
thi
worthwhil
is
the
littl
moment
in
the
film
where
it
acheiv
true
awesom
.

Sure
,
it
feel
overstuf
,
but
it
's
extrem
enjoy
.

The
dialogu
that
Affleck
and
Damon
have
written
is
amazingli
fresh
,
and
seem
extrem
human
.

I
think
that
thei
should
get
a
nomin
for
their
script
chiefli
becaus
of
the
dialogu
,
which
is
amaz
.

While
thi
is
probabl
director
Gu
Van
Sant
's
most
conserv
film
-LRB-
you
'd
hardli
know
it
's
the
same
director
of
``
To
Die
For
,
''
the
onli
Van
Sant
film
I
've
seen
,
realli
-RRB-
,
it
's
still
a
pretti
remark
film
,
albeit
a
tad
overr
-LRB-
what
's
thi
``
best
pictur
''
deal
?
-RRB-
.

And
if
a
film
put
a
smile
on
my
face
despit
posess
a
lot
of
flaw
,
I
have
no
problem
with
reccomend
it
to
anyon
.

