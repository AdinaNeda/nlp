Everi
onc
in
a
while
a
movi
come
along
that
complet
redefin
the
genr
:
with
drama
,
it
wa
Citizen
Kane
,
with
arthous
it
wa
Pulp
Fiction
,
and
with
comedi
it
wa
,
well
,
that
Jim
Carrei
gui
-LRB-
okai
,
so
he
's
not
a
movi
,
but
he
DID
have
a
huge
influenc
on
the
genr
.

Not
to
mention
an
expens
on
.
-RRB-

Sometim
a
movi
even
combin
them
all
into
a
big
,
sprawl
motion
pictur
event
,
as
did
Forrest
Gump
four
year
ago
.

With
action
film
,
it
wa
Alien
,
whic
wa
releas
to
much
hype
seven
year
after
it
's
equally-innov
parent
,
Alien
-LRB-
1979
-RRB-
.

Direct
and
written
by
Jame
Cameron
-LRB-
T2
:
Judgement
Dai
,
The
Abyss
,
True
Li
-RRB-
,
THE
author
on
action
film
,
it
wa
a
master
encor
to
hi
sci-fi
thriller
The
Termin
-LRB-
1984
-RRB-
.

While
the
origin
Alien
film
wa
a
dark
,
enclos
horror
film
that
featur
on
alien
slowli
massac
a
horrifi
crew
,
Jame
Cameron
took
the
big-budget
action
film
with
Alien
,
which
featur
multipl
alien
do
basic
the
same
thing
,
although
on
a
much-larg
scale
.

And
boi
,
did
he
take
that
rout
!

I
'd
sai
at
about
165
mph
or
so
...
The
film
open
57
year
after
the
origin
,
with
Lt.
Riplei
-LRB-
Weaver
-RRB-
be
found
in
her
ship
in
a
cryogen
state
by
a
salvag
vessel
.

If
you
'll
recal
,
at
the
end
of
Alien
Riplei
,
the
onli
surviv
member
,
cryogen
``
hibern
''
herself
after
expel
the
rogu
alien
from
her
ship
.

Unfortun
,
she
thought
she
'd
onli
be
out
for
a
coupl
of
week
...
Once
she
's
return
to
earth
,
Riplei
is
quickli
interrog
by
``
the
compani
''
,
who
quickli
dismiss
her
and
her
stori
as
lunaci
.

In
truth
,
thei
believ
her
,
as
thei
soon
approach
Riplei
with
an
offer
to
travel
with
some
Marin
to
a
new
coloni
planet
as
an
``
alien
advisor
''
.

It
seem
that
the
coloni
planet
wa
a
once-breed
ground
for
the
nasti
alien
,
and
now
all
commun
with
the
planet
ha
been
lost
...
It
doe
n't
exactli
take
a
geniu
to
guess
what
happen
next
:
Riplei
agre
,
and
befor
you
can
sai
``
big
mistak
''
,
she
and
the
half
dozen
marin
,
plu
the
slimi
corpor
gui
-LRB-
Reiser
-RRB-
,
who
ha
more
than
it
look
like
up
under
hi
sleev
,
ar
off
to
the
coloni
.

When
thei
arriv
,
thei
find
the
planet
in
ruin
.

Only
on
survivor
is
found
,
a
littl
girl
,
Newt
,
who
confirm
that
,
ye
,
the
alien
were
here
and
that
she
onli
manag
to
surviv
by
hid
in
the
ventil
system
.

And
soon
enough
,
the
marin
come
under
attack
from
the
alien
...
What
happen
for
the
next
hour
and
a
half
or
so
is
what
complet
set
thi
movi
apart
from
ani
other
standard
alien
sci-fi
movi
:
the
action
scene
.

Cameron
direct
them
so
skillfulli
,
and
so
suspensefulli
,
that
you
're
liter
ring
your
hand
by
the
time
the
final
roll
around
.

Which
featur
,
in
my
opinion
,
the
best
fight
scene
ever
record
on
film
,
as
Riplei
strap
herself
into
a
huge
robot
and
battl
the
nasti
Queen
alien
to
the
death
.

Mani
peopl
will
tell
you
that
thi
film
,
while
be
a
great
action
film
,
ha
no
real
drama
and
is
all
clich
.

Well
,
thei
would
be
wrong
,
my
friend
.

If
thi
film
had
no
``
drama
''
,
then
why
wa
Sigournei
Weaver
nomin
for
Best
Actress
at
the
1987
Academi
Award
?

That
's
right
,
best
actress
.

You
know
that
ANY
action
film
that
ha
an
oscar
nomin
attach
to
it
for
someth
other
than
technic
stuff
like
edit
and
F/X
ha
GOT
to
be
good
.

In
short
,
Alien
combin
all
the
right
element
-LRB-
great
action
and
F/X
,
drama
,
a
good
plot
,
good
dialogu
,
and
great
villain
-RRB-
into
what
could
arguabl
be
call
the
best
action
film
of
all
time
.

Then
again
,
mayb
not
.

Movi
rise
and
fall
from
glori
and
,
sad
to
sai
,
Alien
wa
wrestl
from
it
's
throne
of
Best
Action
Movi
by
anoth
Cameron
film
,
T2
:
Judgement
Dai
,
in
1991
.

So
who
will
be
the
next
king
?

Well
,
let
's
wait
until
Decemb
19th
and
see
yet
anoth
Jame
Cameron
film-th
highest
budget
film
of
all
time-Titan
to
make
that
decis
.

I
ca
n't
wait
.

