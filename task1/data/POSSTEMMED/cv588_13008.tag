One
of
the
funniest
Carri
on
movi
and
the
third
with
a
medic
set
.

The
main
stori
is
about
Dr.
Jame
Nookei
-LRB-
Jim
Dale
-RRB-
who
is
dislik
by
hi
superior
,
Frederick
Carver
,
head
of
the
hospit
-LRB-
Kenneth
William
-RRB-
,
and
Dr.
Ernest
Stoppidg
-LRB-
Charl
Hawtrei
-RRB-
who
seem
to
dislik
everyon
.

Thei
attempt
to
caus
him
as
much
troubl
in
the
hospit
as
possibl
and
all
the
blame
is
laid
on
Nookei
.

Carver
learn
of
a
possibl
job
opportun
from
wealthi
Ellen
Moor
-LRB-
Joan
Sim
-RRB-
on
a
distant
tropic
island
and
get
Nookei
trannsfer
there
.

When
Nookei
arriv
at
thi
island
,
he
meet
Gladston
Screwer
-LRB-
Sid
Jame
-RRB-
who
ha
live
on
the
island
just
about
all
hi
life
.

Screwer
is
in
possess
of
a
wonder
slim
drug
made
from
natur
ingredi
.

Dr.
Nookei
return
with
the
drug
to
England
and
set
up
a
new
slim
busi
with
Ellen
Moor
's
financi
back
.

Howev
,
Nookei
and
Moor
face
opposit
from
Screwer
who
goe
to
England
to
cash
in
on
the
drug
,
and
Carver
and
Stoppidg
who
want
to
know
the
ingredi
to
creat
a
rival
drug
.

The
perform
ar
all
veri
commend
.

Sid
Jame
ha
a
slightli
ambigu
charact
to
plai
thi
time
and
he
excel
in
hi
role
.

Kenneth
William
,
as
usual
,
plai
the
unpopular
and
cun
head
of
the
hospit
.

Joan
Sim
is
entertain
as
the
wealthi
benefactor
and
Jim
Dale
should
realli
head
the
cast
with
hi
excel
portray
of
bumbl
Nookei
.

Charl
Hawtrei
plai
an
'
out-of-charact
'
role
as
Dr.
Stoppidg
,
the
nasti
and
evil
practition
who
want
to
knock
Nookei
off
hi
perch
.

Some
critic
have
said
that
thi
combin
of
Hawtrei
and
Stoppidg
doe
not
work
well
,
but
I
think
that
it
at
least
give
Hawtrei
a
new
part
to
plai
instead
of
the
usual
dimwit
charact
.

As
a
matter
of
fact
,
Hawtrei
is
suitabl
cun
in
the
part
.

Other
regular
in
the
movi
includ
Hatti
Jacqu
as
the
Matron
yet
again
-LRB-
but
thi
part
is
probabl
her
most
bore
'
Matron
'
-RRB-
and
Barbara
Windsor
as
Nookei
's
love
interest
and
unoffici
'
Women
's
lib
'
campaign
,
Goldi
Lock
.

Both
Jacqu
and
Windsor
plai
their
part
well
.

Patsi
Rowland
appear
briefli
as
Miss
Fosdick
,
and
there
ar
veri
small
cameo
from
Wilfrid
Brambel
-LRB-
from
'
Stepto
and
Son
'
fame
-RRB-
and
Peter
Butterworth
.

The
joke
ar
mostli
veri
funni
,
the
stori
is
quit
appeal
,
and
the
regular
seem
interest
in
what
thei
're
do
.

Although
it
is
anoth
medic
Carri
on
movi
and
some
of
the
joke
ar
borrow
from
previou
movi
,
it
still
come
out
on
top
-LRB-
it
is
the
best
medic
Carri
On
-RRB-
becaus
of
the
abov
factor
.

A
must-se
Carri
on
movi
.

