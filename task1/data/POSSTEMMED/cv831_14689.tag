RAZOR
BLADE
SMILE
Run
as
part
of
the
Vancouv
Internat
Film
Festiv
Plai
October
2nd
and
4th
,
1998
.

Official
releas
:
Halloween
`
You
think
you
know
all
about
vampir
...
believ
me
...
you
know
f*ck
all
.
'

With
these
word
,
we
ar
brought
into
the
world
of
a
sultri
vampir
seductress
by
the
name
of
Lilith
Silver
-LRB-
plai
by
Eileen
Dali
-RRB-
.

Made
into
an
immort
a
centuri
and
a
half
ago
by
the
sinist
Ethan
Blake
,
she
continu
to
allevi
boredom
with
her
state
of
be
.

So
,
what
's
an
undead
girl
to
do
,
except
squeez
into
the
tightest
fit
cloth
possibl
,
get
out
her
gun
,
seduc
a
few
gui
,
and
kill
a
few
peopl
for
monei
and
blood
?

With
that
out
of
the
wai
,
let
me
sai
thi
.

Thi
film
is
bad
.

Thi
film
is
realli
,
realli
bad
.

Yet
somehow
,
it
is
strang
enjoy
.

With
all
the
element
of
a
direct
to
video
horror
flick
buxom
babe
,
gore
,
cheesi
dialogu
,
and
bad
act
ani
casual
film
viewer
would
be
more
inclin
to
spend
their
monei
on
rent
Innocent
Blood
or
go
to
see
Blade
.

Still
,
it
is
consid
to
be
on
of
the
finest
British
vampir
film
.

Agreed
,
it
might
be
,
but
is
far
from
be
the
best
vampir
flick
ever
.

Period
.

Razor
Blade
Smile
ha
some
rather
high
product
valu
,
shown
by
the
nice
overal
tone
to
the
film
.

The
film
is
also
a
bit
of
a
departur
from
popular
vampir
lore
,
with
a
vampir
that
can
go
out
in
broad
daylight
without
disintegr
,
doe
n't
sleep
in
a
coffin
-LRB-
but
leav
on
next
to
her
comput
-RRB-
,
and
see
religion
as
the
`
opium
of
the
peopl
'
-LRB-
henc
,
immun
to
cross
-RRB-
.

But
asid
from
that
,
it
doe
n't
strai
too
far
awai
from
horror
film
convent
.

That
is
,
unless
that
is
intent
.

Razor
Blade
Smile
liter
goe
for
the
throat
,
then
pump
the
wound
full
of
process
chees
.

You
ca
n't
help
but
laugh
when
a
femal
vampir
get
decapit
and
her
sever
head
goe
fly
into
a
puddl
.

You
'll
laugh
even
harder
at
the
hideous
contriv
situat
-LRB-
to
distract
two
femal
guard
,
Lilith
toss
a
cellular
phone
toward
them
and
dial
the
number
-RRB-
.

You
'll
laugh
the
hardest
when
a
murder
wit
recant
the
tale
and
suddenli
start
sob
uncontrol
.

And
then
,
there
's
the
sudden
twist
end
-LRB-
I
have
enough
heart
not
to
give
it
awai
-RRB-
.

The
film
refus
to
take
itself
serious
,
which
is
a
good
thing
,
as
by
the
time
Lilith
zip
up
her
skin-tight
bodi
suit
and
then
unzip
it
just
to
show
the
right
amount
of
cleavag
,
you
've
most
like
forgotten
the
main
point
of
the
stori
.

But
in
case
you
realli
care
,
she
's
been
assign
to
kill
a
bunch
of
peopl
who
belong
to
the
so-cal
Illuminati
,
and
she
's
leav
a
huge
mess
behind
for
the
polic
to
find
.

As
much
as
my
critic
,
thi
movi
work
best
as
a
guilti
pleasur
or
as
a
film
that
you
watch
with
your
friend
and
within
group
.

Almost
destin
for
cult-statu
,
the
film
's
campi
,
cheesi
overton
actual
work
toward
the
film
.

While
thi
mai
not
necessarili
be
the
film
you
'd
want
to
be
seen
watch
by
yourself
,
it
make
for
a
fun
time
at
the
movi
.

Rate
:
7/10
--
_________________________________________________________________
|
E-mail
address
alter
to
avoid
unsolicit
junkmail
.

|
|
Remov
asterix
mark
-LRB-
*
-RRB-
to
repli
by
e-mail
|
_________________________________________________________________

