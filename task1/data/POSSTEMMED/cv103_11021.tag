I
think
that
ani
compet
member
of
the
human
race
who
's
ever
seen
a
movi
--
ani
movi
--
could
probabl
predict
just
about
everi
turn
in
The
Wed
Singer
.

Even
though
I
try
veri
hard
to
not
predict
film
as
I
'm
watch
them
-LRB-
especi
not
romant
comedi
-RRB-
,
the
plot
of
thi
particular
film
just
advanc
befor
me
second
,
and
sometim
minut
,
befor
I
actual
saw
it
happen
on
screen
.

But
I
do
n't
care
.

Not
even
a
littl
.

My
reason
for
thi
ar
simpl
:
1
-RRB-
Adam
Sandler
is
great
.

2
-RRB-
Drew
Barrymor
is
great
.

3
-RRB-
The
movi
is
so
funni
.

There
ar
part
that
left
me
gasp
for
air
becaus
I
wa
laugh
so
hard
.

I
have
n't
laugh
thi
hard
in
a
movi
sinc
Austin
Power
.

Sandler
plai
Robbi
Hart
,
a
wed
singer
who
onc
had
dream
of
be
in
a
rock
band
and
write
hi
own
music
.

In
the
open
scene
,
he
seem
like
a
happy-go-lucki
wed
singer
,
at
least
partial
enjoi
hi
profess
and
do
a
rather
good
job
of
talk
down
a
realli
drunk
individu
-LRB-
Steve
Buscemi
-RRB-
who
decid
that
hi
brother
's
wed
is
a
good
place
to
tell
the
whole
world
that
hi
life
,
up
to
that
point
,
wa
utter
pointless
.

A
week
later
,
at
hi
own
wed
,
he
is
left
stand
at
the
altar
by
hi
ex-fiancé
,
Linda
-LRB-
Angela
Featherston
-RRB-
.

With
hi
life
in
complet
disarrai
,
he
meet
Julia
-LRB-
Barrymor
-RRB-
,
a
waitress
.

She
is
engag
to
Glenn
-LRB-
Matthew
Glave
-RRB-
,
and
is
to
marri
him
in
about
three
month
.

Julia
and
Robbi
becom
good
friend
when
she
enlist
him
to
help
with
the
wed
plan
.

Soon
-LRB-
and
predict
-RRB-
it
becom
pretti
clear
that
Glenn
is
a
jerk
,
and
that
Robbi
and
Julia
realli
love
each
other
.

Oh
sure
,
it
's
trite
.

And
if
anyon
make
thi
film
expect
us
to
,
like
,
feel
suspens
or
someth
while
watch
The
Wed
Singer
,
then
thei
truli
underestim
their
audienc
.

What
make
the
stori
bearabl
,
asid
from
the
terrif
costum
and
80
music
,
ar
the
perform
.

Barrymor
is
simpli
ador
here
-LRB-
I
've
never
seen
her
look
so
close
to
the
wai
she
did
in
ET
-RRB-
.

Not
onli
is
she
pleasant
to
look
at
,
but
her
act
job
here
realli
is
a
good
on
--
and
it
doe
n't
hurt
that
the
charact
is
likabl
and
adequ
well-written
.

And
the
Jerk
,
plai
in
full-forc
by
Grave
,
is
just
as
despic
as
ani
villain
in
ani
romant
comedi
I
've
ever
seen
.

Natur
,
the
true
star
here
is
Sandler
.

I
think
the
gui
is
flat-out
hilari
.

He
can
sai
someth
that
just
is
n't
funni
,
and
hi
deliveri
make
me
laugh
.

Hi
comed
perform
here
equal
that
of
Happi
Gilmor
,
but
in
thi
film
he
goe
much
further
and
prove
to
me
that
he
can
,
inde
,
hold
hi
own
as
a
lead
man
in
a
romant
comedi
.

Wherea
hi
role
in
hi
previou
film
kind
of
had
Sandler
plai
Sandler
,
The
Wed
Singer
give
him
a
charact
that
,
for
the
first
time
,
he
actual
ha
to
fit
into
.

As
far
as
I
'm
concern
he
succeed
,
and
thi
film
is
testimoni
that
he
doe
,
in
fact
,
have
some
rang
in
hi
talent
.

And
the
movi
is
just
so
funni
.

Perhap
the
best
moment
is
when
he
sing
a
song
for
Julia
that
he
claim
is
a
littl
uneven
becaus
he
began
write
it
while
in
a
good
mood
and
finish
it
after
hi
fiancé
abandon
him
.

Thi
is
the
part
that
liter
had
me
grope
for
a
breath
of
air
.

And
while
the
film
,
like
all
romant
comedi
,
take
a
hiatu
from
laugh
toward
the
end
becaus
the
plot
ha
to
finish
up
,
there
ar
more
than
enough
truli
hilari
moment
in
the
first
hour
that
make
up
for
ani
slump
in
progress
dure
the
second
half
.

My
formal
complaint
for
The
Wed
Singer
ar
n't
veri
import
.

The
film
is
predic
,
but
who
care
?

The
charact
ar
extrem
likabl
,
the
movi
is
ridicul
funni
,
and
the
experi
is
simpli
enjoy
.

In
addit
to
thi
,
I
ca
n't
imagin
anyon
see
the
preview
and
not
want
to
see
the
film
.

In
that
order
,
I
conclud
that
it
is
pointless
for
me
to
even
have
written
thi
review
.

I
just
want
everyon
to
know
that
I
laugh
.

A
lot
.

