In
1912
,
a
ship
set
sail
on
her
maiden
voyag
across
the
Atlantic
for
America
.

Thi
ship
wa
built
to
be
the
largest
ship
in
the
world
,
and
she
wa
.

She
wa
also
build
to
be
on
of
the
most
luxuri
,
and
that
she
wa
.

Final
,
she
wa
built
to
be
unsink
and
that
unfortun
she
wa
not
.

To
get
a
ticket
for
thi
voyag
you
either
:
spent
a
life
's
save
to
get
to
America
to
start
life
anew
,
were
part
of
the
upper
class
and
had
the
monei
to
spare
,
or
final
were
lucki
enough
to
have
a
full
hous
in
a
poker
match
by
the
dock
like
Jack
Dawson
.

Jack
Dawson
make
the
trip
,
and
happen
to
be
at
the
right
place
at
the
right
time
.

Rose
DeWitt
Bukat
,
a
first
class
passeng
,
climb
the
rail
at
the
aft
of
the
ship
with
thought
of
jump
.

Thu
is
start
a
tale
of
romanc
and
intrigu
,
and
a
tale
of
death
and
tragedi
...
Thi
movi
is
about
a
tragic
event
that
took
place
a
great
mani
year
ago
,
an
even
that
should
not
be
taken
lightli
as
ani
other
bit
of
histor
trivia
.

The
movi
Titan
show
what
happen
,
mayb
not
with
a
100
%
degre
of
accuraci
,
but
it
still
show
it
veri
realisticali
.

Now
the
Titan
is
both
a
stori
on
it
own
and
a
backdrop
for
a
stori
.

It
serv
as
both
admir
,
brine
forth
an
interest
stori
that
although
simpl
in
it
most
simpl
premis
is
veri
captiv
.

Thi
movi
is
veri
emot
simpli
becaus
of
what
it
is
,
but
that
alon
is
not
enough
.

The
stori
is
brought
out
with
a
certain
style
that
make
is
so
much
more
emot
and
so
much
more
effect
.

Movi
such
as
thi
will
not
be
forgotten
all
too
quickli
and
unfortun
then
ar
not
someth
that
is
produc
by
Hollywood
with
ani
great
frequenc
.

The
attent
to
detail
that
wa
paid
is
remark
.

The
whole
premis
for
the
tell
of
the
stori
is
interest
,
with
the
show
of
brand
new
footag
from
the
wreck
of
the
Titan
ad
much
flavor
to
an
alreadi
good
movi
.

Part
of
the
magic
chemistri
behind
thi
stori
is
the
act
,
and
for
thi
movi
it
extrem
good
act
from
the
whole
cast
.

The
perform
put
in
by
the
main
star
is
someth
to
be
admir
.

The
charact
were
plai
out
so
memor
that
both
Leonardo
DiCaprip
and
Kate
Winslet
should
receiv
at
the
least
nomin
for
their
role
.

Look
at
the
act
done
in
the
movi
it
seem
as
if
thei
ar
n't
act
but
ar
actual
the
charact
in
the
movi
.

The
cast
for
the
movi
could
realli
have
been
better
,
in
my
humbl
opinion
that
is
.

One
charact
that
will
most
like
not
be
mention
by
ani
other
review
or
commentari
about
thi
film
is
that
of
the
ship
itself
.

Ye
,
you
read
correctli
,
the
ship
is
a
charact
.

How
is
the
Titan
a
charact
?

you
ask
well
simpl
,
a
ship
had
a
certain
charact
about
it
and
as
most
sailor
and
boat
men
will
tell
you
.

Thi
charact
is
everyth
about
the
ship
from
it
specif
to
it
luxuri
and
the
Titan
wa
no
stranger
to
thi
.

Mr.
Cameron
bring
the
ship
to
life
in
an
almost
liter
sens
.

All
thi
add
to
the
movi
in
a
certain
wai
that
most
Hollywood
product
cant
seem
to
grasp
.

Now
,
to
produc
the
effect
that
I
mention
abov
and
to
sink
the
ship
itself
ar
feat
that
ar
accomplish
by
special
effect
wizard
.

The
effect
in
thi
movi
rang
from
marvel
costum
to
beautifulli
render
scene
of
the
ship
sink
.

In
some
respect
you
can
not
tell
that
the
effect
ar
there
,
you
simpli
think
that
what
you
see
is
what
happen
or
what
is
happen
-LRB-
if
your
imagin
is
good
-RRB-
.

The
technic
wizardri
done
in
thi
movi
is
just
spectacular
and
actual
get
new
footag
from
the
wreck
of
the
Titan
is
uniqu
.

The
movi
will
leav
you
amaz
at
the
effect
,
and
that
is
a
feat
sinc
there
is
no
monster
or
alien
in
thi
movi
,
just
human
and
an
overs
ship
.

The
movi
will
amaz
you
and
it
will
pull
on
your
emot
,
the
theater
that
I
went
to
had
a
few
peopl
leav
with
tear
in
their
ey
.

Now
that
is
not
a
feat
accomplish
by
most
movi
,
now
the
fact
that
the
tragedi
actual
occur
is
brought
home
with
someth
of
a
punch
,
I
wont
spoil
the
end
and
sai
what
happen
regardless
it
is
an
interest
movi
from
begin
to
the
veri
end
.

The
histor
valu
of
thi
movi
is
quit
high
,
and
honestli
is
someth
that
should
be
watch
for
the
sake
of
see
it
and
see
the
tragedi
,
for
it
is
extrem
well
done
.

The
method
of
tell
the
stori
is
also
good
,
mayb
not
total
uniqu
but
effect
none
the
less
.

Regardless
of
anyth
mention
abov
thi
movi
is
a
grandios
product
and
the
sheer
size
of
the
project
undertaken
is
someth
to
be
marvel
at
.

The
simpl
fact
that
the
movi
is
smashingli
success
at
what
it
aim
to
achiev
is
just
astonish
.

If
you
get
the
chanc
to
see
thi
movi
GO
!!!

it
might
not
be
the
best
movi
in
the
world
.
.

but
it
rank
fairli
highli
and
is
well
worth
the
time
spent
watch
it
.

Dure
none
of
the
3
hour
and
13
minut
of
the
movi
ar
you
bore
nor
doe
your
attent
wane
from
the
movi
.

