Is
it
just
me
,
or
have
Disnei
film
gradual
lost
their
appeal
?

I
wa
almost
stun
by
the
qualiti
of
``
Who
Frame
Roger
Rabbit
?
''

,
a
fun
,
uniqu
look
at
a
world
where
cartoon
and
peopl
live
togeth
.

``
The
Littl
Mermaid
''
harken
back
to
,
and
perhap
surpass
,
the
classic
Disnei
anim
from
the
first
half
of
the
centuri
.

Howev
,
the
endless
stream
of
``
The
Littl
Mermaid
''
wanna-b
's
is
get
to
me
.

You
know
the
spiel
:
a
lame
plot
recycl
from
some
well-known
stori
,
color
,
almost
psychedel
anim
,
the
lead
charact
who
learn
a
valuabl
moral
lesson
,
the
comed
sidekick/sidekick
,
the
seemingli
unattain
love
interest
,
a
few
catchi
song
,
a
few
in-jok
for
adult
,
and
the
occasion
out
of
place
Warner-Brothers-coyote-fall-off-the-cliff
type
joke
.

It
's
a
formula
,
and
like
ani
formula
,
it
get
tiresom
.

That
's
why
I
'm
delight
to
hear
that
Disnei
recent
pick
up
the
intern
distribut
right
to
the
work
of
Ghibli
,
a
Japanes
cartoon
studio
.

From
what
I
've
seen
of
their
movi
-LRB-
``
Kiki
's
Deliveri
Servic
''
and
the
alreadi
English-dub
and
American-distribut
``
My
Neighbor
Totoro
''
-RRB-
,
foreign
audienc
ar
in
for
a
treat
.

These
movi
,
particularli
``
Kiki
's
Deliveri
Servic
,
''
ar
wonderfulli
origin
and
entertain
.

``
Kiki
's
Deliveri
Servic
''
start
with
an
unusu
premis
:
Kiki
,
upon
hear
forecast
of
clear
ski
,
decid
it
is
time
for
her
to
leav
home
.

Thi
is
unusu
becaus
Kiki
is
n't
a
colleg
student
or
a
young
adult
make
her
start
on
the
world
,
but
a
thirteen
year
old
girl
eager
to
begin
her
train
as
a
witch
.

Her
mother
is
also
a
witch
,
and
as
thirteen
is
the
obligatori
ag
for
such
train
,
her
parent
treat
her
leav
understandingli
and
put
up
onli
token
resist
.

Just
the
idea
of
a
witch
heroin
mai
scare
awai
some
audienc
,
but
no
mention
is
made
of
the
occult
,
and
the
subject
is
dealt
with
harmlessli
throughout
the
movi
:
Kiki
's
mother
act
as
a
villag
doctor
,
and
Kiki
is
abl
to
do
no
more
than
fly
on
a
broom
.

Kiki
must
choos
a
citi
to
move
to
,
and
what
she
choos
is
the
co-star
of
the
movi
:
A
German
seaport
town
,
far
remov
from
the
countri
life
she
ha
been
us
to
.

Initial
lost
in
the
imperson
huge
of
her
surround
,
she
's
discourag
,
and
unabl
to
support
herself
though
her
rudimentari
skill
as
a
witch
.

On
the
verg
of
leav
for
a
new
town
,
she
us
her
power
of
flight
to
deliv
a
pacifi
to
a
babi
who
drop
it
.

The
favor
eventu
blossom
into
her
work
as
a
courier
,
a
one-person
deliveri
servic
.

Thi
is
where
the
movi
becom
shockingli
good
.

Kiki
's
job
is
a
catalyst
for
her
realiz
the
beauti
of
the
citi
and
the
world
around
her
.

The
townspeopl
she
is
put
in
contact
possess
an
inner
worth
that
doe
n't
seem
forc
or
artifici
,
but
mere
deriv
from
look
at
ordinari
peopl
in
a
differ
light
.

Thei
're
three-dimension
,
imposs
to
quickli
evalu
,
possess
of
fault
,
and
understand
reserv
in
kind
toward
a
strang
girl
.

Howev
,
each
on
ha
someth
about
them
,
some
interest
or
trait
,
that
make
them
interest
and
uniqu
.

The
person
beauti
of
the
citizen
is
compliment
by
the
beauti
of
the
surround
:
the
seaport
town
genuin
look
impress
,
captur
the
European
charm
of
a
mediev
citi
adapt
to
the
more
modern
need
of
the
1950
's
.

Thi
beauti
is
beauti
with
depth
;
for
everi
detail
left
in
,
dozen
ar
onli
hint
at
.

I
could
probabl
go
on
all
dai
,
but
I
wo
n't
;
suffic
it
to
sai
that
mani
of
these
minor
detail
could
have
been
the
highlight
of
the
movi
.

Some
of
my
favorit
ar
Kiki
's
relationship
with
the
baker
's
husband
:
although
he
's
quiet
,
never
shown
speak
to
Kiki
throughout
the
movi
,
and
he
initi
scare
her
,
hi
kind
win
her
friendship
.

In
the
garag
of
a
delivere
's
hous
can
be
found
a
vintag
Model
T.
Two
old
ladi
have
a
relationship
evolv
from
master-serv
to
friend-friend
.

Such
detail
give
the
impress
that
the
beauti
shown
in
the
movi
is
onli
the
tip
of
the
iceberg
.

Added
on
top
of
thi
is
a
lesson
on
the
import
of
self-worth
.

Kiki
initi
seem
to
deriv
much
of
her
self-worth
from
the
beauti
of
her
surround
and
the
peopl
around
her
,
but
an
encount
with
a
rude
girl
crash
down
her
bewonder
,
make
her
question
the
import
of
such
beauti
.

Thi
caus
Kiki
to
lose
faith
in
herself
.

Howev
,
Kiki
come
to
realiz
not
onli
the
beauti
of
her
surround
,
but
the
innat
beauti
insid
her
.

See
Kiki
regain
faith
in
herself
is
marvel
,
and
the
princip
devic
us
to
do
it
is
so
sublim
that
I
do
n't
even
want
to
hint
at
it
.

The
end
of
the
movi
ha
a
subtl
charm
combin
with
a
sens
of
aw
and
,
iron
,
the
most
Disney-esqu
moment
I
've
ever
seen
;
if
you
do
n't
hold
your
breath
in
wonder
you
might
want
to
get
medic
attent
.

The
movi
steer
free
of
mani
of
the
Disnei
convent
:
there
is
no
violenc
,
howev
cartoonish
,
no
song-and-d
number
,
and
Kiki
's
pet
cat
Jiji
is
more
a
friend
than
a
comed
sidekick
,
although
Jiji
is
given
mani
funni
line
and
scene
.

Howev
,
I
would
be
surpris
if
Disnei
doe
n't
give
Jiji
more
joke
,
or
at
least
the
voic
of
some
comedian
.

Refreshingli
enough
,
the
moral
lesson
of
the
movi
is
not
self-contradict
,
a
habit
Disnei
movi
have
gotten
into
.

``
Beauti
and
the
Beast
''
's
beauti
Bell
,
for
instanc
,
learn
that
beauti
wa
deriv
from
a
person
's
insid
,
and
wa
then
reward
by
have
the
beast
turn
into
a
handsom
princ
in
snazzi
cloth
.

Kiki
,
on
the
other
hand
,
learn
to
separ
her
wardrob
from
her
self-imag
,
and
noth
more
is
made
of
her
earli
desir
for
better
cloth
than
illustr
her
increas
matur
at
the
end
of
the
film
.

Another
differ
with
Disnei
movi
is
in
the
style
of
anim
for
``
Kiki
's
Deliveri
Servic
''
:
not
as
color
and
dynam
as
Disnei
anim
can
be
,
but
with
a
greater
attent
to
realism
and
detail
,
it
fit
the
movi
to
a
T
.

Thi
is
particularli
true
with
scene
of
Kiki
's
flight
.

The
extrem
realism
of
the
bird
in
flight
near
Kiki
is
magnific
to
behold
,
and
help
these
scene
captur
the
wonder
of
flight
better
than
ani
other
film
I
've
ever
seen
.

I
'm
not
sure
when
or
how
thi
movi
will
be
releas
in
the
Unite
State
.

Disnei
tent
plan
a
theater
releas
of
``
Monoko
Hime
''
shortli
follow
the
Summer
'97
Japanes
releas
,
and
should
start
releas
the
other
Ghibli
film
after
that
.

I
'd
guess
that
some
will
be
releas
on
video
and
some
will
be
theatric
releas
in
peak
season
with
no
compet
Disnei
releas
.

Although
the
wait
is
unfortun
,
it
's
good
to
know
that
it
's
be
releas
at
all
,
for
``
Kiki
's
Deliveri
Servic
''
is
definit
a
movi
worthi
of
widespread
promot
and
releas
.

Watch
thi
movi
.

Along
with
``
Man
Bite
Dog
,
''
and
perhap
someth
by
Woodi
Allen
,
thi
is
the
best
movi
I
've
ever
seen
.

--
-LSB-
1
-RSB-
****
jdrutsch@ucdavis.edu
***********************
Jeff
Rutsch
*****************

