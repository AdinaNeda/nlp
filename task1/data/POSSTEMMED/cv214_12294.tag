Capsul
:
Suprisingli
more
of
a
comedi
than
a
straight
action
flick
,
which
is
n't
necessarili
a
bad
thing
.

Not
exactli
Oscar
calib
,
but
on
helluva
bullet-riddl
good
time
.

Extend
Review
:
You
know
,
I
rememb
when
hitmen
were
evil
,
murder
scum
.

Ala
,
the
time
ar
a-changin
'
.

In
a
recent
string
of
movi
,
hitmen
ar
suddenli
wise-crack
,
fun-lov
killers-with-heart
.

Thi
bring
us
to
Hong
Kong
director
Kirk
Wong
's
first
American
featur
,
The
Big
Hit
.

Oddly
enough
,
about
the
same
time
last
year
a
similar
film
,
Gross
Point
Blank
,
wa
releas
.

Advertis
as
a
quirki
comedi
with
hint
of
action
,
it
turn
out
to
have
a
supris
dosag
of
it
.

The
Big
Hit
is
quit
the
opposit
.

It
wa
hype
as
``
the
new
film
from
produc
John
Woo
''
,
so
on
would
it
expect
lot
of
styliz
kill
and
action
.

Howev
,
there
's
a
sore
lack
of
it
,
which
is
about
the
onli
thing
wrong
with
The
Big
Hit
.

The
film
start
out
with
Mel
Smilei
and
hi
cohort
do
a
job
on
a
white
slaver
.

Mel
,
plai
by
Mark
Wahlberg
in
a
dopei
,
milquetoast
role
,
is
a
kill
machin
;
he
flip
,
spin
,
even
breakdanc
whilst
pop
cap
.

Sadli
,
he
doe
n't
get
a
chanc
to
do
much
of
it
.

Except
for
the
begin
set
piec
and
the
lasttwenti
minut
or
so
,
the
film
is
in
comedi
mode
.

The
action
,
at
least
what
there
is
of
it
,
is
prime
cut
stuff
.

Wong
,
after
numer
Hong
Kong
featur
,
make
quit
a
nice
U.S.
debut
.

Howev
,
hi
pace
is
a
bit
off
,
with
the
action
sequenc
onli
bookend
the
movi
and
not
last
long
enough
.

Thei
start
off
electrifi
and
fresh
,
but
just
kinda
stop
.

Normal
,
thi
would
hamper
a
movi
to
the
point
of
be
unenjoy
.

Luckili
,
we
have
Ben
Ramsei
's
screenplai
,
a
bitingli
funni
piec
of
work
.

The
onli
problem
is
there
might
be
too
much
humor
,
on
joke
make
you
laugh
so
hard
you
miss
the
next
few
.

Some
of
best
gag
includ
an
Oriental
film
maker
down
on
hi
luck
and
on
of
Mel
's
hitmen
pal
that
ha
just
discov
onan
.

The
onli
problem
is
how
some
of
the
minor
charact
ar
handl
,
some
be
there
onli
for
a
laugh
,
which
sometim
work
,
sometim
doe
n't
.

Overal
,
The
Big
Hit
mai
have
it
's
flaw
,
but
it
make
up
for
them
in
a
stylishli
direct
,
gut-wrenchingli
funni
joyrid
.

Defin
on
of
the
better
wai
to
spend
two
hour
.

