KOLYA
is
on
of
the
richest
film
I
've
seen
in
some
time
.

Zdenek
Sverak
plai
a
confirm
old
bachelor
-LRB-
who
's
like
to
remain
so
-RRB-
,
who
find
hi
life
as
a
Czech
cellist
increasingli
impact
by
the
five-year
old
boi
that
he
's
take
care
of
.

Though
it
end
rather
abruptli
--
and
I
'm
whine
,
'caus
I
want
to
spend
more
time
with
these
charact
--
the
act
,
write
,
and
product
valu
ar
as
high
as
,
if
not
higher
than
,
compar
American
drama
.

Thi
father-and-son
delight
--
Sverak
also
wrote
the
script
,
while
hi
son
,
Jan
,
direct
--
won
a
Golden
Globe
for
best
Foreign
Languag
film
and
,
a
coupl
dai
after
I
saw
it
,
walk
awai
an
Oscar
.

