Of
the
major
horror
sub-genr
,
vampir
movi
ar
easili
the
most
adapt
.

Think
about
it
.

Frankenstein
movi
have
n't
chang
all
that
much
becaus
thei
still
need
that
mad-scientist-in-Bavarian-castl
set
.

If
you
made
a
Frankenstein
movi
todai
,
you
'd
have
to
have
the
monster
be
a
viru
or
someth
--
and
outsid
of
Michael
J.
Fox
,
there
ar
n't
ani
actor
that
could
plai
a
viru
convincingli
.

Movi
about
mummi
would
still
have
to
be
set
in
Egypt
.

There
's
not
much
you
can
do
with
werewolv
except
take
them
to
London
or
Pari
,
unless
you
want
to
have
a
teenag
werewolf
or
someth
.

-LRB-
And
there
's
that
Michael
J.
Fox
thing
again
.
-RRB-

But
you
can
have
vampir
do
anyth
,
and
you
can
place
them
in
ani
genr
you
want
,
just
about
.

You
can
have
tradit
vampir
movi
-LRB-
Bram
Stoker
's
Dracula
-RRB-
or
have
the
Anne
Rice
version
-LRB-
Interview
with
the
Vampir
-RRB-
,
or
plai
it
for
laugh
-LRB-
Vampir
in
Brooklyn
-RRB-
.

You
can
put
vampir
in
local
from
sunni
Southern
California
-LRB-
The
Lost
Boi
-RRB-
to
squalid
Mexican
dive
-LRB-
From
Dusk
Till
Dawn
-RRB-
to
your
local
high
school
-LRB-
Buffi
,
the
Vampir
Slayer
-RRB-
.

Were
it
not
redund
,
you
could
have
a
movi
about
a
law
firm
full
of
vampir
.

Any
dai
now
,
I
expect
we
'll
see
a
movi
about
a
vampir
third
baseman
who
ha
to
decid
whether
to
plai
a
dai
game
in
order
to
break
the
home
run
record
.

Or
,
you
can
insert
the
vampir
legend
into
a
Hong
Kong
action
movi
--
and
when
you
do
that
,
you
get
Blade
.

Weslei
Snipe
ha
the
Chow
Yun
Fat
role
in
thi
movi
--
the
silent
,
expressionless
hit
man
who
destroi
everyth
in
hi
path
.

In
thi
case
,
the
everyth
happen
to
be
vampir
that
explod
into
CGI
shard
instead
of
dead
,
bleed
corps
.

Snipe
ha
less
to
sai
here
than
as
the
fugit
in
U.S.
Marshal
,
but
doe
a
demonstr
better
job
here
as
a
half-vampir
wreak
vengeanc
on
the
bloodsuck
.

Blade
is
a
silent
,
brood
presenc
,
lai
wast
to
vampir
without
a
shred
of
remors
.

He
is
as
cold
as
hi
silver-blad
sword
,
as
singl
mind
as
hi
garlic-fil
bullet
.

Yet
,
he
's
not
without
quirk
:
Blade
drive
a
batter
muscl
car
and
ha
to
glean
Rolex
watch
from
hi
vampir
victim
in
order
to
stai
solvent
.

Like
ani
good
Hong
Kong
movi
,
Blade
is
heavi
on
the
chopsocki
action
.

For
some
reason
,
Blade
spend
a
lot
more
time
us
hi
kung
fu
artistri
than
us
the
tradit
anti-vampir
weapon
of
garlic
and
silver
.

-LRB-
Blade
dismiss
a
man-port
arc
lamp
as
be
too
heavi
,
although
it
look
to
be
a
more
effici
tool
to
dispatch
vampir
.
-RRB-

Unfortun
,
the
guid
hand
of
John
Woo
is
absent
from
thi
film
,
releg
Blade
to
the
statu
of
The
Replac
Killer
,
which
it
most
strongli
resembl
.

The
Replac
Killer
is
the
last
movi
I
saw
that
I
did
n't
review
--
mostli
becaus
it
made
no
impress
on
me
,
and
I
could
n't
rememb
anyth
other
than
nois
,
violenc
,
and
the
intens
of
Fat
's
perform
.

Blade
ha
a
littl
more
go
for
it
,
and
ha
the
vampir
myth
to
draw
from
--
but
other
than
that
,
it
's
the
same
kind
of
movi
--
excit
,
with
well
choreograph
action
scene
,
but
with
no
reson
.

With
a
stronger
villain
-LRB-
Stephen
Dorff
is
-LRB-
pardon
the
pun
-RRB-
curious
bloodless
as
the
head
vampir
,
leav
us
to
wonder
what
Deni
Leari
might
have
done
with
the
role
-RRB-
,
a
wittier
script
,
and
a
strong
support
cast
,
Blade
might
have
been
abl
to
rise
beyond
the
level
of
commonplac
summer
entertain
.

As
it
is
,
Blade
is
an
averag
action
movi
that
serv
to
do
noth
but
remind
us
that
the
summer
movi
just
keep
get
dumber
and
the
vampir
movi
just
keep
multipli
.

