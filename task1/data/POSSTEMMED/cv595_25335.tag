Hollywood
is
a
pimp
.

A
fat
,
cigar-smok
chump
wear
a
fur
hat
and
12
gold
chain
around
it
fat
,
hairi
chest
.

All
of
it
star
and
starlet
ar
an
evil
brood
of
scum-suck
vampir
look
for
the
next
percentag
take
,
the
next
summer
blockbust
,
the
next
cast
couch
to
audit
on
.

Pump
out
comic-book
adapt
,
terribl
sequel
to
mediocr
film
,
and
remak
of
foreign
film
to
the
nearest
American
movi
multiplex
mall
theater
equip
with
thin
wall
and
bad
sound
system
.

How
much
longer
can
the
work
of
Peckinpah
,
Fassbind
,
Fuller
,
Castl
,
Preming
,
and
Lee
be
place
and
forgotten
in
the
wrong
section
of
the
local
Blockbust
store
?

How
mani
more
Silver
and
Weinstein
film
can
we
endur
in
thi
stinki
,
decai
state
of
American
cinema
?

But
now
,
from
John
``
I
do
n't
give
a
shit
what
you
think
about
my
movi
''
Water
,
come
the
siren
call
to
all
frustrat
filmmak
and
aficionado
:
Cecil
B.
DeMent
,
a
warp
and
twist
tale
of
how
far
a
filmmak
will
go
to
creat
a
person
vision
of
intern
and
social
revolut
.

Stephen
Dorff
,
in
a
career-defin
role
,
is
Cecil
B.
DeMent
,
a
craze
director
devot
to
make
the
most
radic
underground
film
.

Togeth
with
hi
film
product
cult
,
the
Sprocket
Holes-who
wear
tattoo
of
Peckinpah
,
Lee
,
Fuller
,
Castl
,
Anger
,
Fassbind
,
Preming
on
variou
part
of
their
bodi
as
badg
of
honor
,
thei
kidnap
a
Hollywood
movi
starlet
,
plai
with
perfect
ridicul
by
Melain
Griffith
,
and
forc
her
to
take
the
star
role
in
DeMent
's
film
.

With
no
budget
and
no
contract
for
extra
,
DeMent
and
hi
crew
take
to
the
street
for
product
of
Rave
Beauti
,
a
crass
terrorist
film
about
an
angri
owner
of
an
independ
theater
and
her
brood
out
to
destroi
the
mainstream
film
busi
.

Using
``
ultim
realiti
''
-
with
real
bullet
,
real
peopl
,
and
real
terror-DeM
and
hi
crew
of
misfit
attack
a
mall
theater
,
terror
the
Maryland
Film
Commiss
and
crash
a
movi
studio
shoot
a
certain
sequel
to
a
realli
annoi
Tom
Hank
film
.

Dement
's
crewmemb
ar
maim
and
kill
,
popcorn
machin
ar
us
for
target
practic
,
and
no
on
can
have
sex
until
the
film
is
complet
.

It
's
like
Bowfing
,
onli
,
you
know
,
good
.

The
film
move
with
zig
and
zag
like
the
Magic
Bullet
of
Kennedi
's
assassin
.

The
zeal
of
DeMent
's
caus
catch
quickli
and
convei
the
urgent
messag
of
``
do
someth
,
anyth
,
for
the
accomplish
of
artist
motiv
.
''

The
crewmemb
all
hold
the
quirki
common
in
Water
's
previou
films-Pink
Flamingo
,
Hairsprai
,
Polyest
,
Pecker
--
and
speak
in
the
choppi
,
jade
dialogu
us
frequent
by
Water
.

It
is
as
if
Water
's
script
strip
awai
the
unnecessari
dialogu
common
to
most
pretenti
indi
film
and
just
deliv
the
good
.

Cecil
take
such
warp
avenu
of
express
that
it
seem
like
it
might
actual
outdo
itself
.

You
can
see
how
a
major
studio
might
take
thi
film
,
re-edit
it
,
cut
a
deal
with
the
remain
crew
member
who
ar
still
aliv
,
and
make
a
few
sequel
,
a
la
The
Blair
Witch
Project
.

But
that
's
for
the
futur
.

Overal
,
the
ride
is
fantast
;
it
's
on
of
Water
's
best
film
to
date
and
thi
year
's
Fight
Club
for
filmmak
.

