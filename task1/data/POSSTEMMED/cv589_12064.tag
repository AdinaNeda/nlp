Whenev
writer/director
Robert
Altman
work
in
a
specif
genr
,
he
ha
the
tendenc
to
rewrit
it
on
hi
own
term
.

He
made
the
West
dirti
in
``
McCabe
and
Mrs.
Miller
''
-LRB-
1971
-RRB-
,
he
parodi
hard-boil
detect
stori
in
``
The
Long
Goodby
''
-LRB-
1973
-RRB-
,
and
he
transform
a
cartoon
into
flesh
and
blood
with
``
Popey
''
-LRB-
1980
-RRB-
.

The
same
hold
true
for
hi
most
recent
film
,
``
The
Gingerbread
Man
,
''
which
reinvent
a
genr
that
ha
develop
at
an
exponenti
rate
over
the
last
five
year
:
the
John
Grisham
Movi
.

By
the
time
``
The
Gingerbread
Man
''
wa
complet
,
there
wa
veri
littl
evid
that
it
wa
ever
base
on
a
stori
by
Grisham
.

There
's
no
idealist
young
lawyer
ala
Tom
Cruis
,
Matthew
McConaughei
,
Chri
O'Donnel
or
Matt
Damon
,
and
it
doe
n't
featur
a
great
showdown
in
a
courtroom
.

Instead
of
be
a
grand
od
to
the
power
of
the
legal
profess
,
``
The
Gingerbread
Man
''
is
a
rain-soak
,
Southern
Gothic
noir
thriller
with
wildli
eccentr
charact
,
a
twist
plot
line
,
black
humor
,
a
somewhat
bleak
end
,
and
even
Altman
's
trademark
full-front
femal
nuditi
.

The
central
charact
of
the
film
is
Rick
Magrud
,
who
is
unlik
ani
of
Grisham
's
other
lawyer
hero
.

He
is
older
,
experienc
,
and
rich
;
he
drive
a
cherry-r
$
80,000
Merced
that
never
get
dirti
,
even
in
the
incessantli
pour
rain
of
Savannah
,
Georgia
,
where
the
stori
unfold
.

Rick
is
in
the
midst
of
an
ugli
divorc
with
hi
wife
,
Leeann
-LRB-
Famk
Janssen
-RRB-
,
and
he
rare
get
to
see
hi
two
children
,
Jeff
-LRB-
Jess
Jame
-RRB-
and
Libbi
-LRB-
Mae
Whitman
-RRB-
.

A
reason
is
never
given
for
the
divorc
,
but
it
is
n't
hard
to
surmis
that
Rick
's
woman
had
a
larg
part
in
it
.

Whenev
he
's
late
to
pick
up
the
kid
on
hi
visit
dai
,
Leeann
alwai
assum
it
's
becaus
he
's
been
``
screw
around
.
''

After
an
offic
parti
celebr
anoth
of
hi
court
victori
-LRB-
he
ha
n't
lost
in
eight
year
-RRB-
,
Rick
becom
involv
with
Mallori
Doss
-LRB-
Embeth
Davidtz
-RRB-
,
on
of
the
cater
waitress
.

He
drive
her
home
becaus
her
car
ha
been
stolen
,
and
thei
end
up
in
bed
togeth
.

Rick
find
out
that
Mallori
is
be
terror
and
stalk
by
her
slightli
psychot
father
,
Dixon
Doss
-LRB-
Robert
Duval
-RRB-
,
a
grungi
,
stringi
,
bare-foot
old
man
who
lead
a
commun
of
other
greasi
old
codger
.

It
wa
actual
Dixon
who
stole
her
car
that
night
,
and
when
Rick
ask
why
,
Mallori
repli
that
he
alwai
doe
``
weird
''
stuff
like
that
.

As
anoth
charact
put
it
,
Dixon
is
``
a
few
beer
short
of
six-pack
.
''

Rick
end
up
convinc
Mallori
to
have
Dixon
brought
to
court
and
tri
for
compet
.

He
succe
with
inform
dug
up
by
Clyde
Pell
,
a
privat
investig
friend
-LRB-
Robert
Downei
Jr.
-RRB-
,
and
testimoni
from
Mallori
's
begrudg
ex-husband
,
Pete
Randl
-LRB-
Tom
Bereng
-RRB-
.

Dixon
is
put
in
a
mental
hospit
,
but
hi
commun
buddi
succe
in
break
him
out
.

From
there
,
the
stori
delv
into
kidnap
,
murder
,
double-cross
,
and
even
a
hurrican
that
add
an
omin
cloud
of
constant
violenc
to
the
action
.

Altman
's
cinematograph
,
Changwei
Gu
,
give
the
film
a
dark
,
soak
look
.

If
it
is
n't
night-tim
,
at
least
it
's
rain
.

Gu
shoot
the
interior
,
which
ar
almost
all
dark
wood
panel
,
with
a
minimum
of
light
.

A
great
deal
of
the
action
take
place
in
the
leafi
Georgia
backwood
,
which
Altman
us
to
creat
an
acut
sens
of
dread
and
vulner
.

In
the
citi
there
is
danger
enough
,
but
when
Rick
ha
to
ventur
into
the
wood
,
you
can
almost
feel
him
leav
all
hope
of
civil
behind
as
he
liter
enter
anoth
world
.

Altman
ha
work
in
just
about
everi
conceiv
genr
,
from
western
to
epic
drama
to
comedi
.

But
,
no
matter
what
the
genr
,
he
is
alwai
sure
to
give
it
the
Altman
stamp
,
which
usual
consist
of
all
kind
of
idiosyncrat
quirk
and
littl
detail
that
ar
often
miss
without
repeat
view
.

``
The
Gingerbread
Man
''
is
no
differ
,
although
hi
style
is
much
more
restrain
here
.

Howev
,
without
those
littl
touch
,
the
film
could
have
easili
slump
into
a
routin
action/thril
.

A
great
deal
of
the
credit
for
the
film
's
success
can
be
given
to
the
actor
,
especi
Branagh
,
who
rare
work
outsid
of
period
piec
and
hi
own
direct
.

Here
,
the
British
actor
consist
maintain
a
servic
Southern
drawl
,
while
make
an
essenti
contempt
charact
interest
and
sympathet
.

Rick
is
an
extrem
flaw
man
,
but
Branaugh
bring
real
human
to
hi
charact
.

Without
it
,
the
entir
film
would
fall
flat
becaus
so
much
of
it
is
reliant
on
the
audienc
feel
Rick
's
pain
and
frustrat
.

The
support
actor
also
put
in
fine
perform
,
includ
Embeth
Davidtz
,
who
is
probabl
best
known
for
her
heart-break
role
as
the
Jewish
maid
in
``
Schindler
's
List
''
-LRB-
1993
-RRB-
.

Tom
Bereng
provid
some
gruff
comic
relief
,
and
Robert
Duval
spend
most
of
hi
on-screen
time
just
look
weird
.

It
doe
n't
seem
like
he
doe
much
becaus
he
ha
veri
littl
spoken
dialogu
,
but
watch
him
close
dure
the
film
's
on
courtroom
scene
:
hi
entir
perform
is
in
hi
ey
and
hi
bodi
languag
,
and
few
actor
could
have
pull
it
off
without
be
either
silli
or
overbear
.

While
``
The
Gingerbread
Man
''
is
n't
in
leagu
with
Altman
's
greatest
work
like
``
Nashvil
''
-LRB-
1975
-RRB-
or
``
The
Player
''
-LRB-
1992
-RRB-
,
it
is
nonetheless
a
solid
piec
of
genr
filmmak
,
which
mai
prove
a
success
vehicl
to
restart
hi
somewhat
lag
career
.

Some
critic
love
to
stamp
film
like
thi
as
``
commerci
,
''
as
if
Altman
can
onli
maintain
artist
integr
if
hi
film
ar
aim
at
a
tini
audienc
and
do
n't
make
monei
.

Altman
ha
done
someth
much
better
:
he
took
what
could
have
been
a
gener
movi
,
and
by
invest
artistri
and
effort
,
he
made
it
into
someth
more
.

