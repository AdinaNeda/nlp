The
Andromeda
Strain
is
the
greatest
scienc
fiction
film
ever
made
.

I
know
that
is
veri
sweep
statement
,
so
I
'll
qualifi
it
by
ad
that
The
Andromeda
Strain
is
on
of
the
few
film
made
that
genuin
deserv
the
label
of
``
scienc
fiction
,
''
stori
in
which
specul
scienc
is
at
the
core
of
the
plot
.

2001
:
A
Space
Odyssei
is
probabl
a
better
film
,
but
it
realli
onli
qualifi
as
scienc
fiction
if
you
consid
metaphys
to
be
a
scienc
.

Most
all
other
film
we
normal
classifi
as
scienc
fiction
,
or
SF
,
ar
realli
just
fantasi
,
action
or
horror
stori
set
in
a
futurist
set
.

The
Andromeda
Strain
start
out
with
two
soldier
in
a
high-tech
-LRB-
for
1970
-RRB-
van
look
for
a
crash
satellit
in
a
tini
New
Mexico
town
.

Someth
bad
happen
to
them
.

Pictur
from
a
reconnaiss
plane
show
a
shock
sight
.

Apparent
everyon
in
the
town
is
dead
.

The
author
call
a
``
Wildfir
Alert
,
''
summon
four
scientist
,
all
but
on
of
them
somewhat
reluct
,
to
a
super-secret
underground
germ
warfar
laboratori
in
Nevada
.

Two
of
scientist
,
Stone
and
Hall
,
fly
directli
to
the
town
in
space
suit
and
find
a
town
where
almost
everi
singl
resid
liter
drop
in
their
track
,
their
blood
turn
to
powder
in
their
vein
.

Some
went
insan
befor
thei
di
and
two
of
them
,
a
babi
and
an
old
wino
,
ar
miracul
still
aliv
.

Thei
take
the
survivor
and
the
satellit
back
to
the
lab
,
where
Stone
pressur
the
White
Hous
to
call
up
a
``
Direct
712
,
''
an
execut
order
to
cauter
the
area
around
the
town
with
a
nuclear
bomb
.

What
thei
find
on
the
satellit
when
thei
get
back
to
the
lab
is
Andromeda
,
an
organ
that
defi
all
the
normal
rule
of
Earth-lik
life
and
mutat
as
it
grow
.

Their
onli
hope
to
cure
it
is
to
find
out
what
a
perfectli
healthi
babi
boi
ha
in
common
with
an
old
derelict
.

What
thei
find
out
is
that
organ
feed
directli
on
energi
and
that
deton
the
A-bomb
over
the
town
would
onli
spread
it
across
the
entir
planet
.

Thei
bare
call
off
the
bomb
in
time
.

But
then
the
organ
mutat
into
someth
that
threaten
to
eat
through
the
lab
defens
and
break
out
.

Thi
trigger
the
lab
's
last-ditch
defens
mechan
,
an
atom
bomb
.

To
those
rais
on
the
brainless
action
fare
that
pollut
movi
theater
these
dai
,
The
Andromeda
Strain
will
probabl
seem
intermin
slow
.

Much
of
the
film
is
a
lot
of
peopl
stand
around
look
at
video
screen
and
comput
readout
.

But
what
the
charact
see
on
those
screen
ratchet
up
the
tension
with
everi
turn
of
the
screw
.

The
perform
ar
univers
fine
,
with
the
actor
keep
thing
low-kei
and
restrain
,
just
like
scientist
.

To
me
,
the
real
appeal
of
thi
film
is
the
fact
that
it
show
scientist
act
like
scientist
and
make
it
seem
excit
.

We
follow
the
logic
of
their
deduct
step
by
method
step
,
puzzl
like
thei
do
everi
time
Andromeda
behav
in
a
wai
we
do
n't
expect
.

Thi
film
is
onli
avail
on
wide
screen
on
DVD
and
that
is
the
wai
to
see
it
.

It
wa
origin
rate
G
when
first
releas
but
it
now
carri
the
PG
rate
,
mostli
for
veri
mild
nuditi
and
on
scene
in
which
a
bodi
's
wrist
is
slash
,
spill
it
powder
blood
.

