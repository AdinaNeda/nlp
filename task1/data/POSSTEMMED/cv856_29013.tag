My
filmcritic.com
colleagu
Norm
Schrager
nail
Session
9
,
Brad
Anderson
's
throwback
to
spooki
horror
film
from
the
70
's
.

It
work
as
an
eeri
homag
without
be
self-referenti
or
smugli
postmodern
.

Genr
aficionado
will
acknowledg
the
similar
in
tone
to
Stanlei
Kubrick
's
The
Shine
and
Georg
Romero
's
Dawn
of
the
Dead
without
be
taken
out
of
the
engross
narr
-LRB-
i.e.
,
a
psycholog
addl
wast
manag
team
clear
out
an
abandon
lunat
asylum
;
unspeak
dread
ensu
-RRB-
.

In
a
double-whammi
for
2001
,
Anderson
shoot
and
-LRB-
mostli
-RRB-
score
again
with
hi
eclect
riff
on
time-travel
episod
from
The
Twilight
Zone
,
appropri
titl
Happi
Accident
.

Much
like
Session
9
,
the
card
ar
plai
veri
close
to
the
vest
here
.

Is
boyish
,
eccentr
``
Sam
Deed
from
Dubuqu
,
Iowa
''
a
futurist
voyag
from
the
year
2470
or
just
your
run-of-the-mil
psycholog
disturb
nutcas
let
loos
on
the
present-dai
street
of
NYC
?

As
plai
by
wonder
charact
actor
Vincent
D'Onofrio
-LRB-
Full
Metal
Jacket
-RRB-
,
it
's
up
in
the
air
whether
or
not
we
should
accept
hi
detail
monologu
about
life
after
the
polar
ic
cap
have
melt
.

The
question
prove
to
be
moot
,
at
least
for
a
time
.

Even
if
the
whole
thing
prove
to
be
a
creativ
delus
,
on
agre
with
the
charact
judgment
pass
down
on
him
by
hi
new
girlfriend
,
Rubi
-LRB-
Marisa
Tomei
-RRB-
:
``
He
's
a
freak
,
but
he
sure
tell
a
good
stori
!
''

Neurot
Rubi
think
she
mai
have
found
True
Love
after
a
seri
of
nightmarish
date
disast
-LRB-
the
Junki
,
the
Fetishist
,
the
Artist
,
the
Frenchman
,
etc.
-RRB-
,
but
is
n't
quit
sure
how
to
handl
``
Sam
Deed
''
when
he
start
explain
the
barcod
on
hi
arm
,
hi
elabor
construct
fake
ident
,
hi
patholog
fear
of
dog
,
hi
abil
to
speak
five
differ
languag
,
and
hi
mission
to
chang
a
crucial
moment
in
time
that
mai
have
ramif
on
time
's
altern
realiti
.

-LRB-
Do
n't
ask
.
-RRB-

It
's
all
a
bit
much
to
take
in
.

Rubi
's
close
friend
Gretchen
-LRB-
cuz
ya
ca
n't
have
a
love
stori
without
the
token
friend
,
though
Nadia
Dajani
invest
the
thankless
role
with
warmth
-RRB-
chalk
it
up
as
a
sexi
role-plai
game
,
but
her
cautiou
therapist
-LRB-
Holland
Taylor
-RRB-
warn
her
that
co-depend
is
rear
it
ugli
head
again
and
she
's
in
over
her
head
with
yet
anoth
doom
relationship
.

Who
ya
gon
na
believ
?

Despit
her
win
an
Academi
Award
,
Marisa
Tomei
ha
alwai
struck
me
as
an
annoi
and
unwelcom
screen
presenc
,
on
that
undermin
the
pleasur
of
watch
Happi
Accident
.

Her
brassi
New
Yawk
attitud
never
realli
mesh
with
her
desper
desir
to
appear
``
cute
''
to
her
ador
fan
.

Be
loud
and
flash
a
-LRB-
disingenu
-RRB-
smile
doe
not
necessarili
equal
``
substanti
and
sexi
.
''

It
take
more
than
a
crack
team
of
hair
and
wardrob
peopl
to
imbu
her
with
person
.

Then
there
's
that
damn
voic
,
which
strain
to
be
oh-so-ador
.

Look
,
thi
stuff
is
pure
subject
.

Some
peopl
feel
thi
wai
about
Richard
Gere
,
other
can
not
bear
to
watch
Robin
William
'
hyperact
schtick
.

For
my
monei
,
it
's
M.
Tomei
with
a
bullet
.

Happi
Accident
is
a
romant
comedi
filter
through
Twelv
Monkei
-LRB-
or
,
more
appropri
,
Chri
Marker
's
La
Jete
,
especi
with
those
still
frame
``
memori
''
photograph
Anderson
emploi
as
a
stylist
devic
throughout
-RRB-
.

Modern
Manhattan
is
film
with
an
otherworldli
,
vagu
alien
ey
with
a
color
scheme
oddli
reminisc
of
Logan
's
Run
.

As
the
stranger
in
a
strang
land
,
D'Onofrio
walk
slightli
out-of-step
,
wonderfulli
affabl
but
often
inscrut
with
hi
wayward
express
and
bemus
detach
.

Thi
is
scienc
fiction
told
mainli
via
the
power
of
suggest
-LRB-
though
it
often
fall
into
the
sci-fi
trap
of
have
entir
too
much
forc
exposit
--
we
want
deed
,
not
word
!
-RRB-

At
least
twenti
minut
too
long
,
Happi
Accident
eventu
get
around
to
a
race-against-tim
scenario
that
put
``
Sam
Deed
''
to
the
Ultimat
Test
.

No
movi
can
live
in
ambigu
forev
,
but
Anderson
seem
termin
unabl
to
provid
satisfactori
conclus
to
hi
otherwis
well
structur
recent
narr
.

-LRB-
Let
's
pretend
the
loathsom
and
predict
Next
Stop
Wonderland
never
happen
,
shall
we
?
-RRB-

There
ar
also
some
slow
,
repetit
stretch
as
Rubi
and
Sam
go
over
the
same
argument
again
and
again
over
whether
or
not
he
's
crazi
.

The
premis
is
strong
enough
to
sustain
interest
,
but
it
's
enough
to
throw
a
nod
in
the
gener
direct
of
Rod
Serl
for
wrap
up
hi
idea
in
half-hour
time
slot
,
commerci
includ
.

