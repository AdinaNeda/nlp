A
young
man
who
love
heavi
metal
music
and
especi
the
band
Steel
Dragon
,
to
whom
he
's
devot
a
tribut
band
in
which
he
sing
,
get
launch
into
stardom
when
the
real
group
get
rid
of
their
lead
singer
and
call
on
him
to
take
hi
place
.

CRITIQUE
:
I
'm
a
sucker
for
movi
like
thi
.

A
young
man
with
a
humbl
background
and
lofti
dream
,
work
hard
,
devot
the
time
,
the
energi
and
the
patienc
,
and
ultim
hit
it
big-tim
.

In
the
case
of
thi
film
,
our
boi
love
a
certain
heavi
metal
band
and
as
luck
would
have
it
-LRB-
yup
,
luck
alwai
find
it
wai
into
these
type
of
equat
,
although
gener
ti
veri
close
to
hard
work
-RRB-
,
thei
need
a
new
singer
.

Hi
entri
and
adapt
to
the
whole
``
rock
'
n
roll
''
lifestyl
fill
the
rest
of
the
film
and
is
realli
fun
to
watch
.

Although
I
will
prefac
thi
by
sai
that
on
thing
that
would
definit
enhanc
your
appreci
for
thi
film
is
your
own
love
-LRB-
or
past
love
-RRB-
of
heavi
metal
music
and
the
whole
scene
around
it
.

Metal
wa
on
of
my
first
love
as
a
teen
and
even
though
the
genr
of
music
is
n't
that
promin
anymor
,
I
still
check
out
my
Motlei
Crue
,
Twist
Sister
and
Anthrax
CD
everi
now
and
again
.

That
's
not
to
sai
that
you
wo
n't
like
thi
film
if
you
do
n't
like
the
music
,
but
the
music
and
live
perform
from
the
band
,
plai
a
big
part
in
the
movi
,
and
I
for
on
,
had
a
blast
watch
and
listen
to
it
all
.

But
the
even
greater
draw
in
thi
film
is
the
standout
perform
given
here
by
Mark
Wahlberg
.

Wow
,
hand
thi
fella
some
major
prop
,
as
he
total
becom
thi
heavi
metal
geek/god
-LRB-
incident
,
METAL
GOD
wa
the
film
's
origin
titl
,
and
a
much
better
on
if
you
ask
me
-RRB-
.

He
is
thi
movi
and
I
wa
quit
taken
by
hi
charact
pretti
much
the
whole
wai
through
.

He
came
off
like
a
regular
gui
with
extrem
passion
goal
and
work
ethic
,
who
wa
will
to
do
anyth
in
order
to
fulfil
hi
dream
.

Aniston
wa
also
surprisingli
good
as
the
girlfriend
-LRB-
and
the
romanc
angl
between
them
wa
sweet
-RRB-
,
but
she
did
n't
pull
me
in
hard
enough
dure
their
emot
scene
.

I
wa
also
impress
by
some
of
the
``
real
''
musician
who
plai
in
the
film
-LRB-
Zakk
Wyld
from
Ozzy
Osbourn
,
Jeff
Pilson
from
Dokken
,
Stephan
Jenkin
from
Third
Eye
Blind
,
Bla
Elia
from
Slaughter
-RRB-
,
but
actor
Domin
West
as
Kirk
Cuddi
made
the
biggest
impress
among
the
band
member
.

It
's
to
note
that
thi
film
wa
base
on
a
real-lif
tale
of
a
young
man
who
us
to
sing
in
a
Juda
Priest
cover
band
and
then
went
on
to
becom
their
actual
singer
-LRB-
their
origin
singer
also
admit
to
be
gai
,
as
in
thi
film
-RRB-
.

Just
for
the
record
,
I
'm
certainli
not
recommend
thi
film
for
it
origin
or
surpris
element
,
sinc
most
of
thi
stuff
ha
alreadi
been
cover
in
some
wai
or
anoth
in
other
movi
,
but
becaus
it
's
a
fun
,
uplift
,
well-pac
movi
with
a
solid
central
show
by
Wahlberg
and
energet
live
perform
.

Oh
yeah
,
and
for
those
who
dig
the
``
heavier
''
side
of
music
,
the
soundtrack
also
rock
!!

You
see
...
dream
can
come
true
...

