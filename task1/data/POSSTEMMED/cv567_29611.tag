A
peculiar
French
girl
grow
up
lone
with
her
father
and
doe
n't
quit
know
what
she
want
out
of
life
.

One
dai
,
she
fall
upon
someth
,
and
believ
that
her
ticket
to
happi
mai
be
in
help
other
.

She
start
with
the
peopl
around
her
,
but
when
she
suddenli
fall
for
her
own
gui
,
she
ca
n't
act
strongli
enough
becaus
of
her
shy
and
dreami
natur
.

A
clever
,
quirki
,
origin
French
flick
set
in
a
picturesqu
Pari
,
featur
an
endear
lead
with
a
giant
imagin
,
much
loneli
and
a
littl
bit
of
love
for
everyon
.

Thi
is
a
``
feel-good
''
kind
of
movi
,
a
fairi
tale
for
grown-up
who
ar
bum
out
about
life
.

It
doe
n't
pretend
to
be
deep
,
it
doe
n't
weigh
itself
down
with
long
exposit
or
intric
studi
of
it
charact
,
it
's
basic
just
a
``
cute
movi
''
in
the
same
vein
as
CHOCOLAT
and
PAY
IT
FORWARD
,
with
some
amaz
visual
and
incred
nice
intent
-LRB-
and
if
CHOCOLAT
wa
somehow
abl
to
nab
an
Oscar
nomin
for
Best
Pictur
last
year
-LRB-
a
sham
on
so
mani
differ
level
-RRB-
,
you
might
as
well
slap
thi
film
up
for
consider
also
,
sinc
it
's
quit
a
bit
better
and
much
more
origin
than
the
former
-RRB-
.

One
thing
that
thi
film
is
n't
though
,
at
least
accord
to
me
,
is
the
best
film
of
the
year
-LRB-
it
wa
bestow
the
honor
of
``
best
''
in
at
least
differ
film
festiv
so
far
thi
year
-RRB-
.

It
actual
manag
to
stagnat
a
littl
bit
about
halfwai
through
and
it
just
went
on
for
wai
too
long
-LRB-
it
's
a
littl
over
two
hour
but
it
felt
even
longer
-RRB-
,
with
much
of
the
second
half
of
the
movi
devot
to
mani
differ
charact
,
all
of
whom
were
n't
as
interest
as
the
lead
,
and
a
lot
of
indecis
from
Ms.
Ameli
herself
,
which
got
frustrat
.

Thankfulli
,
thi
film
doe
n't
bog
itself
down
with
too
much
of
that
stuff
,
and
actual
goe
out
of
it
wai
to
invent
new
wai
to
shoot
scene
,
to
integr
moment
of
fantasi
into
it
fabric
and
to
transport
the
audienc
insid
the
imagin
of
thi
kind-heart
French
girl
.

On
the
whole
,
it
's
a
lot
of
fun
and
it
carri
a
beauti
messag
of
love
and
support
for
your
fellow
man
.

The
lead
,
Audrei
Tautou
,
is
perfect
for
the
role
,
and
give
you
enough
moment
of
truth
to
develop
her
charact
into
someon
that
you
care
about
by
the
end
of
the
film
.

Unfortun
,
the
rest
of
the
cast
is
n't
as
well-develop
,
and
even
though
most
of
them
ar
pretti
appeal
,
a
coupl
could
've
been
left
out
to
save
time
and
at
least
on
wa
left
strand
plot-wis
,
by
the
film
's
end
-LRB-
what
happen
to
the
gui
in
the
café
who
wa
spy
on
hi
ex-girlfriend
the
whole
movi
?
-RRB-
.

Now
I
have
n't
seen
too
mani
recent
French
film
,
I
will
admit
,
but
if
thi
is
the
kind
of
``
scene
''
that
thei
're
develop
over
in
``
le
pai
du
vin
''
,
I
would
n't
mind
catch
more
of
their
flick
in
the
futur
-LRB-
CRIMSON
RIVERS
wa
anoth
French
film
that
I
saw
earlier
thi
year
,
which
I
realli
liked-iron
,
that
film
wa
direct
by
Mathieu
Kassovitz
,
who
is
the
co-star
in
thi
film
-RRB-
.

Grant
,
the
English
subtitl
got
a
littl
tough
to
keep
up
with
at
time
-LRB-
I
speak
and
understand
French
also
,
but
some
of
the
local
colloqui
went
over
my
head
-RRB-
,
especi
sinc
the
film
's
visual
were
so
intrigu
that
you
just
could
n't
help
but
constantli
gawk
at
them
,
but
the
gener
idea
,
the
background
music
,
the
nifti
French
neighborhood
,
the
veri
creativ
wai
of
envelop
the
audienc
into
the
story-lin
from
the
start
,
were
all
veri
easi
to
appreci
,
and
I
for
on
wa
especi
glad
to
have
seen
thi
movi
dure
these
try
time
in
the
world
.

In
fact
,
I
'd
recommend
it
to
anyon
look
for
a
jolli
ol'
time
at
the
movi
hous
,
with
a
particular
emphasi
on
folk
with
a
lot
of
imagin
,
a
littl
loneli
in
their
live
and
mani
dream
in
their
heart
.

And
thi
gui
direct
ALIEN
:
RESURRECTION
?

Get
outta
here
!!

Note
:
I
especi
love
the
wai
that
the
script
detail
and
show
the
loves/h
of
everyon
with
exact
mention
in
the
film
.

It
wa
veri
origin
,
veri
well
shot
and
veri
nostalg
,
as
mani
of
us
were
abl
to
relat
to
the
behavior
describ
.

Good
stuff
!

