Take
a
few
tip
from
the
Pulp
Fiction
school
of
filmmak
,
Go
is
the
new
featur
from
the
director
of
the
cult
hit
Swinger
.

The
stori
centr
around
a
group
of
peopl
,
who
includ
Brit
Simon
-LRB-
Askew
-RRB-
,
and
checkout
girl
Clair
-LRB-
Holm
-RRB-
and
Ronna
-LRB-
Pollei
-RRB-
and
the
misadventur
that
thei
get
into
,
split
into
three
chapter
.

Everyth
from
sex
to
drug
to
violenc
is
cover
in
a
film
with
a
much
blacker
edg
in
comedi
than
Swinger
.

Although
QT
's
touch
is
evid
in
nearli
everi
frame
,
Go
is
entertain
enough
to
forget
about
the
similar
and
just
enjoi
the
rollercoast
ride
.

Liman
,
who
also
photograph
the
film
,
ha
a
deft
touch
with
the
camera
,
and
the
film
look
good
.

The
film
also
seem
to
have
benefit
from
a
budget
,
with
the
night-club
scene
in
particular
look
pretti
good
.

There
's
a
twist
sens
of
humour
run
throughout
which
ensur
that
even
the
darkest
scene
get
a
smirk
.

The
young
cast
ar
talent
and
help
bring
the
stori
to
life
.

Holm
,
on
of
the
Dawson
's
Creek
cast
-LRB-
a
show
I
'm
not
particularli
fond
of
-RRB-
displai
much
talent
here
,
and
despit
her
larg
limit
screen
time
manag
to
make
a
three
dimension
charact
and
never
becom
a
caricatur
.

Askew
is
slightli
success
,
be
rather
obnoxi
.

No
doubt
the
filmmak
intend
thi
to
make
the
audienc
cheer
when
what
happen
to
him
,
but
instead
of
`
funni
annoi
'
he
's
just
annoi
.

He
drag
down
slightli
on
of
the
funniest
chapter
,
but
the
talent
around
him
in
that
particular
tale
more
than
make
up
for
it
.

The
real
standout
is
Sarah
Pollei
as
the
drug
deal
checkout
girl
:
her
refresh
and
deepli
enjoy
perform
make
the
heavi
go
stori
she
is
involv
in
much
more
entertain
.

Also
entertain
Timothi
Olyphant
as
the
rather
sinist
drug
dealer
,
who
give
an
nice
evil
perform
.

The
script
,
written
John
August
,
is
sharp
and
witti
,
with
good
dialogu
and
some
funni
joke
.

It
start
off
rather
slow
howev
,
but
in
about
twenti
minut
the
script
ha
found
it
's
foot
and
just
keep
get
better
.

The
stori
ar
larg
satisfi
,
although
occasion
there
's
too
much
attent
on
on
detail
but
not
on
anoth
.

Also
,
some
of
the
end
seem
a
littl
forc
and
lucki
.

Still
,
the
cast
have
a
meati
script
to
get
into
and
obvious
enjoi
it
.

It
could
of
tri
a
littl
harder
in
some
part
howev
,
and
these
part
lag
.

Becaus
thi
is
a
Gen-Xer
movi
,
the
obligatori
rave
soundtrack
must
accompani
,
and
Go
's
on
is
pretti
decent
.

Unlike
other
Gen-Xer
movi
,
Go
never
allow
the
music
to
substitut
for
plot
or
dialogu
,
which
is
a
plu
.

Go
is
a
veri
easi
movi
to
absorb
into
,
and
the
audienc
realli
start
feel
for
these
charact
.

Thankfulli
,
thei
never
becom
two
dimension
charact
who
ar
bent
and
twist
throughout
the
movi
to
fit
into
the
stori
mechan
-LRB-
like
the
heroin
inTh
I
Hate
About
You
.
-RRB-

Thei
ar
who
thei
ar
and
thei
stai
that
wai
.

Go
is
great
fun
,
and
a
worthi
follow
up
from
the
director
of
Swinger
.

Ignore
the
fact
that
it
steal
from
Pulp
Fiction
and
Veri
Bad
Thing
,
and
just
sit
back
and
enjoi
the
rollercoast
ride
.

Erm
,
go
to
Go
,
I
guess
.

