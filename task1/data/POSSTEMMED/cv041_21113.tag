``
Both
my
wife
and
daughter
think
I
'm
thi
gigant
loser
.
''

And
thei
're
right
.

I
have
lost
someth
.

I
did
n't
alwai
feel
thi
sedat
.
''
''

With
that
piec
of
dialogu
,
we
ar
fulli
introduc
to
Lester
Burnham
-LRB-
Kevin
Spacei
-RRB-
,
a
year
old
man
trap
within
hi
own
life
.

Lester
work
for
an
advertis
agenc
.

Hi
grow
dissatisfact
with
hi
job
is
onli
on
of
the
mid-lif
crisi
he
is
suffer
.

Estrang
from
both
hi
wife
and
hi
daughter
,
Lester
start
to
crack
under
the
stress
.

Hi
life
becom
noth
more
than
a
seri
of
mastubatori
fantasi
,
and
sullen
apathi
.

Carolyn
Burnham
-LRB-
Annett
Bene
-RRB-
,
is
the
prototyp
career
woman
/
homemak
.

She
carefulli
tend
her
rose
garden
,
decor
her
home
with
the
best
furnish
.

But
insid
she
is
desper
.

Unsuccess
at
her
profess
as
a
real
estat
agent
,
she
drive
herself
into
state
of
denial
and
self
loath
that
onli
feed
her
grow
resent
toward
her
famili
.

The
third
member
of
thi
anti-famili
is
Jane
Burnham
-LRB-
Thora
Birch
-RRB-
,
Lester
and
Carolyn
's
daughter
.

A
young
girl
go
through
the
typic
stage
of
adolesc
,
but
have
to
also
come
to
term
with
her
famili
grow
dysfunct
.

Thi
funni
-LRB-
and
frighten
-RRB-
slice
of
life
is
refreshingli
simpl
.

Not
on
charact
is
given
less
than
is
need
to
identifi
him
or
her
.

Some
of
the
most
tell
charact
ar
those
that
serv
to
highlight
other
charact
.

Most
notabl
is
Barbara
Fitt
-LRB-
Allison
Jannei
-RRB-
,
the
almost
cataton
wife
of
Retir
Marin
Colonel
Fitt
.

She
ha
almost
no
dialogu
,
and
seem
to
be
either
recov
from
a
stroke
or
suffer
from
on
.

Colonel
Fitt
-LRB-
Chri
Cooper
-RRB-
is
an
angri
and
frighten
man
.

Hi
rage
direct
almost
singularli
at
hi
son
,
Ricki
-LRB-
We
Bentlei
-RRB-
who
ha
just
move
with
hi
famili
to
the
same
neighborhood
as
the
Burnham
's
.

In
fact
thei
ar
the
next
door
neighbor
.

Anymor
detail
into
the
person
of
these
marvel
divers
charact
would
depriv
on
of
the
mani
pleasur
and
surpris
to
be
had
in
``
American
Beauti
''
.

Alan
Ball
,
who
wa
the
co-execut
produc
of
the
hit
TV
seri
``
Cybil
''
and
creator
of
the
new
sitcom
``
Oh
Grow
Up
''
,
ha
fashion
on
of
the
most
insight
and
expertli
tune
screenplai
to
have
hit
the
screen
in
the
past
ten
year
.

Not
on
charact
in
thi
film
ring
fals
.

And
the
depth
of
person
in
each
,
is
achingli
honest
and
insight
.

The
plot
detail
the
last
year
in
the
life
of
a
man
as
he
tri
to
find
hi
own
self
worth
is
so
resoundingli
clich
and
at
the
same
time
origin
as
to
bring
to
mind
such
marvel
film
work
as
``
The
Graduat
''
or
``
Lolita
''
.

Kevin
Spacei
turn
in
on
of
thi
year
most
pragmat
perform
.

Hi
Lester
Burnham
is
an
everyman
caught
up
in
the
middl
of
a
life
that
is
spiral
into
old
ag
.

Final
unabl
to
deal
with
the
lost
soul
that
he
ha
becom
,
Lester
tri
to
return
to
hi
youth
.

He
quit
hi
job
-LRB-
actual
blackmail
hi
employ
for
on
year
's
salari
-RRB-
,
start
smoke
pot
,
fantas
about
hi
daughter
's
girlfriend
Angela
-LRB-
Mena
Suvari
,
in
a
marvel
perform
here
-RRB-
,
start
work
out
,
and
final
take
a
job
at
the
local
fast
food
outlet
.

Mr.
Spacei
's
perform
is
so
remark
subtl
and
ingenu
,
that
we
can
be
certain
that
come
Oscar
®
time
,
we
can
be
assur
to
see
him
name
among
the
five
nomine
.

Annett
Bene
also
turn
in
what
mai
be
the
best
perform
of
her
career
.

Carolyn
Burnham
is
a
woman
who
ha
becom
so
insecur
as
to
be
caught
up
in
maintain
the
facad
of
normalci
.

Unabl
to
cope
with
her
grow
disillusion
,
she
forc
herself
to
maintain
her
demeanor
,
drive
her
further
and
further
into
desper
,
adulteri
and
possibl
murder
.

Ms.
Bene
make
the
charact
of
Carolyn
so
compel
as
to
be
identifi
to
almost
anyon
.

The
slow
and
method
turn
from
happi
home
maker
to
charnel
hous
Martha
Stewart
is
too
frighten
to
express
in
word
.

Thora
Birch
hold
her
own
against
the
talent
cast
with
her
.

Jane
Burnham
is
both
an
innoc
girl
caught
up
in
what
is
a
dysfunct
famili
,
and
a
rebelli
child
readi
to
abandon
them
at
ani
moment
.

Her
grow
distrust
and
isol
from
her
famili
is
someth
that
mani
parent
should
view
as
a
warn
.

She
is
the
prototyp
trophi
child
.

When
Carolyn
prais
her
daughter
after
a
perform
by
her
cheerlead
team
-LRB-
``
I
'm
so
proud
of
you
,
Honei
.
''

You
did
n't
screw
up
onc
!
''
-RRB-

,
the
bitter
and
resent
ar
palpabl
.
''

Thora
Birch
is
a
young
actress
to
watch
.

Prais
must
also
go
to
Chri
Cooper
who
bring
yet
anoth
angri
father
role
to
life
with
depth
and
reson
.

Colonel
Finn
is
a
man
in
complet
denial
.

Hi
militari
upbring
mask
a
desir
he
is
too
repuls
to
even
comprehend
.

We
Bentlei
as
Ricki
Finn
is
fine
as
the
enigma
.

The
boi
next
door
who
mai
be
more
foe
than
friend
.

A
deceit
and
calcul
young
man
,
whose
voyeurist
procliv
mask
a
deeper
and
more
profound
understand
of
life
than
ani
of
the
adult
around
him
.

Mr.
Bentlei
's
perform
is
the
on
troublesom
note
in
the
film
.

That
is
simpli
becaus
the
charact
is
such
a
grand
and
complet
liar
,
that
on
almost
fault
the
perform
for
it
.

Director
Sam
Mend
handl
all
of
the
particular
of
the
cast
and
the
set
with
a
sure
and
compos
style
that
is
neither
intrus
nor
subtl
.

Scene
of
Carolyn
walk
through
the
rain
,
carri
an
omin
fission
that
call
attent
to
the
emot
of
the
charact
.

The
subtl
cinematographi
by
Conrad
Hall
is
wonder
,
captur
the
season
in
all
of
it
's
harsh
beauti
-LRB-
Fall
ha
never
look
so
invit
,
yet
depress
-RRB-
.

Everi
nuanc
of
``
American
Beauti
''
beg
it
's
audienc
to
think
and
examin
.

It
is
a
film
that
will
be
discuss
and
debat
for
year
to
come
.

And
well
it
should
,
as
it
is
simpli
on
of
thi
year
best
.

