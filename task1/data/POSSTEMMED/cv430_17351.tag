An
attract
mute
makeup
artist
,
work
on
an
ultra-cheesi
slasher
movi
in
Moscow
,
wit
the
product
of
a
brutal
snuff
film
and
is
subsequ
chase
by
realli
bad
Russian
.

Meanwhil
,
the
artist
's
sister
and
boyfriend
clumsili
try
to
save
her
.

Comment
:
MUTE
WITNESS
came
as
a
surpris
to
me
the
first
time
I
watch
it
.

Drawn
by
the
clever
artwork
on
the
video
box
,
I
rent
the
film
expect
a
complet
turkei
.

MUTE
WITNESS
,
howev
,
wa
origin
,
offbeat
,
and
well-mad
.

It
's
on
of
those
cool
littl
find
that
no
on
seem
to
know
about
.

I
've
subsequ
found
it
at
most
video
rental
place
I
visit
,
and
it
mai
be
seen
,
on
occas
,
on
the
Independ
Film
Channel
.

The
first
hour
of
MUTE
WITNESS
is
extrem
tens
,
as
Billi
,
the
quit
believ
mute
heroin
,
see
member
of
the
Russian
mob
brutal
kill
a
prostitut
while
film
an
illeg
snuff
film
.

The
rest
of
the
film
take
good
advantag
of
Billi
's
vulner
posit
as
a
mute
foreign
in
Moscow
be
pursu
by
power
crimin
figur
.

To
throw
a
curvebal
into
the
frai
,
Billi
's
sister
Karen
and
her
filmmak
beau
becom
bizarr
figur
of
comic
relief
to
offset
sever
violent
sequenc
in
what
ar
some
genuin
funni
scene
.

A
disappointingli
trite
end
and
occasion
comic
blunder
ar
the
onli
two
thing
which
mar
thi
otherwis
suspens
film
.

Look
for
Alec
Guin
-LRB-
Obi-Wan
Kenobi
from
the
first
STAR
WARS
trilogi
-RRB-
in
a
small
role
as
the
evil
Reaper
.

Defin
check
thi
movi
out
;
although
,
as
a
word
of
warn
,
it
doe
contain
sever
scene
of
rather
grisli
violenc
which
certainli
ar
n't
for
the
squeamish
.

