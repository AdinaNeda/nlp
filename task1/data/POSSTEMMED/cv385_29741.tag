CAPSULE
:
Thi
is
a
film
that
avoid
easi
answer
.

Wilhelm
Furtwangl
,
then
the
world
's
greatest
orchestra
conductor
stai
in
Germani
and
cooper
with
the
Nazi
.

What
were
hi
view
,
wa
he
a
war
crimin
or
a
secret
resist
fighter
?

How
much
did
he
know
about
crime
against
human
?

The
US
govern
investig
him
after
the
war
and
thi
film
is
a
dramat
of
that
investig
.

In
my
opinion
on
of
the
best
film
of
the
last
few
year
is
Istvan
Szabo
's
SUNSHINE
,
a
film
that
cover
the
fortun
of
a
Jewish
Hungarian
famili
under
the
reign
of
three
differ
regim
,
Hungarian
aristocrat
,
Nazi
,
and
Communist
.

Szabo
's
follow-up
film
,
a
German
product
,
is
much
more
limit
in
scope
.

It
is
about
the
post-war
investig
of
who
crimin
support
the
Nazi
and
who
oppos
them
.

Ronald
Harwood
's
screenplai
ambigu
look
at
the
investig
of
a
great
classic
music
conductor
who
stai
on
in
Germani
when
the
Nazi
took
power
and
becam
the
most
popular
conductor
of
the
Third
Reich
.

Maj.
Steve
Arnold
-LRB-
plai
by
Harvei
Keitel
-RRB-
ha
been
assign
by
hi
superior
to
investig
Wilhelm
Furtwangl
-LRB-
Stellan
Skarsgard
-RRB-
,
perhap
Europ
's
greatest
classic
music
conductor
.

When
other
artist
fled
Germani
,
Furtwangl
remain
behind
and
conduct
for
the
Hitler
and
hi
henchmen
.

After
the
war
is
over
Arnold
assign
to
interview
Furtwangl
and
member
of
hi
orchestra
and
if
appropri
to
prosecut
him
for
war
crime
.

He
secretli
is
told
by
hi
command
offic
to
find
Furtwangl
guilti
.

From
there
we
follow
him
and
learn
a
littl
about
Arnold
and
someth
about
Furtwangl
and
hi
orchestra
.

As
he
interview
member
of
the
wartim
orchestra
Arnold
start
notic
odd
peculiar
that
mai
or
mai
not
point
to
a
conspiraci
against
hi
investig
.

There
is
a
certain
same
to
the
respons
that
he
is
get
.

Perhap
ani
cooper
he
is
get
ha
been
in
some
wai
manag
.

If
so
,
perhap
he
can
never
come
to
the
truth
.

In
larg
part
the
film
is
about
mind
game
that
Arnold
us
to
manipul
hi
interviewe
and
especi
Furtwangl
.

Where
the
script
ha
problem
is
that
in
the
end
it
is
so
ambigu
.

It
ha
no
obviou
resolut
and
not
much
of
a
final
act
.

When
it
is
over
whether
anyth
ha
been
establish
is
open
to
interpret
.

Perhap
that
is
better
than
so
mani
film
that
make
it
all
to
obviou
what
the
audienc
should
believ
,
but
it
is
like
watch
a
murder
mysteri
and
never
find
out
who
the
killer
is
.

We
ar
given
clue
to
someth
but
thei
ar
never
ti
up
.

In
the
end
we
just
know
more
about
both
Arnold
and
Furtwangl
.

The
film
is
basic
a
stage
plai
.

The
visual
is
not
veri
import
.

Corner
ar
cut
visual
includ
touch
like
fill
window
with
photograph
to
avoid
have
to
shoot
on
locat
.

As
with
a
stage
plai
,
what
thi
film
center
on
the
dialog
,
and
that
is
intrigu
.

-LSB-
There
is
on
piec
of
sloppi
few
peopl
but
me
would
notic
.

At
on
point
we
clearli
see
Arnold
's
desk
calendar
sai
``
Jan
16
Tues.
''
A
quick
mental
calcul
told
me
that
combin
could
occur
in
1945
and
then
not
again
until
1951
.

The
event
had
to
take
place
in
1946
or
1947
.

A
possibl
date
could
be
obtain
from
ani
World
Almanac
.
-RSB-

