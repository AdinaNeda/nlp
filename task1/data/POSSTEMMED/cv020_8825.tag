By
Phil
Curtolo
Mel
Gibson
-LRB-
Braveheart
-RRB-
gave
a
grip
perform
as
the
father
of
a
young
kidnap
boi
in
Ron
Howard
's
Ransom
.

Gibson
plai
Tom
Mullen
,
a
wealthi
busi
tycoon
whose
past
action
ar
come
back
to
haunt
him
as
a
derang
psychopath
,
plai
by
Gari
Sinis
-LRB-
Forrest
Gump
-RRB-
,
and
hi
band
of
low-lif
thug
kidnap
hi
onli
son
for
$
2
million
.

Tom
and
hi
wife
,
Kate
,
plai
by
Rene
Russo
-LRB-
Tin
Cup
-RRB-
were
instruct
not
to
inform
the
polic
,
but
thei
contact
the
FBI
.

Minut
later
,
an
elit
team
of
agent
led
by
Delroi
Lindo
-LRB-
Broken
Arrow
-RRB-
ar
in
Tom
's
hous
and
wire
everi
phone
.

The
plot
sound
averag
,
just
like
most
other
kidnap
movi
that
you
've
alreadi
seen
,
and
it
wa
noth
more
than
that
.

That
is
until
about
half-wai
through
the
movi
.

Suddenli
,
Tom
goe
to
the
Fox
5
new
room
and
make
a
live
broadcast
sai
,
``
Thi
is
your
ransom
.
''

But
thi
is
as
close
as
you
will
ever
get
to
it
.

Instead
,
I
am
offer
thi
monei
as
a
reward
on
your
head
,
dead
or
aliv
.
''
''

At
thi
point
,
the
plot
thicken
,
and
the
unusu
slow
start
of
the
film
turn
into
a
suspense-fil
action
film
with
great
stunt
.

The
last
half
of
the
film
is
veri
well
done
.

Another
thing
that
carri
thi
film
ar
the
superb
perform
by
Gibson
and
Sinis
,
as
thei
collid
in
a
game
of
wit
over
their
cellular
phone
for
most
of
the
movi
.

Owen
Gleiberman
of
Entertain
Weekli
comment
on
the
subject
:
``
It
make
you
wonder
what
kidnapp
did
befor
cell
phone
.
''

Befor
thi
movi
,
Sinis
plai
mostli
``
good
gui
,
''
first
in
Of
Mice
&
Men
,
then
in
Forrest
Gump
,
and
most
recent
,
in
Apollo
13
.

But
he
wa
surprisingli
devilish
and
cold
in
hi
portray
of
a
cop-gone-bad
.

Gibson
,
of
cours
,
wa
just
be
Gibson
,
in
an
Oscar-worthi
perform
.

Although
most
of
the
scene
were
quit
predict
,
Ransom
is
a
veri
entertain
and
suspens
film
.

