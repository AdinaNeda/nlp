Star
Robert
Carlyl
,
Emili
Watson
,
Joe
Breen
,
Ciaran
Owen
,
Michael
Legg
.

Direct
by
Alan
Parker
.

Rate
R.
Few
film
in
1999
have
divid
the
critic
consensu
as
sharpli
as
Alan
Parker
's
adapt
of
Frank
McCourt
's
memoir
Angela
's
Ashe
.

Mani
dismiss
it
as
an
humorless
,
sentiment
,
uninterest
version
of
the
huge
popular
novel
;
other
hail
it
as
a
heartfelt
,
sincer
portrait
of
the
human
spirit
.

I
'm
somewher
in
between
,
lean
strongli
toward
the
latter
.

It
's
certainli
well-mad
and
it
never
becom
tediou
like
some
literari
adapt
have
a
tendenc
to
be
.

But
I
have
a
feel
that
Parker
fill
the
movi
with
pseudo-lyr
shot
of
rain
fall
on
the
home
street
of
Ireland
just
so
it
can
have
the
two-and-a-half
hour
run
time
that
scream
``
I
'm
import
!
''

McCourt
's
book
of
the
same
name
wa
about
him
and
hi
famili
move
from
the
US
to
Ireland
in
the
earli
1900
's
,
a
time
when
most
peopl
were
desper
try
to
get
into
the
US
.

As
he
comment
,
``
we
were
the
onli
Irishfolk
to
sai
goodby
to
the
Statu
of
Liberti
.
''

In
Ireland
,
Frank
-LRB-
plai
by
Joe
Breen
as
a
small
child
-RRB-
,
hisbroth
,
hi
mother
Angela
-LRB-
Emili
Watson
-RRB-
and
hi
father
Malachi
-LRB-
Robert
Carlyl
-RRB-
get
financi
help
from
Angela
's
mother
,
a
stereotyp
strict
Cathol
who
chide
her
daughter
for
marri
a
Northern
Irishman
.

She
help
them
get
a
rat-infest
apart
and
Malachi
goe
look
for
a
job
.

Posit
ar
scarc
and
when
he
doe
final
find
on
,
he
is
unabl
to
hold
on
to
it
becaus
of
hi
fond
for
drink
.

Meanwhil
,
Frank
goe
to
an
uptight
Cathol
school
,
where
the
instructor
beat
student
with
blunt
wooden
object
as
often
as
their
heart
desir
.

Two
of
Frank
's
brother
soon
die
,
crippl
by
the
dread
live
condit
and
malnutrit
.

As
if
the
poverti
and
the
suffer
were
n't
intens
enough
as
it
stood
,
Malachi
and
Angela
have
anoth
babi
,
worsen
the
situat
further
.

I
imagin
thi
doe
n't
sound
like
much
of
a
plot
.

That
's
probabl
becaus
it
is
n't
on
.

These
ar
peopl
.

Thi
is
their
stori
.

Director
Alan
Parker
-LRB-
The
Commit
-RRB-
know
how
to
tell
a
good
stori
and
Angela
's
Ashe
,
despit
it
relentlessli
literari
tone
remain
interest
.

Though
the
film
occasion
indulg
too
much
in
it
gloomili
picturesqu
Irish
set
,
it
avoid
look
like
a
glorifi
travelogu
.

Supplement
Parker
's
abl
direct
ar
the
engag
,
often
poignant
perform
of
the
lead
,
some
of
them
season
thespian
,
other
rel
beginn
.

Carlyl
,
the
charismat
British
actor
who
ha
shown
astonish
rang
by
hop
from
the
lead
in
a
rowdi
slapstick
comedi
to
the
villain
in
a
Jame
Bond
movi
and
now
to
sentiment
melodrama
,
perfectli
portrai
hi
rather
pathet
charact
.

We
empath
with
the
gui
,
but
we
do
n't
like
or
admir
him
.

Emili
Watson
turn
in
a
heartfelt
,
sincer
support
perform
.

It
is
mostli
she
who
implant
that
lump
in
our
throat
.

Also
worthi
of
mention
is
littl
Joe
Breen
,
whose
face
grace
the
film
's
superb
poster
.

It
is
Breen
's
first
featur
film
and
he
's
extraordinari
as
young
Frank
.

Not
unlik
sudden
Oscar
darl
Halei
Joel
Osment
of
The
Sixth
Sens
,
you
can
peer
into
the
charact
's
soul
through
Breen
's
sad
ey
.

On
the
flip
side
,
on
thing
that
could
have
vastli
improv
Angela
's
Ashe
is
a
more
decis
editor
.

While
Gerri
Hambl
sure
make
the
most
of
the
sceneri
,
he
and
Parker
also
leav
in
at
least
a
half
an
hour
of
unnecessari
footag
.

The
first
half
,
especi
,
could
have
been
trim
down
,
bring
the
film
to
a
more
reason
run
time
and
do
our
bladder
a
favor
in
the
process
.

Thing
pick
up
at
about
the
halfwai
point
,
sort
of
dismiss
a
lot
of
the
critic
I
accumul
dure
the
open
hour
,
but
I
maintain
that
the
aggress
deliber
pace
Parker
and
Co.
set
in
the
first
half
wa
ludicr
unnecessari
.

*
Spoiler
Warn
!

Skip
next
paragraph
if
in
suspens
!

*
The
film
's
final
messag
is
somewhat
conflict
,
as
Franki
,
in
effect
,
leav
hi
famili
famish
in
Ireland
while
he
himself
goe
back
to
America
.

What
,
exactli
,
ar
we
suppos
to
make
of
that
?

Hi
mother
doe
n't
seem
to
mind
becaus
she
want
the
best
possibl
futur
for
her
son
,
as
ani
mother
would
,
but
it
still
seem
inconsider
of
him
.

I
have
n't
read
the
book
,
but
I
'm
sure
that
McCourt
justifi
hi
action
therein
;
therefor
,
I
'm
suspici
.

Another
thing
that
's
never
made
clear
in
the
movi
is
the
mean
of
the
titl
,
which
,
I
'm
sure
,
is
also
explain
in
the
memoir
.

If
seen
as
a
film
onto
itself
,
rather
than
an
adapt
,
Angela
's
Ashe
stand
tall
.

Despit
a
few
minor
setback
,
it
is
a
captiv
stori
of
peopl
forc
to
live
in
the
trench
by
an
merciless
societi
where
those
of
weaker
charact
ar
shove
to
the
bottom
of
the
financi
ladder
.

