``
The
Tailor
of
Panama
''
is
a
differ
kind
of
spy
movi
.

Despit
the
presenc
of
Pierc
Brosnan
,
there
is
nari
a
hint
of
Jame
Bond
flash
here
.

Instead
of
big
action
scene
,
leer
super
villain
and
empty-head
femm
fatal
,
we
get
interest
charact
and
an
intrigu
situat
.

Base
on
the
novel
by
John
le
Carr
,
who
also
co-wrot
the
screenplai
,
the
film
offer
viewer
someth
rare
seen
in
theater
thi
time
of
year
:
a
solid
,
well-told
stori
.

Brosnan
plai
Andy
Osnard
,
a
British
oper
walk
on
thin
ic
.

Hi
British
superior
ship
him
off
to
Panama
,
make
sure
he
understand
that
he
had
better
not
screw
up
the
placement
.

Osnard
arriv
in
the
tropic
virtual
ooz
contempt
for
hi
new
co-work
and
hi
new
home
base
.

When
shown
the
Bridg
of
the
America
by
a
person
marvel
over
the
fact
that
,
sinc
the
creation
of
the
Panama
Canal
,
the
structur
is
the
sole
connect
between
North
and
South
America
,
he
bare
keep
from
yawn
.

Brosnan
clearli
relish
the
chanc
to
be
the
antithesi
of
007
,
invest
the
suav
spy
with
a
distinct
reptilian
qualiti
coupl
with
an
air
of
indiffer
that
irk
hi
fellow
to
no
end
.

Search
for
a
wai
to
get
inform
on
the
govern
,
Osnard
set
hi
sight
on
Harri
Pendel
-LRB-
Geoffrei
Rush
-RRB-
,
an
unctuou
tailor
serv
the
Panamanian
elit
.

Harri
claim
to
be
a
transplant
from
Britain
's
renown
Savil
Row
,
but
Osnard
know
hi
secret
:
The
tailor
is
an
ex-con
who
serv
five
year
in
prison
after
torch
hi
uncl
's
shop
in
an
insur
scam
.

He
also
know
that
Harri
is
up
to
hi
ear
in
debt
,
have
spent
a
fortun
on
an
unsuccess
farm
.

Osnard
offer
Harri
a
wai
out
.

He
will
pai
for
inform
Harri
ha
gather
from
hi
upscal
clientel
.

Eager
to
compli
,
Harri
offer
what
he
know
,
then
start
concoct
tidbit
to
keep
the
monei
flow
.

Befor
long
,
he
is
forc
to
spy
on
hi
love
wife
,
Louisa
-LRB-
Jami
Lee
Curti
-RRB-
,
an
aid
to
the
Canal
director
.

Harri
's
littl
li
build
,
eventu
take
on
a
life
of
their
own
.

Osnard
turn
in
report
about
the
``
silent
opposit
,
''
a
group
threaten
to
upset
the
balanc
of
control
over
the
canal
.

The
bogu
revel
prove
explos
,
lead
to
meet
in
Washington
D.C.
over
how
to
best
protect
the
vital
waterwai
.

Meanwhil
,
Osnard
wallow
in
hi
newfound
statu
,
enjoi
the
best
the
citi
ha
to
offer
and
woo
an
attract
co-work
.

And
Harri
grow
ever
more
fret
,
fear
the
consequ
if
hi
client
,
not
to
mention
hi
wife
,
learn
about
hi
li
.

Geoffrei
Rush
is
wonder
as
Harri
,
fawn
over
hi
custom
by
dai
and
savor
hi
rich
famili
life
in
the
even
,
all
while
plai
secret
agent
in
the
off
hour
with
hi
new
benefactor
.

Rush
make
Harri
a
credibl
figur
,
present
the
variou
level
of
the
charact
so
effect
that
he
remain
sympathet
despit
hi
duplic
.

After
gain
fame
plai
larger
than
life
charact
,
Rush
flip
everyth
around
for
thi
role
,
us
hi
energi
to
depict
the
quiet
desper
of
a
man
slowli
realiz
that
the
solut
to
hi
troubl
mai
be
wors
than
the
origin
problem
.

Although
she
get
far
too
littl
screen
time
,
Jami
Lee
Curti
imbu
Louisa
with
a
depth
greater
than
the
screenplai
provid
her
.

Of
the
central
charact
,
she
is
the
onli
on
that
exhibit
matur
and
genuin
self-confid
.

Curti
is
on
of
my
favorit
femal
actor
;
whiplash
smart
,
sultri
and
charismat
,
she
deserv
more
than
support
role
.

Speak
of
support
charact
,
there
ar
some
great
on
here
.

Harold
Pinter
is
amus
as
Harri
's
Uncle
Benni
,
who
pop
up
throughout
the
stori
in
a
number
of
creativ
wai
.

Also
shore
up
the
proceed
is
Jon
Polito
as
a
corrupt
banker
and
Dylan
Baker
,
who
is
a
riot
as
a
Unite
State
gener
straight
out
of
the
Dr.
Strangelov
school
of
arm
patriot
.

But
the
most
import
secondari
perform
ar
Leonor
Varela
and
Brendan
Gleeson
,
both
outstand
as
two
wound
activist
.

Their
presenc
add
weight
to
the
stori
,
remind
us
that
beyond
the
charad
of
the
lead
men
,
there
ar
real
peopl
that
have
suffer
greatli
over
the
polit
connect
to
the
Canal
.

Director
John
Boorman
add
addit
verisimilitud
by
shoot
the
film
on
locat
.

Instead
of
us
Panama
Citi
mere
as
a
color
backdrop
,
he
adroitli
weav
in
footag
of
all
aspect
of
life
there
.

The
metropoli
,
describ
by
on
charact
as
``
Casablanca
without
hero
''
pulsat
with
life
,
underl
the
folli
of
the
two
foreign
plai
danger
game
that
could
have
a
disastr
impact
on
a
great
mani
peopl
.

