Histor
epic
as
a
genr
wa
almost
banish
from
Hollywood
in
earli
1990
.

For
mani
critic
,
scholar
and
,
most
importantli
,
film
produc
,
it
wa
a
thing
of
the
past-someth
that
could
have
attract
crowd
onli
in
the
era
of
black-and-whit
televis
and
non-CGI
special
effect
.

But
in
1995
,
same
as
in
the
case
of
DANCES
WITH
WOLVES
five
year
ago
,
there
came
the
man
who
decid
to
us
of
all
hi
energi
and
talent
of
actor
,
produc
and
director
in
order
to
revitalis
that
particular
genr
.

And
,
same
as
in
the
case
of
DANCES
WITH
WOLVES
,
he
wa
award
for
hi
effort
with
``
Oscar
''
.

The
name
of
the
film
wa
BRAVEHEART
,
and
the
name
of
it
director
and
main
actor
wa
Mel
Gibson
.

The
plot
chronicl
the
life
of
Scottish
warlord
and
folk
hero
William
Wallac
-LRB-
1272-1305
-RRB-
.

In
hi
time
,
English
king
Edward
I
``
Longshank
''
-LRB-
plai
by
Patrick
McGoohan
-RRB-
ha
almost
complet
subdu
entir
British
Isle
,
includ
Scotland
,
whose
proud
inhabit
ar
now
subject
by
``
iu
prima
nocti
''
and
variou
other
form
of
humili
and
oppress
by
English
knight
.

Although
English
had
kill
hi
father
mani
year
ago
,
young
Scottish
common
William
Wallac
-LRB-
plai
by
Mel
Gibson
-RRB-
doe
n't
care
about
polit
and
want
onli
to
have
peac
life
with
hi
new
bride
Murron
-LRB-
plai
by
Catherin
MacCormack
-RRB-
.

Murron
's
death
chang
that
and
Wallac
take
arm
and
,
togeth
with
small
but
dedic
band
of
follow
,
begin
guerrilla
campaign
that
would
systemat
destroi
almost
everi
English
garrison
in
the
countri
.

Through
time
,
more
and
more
peopl
join
hi
rank
and
in
1298
Wallac
's
rag-tag
armi
score
surpris
victori
against
English
knight
in
the
Battl
of
Stirl
Bridg
.

But
the
newli
won
freedom
is
endang
by
the
intern
squabbl
between
Scot
themselv
,
especi
nobl
under
pretend
Robert
the
Bruce
-LRB-
plai
by
Angu
MacFadyen
-RRB-
,
who
would
n't
like
to
have
a
simpl
common
as
a
nation
leader
.

BRAVEHEART
wa
onli
the
second
film
in
the
directori
career
by
Mel
Gibson
,
so
it
would
n't
be
fair
to
compar
it
to
the
better
known
genr
classic
direct
by
David
Lean
or
Anthoni
Mann
.

Howev
,
even
when
we
appli
such
criteria
BRAVEHEART
is
more
than
decent
histor
epic
that
take
new
and
refresh
approach
to
the
genr
.

The
most
notic
element
of
thi
approach
is
natur
.

While
older
Hollywood
film
set
in
mediev
Europ
us
to
give
fairytale-lik
vision
of
picturesqu
castl
,
splendid
costum
and
knight
in
shine
armour
,
Gibson
show
us
the
extrem
unpleas
world
of
poverti
and
feudal
injustic
where
might
made
right
and
life
wa
hard
,
short
and
brutish
.

Thi
gener
unpleas
is
especi
underlin
in
the
scene
of
battl
,
where
hand-to-hand
combat
is
naturalist
displai
in
all
it
gori
detail
,
never
seen
sinc
Verhoeven
's
ultra-naturalist
FLESH
&
BLOOD
.

Compar
with
thi
naturalist
approach
,
most
of
Old
Hollywood
epic
look
like
stage
plai
.

BRAVEHEART
also
emploi
humour
at
the
right
place
,
not
onli
to
make
bloodi
realiti
of
mediev
British
Isle
more
bearabl
to
sensit
audienc
,
but
also
to
underlin
it
down-to-earth
atmospher
,
so
differ
from
previou
histor
epic
.

What
make
BRAVEHEART
``
larger
than
life
''
is
hi
protagonist
.

Mel
Gibson
obvious
invest
plenti
of
hi
energi
and
talent
in
bring
the
mythic
figur
of
William
Wallac
and
,
as
a
result
,
charact
of
15th
Centuri
poem
is
transform
into
superhuman
be
that
resembl
late
20th
Centuri
comic
book
hero
.

Wallac
is
more
than
superior
to
hi
enemi
,
both
physic
and
mental
,
yet
he
is
abl
to
show
hi
sensit
,
more
vulner
side
.

Natur
,
when
the
charact
is
base
on
epic
poem
,
most
of
histor
accuraci
is
go
down
the
drain
-LRB-
which
wa
the
case
with
almost
everi
histor
epic
from
Hollywood
in
1990
-RRB-
.

Howev
,
even
those
who
do
n't
particularli
care
about
histori
or
ar
will
to
give
poetic
licens
to
scriptwrit
Randal
Wallac
might
think
that
he
went
over
the
top
in
idolis
Wallac
,
especi
in
the
second
part
of
film
when
Scottish
superhero
manag
to
seduc
Princess
Isabel
-LRB-
plai
by
Sophi
Marceau
-RRB-
and
make
her
a
child
.

Becaus
of
that
the
disbelief
is
hardli
suspend
,
and
final
scene
ar
n't
as
effect
as
thei
should
be
.

Mel
Gibson
as
an
actor
is
truli
wonder
,
and
he
is
also
help
by
small
armi
of
veri
good
British
and
Irish
actor
.

Most
memor
of
them
all
is
Patrick
McGoohan
,
whose
cold
,
calcul
and
evil
mediev
monarch
is
on
of
the
most
effect
villain
seen
in
contemporari
cinema
.

Brendan
Gleeson
-LRB-
who
would
later
becom
famou
for
hi
role
in
Boorman
's
GENERAL
-RRB-
is
also
veri
good
as
Wallac
's
trust
companion
,
as
well
as
Ian
Bannen
is
effect
as
leprosy-stricken
King
John
Balliol
.

David
O'Harra
is
veri
entertain
as
Wallac
's
Irish
alli
,
and
Angu
MacFadyen
bring
a
lot
of
anxieti
into
hi
conscience-stricken
charact
of
Robert
the
Bruce
.

Women
in
thi
film
have
rather
thankless
role
,
especi
Marceau
as
one-dimension
French
princess
.

Catherin
McCormack
as
Wallac
's
wife
wa
more
interest
,
but
she
wa
elimin
from
the
pictur
earli
on
.

From
the
technic
point
of
view
,
the
film
is
also
veri
good
.

Photographi
by
John
Toll
bring
a
lot
of
Scottish
natur
beauti
to
the
screen
.

On
the
other
hand
,
the
music
score
by
Jame
Horner
emploi
too
much
of
modern-dai
instrument
for
period
piec
set
in
mediev
time
.

Although
mostli
welcom
by
critic
and
audienc
alik
,
BRAVEHEART
wa
often
criticis
for
homophobia
and
other
form
of
extrem
conservat
.

The
main
reason
for
that
wa
the
fact
that
Gibson
,
among
mani
histor
fact
,
chose
to
be
faith
onli
to
those
relat
to
sexual
orient
of
``
Longshank
''
's
son
Princ
Edward
-LRB-
plai
by
Peter
Hanli
-RRB-
.

He
is
not
onli
present
as
a
villain
,
but
also
as
on
of
the
less
compet
in
villain
's
camp-and
portrai
of
homosexu
as
villain
or
inferior
to
heterosexu
is
almost
unforgiv
sin
in
``
polit
correct
''
Hollywood
.

To
make
thing
even
more
obviou
,
Good
Gui
in
thi
film
ar
the
total
opposit
of
effemin
and
incompet
Edward-rug
macho
men
of
mountain
who
ar
more
practic
and
effici
becaus
thei
tend
to
emploi
conserv
'
common
sens
instead
of
fashion
theori
of
ivori
tower
liber
.

Howev
,
although
the
messag
of
thi
film
might
look
right-w
,
it
ow
more
to
leftist
ideolog
of
variou
nation
liber
movement
in
20th
Centuri
.

Accord
to
that
ideolog
,
oppress
class
-LRB-
worker
,
peasant
-RRB-
ar
better
suit
to
fight
for
nation
freedom
becaus
their
member
ha
less
to
lose
in
that
struggl
and
,
as
such
,
thei
ar
more
radic
,
less
will
to
make
compromis
and
more
effici
;
on
the
other
hand
,
privileg
class
-LRB-
aristocraci
,
bourgeoisi
-RRB-
ar
more
like
to
betrai
the
caus
of
freedom
and
be
in
cahoot
with
foreign
oppressor
in
order
to
keep
their
privileg
.

Thi
Marxist
worldview
is
present
in
BRAVEHEART
without
ani
subtleti
.

In
the
end
,
whether
the
audienc
is
patient
enough
to
discov
hidden
mean
or
simpli
want
to
enjoi
entertain
histor
epic
,
BRAVEHEART
is
more
than
adequ
choic
for
all
fan
of
thi
recent
resurrect
genr
.

