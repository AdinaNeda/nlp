Peopl
who
enjoi
scienc
fiction
ar
often
face
with
unpleas
surpris
due
to
the
improp
label
of
novel
,
stori
,
comic
book
or
movi
.

Often
scienc
fiction
aficionado
find
materi
,
previous
label
as
scienc
fiction
,
to
be
pure
fantasi
or
supernatur
horror
,
or
rather
simpl
techno-thril
.

In
such
case
,
mistak
ar
understandable-genr
boundari
ar
never
clearli
mark
.

But
,
I
believ
there
ar
realli
few
case
when
a
movi
label
as
scienc
fiction
actual
happen
to
be
straight
histor
epic
.

Such
thing
occur
in
former
Yugoslavia
some
fifteen
year
ago
,
when
nation
distributor
gave
such
treatment
to
THE
RIGHT
STUFF
,
1983
film
direct
by
Philip
Kaufman
.

The
mistak
of
the
distributor
could
be
explain
with
the
fact
that
the
offici
poster
of
the
film
featur
men
in
space
suit
.

But
THE
RIGHT
STUFF
film
is
n't
even
fiction
.

It
wa
base
on
the
non-fict
book
by
Tom
Wolf
,
cover
the
earli
year
of
American
space
program
.

The
plot
of
the
film
begin
in
1947
when
few
peopl
heard
of
Edward
Air
Forc
Base
,
major
test
site
for
experiment
plane
.

The
major
aim
of
test
flight
is
to
determin
abil
of
man
aircraft
to
reachMach
speed
and
thu
break
the
sound
barrier
.

Mani
pilot
tri
to
achiev
that
goal
and
mani
paid
with
their
live
for
such
bravado
.

But
on
quiet
pilot
,
Chuck
Eager
-LRB-
plai
by
Sam
Shape
-RRB-
succe
and
thu
give
exampl
for
whole
new
gener
of
test
pilot
,
determin
to
enter
histori
book
by
break
new
speed
record
.

Ten
year
later
,
Soviet
have
launch
``
Sputnik
''
mark
the
begin
of
the
Space
Age
.

In
order
to
regain
nation
prestig
,
US
govern
decid
to
be
the
first
to
send
man
into
space
.

Best
Air
Forc
,
Navi
and
Marin
pilot
ar
recruit
into
program
,
but
not
Yeager
,
becaus
he
lack
colleg
educ
and
desir
all-American
imag
.

In
on
of
critic
'
poll
THE
RIGHT
STUFF
wa
name
as
on
among
topfilm
made
in
1980
.

Such
high
posit
could
be
explain
with
the
fact
that
Kaufman
's
film
look
quit
atyp
for
it
time
.

With
more
than
three
hour
of
length
and
epic
scope
it
look
more
suitabl
for
1950
and
1960s-age
when
Hollywood
us
to
make
film
larger
than
life
.

But
the
real
reason
li
in
the
fact
that
it
wa
made
by
truli
remark
and
talent
filmmak
.

Philip
Kaufman
creat
reput
in
1970
by
us
all
the
opportun
of
that
Golden
Age
in
order
to
creat
origin
,
memor
movi
.

THE
RIGHT
STUFF
wa
the
last
of
them
,
swan
song
of
an
era
when
produc
allow
director
to
spend
big
buck
on
unusu
,
risk-tak
``
artsi
''
project
.

THE
RIGHT
STUFF
is
unusu
becaus
it
lack
mani
standard
element
of
Hollywood
film
.

The
plot
is
almost
non-exist
and
not
veri
coher
;
the
stori
,
featur
mani
interest
incid
and
anecdot
,
shift
focu
from
the
old
gener
of
test
pilot
,
embodi
in
Yeager
,
to
new
gener
of
the
astronaut
.

The
film
also
lack
convent
protagonist-Yeag
-LRB-
who
also
appear
in
small
cameo
-RRB-
remain
the
true
hero
of
the
film
,
but
equal
time
and
exposur
is
also
given
to
other
astronaut
and
their
wive
,
make
thi
movi
into
ensembl
piec
.

That
gave
opportun
for
Kaufman
to
us
multitud
of
great
act
talent
,
until
that
specialis
onli
for
bit
or
charact
role
.

Sam
Shepard
is
great
as
Yeager
,
war
hero
whose
greatest
achievement-breech
of
the
sound
barrier-remain
obscur
in
histori
book
,
probabl
due
to
hi
own
modesti
.

On
the
surfac
,
he
lack
person
compar
with
hi
hype
and
more
fortun
astronaut
colleagu
,
but
Shepard
give
textur
to
thi
charact
with
subtl
gestur
and
phrase
.

Shepard
's
perform
is
follow
with
great
act
by
Ed
Harri
as
clean-cut
Marin
-LRB-
and
futur
politician
-RRB-
John
Glenn
.

Young
Denni
Quaid
is
more
than
fine
as
arrog
fighter
jock
Gordo
Cooper
,
and
thi
arrog
is
mirror
in
hi
friend
and
most
tragic
figur
of
Virgil
Grissom
,
superbl
plai
by
Fred
Ward
.

Alwai
reliabl
charact
actor
Scott
Glenn
provid
some
comic
relief
as
Navi
aviat
and
-LRB-
arguabl
-RRB-
first
American
man
in
space
Alan
Shepard
.

Kaufman
left
room
for
femal
talent
to
excel
too-Barbara
Hershei
,
Pamela
Reed
,
Veronica
Cartwright
and
Mari
Jo
Deschanel
ar
great
as
pilot
'
wive
.

Royal
Dano
is
also
impress
as
menac
figur
of
Preacher
,
but
the
most
memor
perform
belong
to
Donald
Moffat
as
Vice-Presid
Lyndon
B.
Johnson
and
Jane
Dornack
as
Nurs
Murch
.

Great
act
talent
assembl
for
thi
film
wa
mirror
with
Kaufman
's
superb
direct
.

Most
notabl
of
all
is
the
method
Kaufman
us
in
order
to
suggest
the
passag
of
time
.

The
begin
of
the
film
show
test
pilot
live
in
the
middl
of
desert
,
as
virtual
unknown
,
far
awai
from
strict
rule
and
disciplin
.

Everyth
seem
natural-Yeag
can
indulg
himself
in
hors
ride
just
few
yard
awai
from
the
fastest
and
most
preciou
aircraft
in
the
world
.

In
that
set
,
it
's
quit
normal
for
import
technic
problem
to
be
solv
with
simpl
chainsaw
and
for
the
test
pilot
not
to
report
their
rib
be
broken
befor
most
import
flight
.

But
the
time
ar
chang
;
WW2
allianc
with
USSR
is
replac
with
Cold
War
,
and
``
natur
''
pilot
like
Yeager
must
be
replac
with
more
disciplin
,
convent
pilot
.

Their
entir
live
becom
focu
of
media
frenzi
,
and
their
job
is
subject
to
strict
rule
,
meticul
plan
and
thei
must
fight
the
bureaucrat
,
publicity-seek
politician
and
uncar
scientist
-LRB-
like
former
Nazi
rocket
expert
Werner
von
Braun
,
plai
by
Scott
Beach
-RRB-
onli
to
preserv
their
most
basic
human
digniti
.

Thi
contrast
is
underlin
with
the
beautifulli
edit
sequence-whil
Yeager
conduct
hi
final
and
most
spectacular
test
flight
in
absolut
obscur
,
``
Mercuri
''
astronaut
receiv
fame
and
fortun
,
although
some
of
them
even
befor
go
in
the
space
.

Those
contrast
and
similar
effect
ar
achiev
with
Kaufman
's
superb
us
of
poetic
movi
languag
.

Edite
is
great
,
and
photographi
by
Caleb
Deschanel
provid
mani
memor
scene
like
the
funer
in
the
desert
or
astronaut
in
their
suit
walk
in
slow
motion
-LRB-
the
same
imag
would
be
copi
in
mani
latter
film
-RRB-
.

Although
Kaufman
enjoi
support
of
NASA
and
American
militari
,
which
provid
authent
locat
and
period
equip
,
he
still
had
to
us
special
effect
in
order
to
simul
space
flight
.

Those
effect
ar
excel
and
thei
can
still
fool
the
audienc
accustom
to
CGI
and
real
footag
of
Earth
from
outer
space
.

Another
fine
contribut
of
thi
film
is
``
Oscar
''
-
award
music
score
by
Bill
Conti
;
I
still
trembl
from
excit
everi
time
I
hear
it
.

It
is
accompani
with
the
us
of
Holst
and
Debussi
.

Some
of
the
song
us
in
the
background
also
provid
authent
atmospher
of
the
histor
period
.

Thi
film
is
great
,
and
it
cult
statu
is
well
deserv
,
but
there
ar
some
minor
flaw
.

Some
of
the
``
Mercuri
''
astronaut
ar
n't
flesh
out
enough
-LRB-
although
be
plai
by
fine
actor
,
like
Lanc
Henriksen
-RRB-
.

Film
also
lack
proper
closur
.

On
the
other
hand
,
most
logic
conclus
of
the
film-land
on
the
Moon-would
requir
thi
film
to
be
more
than
six
hour
long
.

Also
,
some
critic
were
prone
to
attack
thi
film
as
too
Amerocentr
and
hard
on
Russian
,
who
were
portrai
as
evil
monster
,
just
in
line
with
most
virul
Cold
War
rhetor
of
Reagan
's
America
.

In
some
of
contemporari
interview
,
Kaufman
defend
that
approach
by
claim
that
he
want
to
make
film
as
authent
as
possibl
,
and
in
order
to
do
that
,
he
portrai
Russian
not
as
thei
were
in
real
life
,
but
as
thei
were
perceiv
by
American
in
earli
1960
.

Time
ha
pass
;
now
we
do
n't
see
astronaut
as
hero
.

In
thi
ag
of
widespread
satellit
commun
and
routin
space
mission
,
those
men
and
women
in
space
suit
ar
seen
as
mere
mainten
worker
.

Few
young
boi
want
to
be
astronaut
when
thei
grow
up
.

But
thi
film
,
sentiment
remind
of
the
time
``
when
futur
began
''
,
perhap
could
encourag
at
least
some
of
them
to
follow
``
Mercuri
''
astronaut
'
footstep
seek
new
frontier
in
the
sky
.

And
even
if
thei
decid
to
stai
on
Earth
,
thei
could
still
appreci
THE
RIGHT
STUFF
as
an
extraordinari
piec
of
cinema
.

Filmsk
recenzij
na
hrvatskom/Movi
Review
in
Croatian
-LSB-
1
-RSB-
http://film.purger.com

