I
wa
anxiou
to
see
thi
for
a
long
time
.

A
friend
of
mine
recommend
thi
to
me
becaus
he
ha
a
crush
on
Neve
Campbel
,
and
he
want
to
prove
that
she
's
as
hot
as
he
think
he
is
.

He
prove
it
for
me
all
right
,
but
the
reason
I
enjoi
thi
film
go
wai
beyond
that
.

SCREAM
tread
on
familar
ground
,
horror
movi
with
,
as
star
Neve
Campbel
put
it
,
``
Some
stupid
killer
stalk
some
big
breast
girl
who
ca
n't
act
who
's
alwai
run
up
the
stair
when
she
should
be
run
out
the
front
door
.
''

It
's
veri
familar
,
your
typic
horror
film
.

So
why
,
you
mai
ask
after
watch
it
,
doe
it
seem
so
new
and
origin
?

Becaus
it
treat
itself
as
if
it
ha
n't
been
done
befor
,
and
thu
the
viewer
is
disilus
into
believ
that
it
is
not
old
,
familar
tire
stuff
.

Thi
rais
anoth
question
:
How
doe
it
do
thi
?

Simpl
:
The
charact
.

Thei
're
not
your
typic
stupid
horror
film
victim
who
do
n't
know
what
the
hell
to
do
when
be
chase
by
a
gui
in
a
ski
mask
and
a
machett
.

Thei
ar
horror
fan
,
thei
know
how
stuff
happen
in
a
horror
movi
,
thei
know
what
to
do
and
what
not
to
do
,
and
that
make
it
scari
,
and
even
funni
at
certain
time
.

You
can
tell
thi
just
by
watch
the
open
scene
where
Casei
Becker
-LRB-
Drew
Barrymor
-RRB-
is
make
popcorn
,
get
readi
to
watch
a
video
,
and
some
gui
who
sound
like
a
killer
call
her
up
to
plai
a
game
with
her
that
involv
horror
movi
triva
that
,
if
she
get
it
wrong
,
will
get
her
and
her
boyfriend
kill
.

She
know
what
to
answer
when
he
ask
her
horror
film
question
,
she
just
ca
n't
answer
hi
final
question
,
``
What
door
am
I
at
?
''

And
that
get
her
kill
.

That
scene
work
becaus
in
a
normal
horror
film
,
half
the
thing
that
happen
in
that
scene
would
never
happen
.

The
plot
is
typic
horror
stuff
:
Sidnei
Prescott
-LRB-
an
impress
Neve
Campbel
-RRB-
,
an
attract
young
high
school
student
who
's
mother
wa
kill
a
year
ago
tommorrow
,
is
be
stalk
by
a
killer
who
's
previous
murder
on
of
her
classmat
-LRB-
Casei
,
natur
-RRB-
,
and
everybodi
in
thi
small
,
California
town
is
a
suspect
,
includ
Sidnei
's
boyfriend
,
Billi
Loomi
.

But
as
I
said
befor
,
these
peopl
know
how
horror
movi
work
,
and
that
's
what
make
it
so
appeal
.

That
and
the
refer
to
horror
movi
,
like
when
Casei
sai
,
``
The
first
-LRB-
NIGHTMARE
ON
ELM
STREET
-RRB-
wa
scari
,
but
the
rest
suck
.
''

which
is
obvious
director
We
Craven
-LRB-
who
doe
a
good
job
of
poke
fun
of
the
genr
that
made
him
a
legend
-RRB-
pat
himself
on
the
back
as
he
direct
the
first
NIGHTMARE
movi
and
none
of
the
other
,
and
when
Tatum
,
Sidnei
's
friend
,
mention
the
director
,
We
Carpent
,
which
is
obvious
an
homag
by
writer
Kevin
Williamson
to
not
onli
Craven
,
but
also
director
John
Carpent
.

The
cast
is
well
chosen
,
Neve
Campbel
pull
off
a
good
heroin
-LRB-
and
unlik
most
horror
film
heroin
,
she
*
can
*
act
-RRB-
,
Drew
Barrymor
doe
a
good
Janet
Leigh
impress
for
what
littl
time
she
ha
in
the
film
,
and
David
Arquett
is
impress
.

Also
good
-LRB-
and
funni
-RRB-
in
hi
role
is
Henri
Winkler
-LRB-
``
The
Fonz
''
-RRB-
as
the
school
princip
.

I
took
half
a
point
off
for
the
unnecessari
-LRB-
albeit
limit
-RRB-
appear
by
FRIENDS
'
Courtenei
Cox
as
report
,
Gail
Edward
,
Sidnei
's
rival
becaus
she
cover
her
mother
's
disappear
.

I
hate
that
show
,
I
hate
those
actor
.

Thei
're
untal
,
overexpos
,
and
overpaid
,
IMHO
,
and
I
would
think
We
Craven
is
smart
enough
to
know
those
actor
ar
overexpos
enough
,
but
I
suppos
he
is
n't
.

