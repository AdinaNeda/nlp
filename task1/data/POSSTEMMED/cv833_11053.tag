When
it
come
to
modern
gangster
movi
,
it
's
realli
difficult
to
describ
and
review
them
without
make
comparison
to
other
film
of
the
genr
and/or
just
us
the
word
``
routin
.
''

I
've
alwai
subscrib
to
the
philosophi
that
ani
idea
-LRB-
no
matter
how
mani
time
it
's
been
us
befor
-RRB-
can
provid
for
a
good
stori
and
``
Donni
Brasco
''
clinch
thi
idea
.

It
's
not
unlik
most
of
the
great
film
of
the
genr
,
yet
it
never
ap
anoth
's
style
as
it
ha
a
good
layer
of
authent
,
even
if
it
core
is
a
tad
stale
.

The
film
start
off
in
typic
fashion
by
defin
it
atmospher
of
New
York
Citi
in
the
late
1970
and
the
mobster
who
inhibit
it
.

We
meet
Lefti
-LRB-
Pacino
-RRB-
,
an
ag
wisegui
who
can
still
walk
the
walk
and
talk
the
talk
.

He
and
hi
associ
go
through
the
gener
motion
you
expect
to
see
in
crime
film
like
thi
.

Somehow
he
come
across
Donni
Brasco
-LRB-
Depp
-RRB-
,
a
younger
gui
with
a
lot
of
spunk
who
is
n't
afraid
of
Lefti
and
hi
rep
,
and
even
manag
to
befriend
him
after
Lefti
wa
readi
to
kill
him
.

It
's
clear
Donni
is
new
to
the
life
and
Lefti
recogn
thi
immedi
,
tell
him
all
the
trick
of
the
trade
.

I
have
never
seen
thi
techniqu
of
actual
reveal
the
Mafioso
idiosyncrasi
done
befor
and
for
thi
the
film
deserv
credit
.

Howev
,
we
soon
realiz
Donni
is
actual
Joe
Piston
,
an
FBI
agent
work
undercover-a
charact
who
symbol
the
viewer
as
he
will
soon
be
purg
into
the
lifestyl
and
treat
as
a
newcom
.

The
first
act
work
as
a
guid
to
ot
of
rhetor
about
wisegui
honor
,
a
brief
histori
of
the
mob
,
definit
of
their
slang
,
and
where
their
monei
come
from
and
who
it
goe
to
.

The
screenplai
is
rather
sketchi
on
the
detail
surround
these
element
,
howev
,
the
fact
thei
ar
mention
at
all
is
quit
origin
.

Most
gangster
movi
seem
to
be
made
with
the
notion
the
viewer
alreadi
know
how
the
mob
work
-LRB-
probabl
from
watch
other
gangster
movi
-RRB-
,
and
although
thi
attitud
come
across
,
the
film
tri
to
fill
in
all
the
hole
where
and
when
it
can
and
the
effort
is
appreci
.

Thankfulli
the
film
doe
n't
becom
too
caught
up
in
the
tediou
detail
of
organ
crime
,
and
instead
opt
for
charact
develop
.

Much
of
the
stori
is
told
simpli
through
the
interact
between
Lefti
and
Donni
.

Pacino
is
outstand
here
as
the
pathet
hood
who
speak
of
hi
job
in
the
same
manner
ani
blue
collar
worker
would
.

He
's
old
and
exhaust
but
seem
to
enjoi
what
he
doe
,
just
as
anyon
loyal
to
the
same
employ
for
over
30
year
might
be
.

We
learn
of
hi
accomplish
,
which
ar
quit
impress
within
their
context
,
and
when
he
complain
about
not
be
made
top
boss
,
it
's
easi
to
sympath
with
him
.

Newel
constantli
plai
up
thi
aspect
,
make
it
a
major
theme
which
work
well
in
the
long
run
.

As
Donni
is
constantli
impress
and
even
one-up
Lefti
,
it
's
hard
to
tell
which
emot
is
more
power
:
the
fact
Donni
is
get
closer
to
nail
the
mob
;
or
the
fact
Lefti
ha
onc
again
been
over
shadow
.

Most
of
the
film
tell
the
stori
of
Donni
's
life
in
Mafia
,
which
creat
for
mani
sub-plot
and
individu
conflict
,
but
doe
n't
alwai
seem
to
come
togeth
as
a
whole
.

The
storytel
is
genuin
interest
throughout
,
even
when
the
motion
the
charact
go
through
seem
familiar
.

The
reson
to
Donni
's
actual
assign
vari
,
he
often
report
back
to
the
Fed
with
detail
of
hi
progress
,
but
it
doe
n't
alwai
seem
to
have
much
mean
.

Back
home
,
hi
wife
Maggi
-LRB-
Hech
-RRB-
is
readi
to
divorc
him
becaus
he
's
never
a
and
the
famili
is
suffer
.

Sinc
thi
is
base
on
a
true
stori
I
would
n't
doubt
thi
would
happen
,
although
the
wai
it
is
handl
often
border
on
the
melodramat
.

My
onli
major
complaint
is
the
film
seem
to
have
no
final
act
,
or
at
least
ani
real
sens
of
closur
.

A
climax
of
sort
doe
occur
,
but
there
's
littl
feel
of
a
payoff
.

We
get
a
happi
end
,
which
is
good
,
but
perhap
a
sad
end
would
have
been
more
power
.

Aside
from
a
few
minor
,
gener
flaw
,
``
Donni
Brasco
''
manag
to
be
a
solid
piec
of
storytel
and
charact
develop
.

It
mai
be
routin
,
but
it
's
good
,
and
that
's
respect
.

