At
first
glanc
,
it
appear
that
the
Home
Alone
movi
ar
brainless
slapstick
intend
for
those
with
mind
of
8-year
old
.

That
's
true
of
HOME
ALONE
2
and
I
'd
bet
monei
that
it
's
true
of
HOME
ALONE
3
-LRB-
open
soon
-RRB-
.

But
HOME
ALONE
actual
ha
a
lot
go
for
it
,
and
the
cartoon
slapstick
doe
n't
get
in
the
wai
.

The
McAlister
,
all
15
of
them
,
ar
go
to
Franc
for
the
holidai
.

Four
adult
and
eleven
kid
ar
spend
the
night
togeth
befor
head
to
the
airport
EN
MASSE
.

The
littlest
kid
,
Kevin
,
is
the
victim
of
the
older
kid
'
cruelti
.

Kevin
's
patienc
run
out
when
he
learn
that
HIS
plain-chees
pizza
ha
alreadi
been
eaten
,
and
he
's
go
to
have
to
starv
.

He
attack
hi
big
bulli
brother
Buzz
,
who
had
been
teas
him
.

The
fight
disrupt
the
already-chaot
dinner
,
spill
milk
and
soft
drink
all
over
the
tabl
,
a
few
cousin
,
and
an
uncl
.

The
knee-jerk
reaction
is
for
everyon
to
blame
Kevin
.

Nobodi
came
to
hi
rescu
when
he
wa
be
teas
,
but
thei
all
point
their
finger
when
he
fight
back
.

The
ultim
punish
come
from
mom
-LRB-
Catherin
O'Hara
-RRB-
who
send
him
up
to
the
spare
bedroom
in
the
attic
for
the
night
.

On
hi
wai
up
the
stair
,
he
tell
her
that
``
famili
suck
''
and
that
he
hope
never
to
see
ani
of
hi
famili
again
.

That
night
the
power
goe
out
,
knock
out
the
alarm
clock
,
so
the
McAlister
wake
up
in
a
big
hurri
to
get
to
the
airport
.

In
a
surprisingli
plausibl
setup
,
Kevin
is
mistakenli
account
for
,
and
the
famili
head
off
without
him
.

It
's
interest
that
,
in
a
subtl
wai
,
hi
parent
never
notic
him
miss
becaus
of
their
deliber
desir
to
separ
themselv
from
their
children
.

First
thei
deleg
the
headcount
to
an
older
sibl
,
and
second
,
thei
put
all
the
kid
in
coach
while
thei
fly
first
class
.

Thei
're
two-third
of
the
wai
across
the
Atlantic
befor
thei
realiz
thei
ar
miss
a
child
.

Meanwhil
,
Kevin
believ
that
somehow
hi
wish
ha
come
true
.

He
's
final
rid
of
hi
famili
!

Life
is
a
dream
come
true
.

He
can
jump
on
the
bed
,
watch
R-rate
movi
,
eat
gob
of
ic
cream
and
potato
chip
,
and
dig
through
hi
big
brother
's
secret
box
.

Slowli
,
the
novelti
wear
off
and
he
settl
into
the
mundan
.

He
groom
himself
,
he
doe
the
laundri
,
he
goe
shop
-LRB-
``
I
bought
some
milk
,
egg
,
and
fabric
soften
''
-RRB-
.

He
also
start
to
miss
hi
famili
,
not
just
for
their
compani
,
but
for
the
secur
thei
bring
.

The
furnac
in
the
basement
is
a
scari
monster
,
the
old
man
next
door
is
a
snow-shovel
murder
,
and
two
crook
ar
try
to
break
into
hi
hous
.

Macaulai
Culkin
is
cute
but
he
is
n't
much
of
an
actor
.

Still
,
we
can
see
that
Kevin
's
charact
grow
,
thank
to
some
good
direct
and
edit
.

For
exampl
,
act
ha
littl
to
do
with
the
emot
impact
of
the
scene
where
Kevin
search
out
a
season
Santa
at
night
,
just
as
``
Santa
''
is
get
in
hi
car
.

Kevin
ask
the
man
to
pass
on
hi
request-h
want
hi
famili
back-to
the
real
Santa
.

He
learn
how
to
deal
with
the
furnac
.

You
turn
on
the
light
and
it
's
not
so
scari
anymor
.

He
goe
to
a
church
on
even
and
there
he
see
the
snow-shovel
murder
,
who
actual
turn
out
to
be
a
nice
man
.

Thei
talk
about
their
fear
and
Kevin
learn
that
ag
alon
doe
not
make
you
less
afraid
.

Final
,
in
the
last
20
minut
,
Kevin
confront
hi
last
fear-th
bandit
,
head
on
-LRB-
liter
-RRB-
.

Thi
last
act
is
full
of
violent
comed
slapstick
,
and
after
the
movi
's
genuin
dramat
setup
,
it
is
dessert
.

It
's
a
lot
of
fun
,
and
it
doe
n't
overwhelm
the
rest
of
the
movi
.

On
the
whole
,
Home
Alone
is
a
great
holidai
movi
.

John
William
'
origin
score
ha
the
sound
and
feel
of
holidai
music
without
the
disadvantag
of
be
overplai
at
the
mall
,
and
the
Christma
set
can
put
you
in
the
mood
for
those
famili
gather
.

But
it
doe
have
it
flaw
.

The
smallest
problem
is
that
Culkin
doe
n't
act
veri
well
.

As
I
said
befor
,
he
IS
cute
,
but
repeat
view
reveal
more
about
how
Columbu
cover
himself
than
about
Culkin
's
insight
into
hi
charact
.

The
biggest
problem
is
that
dure
the
cartooni
coda
,
the
villain
threaten
Kevin
's
life
about
five
time
.

Instead
of
``
I
'm
gon
na
GET
that
kid
''
thei
sai
``
I
'm
gon
na
KILL
that
kid
.
''

Some
might
argu
that
the
tone
is
the
same
,
but
I
strongli
disagre
.

Mix
comedi
with
specif
and
viabl
threat
of
murder
is
a
sociopath
faux-pa
.

I
'll
probabl
never
see
HOME
ALONE
3
,
and
I
wish
I
had
skip
HOME
ALONE
2
,
but
I
do
enjoi
HOME
ALONE
almost
everi
year
.

Do
n't
let
your
impress
of
the
other
detract
from
the
origin
,
which
ha
much
more
to
offer
than
cartoon
slapstick
.

