East
meet
West
in
MULAN
,
the
latest
instal
in
Disnei
's
parad
of
annual
anim
featur
film
.

An
odd
fusion
of
ancient
Asian
tradit
and
disconcert
element
which
betrai
obviou
mass-market
commerci
sensibl
,
thi
confid
entri
is
easili
the
most
engag
and
satisfi
Hollywood
anim
featur
in
year
,
but
fall
short
of
actual
it
potenti
.

Though
not
for
lack
of
try
.

In
mani
respect
,
MULAN
is
among
Disnei
's
most
matur
effort
,
skirt
the
surfac
of
adult
theme
and
dare
to
sneak
glimps
into
the
messier
side
of
combat
.

The
violenc
of
the
battl
in
the
film
ar
admittedli
sanit
--
the
imageri
of
a
pillag
villag
,
neatli
encapsul
in
the
form
of
a
child
's
discard
doll
,
is
in
itself
no
more
harrow
than
Luke
discov
hi
fallen
guardian
in
STAR
WARS
,
and
Mulan
's
later
heroic
in
dispatch
of
the
ruthless
invad
is
done
so
in
the
most
tidi
,
bloodless
method
imagin
--
yet
how
often
doe
a
Hollywood
anim
film
,
long
consid
as
the
last
safe
refug
for
the
innoc
,
dare
touch
upon
the
consequenti
horror
of
war
in
such
a
provoc
manner
?

In
an
instant
,
idealist
Mulan
's
innoc
is
lost
,
and
her
jovial
squadron
ar
remind
of
what
their
effort
ar
spent
defend
.

The
film
,
base
upon
an
ancient
Chines
fabl
,
center
on
a
spirit
young
woman
who
disguis
herself
as
a
man
in
order
to
serv
as
her
famili
's
obligatori
conscript
when
China
find
itself
under
attack
by
Hun
-LRB-
cartoonishli
depict
by
easi
demon
overton
,
replet
with
glow
yellow
ey
and
dagger-edg
teeth
,
although
lent
a
surprisingli
genuin
sens
of
menac
-RRB-
.

Dip
into
the
mytholog
of
other
cultur
for
the
first
time
after
cavort
in
anim
revisit
of
familiar
Western
/
European
fabl
and
literatur
for
decad
,
Disnei
's
new
film
is
boldli
set
in
ancient
China
rather
than
reloc
elsewher
,
stori
intact
,
to
some
neutral
territori
,
and
revel
in
it
East
Asian
root
;
asid
from
a
predominantli
Asian-American
cast
provid
the
film
's
vocal
work
,
there
's
liber
usag
of
Oriental
iconographi
throughout
,
and
the
artwork
reveal
hint
of
Japanes
anim
influenc
.

While
the
film
doe
occasion
reach
it
limit
and
start
to
tread
upon
old
stereotyp
--
invok
pseudo-Asian
refer
to
item
like
beancurd
and
rice
in
everi
third
sentenc
grow
rather
hoari
,
and
I
ca
n't
imagin
that
the
regiment
for
ancient
Chines
warrior
*
realli
*
includ
karate-chop
block
of
stone
in
two
--
it
's
rather
remark
that
a
movi
primarili
posit
for
American
and
European
audienc
delv
so
wholeheartedli
into
new
foreign
territori
.

The
film
also
freeli
touch
upon
cultur
idiosyncrasi
from
it
outset
;
although
savvi
viewer
should
easili
pick
up
on
the
intrins
immedi
,
it
's
startlingli
assert
in
provid
as
littl
shorthand
as
it
doe
to
unfamiliar
youngster
with
regard
to
the
custom
and
tradit
of
Mulan
's
land
.

At
it
core
,
the
film
is
a
tale
of
heroic
adventur
,
and
as
such
paint
it
stori
in
bold
stroke
of
honour
,
persever
,
nobil
and
courag
.

As
such
,
anim
is
the
perfect
vehicl
for
thi
flavour
of
epic
,
where
stark
emot
simplic
is
abl
to
effect
work
hand-in-hand
with
visual
puriti
.

Scene
which
would
plai
out
as
unbear
hokei
in
live
action
,
such
as
Mulan
's
vener
father
pridefulli
toss
asid
hi
cane
when
accept
hi
draft
notic
,
ar
lent
a
sens
of
strength
and
convei
with
greater
emot
clariti
in
anim
.

MULAN
capit
on
thi
dure
it
strongest
moment
,
particularli
dure
our
heroin
's
character-defin
open
half-hour
.

It
's
when
the
film
attempt
to
integr
the
tradit
mainstai
of
recent
Hollywood
anim
--
the
friendli
anim
sidekick
,
the
colour
arrai
of
support
charact
--
into
the
mix
where
it
goe
slightli
awri
.

Off
to
battl
,
Mulan
find
herself
be
trail
by
a
jive-talk
diminut
dragon
call
Mushu
and
an
agreeabl
littl
``
lucki
''
cricket
,
who
ostens
appear
to
assist
in
her
subterfug
and
realist
serv
as
audience-friendli
comic
relief
.

Thei
're
affabl
presenc
,
and
not
precis
ineffect
--
reason
amount
of
humour
ar
elicit
,
and
Eddie
Murphi
-LRB-
who
voic
Mushu
-RRB-
is
amusingli
earnest
--
but
amidst
all
the
high
melodrama
,
the
effect
is
mildli
jar
and
,
from
a
storytel
perspect
,
question
in
it
necess
.

At
train
camp
,
Mulan
antagon
and
eventu
win
over
a
trio
of
fumbl
recruit
-LRB-
easili
character
simpli
as
the
Short
Clown
,
the
Skinni
Clown
,
and
the
Big
Clown
-RRB-
,
who
provid
more
leviti
into
the
film
with
decidedli
less
effect
,
and
,
in
a
familiar
retread
of
tradit
Disnei
pattern
,
she
grow
a
certain
fond
for
a
handsom
,
square-jaw
young
captain
,
Shang
.

Everybodi
need
love
,
even
ancient
Chines
warrior
princess
,
but
not
necessarili
in
*
thi
*
film
.

The
romant
angl
,
which
let
the
air
out
of
the
sail
of
the
stori
's
neo-feminist
underpin
,
lack
ani
genuin
spark
and
plai
out
like
a
contractu
oblig
.

Another
stapl
of
recent
Disnei
anim
film
,
a
cach
of
hummabl
littl
ditti
,
ha
also
obligingli
been
carri
over
to
MULAN
but
prove
to
be
a
decid
weak
.

Compos
by
Matthew
Wilder
,
none
of
the
hand
of
tune
ar
particularli
distinguish
,
and
on
--
``
I
'll
Make
A
Man
Out
Of
You
''
-LRB-
unimagin
accompani
a
tire
montag
featur
our
crew
of
charact
transform
into
lean
fight
machin
-RRB-
--
becom
so
grate
and
hacknei
that
it
seem
almost
intermin
.

Only
``
Reflect
''
,
an
earli
ballad
of
lament
,
prove
to
be
a
song
of
ani
consequ
,
serv
as
an
effect
showcas
for
Lea
Salonga
,
on
of
music
theatr
's
most
wondrous
pure
voic
.

Nonetheless
,
MULAN
is
an
especi
appeal
piec
of
work
from
the
Disnei
factori
,
with
a
strong
narr
arc
and
pleasant
artwork
;
the
computer-gener
stamped
of
Hun
warrior
charg
down
a
snow
mountain
is
wholli
impress
.

While
the
blatantli
child-friendli
element
at
time
seem
incongru
with
the
remaind
of
the
film
,
thei
're
not
dissuas
and
remain
fairli
enjoy
.

Briskli
pace
with
a
smart
,
energet
heroin
and
fill
with
high
adventur
,
the
film
is
ultim
most
satisfi
on
the
intim
scale
of
father
and
daughter
.

