Base
on
the
Bori
Karloff
's
classic
by
the
same
name
,
The
Mummi
start
off
with
the
high-priest
of
Osiri
,
Imhotep
,
who
murder
the
pharoah
for
hi
mistress
and
is
punish
by
be
mummifi
in
the
most
horrifi
wai
possibl
--
bandag
up
,
have
hi
tongu
remov
,
and
be
cover
in
flesh-eat
scarab
beetl
,
then
entomb
...
all
while
still
aliv
.

Recap
a
few
thousand
year
later
,
where
a
soldier
name
Rick
-LRB-
plai
by
Brandan
Fraser
-RRB-
aid
a
young
historian
name
Evelyn
-LRB-
plai
by
Rachel
Wiesz
-RRB-
and
her
brother
,
Jonathan
-LRB-
plai
by
John
Hannah
-RRB-
in
find
the
book
of
Amon
Ra
,
in
the
process
inadvert
free
the
Mummi
.

Problem
is
that
the
Mummi
want
to
reviv
hi
mistress
,
us
Evelyn
as
a
sacrific
...
Walk
in
with
rel
low
expect
,
I
thought
thi
movi
wa
actual
pretti
good
.

The
visual
and
CGI
ar
astound
,
and
obvious
not
cheapli
done
at
all
.

Thei
pack
a
ton
of
detail
into
the
imag
,
especi
dure
scene
involv
mummi
render
complet
by
CGI
.

The
comput
special
effect
make
for
some
brilliant
scene
,
such
as
unnerv
moment
involv
flesh-eat
scarab
beetl
and
moment
where
the
Mummi
goe
after
the
peopl
who
freed
him
-LRB-
after
all
,
those
who
took
hi
artifact
ar
curs
-RRB-
.

Unfortun
,
the
film
attempt
to
be
wai
too
much
in
such
a
short
span
of
time
,
becom
a
tug
of
war
for
control
between
genr
.

On
on
hand
,
it
is
a
fast
pace
action
film
.

On
the
other
,
it
's
a
frighten
horror
film
.

And
on
the
side
,
it
's
a
hilari
comedi
.

Idealli
,
for
it
to
be
success
,
the
film
would
have
to
focu
on
on
the
more
action
orient
aspect
,
with
on
charact
serv
as
the
comic
relief
-LRB-
that
would
be
Johnson
-RRB-
.

The
problem
with
thi
film
is
that
it
ha
at
least
three
charact
serv
as
comic
relief
,
with
Rick
occasion
deliv
hi
witti
one-lin
.

If
thei
were
try
to
make
a
horror-action-comedi
,
it
would
have
help
if
it
were
establish
earli
on
in
the
film
,
but
unfortun
,
with
the
backstori
of
Imhotep
's
entomb
,
that
would
be
imposs
.

And
then
there
's
the
slapstick
fight
scene
between
a
sword-wield
Rick
and
an
armi
of
mummi
.

While
realli
well
done
,
it
had
the
feel
of
slapstick
comedi
.

Replac
the
sword
with
a
chainsaw
and
you
'd
effect
have
Ash
fight
zombi
in
Army
of
Dark
.

While
entertain
and
funni
,
it
feel
realli
out
of
place
.

But
at
least
it
's
a
break
from
the
naiv
hero
that
Brendan
Fraser
ha
been
plai
a
lot
of
.

On
the
whole
,
the
movi
is
pure
popcorn
fare
from
begin
to
end
,
entertain
the
audienc
.

But
I
must
end
my
review
with
a
plea
to
movi
theatr
owner
...
TURN
THE
SOUND
DOWN
!

While
a
lot
of
theatr
have
good
sound
system
,
my
ear
were
almost
ring
as
I
walk
out
of
the
theatr
-LRB-
the
sound
is
particularli
irrit
and
will
make
you
crap
your
pant
if
you
're
not
care
-RRB-
.

When
thei
were
show
the
trailer
for
the
upcom
Schwarzenegg
film
,
``
End
of
Dai
''
,
it
wa
so
loud
I
could
n't
make
anyth
out
.

