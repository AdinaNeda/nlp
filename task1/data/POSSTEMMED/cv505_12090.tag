Some
critic
,
includ
Siskel
&
Ebert
,
ar
on
the
record
state
that
Martin
Scorses
's
RAGING
BULL
,
the
stori
of
boxer
Jake
La
Motta
,
is
the
best
film
of
the
'80
.

Sinc
there
ar
still
a
number
of
'80
film
that
I
have
n't
seen
,
I
do
n't
feel
qualifi
to
make
such
a
judgment
,
but
I
'll
sai
thi
without
hesit
:
RAGING
BULL
is
a
great
motion
pictur
,
and
I
would
be
surpris
if
more
than
a
hand
of
film
releas
between
Januari
1
,
1980
and
Decemb
31
,
1989
come
close
to
it
level
.

Bio-pic
often
fall
into
on
of
two
categori
:
overblown
hero-worship
or
a
dry
,
dull
textbook
account
.

It
's
rare
that
a
movi
with
the
monik
``
base
on
the
life
of
''
come
across
as
anyth
more
than
sporad
energet
and
margin
entertain
.

RAGING
BULL
is
the
perfect
counterexampl
,
and
a
brilliant
argument
for
film
maker
to
continu
to
work
in
thi
genr
.

The
pictur
take
the
life
of
boxer
Jake
La
Motta
,
a
Middleweight
icon
from
the
'40
and
'50
,
and
develop
on
of
the
most
compel
charact
studi
ever
to
reach
the
big
screen
.

For
all
129
minut
,
director
Martin
Scorses
and
actor
Robert
De
Niro
-LRB-
in
the
titl
role
-RRB-
have
us
mesmer
by
thi
individu
who
is
by
turn
sympathet
,
sad
,
and
horrifi
.

Histor
,
RAGING
BULL
is
said
to
be
mostli
accur
,
with
the
lion
's
share
of
the
detail
cull
from
La
Motta
's
own
autobiographi
-LRB-
which
wa
co-written
with
Joseph
Carter
and
Peter
Savag
-RRB-
.

Howev
,
although
La
Motta
is
credit
as
the
movi
's
``
consult
,
''
he
wa
reportedli
unhappi
with
the
final
result
,
becaus
it
portrai
him
as
noth
short
of
an
uncouth
,
insensit
lout
.

The
La
Motta
of
the
movi
is
a
man
of
extrem
appetit
who
is
driven
by
base
,
bestial
impuls
paranoia
,
jealousi
,
and
blind
rage
.

Sex
and
violenc
ar
inextric
link
.

The
titl
is
apt
La
Motta
is
a
rage
bull
.

In
the
ring
,
he
is
a
terror
,
not
just
beat
hi
oppon
,
but
destroi
them
.

Outsid
of
it
,
he
is
no
less
viciou
or
more
easili
control
.

The
two
peopl
who
spar
with
him
on
life
's
canva
ar
hi
wife
,
Vicki
-LRB-
Cathi
Moriarti
-RRB-
,
and
hi
brother/manag
,
Joei
-LRB-
Joe
Pesci
-RRB-
.

These
ar
the
two
who
mean
the
most
to
Jake
,
and
who
,
as
a
result
of
hi
inabl
to
trust
himself
or
other
,
he
lose
.

He
beat
Vicki
mercilessli
becaus
he
suspect
her
of
infidel
,
and
,
when
he
inaccur
believ
that
hi
brother
betrai
him
,
hi
let
loos
with
an
explos
of
violenc
.

RAGING
BULL
open
in
1941
when
La
Motta
,
an
up-and-com
fighter
,
is
battl
hi
wai
into
the
upper
echelon
of
the
Middleweight
Class
.

Over
the
year
,
he
win
sever
kei
bout
,
includ
on
against
hi
arch-riv
,
Sugar
Rai
Robinson
-LRB-
Johnni
Barn
-RRB-
,
but
hi
unwilling
to
capitul
with
the
local
godfath
keep
him
from
an
opportun
to
particip
in
a
championship
match
.

Meanwhil
,
awai
from
the
ring
,
La
Motta
fall
in
love
with
15-year
old
Vicki
,
who
he
marri
after
discard
hi
shrewish
first
wife
.

Vicki
becom
Jake
's
greatest
prize
-LRB-
a
wife
,
in
hi
view
,
is
not
a
companion
,
but
a
possess
-RRB-
and
the
sourc
of
hi
most
extrem
pain
.

Hi
own
insecur
is
so
great
that
he
can
not
accept
that
a
woman
as
beauti
as
Vicki
could
be
faith
to
him
.

Henc
,
he
is
constantli
haunt
by
a
belief
that
she
is
sleep
with
someon
els
perhap
even
hi
brother
.

-LRB-
Thi
lead
to
the
famou
line
parodi
in
WAITING
FOR
GUFFMAN
and
elsewher
:
``
Are
you
f**king
my
wife
???
''
-RRB-
In
the
late
'40
,
La
Motta
get
hi
first
shot
at
a
championship
fight
,
but
with
on
huge
condit
:
he
must
take
a
fall
.

He
doe
it
so
badli
that
an
investig
is
launch
and
he
is
almost
thrown
out
of
box
.

Two
year
later
,
he
win
the
championship
,
onli
to
lose
it
in
a
subsequ
bout
to
Sugar
Rai
.

By
the
late
'50
and
earli
'60
,
when
the
film
end
,
Jake
ha
becom
a
pathet
figur
a
broke
,
overweight
loser
who
ha
spent
time
in
jail
for
corrupt
the
moral
of
a
minor
,
ha
lost
hi
wife
and
children
,
and
is
try
to
earn
a
few
buck
by
do
a
cheap
standup
routin
.

RAGING
BULL
is
the
other
side
of
ROCKY
.

Sylvest
Stallon
's
tale
of
box
triumph
wa
releas
to
great
critic
acclaim
in
1976
.

It
won
the
1977
Best
Pictur
Oscar
,
and
a
sequel
arriv
in
theater
befor
RAGING
BULL
went
into
product
.

But
,
where
ROCKY
romantic
box
,
RAGING
BULL
take
a
cold
,
unflinch
look
at
the
violenc
both
insid
and
outsid
of
the
ring
.

While
Scorses
's
primari
aim
is
to
present
a
rivet
deconstruct
of
La
Motta
the
man
,
he
never
back
awai
from
show
the
seedi
,
ugli
undersid
of
a
sport
where
gambl
,
greed
,
and
organ
crime
forc
fighter
to
throw
match
to
continu
work
.

ROCKY
show
box
at
it
noblest
;
RAGING
BULL
show
it
at
it
most
diseas
.

There
's
littl
I
can
sai
here
that
ha
n't
alreadi
said
about
De
Niro
's
perform
.

In
a
career
that
ha
includ
mani
fine
role
,
thi
is
hi
most
outstand
.

The
level
of
intens
De
Niro
bring
to
La
Motta
is
unwav
.

And
,
although
there
's
a
lot
of
Travi
Bickl
-LRB-
De
Niro
's
charact
in
TAXI
DRIVER
-RRB-
in
La
Motta
,
Scorses
and
De
Niro
go
to
great
length
to
make
sure
that
,
if
we
never
fulli
sympath
with
him
,
at
least
we
understand
the
forc
that
drive
him
.

Thi
is
a
complet
character
and
a
perfect
exampl
of
act
.

As
the
younger
Jake
,
De
Niro
is
trim
and
fit
.

As
the
fat
,
older
man
,
De
Niro
gain
pound
and
turn
hi
bodi
into
a
grotesqu
parodi
of
it
normal
form
.

How
mani
actor
would
go
that
far
?

The
Best
Oscar
statu
is
onli
on
testimoni
to
the
last
power
of
the
perform
.

Cathi
Moriarti
and
Joe
Pesci
both
earn
nomin
for
their
support
work
-LRB-
although
neither
won
-RRB-
.

Their
portray
have
the
same
raw
energi
that
infus
De
Niro
's
.

Pesci
,
who
ha
basic
re-creat
the
same
person
in
two
other
Scorses
film
-LRB-
GOODFELLAS
and
CASINO
-RRB-
,
present
Joei
as
a
slightli
more
intellig
version
of
hi
brother
.

The
misogyni
and
violenc
ar
still
there
,
onli
better
conceal
.

But
,
when
Joei
snap
,
as
he
doe
in
on
scene
where
he
pound
on
a
mobster
-LRB-
Frank
Vincent
-RRB-
,
it
's
startl
to
behold
.

Moriarti
,
on
the
other
hand
,
must
plai
the
temptress
with
virgin
qualiti
the
onli
kind
of
woman
who
would
ensnar
Jake
.

Later
,
she
's
call
upon
to
portrai
the
batter
wife
and
mother
whose
love
ha
turn
into
fear
.

Scorses
and
cinematograph
Michael
Chapman
elect
to
shoot
the
bulk
of
RAGING
BULL
in
black-and-whit
-LRB-
a
few
``
home
movi
''
segment
ar
in
color
-RRB-
.

The
choic
is
perfect
for
the
movi
,
give
RAGING
BULL
a
uniqu
look
in
an
era
when
a
black-and-whit
approach
is
almost
unheard
of
.

-LRB-
One
import
recent
except
,
SCHINDLER
'S
LIST
,
eschew
color
for
some
of
the
same
reason
that
RAGING
BULL
did
.
-RRB-

In
black-and-whit
-LRB-
especi
consid
Scorses
's
choic
of
shot
and
us
of
slow
motion
-RRB-
,
the
violenc
is
stark
and
disturb
.

There
's
no
room
for
romantic
in
the
ring
with
inki
black
blood
stain
the
canva
.

Dure
fight
sequenc
,
the
director
also
us
a
number
of
point-of-view
shot
design
to
show
the
world
,
howev
briefli
,
from
La
Motta
's
perspect
.

Scorses
could
have
chosen
to
end
the
film
with
La
Motta
's
loss
to
Sugar
Rai
,
where
he
surrend
hi
Middleweight
crown
.

After
that
,
hi
career
wa
all
downhil
.

Yet
the
director
allow
the
film
to
run
for
thirti
minut
after
thi
pivot
moment
,
stretch
more
than
a
decad
into
La
Motta
's
futur
.

The
reason
is
obviou
:
Scorses
is
n't
as
interest
in
box
as
he
is
in
the
charact
.

Follow
hi
retir
,
La
Motta
is
still
as
violent
and
volatil
as
ever
,
but
he
ha
lost
the
arena
in
which
he
can
legal
unleash
those
tendenc
.

We
see
the
result
of
thi
beast
let
loos
on
societi
and
how
the
consequ
of
hi
action
reduc
him
to
a
parodi
of
hi
former
self
.

Who
can
not
feel
a
surg
of
piti
for
La
Motta
as
he
quietli
recit
Marlon
Brando
's
speech
from
ON
THE
WATERFRONT
?

-LRB-
``
I
coulda
been
a
contenda
.
''
-RRB-

Perhap
RAGING
BULL
is
inde
the
best
film
of
the
'80
.

And
,
if
not
,
it
's
certainli
peril
close
to
the
zenith
.

Now
with
more
than
1400
review
...
The
ReelView
web
site
:
-LSB-
1
-RSB-
http://movie-reviews.colossus.net
/
``
My
belief
is
that
no
movi
,
noth
in
life
,
leav
peopl
neutral
.
''

You
either
leav
them
up
or
you
leav
them
down
.
''
''

