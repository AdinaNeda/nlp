In
present
dai
Hanoi
,
three
sister
reflect
on
their
parent
'
relationship
while
try
to
defin
their
own
in
writer/director
Tran
Anh
Hung
's
-LRB-
``
The
Scent
of
Green
Papaya
''
-RRB-
``
The
Vertic
Rai
of
the
Sun
.
''

Youngest
sister
Lien
-LRB-
Tran
Nu
Yen-Kh
-RRB-
greet
the
dai
with
Hai
-LRB-
Ngo
Quanq
Hai
-RRB-
,
the
onli
brother
whom
she
room
and
relentlessli
flirt
with
.

Their
morn
ritual
of
Tai
Chi
and
Lou
Reed
preced
cross
the
street
to
eldest
sister
Suong
's
-LRB-
Nguyen
Nhu
Quynh
-RRB-
cafe
,
where
Lien
work
,
for
breakfast
.

The
three
sister
lovingli
prepar
their
mother
's
memori
banquet
of
tradit
delicaci
,
prepar
as
much
for
look
as
tast
,
while
giggl
over
the
differ
between
the
sex
.

After
the
famili
break
from
the
banquet
,
we
learn
that
Suong
's
young
son
`
littl
monkei
'
is
dote
upon
,
but
there
's
a
distanc
develop
with
her
husband
Quoc
-LRB-
Chu
Ngoc
Hung
-RRB-
,
who
's
leav
for
on
of
hi
habitu
botan
photographi
trip
.

Middl
sister
Khanh
-LRB-
Tran
Manh
Cuong
-RRB-
tell
her
writer
husband
she
's
pregnant
befor
he
leav
on
a
trip
to
Saigon
to
research
Tuan
,
a
mysteri
man
in
hi
mother-in-law
's
past
.

Lien
keep
crawl
into
Hai
's
bed
at
night
,
caus
him
to
fall
out
of
it
.

The
initi
seren
we
wit
start
turn
into
a
soap
opera
befor
come
to
full
circl
on
month
later
,
as
the
three
women
prepar
their
father
's
memori
banquet
.

``
The
Vertic
Rai
of
the
Sun
''
is
a
contempl
piec
where
strong
passion
exist
underneath
calm
exterior
.

Sly
humor
,
self
deceit
and
harsh
truth
all
exist
in
the
cyclic
screenplai
.

Tran
Anh
Hung
and
hi
cinematograph
Mark
Lee
Ping-Bin
-LRB-
``
Flower
of
Shanghai
''
-RRB-
linger
over
and
repeat
the
small
gestur
of
everydai
life-a
wife
wash
her
husband
hand
,
a
woman
make
water
danc
in
a
bowl
,
a
man
pull
hi
lover
to
face
him
befor
thei
part
.

Small
space
such
as
room
,
allei
and
courtyard
creat
intimaci
.

The
color
green
,
symbol
life
and
tranquil
,
is
us
heavili
in
the
film
's
visual
style
,
laps
into
yellowish
hue
and
blue
.

Writer/director
Tran
Anh
Hung
's
film
recal
Ang
Lee
's
``
Eat
Drink
Man
Woman
,
''
anoth
tale
of
three
sister
of
vari
modern
come
togeth
tradition
for
a
parent
.

But
while
Lee
's
more
tradition
story-driven
film
result
in
radic
chang
for
all
concern
,
``
The
Vertic
Rai
of
the
Sun
's
''
charact
strive
for
peac
harmoni
.

Watch
it
is
like
trail
your
fingertip
in
a
stream
on
a
cool
spring
dai
.

