As
my
friend
and
fellow
film
critic
Ted
Prigg
said
in
hi
review
of
`
The
Phantom
Menac
'
-
how
do
I
even
write
thi
review
?

Never
mind
the
fact
that
I
've
been
wait
for
a
new
`
Star
War
'
movi
sinc
I
wa
seven
year
old
.

Never
mind
the
fact
everyon
on
the
Internet
had
alreadi
dissect
and
discuss
the
movi
befor
it
wa
even
releas
.

Never
mind
that
there
's
so
much
go
on
in
thi
movi
in
term
of
plot
,
action
,
special
effect
and
long-term
stori
that
to
analyz
everi
aspect
would
take
a
lot
of
time
and
energi
and
still
would
n't
cover
everyth
.

And
also
like
Ted
doe
in
all
of
hi
review
,
I
'll
try
to
review
thi
in
a
wai
differ
from
my
usual
review
and
instead
write
as
casual
as
possibl
as
if
I
wa
talk
directli
to
you
,
the
reader
.

Befor
we
can
begin
analyz
the
specif
aspect
of
the
film
,
mani
of
the
most
gener
and
most
signific
factor
must
be
consid
-LRB-
to
get
the
specif
of
the
film
,
skip
thi
and
the
next
four
paragraph
-RRB-
.

As
ha
been
made
obviou
for
the
last
sever
year
,
anoth
trilogi
of
film
will
be
produc
to
account
for
the
-LSB-
miss
-RSB-
initi
three
instal
of
the
`
Star
War
'
seri
-LRB-
the
first
of
the
film
wa
actual
episod
four
,
not
on
-RRB-
.

Georg
Luca
,
the
creator
of
the
seri
,
ha
probabl
just
assum
most
peopl
know
the
first-produc
trilogi
-LRB-
`
Star
War
,
'
`
The
Empire
Strike
Back
'
and
`
Return
Of
The
Jedi
'
-RRB-
is
actual
the
second
trilogi
,
chronolog
.

In
media
interview
in
the
past
he
ha
explain
why
the
movi
were
made
out
of
order
,
but
I
ca
n't
recal
hi
reason
.

With
thi
new
film
he
as
writer
and
director
ha
also
assum
viewer
ar
at
least
vagu
familiar
with
the
storylin
and
signific
plot
point
of
the
first
trilogi
-LRB-
err
...
second
trilogi
...
well
you
know
what
I
mean
-RRB-
.

And
in
order
for
thi
review
to
be
thorough
mani
of
those
signific
point
must
be
mention
which
,
unfortun
,
mai
serv
as
spoiler
so
proce
with
caution
.

-LSB-
Critic
's
note
#
1
:
It
is
on
my
person
polici
not
to
intention
analyz
spoiler
becaus
I
would
like
my
review
to
be
abl
to
make
sens
to
the
reader
both
befor
and
after
view
the
film
review
,
but
it
would
be
nearli
imposs
to
do
the
film
justic
here
without
break
the
rule
.

Critic
's
note
#
2
:
Review
thi
film
also
break
the
univers
critic
'
law
of
review
movi
,
which
as
Roger
Ebert
coin
,
`
It
's
not
what
a
movi
is
about
,
but
how
it
is
about
it
.
'

Becaus
so
mani
outsid
factor
come
into
plai
in
the
film
's
plot
and
signific
as
part
of
a
trilogi
,
and
just
consid
modern
societi
,
it
again
would
be
difficult
,
if
not
imposs
to
simpli
review
the
film
as
a
complet
autonom
stori
.
-RSB-

First
of
all
we
have
to
catalog
what
we
know
about
the
film
befor
even
go
into
it
.

We
know
that
young
Anakin
Skywalk
will
grow
up
to
becom
the
evil
ruler
Darth
Vader
who
is
also
the
father
of
Luke
Skywalk
,
who
will
lead
a
success
revolut
against
him
and
hi
Empire
.

We
know
Obi-Wan
`
Ben
'
Kenobi
wa
the
trainer
of
Anakin
in
the
wai
of
`
The
Forc
'
in
attempt
to
becom
a
Jedi
Knight
,
but
would
fail
and
thu
Anakin
would
fall
to
the
`
Dark
Side
.
'

We
also
know
someth
about
`
The
Forc
'
itself
,
that
it
is
some
kind
of
univers
forc
which
determin
fate
and
can
be
manipul
by
those
whose
mind
ar
expand
enough
to
believ
in
it
and
feel
it
-LRB-
the
Jedi
-RRB-
.

What
we
do
n't
know
for
sure
is
the
histori
of
the
rule
of
the
galaxi
,
which
is
most
like
much
longer
and
more
complex
than
ani
govern
we
have
had
here
on
Earth
.

To
me
,
thi
wa
on
of
the
most
fascin
aspect
of
the
first
three
film
becaus
there
seem
to
be
a
subtl
,
definit
order
to
all
the
complex
.

Also
,
it
seem
that
we
're
suppos
to
be
familiar
with
a
few
other
detail
about
the
stori
which
have
come
out
of
the
thousand
of
comic
book
and
paperback
publish
in
the
year
sinc
the
movi
end
.

Most
notabl
,
detail
regard
the
Emperor
from
`
Empire
'
and
`
Jedi
,
'
and
that
he
wa
onc
a
senat
and
hi
last
name
is
Palpatin
.

For
those
who
have
n't
kept
up
with
all
the
`
Star
War
'
paraphernalia
over
the
year
,
there
is
still
a
wai
to
know
these
detail
simpli
by
recogn
an
actor
-LRB-
or
hi
voic
at
least
-RRB-
.

With
`
The
Phantom
Menac
'
be
the
first
chapter
of
a
long
saga
,
on
would
assum
the
basic
groundwork
would
be
laid
,
includ
the
histori
behind
all
the
major
factor
of
the
seri
,
especi
`
The
Forc
'
and
the
Jedi
Knight
.

Unfortun
,
thi
film
doe
more
of
the
opposit
as
it
simpli
build
on
top
of
what
must
be
a
histori
so
long
and
detail
it
could
never
be
explain
.

I
did
n't
like
that
aspect
to
thi
film
and
is
on
of
the
reason
it
fall
just
short
of
great
.

If
you
're
go
to
tell
a
stori
from
the
beginning-tel
it
from
the
begin
-LRB-
is
it
possibl
that
after
thi
trilogi
is
complet
there
could
be
anoth
trilogi
of
episodes-1
through-3
to
clarifi
the
back
stori
even
further
?
-RRB-
.

But
I
digress
.

I
've
said
so
much
and
yet
I
have
n't
even
touch
on
the
specif
of
the
film
itself
.

All
these
necessari
`
disclaim
'
element
should
go
to
show
just
how
vast
the
stori
of
the
`
Star
War
'
seri
is
and
thi
individu
film
itself
.

What
we
get
here
is
a
movi
not
unlik
ani
of
the
other
three
we
've
come
to
know
and
love
sinc
the
late
1970
.

It
similar
to
the
other
film
help
to
keep
the
spirit
of
the
seri
familiar
,
but
at
the
same
time
seem
to
be
a
flaw
in
the
filmmak
process
itself
sinc
it
sometim
ring
of
unorigin
.

The
basic
stori
is
rather
sort
,
complex
and
even
confus
at
time
.

We
're
told
that
an
armi
known
as
`
The
Trade
Feder
'
ha
set
up
a
blockad
around
an
seemingli
insignific
planet
call
Naboo
.

We
're
not
given
too
mani
detail
to
clarifi
the
back
stori
of
what
the
feder
is
and
what
their
purpos
is
,
instead
the
film
opt
to
get
it
stori
move
quickli
.

We
're
introduc
to
two
Jedi
,
the
master
Qui-Gon
Jinn
-LRB-
Neeson
-RRB-
and
hi
apprentic
Obi-Wan
Kenobi
-LRB-
McGregor
-RRB-
.

The
two
ar
act
as
ambassador
of
some
sort
in
hope
of
end
the
obviou
hostil
between
the
feder
and
the
planet
's
queen
,
Amidala
-LRB-
Portman
-RRB-
.

The
leader
of
the
feder
,
an
alien
call
Viceroi
,
is
follow
the
order
of
a
strang
,
mythic
character-an
older
human
man
whose
face
and
bodi
ar
cover
and
shadow
by
the
black
robe
and
hood
he
wear
.

He
is
refer
to
as
Lord
Sidiou
but
consid
hi
appear
,
and
especi
that
creepi
evil
voic
of
hi
,
he
is
a
obvious
on
of
the
most
signific
charact
in
the
seri
.

Sidiou
commun
to
Viceroi
onli
through
digit
transmiss
and
never
in
person
.

Clearli
hi
actual
whereabout
ar
someth
he
want
to
keep
secret
becaus
that
would
also
reveal
hi
ident
-LRB-
which
is
never
openli
reveal
in
the
film
,
but
should
be
clear
to
most
viewer
.

I
have
a
feel
we
'll
delv
into
hi
background
in
the
next
two
film
-RRB-
.

But
it
doe
n't
take
the
film
long
to
spring
into
action
and
within
minut
our
hero
Jedi
ar
fight
for
their
live
and
the
film
's
stori
begin
to
move
along
.

The
film
doe
n't
have
on
specif
plot
and
work
in
the
same
manner
as
the
other
film
in
the
seri
where
as
what
we
get
is
on
mini-plot
and
adventur
on
top
of
anoth
.

There
ar
so
mani
of
these
mini-plots-within-plot
in
thi
film
I
would
not
describ
them
all
becaus
it
is
what
give
the
movi
it
hook
.

The
other
film
have
reli
on
thi
type
of
storytel
,
but
becaus
of
the
limit
of
the
technolog
at
the
time
,
the
film
'
screenplai
were
also
limit
.

The
sens
of
limitless
here
is
what
give
`
The
Phantom
Menac
'
it
uniqu
trait
.

As
we
alreadi
know
,
the
film
take
place
at
least
a
gener
or
two
befor
the
origin
`
Star
War
'
trilogi
which
mean
a
lot
of
the
background
which
wa
unclear
and/or
unexplain
mai
be
clear
up
here
as
it
is
the
begin
of
the
vast
epic
stori
.

And
here
the
most
signific
storylin
is
that
involv
the
discoveri
of
10-year-old
Anakin
Skywalk
-LRB-
Lloyd
-RRB-
,
by
Qui-Gon
Jinn
and
hi
introduct
to
the
Jedi
Council
.

To
briefli
summar
the
stori
,
Jinn
stumbl
upon
Skywalk
in
an
attempt
to
bui
part
for
Queen
Amidala
's
spaceship
which
wa
damag
in
effort
to
free
her
from
the
feder
.

Their
meet
on
Tatooin
is
mostli
chanc
but
of
cours
Jinn
chalk
it
up
to
the
will
of
the
Forc
.

Jinn
can
feel
that
Skywalk
ha
a
great
natur
power
in
the
Forc
,
and
through
anoth
sub-plot
involv
a
bet
on
Skywalk
in
a
fantast
race
he
will
enter
,
more
and
more
detail
of
hi
signific
begin
to
reveal
themselv
.

Thi
also
provid
for
a
new
elabor
and
explan
of
the
Forc
which
ha
never
been
mention
befor
.

In
fact
,
it
seem
to
be
someth
so
import
it
is
upset
it
ha
never
been
mention
befor
.

The
element
serious
undermin
the
mystic
aspect
of
the
Forc
and
transform
it
into
someth
more
physic
and
scientif
which
is
complet
unnecessari
.

By
the
film
's
third
act
all
the
major
signific
element
becom
ti
togeth
in
a
rather
familiar
execut
of
the
plot
.

It
's
funni
how
the
realiti
of
the
`
Star
War
'
world
seem
so
embed
in
mytholog
,
New
Age-lik
philosophi
and
religion
and
yet
the
solut
to
all
the
major
conflict
in
the
seri
ha
been
violenc
.

Not
that
thi
techniqu
ha
n't
been
us
in
movi
and
in
real-lif
situat
countless
time
throughout
histori
.

And
at
least
it
's
all
in
the
spirit
of
the
ultim
power
of
good
versu
evil
in
a
fun
and
excit
matter
.

It
doe
border
of
the
cartoonish
sometim
,
but
that
's
just
the
film
appeal
to
our
inner
child
,
which
is
the
basic
reason
the
entir
saga
exist
and
ha
becom
such
a
big
part
of
our
modern
societi
.

`
The
Phantom
Menac
'
deliv
exactli
what
you
'd
expect
from
a
`
Star
War
'
film
which
is
why
it
succe
as
well
as
it
doe
but
is
alwai
why
it
never
truli
achiev
great
.

So
mani
thing
in
the
film
we
've
seen
befor
and
done
with
more
heart
becaus
the
filmmak
concentr
just
a
bit
more
on
stori
and
charact
becaus
thei
were
limit
.

But
thi
film
is
far
from
bad
becaus
it
at
least
embrac
the
spirit
of
sheer
fun
at
the
movi
.

