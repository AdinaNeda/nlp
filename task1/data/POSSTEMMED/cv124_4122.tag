There
's
more
to
a
quilt
than
fabric
and
thread
--
each
patchwork
design
ha
it
own
uniqu
stori
-LRB-
or
stori
-RRB-
to
tell
.

In
HOW
TO
MAKE
AN
AMERICAN
QUILT
,
the
first
Hollywood
releas
from
Australian
director
Jocelyn
Moorhous
-LRB-
PROOF
-RRB-
,
we
come
to
understand
how
the
tale
of
the
seven
maker
imbu
their
creation
with
passion
and
vital
.

All
the
sorrow
,
joi
,
long
,
suffer
,
and
love
of
these
women
ar
sewn
into
their
quilt
and
,
as
their
bittersweet
memori
ar
given
express
,
on
young
woman
appli
these
lesson
of
the
past
to
her
own
uncertain
futur
.

Motion
pictur
ar
fill
with
male
bond
ritual
;
HOW
TO
MAKE
AN
AMERICAN
QUILT
present
a
distinctli
feminin
altern
.

The
``
quilt
bee
''
consist
of
seven
member
:
sister
Gladi
-LRB-
Ann
Bancroft
-RRB-
and
Hy
-LRB-
Ellen
Burstyn
-RRB-
;
Sophia
-LRB-
Loi
Smith
-RRB-
,
a
woman
known
for
frighten
children
;
Emma
-LRB-
Jean
Simmon
-RRB-
,
the
timid
wife
of
a
perpetu
unfaith
man
;
Constanc
-LRB-
Kate
Nelligan
-RRB-
,
who
ha
been
have
an
affair
with
Emma
's
husband
;
Anna
-LRB-
Maya
Angelou
-RRB-
,
the
leader
of
the
group
;
and
Marianna
-LRB-
Alfre
Woodard
-RRB-
,
Anna
's
daughter
.

The
project
thei
're
busi
with
is
the
wed
quilt
for
Hy
's
granddaught
,
Finn
-LRB-
Winona
Ryder
-RRB-
,
who
ha
just
becom
engag
.

She
's
spend
the
summer
with
Hy
and
Gladi
,
and
awai
from
her
fianc
,
to
decid
whether
a
lifelong
commit
is
realli
what
she
want
,
and
whether
it
's
better
to
marri
a
friend
or
a
lover
.

One
of
the
greatest
pleasur
of
HOW
TO
MAKE
AN
AMERICAN
QUILT
come
from
watch
an
arrai
of
fine
perform
by
an
impress
ensembl
cast
.

From
Winona
Ryder
,
Ann
Bancroft
,
Ellen
Burstyn
,
and
Maya
Angelou
to
minor
player
like
Rip
Torn
,
Clair
Dane
,
and
Kate
Capshaw
,
thi
film
is
overflow
with
realiz
talent
.

The
perform
ar
good
enough
,
in
fact
,
to
cover
up
mani
of
the
script
's
weak
.

HOW
TO
MAKE
AN
AMERICAN
QUILT
is
basic
an
unremark
,
if
affect
,
tale
of
love
across
the
gener
--
a
sort
of
truncat
American
version
of
THE
JOY
LUCK
CLUB
,
with
a
dash
of
FRIED
GREEN
TOMATOES
ad
.

There
ar
probabl
too
mani
charact
.

So
,
instead
of
realli
get
to
know
a
few
of
them
,
we
ar
present
with
quick
glimps
into
a
singl
defin
event
in
each
of
their
live
.

We
learn
about
the
root
of
the
smolder
resent
between
Gladi
and
Hy
,
and
ar
told
the
reason
why
Emma
stai
with
her
husband
and
Sophia
is
so
irasc
.

There
ar
other
episod
as
well
:
the
love
of
Anna
's
life
,
Marianna
's
soul
mate
,
and
a
look
at
the
reason
why
Constanc
enter
into
an
affair
with
her
friend
's
husband
.

The
sum
total
of
these
tale
is
meant
to
provid
the
framework
for
Finn
's
stori
:
whether
to
go
forward
with
her
marriag
or
dalli
with
a
hunki
stranger
.

There
is
no
emot
epiphani
in
HOW
TO
MAKE
AN
AMERICAN
QUILT
.

The
stori
ar
all
well-told
,
but
it
's
difficult
to
realli
connect
with
the
charact
--
their
moment
pass
so
quickli
.

While
we
certainli
feel
someth
for
each
of
the
eight
princip
,
our
emot
invest
is
tenuou
.

There
is
n't
enough
depth
to
pull
the
viewer
in
all
the
wai
,
and
mani
of
the
person
and
relationship
feel
half-form
,
like
a
quilt
with
patch
miss
.

To
put
it
bluntli
,
while
I
enjoi
watch
thi
film
,
it
did
n't
``
do
much
''
for
me
.

On
the
whole
,
HOW
TO
MAKE
AN
AMERICAN
QUILT
is
a
nicely-underst
drama
that
ha
a
lot
to
sai
about
love
,
passion
,
and
monogami
in
relationship
.

Finn
's
segment
is
by
far
the
most
compel
,
becaus
she
's
the
focal
point
:
her
action
ar
shape
by
everyon
els
's
experi
.

The
other
patch
of
thi
American
Quilt
ar
success
onli
to
vari
degre
.

If
there
's
a
disappoint
here
,
it
's
that
the
script
doe
n't
have
more
reson
,
but
the
privileg
of
see
such
a
fine
cast
in
top
form
allow
a
viewer
to
enjoi
thi
pictur
even
if
the
stori
is
somewhat
conventional.-Jam
Berardinelli
-LRB-
-LSB-
1
-RSB-
jberardinell@delphi.com
-RRB-

