After
bloodi
clash
and
independ
won
,
Lumumba
refus
to
pander
to
the
Belgian
,
who
continu
a
condescend
and
paternalist
relationship
with
the
Congo
.

Their
offic
,
particularli
Gener
Janssen
-LRB-
Rudi
Delhem
-RRB-
in
the
Forc
Publiqu
,
the
Congo
's
Army
,
caus
rebellion
,
undermin
Lumumba
,
who
wa
outrag
at
the
rape
and
murder
of
Belgian
nation
.

With
unrest
build
,
Mois
Tshomb
-LRB-
Pascal
Nzonzi
-RRB-
and
the
provinc
of
Katanga
,
which
containedperc
of
the
countri
's
resourc
,
proclaim
secess
.

Lumumba
replac
Janssen
,
make
Mobutu
a
colonel
,
and
went
on
a
pacif
tour
with
Congoles
Presid
Joseph
Kasa
Vubu
-LRB-
Maka
Kotto
-RRB-
,
but
it
wa
too
late
.

`
When
you
want
to
drown
a
dog
,
you
sai
it
ha
rabi
,
'
prophesi
Lumumba
of
hi
own
fate
.

Peck
and
Bonitz
do
an
exemplari
job
tell
a
complic
tale
with
a
myriad
of
player
,
although
thei
frequent
succumb
to
clich
,
particularli
regard
Lumumba
's
privat
life
.

Peck
's
script
illumin
Bantu
sai
like
`
The
hand
that
give
,
rule
'
when
Lumumba
us
it
with
the
American
ambassador
.

Peck
's
direct
is
less
assur
,
with
mani
scene
unfortun
plai
like
standard
televis
fare
.

He
's
serv
well
,
though
,
by
hi
cast
of
Ebouanei
in
the
titl
role
.

Ebouanei
is
dynam
,
radiat
hi
charact
's
fierc
passion
for
hi
peopl
and
hi
countri
.

Lumumba
's
intellig
and
abil
to
strateg
,
even
as
he
's
corner
by
insurmount
odd
,
ar
given
life
by
Ebouanei
.

Peck
's
subject
and
lead
actor
elev
hi
film
abov
it
mediocr
product
.

``
Lumumba
''
is
a
stori
that
deserv
to
be
told
and
Ebouanei
's
perform
make
the
tragedi
person
felt
.

