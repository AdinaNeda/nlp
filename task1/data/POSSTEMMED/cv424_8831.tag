I
can
sum
up
First
Strike
in
on
word
:
awesom
.

Never
in
my
life
have
I
seen
an
actor
as
insan
dedic
as
Jacki
Chan
.

For
the
simpl
reason
that
he
ha
perform
hi
own
stunt
in
hi
everi
movi
he
's
ever
appear
in
,
he
should
be
award
greatli
.

The
same
Tom
and
Jerri
formula
that
wa
us
to
make
Chan
's
Rumbl
in
the
Bronx
and
Supercop
so
entertain
can
be
found
here
as
well
.

Howev
,
First
Strike
wa
probabl
the
most
excit
out
of
the
three
,
for
the
simpl
reason
that
practic
make
perfect
.

Hi
last
two
movi
were
almost
like
practic
and
thi
on
wa
great
.

Like
Rumbl
,
thi
film
is
about
the
Mafia
,
the
former
KGB
.

Now
Chan
must
do
battl
in
the
freez
arctic
temperatur
of
the
Ukrain
against
a
group
of
veri
larg
bad
men
.

The
trailer
for
the
film
sum
it
up
quit
well
.

First
Strike
is
action-pack
,
but
the
fight
scene
ar
so
fast
and
furiou
,
thei
almost
have
a
witti
,
Italian-lik
humor
to
them
.

Jacki
Chan
is
a
regular
comedian
.

Well
,
he
's
no
Eddie
Murphi
,
but
he
can
realli
crack
you
up
.

For
exampl
,
he
is
hang
off
a
roof
and
he
humor
taunt
hi
foe
to
come
and
get
him
,
as
he
claim
to
be
the
Korean
007
-LRB-
that
's
secret
agent
Jame
Bond
-RRB-
.

Even
the
plot
to
thi
film
improv
abov
the
other
,
as
a
sort-of
twist
nuclear-weapon
smuggl
deal
is
botch
and
Jacki
take
the
blame
.

But
never
fear
,
as
the
Chines
Mafia
aid
him
in
uncov
the
truth
behind
the
incid
.

With
some
of
the
most
high-fli
stunt
and
side-split
comedi
,
First
Strike
is
a
sure-fir
hit
that
's
guarante
to
draw
some
major
,
world-wid
attent
.

