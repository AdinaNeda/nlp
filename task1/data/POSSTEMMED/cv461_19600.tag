Vampir
film
,
as
well
as
other
horror
film
,
ar
usual
dumb
and
predict
B-movi
,
meant
to
scare
us
by
clich
and
simpl
shock
.

It
is
therefor
interest
to
watch
Neil
Jordan
recent
film
that
is
not
onli
visual
stun
,
but
ha
also
a
plot
worth
make
movi
about
.

Base
on
Anne
Rice
's
novel
,
``
Interview
with
a
Vampir
''
is
a
long
,
dark
trip
to
hell
.

The
film
open
with
gothic
quir
and
dark
street
of
present-dai
San
Francisco
.

The
camera
slowli
find
particular
window
.

A
shape
of
a
young
man
is
visibl
in
the
dark
.

``
So
you
want
me
to
tell
you
the
stori
of
my
life
?
''
.

The
mysteri
man
is
Loui
-LRB-
Brad
Pitt
-RRB-
,
a
two-centuri
old
vampir
,
tell
hi
stori
to
a
fascin
interview
-LRB-
Christian
Slater
-RRB-
.

Hi
tale
open
in
1791
Louisiana
,
just
south
of
New
Orlean
,
where
Loui
fall
victim
to
the
vampir
Lestat
-LRB-
Tom
Cruis
-RRB-
.

Given
a
choic
between
death
and
etern
life
as
on
of
the
undead
,
Loui
choos
the
latter
,
a
decis
he
will
forev
regret
.

Everlast
life
and
etern
youth
,
promis
to
him
by
Lestat
,
turn
instead
into
never
end
suffer
,
damn
to
etern
hunger
for
blood
and
long
for
peac
.

Loui
can
not
kill
with
the
impun
of
Lestat
,
but
,
to
sate
hi
hunger
,
he
must
feed
,
and
the
blood
of
anim
is
not
enough
.

Eventual
,
he
pierc
the
neck
of
a
grief-stricken
young
girl
name
Claudia
-LRB-
Kirsten
Dunst
-RRB-
,
whom
Lestat
then
curs
with
hi
unholi
form
of
resurrect
so
that
she
can
be
a
surrog
daughter
to
both
himself
and
Loui
.

For
a
while
,
thei
ar
on
``
big
,
happi
famili
.
''

But
all
thing
end
,
and
Claudia
's
grow
resent
of
Lestat
fuel
a
bloodi
confront
.

When
Lui
and
Claudia
break
loos
from
Lestat
,
thei
travel
to
Pari
,
where
the
Euro-vamp
Santiago
-LRB-
Stephen
Rea
-RRB-
and
Armand
-LRB-
Antonio
Bandera
-RRB-
,
who
introduc
them
to
a
bigger
world
of
the
damn
.

Director
Neil
Jordan
-LRB-
``
The
Cry
Game
''
-RRB-
togeth
with
the
talent
director
of
cinematographi
Philipp
Rousselot
and
compos
Elliot
Goldenth
ha
creat
an
incred
atmospher
.

As
the
film
begin
,
there
is
an
incred
palett
of
color
,
beauti
sunris
,
lush
golden
field
,
green
forest
,
inky-blu
cloud
and
blend
sun
.

When
Lui
is
``
born
to
dark
''
,
everyth
suddenli
chang
to
dark
velvet
,
lit
onli
by
the
silver
moonlight
.

The
beauti
product
design
by
Dant
Ferretti
,
wonder
costum
and
art
direct
by
Malcolm
Middleton
re-creat
the
multipl
histor
period
in
the
film
.

From
the
renaiss
New
Orlean
and
the
beauti
rococo
Pari
of
the
18th
centuri
,
to
our
present
dai
.

The
cast
is
likewis
good
,
involv
some
of
the
most
famou
and
beauti
star
of
Hollywood
,
Ireland
and
Spain
.

The
controversi
cast
of
Tom
Cruis
as
Lestat
is
incred
effect
.

Cruis
is
energet
,
sinist
,
charismat
,
wild
and
bloodthirsti
.

Cruis
's
Lestat
like
to
seduc
young
women
befor
exact
hi
dark
red
susten
.

With
alarm
swift
,
the
victim
switch
from
sexual
excit
to
outright
horror
,
as
hi
murder
purpos
becom
clear
.
''

...
''
Kill
them
mercifulli
,
but
do
it
.

You
ar
what
you
ar
.
.

for
do
not
doubt
,
you
ar
a
killer
!
''
''

That
is
how
Lestat
is
teach
Lui
.

But
behind
that
furiou
facad
is
anger
and
loneli
that
he
carri
through
the
centuri
and
tri
to
smother
with
nightli
rampag
.

Brad
Pitt
is
equal
convinc
as
the
``
vampir
with
a
human
soul
''
.

Antonio
Bandera
and
Stephen
Rea
ar
effect
,
but
sinc
thei
plai
more
or
less
secondari
charact
,
their
perform
ar
almost
invis
.

The
greatest
perform
in
the
film
come
suprisingli
from
the
young
Kristen
Dunst
,
who
manag
to
creat
an
incred
emot
and
believ
charact
.

The
world
is
chang
around
the
littl
child
,
but
she
doe
not
.

She
remain
unchangeable-a
child
for
all
etern
.

Only
her
wise
,
dark
ey
reveal
her
ag
.

Ironic
the
film
's
onli
miss
is
the
script
.

Anne
Rice
's
novel
is
a
veri
interest
read
,
and
her
script
is
rich
,
color
and
emot
,
but
it
is
also
tier
and
too
melanchol
,
at
time
resembl
a
soap
opera
:
Claudia
:
''
...
''
Is
that
what
I
should
do
?

Let
you
go
.
.

my
father
,
my
Lui
,
who
made
me
...
Who
will
look
after
me
,
my
dark
angel
,
when
you
ar
gone
?
''
''

Lui
:
``
Everyth
will
be
alright
.
.
''

Claudia
:
``
Do
you
realli
believ
that
?
''

And
then
thei
hug
emotion
.

Moment
like
those
displai
abov
and
Lui
'
self-piti
is
a
bit
tiresom
.

Howev
Rice
's
script
is
otherwis
strong
and
well
structur
,
bring
up
humor
and
comic
episod
,
that
were
more
or
less
hidden
in
the
book
.

When
Lestat
find
Claudia
's
dead
dressmak
,
whom
she
ha
kill
,
he
cri
out
:
``
Who
will
make
you
that
dress
now
?
''

Be
a
littl
practic
...
.

Never
in
the
hous
!
''
''

Moment
like
that
ar
both
entertain
and
appeal
.

Neil
Jordan
's
direct
is
beauti
and
sensual
as
he
plai
with
interest
issu
like
etern
,
homosexu
,
love
and
loneli
.

Hi
gothic
saga
is
not
meant
to
scare
,
but
to
displai
these
issu
differ
.

Wrap
up
in
mysteri
,
hi
new
,
origin
pictur
bring
vampire-film
to
a
new
height
.

