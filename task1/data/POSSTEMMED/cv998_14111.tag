Steven
Spielberg
's
second
epic
film
on
World
War
II
is
an
unquest
masterpiec
of
film
.

Spielberg
,
ever
the
student
on
film
,
ha
manag
to
resurrect
the
war
genr
by
produc
on
of
it
grittiest
,
and
most
power
entri
.

He
also
manag
to
cast
thi
era
's
greatest
answer
to
Jimmi
Stewart
,
Tom
Hank
,
who
deliv
a
perform
that
is
noth
short
of
an
astonish
miracl
.

For
about
160
out
of
it
170
minut
,
``
Save
Privat
Ryan
''
is
flawless
.

Liter
.

The
plot
is
simpl
enough
.

After
the
epic
D-Dai
invas
-LRB-
whose
sequenc
ar
noth
short
of
spectacular
-RRB-
,
Capt.
John
Miller
-LRB-
Hank
-RRB-
and
hi
team
ar
forc
to
search
for
a
Pvt.
Jame
Ryan
-LRB-
Damon
-RRB-
,
whose
brother
have
all
di
in
battl
.

Once
thei
find
him
,
thei
ar
to
bring
him
back
for
immedi
discharg
so
that
he
can
go
home
.

Accompani
Miller
ar
hi
crew
,
plai
with
astonish
perfect
by
a
group
of
charact
actor
that
ar
simpli
sensat
.

Barri
Pepper
,
Adam
Goldberg
,
Vin
Diesel
,
Giovanni
Ribisi
,
Davi
,
and
Burn
ar
the
team
sent
to
find
on
man
,
and
bring
him
home
.

The
battl
sequenc
that
bookend
the
film
ar
extraordinari
.

Liter
.

There
is
noth
in
film
that
ha
ever
been
record
that
will
prepar
you
for
the
sheer
onslaught
of
terror
violenc
in
the
film
's
first
20
minut
.

Spielberg
film
almost
the
entir
movi
without
music
,
leav
it
up
to
the
charact
to
gener
emot
,
and
thei
do
to
perfect
.

The
sequenc
in
Franc
,
all
of
them
,
begin
with
the
battl
and
end
with
the
battl
,
ar
fabul
,
especi
the
dialogu
between
the
men
as
thei
walk
through
the
hill
and
countrysid
,
try
to
save
Privat
Ryan
.

There
ar
no
word
I
can
us
to
describ
the
true
horror
and
power
of
these
sequenc
.

Thi
is
what
Coppola
wa
look
for
in
``
Apocalyps
Now
''
,
but
could
n't
creat
.

The
sheer
horror
of
these
sequenc
all
but
condemn
war
.

The
perform
by
Hank
as
the
leader
of
thi
gang
is
also
extraordinari
.

He
is
head
and
shoulder
abov
of
the
rest
of
the
actor
in
the
world
,
with
hi
comic
time
,
dramat
flair
,
hi
quiet
emot
that
stir
an
entir
nation
to
tear
.

Hank
is
thi
countri
's
finest
actor
,
and
he
prove
it
here
.

Howev
,
Spielberg
almost
destroi
hi
own
masterpiec
.

With
a
chanc
to
make
it
the
on
of
the
greatest
film
of
all
time
,
Spielberg
creat
10
minut
of
pure
worthless
film
.

The
sequenc
involv
Army
Chief-of-Stafff
Georg
Marshal
and
Mrs.
Ryan
is
decent
,
but
doe
n't
hold
up
to
the
rest
of
the
film
,
reli
on
wartim
clich
to
power
it
.

But
that
is
forgiv
.

What
is
n't
is
the
bookend
of
the
film
,
the
cemetari
sequenc
.

The
first
on
is
quit
good
,
a
decent
introduct
into
the
live
of
these
men
.

The
last
sequenc
is
atroci
.

The
forc
emot
,
accompani
by
a
ridicul
piec
of
music
,
is
simpli
horribl
compar
to
the
rest
of
the
magic
film
.

These
flaw
ar
what
downgrad
``
Ryan
''
from
the
greatest
film
of
our
era
,
to
the
greatest
war
film
of
our
era
.

Spielberg
should
have
trust
hi
own
materi
,
and
he
should
have
trust
Hank
to
deliv
the
most
chill
line
of
the
movi
,
to
end
hi
masterpiec
right
there
.

The
us
of
the
flag
,
though
patriot
,
is
in
contrast
to
the
movi
's
theme
.

The
power
of
the
bulk
of
the
film
,
howev
,
is
astonish
.

Spielberg
ha
truli
made
a
wondrou
work
of
art
,
that
persist
even
after
first
view
of
the
film
,
is
extraordinari
.

Thi
is
the
film
of
the
year
.

