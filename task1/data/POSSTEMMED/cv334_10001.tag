The
boom
introduct
music
finish
,
and
the
camera
sweep
over
red
mountain
.

We
see
two
figur
look
over
a
barren
,
and
veri
red
landscap
.

Thei
kiss
,
then
walk
on
.

One
of
the
charact
-LRB-
Schwarzenegg
-RRB-
slip
,
and
fall
.

The
glass
on
hi
mask
crack
.

Hi
face
scruch
up
,
and
hi
ey
's
begin
to
pop
...
.

Then
Doug
Quaid
,
plai
by
Schwarzenegg
,
wake
up
.

It
's
all
been
a
dream
.

He
wake
up
next
to
hi
beauti
wife
,
plai
by
Stone
,
and
the
film
begin
...
TOTAL
RECALL
is
a
typic
sci-fi
film
wrap
around
an
intrest
-LRB-
if
far
fetch
-RRB-
plot
.

Quaid
,
who
ha
never
been
to
Mar
,
keep
on
have
dream
of
the
planet
,
which
ha
now
been
colanis
.

One
dai
,
while
ride
on
a
train
,
he
's
see
's
a
ad
for
a
compani
call
'
Rekal
'
,
which
implant
memori
of
a
holidai
,
instead
of
you
actual
go
there
yourself
.

Quaid
choos
the
'
Secret
Agent
on
Mar
'
memori
,
and
then
the
s
***
realli
hit
the
fan
.

The
implant
goe
wrong
,
and
Quaid
realli
think
he
is
an
agent
on
Mar
for
about
two
minut
,
until
he
goe
back
to
normal
and
ca
n't
rememb
a
thing
.

Howev
,
everyon
he
know
-LRB-
hi
workmat
,
hi
wife
-RRB-
turn
against
him
,
sai
how
'
blab
about
Mar
'
and
'
blew
hi
cover
on
the
mission
'
A
strang
man
,
call
Richter
,
plai
by
Ironsid
,
want
to
kill
him
.

Quaid
,
now
hopelessli
confus
,
follow
the
advic
given
to
him
,
by
a
'
friend
'
.

He
get
's
hi
ass
to
Mar
.

Mar
is
rule
by
Cohagen
,
plai
by
Cox
,
who
charg
peopl
for
air
.

Against
him
ar
the
rebel
,
who
ar
mainli
mutant
.

The
rebel
's
need
Quaid
to
defeat
Cohagen
,
becaus
Quaid
know
-LRB-
but
ca
n't
rememb
yet
-RRB-
about
a
reactor
that
produc
air
.

Eventual
,
Quaid
kill
Cohagen
,
turn
on
the
reactor
,
save
the
planet
,
and
goe
awai
with
a
beauti
girl
,
just
like
the
gui
at
Rekal
promis
him
.

Throughout
thi
plot
,
there
is
huge
explos
,
thousand
of
death
-LRB-
and
I
mean
a
LOT
of
death
-RRB-
swear
,
sex
,
and
everyth
els
that
make
's
a
good
Verhoeven
-LRB-
director
of
Basic
Instinct
and
Starship
Trooper
-RRB-
movi
.

Natur
,
there
's
some
pretti
imagin
death
,
with
arm
get
rip
off
,
someon
get
drill
,
and
head
explod
.

As
you
can
tell
,
it
's
violent
stuff
.

But
it
's
done
so
stylishli
,
and
of
cours
Arnie
tone
it
down
with
a
on
liner
,
that
's
it
excus
.

The
act
is
variabl
.

Arnie
,
of
cours
,
is
useless
,
but
he
goe
ha
some
good
line
,
and
in
the
first
twenti
minut
,
he
even
act
's
like
a
normal
bloke
,
not
like
some
action
hero
.

Stone
,
of
cours
,
is
useless
,
make
a
pretti
poor
wife
,
and
a
pretti
poor
action
hero
.

But
,
she
doe
sex
scene
alright
.

Ticoton
-LRB-
most
recent
seen
in
CON
AIR
-RRB-
,
again
,
is
OK
,
do
some
good
action
,
but
get
her
emot
all
wrong
.

One
minut
she
hate
Quaid
,
the
next
she
love
him
,
and
she
never
get
thi
tranisit
right
.

So
,
the
good
gui
ar
aw
.

But
,
the
bad
gui
's
ar
excel
.

Cox
is
basic
repris
ROBOCOP
,
but
he
's
still
great
in
thi
,
and
he
deliv
the
best
line
in
the
whole
film
near
the
end
.

Ironsid
is
superbl
evil
,
with
an
utterli
emotionless
face
.

The
support
cast
ar
fine
,
and
not
even
the
comedi
sidekick
-LRB-
thi
time
in
the
form
of
a
taxi
driver
-RRB-
is
annoi
.

One
disappoint
aspect
,
though
,
ar
the
special
effect
.

ILM
and
Dreamquest
do
some
good
work
-LRB-
such
as
a
subwai
station
scene
-RRB-
but
the
model
work
is
not
so
good
.

Some
of
the
model
head
look
appal
fake
,
and
Quatto
is
a
major
disappoint
.

Another
irrit
thing
is
the
product
placement
.

There
ar
a
lot
of
plug
-LRB-
and
a
good
Mar
Todai
gag
-RRB-
Howev
,
the
set
look
great
,
and
the
special
effect
at
the
end
of
the
film
ar
n't
too
bad
either
.

There
's
also
a
good
soundtrack
run
through
,
by
Jerri
Goldsmith
-LRB-
THE
OMEN
,
POLTERGEIST
,
STAR
TREK
:
THE
MOTION
PICTURE
-RRB-
Overal
,
then
,
TOTAL
RECALL
is
a
huge
violent
,
yet
huge
enjoy
,
action
fest
,
with
a
reason
plot
thrown
in
there
too
,
some
great
bad
gui
,
and
some
imaganit
death
.

For
a
Schwarzenegg
movi
,
thi
is
not
bad
at
all
.

In
fact
,
it
's
veri
good
.

