One
can
not
observ
a
Star
Trek
movi
and
expect
to
see
seriou
scienc
fiction
.

The
purpos
of
Star
Trek
is
to
provid
flashi
,
innoc
fun
.

Sometim
the
stori
ar
compel
.

Sometim
thei
're
not
.

But
,
with
the
except
of
the
first
film
in
the
seri
-LRB-
which
provid
littl
more
than
endless
shot
of
amaz
face
-RRB-
,
I
've
never
been
bore
by
ani
of
the
Enterpris
's
numer
mission
.

Star
Trek
:
Insurrect
is
no
except
.

The
film
ha
gotten
some
neg
review
-LRB-
a
friend
of
mine
actual
think
it
's
the
worst
in
the
seri
-RRB-
,
but
I
'm
not
realli
sure
why
.

It
's
an
excit
,
often
hilari
movi
that
engag
me
and
left
me
readi
for
the
next
Star
Trek
film
.

Some
sai
it
's
a
bit
too
light
,
and
more
of
a
long
episod
than
a
film
.

Other
sai
the
special
effect
ar
cheesi
and
that
it
's
bore
.

I
simpli
enjoi
the
film
.

Insurrect
,
which
is
the
second
film
to
featur
strictli
the
Next
Gener
cast
,
introduc
us
to
a
race
of
peopl
call
the
Ba
`
ku
;
the
Ba
`
ku
ar
veri
old
-LRB-
most
of
them
ar
about
three
hundr
year
old
-RRB-
,
but
thei
actual
appear
younger
with
ag
due
to
strang
radiat
in
the
ring
of
their
planet
.

Of
cours
,
these
peac
peopl
ca
n't
hord
thi
fountain
of
youth
all
for
themselv
leav
it
up
to
their
archenemi
,
the
Son
`
a
,
led
by
Ru'afo
-LRB-
F.
Murrai
Abraham
-RRB-
,
to
mess
everyth
up
.

The
Son
`
a
,
who
ar
horribl
disfigur
and
reli
on
daili
reconstruct
surgeri
to
be
aesthet
accept
,
strike
a
deal
with
the
Feder
to
move
the
Ba
`
ku
elsewher
and
exploit
the
secret
to
keep
their
race
from
dy
.

That
's
when
Captain
Picard
-LRB-
Patrick
Stewart
-RRB-
step
in
:
he
realiz
that
move
the
Ba
`
ku
would
kill
them
-LRB-
it
also
help
that
he
fall
in
love
with
a
Ba
`
ku
woman
-LRB-
Donna
Murphi
-RRB-
-RRB-
.

So
,
with
hi
trusti
crew
,
Picard
defi
the
feder
to
keep
the
Ba
`
ku
in
their
natur
habitat
.

When
deal
with
a
seri
with
as
much
histori
as
thi
,
it
's
not
entir
necessari
to
re-introduc
your
charact
with
each
episod
.

Thi
is
why
I
believ
non-fan
have
a
hard
time
get
into
Star
Trek
;
in
order
to
enjoi
it
,
on
ha
to
understand
how
to
approach
it
.

Insurrect
,
howev
,
doe
a
surprisingli
good
job
of
us
new
aspect
of
the
charact
.

Stewart
is
bold
as
alwai
,
a
magnet
screen
presenc
and
perfectli
capabl
of
hold
an
entir
film
togeth
.

Jonathan
Frake
,
who
also
direct
,
is
funni
as
Command
Riker
;
a
subplot
with
Data
-LRB-
Brent
Spiner
-RRB-
discov
hi
lost
childhood
is
fairli
interest
;
and
Abraham
make
a
perfect
Star
Trek
villain
,
overact
like
crazi
.

Frake
show
similar
aptitud
for
direct
in
First
Contact
;
Insurrect
is
an
excit
film
,
with
some
realli
attract
special
effect
and
a
lot
of
good
action
.

Thi
is
appar
the
first
Star
Trek
film
to
util
comput
anim
,
and
the
result
is
veri
pleas
to
the
ey
:
particularli
in
the
climact
scene
,
in
which
the
Son
'
a
emploi
a
giant
space
ship
to
suck
up
the
ring
of
the
Ba
`
ku
planet
,
the
special
effect
have
a
clean
,
impress
sharp
look
.

If
I
have
ani
complaint
about
the
film
,
it
's
that
it
tri
to
take
a
moral
stanc
when
it
's
not
veri
appropri
to
do
so
.

I
do
n't
think
it
's
that
big
of
a
deal
that
the
Feder
want
to
move
600
Ba
`
ku
in
order
to
save
the
live
of
thousand
.

Better
yet
,
why
could
n't
thei
have
co-exist
?

Insurrect
feel
a
littl
to
light
to
spring
these
kind
of
big
moral
question
on
the
audienc
.

With
it
's
inher
camp
factor
,
the
Star
Trek
seri
doe
n't
seem
well-equip
to
deal
with
issu
like
thi
.

I
prefer
to
just
enjoi
the
spectacl
.

