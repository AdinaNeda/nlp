Ladi
and
gentlemen
,
1997
's
INDEPENDENCE
DAY
is
here
!

It
's
titl
:
STARSHIP
TROOPERS
.

And
surprisingli
,
it
is
more
entertain
than
ID4
.

I
realiz
that
I
gave
last
year
's
sci-fi
hit
a
4
star
rate
,
but
I
wa
under
the
spell
of
the
power
first
hour
.

After
subsequ
view
,
I
feel
that
INDEPENDENCE
DAY
realli
wa
n't
as
great
as
it
seem
--
though
I
still
give
it
a
posit
review
.

STARSHIP
TROOPERS
,
on
the
other
hand
,
ha
a
mock
sens
of
humor
,
know
perfectli
well
how
ridicul
it
mai
seem
.

As
a
result
,
STARSHIP
TROOPERS
is
a
more
excit
,
energet
,
and
live
scienc
fiction
film
.

In
fact
,
I
probabl
could
never
get
tire
of
watch
thi
film
.

STARSHIP
TROOPERS
is
veri
reminisci
of
STAR
WARS
,
anoth
kick-ass
space
opera
which
reinvent
the
sci-fi
drama
altogeth
.

While
STARSHIP
TROOPERS
is
no
STAR
WARS
,
it
come
quit
close
,
and
the
easiest
assess
would
be
to
call
thi
film
:
``
The
'
Star
War
'
of
the
90
.
''

Am
I
be
to
gener
to
thi
film
?

Perhap
,
but
when
a
scienc
fiction
film
come
along
that
can
mix
humor
and
romanc
,
effect
,
into
the
war
race
.

In
essenc
,
thi
is
the
scienc
fiction
equivol
of
Pulp
Fiction
.

The
easiest
wai
to
write
a
review
of
thi
film
would
be
to
compar
it
to
other
scienc
fiction
film
,
and
I
mai
have
to
.

STARSHIP
TROOPERS
is
not
quit
an
origin
film
persai
,
but
director
Paul
Verhoeven
give
the
film
a
``
kick-the-alien
'
-
ass
''
style
.

ID4
had
thi
same
style
,
except
it
wa
forc
,
most
like
the
caus
of
Will
Smith
have
a
major
role
.

Thi
new
``
Star
War
''
ha
a
veri
simpl
,
basic
plot
:
mankind
vs.
an
alien
speci
.

The
alien
speci
in
question
here
is
the
Bug
,
a
seemingli
advanc
civil
of
arachnid-typ
insect
.

Howev
,
the
underli
moral
of
STARSHIP
TROOPERS
ar
flesh
out
with
deadli
accuraci
.

Sometim
the
moral
ar
buri
by
the
bloodi
violenc
and
intens
action
scene
,
but
dure
quiet
moment
,
we
realiz
that
STARSHIP
TROOPERS
ha
someth
to
tell
us
about
our
civil
.

More
than
like
,
these
moral
will
be
overpow
and
quickli
forgotten
by
the
time
the
movi
end
.

And
thei
should
be
.

I
doubt
Verhoeven
intend
thi
film
to
be
a
thought-provok
film
.

Thi
is
an
``
event
''
film
,
but
surprisingli
abov
averag
.

It
is
extrem
refresh
to
watch
a
sci-fi
war
film
,
and
actual
root
for
the
human
side
-LRB-
someth
INDEPENDENCE
DAY
never
achiev
-RRB-
.

STARSHIP
TROOPERS
begin
with
a
satir
portray
of
those
strang
``
Join
the
Army
''
ad
.

Mix
a
Web-bas
interfac
,
these
summat
of
event
ar
refreshingli
origin
.

State
,
``
Would
you
like
to
know
more
?
''

,
these
short
new
bulletin
give
the
film
an
immedi
sens
of
humor
,
and
we
know
we
should
not
take
it
serious
.

Howev
,
we
also
wit
the
murder
of
sever
trooper
in
battl
,
in
a
gori
,
yet
funni
,
wai
.

After
wit
the
slaughter
,
the
film
jump
back
on
year
,
to
setup
what
we
just
saw
.

We
ar
introduc
to
Johnni
Rico
-LRB-
Casper
Van
Dien
-RRB-
and
Carmen
Ibanez
-LRB-
Denis
Richard
-RRB-
.

Rico
flirt
with
Carmen
by
send
her
messag
over
comput
termin
in
school
classroom
.

Howev
,
Carmen
is
n't
as
interest
in
Rico
as
she
is
in
becom
a
pilot
.

Howev
,
anoth
classmat
,
Dizzi
Flore
-LRB-
Dina
Meyer
-RRB-
,
ha
her
ey
on
Rico
.

Unfortun
,
Rico
is
n't
interest
in
Dizzi
.

Thi
setup
is
rather
clich
,
as
is
the
result
of
the
setup
,
but
it
still
is
fun
to
watch
.

Carmen
sign
up
to
becom
a
starship
trooper
,
and
as
a
result
,
Rico
sign
up
.

And
as
a
result
,
Dizzi
sign
up
.

Everyon
is
assign
a
certain
posit
in
the
academi
:
Carmen
get
the
pilot
posit
she
want
,
Carl
Jenkin
-LRB-
Neil
Patrick
Harri
-RRB-
,
a
psychic
,
get
a
posit
in
the
train
program
,
and
Rico
is
left
in
the
lowest
class
of
the
Starship
Trooper
:
the
Mobil
Infantri
.

The
main
stori
follow
Rico
through
hi
trial
and
relationship
.

In
realiti
,
I
found
myself
watch
a
space
version
of
``
Melros
Place
,
''
but
with
better
special
effect
.

An
hour
into
the
film
,
the
war
begin
.

A
sens
of
tension
grew
in
the
audienc
I
watch
thi
film
with
.

The
first
hour
mai
have
been
a
funni
-LRB-
sometim
unintention
-RRB-
setup
,
but
we
grew
to
care
for
the
flat
charact
,
becaus
no
matter
how
hard
I
tri
not
to
like
the
charact
,
their
gorgeou
look
won
me
over
-LRB-
call
me
superfici
,
if
you
must
-RRB-
.

Follow
the
train
session
,
which
had
mani
drama
in
themselv
-LRB-
too
mani
to
mention
here
-RRB-
,
the
trooper
ar
sent
to
war
.

The
Bug
live
on
the
planet
Klendathu
,
a
planet
which
consist
of
dirt
and
rock
,
and
pretti
much
noth
els
.

Mani
drama
occur
dure
the
war
,
until
final
the
trooper
realiz
that
their
is
someth
behind
the
Bug
'
attack
.

A
sort
of
intellig
which
mastermind
the
defens
.

The
trooper
ar
order
to
locat
the
``
brain
''
and
captur
it
.

I
hopefulli
left
the
plot
as
vagu
as
possibl
,
becaus
STARSHIP
TROOPERS
is
not
about
plot
.

It
's
about
special
effect
and
gori
violenc
.

Director
Verhoeven
,
most
known
for
hi
nudi
film
SHOWGIRLS
and
BASIC
INSTINCT
,
return
to
hi
sci-fi
dai
,
which
gave
us
good
film
like
Total
Recal
.

Verhoeven
like
to
push
the
envelop
of
the
MPAA
,
but
despit
all
the
violenc
,
the
comic
book
feel
give
the
film
a
sens
of
falsehood
.

STARSHIP
TROOPERS
is
extrem
violent
,
with
more
gore
than
thi
year
's
EVENT
HORIZON
.

The
R
rate
is
well
deserv
,
and
hopefulli
parent
will
not
allow
their
12
year
old
children
to
see
it
.

My
guess
is
that
Verhoeven
's
target
audienc
is
the
18
to
25
male
group
.

The
film
ha
a
testosteron
level
off
the
chart
,
with
chisel
men
and
beauti
women
.

Despit
these
superfici
element
,
STARSHIP
TROOPERS
also
creat
a
world
which
made
STAR
WARS
so
success
.

While
watch
the
film
,
we
never
second
guess
the
charact
's
decis
becaus
thei
seem
perfectli
clear
.

Thankfulli
,
no
hero
is
punch
alien
in
the
face
,
which
give
the
flat
charact
some
depth
for
which
we
can
identifi
with
.

The
cast
of
STARSHIP
TROOPERS
is
quit
annoi
to
begin
with
,
but
thei
soon
grow
on
you
.

Casper
Van
Dien
ha
the
chisel
facial
featur
and
tan
skin
which
will
make
most
girl
swoon
.

To
most
gui
,
he
will
come
across
as
artifici
.

Fortun
,
Van
Dien
is
the
worst
of
the
cast
,
despit
how
hard
he
tri
to
come
off
as
a
real
charact
.

Dina
Meyer
doe
a
much
better
job
of
flesh
out
her
charact
.

Her
Dizzi
is
extrem
likeabl
.

Denis
Richard
also
doe
a
good
job
,
and
her
scene
ar
veri
well
done
.

Jake
Busei
,
Gari
Busei
's
son
,
give
a
wonderfulli
funni
perform
,
and
provid
on
of
the
biggest
laugh
of
the
film
.

Neil
Patrick
Harri
-LRB-
rememb
,
he
wa
Doogi
Howser
-RRB-
is
a
littl
wooden
,
and
slightli
out
of
place
with
the
rest
of
the
cast
.

Howev
,
he
doe
give
a
good
perform
.

Michael
Ironsid
give
the
best
perform
of
the
film
as
the
teacher
with
on
arm
.

Hi
perform
rise
abov
the
rest
.

Perhap
it
is
becaus
hi
charact
is
the
most
develop
,
but
I
think
it
is
just
Ironsid
's
presenc
on
screen
.

Unfortun
,
no
particular
cast
member
give
a
star-mak
perform
,
which
is
too
bad
becaus
the
materi
here
could
have
made
mani
of
the
cast
member
star
.

Director
Verhoeven
is
the
main
star
of
the
film
.

Hi
direct
is
fast
and
unrelentless
.

He
never
slow
down
hi
pace
,
which
make
for
a
wild
ride
.

Writer
Edward
Neumeier
adapt
the
screenplai
from
the
book
by
Robert
Heinlein
.

The
write
is
usual
veri
funni
,
and
sometim
veri
corni
.

The
dialogu
is
smart
but
not
too
smart
.

We
ar
n't
requir
to
know
everyth
,
but
some
plot
hole
ar
left
unansw
.

Thankfulli
we
ar
not
intend
to
notic
,
and
we
realli
do
n't
-LRB-
except
for
on
which
linger
in
my
mind
for
quit
a
while
-RRB-
.

Amazingli
,
with
all
the
war
and
romanc
go
on
,
the
screenplai
manag
to
includ
some
surpris
government
issu
.

In
fact
,
these
government
chang
seem
fairli
plausibl
,
and
thi
add
anoth
layer
to
the
stori
.

The
moral
debat
of
the
charact
is
quit
surpris
to
find
in
a
film
of
thi
genr
,
but
what
is
even
more
surpris
is
the
amount
of
humor
includ
into
all
the
carnag
.

I
have
n't
laugh
thi
loud
,
even
in
some
comedi
.

Oh
,
and
the
special
effect
ar
first
rate
.

STARSHIP
TROOPERS
is
rate
R
for
graphic
sci-fi
violenc
and
gore
,
and
for
some
languag
and
nuditi
.

Believ
me
when
I
sai
thi
:
do
n't
take
young
kid
to
see
thi
film
.

It
is
extrem
violent
.

In
fact
,
it
is
probabl
the
most
violent
film
out
thi
year
.

Howev
,
if
you
ar
old
enough
-LRB-
over
17
-RRB-
to
see
it
,
STARSHIP
TROOPERS
is
guarante
to
entertain
.

It
's
been
a
long
time
sinc
I
have
cheer
for
the
hero
,
while
also
cheer
for
the
bad
gui
.

And
still
,
it
is
my
dream
to
see
a
sci-fi
film
in
which
the
alien
race
destroi
mankind
,
and
take
over
the
world
.

Now
that
would
be
entertain
!

