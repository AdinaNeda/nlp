As
I
walk
out
of
CROUCHING
TIGER
,
HIDDEN
DRAGON
I
thought
to
myself
that
I
had
had
just
seen
a
great
film
.

With
the
passag
of
a
few
hour
I
temper
my
enthusiasm
and
start
ponder
the
question
of
whether
a
masterpiec
must
implicitli
be
a
``
great
''
piec
of
work
and
viceversa
.

Attempt
to
make
a
distinct
mai
be
a
matter
of
split
hair
.

I
avoid
a
commit
by
appeal
to
etymolog
.

As
the
word
impli
,
a
masterpiec
is
ani
work
which
embodi
the
skill
of
a
master
.

As
such
it
should
suffic
to
sai
that
it
is
a
product
of
except
qualiti
.

CROUCHING
TIGER
,
HIDDEN
DRAGON
fit
comfort
in
that
categori
.

CROUCHING
TIGER
,
HIDDEN
DRAGON
immers
the
viewer
in
an
ideal
world
of
orient
folklor
,
with
the
requisit
blend
of
legend
,
fantasi
,
magic
and
mytholog
.

It
is
reminisc
of
a
Wagnerian
epic
with
charact
which
might
as
well
be
half-gods-great
than
life
,
purer
than
life
,
stronger
than
life
,
physic
invinc
and
abl
to
accomplish
superhuman
feat
,
but
with
a
human
soul
that
make
them
ultim
vulner
.

All
the
classic
element
of
the
orient
mystiqu
ar
thrown
into
the
mix
,
includ
the
art
of
contempl
and
the
concept
of
martial
dexter
as
the
physic
equival
of
spiritu
advanc
.

The
classic
struggl
between
good
and
evil
is
the
inevit
backdrop
,
with
advanc
master
of
each
and
a
golden
pupil
,
which
must
choos
between
the
two
.

It
is
the
gift
pupil
who
,
under
the
influenc
of
the
evil
master
steal
the
Holi
Grail
in
the
form
of
a
magic
sword
which
is
the
focu
of
the
conflict
at
the
heart
of
the
legend
.

The
element
of
romanc
at
two
differ
level
of
enlighten
-LRB-
a
pair
of
master
and
a
pair
of
youngster
-RRB-
ar
poignantli
repres
.

The
paradox
of
orient
restraint
exist
side
by
side
with
all
consum
passion
in
the
same
breast
is
project
effect
.

The
fight
scene
ar
stun
ballet
tours-de-forc
,
not
to
be
taken
liter
but
clearli
to
be
enjoi
as
superb
cinemat
art
,
as
ar
the
prodigi
leap
and
flight
to
,
from
and
between
rooftop
,
the
martial
combat
at
the
top
of
swai
bamboo
branch
and
the
combat
skip
like
pebbl
along
the
surfac
of
a
lake
.

There
need
be
no
question
of
suspend
disbelief
when
on
is
in
the
presenc
of
poetri
.

As
in
a
Wagnerian
opera
there
is
a
substanti
stori
line
,
which
take
place
at
an
ordinari
human
level
,
yet
the
entir
project
is
to
be
accept
as
a
work
of
art
root
in
fantasi
.

Do
not
assum
,
howev
,
that
the
art
is
limp-wrist
.

For
those
who
ar
put
off
by
the
``
art
''
label
thi
film
can
be
confid
recommend
as
engross
entertain
at
the
level
of
adventur
,
action
and
romanc
.

There
ar
no
weak
perform
in
thi
movi
.

Michel
Yeoh
imbu
her
charact
with
depth
,
human
and
wisdom
.

Chow
Yon
Fat
project
digniti
and
puriti
of
heart
.

Zhang
Ziyi
is
a
bud
superstar
.

She
is
radiantli
beauti
and
total
persuas
in
a
multifacet
role
.

Cheng
Pei
Pei
as
the
evil
master
and
Chang
Cheng
as
the
bandit
princ
acquit
themselv
admir
.

Kudo
to
screenwrit
Jame
Schamu
,
Wang
Hui
Ling
and
Tsai
Kuo
Jing
,
choreograph
Yuen
Wo-P
,
photographi
director
Peter
Pau
and
music
director
Tan
Dun
,
each
of
which
contribut
qualiti
compon
to
thi
extraordinari
film
.

Ang
Lee
as
the
director
,
co-produc
and
mastermind
of
the
project
get
the
lion
share
of
the
credit
.

Thi
is
destin
to
be
on
of
those
film
that
everybodi
like
,
includ
those
who
hate
martial
art
movi
.

Do
n't
miss
it
.

