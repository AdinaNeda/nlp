NOSFERATU
THE
VAMPYRE
-LRB-
Germani
1979
-RRB-
Thi
extraordinari
re-tel
of
Bram
Stoker
's
``
Dracula
''
by
German
filmmak
Werner
Herzog
deserv
the
most
promin
of
place
in
cinemat
vampir
lore
.

Inspire
by
F.W.
Murnau
's
1922
silent
film
of
the
same
name
,
Herzog
's
film
is
a
work
of
exquisit
bleak
,
an
oddli
touch
tragedi
with
a
beauti
and
uniqu
haunt
qualiti
that
linger
long
afterward
.

The
origin
full-length
English
and
German
languag
version
of
NOSFERATU
have
at
last
receiv
a
video
releas
by
U.S.
distributor
Anchor
Bai
Entertain
,
and
in
gorgeou
widescreen
print
.

Until
now
onli
a
shorten
English
languag
version
of
the
film
wa
avail
on
video
,
and
onli
in
Europ
.

Both
of
these
re-releas
restor
the
film
to
it
full
length
,
but
viewer
should
still
be
wari
of
the
English
version
due
to
it
often
stilt
dialogu
.

Apparent
the
English
dialogu
coach
Herzog
had
on
the
set
dure
film
wa
incompet
,
and
some
of
the
voic
also
seem
to
be
dub
.

The
German
version
with
English
subtitl
remain
the
definit
on
.

Everyon
know
the
stori
of
Dracula
.

Or
do
thei
?

Hi
stori
ha
been
so
bastardis
on
film
over
the
year
that
a
brief
remind
of
the
basic
plot
certainli
wo
n't
hurt
.

Jonathan
Harker
is
a
young
lawyer
sent
to
the
gloomi
castl
Dracula
in
Transylvania
to
do
busi
with
a
creepi
count
-LRB-
plai
by
Klau
Kinski
-RRB-
who
want
to
bui
a
hous
in
Harker
's
hometown
.

Upon
see
a
photo
of
Harker
's
wife
-LRB-
the
radiant
Isabel
Adjani
-RRB-
he
instantli
fall
in
love
with
her
.

Lock
Harker
up
in
hi
castl
,
he
set
off
on
a
long
journei
to
meet
thi
woman
whose
beauti
so
bewitch
him
.

Upon
divin
hi
ident
as
a
vampir
,
Adjani
seduc
the
count
and
lure
him
to
hi
death
on
morn
as
the
sun
rise
.

That
is
a
stori
familiar
to
million
,
but
Herzog
ha
elev
Stoker
's
tale
into
the
realm
of-and
I
us
thi
term
a
tad
reluctantly-cinemat
art
.

NOSFERATU
is
as
much
a
medit
as
it
is
a
film
,
shot
through
blue-ish
and
white
filter
and
peopl
with
charact
who
perform
as
if
thei
were
half
hypnotis
.

The
film
's
surreal
,
dream-lik
qualiti
is
utterli
mesmeris
.

There
is
much
to
enjoi
,
but
I
'll
limit
my
prais
to
a
few
kei
point
.

Hold
the
film
togeth
is
Kinski
's
remark
perform
as
Count
Dracula
.

Past
screen
portray
,
includ
Bela
Lugosi
's
famou
turn
,
have
larg
been
on
dimension
and
tend
toward
camp
.

But
Kinski
...
oh
boi
,
thi
is
someth
far
more
compel
.

Aide
by
a
startl
make-up
job
,
he
portrai
Dracula
's
vampir
not
simpli
as
pure
evil
but
as
some
sort
of
loathsom
diseas
.

Thi
man
is
dreadfulli
lone
.

He
live
in
utter
solitud
,
shun
by
the
local
for
hi
hideou
appear
and
reput
for
bloodlust
.

Kinski
's
portray
of
the
Count
is
both
creepi
and
deepli
affect
.

When
he
di
,
you
almost
feel
as
if
thi
man
's
tortur
soul
ha
been
freed
at
last
.

Then
there
's
the
unforgett
soundtrack
,
larg
compos
by
German
group
Popul
Vuh
.

It
is
so
eerili
beauti
and
evoc
that
it
's
quit
imposs
to
imagin
the
film
without
it
.

Popul
Vuh
ar
longtim
Herzog
collabor
and
plai
an
ancient-sound
kind
of
spacemus
us
piano
,
chant
,
and
exot
instrument
.

When
I
first
saw
thi
film
some
year
ago
I
wa
so
impress
I
track
down
and
bought
a
number
of
their
album
.

The
on
I
still
listen
to
the
most
is
``
Tantric
Song
''
,
from
which
most
of
the
music
in
NOSFERATU
is
taken
.

It
's
a
testament
to
the
music
's
depth
that
it
is
as
power
without
the
pictur
as
it
is
with
them
.

The
album
is
still
avail
on
the
highli
respect
ambient
and
world
music
label
Celesti
Harmoni
.

Thi
is
a
time
re-releas
by
Anchor
Bai
.

After
Franci
Ford
Coppola
's
unscari
and
woefulli
overblown
version
of
Bram
Stoker
's
tale
in
1992
,
it
is
a
joi
to
go
back
to
Herzog
's
film
and
see
the
amaz
thing
he
ha
done
with
what
is
now
a
century-old
stori
.

To
some
aficionado
,
NOSFERATU
is
quit
simpli
the
greatest
vampir
film
ever
made
.

Without
a
doubt
,
it
is
an
unmistak
classic
of
the
genr
.

Do
n't
miss
it
.

