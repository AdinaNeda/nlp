Steven
Spielberg
mai
not
have
direct
-LRB-
he
onli
execut
produc
-RRB-
but
hi
touch
certainli
is
evid
in
thi
movi
.

And
he
know
a
monei
make
winner
when
he
see
on
.

The
Mask
of
Zorro
ha
the
hallmark
of
a
blockbust
:
big
action
,
funni
comedi
,
and
the
pace
of
a
rollercoast
.

Hopkin
plai
Don
Diego
de
la
Vega
,
otherwis
known
as
Zorro
.

A
fighter
for
the
peopl
,
he
is
eventu
captur
by
Don
Rafael
Montero
-LRB-
Stuart
Wilson
-RRB-
,
who
also
,
unintention
,
kill
hi
wife
.

The
film
cut
to
twenti
year
later
,
where
Vega
still
want
to
Montero
dead
.

Howev
,
he
's
far
too
old
now
,
and
train
up
thief
Alejandro
Murrieta
-LRB-
Bandera
-RRB-
to
becom
the
new
Zorro
.

Bandera
motiv
?

To
aveng
the
death
of
hi
brother
,
who
wa
kill
at
the
hand
of
Captain
Love
-LRB-
Matthew
Letscher
-RRB-
who
rather
tastefulli
keep
the
head
in
a
jar
,
in
on
of
the
more
graphic
scene
in
thi
famili
movi
.

With
the
scene
set
,
the
movi
goe
on
full
blast
,
with
sword
fight
a-plenti
and
witti
on
liner
.

Bandera
and
Hopkin
have
a
marvel
chemistri
as
the
two
Zorro
's
,
plai
each
other
off
with
eas
.

Hopkin
seem
to
come
off
a
littl
better
,
but
Bandera
doe
some
excel
work
.

As
the
bad
gui
,
Wilson
and
Letscher
seem
on
dimension
,
but
thei
go
through
the
bad
gui
shtick
in
an
abov
averag
wai
.

The
audienc
doe
hate
them
,
which
ca
n't
be
bad
.

Catherin
Zeta-Jon
also
pop
up
as
Hopkin
daughter
,
and
although
she
provid
littl
more
than
the
usual
femal
lead
,
she
deliv
a
charm
,
but
on
note
,
perform
.

The
chemistri
is
excel
between
Bandera
and
Jone
.

There
's
a
delight
littl
scene
where
Bandera
and
Jone
have
a
sword
fight
,
and
everi
swipe
an
item
of
cloth
get
slash
,
until
Jone
cloth
fall
off
all
togeth
.

The
director
,
fresh
from
the
enjoy
GoldenEy
-LRB-
1995
-RRB-
handl
the
film
perfectli
.

The
action
is
well
done
and
fast
pace
,
the
comedi
put
across
well
.

The
cast
help
a
lot
,
but
the
direct
is
still
outstand
.

Although
the
film
is
veri
long
,
the
plot
keep
chug
along
,
and
there
's
bare
a
dull
moment
.

Howev
,
the
film
is
a
littl
over
indulg
,
and
it
could
have
been
trim
.

It
manag
to
hold
the
attent
,
thankfulli
,
and
the
adventur
and
charm
hold
right
up
to
the
last
reel
.

Screenwrit
John
Eskow
,
Ted
Elliot
and
Terri
Rosio
have
unfortun
written
an
clich
and
on
dimension
script
.

Although
thei
've
bought
Zorro
up
to
date
with
big
explos
and
wild
action
,
the
plot
itself
is
bare
exist
.

There
's
lot
of
littl
sub
plot
hang
around
,
but
there
's
noth
substanti
to
bring
them
all
togeth
.

The
main
stori
seem
to
be
the
fact
that
Rafael
want
to
'
bui
'
California
us
gold
stolen
from
the
gui
he
's
bui
the
land
off
.

Thi
set
up
for
a
Templ
of
Doom
type
plot
where
we
see
peasant
slave
driven
into
get
the
gold
from
littl
mine
.

Howev
,
thi
doe
n't
seem
to
appear
until
nearli
over
a
hour
into
the
film
,
so
the
film
is
never
quit
sure
where
it
's
go
.

In
the
end
howev
,
Zorro
provid
action
and
comedi
in
spade
.

It
suitabl
for
everyon
,
so
put
the
guilt
behind
,
and
put
some
more
monei
into
Spielberg
bank
account
.

You
wo
n't
regret
it
.

