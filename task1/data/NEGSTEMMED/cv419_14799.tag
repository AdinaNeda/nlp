Did
I
do
someth
bad
?

I
must
have
,
becaus
sit
through
thi
movi
wa
sheer
punish
.

Here
's
the
plot
.

Ricki
-LRB-
Jeff
Goldblum
-RRB-
is
produc
of
The
Good-Bui
Network
,
on
of
those
24-hour
home
shop
channel
.

The
new
boss
-LRB-
Robert
Loggia
-RRB-
plan
to
can
Ricki
's
behind
if
he
doe
n't
turn
the
previou
month
'
flat
sale
number
around
,
and
to
add
to
hi
problem
,
Ricki
also
ha
to
work
with
Kate
-LRB-
Kelli
Preston
-RRB-
,
the
ivi
leagu
wunderkind
whom
the
boss
ha
brought
with
him
.

Kate
and
Ricki
do
n't
get
along
and
do
n't
have
ani
great
idea
until
thei
meet
G
-LRB-
Eddie
Murphi
-RRB-
,
a
spiritualist
who
see
someth
posit
in
everyth
.

Hi
sooth
voic
and
simpl
logic
make
peopl
feel
good
,
and
it
's
thi
qualiti
that
will
make
G
the
new
GBN
televis
star
and
the
kei
to
the
network
's
success
.

The
first
problem
is
a
flaw
in
the
plot
.

Sure
,
G
make
peopl
feel
good
.

Sure
,
we
're
told
that
most
peopl
feel
guilti
after
bui
an
impuls
item
,
no
matter
how
wealthi
thei
might
be
,
and
I
'll
even
believ
that
G
talk
a
talk
that
allow
peopl
to
feel
good
about
what
thei
've
bought
.

But
,
hei
,
that
's
after
thei
've
bought
it
.

Why
do
sale
skyrocket
the
first
time
G
is
on
camera
?

Far
from
try
to
sell
the
product
,
G
instead
blather
on
about
how
you
do
n't
even
need
the
thing
.

Someon
pleas
tell
me
how
thi
is
suppos
to
move
merchandis
in
the
first
place
.

You
might
sai
that
peopl
feel
good
about
what
thei
've
bought
,
so
thei
come
back
and
by
more
,
but
due
to
the
simpl
fact
that
the
first
round
of
sale
in
inexplic
,
I
'm
not
convinc
thi
is
what
the
filmmak
had
in
mind
.

The
whole
movi
is
therefor
serious
undermin
becaus
the
kei
point
in
the
plot
is
never
credibl
.

Another
problem
is
in
the
humor
.

You
know
,
if
you
see
that
Eddie
Murphi
is
in
a
movi
,
I
think
it
is
not
unreason
for
you
to
expect
that
the
film
is
a
comedi
.

Whoa
,
partner
,
ar
you
in
for
a
surpris
.

HOLY
MAN
is
not
funni
.

Murphi
,
the
on
asset
you
'd
think
thi
movi
ha
-LRB-
rememb
how
thei
brought
him
in
at
the
last
minut
to
save
BEST
DEFENSE
?

-
then
again
,
mayb
you
do
n't
-RRB-
is
serious
reign
in
.

The
script
give
him
almost
noth
to
work
with
,
and
it
seem
as
though
director
Stephen
Herek
kept
him
tone
down
so
that
the
other
actor
in
the
film
would
n't
be
left
as
window
dress
.

There
's
onli
on
moment
in
the
film
where
Murphi
is
allow
to
let
loos
,
and
it
last
for
three
shout
word
,
which
seem
total
out
of
place
as
a
result
.

Pathet
.

In
all
,
there
ar
about
three
joke
that
work
in
the
entir
pictur
,
and
thei
're
not
even
that
great
.

As
a
side
note
,
there
ar
some
cameo
appear
such
as
Dan
Marino
pitch
a
contrapt
which
allow
you
to
cook
off
of
your
car
engin
,
and
Jame
Brown
introduc
a
medic-alert
devic
that
shout
``
help
me
!
''

just
like
the
Hardest
Work
Man
in
Show
Busi
at
the
push
of
a
button
,
but
these
never
caus
your
person
laugh-o-met
to
rise
abov
the
level
of
mild
bemus
.

``
Well
,
''
you
ask
,
``
sure
there
must
be
some
convinc
perform
to
make
up
for
the
lack
of
humor
.
''

Think
again
,
Buckwheat
.

Jeff
Goldblum
disappoint
.

Kelli
Preston
is
flat
-LRB-
but
not
like
that
-RRB-
.

Robert
Loggia
,
in
the
kind
of
role
for
which
he
ha
been
virtual
typecast
,
ca
n't
do
anyth
with
it
.

The
script
is
part
of
the
problem
,
but
these
actor
do
n't
even
look
like
thei
believ
in
the
move
thei
're
make
.

Thei
look
a
lot
like
thei
're
bore
.

Just
like
the
audienc
.

So
what
do
you
have
when
the
humor
is
absent
from
your
comedi
,
and
the
act
is
like
the
Siberian
stepp
?

A
film
that
drag
more
than
a
drop
anchor
.

But
wait
!

As
if
these
problem
do
n't
make
the
film
slow
enough
,
the
screenplai
's
pace
make
the
movi
even
slower
!

It
take
the
entir
first
half
hour
to
establish
the
movi
's
premis
,
then
more
time
as
the
film
wade
through
a
tortuou
-LRB-
and
seemingli
mandatori
-RRB-
romanc
between
Ricki
and
Kate
,
and
a
sub-plot
involv
a
conniv
PR
man
-LRB-
Eric
McCormack
-RRB-
who
want
to
discredit
G
and
take
over
Ricki
's
job
as
produc
.

When
the
PR
gui
's
plan
is
foil
,
the
film
is
climax
,
we
're
treat
to
that
littl
epilogu
,
and
then
we
can
all
go
home
,
right
?

Wrong
.

The
movi
goe
on
for
anoth
half
hour
!

Continent
drift
is
the
Indy
500
compar
to
the
pace
of
HOLY
MAN
!

The
onli
thing
holi
about
HOLY
MAN
will
sure
be
the
number
of
peopl
exclaim
``
Holi
!

@
#
%
that
movi
wa
aw
!
''

Thi
on
is
for
the
truli
piou
.

