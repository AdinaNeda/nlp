Late
in
Down
To
You
,
the
lead
femal
charact
-LRB-
Julia
Stile
-RRB-
state
her
greatest
fear
is
have
an
``
artifici
convers
''
with
her
boyfriend
-LRB-
Freddi
Prinz
Jr.
-RRB-
.

The
ironi
of
thi
statement
will
not
be
lost
on
audienc
member
who
have
not
yet
walk
out
,
fallen
asleep
,
or
otherwis
given
up
on
thi
pathet
attempt
at
romant
comedi
.

The
film
is
a
multi-million
dollar
testament
to
everyth
artifici
.

Fill
with
charact
,
motiv
,
plot
and
dialogu
that
ring
fake
and
shallow
at
everi
step
thi
is
an
unquestion
disast
.

Prinz
plai
a
colleg
student
intent
on
plai
the
field
until
he
fall
for
Stile
'
freshman
co-e
.

At
first
the
movi
seem
to
be
an
attempt
to
string
along
scene
with
the
sole
purpos
of
out-do
the
previou
in
how
``
cute
''
it
can
be
.

The
happi
coupl
``
psychoanalyz
''
each
other
;
thei
pick
out
a
song
becaus
,
as
Prinz
's
DJ
mother
-LRB-
Luci
Arnez
-RRB-
explain
,
everi
coupl
need
a
song
;
he
smell
her
shampoo
;
she
scratch
hi
chest
in
the
morn
to
let
him
know
she
want
to
make
love
.

Then
,
in
standard
romant
comedi
style
,
thing
fall
apart
.

Why
?

Well
,
becaus
thei
have
to
.

Otherwis
the
coupl
ca
n't
make
up
and
live
-LRB-
presum
-RRB-
happili
ever
after
.

Down
To
You
exist
outsid
the
realm
of
normal
logic
.

Scene
do
not
connect
to
each
other
and
it
often
seem
to
be
attempt
some
sort
of
surreal
,
but
thi
is
handl
so
poorli
that
everyth
come
off
random
and
stupid
instead
.

A
picnic
scene
involv
the
six
kei
charact
is
just
bizarr
,
not
onli
for
the
stretch
logic
of
these
peopl
hang
out
togeth
,
or
even
know
each
other
,
but
mostli
becaus
an
unusu
amount
of
screen
time
is
dedic
to
a
goat
.

The
support
charact
ar
remark
flat
,
and
insultingli
ridicul
as
well
.

Stile
'
best
friend
-LRB-
Rosario
Dawson
-RRB-
is
a
pot
head
,
Prinz
's
roommat
-LRB-
Shawn
Hatosi
-RRB-
is
a
girl
crazi
fool
who
ca
n't
meet
women
,
and
then
there
ar
the
porn
star
-LRB-
Zak
Orth
and
Selma
Blair
-RRB-
.

He
is
on
of
those
famou
porn
star
who
is
frequent
interview
on
televis
,
doe
the
colleg
lectur
circuit
,
and
star
in
high
budget
costum
epic
porn
that
ar
shot
on
film
.

You
know
,
the
averag
porn
star
.

The
costum
epic
idea
give
him
a
reason
to
appear
in
a
differ
ridicul
outfit
everytim
he
's
on
screen
,
the
porn
angl
exist
just
so
there
can
be
a
coupl
of
stupid
sex
joke
.

She
is
a
porn
star
mostli
so
we
know
she
's
slutti
,
so
her
veri
exist
will
make
Stile
jealou
and
Prinz
tempt
.

Attempt
at
drama
-LRB-
which
mean
Julia
is
go
to
cry
-RRB-
includ
a
pregnanc
scare
and
the
big
break
up
.

These
charact
ar
so
insipid
onli
the
most
forgiv
viewer
would
be
manipul
by
these
weak
scene
.

What
happen
here
?

The
cast
is
full
of
likabl
,
promis
,
young
actor
-LRB-
Stile
especi
seem
to
be
go
place
-RRB-
.

Writer/director
Kri
Isacsson
ha
simpli
given
them
materi
so
weak
not
even
season
pro
could
pull
it
off
.

In
fact
the
whole
film
feel
like
a
rework
of
last
year
's
extrem
unpleas
The
Stori
of
Us
,
for
teen
audienc
.

The
direct
is
film-school-grad
obviou
.

It
's
stylish
in
a
huge
self
consciou
wai
which
throw
in
split
screen
,
a
pointless
parodi
of
``
Cop
,
''
charact
walk
into
the
flashback
of
other
charact
,
and
a
hilari
cheesi
airport
meet
between
Prinz
and
Stile
where
all
the
extra
move
in
fast
motion
while
the
coupl
move
in
extrem
slow
motion
.

Plu
,
the
whole
stori
is
told
flashback
style
,
narrat
by
Prinz
and
Stile
directli
address
the
camera
.

Thi
is
the
second
film
in
a
row
-LRB-
follow
Wing
Command
-RRB-
star
Freddi
Prinz
Jr.
which
should
have
premier
on
the
aisl
of
the
local
Blockbust
but
wa
like
save
from
that
fate
due
to
the
fluke
success
of
She
's
All
That
.

If
he
doe
n't
start
make
better
choic
fast
hi
futur
film
wo
n't
be
so
lucki
.

