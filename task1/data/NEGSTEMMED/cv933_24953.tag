MISSION
:
IMPOSSIBLE
-LRB-
director
:
John
Woo
;
screenwrit
:
Robert
Town
base
on
a
stori
by
Ronald
D.
Moor
and
Brannon
Braga
and
the
televis
seri
creat
by
Bruce
Geller
;
cinematograph
:
Jeffrei
L.
Kimbal
;
editor
:
Christian
Wagner/Steven
Kemper
;
cast
:
Tom
Cruis
-LRB-
Ethan
Hunt
-RRB-
,
Dougrai
Scott
-LRB-
Sean
Ambrose
-RRB-
,
Thandi
Newton
-LRB-
Nyah
Nordoff-Hal
-RRB-
,
Richard
Roxburgh
-LRB-
Hugh
Stamp
-RRB-
,
John
Polson
-LRB-
Billi
Baird
-RRB-
,
Brendan
Gleeson
-LRB-
McCloi
-RRB-
,
Rade
Sherbedgia
-LRB-
Dr.
Nekhorvich
-RRB-
,
Ving
Rhame
-LRB-
Luther
Stickel
-RRB-
,
Anthoni
Hopkin
-LRB-
Hunt
's
boss
,
Swanbeck
-RRB-
,
William
R.
Mapoth
-LRB-
Walli
-RRB-
,
2000
-RRB-
M
:
I-2
,
the
sequel
to
Mission
Impossibl
,
is
a
Jame
Bond
wannab
film
,
but
it
fail
to
even
come
close
to
that
film
in
wit
,
humor
,
and
entertain
valu
.

It
tri
to
be
a
spy/rom
movi
,
but
without
ani
suspens
the
film
just
look
like
it
's
an
extend
commerci
for
dude
who
think
thei
look
cool
in
throwawai
sunglass
.

It
is
a
film
that
prefer
techi
gadget
to
anyth
human
.

The
coolest
thing
about
thi
movi
,
wa
all
the
hole
it
had
in
it
stori
and
the
most
trite
thing
about
the
movi
,
wa
the
usag
of
dove
throughout
as
peac
symbol
.

The
film
plai
as
if
it
wa
a
wet
fantasi
dream
about
techi
violenc
.

Except
for
the
choreograph
action
sequenc
,
the
film
wa
dull
for
three-quart
of
it
time
,
fill
with
too
mani
dead
spot
in
it
stori
to
garner
concern
about
it
wooden
charact
or
the
superfici
romanc
that
develop
.

As
for
the
action
scene
,
thei
might
look
good
to
those
who
ar
convert
to
violenc
in
their
film
,
but
their
advertis
for
sadist
respons
,
is
noth
short
of
mindless
cartoon
violenc
,
which
make
it
veri
difficult
to
sit
back
and
applaud
without
feel
put
off
by
the
gratuit
cruelti
seen
.

M
:
I-2
open
and
close
with
fast-pac
action
scene
,
but
it
is
hard
to
get
past
the
middl
part
which
just
drag
on
in
banal
dialogu
.

The
film
look
as
if
it
had
been
invad
by
a
comput
viru
,
at
that
point
.

The
onli
thing
that
kept
me
awak
,
wa
the
horribl
music
compos
by
Han
Zimmer
that
becam
veri
loud
at
ani
of
the
film
's
supposedli
moment
action
scene
and
seem
to
make
an
uninterest
scene
even
more
notic
in
the
wrong
wai
.

It
's
a
mega-buck
film
adapt
from
a
popular
high-tech
gadgetri
TV
seri
.

But
it
artist
success
is
an
imposs
task
to
accomplish
becaus
it
hire
the
wrong
director
and
actor
to
star
in
it
,
and
it
fail
to
produc
a
stori
that
had
ani
substanc
.

John
Woo
-LRB-
``
Broken
Arrow
''
/
``
Face/Off
''
-RRB-
is
good
at
do
car
chase
,
choreograph
fight
with
midair
flip
and
kung-fu
kick
,
slo-mo
shot
of
two
gun
blaze
,
and
of
fire
explos
,
but
he
just
ca
n't
seem
to
handl
dialogu
and
suspens
.

The
star
of
the
film
and
co-produc
,
Tom
Cruis
,
and
hi
romant
interest
,
Thandi
Newton
,
ar
miscast
.

Cruis
is
no
Jame
Bond
and
look
more
like
a
yuppi
than
a
superhero
in
hi
stylish
long
hair
and
innocu
smile
,
as
he
tri
to
carri
off
thi
macho
role
,
while
Thandi
is
not
an
action-film
girl
,
and
seem
like
a
fish-out-of-wat
in
thi
on
.

Their
romanc
did
n't
work
,
not
onli
wa
it
tepid
and
not
sexi
,
but
it
wa
n't
convinc
.

The
film
open
with
dizzi
speed
,
perhap
with
the
hope
that
a
befuddl
audienc
is
it
best
bet
for
success
.

We
will
be
in
three
differ
locat
instantan
:
Sydnei
,
the
American
Southwest
,
and
Sevil
.

First
,
we
ar
in
Sydnei
,
Australia
,
where
a
scientist
with
a
muffl
Russian
accent
,
Dr.
Nekhorvich
-LRB-
Rade
-RRB-
,
mention
that
he
creat
a
deadli
killer
viru
call
Chimera
and
an
antidot
for
it
.

He
also
mention
that
everi
hero
need
a
worthi
villain
.

Which
explain
the
film
's
mythic
theme
...
as
we
enter
the
world
of
comic
book
myth
on
good
and
evil
.

The
on
who
plai
the
villain
,
Dougrai
Scott
,
doe
so
in
a
one-dimension
gruff
tone
,
which
did
not
distinguish
him
in
that
role
.

At
least
,
if
the
film
got
the
villain
part
right
,
it
might
have
had
some
fun
with
thi
nonsens
.

Soon
the
diabol
scientist
is
on
a
plane
talk
to
someon
he
trust
call
Dimitri
,
but
then
the
plane
is
taken
over
by
terrorist
who
set
it
on
automat
pilot
and
crash
it
into
the
Rocki
Mountain
.

Befor
thei
crash
the
plane
and
parachut
out
of
it
,
the
on
who
wa
pose
as
Dimitri
,
turn
out
to
be
Sean
Ambrose
-LRB-
Scott
-RRB-
,
a
rogu
member
of
the
IMF
,
which
is
a
CIA-like
clone
.

He
steal
the
packag
with
the
antidot
,
peel
off
a
latex
mask
,
which
is
a
replica
of
the
hero
of
the
stori
,
Ethan
Hunt
-LRB-
Cruis
-RRB-
,
who
had
pose
as
Dimitri
to
the
scientist
befor
and
had
therebi
gain
hi
trust
.

Sean
and
hi
group
of
terrorist
carri
out
thi
attack
becaus
thei
plan
a
viru
plagu
on
the
world
and
then
to
sell
the
victim
the
antidot
at
mark
up
price
.

We
alreadi
saw
the
gimmick
of
peel
mask
us
in
Face/Off
and
in
the
origin
Mission
Impossibl
,
which
as
convolut
a
plot
as
that
film
had
,
it
wa
still
a
superior
film
to
thi
sequel
.

Woo
ha
run
thi
peel
mask
routin
into
the
ground
,
as
it
is
us
so
often
in
thi
film
by
both
side
,
so
much
so
,
that
it
blur
ani
ethic
charact
differ
between
good
gui
or
villain
.

It
make
it
seem
as
if
anyon
could
be
anoth
charact
,
which
distort
the
realiti
of
the
film
and
make
it
imposs
for
the
film
to
make
much
sens
.

Next
we
ar
in
a
mountain
rang
in
the
American
Southwest
,
and
Ethan
is
on
vacat
,
hang
by
hi
fingertip
while
climb
and
look
cool
,
when
a
helicopt
with
hi
boss
Anthoni
Hopkin
aboard
,
deliv
via
a
rocket
launcher
,
a
pair
of
talk
sunglass
.

Hunt
learn
hi
next
mission
is
to
retriev
the
Chimera
packag
and
he
is
allow
to
pick
two
regular
IMF
agent
to
help
,
Billi
Baird
-LRB-
John
Polson
-RRB-
and
Luther
Stickel
-LRB-
Ving
Rhame
-RRB-
,
with
Luther
run
a
high-gadget
comput
,
but
he
also
must
get
a
jewel
thief
name
Nyah
Hall
-LRB-
Thandi
-RRB-
to
join
hi
team
.

He
is
told
,
as
an
incent
to
recruit
her
,
all
her
crimin
charg
will
be
drop
.

Hopkin
then
sign
off
with
the
tag
line
:
Thi
messag
will
self-destruct
in
five
second
.

Actualli
,
with
the
departur
of
Hopkin
,
it
wa
thi
dispos
film
that
actual
self-destruct
at
thi
point
.

In
Sevil
,
Hunt
recruit
Nyah
into
the
team
in
the
middl
of
a
jewel
heist
and
a
subsequ
car
chase
,
where
he
nearli
run
her
Audi
sport
car
over
the
side
of
a
mountain
road
.

He
also
fall
for
her
when
thi
wa
onli
suppos
to
be
a
busi
deal
,
and
learn
that
she
is
valuabl
becaus
her
ex-boyfriend
wa
Sean
Ambrose
and
that
he
still
want
to
fck
her
.

The
IMF
team
then
inject
a
locat
tracer
chip
into
her
to
spot
Sean
so
she
can
go
fck
him
,
as
she
lead
them
to
hi
hide-out
in
the
seasid
of
Australia
in
which
he
share
with
hi
sneer
villain
cohort
,
the
South
African
,
Hugh
Stamp
-LRB-
Richard
Roxburgh
-RRB-
.

Robert
Town
,
the
screenwrit
,
who
contribut
to
the
first
``
Mission
,
''
who
is
note
for
do
``
Chinatown
''
--
write
a
colorless
,
pedestrian
script
,
on
that
fail
even
to
be
funni
in
a
camp
wai
.

The
terrorist
,
who
aim
to
rule
the
world
,
ar
interest
in
own
51
percent
in
a
biotech
compani
and
in
get
stock
option
,
as
thei
plan
to
infect
Sydnei
with
the
viru
and
have
their
compani
sell
the
antidot
,
insur
that
thei
will
make
billion
on
the
stock
.

Ethan
come
to
the
rescu
of
the
world
and
of
Nyah
,
with
hi
onli
conflict
be
who
is
more
import
to
save
first
.

Ethan
doe
thi
rescu
against
all
odd
,
as
he
find
a
wai
to
penetr
a
secur
tight
biotech
compani
,
fight
it
out
with
Sean
and
the
other
terrorist
,
and
rescu
Nyah
,
who
inject
herself
with
the
viru
to
hinder
Sean
's
get
it
,
as
the
onli
wai
to
transport
the
viru
is
through
anoth
person
or
from
the
vaccin
needl
.

Ethan
rescu
her
by
do
stunt
ride
on
a
motorcycl
,
us
kick-box
,
win
a
shootout
,
throw
a
full
John
Wayn
suppli
of
grenad
at
the
terrorist
,
make
some
more
us
out
of
that
peel
mask
bit
,
and
by
be
complet
fearless
and
larger
than
life
,
while
he
kick
's
everyon
's
ass
.

If
I
wa
onli
entertain
by
thi
...
I
could
have
live
with
it
.

But
thi
film
wa
so
badli
made
,
that
it
wa
like
watch
a
highlight
film
of
a
basketbal
game
,
see
onli
the
slam-dunk
,
but
with
the
game
itself
be
exclud
from
the
telecast
.

In
ani
case
,
thi
is
a
critic-proof
film
,
and
will
in
all
probabl
do
well
in
the
box
offic
,
as
it
wa
made
to
appeal
to
all
the
demograph
who
find
commerci
ventur
like
thi
on
easi
to
bui
into
.

Denni
Schwartz
:
``
Ozu
'
World
Movi
Review
''
ALL
RIGHTS
RESERVED
DENNIS
SCHWARTZ

