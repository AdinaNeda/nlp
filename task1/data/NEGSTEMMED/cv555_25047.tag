What
happen
when
you
put
Martin
Lawrenc
in
a
fat
suit
in
real
life
?

You
get
Martin
Lawrenc
in
a
coma
-LRB-
the
comedian
thought
he
wa
get
fat
,
so
he
put
on
numer
layer
of
heavi
cloth
and
went
jog
in
100
degre
heat
,
end
up
in
a
coma
-RRB-
.

So
what
happen
when
you
put
Martin
Lawrenc
in
a
fat
suit
in
the
movi
?

You
get
an
audienc
in
a
coma
.

In
Big
Momma
's
Hous
,
Lawrenc
plai
a
FBI
agent
who
's
a
master
of
disguis
.

When
a
naughti
,
naughti
man
escap
from
jail
and
seek
out
hi
former
girlfriend
-LRB-
the
veri
sexi
Nia
Long
...
the
onli
thing
worth
look
at
in
thi
movi
-RRB-
,
she
flee
to
Georgia
to
stai
with
her
rather
larg
Southern
aunt
``
Big
Momma
''
-LRB-
Ella
Mitchel
-RRB-
.

The
FBI
follow
her
in
order
to
recov
the
larg
sum
of
monei
stolen
in
the
bank
robberi
that
sent
the
naughti
man
to
jail
.

Howev
,
Big
Momma
is
call
awai
on
an
emerg
.

Sens
that
thei
could
lose
their
onli
chanc
to
captur
the
crimin
,
Lawrenc
goe
undercov
as
Big
Momma
.

And
let
the
comedi
begin
!

Big
Momma
's
Hous
is
the
definit
of
a
``
gimmick
''
movi
if
there
ever
wa
on
.

The
plot
can
basic
be
sum
up
as
``
Martin
Lawrenc
dress
up
as
a
fat
woman
''
...
and
even
that
descript
is
too
wordi
.

You
can
just
see
thi
script
plot
out
on
a
chalkboard
with
MARTIN
LAWRENCE/FAT
WOMAN
in
the
middl
of
the
board
with
a
big
circl
around
it
,
and
all
sort
of
clichd
,
``
humor
''
situat
point
to
it
.

Fat
woman
plai
basketbal
!

Fat
woman
take
a
dump
!

Fat
woman
kung
fu
!

Well
thei
left
on
off
the
board
...
movi
critic
slip
into
coma
!

Paul
Giamatti
-LRB-
Privat
Part
,
The
Negoti
-RRB-
plai
Lawrenc
's
partner
and
as
much
as
I
enjoi
hi
work
,
even
he
ca
n't
inject
life
into
thi
lifeless
comedi
.

Hell
,
the
greatest
perform
on
the
planet
could
n't
make
thi
materi
work
...
it
's
just
that
aw
.

