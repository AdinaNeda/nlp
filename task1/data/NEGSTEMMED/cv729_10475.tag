As
forget
as
some
peopl
mai
be
it
is
doubt
that
anyon
could
forget
their
wed
,
especi
three
time
.

But
,
ala
professor
Brainard
somehow
manag
to
accomplish
thi
feat
twice
befor
the
moment
night
that
he
actual
creat
Flubber
.

It
's
amaz
that
he
is
abl
to
rememb
ani
of
the
process
he
us
to
make
anyth
.

The
whole
point
here
is
how
could
a
man
be
so
blatantli
forget
.

Thi
is
not
absent
mind
thi
is
almost
mindless
and
he
is
a
professor
.

Well
,
thi
is
the
case
for
about
the
first
half
of
the
movi
then
thing
begin
to
settl
into
what
might
truli
be
consid
absent
mind
.

Along
the
wai
to
becom
absent
mind
from
absent
of
mind
the
professor
stumbl
onto
Flubber
.

Thankfulli
or
there
would
be
no
movi
or
ani
amus
for
the
audienc
.

The
Flubber
is
suppos
to
be
fly
rubber
,
but
it
seem
to
have
a
life
of
it
own
.

Thi
is
there
strictli
for
the
kid
.

The
properti
of
the
Flubber
provid
some
antic
for
the
audienc
when
Flubber
coat
golfbal
and
bowl
ball
assail
two
would
be
thug
.

Although
amus
thi
is
not
origin
what
so
ever
and
bring
back
haunt
memori
of
Home
Alone
.

The
poor
professor
ha
to
save
the
univers
where
he
teach
at
,
get
hi
fiance
back
,
final
stop
the
plot
of
an
evil
millionair
,
and
do
all
thi
in
the
funniest
wai
possibl
.

Admittedli
there
ar
funni
moment
but
the
movi
is
rather
shallow
and
doe
not
compar
veri
well
to
the
origin
-LRB-
The
Absent
Mind
Professor
-RRB-
.

The
whole
scene
with
the
basketbal
game
wa
just
atroci
and
total
unbeliev
.

The
basketbal
player
,
stereotyp
unqualifi
for
the
sport
,
somehow
manag
to
bounc
,
fly
and
dribbl
their
wai
back
to
be
close
to
win
?

Thi
just
sound
too
bad
to
be
true
,
the
least
that
could
have
been
done
wa
keep
semi
close
to
the
origin
movi
to
give
thi
film
a
chanc
.

As
for
innov
and
new
idea
there
is
but
two
good
on
in
thi
movi
.

Give
life
to
the
Flubber
,
which
is
so
poorli
execut
that
it
's
best
forgotten
,
and
Prof.
Brainard
's
fly
robot
Weebo
.

Give
life
to
the
Flubber
would
have
work
beautifulli
if
it
wa
n't
for
scene
of
the
sort
where
you
have
the
Flubber
danc
around
on
tabl
and
book
for
no
real
reason
other
than
to
put
in
a
bit
of
music
and
extend
the
length
of
the
movi
.

The
life
of
the
Flubber
had
brief
and
rather
whimsic
moment
where
it
might
have
been
us
well
,
but
it
wa
n't
and
in
the
end
might
have
done
the
movi
more
harm
than
good
.

Now
,
Weebo
is
a
total
differ
stori
.

It
's
a
robot
that
Prof.
Brainard
creat
but
forgot
how
and
gave
someth
of
a
life
too
.

Thi
is
a
realli
origin
idea
,
well
mayb
not
,
but
it
is
an
idea
that
is
well
execut
and
in
the
end
make
the
poor
robot
the
most
develop
and
real
charact
in
the
movi
.

Ye
folk
the
littl
yellow
robot
ha
more
emot
and
charact
than
the
charact
.

The
film
lack
the
emot
punch
to
pull
the
audienc
in
at
all
.

The
act
by
Robin
William
-LRB-
Prof.
Brainard
-RRB-
seem
to
be
half
heart
for
most
of
the
movi
and
in
mani
case
quit
forc
.

Most
of
the
act
in
the
movi
wa
so
convinc
as
to
fool
onli
the
part
of
the
audienc
that
had
not
quit
yet
reach
the
ag
to
12
.

Peopl
when
stood
up
at
the
altar
,
do
n't
usual
give
third
chanc
.

Or
for
that
matter
talk
to
the
fiance
afterward
,
but
what
can
be
expect
thi
is
a
children
's
movi
.

Flubber
,
as
a
movi
doe
littl
origin
and
is
noth
special
in
ani
respect
.

With
the
best
charact
in
the
movi
be
someth
of
a
prop
I
doubt
that
mani
peopl
will
find
it
all
that
great
.

It
doe
have
it
amus
moment
and
it
is
a
good
stori
in
the
end
,
but
it
doe
not
wash
up
to
the
origin
.

Kid
should
find
thi
movi
amus
and
fun
and
thei
'll
probabl
enjoi
the
whole
movi
.

As
a
movi
it
's
just
a
kid
movi
and
not
the
best
of
on
at
that
.

