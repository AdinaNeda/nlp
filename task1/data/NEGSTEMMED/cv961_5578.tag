BARB
WIRE
,
Pamela
Anderson
Lee
's
first
forai
into
film
,
highlight
the
fact
that
her
onli
talent
li
in
her
silicon
enhanc
asset
.

Be
the
onli
notabl
member
of
the
cast
,
the
camera
linger
lustili
o
n
her
bodi
at
everi
opportun
,
make
her
charact
's
catch
line
,
``
do
n't
call
me
babe
,
''
sound
veri
iron
inde
.

From
the
veri
open
of
the
movi
,
we
ar
treat
to
a
stripteas
routin
from
Anderson
,
end
in
her
hurl
her
stiletto
smack
between
the
ey
of
a
lusti
male
who
happen
to
call
her
babe
.

Throughout
the
movi
,
there
is
ampl
footag
of
enorm
breast
and
cleavag
,
if
not
of
Anderson
's
,
then
at
least
of
the
femal
extra
.

Thi
alon
is
enough
to
retitl
the
movi
BABE
WIRE
.

For
a
plot
,
BARB
WIRE
rehash
the
CASABLANCA
storylin
.

It
is
2017
,
the
middl
of
the
second
American
Civil
War
,
and
Barb
Wire
,
a
former
resist
fighter
,
run
a
joint
in
Steel
Harbour
call
Hammerhead
-LRB-
!!
-RRB-
.

Known
for
attract
resist
fighter
an
d
charact
of
all
sort
,
the
bar
attract
the
attent
of
the
govern
forc
who
appear
dress
in
Nazi-styl
uniform
.

In
between
bash
up
helpless
male
and
show
off
her
trademark
breast
,
Barb
Wire
ha
to
help
a
former
lover
and
hi
wife
get
to
the
airport
on
the
other
side
of
the
town
,
past
the
government-control
area
,
and
to
freedom
.

Even
the
airport
look
like
the
on
in
CASABLANCA
,
except
that
the
plane
in
the
background
is
a
modern
,
privat
jet
.

There
ar
hardli
ani
signific
moment
in
thi
film
,
and
on
get
the
impress
that
it
wa
design
for
young
teenag
familiar
with
the
Dark
Hors
comic
version
of
``
Barb
Wire
.
''

If
anyth
,
on
leav
the
film
with
the
confirm
that
Anderson
di
d
not
do
her
own
stunt
.

Who
could
fight
and
jump
in
a
skimpi
,
strapless
leather
top
,
and
yet
keep
her
breast
from
spill
out
?

Only
a
stuntwoman
.

Not
Pamela
Anderson
Lee
.

