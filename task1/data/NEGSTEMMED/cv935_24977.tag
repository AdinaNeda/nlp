The
plot
of
BIG
MOMMA
'S
HOUSE
is
Martin
Lawrenc
in
a
fat
suit
and
a
dress
.

That
's
not
just
the
high-concept
premis
;
it
's
the
fully-r
,
all-encompass
plot
.

Such
an
emphasi
is
not
unheard-of
in
the
world
of
Hollywood
summer
entertain
.

One
need
mere
look
back
to
last
summer
,
when
the
plot
of
BIG
DADDY
wa
Adam
Sandler
be
an
incompet
surrog
parent
.

The
trap
inher
in
such
an
approach
is
that
the
high-concept
plot
idea
better
be
pretti
well-real
,
or
rest
on
the
shoulder
of
an
extrem
talent
perform
,
becaus
you
can
bet
there
will
be
noth
els
worth
a
second
of
your
time
--
not
a
develop
charact
,
not
a
provoc
theme
,
not
a
witti
twist
.

You
will
get
90-plu
minut
of
Martin
Lawrenc
in
a
fat
suit
and
a
dress
--
noth
more
,
noth
less
.

Those
who
find
Martin
Lawrenc
more
than
an
occasion
amus
screen
presenc
mai
have
a
shot
at
enjoi
the
one-not
dud
that
is
BIG
MOMMA
'S
HOUSE
.

Other
will
simpli
stare
,
mouth
agap
,
at
it
sheer
unapologet
lazi
.

Lawrenc
plai
FBI
agent
Malcolm
Turner
,
an
undercov
expert
on
a
stakeout
assign
with
hi
partner
John
-LRB-
Paul
Giamatti
-RRB-
.

Danger
convict
bank
robber
and
murder
Lester
Vesco
-LRB-
Terrenc
Howard
-RRB-
ha
escap
from
prison
,
and
the
Fed
think
he
's
head
for
hi
former
girlfriend
and
presumed-but-never-prov
accomplic
Sherri
-LRB-
Nia
Long
-RRB-
.

Sherri
,
howev
,
ha
fled
with
her
son
Trent
-LRB-
Jascha
Washington
-RRB-
,
possibl
to
visit
her
grandmoth
Hatti
Mae
-LRB-
Ella
Mitchel
-RRB-
,
better
known
as
Big
Momma
.

Indeed
,
Sherri
appear
to
be
on
her
wai
,
but
Big
Momma
is
head
out
of
town
without
know
Sherri
is
come
.

That
leav
master
of
disguis
Malcolm
to
go
under
veri
heavi
cover
as
Big
Momma
and
find
out
what
Sherri
know
.

BIG
MOMMA
'S
HOUSE
's
bloodlin
ar
certainli
traceabl
to
MRS.
DOUBTFIRE
--
director
Raja
Gosnel
edit
that
film
,
and
the
makeup
effect
were
similarli
creat
by
Greg
Cannom
--
but
there
's
just
as
strong
a
whiff
of
TOOTSIE
in
the
main
charact
's
attempt
to
us
hi
altern
ident
to
get
closer
to
a
woman
.

Unfortun
,
BIG
MOMMA
'S
HOUSE
make
a
ridicul
decis
neither
of
those
other
film
made
:
Instead
of
have
the
protagonist
pose
as
a
complet
manufactur
charact
unfamiliar
to
anyon
els
,
it
place
Malcolm
in
the
posit
of
plai
a
friend
and
famili
member
to
sever
other
charact
.

Suspens
of
disbelief
in
BIG
MOMMA
'S
HOUSE
requir
you
to
believ
everi
other
person
in
the
film
is
blind
and/or
stupid
,
sinc
no
on
notic
that
on
Big
Momma
look
or
sound
absolut
noth
like
the
other
.

Of
cours
,
laugh
trump
logic
everi
time
,
and
BIG
MOMMA
'S
HOUSE
probabl
still
would
have
work
in
spite
of
it
utter
disdain
for
common
sens
if
it
had
just
manag
to
be
funni
.

And
it
miss
it
best
possibl
opportun
for
some
great
farc
by
ignor
the
simpl
fact
that
the
real
Big
Momma
is
set
up
as
a
foul-temp
beast
,
while
Malcolm
ha
to
be
nurtur
in
order
to
get
the
inform
he
want
out
of
Sherri
.

Unfortun
,
no
on
involv
appear
to
have
the
faintest
idea
how
to
deal
with
the
comic
gold
mine
involv
in
on
real
person
pretend
to
be
anoth
,
veri
differ
real
person
,
so
thei
fall
back
on
an
endless
parad
of
sight
gag
:
Malcolm
react
violent
to
Big
Momma
's
explos
diarrhea
attack
;
Malcolm-as-Momma
school
a
pair
of
cocki
teen
in
basketbal
;
Malcolm
try
to
avoid
detect
as
variou
prosthes
give
wai
at
inopportun
moment
;
Malcolm
deliv
a
babi
becaus
Big
Momma
is
the
town
midwif
-LRB-
on
of
the
few
sequenc
that
work
-RRB-
.

Martin
Lawrenc
can
be
likeabl
enough
at
time
,
but
there
's
no
reason
to
care
a
whit
about
hi
bud
romanc
with
Sherri
becaus
Malcolm
is
never
an
independ
signific
charact
.

He
's
just
Big
Momma
without
the
makeup
on
.

I
wo
n't
wast
time
comment
on
how
ineptli
the
set-up
of
the
escap
convict
is
emploi
,
sinc
it
wa
clearli
a
wast
of
time
to
the
film-mak
.

There
ar
a
few
token
scene
of
Lester
loom
as
a
threaten
figur
,
but
he
's
ultim
a
distract
in
a
film
that
's
realli
about
it
central
visual
incongru
-LRB-
and
the
accompani
lascivi
glanc
at
Nia
Long
's
posterior
-RRB-
.

I
'm
never
prepar
to
underestim
how
appeal
that
idea
mai
be
to
other
peopl
--
sever
million
of
them
appar
found
Adam
Sandler
as
an
incompet
surrog
parent
appeal
--
but
I
know
that
when
a
film-mak
tri
to
throw
a
concept
at
me
and
pretend
that
it
's
an
entir
film
,
I
duck
out
of
the
wai
.

The
gross
implaus
of
BIG
MOMMA
'S
HOUSE
might
have
been
toler
if
it
wa
season
with
more
big
,
cleverly-construct
laugh
.

It
lack
of
big
laugh
might
have
been
toler
if
it
charact
were
at
all
relev
.

BIG
MOMMA
'S
HOUSE
is
ridicul
_
and
_
not
funni
.

It
's
just
a
sad
exercis
in
the
jade
presumpt
that
ani
scene
should
be
consid
wacki
and
hilari
if
it
involv
Martin
Lawrenc
in
a
fat
suit
and
a
dress
.

