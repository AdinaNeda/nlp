The
Phantom
is
an
unimagin
comic
book
caper
,
that
featur
bad
perform
,
lifeless
direct
,
and
poorli
stage
action
sequenc
.

It
is
a
truli
dissapoint
movi
,
which
is
too
bad
,
becaus
I
realli
like
Kristi
Swanson
.

The
Phantom
is
anoth
duel
ident
super
hero
in
the
line
of
Superman
and
Batman
,
but
the
charact
,
at
least
in
thi
movi
,
ha
none
of
the
intrigu
of
those
charact
.

There
seem
to
be
veri
littl
inner
conflict
in
thi
charact
,
and
he
appear
to
have
hardli
ani
superpow
or
nifti
gadget
.

The
familiar
plot
deal
with
some
businessman
try
to
find
some
kind
of
power
skull
that
will
give
him
some
sort
of
power
.

The
movi
never
make
clear
what
the
skull
will
do
,
or
mayb
it
did
and
I
wa
just
too
bore
to
care
at
the
time
.

The
Phantom
tri
to
stop
thi
and
rescu
a
heiress
plai
by
Swanson
.

There
is
an
attempt
at
a
romanc
between
the
two
,
but
it
seem
forc
,
becaus
Swanson
and
Zane
have
zero
chemistri
togeth
.

Everyth
in
The
Phantom
you
have
seen
mani
time
befor
and
there
is
noth
new
present
here
.

Wincer
displai
absolut
no
skill
in
set
up
an
excit
action
sequenc
.

Billi
Zane
is
wooden
as
the
hero
.

Kristi
Swanson
is
given
veri
littl
to
do
,
and
doe
veri
littl
with
it
.

Treat
William
,
look
like
Rhett
Butler
but
sound
like
Mickei
Mous
,
is
on
of
the
worst
villain
I
have
ever
seen
in
a
movi
.

Only
Catherin
Zeta
Jone
,
as
on
of
William
cohort
turn
in
a
good
perform
.

She
ha
energi
and
spunk
,
which
the
movi
need
much
more
of
.

Oh
yeah
,
the
Phantom
also
ha
a
secret
ident
but
thi
is
so
poorli
plai
out
you
wo
n't
even
care
.

About
the
onli
thing
I
can
recommend
ar
a
good
perform
by
Jone
,
and
some
color
sceneri
.

Howev
,
if
your
look
for
a
fun
famili
movi
,
go
watch
the
underr
Flipper
.

Thi
is
not
a
good
movi
.

