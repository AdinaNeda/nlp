Star
Denzel
Washington
,
John
Hannah
,
Deborah
Kara
Unger
,
Liev
Schreiber
,
Dan
Hedaya
,
Vicel
Reon
Shannon
.

Rate
R
.

Some
movi
'
pre-releas
buzz
is
so
insist
on
their
high
Oscar
potenti
that
when
thei
're
final
releas
,
everyon
just
goe
along
with
it
.

Thu
sometim
,
film
unworthi
of
ani
award
san
the
Razzi
becom
Oscar
hope
and
some
even
score
nomin
.

Last
year
it
wa
The
Thin
Red
Line
.

Thi
year
's
``
winner
''
is
The
Hurrican
,
pure
unadulter
tripe
that
is
all
of
a
sudden
be
receiv
with
critic
plaudit
and
vote
in
the
offic
Oscar
pool
.

Acclaim
director
Norman
Jewison
's
biopic
retread
all
the
convent
of
courtroom
movi
without
ani
of
the
fun
.

And
that
's
suppos
to
be
ok
becaus
,
see
,
it
's
a
true
stori
.

It
's
about
Rubin
``
Hurrican
''
Carter
-LRB-
Denzel
Washington
-RRB-
,
a
legendari
African
American
prizefight
who
is
unjustli
convict
of
tripl
homicid
with
the
help
of
a
ghastli
-LRB-
racist
!
-RRB-

detect
-LRB-
Dan
Hedaya
-RRB-
who
's
had
it
in
for
the
``
big-shot
''
Hurrican
ever
sinc
he
arrest
him
for
stab
a
white
man
with
a
knife
-LRB-
in
self-defens
but
who
care
,
right
?
-RRB-

when
Rubin
wa
onli
ten
.

He
is
to
serv
three
life
sentenc
with
no
possibl
of
parol
.

Most
of
the
film
take
place
when
he
is
in
hi
15th
year
in
prison
.

To
the
rescu
--
Lesra
Martin
-LRB-
Vicel
Reon
Shannon
-RRB-
and
hi
team
of
wanna-b
detect
.

Lesra
is
an
African
American
teenag
taken
in
by
three
well-off
Canadian
white
peopl
-LRB-
Hanna
,
Unger
,
Schreiber
-RRB-
to
get
an
educ
.

Lesra
and
hi
buddi
go
to
a
book
sale
where
Lesra
pick
out
hi
veri
first
book
--
``
The
Sixteenth
Round
,
''
Rubin
's
autobiographi
.

Immediat
thereaft
,
Schreiber
's
charact
tell
Lesra
``
Sometim
we
do
n't
pick
the
book
we
read
,
thei
pick
us
.
''

Hmmm
.

After
a
few
visit
to
the
prison
,
thei
becom
convinc
of
Rubin
's
innoc
and
launch
their
own
full-scal
investig
even
though
two
juri
have
convict
him
.

Thei
meticul
go
through
all
the
file
and
revisit
all
the
old
wit
-LRB-
I
wonder
if
ani
of
them
might
be
cranki
old
women
who
slam
the
door
in
their
face
?
-RRB-

and
in
the
cours
of
their
relentlessi
tediou
sleuth
uncov
obviou
evid
confirm
the
Hurrican
's
innoc
that
wa
either
ignor
or
never
seek
out
dure
the
first
two
proceed
.

Of
cours
,
we
're
alreadi
told
that
he
's
innoc
.

A
movi
like
thi
is
especi
frustrat
becaus
we
're
not
be
shown
everyth
signific
that
happen
to
Carter
dure
thi
period
in
hi
life
.

All
too
obvious
,
we
're
get
the
Hollywood
watered-down
version
.

That
would
be
ok
except
that
the
part
we
ar
get
is
milk
for
everi
singl
drop
of
melodrama
that
the
filmmak
could
possibl
squeez
out
of
it
.

It
's
almost
cruel
.

The
melodrama
is
n't
of
the
entertain
kind
either
;
it
's
the
hokei
,
rammed-down-your-throat
varieti
where
everi
emot
is
exagger
to
the
point
of
absurd
.

Wit
the
villain
feroci
grind
hi
teeth
at
the
final
trial
or
the
constant
pseudo-saintli
of
our
four
protagonist
.

The
Hurrican
is
your
basic
courtroom
movi
except
it
's
a
no-fril
courtroom
movi
.

It
's
formula
strip
to
the
bare
necess
.

You
have
your
wrongli
accus
black
man
.

You
have
your
melodramat
final
courtroom
scene
.

But
there
's
noth
els
.

You
'd
expect
some
sort
of
involv
investig
.

But
that
's
not
necessari
sinc
we
're
implicitli
inform
of
hi
innoc
.

Thu
the
whole
middl
portion
of
the
movi
is
reduc
to
the
protagonist
brood
.

Brood
to
himself
.

Brood
to
other
inmat
.

Brood
to
Lesra
.

Brood
to
Lesra
's
Canadian
friend
.

Brood
in
letter
.

I
do
n't
think
I
'll
ever
look
at
brood
the
same
wai
again
.

It
wa
n't
long
befor
I
got
tire
of
hear
the
Hurrican
's
exceedingli
deep
medit
on
hi
condit
.

I
want
someth
to
happen
.

As
for
Washington
's
perform
in
the
titl
role
--
I
figur
I
'd
have
to
address
it
sooner
or
later
consid
the
amount
of
attent
it
's
receiv
--
he
is
top
notch
,
though
still
bore
.

How
is
that
possibl
?

Well
,
he
doe
what
Jewison
want
him
to
do
perfectli
.

Unfortun
what
Jewison
ask
him
to
do
is
a
load
of
crap
.

What
a
wast
of
a
great
perform
.

Speak
of
wast
,
why
the
hell
did
John
Hannah
agre
to
do
thi
?

He
's
an
extrem
talent
actor
and
I
love
him
dearli
,
but
what
is
he
do
here
?

To
call
hi
charact
-LRB-
along
with
Schreiber
's
and
Unger
's
-RRB-
a
stick
figur
would
be
a
gross
understat
.

All
three
of
them
come
off
as
verit
Mother
Theresa
,
as
benevol
as
do-good
come
.

If
thei
're
to
plai
a
major
part
in
thi
movi
,
why
not
make
them
real
peopl
,
with
real
feel
and
emot
?

Look
:
if
you
're
go
to
make
a
formula
movi
,
do
n't
undermin
the
formula
.

Courtroom
drama
can
be
fun
,
but
thi
is
ridicul
.

Not
onli
is
it
trite
,
it
's
bore
.

