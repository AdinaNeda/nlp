Sonni
Koufax
-LRB-
Adam
Sandler
-RRB-
is
a
rich
,
childish
,
angri
man
who
ha
just
been
dump
by
hi
girlfriend
Vanessa
-LRB-
Kristi
Swanson
-RRB-
.

In
a
bid
to
impress
Vanessa
Sonni
imperson
hi
friend
Kevin
-LRB-
Jon
Stewart
-RRB-
and
adopt
a
5
year
old
boi
name
Julian
-LRB-
Cole
and
Dylan
Sprous
-RRB-
while
Kevin
is
on
an
oversea
trip
.

Under
Sonni
's
supervis
Julian
soon
learn
to
lie
to
women
,
tell
peopl
how
he
``
wipe
hi
ass
,
''
throw
tantrum
,
and
scream
for
hi
``
God
damn
''
treat
.

Self
center
,
Julian
break
a
school
classmat
's
arm
without
apolog
or
even
realiz
he
ha
done
anyth
wrong
.

Meanwhil
Sonni
bribe
Julian
with
sugari
talk
,
food
,
toi
,
and
flashi
promis
in
order
to
get
the
kid
to
perform
.

Not
surprisingli
the
govern
take
Julian
awai
from
Sonni
's
incompet
supervis
,
and
thi
lead
to
a
custodi
battl
.

Opinion
:
It
's
a
movi
about
an
embitt
creep
teach
a
littl
kid
to
be
a
jerk
,
and
we
all
get
cheap
laugh
becaus
for
90
minut
the
innoc
kindergartn
never
find
out
what
it
mean
when
he
mimic
all
the
bad
boi
behavior
.

That
's
the
essenc
of
BIG
DADDY
.

But
there
's
a
bigger
issu
involv
:
market
.

Movi
that
ar
rate
PG
and
PG-13
ar
heavili
market
toward
children
of
preteen
ag
and
below
.

On
TV
,
film
clip
advertis
these
movi
as
family-friendli
hit
comedi
.

Then
when
you
go
see
them
thei
turn
out
to
be
either
raunchi
sex
act
like
Austin
Power
with
charact
name
Fat
Bastard
--
or
BIG
DADDY
where
adult
charact
get
their
jolli
by
buddi
up
to
naiv
five
year
old
and
encourag
them
to
experi
with
drug
and
mistreat
.

Folk
in
Hollywood
ar
try
to
develop
a
preteen
market
for
raunchi
stuff
,
but
I
think
most
American
parent
would
agre
that
earli
childhood
is
a
time
of
mental
innoc
that
should
be
protect
from
uncar
media
market
exploit
.

To
mani
American
mother
there
's
probabl
noth
more
pathet
and
unsettl
than
the
sight
of
a
theater
full
of
unsupervis
littl
eight
year
old
laugh
raucous
as
movi
charact
father
Adam
Sandler
joke
about
a
woman
's
``
ic
cold
tit
.
''

