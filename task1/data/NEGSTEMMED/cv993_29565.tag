Salari
of
Hollywood
top
actor
ar
get
obscen
larg
these
dai
and
mani
find
thi
to
be
the
main
reason
for
skyrocket
movi
budget
.

Actor
who
demand
such
salari
might
be
greedi
,
but
in
some
instanc
thei
ar
quit
justifi
,
becaus
mani
film
would
never
be
watch
or
even
made
without
their
particip
.

Proof
for
that
can
be
found
even
in
the
realm
of
low-budget
movi
,
and
on
fine
exampl
is
BREAKAWAY
,
1995
thriller
direct
by
Sean
Dash
and
star
-LRB-
in
-RRB-
famou
figur
skater
Tonya
Hard
.

Face
of
Tonya
Hard
is
most
promin
featur
on
movi
's
poster
,
but
the
main
star
of
the
film
is
Terri
Thompson
who
plai
Myra
,
attract
woman
who
work
as
a
courier
for
gangster
.

One
dai
she
decid
to
retir
,
but
her
employ
ar
anyth
but
enthusiast
about
that
.

Realis
that
her
life
suddenli
becam
worthless
,
Myra
start
run
for
her
life
,
follow
by
profession
assassin
.

Terri
Thompson
be
the
actual
star
of
the
film
instead
of
Tonya
Hard
becom
quit
understand
after
the
scene
that
featur
former
figur
skater
.

Although
Tonya
Hard
displai
convinc
martial
art
abil
,
her
act
leav
much
to
be
desir
.

On
the
other
hand
,
her
disappoint
effort
ar
hardli
out
of
place
in
the
film
that
lack
origin
,
believ
charact
and
situat
and
actual
repres
anyth
that
gave
B-film
a
bad
name
.

Martin
Sheen
's
brother
Joe
Estevez
,
whose
charact
look
like
he
had
enter
from
anoth
movi
'
set
,
is
the
onli
bright
spot
of
BREAKAWAY
.

Unfortun
,
he
appear
in
thi
film
too
littl
too
late
to
prevent
viewer
from
realis
why
Tonya
Hard
's
silver
screen
debut
prove
to
be
her
last
film
.

