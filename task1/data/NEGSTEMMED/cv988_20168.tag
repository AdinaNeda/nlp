For
more
review
and
movi
screensav
,
visit
Thi
movi
is
written
by
the
man
who
is
deem
to
be
``
on
of
the
hottest
writer
in
Hollywood
''
.

He
wrote
the
groundbreak
screenplai
for
SCREAM
,
then
ad
the
success
I
KNOW
WHAT
YOU
DID
LAST
SUMMER
script
to
hi
mix
,
and
also
creat
the
popular
TV
seri
``
Dawson
's
Creek
''
.

So
when
he
ask
to
direct
hi
first
movi
,
base
on
hi
first
ever
script
written
,
everyon
and
their
grandma
said
``
Sure
,
go
for
it
!
''
.

Uhhm
,
my
question
is
...
did
anyon
bother
read
thi
stupid
script
???
Ace
student
Leigh
Ann
Watson
is
mistakenli
caught
with
some
cheat
paper
by
the
bitchiest
teacher
in
the
West
,
Mrs.
Tingl
,
and
set
to
lose
her
scholarship
to
Colleg
.

When
she
and
her
friend
visit
the
teacher
at
home
in
order
to
explain
their
side
of
the
stori
,
thei
end
up
ty
her
up
,
and
slowli
try
to
talk
some
sens
into
the
hardhead
woman
.

It
's
not
so
much
that
thi
is
a
bad
movi
,
than
the
fact
that
it
suck
.

Thi
movi
is
chock-ful
of
one-dimension
charact
,
contain
no
actual
humor
that
I
wa
abl
to
zone
in
on
,
zero
tension
or
thrill
,
plot
hole
the
size
of
my
big
ass
,
lame
pop
tune
plai
to
mask
noth
go
on
in
the
movi
,
and
Molli
Ringwald
,
Vivica
A.
Fox
and
Leslei
Anne
Warren
toss
awai
in
throwawai
role
.

I
wa
prime
for
thi
film
as
it
interest
premis
had
me
think
MISERY
and
9
TO
5
,
but
unfortun
for
Williamson
,
he
went
nowher
with
the
bright
foundat
,
make
refer
to
THE
EXORCIST
and
DR.
ZHIVAGO
,
which
I
doubt
mani
in
hi
target
market
will
appreci
.

He
also
forgot
about
suspens
,
with
all
of
hi
charact
base
on
incomprehens
decis
,
unbeliev
motiv
and
simpli
bore
us
with
all
of
their
trite
dialogu
.

Sure
,
Kati
Holm
is
cute
,
and
her
co-star
,
Marisa
Coughlan
,
did
a
reason
amus
impress
of
THE
EXORCIST
-LRB-
Worth
two
point
out
of
my
three
on
ten
-RRB-
,
but
how
can
we
forgiv
the
biggest
one-dimension
charact
in
ani
film
,
Holm
'
rival
in
the
film
,
Mrs.
Tingl
's
complet
unprofession
be
let
go
by
all
other
around
her
-LRB-
Are
teacher
allow
to
behav
that
wai
nowadai
?
-RRB-

and
a
transpar
romanc
between
Holm
and
some
long-hair
dude
,
hire
to
be
the
poor
man
's
version
of
Skeet
Ulrich
-LRB-
Who
himself
is
a
poor
man
's
version
of
Johnni
Depp
!
-RRB-
.

All
in
all
,
thi
movi
wa
laughabl
for
me
,
provid
me
with
no
insight
into
anyth
,
demonstr
Williamson
's
genuin
lack
of
directori
skill
alongsid
a
juvenil
script
,
and
provid
Helen
Mirren
with
a
great
role
to
chew
into
,
unfortun
forget
to
give
her
charact
ani
believ
,
human
or
capac
to
comprehend
.

No
hip
line
,
no
cheap
thrill
,
just
a
dull
time
at
the
movi
theater
.

If
you
want
to
see
a
funni
teenag
movi
,
go
see
DETROIT
ROCK
CITY
,
and
thank
my
drunken
,
sorri
ass
in
the
morn
.

Littl
Known
Fact
about
thi
film
and
it
star
:
Kevin
Williamson
's
father
wa
a
fisherman
.

Kevin
us
hi
knowledg
of
fish
hook
and
winch
when
creat
the
killer
in
I
KNOW
WHAT
YOU
DID
LAST
SUMMER
.

He
is
also
a
huge
fan
of
Steven
Spielberg
and
coincident
,
so
is
Dawson
Leeri
on
TV
's
``
Dawson
's
Creek
''
,
a
show
Kevin
creat
.

He
wa
onc
an
aspir
actor
.

In
fact
,
he
even
land
a
bit
part
on
TV
's
``
Another
World
''
.

Also
,
Williamson
ha
gone
on
record
to
sai
that
an
unsupport
English
teacher
who
onc
told
him
that
he
would
never
amount
to
anyth
wa
the
inspir
for
Mrs.
Tingl
,
which
is
also
loos
base
on
the
book
``
Kill
Mr.
Griffin
''
by
the
writer
of
I
KNOW
WHAT
YOU
DID
LAST
SUMMER
,
Loi
Duncan
.

He
ha
also
come
out
and
said
that
he
is
a
gai
man
.

Helen
Mirren
wa
born
in
London
,
England
under
the
name
Ilynea
Lydia
Mironoff
.

She
is
marri
to
director
Taylor
Hackford
,
whose
work
includ
DEVIL
'S
ADVOCATE
and
AN
OFFICER
AND
A
GENTLEMAN
.

Thi
film
wa
origin
titl
KILLING
MRS.
TINGLE
,
but
wa
chang
after
the
Columbin
high
school
shoot
incid
.

Actress
Marisa
Coughlan
will
star
in
Kevin
Williamson
next
TV
project
call
``
Wasteland
''
.

Thi
is
actor
Barri
Watson
first
full
featur
film
.

He
ha
plai
the
charact
of
``
Seth
''
on
TV
's
``
Malibu
Shore
''
sever
time
.

