Deep
Impact
DEEP
IMPACT
begin
the
offici
summer
movi
season
,
and
it
also
bring
back
memori
of
1997
.

Rememb
when
DANTE
'S
PEAK
came
out
in
Februari
?

A
few
month
later
,
VOLCANO
wa
releas
.

The
first
film
wa
smart
,
exhilir
,
and
on
of
the
best
disast
film
I
had
ever
seen
.

The
latter
film
wa
an
incohes
mess
that
defi
logic
and
wast
talent
.

Well
,
it
's
deja
vu
all
over
again
as
two
disast
film
go
head
to
head
in
competit
.

Thi
time
,
unfortun
,
the
first
comet
flick
is
so
bad
that
peopl
mai
shy
awai
from
ARMAGEDDON
,
the
upcom
comet-disast
to
be
releas
the
begin
of
Juli
.

Of
cours
,
the
gener
reaction
of
the
audienc
wa
opppos
to
mine
,
and
so
I
am
in
the
minor
,
as
I
wa
when
I
stood
on
the
side
of
DANTE
'S
PEAK
.

But
while
watch
DEEP
IMPACT
,
I
began
to
wonder
how
anyon
in
their
right
mind
could
actual
like
thi
film
.

Apparent
mani
did
,
and
it
utterli
baffl
me
.

To
be
complet
honest
,
I
have
n't
had
thi
littl
fun
watch
a
disast
film
in
my
entir
life
.

VOLCANO
had
implaus
up
the
wazoo
,
but
it
wa
still
rather
fun
to
watch
.

DEEP
IMPACT
doe
n't
just
have
implaus
,
it
also
contain
cheap
human
drama
,
incred
horribl
special
effect
,
and
a
poorli
construct
plot
.

The
onli
thing
that
give
it
a
half
star
abov
my
bottom
rank
is
a
slightli
entertain
final
fifteen
minut
and
some
good
actor
make
the
most
of
their
charact
.

DEEP
IMPACT
begin
in
an
unnam
year
-LRB-
the
year
vari
;
advanc
technolog
set
it
in
the
futur
,
but
FIRE
IN
THE
SKY
is
show
on
a
local
movi
theater
,
push
it
back
to
1993
-RRB-
.

A
line
of
student
is
outsid
at
night
,
peer
through
telescop
at
the
dark
sky
abov
.

Among
these
ar
Leo
Biederman
-LRB-
Elijah
Wood
-RRB-
and
Sarah
Hotchner
-LRB-
Leele
Sobieski
-RRB-
.

Leo
unknowingli
discov
a
comet
,
and
hi
teacher
send
a
photo
of
the
unknown
object
to
an
astronom
,
who
then
is
abl
to
determin
the
correct
path
of
thi
distant
comet
in
about
a
few
second
.

He
race
off
to
mail
the
inform
,
but
is
kill
in
a
reckless
car
accid
.

A
year
pass
,
and
noth
is
heard
about
it
again
.

We
ar
then
introduc
to
Jenni
Lerner
-LRB-
Ta
Leoni
-RRB-
,
a
report
for
MSNBC
.

She
get
hand
a
job
to
investig
a
possibl
cover-up
in
the
govern
involv
Senat
Alan
Rittenhous
-LRB-
Jame
Cromwel
-RRB-
.

She
talk
to
a
woman
who
mention
that
Rittenhous
wa
have
an
affair
with
a
girl
name
Ellie
.

After
talk
with
Rittenhous
,
and
unsatisfi
with
the
inform
she
get
,
she
decid
to
us
the
Internet
for
help
.

Luckili
,
she
know
exactli
how
to
spell
the
certain
``
Ellie
''
that
she
is
look
for
-LRB-
thei
spell
it
ELE
in
the
film
...
that
girl
is
pretti
darn
smart
for
guess
how
it
wa
spell
-RRB-
.

Befor
she
can
us
the
inform
,
the
govern
decid
to
push
her
car
off
the
road
.

Thei
take
her
in
to
meet
the
Presid
of
the
Unite
State
,
Presid
Beck
-LRB-
Morgan
Freeman
-RRB-
.

Beck
recommend
that
Jenni
keep
the
inform
secret
for
48
hour
so
thei
can
confirm
it
and
then
hold
a
press
confer
.

Natur
,
she
want
to
be
compens
,
and
thei
offer
her
a
front
row
seat
and
the
chanc
for
the
first
question
.

And
so
,
yada
yada
yada
,
thei
reveal
the
comet
to
the
public
and
their
plan
:
send
a
massiv
spacecraft
out
to
destroi
it
befor
it
can
arriv
.

Thei
announc
a
plan
call
``
ARK
,
''
which
is
their
onli
hope
for
surviv
.

A
comput
will
select
800,000
peopl
at
random
.

These
peopl
ar
the
on
who
will
go
into
a
larg
cave
underground
so
that
the
impact
of
the
comet
wo
n't
kill
off
the
entir
human
race
.

After
two
year
,
the
dust
will
settl
-LRB-
actual
,
it
would
take
much
,
much
longer
--
approxim
nineti
year
,
from
what
I
understand
-RRB-
and
the
human
could
come
back
to
the
surfac
and
start
over
.

The
rest
of
the
plot
is
your
standard
disast
film
procedur
,
but
there
is
on
subplot
worth
mention
.

Jenni
and
her
father
,
Jason
-LRB-
Maximilian
Schell
-RRB-
,
have
a
veri
touch
relationship
that
form
out
of
the
impend
doom
.

The
final
moment
involv
the
two
charact
is
heartfelt
and
emot
.

It
's
a
shame
that
noth
els
is
heartfelt
.

Now
,
of
cours
,
we
all
know
that
the
comet
doe
impact
the
surfac
.

The
titl
alon
suggest
it
,
and
the
preview
actual
show
it
!

By
do
thi
,
absoltu
no
tension
can
be
drawn
from
ani
attempt
to
stop
the
comet
becaus
we
all
know
that
it
wo
n't
work
.

Director
Mimi
Leder
came
from
her
success
tri
at
direct
with
episod
of
the
hit
televis
show
``
ER
.
''

Her
major
film
debut
wa
THE
PEACEMAKER
,
a
pathet
and
heartless
action
film
.

Well
,
thi
time
Leder
outdid
herself
,
creat
a
film
wors
than
that
on
.

Suggest
to
Ms.
Leder
:
Pleas
,
stai
awai
from
the
big
screen
,
or
at
least
the
action
genr
.

Much
of
the
blame
can
be
place
on
Leder
directli
,
becaus
the
pace
is
disastr
off
.

Throughout
the
film
we
ar
given
subtitl
that
tell
us
how
much
time
ha
pass
-LRB-
it
goe
from
month
to
week
to
hour
-RRB-
.

It
liter
feel
like
thi
laps
time
is
take
place
in
real-tim
--
it
's
that
bore
.

Of
cours
,
Leder
is
n't
all
to
blame
for
it
.

Screenwrit
Michael
Tolkin
and
Bruce
Joel
Rubin
have
craft
a
simplist
stori
that
onli
get
wors
with
time
.

What
start
out
promis
soon
turn
deadli
-LRB-
for
the
audienc
anywai
-RRB-
.

Chock
full
of
cheesi
on
liner
-LRB-
``
You
know
,
you
ar
go
to
have
more
sex
than
anyon
els
in
school
!
''
-RRB-

and
stupid
charact
,
you
might
think
we
were
back
in
the
70
again
.

Only
on
of
the
subplot
is
remot
interest
,
while
the
rest
ar
forgett
and
bore
.

And
the
main
plot
is
so
outrag
that
you
ca
n't
figur
out
if
thi
film
is
suppos
to
be
an
action
,
drama
,
or
sci-fi
.

To
put
it
simpli
:
the
special
effect
of
thi
film
ar
a
hit
and
miss
situat
.

That
's
right
,
80
%
miss
,
and
20
%
hit
.

Scene
abov
the
Earth
ar
well
done
,
and
the
orbit
ship
is
majest
.

But
the
comet
is
a
huge
mistak
,
make
it
more
laughabl
than
frighten
.

The
concept
of
even
try
to
land
on
a
comet
is
preposter
enough
,
but
that
's
forgiv
.

What
is
n't
forgiv
is
actual
have
human
walk
on
the
surfac
.

Give
me
a
break
,
will
ya
?

And
of
cours
,
the
much
hype
collis
of
comet
and
Earth
.

Well
,
it
is
far
from
spectacular
,
and
it
make
INDEPENDENCE
DAY
look
brilliant
.

The
water
rush
toward
the
land
is
effect
,
but
onc
it
hit
the
contin
,
the
effect
turn
ridicul
.

CGI
water
is
us
,
and
it
look
so
bad
that
I
heard
more
laugh
from
the
audienc
than
shriek
.

In
fact
,
I
mai
even
recommend
the
film
for
those
who
want
to
see
how
bad
effect
can
actual
get
these
dai
.

Just
when
we
think
visual
effect
ca
n't
be
improv
,
along
come
a
film
to
show
that
thei
realli
can
and
should
be
.

One
thing
ha
struck
a
wrong
note
with
me
concern
Mimi
Leder
's
direct
,
which
also
influenc
the
actor
.

Leder
love
to
show
the
actor
'
face
befor
and
dure
moment
of
terror
.

Thi
remind
me
of
anoth
action
director
,
Renni
Harlin
-LRB-
DIE
HARD
2
,
CLIFFHANGER
-RRB-
.

But
Harlin
succe
becaus
he
show
the
face
of
the
victim
in
realist
situat
.

Leder
like
to
flaunt
peopl
's
fear
via
their
face
,
but
instead
of
come
off
as
sympathet
,
Leder
seem
more
of
a
Sadist
.

One
moment
ha
an
astronaut
fly
off
into
space
.

Thi
should
be
enough
to
warrant
a
respons
from
the
audienc
,
but
Leder
want
to
go
farther
and
show
us
the
person
's
face
while
drift
into
space
.

Thi
is
on
of
the
cheapest
wai
to
ellicit
an
emot
respons
from
an
audienc
,
and
I
,
for
on
,
am
not
go
to
be
fool
.

On
anoth
note
,
Leder
's
film
onli
pick
up
it
pace
dure
the
last
fifteen
minut
when
the
comet
actual
impact
.

The
pace
doe
pick
up
,
and
some
veri
emot
moment
ar
shown
.

Then
again
,
when
you
watch
peopl
cry
for
their
love
on
,
it
's
obviou
it
will
be
emot
.

It
's
mostli
just
a
big
trick
to
rope
viewer
into
``
feel
''
for
the
charact
,
but
it
did
n't
work
for
me
.

But
anywai
,
the
actor
do
as
much
as
thei
can
with
what
thei
ar
given
.

Ta
Leoni
-LRB-
BAD
BOYS
,
TV
's
``
The
Nake
Truth
''
-RRB-
is
the
best
of
the
film
,
and
she
is
given
the
meatiest
role
.

Her
charact
is
made
stronger
by
Leoni
's
presenc
,
and
we
grow
to
care
for
her
.

Robert
Duval
is
energet
and
fun
to
watch
,
but
hi
charact
is
turn
into
shred
by
the
plot
.

Elijah
Wood
also
come
off
rather
successfulli
,
but
he
still
ha
n't
had
mani
good
role
-LRB-
Wood
,
stick
to
drama
!

You
ar
too
talent
for
thi
stuff
-RRB-
.

Vanessa
Redgrav
is
bare
acknowledg
,
and
her
perform
is
onli
enhanc
by
her
strong
presenc
on
screen
.

Maximilian
Schell
is
distract
,
but
he
doe
provid
some
nice
humor
.

Morgan
Freeman
ha
been
infinit
better
than
thi
,
and
give
on
of
hi
most
shallow
perform
to
date
-LRB-
which
is
quit
remark
for
him
-RRB-
.

Leele
Sobieski
could
have
been
better
,
but
I
think
she
just
suffer
from
a
poorli
written
charact
.

A
special
note
should
go
to
Ron
Eldard
and
Denis
Crosbi
.

Eldard
is
good
in
hi
role
,
but
is
limit
by
the
plot
.

Crosbi
is
special
to
me
person
becaus
she
wa
Tasha
Yar
from
TV
's
``
Star
Trek
:
The
Next
Gener
.
''

See
her
wa
on
of
the
highlight
of
the
film
.

Overal
,
a
veri
talent
cast
virtual
wast
.

DEEP
IMPACT
is
rate
PG-13
for
disast
relat
element
and
brief
languag
.

Thi
is
on
of
the
worst
film
of
the
year
,
and
if
it
is
ani
omen
of
thing
to
come
,
thi
summer
could
be
on
of
the
worst
ever
.

Luckili
,
THE
X-FILES
MOVIE
is
come
up
,
and
hopefulli
ARMAGEDDON
will
be
more
success
.

It
's
a
shame
that
thi
film
will
do
so
successfulli
becaus
it
just
is
n't
worth
much
.

Cost
nearli
$
75
million
,
with
special
effect
done
by
the
illustri
ILM
-LRB-
which
is
a
huge
shocker
-RRB-
,
and
with
a
score
compos
by
Oscar-winn
Jame
Horner
-LRB-
TITANIC
-RRB-
,
on
might
have
expect
thi
to
be
more
fun
to
wit
and
experi
.

Well
,
it
's
not
.

When
the
comet
doe
hit
the
Earth
,
you
almost
wish
it
could
just
take
thi
film
along
with
it
.

