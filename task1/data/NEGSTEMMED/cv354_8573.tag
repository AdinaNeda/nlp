Wow
,
a
film
without
ani
redeem
qualiti
whatsoev
.

I
'm
amaz
that
someon
thought
thi
wa
a
stori
that
must
be
told
on
screen
.

Mani
black
in
Hollywood
complain
that
thei
ar
not
nomin
for
award
base
on
their
race
.

I
think
first
thei
need
to
concentr
their
energi
on
themselv
,
and
stop
make
movi
which
make
them
look
like
noth
more
than
sex-craz
buffoon
.

Even
I
'm
offend
by
it
.

Bill
Bellami
is
a
``
player
''
which
mean
he
sleep
with
a
lot
of
women
and
li
to
them
.

What
a
pleasant
main
charact
.

One
dai
,
hi
friend
decid
that
thei
want
to
find
out
how
to
be
a
player
.

Why
thei
decid
thi
``
on
dai
''
I
have
no
idea
.

How
mani
year
have
thei
been
friend
and
why
all
of
a
sudden
would
thei
want
to
learn
?

Anywai
,
Bellami
agre
to
teach
,
and
hi
wonder
lesson
to
hi
friend
consist
of
let
them
ride
in
hi
car
while
he
ride
from
hous
to
hous
have
sex
with
women
.

Thi
is
the
bulk
of
the
film
folk
,
gui
ride
from
hous
to
hous
,
on
keep
get
out
to
have
sex
,
while
the
other
sit
in
the
car
.

Sinc
Russel
Simmon
is
produc
,
and
sinc
the
offici
titl
of
the
film
is
``
Def
Jam
's
How
To
Be
A
Player
''
,
I
wa
readi
to
be
jump
up
and
down
and
stomp
my
feet
and
beat
the
person
next
to
me
becaus
of
the
comedi
.

Amazingli
,
asid
from
Gilbert
Gottfri
`
ssecond
of
screen
time
,
there
wa
no
comedi
in
the
film
for
me
to
express
myself
in
such
a
manner
.

Instead
of
joke
,
there
's
just
an
endless
stream
of
profan
and
nake
breast
-LRB-
and
nake
breast
ar
n't
so
bad
,
but
nake
breast
alon
do
n't
make
a
good
movi
-RRB-
.

Be
sure
to
bring
along
your
ebon
to
english
dictionari
as
well
.

Gilbert
Gottfri
's
charact
serv
as
on
in
hi
veri
brief
appear
,
ask
for
explan
as
to
what
is
be
said
.

Thei
should
have
brought
him
along
for
the
entir
film
.

