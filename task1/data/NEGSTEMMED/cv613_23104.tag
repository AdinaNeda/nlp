``
Be
gentl
,
''
urg
Natasha
Henstridg
to
Matthew
Perri
in
``
The
Whole
Nine
Yard
.
''

``
I
have
n't
made
love
in
five
year
.
''

``
Neither
have
I
,
''
rebut
Perri
.

``
I
'm
marri
!
''

If
Jonathan
Lynn
's
latest
comedi
reli
simpli
on
joke
of
that
calib
--
and
it
certainli
tri
--
then
it
'd
be
an
innocu
if
rather
obviou
littl
film
.

Instead
,
it
fail
go
much
deeper
.

First
off
,
who
ha
n't
had
enough
of
the
tough
wisegui
intimid
the
timid
wise
gui
genr
?

If
you
want
to
make
a
pretti
penni
or
two
in
Hollywood
nowadai
simpli
write
a
``
comedi
''
which
pair
a
Robert
De
Niro/Jam
Caan/Bruc
Willi
type
with
a
Billi
Crystal/Hugh
Grant/Matthew
Perri
type
and
wait
for
the
royalti
to
roll
in
.

Who
's
next
?

Jack
Nicholson
and
Martin
Short
?

It
certainli
doe
n't
have
to
be
funni
.

``
The
Whole
Nine
Yard
''
is
not
a
particularli
funni
film
,
but
it
is
borderlin
offens
.

Offensiv
in
the
wai
it
continu
a
trend
of
poke
fun
at
career
crimin
who
would
n't
think
twice
about
push
your
mother-in-law
off
the
Brooklyn
Bridg
with
her
feet
encas
in
concret
.

That
might
sound
like
a
funni
sight
gag
,
but
the
problem
is
these
film
have
long
sinc
lost
sight
of
the
fact
that
take
a
human
life
is
n't
all
that
funni
to
begin
with
.

When
the
corps
is
place
in
a
car
,
dous
with
gasolin
,
set
ablaz
and
refer
to
as
``
barbecu
,
''
it
make
you
wonder
when
all
thi
plai
kill
for
laugh
is
go
to
end
.

``
The
Whole
Nine
Yard
''
is
also
offens
in
so
much
as
it
three
central
femal
charact
ar
portrai
as
noth
more
than
sex
object
.

Rosanna
Arquett
plai
a
slutti
,
chain-smok
French
Canadian
who
's
marri
to
Matthew
Perri
's
none-too-success
dentist
.

It
's
an
unflatt
role
made
all
the
more
so
by
the
unflatt
outfit
--
and
unflatt
situat
--
into
which
Arquett
is
thrust
.

Then
there
's
Natasha
Henstridg
-LRB-
``
Speci
''
I
and
II
-RRB-
who
plai
the
well-to-do
wife
of
contract
killer
Bruce
Willi
.

She
get
involv
with
Perri
's
charact
when
he
come
to
Chicago
hope
to
negoti
a
finder
's
fee
with
mob
boss
Yanni
Gogolack
-LRB-
Kevin
Pollack
,
transpos
hi
Vs
with
hi
Ws
-RRB-
after
Willi
'
Jimmi
the
Tulip
Tudeski
move
in
next
door
.

Henstridg
and
Perri
's
charact
hit
it
off
is
about
as
like
as
Bruce
and
Demi
get
back
togeth
.

Perri
might
have
the
charm
but
he
doe
n't
have
the
physic
attribut
of
a
tradit
lead
man
,
especi
in
close-up
.

Most
objectifi
of
the
bunch
is
Amanda
Peet
,
who
turn
in
a
sexually-rip
perform
as
Perri
's
dental
assist
with
,
it
transpir
,
question
career
goal
.

Peet
's
gratuit
nude
scene
prove
how
low
thi
movi
will
stoop
to
keep
it
audienc
from
drop
off
.

When
``
My
Cousin
Vinni
''
is
the
highpoint
of
a
directori
career
includ
such
forgett
film
as
``
Clue
,
''
``
Greedi
,
''
and
``
Sgt.
Bilko
,
''
you
have
to
wonder
if
Lynn
chose
the
wrong
career
path
.

Perri
's
pratfal
goofi
coupl
with
Willi
'
likabl
hard
could
have
had
some
potenti
but
with
no
script
to
work
with
,
and
a
director
who
seem
to
be
watch
from
the
wing
,
their
charact
run
out
of
ga
quickli
.

Pleas
--
no
more
mob
comedi
.

No
more
bad
on
like
thi
,
at
least
.

