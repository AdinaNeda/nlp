Although
I
had
not
been
a
viewer
of
the
``
Rugrat
''
televis
seri
,
I
went
into
their
first
anim
featur
film
,
``
The
Rugrat
Movi
,
''
with
a
posit
attitud
.

The
trailer
look
cute
enough
,
after
all
.

After
see
it
,
I
think
the
word
in
my
recent
``
Antz
''
review
,
in
which
I
state
that
it
wa
the
worst
film
of
it
type
sinc
1995
's
``
The
Pebbl
and
the
Penguin
,
''
were
a
bit
prematur
.

``
The
Rugrat
Movi
,
''
is
bottom-of-the-barrel
children
's
fare
at
it
worst
,
and
start
to
make
,
``
Antz
''
look
good
in
comparison
.

As
in
the
show
,
``
The
Rugrat
Movi
,
''
is
about
a
group
of
veri
littl
friend
,
rang
from
babi
to
a
three-year-old
.

The
head
of
the
group
,
Tommi
Pickl
-LRB-
E.G.
Daili
-RRB-
,
becom
distraught
when
hi
mother
ha
a
newborn
babi
name
Dill
-LRB-
get
it
?

Dill
Pickl
?

...
hardee-har-har
!
-RRB-

and
is
inform
by
hi
three-year-old
cousin
,
Anjelica
-LRB-
Cheryl
Chase
-RRB-
,
that
the
new
babi
alwai
take
all
of
the
attent
awai
from
the
other
children
.

When
the
other
children
,
Chucki
,
and
twin
Lil
and
Phil
,
suggest
take
Dill
back
to
the
hospit
,
Tommi
goe
along
with
it
,
but
on
their
wai
there
,
thei
crash
in
the
forest
,
and
becom
lost
,
run
into
wolv
and
circu
monkei
,
among
other
thing
.

``
The
Rugrat
Movi
,
''
judg
from
the
idea
of
be
lost
in
the
wood
,
could
have
been
a
potenti
fun
famili
film
,
and
there
were
mani
differ
thing
that
could
have
been
done
with
the
stori
.

So
what
did
the
writer
choos
to
do
?

Why
,
thei
set
up
a
protract
,
clich
scene
where
the
children
almost
go
over
a
waterfal
,
of
cours
!

For
the
youngest
of
children
-LRB-
ag
3-7
-RRB-
,
``
The
Rugrat
Movi
,
''
mai
veri
well
entertain
them
,
judg
from
the
audienc
I
saw
thi
with
.

Of
cours
,
if
you
look
around
at
all
of
the
older
kid
and
their
parent
,
thei
were
all
desper
struggl
to
stai
awak
,
and
that
includ
me
.

For
an
adult
,
``
The
Rugrat
Movi
,
''
is
a
piec
of
garbag
.

The
plotlin
is
unorigin
and
the
write
ha
absolut
no
wit
or
charm
.

There
is
n't
on
laugh
to
be
had
in
the
film
,
nor
is
there
ani
excit
.

If
anyth
is
even
margin
good
in
the
pictur
,
it
is
the
bright
anim
style
,
so
it
is
especi
unfortun
that
it
wa
n't
to
servic
a
more
qualiti
film
.

``
The
Rugrat
Movi
''
is
DOA
from
the
start
.

I
am
all
for
a
worthwhil
famili
movi
,
but
sometim
an
anim
film
come
along
that
is
simpli
aw
.

As
said
befor
,
young
children
mai
like
it
,
but
even
thei
deserv
better
than
thi
.

For
adult
,
it
is
a
nearli
unbear
,
excruci
chore
to
sit
through
.

As
for
me
,
``
The
Rugrat
Movi
,
''
is
not
the
worst
of
the
year
,
nor
is
it
the
most
deepli
hate
,
but
it
is
the
most
bore
.

Parent
:
do
yourselv
a
favor
and
take
your
kid
to
see
the
rereleas
of
,
``
The
Wizard
of
Oz
.
''

That
is
a
pictur
that
contain
a
great
deal
of
magic
and
wonder
,
two
thing
of
which
,
``
The
Rugrat
Movi
,
''
is
complet
miss
.

