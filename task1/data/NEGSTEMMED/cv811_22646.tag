Star
Liam
Neeson
;
Catherin
Zeta-Jon
;
Owen
Wilson
;
Lili
Taylor
&
Bruce
Dern
When
The
Haunt
arriv
in
theater
,
all
I
kept
hear
about
wa
the
overdon
special
effect
and
the
fact
that
veri
often
the
unseen
bump
in
the
night
in
a
horror
film
ar
far
scarier
than
those
that
you
can
put
a
face
to
courtesi
of
special
effect
.

While
I
agre
that
thi
remak
of
The
Haunt
goe
a
bit
overboard
in
the
visual
effect
depart
,
I
do
n't
think
that
thei
ar
complet
to
blame
for
thi
movi
's
failur
.

It
appear
that
some
peopl
have
fail
to
take
into
account
that
the
origin
Haunt
had
the
``
unseen
''
terror
,
and
it
wa
about
as
scari
as
a
dust
bunni
.

So
special
effect
or
not
,
if
the
stori
is
n't
the
least
bit
scari
,
you
ar
n't
go
to
end
up
with
a
veri
frighten
movi
.

The
thing
that
interest
me
most
about
thi
movi
wa
the
caretak
of
thi
build
plai
by
Bruce
Dern
.

Dern
is
alwai
great
,
and
even
though
he
mai
have
had
onli
about
3
minut
of
screen
time
he
wa
still
the
most
interest
element
of
the
movi
.

As
I
sat
through
the
seemingli
endless
,
albeit
fairli
impress
,
special
effect
,
I
kept
wish
that
thi
movi
wa
about
Dern
's
caretak
and
not
the
on
dimension
charact
that
popul
the
cast
.

Never
a
good
sign
when
a
bit
player
is
the
best
part
of
the
movi
.

Liam
Neeson
plai
a
scientist
who
is
conduct
experi
on
fear
.

He
decid
the
best
wai
to
get
result
is
to
trick
a
group
of
fairli
unstabl
individu
to
spend
a
few
dai
in
a
haunt
mansion
.

He
trick
them
into
particip
by
let
on
that
he
is
conduct
an
experi
on
insomnia
.

And
he
also
fail
to
mention
that
the
mansion
ha
a
reput
for
strang
goings-on
.

Catherin
Zeta-Jon
,
Lili
Taylor
and
Owen
Wilson
plai
hi
subject
.

Like
the
origin
,
Taylor
's
charact
is
the
star
of
the
movi
.

But
sinc
she
doe
n't
quit
have
the
marque
power
of
Liam
Neeson
and
Ms.
Zeta-Jon
,
their
part
did
seem
to
be
a
big
larger
than
in
the
origin
.

And
let
's
face
it
,
probabl
half
the
peopl
who
see
thi
movi
will
do
so
becaus
of
Zeta-Jon
.

I
'll
admit
that
I
would
have
never
seen
thi
thing
if
she
were
n't
in
it
.

But
the
real
star
here
is
the
special
effect
combin
with
the
fairli
overdon
set
.

Thei
take
over
the
movi
as
the
supernatur
element
of
the
hous
start
to
interact
with
our
hapless
insomnia
patient
.

There
realli
is
n't
much
of
a
stori
here
.

Just
endless
setup
so
director
Jan
de
Bont
can
showcas
all
the
nifti
special
effect
that
he
got
to
plai
with
.

And
the
special
effect
ar
great
.

In
mani
case
thei
ar
as
good
as
you
ar
like
to
see
anywher
.

In
other
case
thei
ar
overdon
and
obvious
thrown
in
just
for
the
sake
of
hit
the
``
cool
shot
''
quota
.

At
no
point
in
the
movi
do
ani
of
these
thing
ever
come
close
to
be
scari
--
funni
,
mayb
.

But
not
scari
.

Then
we
have
the
set
.

When
I
first
saw
the
hous
,
I
wa
veri
impress
with
the
veri
cool
gothic
look
about
it
.

But
it
onli
took
a
short
tour
by
the
charact
around
the
place
to
see
that
the
set
design
obvious
had
as
much
monei
to
burn
as
the
visual
effect
peopl
did
,
and
decid
to
take
the
idiot
overdon
rout
.

Thi
includ
a
flood
hallwai
with
book
as
step
stone
and
a
mirror
circular
room
that
revolv
.

What
part
did
these
room
plai
in
the
stori
?

Absolut
none
.

Thei
were
just
there
to
take
our
mind
off
the
fact
that
there
wa
neither
a
descent
stori
nor
a
singl
scare
in
the
entir
movi
.

Then
we
have
the
actor
.

Lili
Taylor
ha
never
been
on
of
my
favorit
.

And
when
the
fact
that
her
charact
is
mousi
and
pathet
is
factor
in
,
she
come
in
around
the
averag
or
slightli
below
mark
.

I
have
no
idea
why
Liam
Neeson
took
thi
role
.

He
basic
remind
me
of
the
ringmast
at
an
out
of
control
circu
.

Hi
charact
wa
in
charg
of
thi
farc
but
it
quickli
got
awai
from
him
.

I
have
no
doubt
that
Liam
will
want
to
lock
all
print
of
thi
movi
in
a
veri
secur
vault
along
with
all
copi
of
Darkman
.

Zeta-Jon
wa
cast
becaus
she
is
too
hot
for
word
.

The
fact
that
her
charact
is
bi-sexu
is
just
ic
on
the
cake
.

All
Catherin
ha
to
do
in
thi
movi
is
look
good
.

Fortun
that
is
someth
she
doe
veri
well
.

While
she
doe
have
a
fairli
good
size
part
,
it
is
obviou
that
her
onli
purpos
in
the
movi
wa
as
eye-candi
.

It
's
too
bad
someon
of
her
talent
wast
them
here
.

Any
random
supermodel
pull
out
of
a
fashion
show
could
have
easili
fill
her
role
.

The
Haunt
is
the
antithesi
of
anoth
of
1999
's
horror
movi
,
The
Blair
Witch
Project
.

The
Haunt
had
a
seemingli
limitless
effect
budget
,
while
Blair
Witch
reli
on
pile
of
rock
for
it
scare
.

Both
prove
quit
nice
that
special
effect
ar
irrelev
to
a
horror
film
.

If
the
stori
suck
,
it
's
all
downhil
from
there
.

My
advic
?

If
you
ar
look
for
special
effect
,
go
rent
Star
War
.

If
it
's
scare
you
want
,
rent
Halloween
.

Either
wai
,
it
's
probabl
in
your
best
interest
to
skip
The
Haunt
.

