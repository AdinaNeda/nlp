Star
:
Jean
Reno
,
Natali
Portman
,
Gari
Oldman
,
Danni
Aiello
.

Screenplay/Director
:
Luc
Besson
.

I
wonder
if
budget
is
at
all
a
criterion
for
whether
or
not
a
movi
can
be
consid
an
exploit
flick
.

Take
THE
PROFESSIONAL
,
for
exampl
.

It
boast
extrem
glossi
cinematographi
,
a
coupl
of
recogniz
name
actor
,
and
a
coupl
of
fairli
impress
explos
.

It
's
also
basic
about
violenc
and
cheap
titil
,
and
featur
a
central
relationship
between
a
middle-ag
man
and
a
twelve-year-old
girl
with
a
decidedli
ambigu
sexual
dimens
.

THE
PROFESSIONAL
is
all
over
the
map
,
and
it
pretens
of
be
about
anyth
more
than
it
most
unpleas
element
simpli
make
it
all
the
more
unpleas
.

THE
PROFESSIONAL
of
the
titl
is
a
New
York
hit
man
name
Leon
-LRB-
Jean
Reno
-RRB-
,
brutal
effici
but
also
veri
isol
.

One
dai
he
is
forc
to
let
someon
into
hi
life
when
a
girl
from
hi
apart
build
knock
on
hi
door
.

Her
name
is
Matilda
-LRB-
Natali
Portman
-RRB-
,
and
the
rest
of
her
famili
ha
just
been
kill
by
crook
and
veri
wire
D.E.A.
agent
Norman
Stansfield
-LRB-
Gari
Oldman
-RRB-
after
Matilda
's
father
tri
to
rip
him
off
.

Leon
reluctantli
take
Matilda
in
,
then
begin
to
teach
her
hi
profess
when
she
sai
that
she
want
to
aveng
the
murder
of
her
young
brother
.

The
two
becom
closer
,
which
make
them
all
the
more
vulner
when
Stansfield
learn
that
thei
know
too
much
,
and
set
out
to
elimin
them
both
.

Writer/director
Luc
Besson
ha
gone
thi
rout
befor
with
hi
popular
French
import
LA
FEMME
NIKITA
,
a
slick
potboil
about
a
femal
assassin
.

There
is
no
question
that
Besson
can
make
a
great
look
film
;
with
the
assist
of
cinematograph
Thierri
Arbogast
,
he
ha
creat
a
film
chock
full
of
moodi
close-up
and
evoc
light
.

But
beneath
the
shini
wrapper
,
there
is
n't
nearli
as
much
go
on
as
Besson
would
like
us
to
believ
.

The
relationship
between
Leon
and
Matilda
never
click
becaus
neither
on
is
given
a
charact
to
develop
.

Leon
is
mostli
a
collect
of
quirki
trait
,
all
intend
to
show
us
that
for
a
hire
killer
,
he
's
realli
not
so
bad
a
gui
:
he
drink
lot
of
milk
,
take
meticul
care
of
a
pot
plant
,
and
enjoi
Gene
Kelli
movi
.

Jean
Reno
succe
at
give
Leon
a
haunt
and
desper
qualiti
,
but
he
never
make
an
emot
connect
to
Matilda
.

Natali
Portman
is
all
wrong
for
a
part
that
call
for
a
much
grittier
qualiti
,
but
she
doe
n't
have
too
much
to
work
with
,
either
.

Besson
would
have
been
better
serv
spend
more
time
try
to
bring
hi
charact
to
life
,
and
less
on
feebl
attempt
at
humor
which
ar
often
embarrass
.

A
silli
game
between
Leon
and
Matilda
involv
celebr
imperson
is
complet
out
of
place
,
as
is
a
scene
where
Matilda
shock
a
hotel
manag
by
announc
that
Leon
is
her
lover
;
charact
is
thoroughli
sacrif
for
a
cheap
gag
.

Gari
Oldman
's
entir
part
is
someth
of
a
cheap
gag
,
wild-ei
and
wai
over
the
top
,
but
at
least
he
is
interest
to
watch
.

There
is
n't
a
real
person
to
be
found
anywher
in
THE
PROFESSIONAL
,
which
is
n't
alwai
a
problem
in
an
action
thriller
,
except
that
thi
on
is
try
to
pass
itself
off
as
someth
more
.

A
more
disconcert
problem
with
THE
PROFESSIONAL
is
that
it
plai
around
with
the
sexual
of
a
twelve-year-old
in
a
realli
distast
wai
.

There
were
onli
two
real
choic
for
deal
with
that
compon
of
Leon
and
Matilda
's
relationship
:
confront
it
head
on
,
or
ignor
it
entir
.

But
Besson
flirt
and
teas
the
audienc
with
the
idea
that
he
's
go
to
show
them
a
forbidden
love
stori
,
while
choos
simpli
to
focu
hi
camera
on
Portman
's
rear
end
and
dress
her
in
skimpi
cloth
.

Thi
is
to
sai
noth
of
the
question
decis
to
make
it
look
like
qualiti
patern
time
when
Leon
is
teach
a
child
to
load
a
9mm
pistol
,
or
the
blood
which
is
spill
aplenti
.

A
great
deal
of
the
time
,
THE
PROFESSIONAL
is
just
plain
sleazi
,
and
all
the
soft
filter
in
the
world
ca
n't
disguis
that
fact
.

