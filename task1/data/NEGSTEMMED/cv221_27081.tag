It
's
probabl
inevit
that
the
popular
virtual
realiti
genr
-LRB-
``
The
Matrix
,
''
``
eXistenZ
''
-RRB-
would
collid
with
the
even
more
popular
serial-kil
genr
-LRB-
``
Kiss
the
Girl
,
''
``
Se7en
''
-RRB-
.

The
result
should
have
been
more
interest
than
``
The
Cell
.
''

As
the
movi
open
,
therapist
Catharin
Dean
-LRB-
Jennif
Lopez
-RRB-
treat
a
cataton
boi
-LRB-
Colton
Jame
-RRB-
by
enter
hi
mind
through
some
sort
of
virtual
realiti
techniqu
that
's
never
fulli
explain
.

After
month
of
therapi
session
in
a
surreal
desert
,
Catharin
ha
no
success
to
report
.

Meanwhil
,
killer
Carl
Stargher
-LRB-
Vincent
D'Onofrio
-RRB-
ha
claim
anoth
victim
.

Hi
particular
hobbi
is
to
kidnap
young
women
,
keep
them
in
a
glass
cell
overnight
,
and
drown
them
.

He
take
the
corps
and
soak
it
in
bleach
,
then
suspend
himself
over
the
bodi
and
jerk
off
while
watch
a
video
tape
of
the
drown
.

Although
Carl
's
been
do
thi
for
awhil
,
he
's
recent
becom
sloppi
,
and
FBI
Agent
Peter
Novak
-LRB-
Vinc
Vaughn
-RRB-
is
close
in
fast
.

Not
fast
enough
,
though
,
to
keep
Carl
from
stick
anoth
woman
-LRB-
Tara
Subkoff
-RRB-
in
the
cell
or
to
catch
him
befor
he
suffer
a
schizophren
attack
that
leav
him
in
a
coma
.

From
the
video
in
Carl
's
hous
,
Peter
can
see
that
the
drown
cell
is
autom
and
will
fill
with
water
forti
hour
after
the
abduct
.

To
save
the
kidnap
girl
,
Peter
ha
to
find
the
cell
befor
the
end
of
the
dai
,
and
comatos
Carl
's
not
talk
.

So
off
thei
go
to
Catharin
in
the
hope
that
she
can
go
insid
Carl
's
mind
and
find
out
where
the
cell
is
in
time
.

The
focu
of
``
The
Cell
''
in
on
the
ornat
interior
of
Carl
's
mind
,
but
the
univers
director
Tarsem
Singh
creat
seem
more
an
exercis
in
computer-gener
spectacl
than
an
explor
of
the
psychot
person
.

For
the
most
part
,
it
's
style
without
substanc
.

In
hi
own
mind
,
Carl
is
a
decad
emperor
in
flow
robe
,
Ming
the
Merciless
,
as
well
as
a
frighten
boi
-LRB-
Jake
Thoma
-RRB-
abus
by
hi
father
.

All
in
all
,
the
mind
of
a
psycho
killer
turn
out
to
be
a
strang
dull
place
,
and
I
kept
wish
I
could
fast-forward
to
the
next
develop
.

Singh
is
best
known
for
direct
music
video
,
particularli
REM
's
``
Lose
My
Religion
,
''
and
``
The
Cell
''
seem
veri
much
like
a
realli
long
,
realli
slow
MTV
video
with
the
sound
delet
.

Singer
Lopez
seem
to
think
she
's
in
a
video
as
well
;
she
devot
more
time
to
pose
in
elabor
costum
than
she
doe
to
act
.

The
premis
had
great
promis
.

The
computer-gener
world
within
Carl
's
mind
could
have
been
a
bizarr
,
surreal
univers
govern
by
insan
and
symbol
rather
than
logic
.

The
first
room
Catharin
enter
in
Carl
's
head
show
thi
promis
.

She
find
a
hors
stand
in
center
of
the
room
;
suddenli
,
sheet
of
sharp-edg
glass
fall
into
the
hors
,
divid
it
into
segment
.

The
pane
of
glass
separ
,
pull
apart
the
piec
of
the
still-liv
hors
.

Thi
scene
is
twist
,
disturb
,
and
thought-provok
,
becaus
the
psycholog
import
of
the
hors
and
it
fate
is
left
to
the
viewer
to
ponder
.

Another
element
that
should
have
been
develop
is
the
effect
on
Catharin
of
merg
with
the
mind
of
a
psychopath
.

Their
mind
begin
to
bleed
togeth
at
on
point
in
the
movi
,
and
thi
should
have
provid
an
opportun
to
discov
the
dark
corner
of
Catharin
's
own
psych
.

Like
Sidnei
Lumet
's
``
The
Offenc
''
or
Michael
Mann
's
``
Manhunt
,
''
``
The
Cell
''
could
have
explor
how
the
mad
of
the
killer
bring
out
a
repress
dark
in
the
investig
.

Howev
,
Catharin
's
charact
is
hardli
develop
at
all
,
and
Lopez
ha
no
depth
to
offer
the
role
.

Bottom
Line
:
Do
n't
get
trap
in
thi
on
.

