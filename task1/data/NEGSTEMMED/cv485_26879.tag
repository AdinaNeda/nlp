When
critic
attack
seemingli
well-intent
film
like
PATCH
ADAMS
or
more
recent
PAY
IT
FORWARD
as
I
am
about
to
do
,
their
opinion
ar
often
greet
with
a
backlash
of
angri
e-mail
,
sometim
even
mock
death
threat
from
those
who
-LRB-
I
suppos
-RRB-
like
to
be
shamelessli
manipul
by
their
entertain
.

Even
politician
-LRB-
!
-RRB-

tend
to
dismiss
film
critic
as
heartless
cynic
for
occasion
dump
on
good-heart
film
while
prais
``
filth
''
like
PULP
FICTION
.

These
-LSB-
fill
in
the
derogatori
term
that
you
ar
comfort
with
-RSB-
tend
to
forget
that
a
film
critic
's
job
is
to
review
the
movi
,
not
the
messag
,
howev
sweet
and
endear
that
messag
mai
be
.

Thusli
PATCH
ADAMS
is
n't
a
bad
film
becaus
it
's
about
a
doctor
who
care
for
hi
patient
;
it
's
a
bad
film
becaus
it
's
a
calcul
piec
of
brazen
audienc
exploit
.

Similarli
while
PAY
IT
FORWARD
mai
have
it
heart
in
the
right
place
-LRB-
though
I
even
doubt
that
,
stai
tune
,
an
explan
is
forthcom
-RRB-
,
it
's
still
an
unpleasantli
maudlin
mess
.

The
pictur
is
about
an
emotion
guard
5th
grade
histori
teacher
-LRB-
a
somewhat
similarli
guard
Kevin
Spacei
-RRB-
,
with
a
burn
scare
face
and
equal
sing
attitud
,
who
give
hi
class
the
seemingli
imposs
assign
of
do
on
thing
over
the
cours
of
the
semest
that
will
chang
the
world
.

12
year
old
Trevor
-LRB-
Halei
Joel
Osmet
-RRB-
,
a
Disneyfi
do
gooder
who
take
care
of
hi
alcohol
mother
-LRB-
Helen
Hunt
-RRB-
,
come
up
with
the
notion
to
PAY
IT
FORWARD
;
thi
entail
a
person
do
on
good
deed
-LRB-
call
it
an
uber
favor
-RRB-
for
three
other
,
then
tell
them
to
do
the
same
for
three
other
and
befor
long
we
're
all
live
in
a
Frank
Capra
movi
.

Meanwhil
in
a
parallel
stori
,
a
weas
report
-LRB-
Jai
Mohr
-RRB-
track
the
``
movement
''
which
ha
appar
begun
to
grow
.

PAY
IT
FORWARD
ha
odd
idea
about
tone
;
at
certain
point
it
hurl
a
smatter
of
unnecessari
cynic
at
us
as
if
that
would
be
the
appropri
antidot
to
the
unrel
sappi
at
it
core
.

It
's
as
if
director
Mimi
Leder
did
n't
have
the
confid
to
make
PAY
IT
FORWARD
the
wai
she
intend
,
and
compromis
out
of
fear
that
her
film
wa
n't
``
gutsi
''
enough
to
earn
the
acclaim
it
ha
clearli
been
made
for
.

So
the
pictur
add
grit
-LRB-
a
child
molest
proposit
Trevor
,
a
homeless
man
return
to
heroin
after
be
``
save
''
,
a
charact
is
knife
while
do
a
good
deed
,
etc
-RRB-
though
it
seem
shallow
,
incorpor
into
the
proceed
becaus
the
film
itself
is
n't
quit
sure
if
it
's
readi
to
bui
into
it
own
utopian
blather
.

PAY
IT
FORWARD
offer
``
crowd
pleas
''
element
for
gener
audienc
;
Jai
Mohr
's
umpteenth
variat
on
the
WASPy
sleazebal
,
Angie
Dickinson
's
earthi
alcohol
bag
woman
,
and
the
jive-talk
``
black
hoodlum
with
a
heart
of
gold
''
-LRB-
come
on
the
heal
of
the
black
,
god-lik
inmat
embodi
by
Gabriel
Casseu
in
BEDAZZLED
,
thi
mai
just
be
the
begin
of
a
brand
new
guilti
white
liber
clich
.

Hurrai
for
Hollywood
-RRB-
who
actual
call
the
Mohr
charact
a
``
nigga
''
and
sai
thing
like
``
can
you
feel
me
?
''

Leav
it
to
PAY
IT
FORWARD
to
happili
includ
a
littl
minstrel
act
for
our
further
enjoy
.

-LRB-
Mayb
Spike
's
flick
wa
n't
so
misguid
after
all
-RRB-
.

These
scene
feel
as
if
thei
belong
in
a
dopei
comedi
with
feel
good
aspir
rather
than
the
irritatingli
saccharin
drama
that
you
'll
find
yourself
trap
in
should
you
not
heed
my
warn
.

While
the
filmmak
have
stress
in
interview
that
thei
actual
hope
thi
is
a
film
that
could
make
the
world
a
better
place
,
to
me
the
final
product
ha
the
oili
feel
of
a
cynic
politician
contemptu
recit
cooki
fortun
slogan
to
a
hope
audienc
.

It
's
a
pictur
that
look
to
be
tailor
made
for
Academi
consider
,
with
the
main
player
-LRB-
all
previou
Oscar
winner
or
nomine
-RRB-
given
big
emot
scene
that
ar
practic
variat
on
their
most
laud
turn
.

One
scene
in
which
a
slightli
de-glam
Helen
Hunt
-LRB-
de-glam
here
mean
that
not
onli
is
she
a
fashion
victim
-LSB-
her
look
is
somewher
between
Goth
queen
and
trailer
park
mama
-RSB-
,
Hunt
's
hair
is
also
natti
and
badli
bleach
-RRB-
verbal
castig
Spacei
,
feel
right
out
of
her
AS
GOOD
AS
IT
GETS
Oscar
clip
.

Spacei
,
of
cours
,
respond
in
hi
cultur
monoton
,
that
could
be
straight
from
hi
low
kei
AMERICAN
BEAUTY
perform
.

Mayb
the
actor
should
get
an
award
for
come
off
the
least
self
conscious
irrit
;
though
how
can
he
not
,
even
when
he
's
emot
Spacei
seem
somehow
shield
by
a
wry
indiffer
.

The
usual
talent
Helen
Hunt
is
the
worst
offend
.

In
on
atroci
TOUCHED
BY
AN
ANGEL
moment
Hunt
slap
Osmet
,
immedi
cover
her
mouth
with
the
guilti
hand
in
that
veri
same
stroke
,
and
with
said
hand
still
on
said
mouth
,
she
actual
begin
convuls
-LRB-
dry
heav
realli
-RRB-
,
then
-LRB-
and
I
'm
not
make
thi
up
-RRB-
she
scurri
to
the
kitchen
tear
the
place
apart
in
a
frenzi
search
for
alcohol
.

The
scene
is
so
hokei
it
could
veri
nearli
be
constru
as
a
parodi
of
hacknei
TV
movi
blow-up
;
it
's
all
veri
theatric
,
especi
Hunt
's
perform
,
which
feel
like
a
pathet
plea
to
the
Academi
for
anoth
Oscar
.

PAY
IT
FORWARD
ha
the
dubiou
distinct
of
be
a
film
that
could
be
us
as
an
argument
for
why
actor
should
n't
get
Academi
Award
.

The
pictur
unknowingli
affirm
that
Oscar
transform
nuanc
talent
into
overwrought
expressionist
.

Even
Halei
Joel
Osemt
,
the
wunderkind
who
wa
nomin
for
an
Academi
award
for
hi
unforc
perform
in
SIXTH
SENSE
,
seem
more
divis
about
hi
express
and
inflect
.

Hi
act
ha
becom
broader
,
less
intim
;
it
's
as
if
we
can
make
out
the
littl
munchkin
's
thought
:
``
boi
thi
outta
floor
em
''
.

Natur
Osmet
is
made
to
plai
on
of
those
only-in-the-movi
children
,
a
martyr-figur
who
clean
up
after
hi
mother
,
lectur
her
on
the
ill
of
drink
,
and
even
fix
her
up
on
a
date
with
hi
intellectu
teacher
.

Never
mind
that
the
two
could
n't
be
more
dissimilar
;
hi
mother
is
trailer
trash
dopei
,
and
the
teacher
is
on
of
those
Denni
Miller-ei
intellectu
who
hide
hi
insecur
behind
a
vast
vocabulari
.

Nevertheless
thi
littl
nudnik
goe
out
of
hi
wai
to
bring
the
pair
togeth
in
a
scene
that
recal
PARENT
TRAP-ish
cornbal
antic
.

But
oh
how
we
love
bright
,
articul
,
self-sacrif
children
who
pick
adult
up
by
their
bootstrap
and
guid
them
through
life
.

Thei
're
so
ador
.

Of
cours
the
blame
ca
n't
all
be
hoist
onto
the
actor
-LRB-
though
with
the
except
of
the
littl
kid
,
thei
probabl
should
have
known
better
-RRB-
,
instead
the
brunt
of
it
should
be
pass
on
to
Mimi
Leder
,
who
direct
on
of
the
most
thrill
episod
of
ER
,
then
went
on
to
make
two
aw
genr
film
in
a
row
.

The
first
be
THE
PEACEMAKER
,
a
witless
post-Cold
War
Georg
Cloonei
vehicl
,
and
DEEP
IMPACT
,
on
of
the
two
film
of
1998
to
squander
the
premis
of
Earth
's
possibl
demis
by
a
craze
meteorit
.

The
first
flick
wa
sunk
by
an
over
relianc
on
clich
,
and
a
complet
absenc
of
ani
kind
of
emot
involv
,
not
aid
by
an
end
which
actual
center
around
the
diffus
of
a
tick
time
bomb
complet
with
digit
read
out
-LRB-
appar
present
for
an
invis
audienc
-RRB-
.

DEEP
IMPACT
ha
more
in
common
with
PAY
IT
FORWARD
;
it
's
a
movi
that
treat
Earth
's
impend
destruct
in
awfulli
simplist
term
,
complet
ignor
the
havoc
that
would
so
obvious
take
place
if
the
world
believ
it
planet
would
be
a
goner
within
dai
.

The
film
wa
full
of
inspir
speech
where
peopl
realli
said
noth
,
though
the
sappi
score
swell
up
to
make
it
appear
as
if
thei
were
be
profoundli
touch
.

In
PAY
IT
FORWARD
Leder
continu
in
thi
vein
with
her
intermitt
dollop
of
cynic
seem
almost
like
a
rebutt
:
``
See
my
movi
is
n't
as
nave
as
you
might
think
''
she
seem
to
be
sai
.

No
,
it
's
just
horribl
confus
.

