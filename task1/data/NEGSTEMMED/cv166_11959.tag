There
ar
some
pretti
impress
star
in
LOST
IN
SPACE-it
's
just
that
none
of
them
happen
to
be
actor
.

The
star
I
'm
refer
to
ar
the
comput
gener
on
that
make
up
the
movi
's
``
outer
space
''
;
the
star
that
the
less
impress
actor
hurtl
thru
as
thei
try
to
find
their
wai
home
.

Ye
,
in
term
of
act
,
the
star
power
is
...
well
,
more
like
a
fall
star
.

Kinda
pretti
,
short-liv
,
and
pretti
much
dead
.

LOST
IN
SPACE
,
as
if
you
realli
did
n't
know
,
is
base
on
the
60
's
sci-fi
televis
seri
of
the
same
name
.

It
's
the
year
2058
,
and
Earth
's
preciou
resourc
ar
quickli
be
usurp
by
the
need
of
it
massiv
popul
.

John
Robinson
-LRB-
William
Hurt
-RRB-
is
the
scientist
lead
a
mission
program
to
colon
a
foreign
planet
.

Earth
's
entir
exist
is
conting
upon
a
success
mission
,
but
nobodi
,
whether
it
be
John
's
famili
or
hire
battl
pilot
Don
West
-LRB-
Matt
LeBlanc
-RRB-
,
seem
too
enthusiast
about
leav
their
home
planet
for
sever
year
.

John
's
famili
consist
of
hi
wife
Maureen
-LRB-
Mimi
Roger
-RRB-
,
hi
atyp
teenag
daughter
Penni
-LRB-
Lacei
Chabert
-RRB-
,
hi
ingeni
son
Will
-LRB-
Jack
Johnson
-RRB-
,
and
hi
beauti
scientist
daughter
Judi
-LRB-
Heather
Graham
-RRB-
.

Despit
everyon
's
reluct
,
the
Jupiterspacecraft
abandon
planet
Earth
and
make
it
wai
into
the
vast
etern
of
space
.

Unknown
to
anyon
but
the
audienc
,
there
is
an
evil
doctor
stowawai
determin
to
sabotag
the
entir
mission
.

Dr.
Smith
-LRB-
Gari
Oldman
-RRB-
ha
been
hire
by
a
group
of
rebel
conspir
to
turn
the
expedit
sour
,
and
Dr.
Smith
ha
re-program
a
talk
robot
to
``
destroi
Robinson
famili
''
!

When
everyth
that
could
go
wrong
doe
go
wrong
for
both
side
,
the
spaceship
is
warp
to
an
unknown
destin
,
and
now
the
premis
of
be
lost
in
space
is
complet
.

As
for
the
audienc
,
you
will
like
be
lost
in
boredom
by
thi
point
,
wonder
if
the
plot
,
like
the
Jupit
2
,
will
ever
get
off
the
ground
.

It
's
hard
to
tell
who
deserv
the
most
blame-th
incred
bland
and
corni
charact
or
the
horrif
lame
script
?

Chabert
is
basic
the
onli
on
to
overdo
it
,
sound
like
a
whini
munchkin
on
helium
.

If
you
,
like
me
,
were
convinc
by
commerci
that
her
voic
wa
alter
for
some
sort
of
plot
twist
where
her
bodi
would
be
taken
over
by
alien
,
you
're
wrong
!

That
's
just
her
normal
voic
!

In
yet
anoth
exampl
of
a
``
Friend
''
falter
on
the
big
screen
,
LeBlanc
is
so
incred
dull
and
yet
so
obvious
try
so
hard
to
be
so
incred
charm
-LRB-
make
sens
?
-RRB-

,
it
make
you
want
to
shove
hi
ass
out
the
escap
pod
corridor
without
an
escap
pod
.

Graham
is
a
babe-thank
good
there
wa
someth
for
me
to
think
about
dure
thi
film
.

Hurt
,
the
black
hole
of
excit
,
suck
up
ani
energi
that
might
have
been
left
.

If
Hurt
were
avail
in
tablet
form
,
he
would
be
a
prescript
strength
sleep
pill
.

Johnson
is
n't
dull
,
he
's
just
lame
as
the
young
know
it
all
who
wind
up
save
everybodi
's
ass
all
the
time
.

Want
an
exampl
of
how
cool
thi
kid
can
be
?

How
about
when
he
convinc
the
robot
to
think
with
it
heart
and
reconsid
kill
the
famili
?

Hei
,
do
n't
laugh-th
thing
actual
listen
to
the
Mr.
Rogers-would-be-proud
sentiment
.

But
ala
,
if
you
thought
a
charact
could
n't
be
much
wors
,
there
wa
Roger
as
the
epitom
of
gener
-LRB-
or
,
mother
as
she
wa
known
-RRB-
.

Why
hire
an
actress
?

Thei
could
've
had
a
white
cardboard
cutout
with
the
word
MOM
print
on
it
.

Now
that
would
've
had
some
pizzazz
!

LOST
IN
SPACE
luckili
doe
n't
suffer
in
everi
singl
categori
that
it
could
have
.

The
special
effect
ar
crisp
,
clear
,
and
at
least
mildli
captiv
,
unlik
ani
of
the
presenc
onscreen
save
it
be
Oldman
,
who
plai
hi
evil
charact
with
a
great
deal
of
fun
and
finess
.

Unfortun
,
Oldman
is
lock
awai
for
most
of
the
film
,
give
us
noth
but
ampl
mock
opportun
to
enjoi
.

While
the
special
effect
ar
pleas
to
the
ey
,
thei
ar
noth
you
could
n't
find
in
most
modern
sci-fi
film
.

CONTACT
,
for
exampl
,
far
exce
thi
film
in
term
of
imageri
and
imagin
.

LOST
IN
SPACE
just
ha
too
mani
shortcom
to
ever
be
consid
a
work
of
cinemat
art
,
with
numer
contradict
-LRB-
the
time
travel
aspect
wa
horribl
flaw
!
-RRB-

,
wooden
and
corni
act
,
wors
dialogu
,
and
an
end
so
disappoint
,
you
'd
be
happier
to
have
seen
the
entir
Robinson
famili
get
blown
to
smithereen
.

Then
again
,
with
an
end
like
thi
film
ha
,
it
's
obviou
a
sequel
is
alreadi
be
consid
.

What
an
aw
note
to
end
on
,
know
there
could
be
more
of
thi
in
a
year
or
two
.

The
attempt
to
be
famili
orient
is
commend
,
but
LOST
IN
SPACE
is
lost
with
the
illus
that
special
effect
and
the
nostalgia
of
a
classic
TV
seri
be
revisit
is
enough
to
satisfi
all
ag
group
.

Well
,
danger
potenti
movi
goer
!

Danger
!

Thi
movi
crash
land
without
ever
break
thru
the
atmospher
of
mediocr
.

