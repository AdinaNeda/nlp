Best
rememb
for
hi
underst
perform
as
Dr.
Hannib
Lecter
in
Michael
Mann
's
forens
thriller
,
Manhunt
,
Scottish
charact
actor
Brian
Cox
bring
someth
special
to
everi
movi
he
work
on
.

Usualli
plai
a
bit
role
in
some
studio
schlock
-LRB-
he
di
halfwai
through
The
Long
Kiss
Goodnight
-RRB-
,
he
's
onli
occasion
given
someth
meati
and
substanti
to
do
.

If
you
want
to
see
some
brilliant
act
,
check
out
hi
work
as
a
dog
polic
inspector
opposit
Franc
McDormand
in
Ken
Loach
's
Hidden
Agenda
.

Cox
plai
the
role
of
Big
John
Harrigan
in
the
disturb
new
indi
flick
L.I.E.
,
which
Lotpick
up
at
Sundanc
when
other
distributor
were
scare
to
budg
.

Big
John
feel
the
love
that
dare
not
speak
it
name
,
but
he
express
it
through
seek
out
adolesc
and
bring
them
back
to
hi
pad
.

What
bother
some
audienc
member
wa
the
present
of
Big
John
in
an
oddli
empathet
light
.

He
's
an
even-temp
,
funni
,
robust
old
man
who
actual
listen
to
the
kid
'
problem
-LRB-
as
oppos
to
their
parent
and
friend
,
both
caught
up
in
the
high-wir
act
of
their
own
confus
live
.
-RRB-

He
'll
have
sex-for-pai
with
them
onli
after
an
elabor
courtship
,
charm
them
with
temptat
from
the
grown-up
world
.

L.I.E.
stand
for
Long
Island
Expresswai
,
which
slice
through
the
strip
mall
and
middle-class
home
of
suburbia
.

Filmmak
Michael
Cuesta
us
it
as
a
-LRB-
pretti
transpar
-RRB-
metaphor
of
danger
escap
for
hi
15-year
old
protagonist
,
Howi
-LRB-
Paul
Franklin
Dano
-RRB-
.

In
hi
open
voice-ov
,
Howi
reveal
a
morbid
preoccup
with
death
on
the
road
,
cite
the
L.I.E.
highwai
death
of
filmmak
Alan
J.
Pakula
,
songwrit
Harri
Chapin
,
and
hi
own
mother
on
Exit
52
.

He
's
both
fascin
and
disturb
by
the
L.I.E.
,
and
those
feel
ar
project
onto
Big
John
-LRB-
who
follow
Howi
around
in
hi
bright
red
car
,
but
never
make
a
move
to
forc
the
boi
to
do
someth
he
doe
n't
want
to
do
.

Thi
make
him
much
more
complex
than
the
usual
child
molest
seen
in
movi
--
he
's
a
beast
,
but
asham
of
it
.
-RRB-

L.I.E.
would
have
work
best
as
a
half-hour
short
film
about
Howi
's
ill-advis
forai
into
Big
John
's
haven
.

There
is
unnecessari
pad
with
Howi
's
miser
dad
-LRB-
Bruce
Altman
-RRB-
in
the
hot
seat
for
a
white-collar
crime
,
degener
youngster
who
get
their
kick
from
rob
middle-class
hous
,
and
some
homoerot
shenanigan
with
wise-ass
Gari
Terrio
-LRB-
Billi
Kai
-RRB-
,
a
handsom
Artful
Dodger
.

Rather
than
add
to
the
theme
of
suburban
ennui
-LRB-
not
that
we
need
anoth
movi
on
that
subject
-RRB-
,
these
awkward
subplot
pad
out
the
run
time
to
adequ
featur
length
.

Concurr
,
the
relationship
between
Howi
and
Big
John
is
evenli
pace
and
exception
well
act
.

Cox
,
sport
a
basebal
cap
and
a
fade
marin
tattoo
,
is
all
bluff
and
bluster
.

Dano
is
quiet
and
at
first
glanc
seem
so
withdrawn
as
to
be
transpar
.

We
're
so
us
to
child
actor
whose
dramat
choic
ar
broad
and
obviou
-LRB-
call
Halei
Joel
!
-RRB-

,
it
's
surpris
to
see
on
who
actual
listen
throughout
ani
given
scene
.

The
restraint
is
admir
.

But
L.I.E.
's
screenplai
doe
n't
alwai
give
them
the
best
materi
.

When
Howi
read
Big
John
a
Walt
Whitman
poem
,
the
moment
feel
a
bit
too
preciou
.

Director
Michael
Cuesta
linger
on
an
ecstat
reaction
shot
of
Big
John
,
who
mai
as
well
be
hear
Glenn
Gould
perform
Bach
's
Goldberg
Variat
.

It
's
too
much
.

There
ar
also
some
obviou
dramat
contriv
involv
Big
John
's
other
boi
toi
-LRB-
Walter
Masterson
-RRB-
,
jealou
over
the
newbi
.

Thi
plot
thread
predict
lead
to
violenc
.

Not
content
to
be
a
haunt
,
observ
portrait
of
teen
alien
in
a
royal
screw
up
world
-LRB-
like
Terri
Zwigoff
's
superb
Ghost
World
-RRB-
,
Cuesta
lack
the
confid
in
hi
own
work
to
end
on
an
ambival
note
.

It
's
typic
of
unimagin
cinema
to
wrap
thing
up
with
a
bullet
,
spare
the
writer
from
actual
have
to
come
up
with
a
complex
,
philosoph
note
.

In
thi
regard
,
L.I.E.
-LRB-
and
countless
other
indi
film
-RRB-
share
someth
in
common
with
blockbust
action
film
:
problem
ar
solv
when
the
obstacl
is
remov
.

How
often
doe
real
life
work
thi
wai
?

To
extend
the
question
:
If
a
movi
is
strive
for
realism
,
do
dramat
contriv
destroi
the
illus
?

