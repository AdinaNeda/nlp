How
do
film
like
MOUSE
HUNT
get
into
theatr
?

Is
n't
there
a
law
or
someth
?

Thi
diabol
load
of
claptrap
from
Steven
Speilberg
's
Dreamwork
studio
is
Hollywood
famili
fare
at
it
deadli
worst
.

MOUSE
HUNT
take
the
bare
thread
of
a
plot
and
tri
to
prop
it
up
with
overact
and
flat-out
stupid
slapstick
that
make
comedi
like
JINGLE
ALL
THE
WAY
look
decent
by
comparison
.

Writer
Adam
Rifkin
and
director
Gore
Verbinski
ar
the
name
chiefli
respons
for
thi
swill
.

The
plot
,
for
what
it
worth
,
concern
two
brother
-LRB-
Nathan
Lane
and
an
appal
Lee
Even
-RRB-
who
inherit
a
poorli
run
string
factori
and
a
seemingli
worthless
hous
from
their
eccentr
father
.

Decid
to
check
out
the
long-abandon
hous
,
thei
soon
learn
that
it
's
worth
a
fortun
and
set
about
sell
it
in
auction
to
the
highest
bidder
.

But
battl
them
at
everi
turn
is
a
veri
smart
mous
,
happi
with
hi
run-down
littl
abod
and
want
it
to
stai
that
wai
.

The
stori
altern
between
unfunni
scene
of
the
brother
bicker
over
what
to
do
with
their
inherit
and
endless
action
sequenc
as
the
two
take
on
their
increasingli
determin
furri
foe
.

Whatev
promis
the
film
start
with
soon
deterior
into
bore
dialogu
,
terribl
overact
,
and
increasingli
uninspir
slapstick
that
becom
all
sound
and
furi
,
signifi
noth
.

The
script
becom
so
unspeak
bad
that
the
best
line
poor
Lee
Even
can
utter
after
anoth
run
in
with
the
rodent
is
:
``
I
hate
that
mous
''
.

Oh
cring
!

Thi
is
HOME
ALONE
all
over
again
,
and
ten
time
wors
.

One
touch
scene
earli
on
is
worth
mention
.

We
follow
the
mous
through
a
maze
of
wall
and
pipe
until
he
arriv
at
hi
makeshift
abod
somewher
in
a
wall
.

He
jump
into
a
tini
bed
,
pull
up
a
makeshift
sheet
and
snuggl
up
to
sleep
,
seemingli
happi
and
just
want
to
be
left
alon
.

It
's
a
magic
littl
moment
in
an
otherwis
soulless
film
.

A
messag
to
Speilberg
:
if
you
want
Dreamwork
to
be
associ
with
some
kind
of
artist
credibl
,
then
either
give
all
concern
in
MOUSE
HUNT
a
swift
kick
up
the
ars
or
hire
yourself
some
decent
writer
and
director
.

Thi
kind
of
rubbish
will
just
not
do
at
all
.

