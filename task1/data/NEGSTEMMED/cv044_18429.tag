I
'm
current
accept
all
futur
name
for
Drew
Barrymor
charact
.

In
_
The
Wed
Singer
_
,
she
wa
Julia
Gulia
.

In
_
Never
Been
Kiss
_
,
she
's
Josi
Grossi
.

Futur
DB
charact
name
includ
:
Janet
Granit
,
Janei
Graini
,
and
for
that
NC-17
project
in
the
work
,
Jo-Jo
...
aw
forget
it
.

I
'll
stick
to
my
dai
job
.

Thi
is
a
teen
movi
,
all
right
,
except
the
main
charact
ar
n't
teen
.

Drew
Barrymor
is
a
copi
editor
at
the
Chicago
Sun
Time
who
get
her
big
break
as
a
report
,
onli
it
's
veri
clear
from
the
onset
that
she
lack
the
tough
and
the
pushi
extraverted
that
mark
the
best
of
report
.

The
stori
she
's
cover
is
not
realli
a
stori
,
but
a
stori
in
the
make
.

She
is
to
return
to
high
school
as
a
student
and
explain
what
's
REALLY
go
on
.

The
ironi
is
that
Drew
's
charact
,
wa
such
a
dweeb
first
time
around
,
that
she
is
terrifi
at
go
back
.

When
she
doe
,
she
sai
the
wrong
thing
,
wear
the
wrong
cloth
,
and
project
the
``
I
know
the
answer
''
in
class
that
popular
kid
-LRB-
or
at
least
popular
kid
in
the
movi
-RRB-
reject
.

At
least
she
befriend
nerdi
Aldy
-LRB-
Joan
of
Arc
's
Leele
Sobieski
--
watch
for
her
-RRB-
,
who
turn
out
to
be
more
beauti
than
those
who
ar
``
suppos
''
to
be
more
beauti
than
her
.

What
work
:
Drew
's
geeki
old-self
.

After
a
start
in
film
that
wa
headlin
driven
,
and
a
nadir
of
role
that
had
her
plai
the
sluttish
charact
,
it
's
a
surpris
to
see
her
with
bad
hair
,
big
glass
and
brace
.

It
's
veri
funni
.

What
doe
n't
work
:
Drew
's
geeki
new-self
.

Come
on
.

Nobodi
dress
as
bad
as
she
doe
.

Could
n't
she
just
go
to
_
The
Gap
_
and
take
suggest
?

Had
she
dress
like
Princess
Leia
it
would
have
been
better
.

The
comedi
is
suppos
to
progress
when
Drew
's
younger
brother
-LRB-
plai
with
zest
by
David
Arquett
-RRB-
reenlist
to
jump-start
hi
basebal
career
.

Now
,
how
in
the
world
can
somebodi
as
nerdi
-LRB-
but
in
a
funni
wai
-RRB-
as
he
can
be
the
most
popular
kid
in
the
school
...
in
a
dai
?!

The
dialogu
in
the
film
is
,
well
,
an
embarrass
.

Her
co-work
-LRB-
Molli
Shannon
,
John
C.
Reilli
and
Gari
Marshal
-RRB-
ar
in
termin
hyper-dr
.

Her
teenag
peer
-LRB-
except
Sobieski
-RRB-
ar
so
inept
and
stupid
that
there
's
littl
bite
from
them
.

Could
n't
the
filmmak
watch
_
Heather
_
first
?

Lastli
,
I
am
proud
to
sai
that
I
caught
a
signific
gaff
in
the
film
.

If
you
see
the
film
,
you
'd
know
what
I
'm
talk
about
:
Drew
walk
into
a
bar
,
ha
her
hand
stamp
,
and
over
the
night
associ
with
some
Rastafarian
with
some
delici
,
um
,
cake
.

She
goe
wild
,
sleep
in
late
.

When
she
wake
,
she
rush
off
to
school
,
without
shower
,
without
notic
that
her
head
,
ly
on
her
stamp
hand
all
night
,
ha
transfer
part
of
the
stamp
's
imag
to
her
forehead
,
spell
``
LOSER
''
.

Funni
,
eh
?

...
except
that
the
hand
would
have
transfer
that
imag
BACKWORDS
.

