``
GODZILLA
''
Review
by
R.
L.
Strong
TriStar
Pictur
present
a
Fri
Film
/
Centropoli
Film
Product
in
associ
with
Independ
Pictur
Matthew
Broderick
Jean
Reno
Maria
Pitillo
Hank
Azaria
Kevin
Dunn
Cinematographi
by
Ueli
Steiger
.

Origin
Score
by
David
Arnold
.

Product
Design
by
Oliver
Scholl
.

Edite
by
Peter
Amundson
and
David
Siegel
.

Special
Visual
Effect
by
Volker
Engel
.

Godzilla
Design
and
Supervis
by
Patrick
Tatopoulo
.

Co-Execut
Produc
Robert
N.
Fri
&
Cari
Wood
.

Co-Produc
by
Peter
Winther
,
&
Kelli
Van
Horn
.

Execut
Produc
:
William
Fai
,
Ute
Emmerich
,
and
Roland
Emmerich
.

Produc
by
Dean
Devlin
.

Stori
by
Ted
Elliott
&
Terri
Rossio
,
Roland
Emmerich
&
Dean
Devlin
.

Screenplai
by
Dean
Devlin
and
Roland
Emmerich
.

Direct
by
Roland
Emmerich
.

Run
time
:
138
minut
.

Rate
PG-13
Let
's
get
thi
on
over
with
as
quickli
as
possibl
.

If
there
wa
a
possibl
to
receiv
a
refund
,
thi
review
would
not
be
forthcom
.

But
as
it
is
,
``
Godzilla
''
is
without
a
doubt
the
loudest
,
longest
,
and
ultim
most
amateurishli
written
film
ever
releas
through
a
major
studio
.

Produc
Dean
Devlin
and
Director
Roland
Emmerich
should
be
asham
of
themselv
,
and
as
penanc
be
forc
to
return
to
film
school
to
watch
``
Last
Year
at
Merienbad
''
until
thei
can
grasp
the
idea
of
content
and
plot
.

No
amount
of
hype
,
no
amount
of
monei
can
hide
the
fact
that
these
filmmak
ar
the
90
equival
to
William
Beaudin
-LRB-
Billi
the
Kid
VS.
Dracula
-RRB-
.

``
Godzilla
''
open
with
stock
footag
of
the
Bikini
Atoll
nuclear
test
interspers
with
footag
of
iguana
playfulli
swim
and
nuzzl
their
egg
.

We
ar
then
introduc
to
the
crew
of
a
Japanes
fish
can
ship
-LRB-
a
question
enterpris
consid
that
tuna
process
is
suppos
to
be
supervis
to
elimin
the
net
of
dolphin
-RRB-
.

Well
,
the
ship
is
attack
and
sunk
by
an
unseen
creatur
.

Later
,
a
group
of
Frenchmen
led
by
Philipp
Roach
-LRB-
Jean
Reno
-RRB-
interview
the
sole
survivor
.

In
a
state
of
shock
,
the
onli
thing
the
man
can
utter
is
the
word
``
Gojira
''
-LRB-
the
Japanes
name
of
the
fame
beast
-RRB-
.

We
ar
then
introduc
to
Dr.
Nick
Tatopoulo
-LRB-
Matthew
Broderick
,
with
a
monik
obvious
taken
from
the
effect
design
of
the
film
-RRB-
.

He
is
current
studi
the
effect
that
the
Chernobyl
disast
ha
had
on
the
local
earthworm
popul
.

He
is
immedi
draft
by
the
U.S.
militari
and
taken
to
Panama
where
he
is
shown
the
huge
footprint
of
a
creatur
.

In
a
short
amount
of
time
,
anoth
fish
boat
-LRB-
load
with
can
tuna
from
the
U.S.
and
Korea
for
some
reason
-RRB-
is
found
ground
in
Jamaica
.

Well
,
it
seem
that
whatev
is
eat
these
ship
is
head
for
New
York
.

When
the
beast
final
appear
,
he
tear
up
on
build
,
stomp
a
coupl
of
truck
and
make
life
hell
for
the
Incumbent
Mayor
Ebert
-LRB-
thumb
up
for
New
York
-RRB-
.

The
militari
,
with
Dr.
Tatopoulo
's
help
,
have
two
ton
of
fresh
fish
dump
in
the
middl
of
New
York
to
lure
the
beast
out
of
it
hide
place
in
the
New
York
subwai
system
.

It
final
come
crash
through
the
citi
street
and
ha
a
cute
face
to
face
with
Dr.
Tatopoulo
who
snap
the
beast
's
pictur
.

The
monster
eat
the
fish
,
the
militari
start
shoot
,
and
the
chase
is
on
,
with
the
Army
caus
90
%
of
the
ensu
damag
.

Work
from
a
hunch
as
to
why
Godzilla
ha
decid
to
come
to
the
Big
Apple
,
Dr.Totopoulo
bui
a
few
home
pregnanc
kit
from
a
local
drugstor
that
ha
chosen
to
remain
open
-LRB-
even
though
New
York
ha
been
evacu
-RRB-
.

Well
the
test
prove
that
the
monster
is
a
hermaphrodit
and
is
pregnant
.

Needless
to
sai
,
no
on
believ
the
good
doctor
about
hi
discoveri
,
so
he
must
join
up
with
the
renegad
French
Secret
Servic
agent
and
find
the
monster
's
nest
site
and
destroi
the
egg
befor
the
Mayor
start
let
the
popul
back
into
the
citi
-LRB-
even
though
the
monster
is
n't
dead
yet
-RRB-
.

I
sincer
hope
that
I
've
complet
spoil
ani
interest
anyon
might
have
of
see
thi
film
.

I
gave
awai
the
relev
plot
so
that
I
could
spare
those
of
you
courag
-LRB-
or
foolish
-RRB-
enough
to
drop
down
an
hour
's
wage
on
thi
tripe
.

Everyth
about
``
Godzilla
''
reek
almost
as
bad
as
the
pile
of
rot
fish
us
to
trap
the
beast
.

The
script
-LRB-
and
let
's
be
clear
here
-RRB-
by
Devlin
and
Emmerich
is
so
full
of
plot
hole
and
non
charact
as
to
be
sure
to
be
the
recipi
of
next
year
``
Razzi
''
award
.

The
dialogu
between
Maria
Pitillo
-LRB-
as
Audrei
Timmond
,
Dr.
Tatopoulo
'
estrang
girlfriend
-RRB-
and
Mr.
Broderick
is
so
adolesc
,
it
make
teenag
giggl
in
disbelief
-LRB-
as
happen
dure
the
screen
I
wit
-RRB-
.

The
film
could
be
enjoy
campi
if
it
did
n't
take
itself
so
damn
serious
.

But
to
what
end
,
as
there
is
no
commentari
on
human
foibl
against
natur
,
nor
is
there
ani
refer
about
Godzilla
be
some
sort
of
retali
against
mankind
.

No
.

Godzilla
is
just
a
big
dummi
that
got
knock
up
by
some
French
immigr
and
decid
to
let
the
state
of
New
York
pai
for
it
.

The
film
is
bleak
and
ugli
look
.

Take
place
at
night
dure
a
rain
storm
,
the
movi
ha
littl
or
no
depth
.

Everyth
is
ugli
and
dark
.

New
York
ha
never
look
so
inhospit
sinc
``
Death
Wish
''
.

In
an
attempt
to
give
the
film
some
color
,
Audrei
Timmond
carri
a
bright
red
umbrella
which
is
uniqu
in
that
everyon
els
in
the
film
carri
the
standard
grai
issu
.

The
onli
moment
of
composit
and
color
is
dure
the
brief
scene
on
Jamaica
,
where
Dr.
Tatopoulo
find
himself
stand
in
a
giant
footprint
.

The
scene
is
nice
photograph
but
poorli
set
up
.

You
know
from
the
outset
that
Nick
is
stand
in
a
footprint
.

For
a
scientist
,
thi
is
veri
poor
observ
.

Let
's
look
at
some
of
the
more
interest
plot
hole
in
the
film
-LRB-
thi
activ
,
is
becom
almost
as
popular
as
the
Kevin
Bacon
Game
-RRB-
:
1
.
-RRB-

Why
doe
the
French
nuclear
test
onli
affect
on
clutch
of
Iguana
egg
,
and
how
do
those
egg
fuse
into
on
beast
?

2
.
-RRB-

Why
would
a
cold
blood
creatur
choos
a
cold
climat
-LRB-
such
as
New
York
-RRB-
to
nest
?

I
do
n't
think
iguana
's
have
a
habit
of
migrat
.

3
.
-RRB-

How
can
Godzilla
crawl
through
the
New
York
Subwai
system
,
slice
a
submarin
in
half
,
yet
be
unabl
to
extric
himself
from
some
thin
-LRB-
in
comparison
-RRB-
steel
cabl
on
the
Brooklyn
Bridg
?

4
.
-RRB-

The
Brooklyn
Bridg
is
the
onli
suspens
bridg
in
exist
that
doe
not
need
it
's
suspens
cabl
.

5
.
-RRB-

Did
Godzilla
carri
all
two
hundr
egg
in
her
belli
?

If
so
,
then
radioact
mutat
sure
ar
wondrou
creatur
.

-LRB-
NOTE
:
each
egg
isfeet
tall
and
almost
as
wide
.

Godzilla
would
have
to
be
over
1,000
feet
tall
to
carri
them
all
-RRB-
.

6
.
-RRB-

Godzilla
can
crush
ship
and
eat
helicopt
,
but
New
York
cab
ar
made
of
stronger
steel
.

7
.
-RRB-

Godzilla
can
out
maneuv
Helicopt
,
bullet
,
torpedo
and
missil
,
but
ca
n't
catch
you
on
foot
.

9
.
-RRB-

Godzilla
can
burrow
through
the
subwai
system
but
ca
n't
tear
through
the
Park
Avenu
Tunnel
.

10
.
-RRB-

Why
wa
noth
els
mutat
by
the
nuclear
test
as
quickli
as
Godzilla
?

Mayb
a
giant
hermit
crab
in
the
sequel
.

11
.
-RRB-

Galapago
Iguana
's
actual
live
in
the
French
Polynesian
Island
?

12
.
-RRB-

Why
wa
Dr.
Tatopoulo
brought
in
by
the
militari
if
thei
were
not
go
to
listen
to
him
anywai
?

13
.
-RRB-

How
did
thei
evacu
New
York
Island
in
less
than
a
dai
,
and
how
did
thei
convinc
those
New
Yorker
to
go
to
New
Jersei
?

14
.
-RRB-

New
York
Televis
station
us
VHS
tape
for
both
film
and
broadcast
.

New
Yorker
hate
Beta
cam
.

I
could
go
on
and
on
,
but
that
would
onli
serv
to
make
the
film
seem
more
enjoy
than
it
is
.

Do
n't
be
fool
,
thi
film
ha
less
grai
matter
than
ani
episod
of
``
America
's
Funniest
Home
Video
''
.

The
perform
in
the
film
ar
singularli
bland
.

Not
on
perform
belai
ani
aw
or
fear
in
the
face
of
thi
two
hundr
foot
tall
terror
.

The
charact
,
in
the
midst
of
the
onslaught
,
have
time
to
stop
and
discuss
the
lack
of
good
coffe
,
fail
relationship
,
career
choic
.

The
onli
common
occurr
that
doe
n't
take
place
here
is
have
on
of
the
charact
have
a
bowel
movement
,
but
then
that
would
have
made
them
believ
.

Godzilla
for
the
most
part
is
okai
.

The
design
of
the
beast
is
funki
,
if
not
Veri
memor
.

One
thing
that
come
to
mind
--
the
major
redirect
of
Godzilla
in
thi
film
is
to
remov
hi
most
familiar
trademark
,
name
hi
atom
breath
.

Now
,
I
for
on
ca
n't
quit
fathom
how
you
can
call
thi
monster
Godzilla
without
that
littl
trait
.

A
good
comparison
would
be
to
make
a
Superman
film
and
elimin
hi
abil
to
fly
.

There
is
so
much
wrong
with
thi
film
that
I
ca
n't
realli
recal
anyth
recent
that
ha
left
me
thi
cold
heart
-LRB-
except
for
my
divorc
-RRB-
.

Any
film
that
can
have
a
two
ton
lizard
slip
on
gum
ball
ha
got
to
be
envis
under
the
influenc
of
Prozac
.

The
addit
of
the
babi
raptor
-LRB-
ah
,
I
meant
Godzilla
-RRB-
,
ar
noth
but
a
direct
rip
off
of
`
Jurass
Park
''
,
but
with
none
of
that
film
's
suspens
or
tension
.
''

Suffic
to
sai
that
,
``
Godzilla
''
is
without
a
doubt
the
most
brain
dead
motion
pictur
of
the
decad
.

Thi
is
a
film
that
need
the
hype
.

With
the
current
level
of
write
and
direct
,
noth
els
about
the
film
succe
.

If
you
've
seen
the
trailer
,
you
've
seen
the
best
part
.

My
onli
suggest
for
Mr.
D
and
Mr.
E.
is
that
thei
could
alwai
go
back
to
sell
shoe
.

out
of
.

Thi
film
could
be
the
next
``
Rocki
Horror
''
.

Only
it
's
not
funni
!

Copyright
1998
R.
L.
Strong
.

Noth
in
thi
articl
mai
be
reprint
or
copi
without
the
express
written
permiss
of
the
author
.

