Human
quest
for
knowledg
never
end
.

So
a
team
of
scientist
and
film-mak
travel
to
the
Amazon
to
search
for
a
legendari
Indian
tribe
.

The
parti
consist
of
anthropologist
Steven
Cale
-LRB-
Eric
Stoltz
-RRB-
and
the
camera
team
consist
of
Terri
Flore
-LRB-
Jennif
Lopez
-RRB-
,
Danni
Rich
-LRB-
Ice
Cube
-RRB-
,
Gari
Dixon
-LRB-
Owen
Wilson
-RRB-
,
Denis
Kahlberg
-LRB-
Kari
Wuhrer
-RRB-
and
Warren
Westridg
-LRB-
Jonathan
Hyde
-RRB-
.

Earli
on
their
journei
thei
meet
Paul
Saron
-LRB-
Jon
Voight
-RRB-
whose
boat
is
stuck
on
the
shore
.

Thei
agre
to
give
him
a
ride
to
the
next
villag
.

He
claim
to
know
the
area
well
and
can
be
us
locat
the
nativ
tribe
.

Veri
soon
their
friendli
backfir
on
the
group
becaus
Saron
turn
out
to
be
a
snake
hunter
without
scrupl
who
onli
want
to
catch
a
giant
anaconda
and
sell
it
to
a
zoo
.

We
do
n't
have
to
wait
too
long
for
the
giant
snake
.

She
just
had
a
panther
hor
d'oevr
and
now
is
look
for
the
main
cours
.

Our
hero
paddl
around
in
the
Amazona
as
if
it
were
the
pool
in
their
own
backyard
.

No
wonder
giant
anim
mistak
their
splash
for
a
dinner
bell
.

Our
anaconda
is
a
polit
on
and
swallow
the
first
victim
in
on
big
gulp
.

Enjoi
!

So
much
for
the
first
attempt
to
catch
her
.

But
who
would
want
to
catch
a
giant
snake
with
a
fish
pole
?

Our
villain
Saron
show
hi
soft
side
when
he
stop
Terri
from
shoot
the
snake
.

Too
bad
that
anaconda
is
just
about
to
strangl
anoth
member
of
the
expedit
.

One
by
on
she
goe
after
the
other
.

Eric
Stoltz
is
stung
by
a
giant
wasp
right
in
the
begin
and
is
mercifulli
unconci
for
the
rest
of
the
adventur
.

The
rest
of
the
crew
keep
entertain
the
viewer
although
not
the
wai
the
maker
of
the
movi
had
plan
.

Howev
the
scene
without
the
anaconda
ar
rather
bore
.

Whenev
the
lead
ladi
show
up
we
're
in
for
a
laugh
.

The
snake
remind
us
of
a
favorit
charact
of
a
famou
anim
movi
even
if
she
should
be
an
awe-inspir
monster
.

Her
attack
alwai
follow
the
same
plan
:
on
last
hypnot
look-sh
's
look
at
you
,
kid-then
she
speedili
wrap
herself
around
her
victim
and
start
to
gush
it
down
.

Mostli
we
do
n't
see
the
act
of
devour
.

But
she
look
nice
when
she
wiggl
awai
with
her
bulg
middl
part
.

Whoever
did
the
special
effect
on
thi
movi
mai
have
want
to
go
to
a
zoo
first
and
studi
some
real
snake
.

Mayb
then
the
anaconda
model
would
have
look
more
real
.

The
animatron
ar
somewhat
more
believ
.

But
that
did
n't
work
for
the
strangl
scene
.

Do
n't
go
see
the
movi
for
the
F/X
.

Thei
ar
everyth
but
up-to-d
.

The
viewer
who
like
to
watch
the
end
credit
will
see
to
his/her
surpris
that
a
snake
expert
wa
a
consult
for
the
team
.

We
mai
doubt
though
that
he
ha
ever
seen
the
final
result
of
hi
work
.

A
well
known
American
scienc
magazin
is
also
mention
in
the
credit
,
but
I
will
refrain
from
name
it
here
to
avoid
further
damag
to
it
reput
.

The
major
of
viewer
will
have
left
the
theater
as
soon
as
the
credit
start
roll
,
anywai
.

What
kind
of
audienc
is
the
target
group
for
thi
movi
?

Hard
to
sai
.

Thi
ca
n't
be
a
seriou
horror
movi
,
or
can
it
?

See
for
yourself
.

