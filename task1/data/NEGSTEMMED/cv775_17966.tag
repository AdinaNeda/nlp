Sometim
a
stellar
cast
can
compens
for
a
lot
of
thing
,
and
``
Push
Tin
''
certainli
featur
some
name
star
who
ar
go
place
:
Billi
Bob
Thornton
,
Cate
Blanchett
,
Angelina
Joli
,
and
oh
ye
John
Cusack
who
might
not
realiz
it
at
first
,
but
he
's
actual
the
*
veteran
*
among
thi
quartet
of
fine-look
peopl
.

Sometim
a
terrif
cast
like
thi
can
compens
for
a
lacklust
screen
treatment
of
an
idea
that
ha
``
hip
comedi
''
written
all
over
it
,
compens
for
workmanlik
but
uninspir
direct
,
compens
for
an
obnoxi
score
that
would
have
anyon
but
the
tone
deaf
scream
for
the
exit
,
compens
for
clichd
character
,
compens
for
embarrass
``
you
have
to
be
joke
''
situat
.

Etc.
.

In
``
Push
Tin
,
''
Thornton
,
Blanchett
,
Joli
,
and
Cusack
do
n't
have
an
earthli
.

From
the
open
sequenc
the
film
is
in
big
troubl
:
squiggli
,
``
quirki
''
credit
,
fake-look
passeng
plane
circl
New
York
,
and
Anne
Dudlei
's
in-your-ear
music
make
us
wonder
how
she
ever
got
that
Best
Origin
Score
nomin
for
``
The
Full
Monti
,
''
let
alon
won
it
.

But
I
,
for
on
,
wa
n't
readi
to
walk
just
yet
.

So
quickli
we
descend
into
a
tightly-edit
air
traffic
control
montag
which
scream
to
us
in
larg
capit
letter
THESE
PEOPLE
HAVE
A
DIFFICULT
JOB
,
YES
,
what
with
their
frantic
,
mile-a-minut
instruct
persona
,
juggl
plane
and
passeng
's
live
like
some
huge
,
real
,
mid-air
video
game
.

Hip
,
cool
,
demon
auction
Nick
``
the
Zone
''
Falzon
-LRB-
Cusack
-RRB-
is
the
best
in
the
biz
.

Of
cours
.

Until
some
hipper
,
cooler
,
leather-clad
flyboi
assist
in
the
guis
of
Russel
Bell
-LRB-
Thornton
-RRB-
show
up
to
challeng
Falzon
's
finit
air
space
.

Boi
will
be
boi
and
some
heavi
duti
testosteron
start
exud
,
then
the
macho
one-upmanship
begin
.

It
doe
n't
stop
with
see
who
can
juggl
three
747
within
a
cat
's
whisker
of
each
other
.

Oh
no
.

There
ar
some
broken
hoop
dream
,
some
wanna-see-how-fast-I-can-dr
,
and
then
the
ultim
showdown
:
wa
that
my
wife
I
saw
you
with
last
night
?

Director
Mike
Newel
-LRB-
``
Four
Wed
and
a
Funer
''
-RRB-
must
have
read
a
differ
draft
of
thi
script
becaus
the
on
that
's
be
act
out
up
there
between
Newark
,
JFK
,
and
La
Guardia
doe
n't
have
an
ounc
of
subtleti
,
and
Newel
ha
made
some
awfulli
good-and
funny-movi
befor
.

The
antic
of
these
air
traffic
control
will
make
you
cring
.

Thei
'll
make
you
frown
in
disbelief
.

Thei
'll
have
you
constantli
look
at
your
watch
.

But
wait
!

There
's
still
100
minut
to
go
!!

The
film
's
onli
save
grace
is
Blanchett
,
whose
Conni
Falzon
is
a
spunki
,
brash
,
Long
Island
housewif
who
want
to
better
herself
by
take
art
class
.

Thi
is
a
wonder
accomplish
for
the
fine
actress
who
ha
previous
plai
a
red-head
Australian
gambler
-LRB-
``
Oscar
and
Lucinda
''
-RRB-
and
a
tempestu
British
monarch
-LRB-
``
Elizabeth
''
-RRB-
.

But
she
's
not
enough
to
save
the
pictur
.

Thornton
look
terrif
and
perform
solidli
but
hi
charact
is
a
joke
.

Joli
-LRB-
as
Russel
's
knock
'
em
dead
wife
-RRB-
is
n't
bad
,
but
the
up-and-com
actress
disappoint
by
allow
herself
to
be
displai
like
a
playth
.

Cusack
crack
gum
,
don
shade
,
and
act
hip
throughout
but
,
like
everyth
els
in
the
film
,
hi
perform
is
forc
.

In
the
last
ten
minut
or
so
,
for
some
inexplic
reason
,
thing
start
come
togeth
and
you
begin
to
get
a
sens
of
how
thi
film
might
have
been
,
like
the
trailer
teas
.

But
it
's
too
littl
too
late
.

A
fine
cast
asid
,
``
Push
Tin
''
is
noth
more
than
an
embarrass
.

