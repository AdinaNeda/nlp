Mighti
Joe
Young
blunder
about
for
nearli
twenti
minut
befor
we
actual
get
to
see
a
great
big
gorilla
.

Hi
entranc
,
howev
,
is
a
grand
on
:
out
from
the
tree
he
leap
,
gargantuan
and
impos
,
sport
hand
big
enough
to
crush
a
Volkswagen
bug
,
and
a
pair
of
feet
much
larger
than
ani
pro
basketbal
player
will
ever
have
.

Joe
bellow
at
the
poacher
befor
him
,
angri
that
thei
would
like
to
kill
him
and
sell
him
for
million
of
dollar
.

Dure
thi
scene
,
I
turn
to
my
friend
and
said
,
``
Wow
.
''

That
's
some
ap
.
''
''

Joe
is
quit
a
gorilla
--
a
big
,
digit
gorilla
.

He
run
around
the
field
,
chase
car
and
peopl
.

He
pick
thing
up
and
stare
at
them
pensiv
.

He
break
thing
on
accid
,
becaus
he
's
just
too
big
to
avoid
clumsi
.

Joe
,
as
a
special
effect
,
is
an
imag
that
might
hold
your
interest
for
five
minut
.

As
a
charact
,
he
doe
n't
quit
regist
.

And
the
stori
he
's
been
put
in
is
as
limp
as
thei
come
.

The
word
``
Famili
Entertain
''
have
never
been
a
stamp
of
qualiti
,
but
Might
Joe
Young
is
just
a
silli
special
effect
movi
pretend
to
have
a
heart
.

The
pictur
is
a
remak
of
the
1949
film
.

It
's
mostli
just
about
the
big
gorilla
,
but
,
strictli
as
a
formal
,
the
movi
also
ha
a
few
human
charact
.

The
main
on
is
Jill
Young
-LRB-
poor
Charliz
Theron
,
forc
to
wast
her
talent
-RRB-
,
a
lover
of
gorilla
.

As
a
child
,
she
wit
a
group
of
poacher
murder
her
mother
;
now
,
she
live
in
the
jungl
to
protect
the
wildlif
.

Specif
,
she
's
out
to
protect
Joe
,
a
giant
gorilla
who
ha
been
her
friend
sinc
her
childhood
.

Soon
,
zoologist
Gregg
O'Hara
-LRB-
Bill
Paxton
-RRB-
discov
Joe
;
he
quickli
realiz
that
poacher
want
to
get
Joe
and
sell
him
to
a
big
game
hunter
name
Strasser
-LRB-
Rade
Sherbedgia
-RRB-
,
who
just
happen
to
be
the
same
gui
who
kill
Jill
's
mother
.

So
,
in
an
effort
to
save
Joe
,
Jill
and
Gregg
take
the
big
gorilla
to
a
preserv
,
where
it
is
instantli
clear
that
he
is
not
safe
from
poacher
.

In
addit
,
the
space
is
wai
too
small
for
him
-LRB-
after
all
,
he
is
a
realli
big
gorilla
-RRB-
.

And
,
to
add
to
the
complic
,
Strasser
show
up
under
a
benevol
pretext
-LRB-
he
tell
Jill
that
he
ha
a
preserv
big
enough
for
Joe
-RRB-
,
and
Joe
recogn
Strasser
as
the
gui
who
kill
Jill
's
mother
.

Jill
,
of
cours
,
doe
n't
recogn
Strasser
,
even
though
he
did
kill
her
mother
.

Thi
is
just
on
of
the
mani
exampl
of
stupid
in
Mighti
Joe
Young
,
a
film
that
fulfil
everi
stereotyp
that
come
with
a
Special
Effect
Film
.

The
movi
is
n't
realli
offens
--
it
ha
some
compet
actor
,
a
few
nifti
set
,
and
a
lot
of
scene
with
a
big
,
digit
gorilla
.

But
the
stori
is
realli
dumb
,
there
ar
n't
ani
charact
,
and
the
film
is
n't
satisfi
on
ani
level
beyond
it
visual
.

The
movi
also
ha
bad
dialogu
.

If
a
critic
sai
that
a
film
ha
bad
dialogu
,
it
's
onli
fair
to
quot
some
of
it
,
but
most
of
the
word
spoken
in
thi
pictur
ar
phrase
like
``
Joe
is
n't
happi
!
''

and
``
Joe
is
n't
safe
here
!
''

``
We
need
to
get
Joe
out
of
here
!
''

``
Joe
is
wai
too
big
for
that
cage
!
''

I
do
n't
know
if
these
phrase
ar
word
for
word
,
but
thei
're
close
.

As
I
said
,
the
charact
here
ar
onli
present
as
a
formal
:
thei
do
littl
more
than
state
the
obviou
-LRB-
of
*
cours
*
Joe
is
too
big
for
that
cage
-RRB-
.

Paxton
and
Theron
ar
talent
perform
who
deserv
to
work
on
a
script
that
provid
charact
for
them
.

Sadli
,
Mighti
Joe
Young
wast
them
both
.

There
ar
a
few
clever
moment
-LRB-
although
none
of
them
ar
surfac
in
my
memori
at
the
moment
-RRB-
,
and
,
as
I
said
,
the
digit
effect
held
my
attent
for
at
least
five
minut
.

It
's
certainli
not
a
bad
film
for
kid
;
I
doubt
Joe
will
give
them
nightmar
.

But
adult
in
the
audienc
ar
n't
like
to
find
much
of
anyth
interest
about
the
big
gorilla
.

A
big
gorilla
is
onli
interest
for
a
littl
while
;
after
that
,
someth
need
to
be
done
with
the
big
gorilla
.

Nobodi
ever
figur
out
what
to
do
with
the
big
gorilla
.

