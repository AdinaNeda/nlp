It
's
been
hour
sinc
I
return
from
the
much
anticip
sci-fi
opu
`
Mission
to
Mar
'
,
and
I
can
still
detect
the
reek
of
moldi
cheddar
.

Why
?

The
movi
is
a
shoddi
cheesefest
full
of
digit
ey
candi
,
stapl
carelessli
onto
a
flimsi
screenplai
which
somehow
manag
to
leapfrog
the
great
promis
of
a
space
opera
,
instead
shoot
for
the
angl
of
a
feel-good
scienc
fiction
drama
more
akin
to
`
2001
:
A
Space
Odyssei
'
.

I
got
the
feel
that
most
of
my
fellow
movie-go
patron
were
expect
anoth
`
Armageddon
'
.

But
no
,
`
Mission
to
Mar
'
certainli
is
n't
on
larg
action
sequenc
about
coloss
disast
.

Thi
is
a
supposedli
thought
,
family-friendli
space
flick
in
which
the
apocalypt
excit
take
a
back
seat
to
visual
eleg
and
uplift
drivel
.

You
have
been
warn
.

Of
cours
,
craft
a
tightli
claustrophob
space
drama
is
not
imposs
-LRB-
see
`
Apollo
13
'
for
an
excel
exampl
-RRB-
,
but
few
director
possess
the
skill
and
craftsmanship
to
pull
it
off
without
serious
scar
their
reput
.

Brian
De
Palma
ha
enough
directori
expertis
and
visual
wizardri
up
hi
sleev
to
pull
it
off
.

When
he
get
hi
hand
on
an
intellig
,
systemat
practic
script
like
`
The
Untouchabl
'
or
`
Mission
:
Impossibl
'
,
the
director
ha
the
abil
to
creat
a
sound
technic
achiev
-LRB-
although
hi
overli
indulg
style
becom
bothersom
more
than
occasion
-RRB-
.

Of
cours
,
there
's
also
the
inexcus
string
of
crap
that
ha
carri
hi
name
-LRB-
includ
`
Snake
Eye
'
and
the
notori
bomb
`
The
Bonfir
of
the
Vaniti
'
-RRB-
...
.

all
of
which
make
me
want
to
call
De
Palma
the
most
talent
hack
in
Hollywood
.

That
term
mai
be
too
harsh
,
but
if
I
were
judg
him
sole
on
the
perpetu
wast
of
talent
that
is
`
Mission
to
Mar
'
,
my
choic
of
word
would
have
been
slightli
less
lenient
.

If
I
were
Gari
Sinis
,
I
would
n't
touch
De
Palma
with
a
10-foot
pole
.

Sinis
is
a
wonder
,
wonder
actor
,
but
after
appear
in
`
Snake
Eye
'
and
thi
vomit-induc
sham
,
I
'm
sure
he
would
n't
want
to
risk
the
embarrass
of
a
third
collabor
.

The
Academi
Award-winn
plai
NASA
astronaut
Jim
McConnel
,
a
man
who
recent
lost
hi
wife
-LRB-
Kim
Delanei
-RRB-
and
is
appar
psycholog
unfit
for
an
upcom
space
shuttl
mission
to
Mar
-LRB-
oop
,
forgot
to
mention
the
year-2020
-RRB-
.

After
a
barbecu
get-togeth
for
the
astronaut
,
we
cut
to
Luc
Goddard
-LRB-
Don
Cheadl
-RRB-
and
hi
team
,
who
ar
alreadi
take
measur
and
calcul
on
the
Red
Planet
.

Suddenli
,
a
tower
format
of
rock
and
soil-prob
best
dub
a
`
sand
tornado
'
-
appear
and
creat
a
whirlwind
of
suction
.

For
some
reason
,
the
astronaut
just
stand
there
calmli
to
admir
thi
,
as
if
it
were
a
love
piec
of
art
.

The
team
is
kill
within
second
,
expect
for
Luc
,
who
wa
abl
to
send
on
final
transmiss
and
mai
still
be
aliv
.

Immediat
,
a
second
mission-consist
of
astronaut
McConnel
,
husband
and
wife
Woodi
and
Terri
Blake
-LRB-
Tim
Robbin
and
Conni
Nielsen
-RRB-
and
Phil
Ohlmyer
-LRB-
Jerri
O'Connel
-RRB-
-
ar
dispatch
to
rescu
Luc
and
discov
the
mysteri
secret
of
planet
Mar
.

Let
's
put
the
`
secret
'
on
hold
for
now
,
and
discuss
the
trip
there
.

It
is
explain
,
whether
scientif
accur
or
not
-LRB-
probabl
not
-RRB-
,
that
a
trip
to
Mar
take
roughli
six
month
.

I
'm
not
sure
why
the
quartet
of
screenwrit
behind
`
M2M
'
did
n't
capit
on
thi
juici
opportun
of
creat
tension
and
claustrophobia
.

Instead
,
we
join
the
team
dure
their
final
dai
aboard
the
ship
.

What
happen
dure
the
five
month
prior
to
thi
?

Did
thei
just
plai
card
and
tell
dirti
joke
?

Still
,
there
ar
few
nice
tens
moment
-LRB-
mayb
the
onli
in
the
movi
-RRB-
dure
the
time-fram
involv
a
fuel
leak
.

DePalma
's
direct
is
quit
good
in
these
scene
,
although
the
score
by
Ennio
Morricon
is
larg
inconsist
-LRB-
organ
music
in
space
?

C'mon
-RRB-
.

There
's
a
few
good
,
imagin
idea
in
the
landslid
of
chees
,
a
sad
realiz
that
caus
me
to
sigh
out
loud
.

It
's
a
coloss
bummer
that
`
Mission
to
Mar
'
is
poorli
assembl
and
laughabl
written
,
with
a
dubiou
and
suprem
silli
final
that
will
onli
satisfi
dedic
optimist
.

As
mention
befor
,
anyon
look
for
some
disaster-movi
carnag
is
go
to
feel
savag
disappoint
...
mayb
even
cheat
.

After
the
unbeliev
hokei
final
shot
-LRB-
with
the
word
The
End
'
somehow
ad
insult
to
injuri
-RRB-
,
a
few
audienc
member
made
the
effort
to
boo
and
hiss
at
the
screen
.

Other
mutter
obscen
,
shake
their
head
in
disbelief
while
mumbl
`
Jeez
,
that
suck
.
'

Okai
,
it
did
suck
.

But
you
have
to
show
the
actor
some
sympathet
merci
...
after
all
,
thei
do
pretti
well
.

Sinis
is
sincer
and
effect
in
mani
of
hi
scene
,
Robbin
and
Nielsen
wholeheartedli
convinc
as
a
love
NASA
coupl
,
and
funnyman
O'Connell-wel
,
he
ha
a
coupl
line
ar
actual
amus
-LRB-
and
intention
so
-RRB-
.

The
digit
effect
accompani
the
sand
tornado
sequenc
ar
quit
impress
.

So
,
by
golli
,
where
did
thi
`
Mission
'
go
wrong
?

Look
back
on
the
appal
experi
,
I
would
sai
in
practic
everi
conduit
and
crevass
it
could
have
.

While
watch
`
Mission
to
Mar
'
,
my
suggest
would
be
to
immedi
abort
,
or
better
yet
,
do
n't
even
strap
yourself
in
for
lift-off
.

