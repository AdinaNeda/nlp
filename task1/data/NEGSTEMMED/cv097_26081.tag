SAVING
GRACE
-LRB-
director
:
Nigel
Cole
;
screenwrit
:
Craig
Ferguson/Mark
Crowdy/bas
on
a
stori
by
Crowdi
;
cinematograph
:
John
de
Borman
;
editor
:
Alan
Strachan
;
cast
:
Brenda
Blethyn
-LRB-
Grace
Trevethyn
-RRB-
,
Craig
Ferguson
-LRB-
Matthew
-RRB-
,
Martin
Clune
-LRB-
Dr.
Bamford
-RRB-
,
Tcheki
Karyo
-LRB-
Jacqu
-RRB-
,
Jami
Foreman
-LRB-
China
-RRB-
,
Bill
Bailei
-LRB-
Vinc
,
low-level
drug
dealer
-RRB-
,
Valeri
Edmond
-LRB-
Nicki
-RRB-
,
Tristan
Sturrock
-LRB-
Harvei
-RRB-
,
Clive
Merrison
-LRB-
Quentin
-RRB-
,
Lesli
Phillip
-LRB-
Vicar
-RRB-
,
Diana
Quick
-LRB-
Honei
-RRB-
,
Phyllida
Law
-LRB-
Margaret
-RRB-
,
Linda
Kerr
Scott
-LRB-
Diana
-RRB-
,
Ken
Campbel
-LRB-
Sgt.
Alfred
-RRB-
;
Runtim
:
93
;
2000-UK
-RRB-
Another
formula
`
feel
good
'
quirki
comedi
from
the
British
Isle
,
much
like
``
Wake
Ned
Devin
,
''
and
a
host
of
other
popular
low
budget
movi
that
turn
a
nifti
profit
off
a
thin
stori
line
.

It
is
fill
with
likabl
establish
type
live
in
a
quaint
town
,
such
as
toler
vicar
and
policemen
,
plenti
of
eccentr
,
and
of
local
who
stick
togeth
through
thick
and
thin
.

The
strength
of
thi
film
li
in
it
ration
theme
,
that
smoke
pot
should
be
legal
sinc
it
is
even
less
danger
than
drink
alcohol
.

The
film
is
toler
until
after
it
first
hour
,
it
then
fall
complet
apart
,
becom
just
too
absurd
to
consid
as
anyth
but
sitcom
fluff
gone
complet
banana
.

But
it
long
overdu
messag
of
decrimin
for
certain
drug
is
on
target
and
allow
the
film
to
,
at
least
,
be
view
as
contain
a
pertin
messag
.

It
messag
is
especi
appropri
nowadai
consid
the
`
War
on
Drug
'
is
about
to
be
escal
into
Colombia
.

The
drug
war
ha
been
a
total
failur
so
far
and
with
no
hope
of
it
be
won
by
law
enforc
method
in
the
futur
,
it
ha
so
far
just
end
up
caus
America
's
jail
to
be
overcrowd
with
non-viol
drug
prison
and
offer
zero
hope
of
curtail
America
's
drug
usag
and
drug
suppli
.

Billion
of
dollar
have
just
been
alloc
by
Congress
to
get
offici
involv
in
the
Colombian
Civil
War
,
by
back
a
corrupt
govern
-LRB-
Rememb
Vietnam
!
-RRB-

with
the
idea
that
militari
might
could
stop
the
cocain
and
marijuana
harvest
in
that
countri
.

Everyth
about
thi
propos
action
smack
of
Vietnam
,
with
thi
scenario
closer
to
home
,
so
what
probabl
await
such
an
effort
is
a
more
futil
result
than
the
other
war
.

The
film
's
mild
theme
of
show
squar
get
into
the
weed
busi
and
find
it
accept
by
the
public
,
someth
that
should
have
been
accept
someyear
ago
,
is
a
theme
long
overdu
for
public
accept
,
but
on
that
is
better
receiv
late
than
never
.

There
ha
to
be
some
common
sens
in
distinguish
what
drug
should
be
legal
and
how
to
educ
the
public
on
thi
,
rather
than
make
it
a
polic
matter
altogeth
.

The
film
open
in
a
quaint
Cornish
seasid
villag
at
the
funer
of
the
husband
of
a
middle-ag
woman
,
Grace
-LRB-
Brenda
Blethyn
-RRB-
,
who
fell
out
of
an
airplan
,
evid
commit
suicid
.

The
widow
soon
find
out
her
husband
wa
a
bigger
bastard
than
she
imagin
.

He
left
her
with
a
stack
of
enorm
bill
and
put
up
,
without
her
knowledg
,
their
comfort
300
year
old
hous
as
collater
for
a
fail
busi
ventur
by
take
out
a
second
mortgag
.

The
widow
find
herself
bankrupt
and
with
no
market
skill
to
pai
back
her
husband
's
enorm
debt
and
prevent
her
from
lose
the
hous
and
becom
homeless
.

She
also
know
that
he
wa
have
an
affair
with
a
sophist
woman
from
London
,
Honei
-LRB-
Quick
-RRB-
.

What
make
the
affair
doubli
unsatisfi
,
is
that
when
confer
with
Honei
,
she
find
out
that
he
wa
a
tiger
with
her
in
bed
but
refrain
from
have
sex
with
her
.

Even
though
the
local
try
to
help
the
bankrupt
widow
out
as
best
thei
can
,
the
creditor
swoop
down
on
her
like
vultur
.

Her
garden
and
handyman
,
Matthew
-LRB-
Ferguson
-RRB-
,
decid
to
stai
on
with
her
,
despit
hi
last
check
bounc
.

Sinc
she
is
an
expert
amateur
horticulturist
,
he
convinc
her
to
help
him
reviv
hi
hemp
plant
,
which
he
grow
for
privat
us
.

The
two
soon
see
thi
as
a
possibl
big
busi
opportun
,
as
Grace
work
miracl
with
the
plant
which
turn
out
to
be
high-qual
stuff
.

For
Grace
,
thi
is
a
wai
to
save
her
hous
and
get
her
out
of
debt
;
for
the
Scotsman
,
Matthew
,
it
offer
a
chanc
for
him
to
continu
to
live
in
thi
beauti
villag
and
earn
enough
monei
to
marri
hi
fisherman
girlfriend
Nicki
-LRB-
Valeri
-RRB-
.

The
local
all
let
the
illeg
activ
go
on
undisturb
:
the
polic
sergeant
-LRB-
Campbel
-RRB-
pretend
not
to
notic
what
the
bright
light
in
Grace
's
greenhous
is
for
,
the
friendli
vicar
-LRB-
Phillip
-RRB-
counsel
about
the
wisdom
of
not
take
action
over
someth
that
you
ca
n't
control
,
and
the
doctor
-LRB-
Clune
-RRB-
enjoi
smoke
the
weed
for
recreat
.

The
stori
becom
incredul
when
Grace
decid
to
go
to
London
and
score
with
a
big-tim
drug
dealer
instead
of
have
Matthew
go
,
becaus
she
find
out
Nicki
is
pregnant
and
doe
n't
want
Matthew
to
end
up
in
jail
.

It
is
interest
to
note
,
that
no
on
think
smoke
grass
is
bad
for
you
,
onli
that
on
can
go
to
jail
.

The
scene
with
Grace
on
Portobello
Road
in
the
Not
Hill
section
of
London
,
try
to
find
a
drug
dealer
,
while
dress
out
of
place
,
in
a
white
dress
and
hat
on
would
wear
to
a
tea
parti
in
Cornwal
but
not
on
the
street
of
Not
Hill
while
look
to
make
a
drug
deal
,
wa
complet
hokei
and
the
film
descend
into
a
huge
black
hole
,
wherebi
it
never
recov
it
digniti
.

The
film
move
into
the
formula
mode
,
wherea
Grace
beat
the
odd
against
her
fail
to
surviv
,
as
she
meet
an
intern
drug
dealer
,
a
Frenchman
-LRB-
Tcheki
Karyo
-RRB-
,
who
is
first
seen
threaten
to
cut
off
her
finger
but
later
becom
her
busi
partner
and
husband
as
she
becom
a
millionair
and
write
a
best
sell
fiction
book
about
her
experi
,
therebi
becom
a
celebr
.

Thing
work
out
in
a
ridicul
wai
,
where
there
is
a
total
breakdown
in
the
charact
develop
of
Grace
,
where
thi
matronli
figur
suddenli
becom
mere
a
cartoon
figur
instead
of
live
flesh
and
blood
.

There
ar
a
lot
of
silli
giggl
that
come
forth
and
the
film
ha
just
enough
of
an
act
perform
by
Brenda
Blethyn
to
breath
air
into
it
befor
it
come
to
it
belat
final
,
as
it
drone
on
with
it
transpar
gratuit
upbeat
tone
,
until
it
make
the
audienc
feel
good
that
the
sweet
old
ladi
succeed
.

Thi
`
unhip
'
film
leav
on
with
the
impress
that
smoke
and
grow
pot
is
silli
,
but
should
be
toler
as
someth
naughti
middle-ag
widow
screw
by
their
terribl
former
husband
must
do
in
order
not
to
remain
destitut
.

Ummm
!

Denni
Schwartz
:
``
Ozu
'
World
Movi
Review
''
ALL
RIGHTS
RESERVED
DENNIS
SCHWARTZ

