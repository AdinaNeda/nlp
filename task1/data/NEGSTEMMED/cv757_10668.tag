HARD
RAIN
One
night
,
dure
a
torrenti
downpour
that
flood
the
street
,
we
went
to
see
--
what
els
--
HARD
RAIN
.

``
So
,
ar
we
all
go
to
die
?
''

the
Sheriff
-LRB-
Randi
Quaid
-RRB-
ask
in
the
stori
's
open
line
as
he
evacu
hi
flood
town
.

The
answer
is
pretti
much
ye
,
but
not
nearli
soon
enough
.

And
to
add
insult
to
injuri
,
the
supposedli
dead
,
regretfulli
,
often
turn
out
not
to
be
so
.

Popul
thi
bad
TV-movie-of-the-week
materi
ar
a
host
of
talent
actor
.

One
can
onli
hope
thei
were
reward
handsom
for
act
in
thi
hopelessli
muddl
pictur
.

Besid
the
obviou
hardship
of
act
most
scene
while
dog
paddl
in
the
water
,
thei
will
all
receiv
black
mark
on
their
record
for
appear
in
thi
dismal
movi
.

Graham
Yost
's
script
serv
up
on
clich
after
anoth
for
the
actor
,
who
thankfulli
manag
to
mumbl
quit
a
few
of
the
line
.

Director
Mikael
Salomon
's
stage
is
so
confus
that
you
mai
have
troubl
figur
out
what
is
happen
.

The
befuddl
present
is
exacerb
by
Peter
Menzi
,
Jr.
's
dark
and
ugli
cinematographi
.

The
plot
concern
an
armor
car
that
get
stuck
in
the
rage
water
.

Onboard
ar
guard
Tom
and
hi
Uncle
Charli
.

Christian
Slater
,
who
is
much
better
in
hi
tender
role
as
in
UNTAMED
HEART
,
plai
Tom
.

Edward
Asner
drop
by
briefli
to
take
on
the
role
of
the
soon
to
be
dead
Charli
.

Come
to
their
``
rescu
''
is
a
gang
head
by
Jim
,
plai
on
autopilot
by
the
great
actor
Morgan
Freeman
.

He
view
the
loot
,
three
million
dollar
worth
,
as
hi
retir
plan
.

The
entir
movi
is
on
big
wateri
chase
with
the
Sheriff
and
hi
poss
track
Jim
and
hi
gang
,
who
ar
in
turn
after
Tom
.

Along
the
wai
,
Tom
pick
up
a
love
interest
in
the
person
of
a
crucifix-weapon
wield
woman
name
Karen
,
plai
in
a
total
wast
perform
by
Minni
Driver
.

The
action
sequenc
ar
repetit
and
without
much
interest
.

Thei
do
featur
lot
of
explos
and
gunfir
to
keep
you
awak
.

Christoph
Young
's
emotionless
score
for
the
film
ha
a
singl
trait
,
ear-shatt
loud
.

The
plot
hole
ar
as
big
as
the
on
in
the
dam
that
break
,
submerg
the
town
.

The
charact
have
an
infinit
number
of
bullet
and
rare
do
thei
have
to
bother
reload
their
gun
.

The
weapon
and
the
ammunit
spend
most
of
the
time
under
water
or
be
rain
on
but
alwai
fire
perfectli
.

When
on
of
the
bad
gui
drop
a
gun
into
the
water
,
it
stai
in
the
same
place
until
much
later
when
Tom
swim
to
get
it
,
even
though
the
swift
water
is
so
strong
it
is
uproot
larg
tree
.

Count
these
improb
is
on
of
the
more
enjoy
wai
to
spend
your
time
as
you
wait
for
the
charact
to
kill
each
other
.

The
show
ha
a
singl
,
but
unprint
,
good
line
.

Betti
White
plai
an
incessantli
bossi
wife
,
and
,
when
her
hen-peck
husband
final
told
her
off
,
our
audienc
roar
with
laughter
.

The
show
conclud
with
a
sicken
set
of
twist
.

The
best
that
can
be
said
of
the
pictur
is
that
it
is
mere
stupefyingli
aw
as
oppos
to
laughabl
bad
.

HARD
RAIN
run
1:37
.

It
is
rate
R
for
violenc
and
would
be
fine
for
teenag
.

-LRB-
The
two
famili
behind
us
shockingli
had
a
half-dozen
preschool
among
them
.
-RRB-

