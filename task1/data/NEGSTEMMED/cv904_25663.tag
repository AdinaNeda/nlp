It
's
a
good
thing
most
anim
sci-fi
movi
come
from
Japan
,
becaus
``
Titan
A.E.
''
is
proof
that
Hollywood
doe
n't
have
a
clue
how
to
do
it
.

I
do
n't
know
what
thi
film
is
suppos
to
be
about
.

From
what
I
can
tell
it
's
about
a
young
man
name
Kale
who
's
on
of
the
last
survivor
of
Earth
in
the
earli
31st
Centuri
who
unknowingli
possess
the
kei
to
save
and
re-gener
what
is
left
of
the
human
race
.

That
's
a
fine
premis
for
an
action-pack
sci-fi
anim
movi
,
but
there
's
no
payoff
.

The
stori
take
the
main
charact
all
over
the
galaxi
in
their
search
for
a
legendari
ship
that
the
evil
``
Dredg
''
alien
want
to
destroi
for
no
appar
reason
.

So
in
the
process
we
get
a
lot
of
spaceship
fight
,
fistfight
,
blaster
fight
and
more
double-cross
than
you
can
shake
a
stick
at
.

There
's
so
much
pointless
sci-fi
banter
it
's
too
much
to
take
.

The
galaxi
here
is
a
total
rip-off
of
the
``
Star
War
''
univers
the
creator
do
n't
bother
fill
in
the
basic
detail
which
make
the
stori
confus
,
the
charact
unmotiv
and
superfici
and
the
plot
just
plain
bore
.

Despit
the
fantast
anim
and
special
effect
,
it
's
just
not
an
interest
movi
.

Chad
'
z
Movi
Page
is
back
!

New
review
of
the
latest
movi
in
easy-to-swallow
capsul
form
with
the
trendi
,
Entertain
Weekly-styl
of
letter
grade
instead
of
the
old
star
system
.

Have
I
sold
out
?

Visit
my
page
and
decid
for
yourself
;--RRB-

