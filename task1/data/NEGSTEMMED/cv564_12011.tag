Star
Billi
Crystal
,
Zane
Carnei
,
Walt
G.
Ludwig
,
Gheorgh
Muresan
,
Kathleen
Quinlan
and
Steven
Seagal
-LRB-
as
himself
-RRB-
Written
by
David
Seltzer
Direct
by
Michael
Lehmann
Produc
by
Castl
Rock
Entertainment/Columbia
Pictur
Corpor
-LSB-
US
-RSB-
and
distribut
by
Columbia
Pictur
-LSB-
US
-RSB-
/
Soni
Pictur
Entertain
-LSB-
US
-RSB-
My
Giant
begin
with
a
monologu
that
's
more
funni
than
not
and
a
distinct
`
Princess
Bride
,
'
mediev
fairy-tale-in-the-90
feel
.

I
wa
pleasantli
surpris
how
sharp
the
comedi
wa
,
with
veri
funni
scene
occur
on
a
movi
set
in
Romania
where
talent
agent
Sammi
-LRB-
Billi
Crystal
-RRB-
is
visit
a
client
and
in
the
monasteri
where
Sammi
end
up
after
be
mysteri
save
when
he
accident
plung
hi
car
into
a
stream
.

And
when
Sammi
meet
Max
,
hi
giant
,
mysteri
savior
plai
by
Gheorgh
Muresan
,
there
is
magic
in
the
air
;
albeit
it
a
bit
goofi
,
I
love
everi
minut
of
it
.

The
film
,
of
cours
,
plai
on
the
size
differ
between
the
two
,
and
at
time
you
would
almost
swear
it
must
be
special
effect
.

I
particularli
enjoi
a
shot
of
Sammi
dangl
hi
leg
from
a
Max-siz
chair
as
the
two
face
each
over
an
enorm
tabl
and
eat
from
overs
bowl
and
spoon
.

I
also
like
Jere
Burn
'
-LRB-
from
TV
's
`
Someth
So
Right
'
-RRB-
perform
as
the
director
of
the
film
in
Romania
.

At
thi
point
,
the
primari
flaw
in
the
film
is
that
Gheorgh
Muresan
is
incred
hard
to
understand
,
particularli
dure
hi
fast-pac
first
scene
.

It
is
unfortun
that
the
script
requir
him
to
speak
quickli
right
out
of
the
gate
,
as
I
did
understand
him
more
as
the
film
continu
and
I
becam
accustom
to
hi
speech
.

Howev
,
about
the
time
Muresan
becom
coher
,
the
charact
head
to
New
York
and
the
film
take
a
dive
.

Off
the
high
board
.

Suddenli
,
we
ar
expect
to
believ
Sammi
is
a
do-whatever-it-tak
slime
ball
.

Up
until
thi
point
he
wa
pursu
hi
own
interest
to
be
sure
,
but
he
did
n't
come
off
as
a
desper
jerk
.

If
he
had
,
the
first
scene
could
never
have
been
so
lightheart
and
magic
.

But
now
,
in
New
York
,
we
ar
further
introduc
to
hi
neglect
wife
and
son
,
and
hi
charact
is
desper
enough
to
involv
Max
in
a
disturb
giant
vs.
midget
wrestl
match
,
which
I
found
quit
unpleas
and
jar
within
the
framework
of
the
film
.

Indeed
,
mani
scene
stuck
out
of
thi
film
like
incorrectli
place
puzzl
piec
.

Thi
includ
the
scene
featur
Steven
Seagal
,
when
Sammi
get
Max
a
role
in
a
big
,
Hollywood
movi
film
in
La
Vega
.

While
I
enjoi
see
Seagal
poke
fun
at
himself
,
the
scene
appear
to
exist
sole
becaus
of
hi
particip
,
rather
than
becaus
the
film
demand
it
.

And
the
audienc
is
requir
to
take
a
huge
leap
to
believ
that
Max
would
win
the
role
base
on
hi
quotat
of
Shakespear
.

At
thi
point
I
found
myself
think
,
`
We
're
suppos
to
believ
that
someon
would
cast
thi
gui
in
a
film
?
'

Then
I
realiz
he
wa
,
and
I
wa
watch
it
.

In
the
final
third
,
the
film
undergo
anoth
transform
,
thi
time
to
unbridl
sentiment
.

Note
:
Major
plot
point
ar
reveal
in
thi
and
the
final
paragraph
.

Max
went
to
America
becaus
Sammi
promis
to
reunit
him
with
hi
childhood
love
,
Lillianna
,
who
ha
n't
seen
him
sinc
he
wa
a
normal-s
kid
.

Her
refus
to
see
Max
-LRB-
unbeknownst
to
him
-RRB-
lead
us
to
an
awkward
`
end
justifi
the
mean
'
scene
as
Sammi
's
wife
Serena
,
plai
nice
by
Kathleen
Quinlan
,
pose
as
Lillianna
.

I
found
thi
scene
offens
.

Not
onli
did
it
reli
on
decept
to
induc
warm
,
fuzzi
feel
from
the
audienc
,
it
reduc
Max
to
someon
we
should
piti
and
coddl
,
which
I
thought
wa
quit
undeserv
.

I
think
Max
could
have
handl
the
truth
,
gratuit
ill
and
all
.

Late
in
the
film
we
learn
Max
,
and
in
fact
all
giant
,
have
a
heart
condit
which
shorten
their
live
consider
.

Thi
could
be
an
enlighten
revel
,
but
the
film
seemingli
present
it
onli
to
justifi
Sammi
's
transform
into
a
care
,
sensit
gui
--
and
hei
,
a
great
dad
and
husband
,
too
!

`
My
Giant
'
suffer
from
a
poorli
construct
stori
line
and
undevelop
charact
whose
action
ar
determin
by
plot
point
rather
than
their
own
intern
persuas
.

A
stronger
stori
with
more
room
for
charact
growth
might
have
been
possibl
if
the
focu
wa
on
Max
's
struggl
to
be
accept
and
cast
in
movi
instead
of
Sammi
's
struggl
to
get
monei
and
becom
a
better
person
.

In
thi
scenario
,
Max
's
ill
could
have
been
a
integr
part
of
the
film
not
a
stori
motiv
.

Sentiment
and
emot
would
have
follow
natur
.

Instead
,
we
're
appar
not
suppos
to
like
Sammi
until
the
end
,
but
we
're
not
allow
to
focu
on
Max
.

Crystal
and
Muresan
give
adequ
and
at
time
enjoy
perform
,
but
in
the
end
,
`
My
Giant
'
left
me
feel
like
I
'd
been
fed
gruel
from
a
giant
spoon
.

