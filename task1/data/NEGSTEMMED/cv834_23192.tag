``
Meg
Ryan
is
irresist
in
the
comedi
that
celebr
sisterhood
!

,
''
scream
the
televis
ad
for
``
Hang
Up
,
''
disastr
written
by
real-lif
sister
Delia
and
Nora
Ephron
and
sloppili
direct
by
Dian
Keaton
.

Make
me
laugh
again
!

Not
onli
is
``
Hang
Up
''
misadvertis
,
sinc
the
film
wholeheartedli
focus
on
middl
sister
Meg
Ryan
and
give
her
two
co-star
,
Lisa
Kudrow
and
Dian
Keaton
,
littl
more
than
extend
cameo
,
but
thei
do
n't
actual
come
togeth
until
the
final
ten
to
fifteen
minut
.

And
we
ar
suppos
to
believ
their
strong
bond
,
and
smile
in
the
last
scene
when
thei
rekindl
their
rocki
relationship
,
despit
them
be
apart
for
the
major
of
the
run
time
?

Excuse
me
,
again
,
while
I
almost
bust
a
gut
at
that
truli
delusion
notion
.

Eve
Mark
-LRB-
Meg
Ryan
-RRB-
is
the
middl
Mozzel
sister
,
still
live
in
the
California
town
where
she
grew
up
,
and
with
a
husband
-LRB-
Adam
Arkin
-RRB-
and
pre-teen
son
-LRB-
Jess
Jame
-RRB-
.

Her
elderli
,
wisecrack
father
,
Lou
-LRB-
Walter
Matthau
,
in
hi
brightest
perform
in
year
-RRB-
ha
just
recent
been
put
into
the
hospit
,
in
the
final
stage
of
what
I
assum
is
Alzheimer
's
-LRB-
even
though
the
film
never
enlighten
us
on
exactli
what
is
wrong
with
him
-RRB-
.

While
Eve
's
relationship
with
her
mother
-LRB-
Clori
Leachman
-RRB-
is
nearli
nonexist
,
sinc
she
ran
out
on
Lou
and
her
children
year
ago
,
she
ha
had
her
fair
share
of
up
's
and
down
's
with
Lou
,
who
us
to
be
an
alcohol
.

Her
older
sister
is
Georgia
-LRB-
Dian
Keaton
-RRB-
,
an
editor
for
the
self-titl
magazin
,
``
Georgia
,
''
while
Maddi
-LRB-
Lisa
Kudrow
-RRB-
is
the
youngest
,
a
soap
opera
actress
.

Interestingli
,
their
profess
ar
given
,
but
we
not
onc
ever
see
them
work
at
their
job
,
or
,
for
that
matter
,
learn
much
of
anyth
about
them
.

Maddi
,
Georgia
,
and
Eve
do
not
see
each
other
much
anymor
,
their
adult
live
gradual
caus
them
to
drift
apart
,
but
thei
do
manag
to
consist
talk
on
the
phone
to
on
anoth
.

And
thei
talk
.

And
talk
.

And
talk
...
In
fact
,
the
telephon
is
the
major
star
of
the
film
,
even
more
so
than
Ryan
,
as
it
appear
in
virtual
everi
scene
of
thi
intermin
92-minut
catastroph
that
feel
like
it
three
hour
long
.

If
you
ar
abl
to
get
through
the
open
half-hour
,
in
which
phone
ring
so
much
you
feel
like
jump
through
the
screen
and
take
a
sledgehamm
to
them
,
you
will
sure
surviv
the
rest
.

The
question
is
,
who
would
want
to
subject
themselv
to
thi
resolut
irrit
,
self-involv
pat-on-the-back
?

How
could
a
comedy-drama
that
ha
the
star-pow
of
Meg
Ryan
,
Lisa
Kudrow
,
and
Dian
Keaton
be
so
veri
bad
in
so
mani
differ
wai
?

Issue
from
the
past
involv
the
possibl
jealousi
each
ha
had
for
on
of
the
other
is
fleetingli
brought
up
,
but
the
film
is
n't
matur
enough
to
deal
with
such
a
thing
in
a
thought
manner
,
and
sinc
we
learn
next
to
noth
about
their
childhood
,
it
is
a
lost
caus
that
come
off
as
noth
more
than
an
afterthought
.

Also
,
it
is
expect
that
the
viewer
quickli
catch
on
to
the
tricki
dynam
that
the
three
sister
have
with
on
anoth
,
but
no
dynam
metamorphos
.

And
when
thei
do
reunit
in
the
final
,
their
whole
consanguin
is
reduc
to
a
repuls
annoi
three-minut
scene
in
which
thei
argu
like
littl
children
.

You
can
see
the
impend
death
come
a
mile
awai
,
and
it
conveni
occur
in
the
next
scene
,
so
that
the
three
can
quickli
come
to
term
with
themselv
,
and
with
each
other
.

You
think
to
yourself
:
``
The
onli
thing
left
for
them
to
do
is
have
a
play
food-fight
,
''
and
like
clockwork
,
it
also
occur
by
the
end
credit
.

Meg
Ryan
is
a
versatil
actress
-LRB-
look
no
further
than
1998
's
``
Hurlyburli
''
or
1994
's
``
When
a
Man
Love
a
Woman
''
-RRB-
,
despit
her
variou
detractor
who
stubbornli
believ
all
she
can
do
is
romant
comedi
.

With
``
Hang
Up
,
''
the
onli
thing
she
need
to
do
is
complet
sever
her
filmmak
ti
with
Nora
Ephron
,
a
writer/director/hack
who
should
n't
be
allow
to
work
in
Hollywood
again
after
thi
big-budget
,
high-profil
debacl
.

Even
if
she
knew
what
she
wa
make
wa
not
exactli
up
to
par
in
the
qualiti
depart
,
she
nonetheless
is
veri
good
,
and
the
two
scene
that
work
,
flashback
to
Christma
1988
,
when
she
had
a
heartbreak
run-in
with
her
mother
,
and
to
Halloween
1993
,
when
Lou
crash
her
son
's
birthdai
parti
in
a
drunken
stupor
,
ar
effect
becaus
of
the
realism
Ryan
bring
to
the
situat
.

Dian
Keaton
,
as
Georgia
,
is
better
as
an
actress
than
a
director
here
,
but
that
is
a
wildli
feebl
compliment
.

What
is
more
than
a
littl
far-fetch
is
that
Keaton
is
distinctli
older
than
Ryan
and
Kudrow
,
although
in
the
veri
brief
glimps
we
get
at
them
as
children
,
she
is
no
more
than
five
year
Ryan
's
senior
.

Yeah
,
right
.

Lastli
,
poor
Lisa
Kudrow
ha
been
wast
onc
again
in
a
big-screen
ventur
,
after
her
even
more
thin
role
in
1999
's
``
Analyz
Thi
.
''

Kudrow
is
n't
given
enough
time
to
creat
a
full
person
with
Maddi
,
so
it
is
n't
her
fault
she
doe
n't
regist
until
a
few
quiet
moment
sprinkl
throughout
where
she
is
actual
bless
with
be
given
dialogu
.

If
anyth
,
though
,
Kudrow
is
a
real
talent
,
and
I
anxious
await
the
next
time
she
is
given
a
role
more
deserv
of
her
time
,
as
in
her
brilliantli
nuanc
,
Oscar-calib
work
in
1998
's
``
The
Opposit
of
Sex
.
''

If
you
ar
a
fan
of
Kudrow
's
-LRB-
and
who
is
n't
?
-RRB-

,
do
yourself
a
favor
and
rent
thi
gem
that
put
more
good
us
to
Kudrow
in
sixti
second
than
``
Hang
Up
''
doe
in
it
entireti
.

As
Eve
's
hardwork
husband
,
Adam
Arkin
is
,
predict
,
squander
with
a
role
that
give
him
next
to
noth
to
do
,
until
a
subplot
reveal
itself
midwai
through
,
onli
to
never
be
mention
again
.

Clori
Leachman
,
as
with
Ryan
and
Matthau
,
make
a
small
,
but
notic
impress
with
her
,
albeit
,
veri
brief
appear
,
while
Edie
McClurg
,
as
a
rosy-cheek
woman
Lou
had
an
affair
with
in
the
Christma
1988
flashback
,
manag
on
of
the
few
laugh
in
thi
otherwis
joyless
product
.

Nearli
all
the
emot
displai
within
``
Hang
Up
''
ar
patent
manufactur
,
and
despit
the
movi
want
the
viewer
to
care
about
the
charact
,
asid
from
Eve
,
why
would
you
want
to
when
thei
ar
all
spoil
brat
?

If
,
for
some
bizarr
,
``
Twilight
Zone
''
-
type
of
reason
,
you
find
yourself
in
a
movi
theater
show
thi
film
,
my
suggest
would
be
to
hang
up
on
it
befor
the
open
credit
ar
over
.

Sai
it
is
a
wast
of
time
is
an
understat
of
epic
proport
.

