Depend
on
your
degre
of
cinemat
acumen
,
LAST
MAN
STANDING
is
either
a
-RRB-
a
Prohibition-era
remak
of
Akira
Kurosawa
's
1961
classic
YOJIMBO
;
b
-RRB-
a
Prohibition-era
remak
of
Sergio
Leon
's
1964
classic
A
FISTFUL
OF
DOLLARS
;
c
-RRB-
a
Prohibition-era
action
drama
with
a
complet
origin
story-lin
.

New
Line
is
certainli
count
on
the
fact
that
there
ar
far
more
potenti
viewer
in
categori
-LRB-
c
-RRB-
than
in
the
other
two
combin
,
much
as
the
recent
remak
of
DIABOLIQUE
and
THE
VANISHING
count
on
avoid
such
comparison
.

The
fact
is
that
there
is
yet
anoth
wai
of
look
at
LAST
MAN
STANDING
,
which
is
as
the
first
film
version
of
the
stori
which
nod
to
the
_
real
_
sourc
materi
,
the
Dashiel
Hammett
novel
_
Red
Harvest
_
.

It
also
show
that
Kurosawa
and
Leon
knew
better
what
to
do
with
that
materi
than
Hammett
himself
.

Thi
time
around
,
The
Man
With
No
Name
is
plai
by
Bruce
Willi
,
a
fellow
with
a
shadi
histori
of
an
undefin
natur
who
roll
into
the
Texa
border
town
of
Jericho
on
dai
on
hi
wai
to
Mexico
.

By
all
appear
,
Jericho
is
well
on
it
wai
to
becom
a
ghost
town
,
with
the
few
remain
inhabit
gener
belong
to
on
of
two
bootleg
oper
fight
for
control
of
liquor
come
over
the
border
.

One
is
head
by
an
Irishman
name
Doyl
-LRB-
David
Patrick
Kelli
-RRB-
;
the
other
is
run
by
Chicago-connect
Italian
mobster
Strozzi
-LRB-
Ned
Eisenberg
-RRB-
.

Call
himself
John
Smith
,
the
man
decid
that
there
is
monei
to
be
made
from
the
conflict
,
and
hire
himself
out
as
an
enforc
to
Doyl
's
side
.

But
Smith
's
allegi
is
as
uncertain
as
hi
name
,
and
he
begin
to
plai
the
two
side
against
each
other
while
try
to
stai
on
step
ahead
of
both
of
them
.

Viewer
familiar
with
both
previou
incarn
of
thi
stori
will
find
virtual
noth
radic
chang
from
a
plot
standpoint
,
and
that
alon
should
make
LAST
MAN
STANDING
somewhat
more
respect
than
other
recent
Hollywood
remakes-cum-bastard
.

There
is
the
happili
ineffectu
lawman
-LRB-
Bruce
Dern
-RRB-
,
the
unhappili
detain
object
of
on
of
the
boss
'
affect
-LRB-
Karina
Lombard
-RRB-
,
the
barkeep
who
becom
our
anti-hero
's
onli
friend
-LRB-
William
Sanderson
-RRB-
,
suspici
lieuten
-LRB-
Christoph
Walken
and
Michael
Imperioli
-RRB-
to
question
the
boss
'
trust
in
Smith
,
a
brutal
beat
,
and
a
big
fire
.

Director
Walter
Hill
give
the
proceed
hi
usual
inject
of
steroid
,
includ
a
pair
of
gun
for
Willi
which
have
the
abil
to
propel
an
assail
backward
with
suffici
thrust
to
achiev
escap
veloc
,
but
at
least
he
doe
n't
try
to
turn
the
stori
into
a
slasher
film
or
a
buddi
pictur
.

What
he
_
doe
_
do
is
nearli
as
big
a
mistak
,
and
that
is
to
provid
a
run
voice-ov
narrat
by
Willi
which
ring
of
the
standard
hard-boil
style
of
pulp
detect
fiction
.

Ye
,
that
narrat
is
full
of
clich
,
but
those
ar
ar
not
particularli
troubl
.

The
problem
is
that
both
YOJIMBO
and
A
FISTFUL
OF
DOLLARS
succeed
larg
on
the
inscrut
of
their
lead
charact
.

Thei
were
a
mysteri
,
to
the
other
charact
in
the
film
and
to
the
audienc
,
their
motiv
never
entir
clear
even
after
thei
have
act
,
and
that
qualiti
contribut
to
their
almost
mythic
statu
.

With
John
Smith
's
voic
chatter
on
in
the
background
and
allow
us
into
hi
everi
thought
,
he
becom
more
mundan
,
just
anoth
tough
gui
try
to
stai
aliv
.

It
feel
like
a
Hammett
novel
,
all
right
,
and
Hill
can
plead
faith
to
hi
text
for
hi
choic
,
but
it
simpli
doe
n't
work
.

The
narrat
allow
The
Man
With
No
Name
to
take
us
into
hi
confid
,
and
The
Man
With
No
Name
take
_
no
on
_
into
hi
confid
.

Even
if
you
walk
into
LAST
MAN
STANDING
as
a
blank
slate
,
I
ca
n't
imagin
it
be
much
more
than
a
heavili
arm
minor
distract
.

Willi
tone
down
hi
macho
swagger
as
the
taciturn
Smith
,
but
there
is
still
a
level
on
which
he
alwai
seem
like
he
is
count
on
be
tougher
than
everyon
els
rather
than
smarter
than
everyon
els
.

Christoph
Walken
plai
Doyl
's
brutal
henchman
as
a
slight
variat
on
hi
galleri
of
soft-spoken
psycho
,
and
there
is
n't
anoth
singl
charact
whom
make
even
the
slightest
impress
.

With
no
compel
antagonist
for
Smith
,
there
is
no
build-up
toward
the
expect
showdown
,
and
when
it
doe
come
,
that
showdown
is
over
so
quickli
you
wonder
what
all
the
fuss
wa
about
.

Cinematograph
Lloyd
Ahern
-LRB-
who
also
did
the
onli
noteworthi
work
on
Hill
's
1995
flop
WILD
BILL
-RRB-
creat
some
nifti
sunburn
vista
,
but
hi
work
is
onli
to
keep
the
ey
distract
between
the
spurt
of
gunfir
and
the
next
bit
of
counter-product
narrat
.

Sometim
when
someon
see
a
lacklust
remak
of
a
rever
origin
,
thei
'll
wonder
what
all
the
talk
wa
about
.

In
the
case
of
LAST
MAN
STANDING
,
it
is
those
who
know
the
origin
who
will
wonder
what
all
that
talk
wa
about
.

