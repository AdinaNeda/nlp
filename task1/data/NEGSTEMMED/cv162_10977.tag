``
Desper
Measur
''
is
a
gener
titl
for
a
film
that
's
beyond
gener
.

It
's
also
a
depress
wast
of
talent
,
with
the
solid
team
of
Michael
Keaton
and
Andy
Garcia
unthankfulli
thrown
thankless
lead
role
,
not
to
mention
once-cool
director
Barbet
Schroeder
sadli
continu
hi
string
of
not-cool
flick
--
thi
thriller
is
more
``
Befor
and
After
''
than
``
Revers
of
Fortun
.
''

The
movi
is
a
big
disappoint
,
and
yet
it
's
somewhat
easi
to
see
what
motiv
such
big
name
to
attach
themselv
to
it
--
the
premis
is
both
promis
and
intrigu
.

Too
bad
the
execut
's
all
wrong
,
though
,
becaus
the
set-up
of
``
Desper
Measur
''
boast
some
rather
entic
element
that
deserv
to
be
put
to
far
better
us
.

San
Francisco
cop
Frank
Connor
-LRB-
Garcia
-RRB-
is
a
singl
parent
with
a
troubl
dilemma
--
hi
son
Matt
-LRB-
Joseph
Cross
-RRB-
is
stricken
with
cancer
which
onli
a
bone
marrow
transplant
can
push
into
remiss
.

Even
wors
,
the
onli
compat
donor
is
violent
sociopath
Peter
McCabe
-LRB-
Michael
Keaton
-RRB-
,
current
serv
a
life
sentenc
for
multipl
murder
and
other
variou
crime
against
societi
.

Connor
tri
hi
best
to
convinc
McCabe
to
go
along
with
the
surgeri
;
he
's
at
first
reluct
,
but
reconsid
after
he
realiz
he
can
plan
hi
escap
at
the
hospit
.

When
he
doe
make
a
run
for
it
,
Connor
offer
close
pursuit
,
but
for
differ
reason
than
hi
superior
-LRB-
includ
the
crusti
Brian
Cox
-RRB-
--
thei
want
to
take
McCabe
down
,
while
Connor
need
to
keep
him
aliv
,
or
all
hope
is
lost
for
hi
littl
boi
.

One
misstep
``
Desper
Measur
''
make
is
in
it
underdevelop
of
Matt
's
ill
.

What
's
need
ar
more
detail
as
to
exactli
why
McCabe
is
the
onli
bone
marrow
match
that
work
;
there
would
seem
to
be
other
possibl
contend
somewher
in
the
countri
,
and
thu
mani
of
the
wai
that
Connor
endang
the
live
of
himself
and
those
around
him
by
attempt
to
keep
McCabe
aliv
ar
just
a
littl
too
hard
to
swallow
.

Take
,
for
exampl
,
a
scene
where
McCabe
is
try
to
make
a
getawai
by
climb
over
an
elev
passag
tunnel
connect
two
section
of
the
hospit
.

The
polic
have
their
spotlight
and
gun
aim
right
on
him
,
and
what
doe
Connor
do
?

He
shoot
the
spotlight
out
so
that
McCabe
can
continu
hi
flight
.

Connor
know
veri
well
what
danger
deed
thi
gui
is
capabl
of
,
and
yet
never
seem
to
think
that
McCabe
will
eventu
get
to
and
try
to
harm
hi
son
.

Whatev
.

To
be
fair
,
though
,
the
lack
of
stori
background
is
the
least
of
the
movi
's
problem
.

First
and
foremost
is
how
cheapli
``
Desper
Measur
''
trade
the
potenti
of
it
's
open
scene
for
the
routin
action
ballist
of
it
final
hour
.

Once
McCabe
escap
,
the
film
becom
The
Fugit
in
revers
and
with
no
thrill
.

There
ar
countless
scene
where
the
good
gui
catch
up
to
Keaton
onli
to
have
him
grab
a
hostag
and
get
awai
.

Also
,
the
movi
is
so
intent
on
give
you
a
hoot-induc
,
lip-smack
villain
-LRB-
the
ad
have
compar
Peter
McCabe
to
Hannib
Lechter
,
natch
-RRB-
that
it
complet
cast
Garcia
's
Frank
Connor
by
the
Dullsvil
waysid
;
``
Desper
Measur
''
appear
to
like
it
antagonist
so
much
more
than
it
protagonist
,
and
the
movi
's
head-scratch
of
a
send-off
confirm
thi
.

There
is
some
good
to
be
found
amongst
thi
mess
,
particularli
in
the
act
depart
.

Cast
against
type
,
Michael
Keaton
's
underst
menac
is
highli
effect
.

Although
he
's
not
given
much
believ
to
work
with
,
Andy
Garcia
plai
off
a
taut
emot
chord
.

Joseph
Cross
,
as
Garicia
's
ail
son
,
is
surprisingli
unsentiment
,
and
Marcia
Gai
Harden
lend
solid
support
as
a
doctor
who
becom
a
major
player
in
the
unfold
chao
.

Thi
cast
doe
it
best
to
camouflag
the
sorri
plot
as
it
chug
toward
an
inevit
happi
end
,
but
most
is
lost
.

It
goe
without
sai
that
``
Desper
Measur
''
need
an
oper
of
it
veri
own
.

