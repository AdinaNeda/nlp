To
sum
the
entir
film
``
54
''
up
in
on
sentenc
,
it
would
be
:
Watch
a
VH1
documentari
instead
.

``
54
''
,
seem
like
someon
brought
William
Faulkner
into
1978
,
brought
him
into
Studio
54
,
got
him
realli
drunk
,
told
him
to
write
about
it
,
and
then
dumb
that
down
to
be
releas
to
the
public
.

A
sloppi
version
of
almost
stream
of
concious
spiral
down
into
an
oblivion
of
the
illus
of
sex
,
drug
,
and
disco
.

The
narrat
,
Shane
O'Shae
-LRB-
Ryan
Phillipp
-RRB-
,
work
as
a
greas
monkei
in
New
Jersei
who
,
of
cours
,
on
a
whim
decid
to
go
into
New
York
and
try
to
get
into
Studio
``
54
''
.

Phillipp
give
a
passabl
perform
which
could
have
been
made
by
ani
young
,
attract
actor
with
a
six
pack
stomach
.

He
is
let
in
by
Steve
Rubbel
-LRB-
Mike
Myer
-RRB-
,
the
infam
co-own
of
Studio
54
,
becaus
he
is
attract
.

He
eventu
becom
a
bu
boi
and
then
a
bartend
...
you
expect
more
,
did
n't
you
.

There
is
n't
.

The
film
build
up
from
noth
and
becom
noth
as
it
's
climax
-LRB-
is
n't
that
an
apt
word
for
Studio
54
-RRB-
land
with
a
thud
.

The
glitz
of
the
club
and
perpetu
semi-cloth
patron
ar
us
in
an
to
attempt
to
show
the
it
's
glamour
.

For
much
of
it
target
audienc
,
colleg
ag
to
earli
thirti
,
it
attempt
to
show
celebr
mix
with
``
normal
peopl
.
''

Thi
even
fail
sinc
the
onli
two
celeb
truli
introduc
ar
Andy
Warhol
and
Truman
Capot
.

You
can
be
sure
that
half
of
the
audienc
ha
n't
heard
of
them
,
anoth
quarter
onli
know
their
name
,
and
the
other
quarter
knew
thei
were
there
alreadi
.

The
humor
,
if
you
can
call
it
that
,
is
built
on
an
eighti
year
old
woman
get
high
and
70
refer
like
John
Travolta
and
Olivia
Newton
John
make
us
laugh
at
how
stupid
we
were
back
then
The
best
perform
in
the
film
is
given
by
Mike
Myer
as
the
perpetu
high
,
sexual
ambivla
,
veri
New
York
Steve
Rubel
.

He
seem
to
plai
Rubel
better
than
Rubel
would
if
he
wa
still
aliv
.

He
look
like
Rubel
and
sound
just
like
him
while
give
a
subdu
,
almost
nostalg
perform
,
when
need
.

It
remind
you
of
hi
Saturdai
Night
Live
charact
Linda
Richmond
on
``
Coffe
Talk
''
san
dress
and
wig
.

The
support
cast
of
Salma
Hayek
,
Breckin
Meyer
,
and
Neve
Cambel
ar
their
onli
there
to
give
ad
subplot
which
ar
introduc
but
lead
absolut
nowher
.

The
romanc
between
Phillipp
and
Cambel
,
as
a
soap
opera
star
,
is
complet
implaus
.

The
two
share
about
half
as
much
screen
time
as
there
ar
shot
of
the
eighti
year
old
hook
on
amphetimen
.

Hayek
's
wanna-b
singer
seem
extrem
forc
and
her
husband
,
Greg
-LRB-
Meyer
-RRB-
stand
in
as
Shane
O'Shae
's
surrog
best
friend
.

The
subplot
seem
forc
and
seem
like
thei
have
been
ad
just
to
make
sure
the
film
wa
over
an
hour
and
a
half
long
.

Overal
,
``
54
''
tri
to
give
a
view
of
the
brash
of
the
place
where
crack
flow
like
heroin
which
flow
like
wine
.

The
film
never
lead
to
anyth
,
ha
no
obser
point
,
and
cover
up
a
lack
of
real
plot
with
a
veil
of
beauti
peopl
.

In
truth
,
that
remind
me
of
the
eighi
.

