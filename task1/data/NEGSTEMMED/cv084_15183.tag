Brian
De
Palma
,
the
director
who
bought
us
Carri
,
Dress
To
Kill
and
Mission
:
Impossibl
is
back
,
and
ha
bought
all
hi
technic
expertis
with
him
.

Sadli
,
he
forget
to
bring
a
good
stori
and
believ
charact
.

Nic
Cage
plai
Rick
Santaro
,
a
fast
talk
cop
who
is
watch
a
box
match
with
hi
friend
Command
Kevin
Dunn
-LRB-
Sinis
.
-RRB-

An
assassin
take
place
,
and
Santaro
tri
to
peac
togeth
how
it
took
place
,
try
to
work
out
the
role
of
the
peopl
involv
in
thi
conspiraci
.

The
film
us
flashback
and
video
camera
to
bring
the
mysteri
togeth
.

Howev
,
the
film
doe
n't
keep
the
mysteri
for
long
,
and
onc
the
big
'
secret
'
is
out
concern
Kevin
Dunn
,
the
film
is
at
a
dead
end
.

Although
there
ar
some
fantast
shot
by
De
Palma
,
includ
a
15
minut
Steadicam
shot
at
the
start
of
the
film
,
halfwai
through
the
film
drop
dead
.

The
problem
is
that
De
Palma
doe
n't
have
a
strong
stori
or
charact
to
work
with
.

He
tri
to
offshoot
thi
with
flashi
camera
techniqu
,
but
pretti
soon
thi
ha
no
effect
on
the
audienc
,
and
I
quickli
got
bore
.

Nic
Cage
and
Gari
Sinis
try
to
inject
some
life
into
some
poorli
realis
charact
,
but
it
's
no
us
.

Thei
ar
bore
,
faceless
,
and
complet
unlik
.

Ye
,
anoth
problem
with
the
film
is
that
there
's
no-on
to
root
for
.

We
're
suppos
to
be
on
Cage
's
side
,
becaus
hi
charact
becom
nicer
throughout
the
film
.

Howev
,
hi
chang
is
sadli
unbeliev
.

Poor
Gari
Sinis
's
charact
is
terribl
,
who
complet
chang
throughout
the
start
,
the
middl
and
the
end
,
in
an
appal
wai
.

The
film
's
us
of
flashback
quickli
get
bore
.

While
clever
and
interest
the
first
few
time
,
it
quickli
becom
appar
that
flashback
ar
be
us
becaus
the
film
ha
nowher
to
go
.

Thei
certainli
do
n't
increas
the
tension
or
suspens
of
the
movi
.

There
is
small
thread
of
suspens
run
through
the
film
,
but
it
certainli
doe
n't
make
thi
film
a
powerhous
thriller
,
rather
just
a
slightli
below
averag
on
.

The
script
is
lousi
and
contriv
,
the
charact
flat
and
two
dimension
.

Certainli
not
good
factor
for
a
film
pass
itself
off
for
a
thriller
.

The
most
depress
aspect
of
the
film
is
that
it
had
potenti
.

There
's
a
good
stori
buri
in
Snake
Eye
,
it
's
just
bog
down
in
razzl
dazzl
and
flashback
.

It
also
give
itself
awai
far
too
quickli
,
rather
than
leav
the
big
twist
for
the
end
.

While
the
perform
ar
intrigu
,
especi
by
newcom
Carla
Gugino
as
a
sexi
'
number
cruncher
'
wrap
up
in
the
case
,
there
's
no
wai
of
alter
the
fact
that
Snake
Eye
is
a
termin
case
of
style
of
substanc
,
and
unless
you
're
a
big
fan
of
either
Cage
or
flash
direct
,
should
be
avoid
.

