In
French
,
the
phrase
``
film
noir
''
liter
mean
``
black
film
.
''

Webster
defin
it
as
``
a
type
of
crime
film
featur
cynic
malevol
charact
in
a
sleazi
set
and
an
omin
atmospher
that
is
convei
by
shadowi
photographi
and
forebod
background
music
.
''

Classic
film
noir
,
includ
such
memor
fare
as
``
The
Big
Sleep
,
''
the
origin
``
Cape
Fear
,
''
and
Orson
Well
'
strike
``
A
Touch
Of
Evil
,
''
emploi
black
and
white
photographi
to
emphas
the
long
shadow
associ
with
the
genr
.

Color
came
into
plai
with
neo-noir
film
like
``
Chinatown
''
and
``
Blade
Runner
.
''

``
L.A.
Confidenti
,
''
easili
the
best
film
of
1997
,
is
a
wonder
piec
of
contemporari
neo-noir
filmmak
.

For
a
textbook
exampl
of
how
to
take
all
the
element
of
neo-noir
and
creat
an
absolut
mess
,
there
's
``
Palmetto
.
''

Base
on
``
Just
Another
Sucker
,
''
a
short
stori
written
by
British
author
Rene
Raymond
under
the
pseudonym
Jame
Hadlei
Chase
,
``
Palmetto
''
show
what
happen
when
a
filmmak
put
style
ahead
of
substanc
.

Director
Volker
Schlondorff
-LRB-
``
Tin
Drum
,
''
The
Handmaid
's
Tale
''
-RRB-
state
''
We
were
n't
even
sure
for
a
long
time
if
it
wa
go
to
be
a
thriller
or
a
comedi
.
''
''

It
show
.

``
Palmetto
''
is
too
preposter
too
be
taken
serious
as
a
thriller
and
too
ponder
to
work
as
a
comedi
.

The
stori
begin
when
journalist
Harri
Barber
-LRB-
Woodi
Harrelson
-RRB-
is
releas
from
prison
.

Someon
turn
state
's
wit
and
reveal
that
Harri
wa
frame
,
a
``
reward
''
for
blow
the
lid
on
corrupt
in
the
small
Florida
town
of
Palmetto
.

Bitter
and
broke
,
Harri
plan
to
hitchhik
to
Miami
and
start
hi
life
over
,
but
ex-girlfriend
Nina
-LRB-
Gina
Gershon
-RRB-
appear
to
return
him
to
Palmetto
.

While
hang
out
at
a
bar
,
he
notic
that
a
beauti
woman
ha
left
her
purs
in
a
phone
booth
.

Harri
pocket
her
cash
,
onli
to
have
the
woman
reappear
and
catch
him
with
her
monei
in
hi
pocket
.

No
problem
,
though
.

The
radiant
blond
is
Rhea
Malroux
-LRB-
Elisabeth
Shue
,
-RRB-
the
young
wife
of
a
veri
rich
older
man
with
heart
problem
,
and
she
ha
a
proposit
for
Harri
.

Rhea
need
``
a
threaten
voic
and
someon
to
collect
the
ransom
''
for
the
stage
kidnap
of
her
teenag
stepdaught
Odett
-LRB-
Chloe
Sevigni
.
-RRB-

The
girl
want
to
bilk
a
half-million
dollar
``
ransom
''
from
the
old
man
and
will
happili
give
Harri
$
50,000
for
help
with
the
scam
.

Thing
go
wrong
,
of
cours
.

Odett
is
found
dead
,
leav
Harri
frantic
try
to
dispos
of
the
corps
and
cover
hi
track
.

In
an
iron
twist
,
Harri
is
ask
to
work
for
the
local
D.A.
's
offic
.

Thei
need
a
press
liaison
to
field
question
about
Odett
's
kidnap
and
feel
that
Harri
is
the
perfect
man
for
the
job
.

Not
a
bad
set-up
for
a
noir
film
,
if
onli
Schlondorff
knew
how
to
handl
the
materi
,
but
he
never
settl
on
a
consist
tone
.

The
actor
do
n't
know
what
to
do
with
their
charact
either
,
muddl
the
proceed
even
further
.

As
if
that
wa
n't
enough
,
the
stori
suffer
from
major
problem
in
logic
.

Woodi
Harrelson
is
a
talent
actor
with
an
admir
willing
to
take
on
riski
part
,
but
he
's
lost
here
.

Present
as
a
crusad
journalist
who
wa
horribl
wrong
,
it
make
no
sens
that
Harri
would
be
stupid
and
dishonest
enough
to
get
caught
up
in
thi
scheme
.

Harrelson
clearli
doe
n't
know
what
to
do
with
Harri
's
charact
,
so
he
spend
most
of
the
film
glower
,
sweat
and
gener
act
miser
.

Meanwhil
,
Elisabeth
Shue
give
a
goofi
perform
,
behav
like
a
vamp
on
nitrou
oxid
.

As
the
stepdaught
,
Chloe
Sevigni
lai
on
so
mani
slurpi
quirk
that
she
come
off
like
Juliett
Lewi
Jr.
.

Despit
a
number
of
steami
scene
,
there
's
no
chemistri
between
Harrelson
and
the
women
.

To
make
matter
wors
,
Schlondorff
badli
dub
in
dialogu
while
the
charact
's
lip
ar
run
over
each
other
bodi
.

Two
particularli
bad
scene
highlight
the
film
's
problem
.

While
drive
with
a
bodi
in
hi
trunk
,
Harri
ha
a
minor
car
wreck
and
a
cop
show
up
.

The
offic
want
to
help
chang
Harri
's
flat
tire
and
ask
him
to
open
the
trunk
.

Harri
's
pathet
attempt
to
keep
the
trunk
close
might
have
work
if
plai
as
comedi
,
but
under
Schlondorff
's
grim
direct
,
the
scene
is
just
embarrass
.

The
film
's
nadir
come
when
a
bad
gui
prepar
to
kill
Harri
and
Nina
.

We
're
suppos
to
be
horrifi
watch
our
hero
dangl
over
a
bathtub
fill
with
acid
,
but
by
thi
point
the
film
ha
founder
so
badli
that
the
scene
is
mere
reminisc
of
when
Jessica
and
Roger
Rabbit
were
suspend
over
a
vat
of
dip
.

Had
``
Palmetto
''
been
plai
with
tongu
firmli
in
cheek
,
it
might
have
been
an
entertain
shaggi
dog
stori
.

But
under
the
harsh
direct
of
Schlondorff
,
the
film
is
just
a
sluggish
paint-by-numb
exercis
in
neo-noir
clueless
.

Avoid
thi
nonsens
and
go
see
``
L.A.
Confidenti
''
instead
.

