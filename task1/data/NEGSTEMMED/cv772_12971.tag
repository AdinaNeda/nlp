It
happen
everi
year
--
the
dai
get
longer
,
the
weather
get
warmer
and
the
studio
start
releas
their
big-budget
blockbust
.

Thi
year
's
crop
alreadi
seem
inferior
to
that
of
past
summer
,
even
1997
's
lacklust
trio
of
BATMAN
&
ROBIN
,
THE
FIFTH
ELEMENT
and
THE
LOST
WORLD
.

The
market
blitz
in
1998
ha
been
center
on
GODZILLA
-LRB-
``
Heeer
,
lee-zerd
,
lee-zerd
...
''
-RRB-
,
which
doe
n't
make
me
optimist
about
futur
summer
.

GODZILLA
is
,
of
cours
,
base
on
a
seri
of
cult
movi
-LRB-
translat
:
realli
bad
movi
onli
a
few
peopl
can
toler
-RRB-
from
Japan
that
turn
up
realli
late
at
night
on
Ted
Turner-own
cabl
station
.

That
thi
big-budget
remak
wo
n't
rise
abov
it
root
is
fairli
obviou
.

The
credit
show
us
the
origin
of
Godzilla
.

In
five
word
:
Nuclear
explos
creat
giant
lizard
.

Sound
like
an
Enquirer
headlin
,
doe
n't
it
?

The
mushroom
cloud
is
follow
by
the
inevit
``
discoveri
''
sequenc
.

Thi
time
,
a
Japanes
gui
is
eat
noodl
with
chopstick
while
watch
Sumo
wrestl
-LRB-
if
that
ai
n't
a
stereotyp
...
-RRB-
when
he
discov
the
telltal
radar
blip
.

Peopl
die
.

Cut
to
our
protagonist
,
plai
by
Matthew
Broderick
.

He
make
hi
first
appear
wear
headphon
,
warbl
along
to
``
Singin
'
in
the
Rain
.
''

It
's
a
none-too-subtl
sign
that
he
wish
he
were
in
a
classier
movi
.

No
dice
,
Bueller
.

From
the
begin
,
poor
Matthew
ha
to
do
embarass
thing
like
fondl
giant
earthworm
and
stand
in
Godzilla
's
enorm
footprint
.

Everi
disast
movi
ha
to
have
a
know-it-al
scientist
,
and
thi
time
Broderick
is
it
.

As
the
world
's
lead
expert
on
radiat
earthworm
-LRB-
And
would
n't
you
love
to
have
that
print
on
your
busi
card
?
-RRB-

,
Broderick
is
invalu
to
the
govern
.

He
immedi
dispel
Vicki
Lewi
'
theori
that
Godzilla
is
a
dinosaur
becaus
,
hei
,
you
ca
n't
take
serious
the
intellectu
argument
of
``
Newsradio
''
cast
member
.

Broderick
instead
hit
the
nail
on
the
head
,
announc
Godzilla
is
a
radiat
lizard
.

``
The
radiat
is
n't
an
anamoli
,
''
he
announc
,
and
lightn
strike
.

``
I
believ
thi
is
a
mutat
abber
,
''
he
continu
,
and
lightn
strike
again
.

It
's
vocabulari
lightn
,
you
see
,
activ
by
word
of
four
syllabl
or
more
.

Meanwhil
,
we
're
introduc
to
our
New
York
cast
,
head
by
an
ambiti
broadcast
journalist
-LRB-
Maria
Patillo
-RRB-
,
Broderick
's
former
love
.

Gee
,
what
ar
the
odd
their
path
will
cross
again
at
a
dramat
import
time
?

Poor
Patillo
ha
been
try
to
get
ahead
in
the
new
busi
for
year
but
ha
been
held
down
by
heartless
anchorman
Harri
Shearer
.

Italian
cameraman
Hank
Azaria
tell
her
she
's
not
ruthless
enough
:
``
Nice
doe
n't
get
you
anywher
in
thi
town
.
''

It
's
dog
eat
dog
.
''
''

Actualli
,
it
's
lizard
eat
citi
,
as
Godzilla
emerg
from
the
Atlantic
to
begin
a
rampag
on
the
Big
Apple
's
core
.

The
filmmak
provid
us
with
a
one-not
drunk
fisherman
who
hook
Godzilla
.

``
I
think
I
've
got
a
bite
,
''
he
announc
as
a
gigant
tidal
wave
begin
rush
toward
him
.

You
can
guess
what
happen
next
.

Similar
reaction
spring
forth
as
the
monster
prowl
the
citi
.

Hear
the
rumbl
of
approach
footstep
,
on
New
Yorker
remark
,
``
Pleas
do
n't
tell
me
that
's
anoth
parad
.
''

Pleas
do
n't
tell
me
that
's
the
best
line
you
could
come
up
with
.

Mayor
Ebert
is
not
pleas
.

Plai
by
the
princip
from
``
Head
of
the
Class
,
''
he
continu
make
the
wrong
decis
when
given
an
option
,
and
bicker
with
hi
assist
Gene
.

I
guess
the
filmmak
knew
thei
'd
be
get
two
thumb
down
from
the
critic
and
did
n't
even
bother
to
kiss
ass
.

The
problem
is
,
if
you
're
go
to
attack
Siskel
and
Ebert
,
you
should
at
least
make
it
funni
.

Dialogu
like
,
``
Did
n't
we
agre
that
we
were
n't
go
to
have
ani
sweet
until
after
the
elect
?
''

follow
by
,
``
Back
off
,
Gene
,
''
just
doe
n't
work
for
me
.

Other
lame
run
joke
includ
everyon
mispronounc
the
Broderick
charact
's
last
name
and
Frenchman
Jean
Reno
's
inabl
to
find
a
good
cup
of
coffe
in
New
York
.

There
's
even
more
fun
to
be
had
as
GODZILLA
progress
.

As
the
beast
head
back
into
hide
,
Broderick
suggest
the
militari
lure
it
out
with
food
.

Cue
twelv
dump
truck
,
all
drop
fish
into
a
New
York
intersect
.

-LRB-
BRODERICK
:
That
's
a
lot
of
fish
.
-RRB-

That
ambush
fail
,
but
Broderick
soon
figur
out
why
Godzilla
came
to
New
York
by
bui
$
50
worth
of
home
pregnanc
test
and
run
lizard
blood
through
them
.

Yep
,
Godzilla
's
with
children
,
which
make
you
wonder
just
what
kind
of
creatur
would
be
horni
enough
to
have
sex
with
Godzilla
.

That
's
until
Broderick
explain
that
Godzilla
reproduc
asexu
,
like
Linda
Tripp
.

GODZILLA
come
to
us
from
the
maker
of
INDEPENDENCE
DAY
,
so
it
ha
a
lot
of
dumb
action
scene
,
destruct
special
effect
and
shallow
subplot
.

Look
no
further
than
Patillo
's
betray
of
Broderick
-LRB-
PATILLO
:
What
have
I
done
,
Animal
?

What
have
I
becom
?
-RRB-

and
the
climact
``
Godzilla
's
nest
''
sequenc
in
Madison
Squar
Garden
.

The
main
differ
is
,
INDEPENDENCE
DAY
wa
about
the
experi
.

It
had
a
real
global
,
patriot
element
to
it
,
and
some
genuin
fun
charact
.

GODZILLA
ha
lot
of
rain
and
lightn
,
reptilian
action
rip
off
from
JURASSIC
PARK
and
endless
product
placement
from
the
like
of
Kodak
,
Blockbust
,
Juici
Fruit
,
Swatch
,
Sprint
and
Bumbl
Bee
Tuna
.

Ye
,
Bumbl
Bee
actual
paid
to
be
known
as
the
offici
tuna
of
GODZILLA
.

That
fact
alon
is
twice
as
interest
as
anyth
in
the
movi
.

