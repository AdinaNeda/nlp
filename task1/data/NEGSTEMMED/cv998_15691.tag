There
wa
a
time
when
John
Carpent
wa
a
great
horror
director
.

Of
cours
,
hi
best
film
wa
1978
's
masterpiec
,
``
Halloween
,
''
but
he
also
made
1980
's
``
The
Fog
,
''
and
1987
's
underr
,
``
Princ
of
Dark
.
''

Heck
,
he
even
made
a
good
film
in
1995
,
with
``
In
the
Mouth
of
Mad
.
''

But
someth
terribl
wrong
happen
to
him
in
1992
,
with
the
terribl
comedi
,
``
Memoir
of
an
Invisibl
Man
.
''

Somehow
,
Carpent
ha
lost
hi
touch
,
with
junk
like
hi
fail
1995
remak
of
,
``
Villag
of
the
Damn
,
''
to
hi
uninspir
1996
sequel
,
``
Escape
From
L.A.
''
Those
movi
,
howev
,
look
like
cinemat
work
of
art
compar
to
hi
latest
film
,
``
John
Carpent
's
Vampir
.
''

If
I
wa
him
,
I
defin
would
n't
want
to
put
my
own
name
in
the
titl
.

It
is
a
sad
state
of
affair
when
Carpent
can
make
someth
as
misguid
and
flatli
written
and
film
as
,
``
Vampir
.
''

The
stori
is
simpl
.

Jack
Crow
-LRB-
Jame
Wood
-RRB-
is
a
vampir
hunter
who
,
along
with
on
of
hi
partner
,
Montoya
-LRB-
Daniel
Baldwin
-RRB-
,
and
a
prostitut
,
Katrina
-LRB-
Sheryl
Lee
-RRB-
,
surviv
an
attack
from
the
Master
vampir
,
Valek
-LRB-
Thoma
Ian
Griffith
-RRB-
.

Sinc
Katrina
wa
previous
bitten
by
him
,
Crow
take
her
along
becaus
anyon
who
is
bitten
by
Valek
becom
telepath
link
to
him
until
thei
themselv
turn
into
vampir
a
coupl
dai
later
,
and
Crow
is
hope
to
find
him
with
the
help
of
her
.

It
seem
Valek
's
mission
is
to
steal
a
black
,
wooden
cross
from
a
Roman
Cathol
church
that
will
enabl
him
to
becom
so
power
that
sunlight
will
not
destroi
him
.

My
question
is
:
how
mani
time
have
we
seen
thi
same
stori
plai
out
?

Well
,
the
answer
is
just
about
as
mani
time
as
a
better
version
of
the
stori
ha
been
made
.

``
John
Carpent
's
Vampir
,
''
sadli
enough
,
is
on
of
the
most
unscari
horror
film
I
've
ever
seen
.

In
fact
,
there
is
n't
even
on
suspens
moment
in
the
whole
105-minut
run
time
.

The
non-stop
vampir
attack
sequenc
ar
stylelessli
film
,
without
ani
interest
camera
work
,
which
is
usual
a
trademark
of
Carpent
's
.

And
then
we
come
to
the
screenplai
,
which
,
as
far
as
I
can
tell
,
is
nearli
non-exist
.

There
is
no
stori
develop
,
and
there
is
n't
even
an
attempt
to
flesh
out
the
charact
.

Jame
Wood
can
be
a
good
actor
,
but
he
ha
noth
to
do
here
but
to
sai
a
coupl
of
``
pseudo
''
-
clever
line
of
dialogu
.

Daniel
Baldwin
ha
some
potenti
,
but
hi
charact
come
off
as
be
veri
dens
.

And
Sheryl
Lee
-LRB-
fare
much
better
as
Laura
Palmer
in
``
Twin
Peak
''
-RRB-
,
like
all
of
the
femal
charact
,
plai
an
offens
stereotyp
whore
.

There
is
not
an
ounc
of
intellig
,
or
excit
in
,
``
John
Carpent
's
Vamir
,
''
which
is
veri
dishearten
come
from
an
ex-fan
of
Carpent
's
.

He
ha
said
that
he
turn
down
direct
,
``
Halloween
:
H20
,
''
becaus
he
could
n't
work
up
ani
excit
for
it
.

And
yet
,
when
ask
about
a
``
Vampir
''
sequel
,
he
said
he
would
be
happi
to
do
it
.

I
think
that
's
a
definit
sign
that
Carpent
ha
final
lost
ani
trace
of
hi
last
talent
,
not
to
mention
a
signific
number
of
IQ
point
.

