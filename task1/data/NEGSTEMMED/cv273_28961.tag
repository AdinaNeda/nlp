LEFT
LUGGAGE
-LRB-
director
:
Jeroen
Krabb
;
screenwrit
:
Edwin
de
Vries/from
book
The
Shovel
And
The
Loom
by
Carl
Friedman
;
cinematograph
:
Walther
van
den
Ende
;
editor
:
Edgar
Burcksen
;
cast
:
Laura
Fraser
-LRB-
Chaja
Silberschmidt
-RRB-
,
Isabella
Rossellini
-LRB-
Mrs.
Kalman
-RRB-
,
Jeroen
Krabb
-LRB-
Mr.
Kalman
-RRB-
,
Maximilian
Schell
-LRB-
Chaja
's
Father
-RRB-
,
Mariann
Sgebrecht
-LRB-
Chaja
's
Mother
-RRB-
,
David
Bradlei
-LRB-
Concierg
-RRB-
,
Adam
Monti
-LRB-
Simcha
Kalman
-RRB-
,
Chaim
Topol
-LRB-
Mr.
Apfelschnitt
-RRB-
,
Heather
Week
-LRB-
Sofi
-RRB-
;
Runtim
:
100
;
Castl
Hill
Product
;
1998-USA
/
Belgium
/
Netherland
-RRB-
A
film
that
mean
well
,
but
is
too
pushi
in
promot
it
belabor
point
and
too
sentiment
to
be
compel
as
a
drama
.

It
's
Jeroen
Krabb
's
melodrama
about
a
commun
of
Jew
in
Antwerp
in
1972
who
ca
n't
forget
their
bitter
past
.

The
heroin
is
an
attract
,
carefre
,
nonreligi
20-year-old
,
Chaja
-LRB-
Fraser
-RRB-
,
who
want
to
forget
her
Jewish
root
by
get
involv
in
student
demonstr
,
screw
a
student
rebel
leader
,
and
live
with
her
Gentil
friend
awai
from
her
nag
mother
-LRB-
Sgebrecht
-RRB-
and
her
self-absorb
,
eccentr
father
-LRB-
Schell
-RRB-
.

Her
parent
ar
survivor
of
the
Holocaust
concentr
camp
,
and
her
home
life
is
fill
with
angst
.

Her
mother
is
in
denial
about
the
past
alwai
busi
herself
by
cook
soup
and
bake
cake
and
complain
about
everyth
,
while
her
father
is
more
openli
love
but
ha
recent
been
absorb
in
search
for
two
suitcas
fill
with
memento
,
a
famili
album
,
silverwar
,
and
hi
old
violin
.

He
buri
them
in
a
garden
dure
the
war
while
flee
the
Nazi
,
but
ca
n't
locat
them
now
due
to
all
the
chang
in
the
citi
.

Out
of
frustrat
after
quit
her
job
as
a
dishwash
and
face
evict
from
her
apart
,
she
reluctantli
accept
a
job
as
a
nanni
with
an
Hasid
coupl
that
an
old
man
in
her
parent
'
apart
build
,
Mr.
Apfelschnitt
-LRB-
Topol
-RRB-
,
tell
her
about
.

The
Hasid
ar
ultra-orthodox
:
thei
do
n't
go
to
the
cinema
or
watch
TV
,
adher
to
strict
dress
code
,
and
strictli
observ
their
religi
law
.

Chaja
is
at
first
put
off
by
the
wai
thei
expect
her
to
follow
their
rule
,
but
soon
find
the
wife
,
Mrs.
Kalman
-LRB-
Rossellini
-RRB-
,
to
be
kind
and
she
becom
attach
to
on
of
herchildren
,
a
4-year-old
name
Simcha
-LRB-
Monti
-RRB-
who
doe
n't
talk
.

The
stern
Mr.
Kalman
-LRB-
Krabb
-RRB-
is
not
friendli
to
her
and
consid
her
to
be
a
whore
becaus
of
the
wai
she
dress
.

To
hammer
home
the
point
of
how
anti-Semit
ha
n't
gone
awai
there
's
a
barrag
of
overdon
and
uninvolv
scene
with
a
sneer
Concierg
-LRB-
Bradlei
-RRB-
,
who
keep
make
nasti
remark
about
Jew
and
tri
to
make
thing
inconveni
for
them
by
prevent
the
Jewish
famili
from
us
the
elev
.

The
film
is
done
in
by
it
ham-fist
script
as
the
stori
,
adapt
from
Carl
Friedman
's
book
``
The
Shovel
And
The
Loom
,
''
goe
from
on
fals
note
to
anoth
until
it
get
lost
in
all
the
goo
of
it
sentiment
.

It
start
off
tell
Chaja
's
stori
of
how
she
's
a
lost
Jewess
try
to
find
her
ident
,
then
to
her
nanni
role
as
she
love
a
mute
child
stuck
in
an
insul
environ
,
and
then
it
make
it
a
stori
about
the
linger
effect
of
the
bitter
past
that
ca
n't
be
forgotten
.

The
effort
seem
heavy-hand
,
as
the
film
kept
deliv
too
mani
obviou
messag
that
it
ponder
kept
deliv
through
the
stock
charact
.

The
support
cast
consist
of
all
wooden
charact
who
give
their
role
a
cartoonish
flavor
:
Chaja
's
parent
ar
given
no
human
shade
,
while
the
janitor
villain
wa
a
particularli
annoi
role
that
wa
one-dimension
and
fals
act
.

The
onli
on
who
fought
through
the
script
and
show
some
feel
were
Fraser
,
whose
effervesc
face
wa
express
of
both
the
try
time
she
wa
go
through
and
the
joi
she
felt
--
but
most
admir
show
how
she
could
be
so
mistaken
as
to
think
that
she
could
forget
her
root
;
while
Rossellini
gave
a
warm
perform
of
a
woman
suffer
in
silenc
,
but
is
strong
in
accept
her
faith
.

Topol
's
reassur
perform
as
the
wise
man
who
sai
all
the
right
thing
to
Fraser
,
act
as
the
true
voic
of
the
filmmak
in
explain
all
the
suffer
with
common
sens
.

The
film
brazenli
us
littl
Simcha
to
get
across
it
agenda
of
point
out
how
the
patriarch
world
can
be
cruel
when
it
ca
n't
love
:
it
start
out
by
show
how
under
Chaja
's
love
care
he
's
taken
to
the
duck
pond
and
soon
start
jabber
awai
,
begin
by
sai
`
quack
,
quack
'
and
then
go
on
to
ask
the
four
question
dure
a
Passov
seder
.

But
by
show
how
the
boi
is
so
terrifi
of
hi
strict
father
that
he
wet
hi
pant
in
hi
presenc
,
refus
to
speak
becaus
of
hi
stern
dad
,
and
eventu
becom
the
victim
of
a
tragic
accid
,
the
film
therebi
exploit
the
boi
's
suffer
and
hi
stori
just
becom
tiresom
and
not
sincer
done
.

I
felt
I
did
n't
just
see
a
movi
,
but
I
attend
a
lectur
for
the
whole
100
minut
of
thi
seriou
but
unappet
stori
.

It
wa
the
kind
of
movi
that
you
hope
would
somehow
end
soon
,
as
it
seem
to
be
in
the
habit
of
rehash
it
same
viewpoint
unnecessarili
--
the
messag
it
keep
send
wa
alreadi
receiv
.

In
the
last
shot
where
father
and
daughter
ar
hopelessli
dig
for
the
lost
luggag
,
on
ha
the
impress
that
no
on
in
the
film
learn
anyth
about
themselv
or
the
past
.

That
seem
strang
,
sinc
I
thought
that
wa
what
thi
film
wa
suppos
to
be
about
.

Unless
I
wa
mistaken
and
the
film
's
real
aim
wa
to
make
us
cry
over
Simcha
.

Denni
Schwartz
:
``
Ozu
'
World
Movi
Review
''
ALL
RIGHTS
RESERVED
DENNIS
SCHWARTZ

