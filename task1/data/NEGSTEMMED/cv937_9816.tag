Star
Bill
Murrai
and
Karen
Allen
Direct
by
Richard
Donner
-LRB-
Lethal
weapon
,
The
Omen
,
Superman
-RRB-
A
Paramount
Pictur
Releas
Take
a
look
at
the
follow
equat
...
.

A
CHRISTMAS
CAROL+GHOSTBUSTERSSCROOGED
Ye
,
SCROOGED
is
the
odd
mixtur
of
sentiment
,
comedi
and
horror
you
would
get
if
you
mix
those
two
element
togheth
.

SCROOGED
is
altern
sick
,
gross
,
funni
,
and
then
sickli
soppi
.

Bill
Murrai
plai
Frank
Cross
,
a
T.V
execut
with
a
horribl
person
.

He
's
evil
to
secretari
,
actor
,
crew
,
everyon
,
except
the
T.V
's
station
's
boss
,
of
cours
-LRB-
plai
by
the
late
Robert
Mitchum
-RRB-
Howev
,
he
is
then
visit
by
a
veri
dead
exec
,
who
warn
Cross
that
he
will
be
visit
by
three
ghost
,
past
,
present
and
futur
-LRB-
who
is
call
The
Ghost
of
Yet
to
Come
,
for
some
reason
-RRB-
Sure
enough
,
thei
arriv
,
show
Cross
how
much
of
a
S.O.B
he
is
,
and
he
chang
hi
wai
.

Howev
,
throughout
thi
simpl
plot
,
we
've
got
to
suffer
outlandish
special
effect
,
poor
comedi
,
and
an
veri
,
veri
mean
perform
from
Bill
Murrai
.

Frank
Cross
is
n't
`
funni
'
mean
,
he
's
just
mean
.

He
also
is
n't
veri
good
at
emot
scene
,
and
total
destroi
the
last
ten
minut
of
the
film
,
with
an
utterli
desper
speech
sai
how
great
christma
is
,
and
how
he
ha
chang
.

Howev
,
the
support
cast
ar
OK
,
with
good
perform
from
Mitchum
,
Allen
-LRB-
who
plai
hi
girlfriend
-RRB-
and
John
Glover
-LRB-
who
plai
Cross
's
`
partner
'
-RRB-
The
ghost
of
christma
past
is
n't
that
bad
either
.

Sadli
,
though
,
the
audienc
ha
to
sufferminut
of
`
Home
Alone
'
style
violenc
from
the
ghost
of
christma
present
,
plai
by
Carol
Kane
.

Whoever
thought
smack
that
Kane
smack
Murrai
in
the
head
with
a
toaster
wa
funni
,
should
be
fire
straight
awai
.

And
the
audienc
also
ha
to
suffer
Bobcat
Goldthwait
-LRB-
the
gui
with
the
annoi
voic
in
Polic
Academi
3
,
if
I
remeb
correctli
...
-RRB-
who
,
thankfulli
,
dose
n't
sai
much
.

The
script
is
horrend
.

Michael
O'Donaghu
churn
out
terribl
,
bad
tast
joke
-LRB-
which
I
guess
is
the
whole
point
realli
-RRB-
then
chang
direct
complet
to
emot
scene
.

And
he
must
of
been
on
some
drug
when
he
wrote
the
final
ten
minut
,
which
ar
aw
.

The
special
effect
look
nice
,
but
do
noth
for
the
film
.

There
's
some
impress
make
up
effect
also
.

The
music
is
also
good
,
which
is
score
by
Danni
Elfman
.

But
great
effect
and
make
up
do
n't
make
a
great
film
.

SCROOGED
is
an
appal
attempt
to
inject
some
christma
spirit
into
the
audienc
,
see
as
the
firsthourminut
of
the
film
ar
so
depress
anywai
,
and
the
last
ten
minut
had
to
make
up
for
it
with
an
godaw
speech
.

Why
did
n't
Cross
just
look
out
hi
exec
window
,
and
ask
a
young
boi
to
bui
a
goos
for
him
?

Overal
,
then
,
you
'd
have
a
much
better
christma
if
you
avoid
thi
film
like
someth
that
should
be
avoid
-LRB-
perhap
a
plagu
-RRB-

