In
thi
year
's
summer
movi
preview
issu
of
_
Entertainment_Weekli
_
,
Theresa
Connelli
describ
her
writing-direct
debut
,
_
Polish_Wed
_
,
as
``
a
child
that
did
not
quit
becom
the
child
I
thought
it
would
.
''

One
wonder
what
exactli
she
origin
had
in
mind
for
thi
jumbl
film
,
a
comedy-drama
that
appear
doom
at
it
most
basic
element
.

The
famili
at
the
center
of
_
Polish_Wed
_
is
the
Pzoniak
,
which
consist
of
mother
Jadzia
-LRB-
Lena
Olin
-RRB-
,
father
Bolek
-LRB-
Gabriel
Byrn
-RRB-
,
sole
daughter
Hala
-LRB-
Clair
Dane
-RRB-
and
four
son
of
vari
degre
of
faceless
.

It
's
a
larg
famili
,
but
there
's
not
a
sympathet
on
in
the
whole
bunch
,
certainli
not
in
the
primari
trio
.

Jadzia
take
pride
in
build
and
maintain
a
home
and
famili
,
but
she
's
kind
of
a
hypocrit
sinc
she
's
carri
on
an
affair
with
a
businessman
Rade
Serbedzija
-RRB-
.

Her
excus
for
her
affair
is
neglect
from
Bolek
,
who
is
such
a
passiv
wimp
that
on
can
not
connect
with
hi
sad
and
frustrat
.

Also
,
how
could
he
possibl
pai
so
littl
attent
to
the
sauci
,
sexi
Jadzia
?

Hala
is
a
spoil
,
self-cent
high
school
dropout
whose
reckless
sexual
experiment
predict
lead
to
pregnanc
.

With
such
an
unapp
set
of
charact
,
it
's
no
surpris
that
_
Polish_Wed
_
's
plot
complic
ar
far
from
involv
.

Natur
,
Jadzia
and
Bolek
would
like
Hala
to
marri
the
young
cop
,
Russel
Schuster
-LRB-
Adam
Trese
-RRB-
,
who
father
the
child
,
but
he
refus
to
make
such
a
commit
.

Ho-hum
.

Another
complic
,
involv
the
decidedli
un-virgin
Hala
be
select
to
crown
a
statu
of
the
Virgin
Mari
,
is
first
plai
for
laugh
and
then
,
inexplic
,
as
a
Profound
Statement
in
the
film
's
climax
--
which
,
iron
,
is
funnier
than
ani
of
the
film
's
lame
attempt
at
humor
,
such
as
a
painfulli
labor
slapstick
attempt
where
Jadzia
lead
her
son
in
a
charg
to
beat
up
Russel
.

That
scene
is
but
on
in
a
number
of
write
miscu
by
Connelli
.

The
Jadzia-Bolek
conflict
is
resolv
in
an
overli
pat
wai
not
unfamiliar
to
sitcom
viewer
.

The
Hala-Russel
conflict
is
n't
resolv
in
as
contriv
a
manner
,
but
their
ultim
resolut
will
leav
viewer
wonder
if
thei
had
miss
someth
.

And
then
there
's
some
atroci
dialogu
,
which
I
am
sure
wa
not
suppos
to
be
as
ridicul
as
thei
sound
:
``
Look
at
all
these
pickl
.
''

Just
look
at
them
give
me
such
great
sad
.
''
''

As
misguid
as
_
Polish_Wed
_
is
,
the
affair
is
someth
of
a
letdown
,
consid
the
strong
perform
by
Byrn
,
Dane
,
and
especi
the
fieri
Olin
.

Thei
obvious
believ
in
Connelli
and
her
materi
--
a
faith
that
audienc
will
be
hard-press
to
share
.

