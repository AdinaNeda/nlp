Take
two
old
and
dy
men
,
a
lifetim
of
regret
,
a
hous
full
of
sin
,
a
thoroughli
despic
man
,
enough
li
,
insecur
and
other
charact
defect
to
keep
a
team
of
psychiatrist
gainfulli
emploi
,
then
add
a
inexplic
meteorolog
and
amphibian-bas
phenomenon
and
you
will
have
sum
up
MAGNOLIA
,
the
newest
film
from
Paul
Thoma
Anderson
-LRB-
BOOGIE
NIGHTS
-RRB-
.

The
movi
tell
multipl
stori
,
weav
them
togeth
,
or
overlap
them
dure
the
cours
of
it
three
hour
run
time
.

Would
that
the
stori
were
worth
the
tell
.

Earl
Partridg
-LRB-
Jason
Robard
,
A
THOUSAND
ACRES
-RRB-
is
dy
of
cancer
.

Bedridden
,
in
much
pain
,
it
is
obviou
that
hi
time
is
grow
short
.

Hi
much
younger
wife
plai
by
Juliann
Moor
-LRB-
AN
IDEAL
HUSBAND
-RRB-
is
surpris
to
find
herself
struggl
with
hi
impend
death
.

Have
marri
for
monei
,
she
discov
that
she
ha
actual
fallen
in
love
with
the
old
gui
and
regret
have
cheat
and
li
.

Earl
regret
have
cheat
on
hi
first
wife
and
estrang
himself
from
hi
onli
son
-LRB-
Tom
Cruis
,
EYES
WIDE
SHUT
-RRB-
,
now
a
misogynist
self-help
guru
who
teach
men
how
to
``
Seduc
and
Destroi
,
''
Hi
sexual
and
vulgar
perspect
on
male/femal
relationship
is
so
over-the-top
as
to
be
utterli
unbeliev
.

Meanwhil
,
Jimmi
Gator
-LRB-
Philip
Baker
Hall
,
THE
INSIDER
-RRB-
is
also
dy
of
cancer
.

Not
as
physic
incapacit
as
Earl
,
Jimmi
is
still
abl
to
perform
hi
duti
as
the
lovabl
host
of
a
long-run
quiz
show
which
pit
adult
against
children
.

The
current
whiz-quiz-kid
is
Stanlei
Spector
-LRB-
Jeremi
Blackman
in
hi
film
debut
-RRB-
who
is
tire
of
the
pressur
of
perform
and
is
will
to
call
it
quit
.

On
the
other
hand
,
quiz
kid
Donni
Smith
-LRB-
William
Maci
,
MYSTERY
MEN
-RRB-
would
like
noth
more
than
to
return
to
the
spotlight
when
he
wa
a
``
somebodi
.
''

Hi
pathet
life
is
sour
as
he
can
no
longer
capit
on
hi
briefminut
of
fame
which
Stanlei
is
all
too
eager
to
relinquish
.

Jimmi
ha
hi
own
famili
crisi
as
hi
drug-addict
daughter
-LRB-
Melora
Walter
,
BOOGIE
NIGHTS
-RRB-
refus
to
have
anyth
to
do
with
him
for
reason
which
ar
not
disclos
to
us
until
the
end
of
the
film
.

Grab
at
on
last
attempt
at
happi
she
reach
out
to
a
softheart
cop
-LRB-
John
C.
Reilli
,
NEVER
BEEN
KISSED
-RRB-
even
as
she
tri
to
push
herself
awai
from
him
becaus
she
deem
herself
not
worthi
of
hi
affect
.

Thi
dysfunct
group
carri
on
for
what
seem
to
be
an
intermin
two-third
of
the
movi
.

And
then
it
get
wors
,
liter
rain
frog
.

Ye
,
frog
.

Assume
it
to
be
an
intend
deus-ex-machina
devic
,
it
is
an
ineffect
on
becaus
it
doe
n't
seem
to
faze
the
charact
much
.

Oh
,
thei
mai
step
gingerli
around
the
splatter
frog
corps
litter
the
street
,
but
otherwis
,
the
frog
shower
did
n't
seem
to
chang
their
behavior
or
pattern
of
live
-LRB-
or
dy
-RRB-
.

There
is
simpli
too
much
go
on
in
thi
movi
and
most
of
it
is
distast
to
watch
.

Mr.
Anderson
further
obscur
the
film
by
incorpor
a
loud
and
intrus
sound
track
that
often
drown
out
the
dialogu
,
a
charact
who
appar
rap
a
signific
clue
to
a
plot
develop
which
wa
complet
unintelligbl
,
and
a
heavi
hand
segment
of
``
histor
''
occur
contain
iron
twist
which
set
up
absolut
noth
.

One
of
the
recur
theme
is
found
in
a
line
Donni
quot
:
``
We
mai
be
done
with
the
past
,
but
the
past
is
not
done
with
us
.
''

Thi
is
an
absolut
lie
.

God
is
in
the
forgiv
busi
.

In
fact
,
as
we
humbl
ask
for
forgiv
and
repent
or
chang
our
offend
mindset
,
God
's
Word
sai
that
He
not
onli
forgiv
,
He
also
forget
.

``
I
,
even
I
,
am
he
that
blotteth
out
thy
transgress
for
mine
own
sake
,
and
will
not
rememb
thy
sin
.
''

Isaiah
43:25
-LSB-
KJV
-RSB-
The
spiritu
on
who
keep
bring
up
our
unright
past
is
the
same
on
who
want
to
keep
us
in
a
state
of
condemn
.

Do
n't
let
him
.

Next
time
your
spiritu
adversari
remind
you
of
your
past
,
take
great
pleasur
in
remind
him
of
hi
futur
.

He
hate
that
.

