There
is
n't
much
good
about
thi
movi
.

Not
much
I
can
sai
about
the
act
,
direct
,
or
write
that
would
make
you
consid
see
thi
movi
.

So
I
'll
get
my
on
good
comment
out
of
the
wai
,
at
least
Joel
Schuemach
-LRB-
Batman
&
Robin
-RRB-
did
n't
direct
it
or
it
would
be
titl
``
Technicolor
Citi
''
.

In
fact
there
is
noth
color
about
thi
movi
,
it
dark
and
depressingli
gloomi
right
down
to
the
bitter
end
.

The
plot
ha
a
tendenc
to
be
interest
,
but
all
that
pass
while
your
laugh
at
the
ridicul
thing
thrown
into
what
could
have
been
a
fascin
movi
.

The
plot
is
imposs
to
explain
due
to
how
senseless
it
get
,
so
I
will
just
touch
on
the
bare
minimum
.

John
Murdoch
-LRB-
Rufu
Sewel
-RRB-
awak
in
the
bathtub
of
a
cheap
hotel
,
onli
to
find
out
he
ha
forgotten
everyth
.

John
must
find
out
who
he
is
befor
the
``
stranger
''
-LRB-
a.k.a.
mind
eras
alien
-RRB-
find
him
and
us
him
for
their
own
evil
conspiraci
.

Dure
the
film
some
interest
point
ar
rais
about
human
individu
,
and
the
exist
of
inher
evil
peopl
.

Any
of
these
point
howev
ar
complet
``
eras
''
from
your
mind
as
you
watch
the
actor
stumbl
through
the
dread
script
.

As
I
mention
earlier
,
noth
but
the
atmospher
is
right
in
thi
film
.

The
act
is
bland
,
and
sinc
there
is
virtual
no
charact
develop
no
on
seem
to
care
.

The
special
effect
ar
low
budget
and
some
even
hilari
fake
,
a
sign
of
a
true
``
B-movi
''
.

The
direct
is
poor
and
there
is
littl
continu
,
not
that
you
would
expect
it
in
a
movi
switch
realiti
constantli
.

Lastli
the
script
is
weak
and
ha
no
concept
of
realiti
,
and
doe
n't
deserv
to
have
the
word
``
scienc
''
in
science-fict
.

If
I
have
n't
got
my
point
across
,
I
'll
sai
it
more
plainli
:
Thi
is
a
bad
movi
.

Let
hope
the
next
movi
by
Alex
``
I
wish
I
wa
Tim
Burton
''
Proya
is
at
least
toler
.

