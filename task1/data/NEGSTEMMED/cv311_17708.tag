Drew
Barrymor
is
begin
to
corner
the
market
on
plai
the
girl
outside-th
on
who
's
the
awkward
klutz
or
the
spunki
do-it-yourself
;
the
on
who
just
doe
n't
fit
in
with
the
other
.

She
ha
perfect
these
charact
in
movi
such
as
``
The
Wed
Singer
''
and
,
most
notabl
,
``
Ever
After
.
''

Now
she
's
back
,
star
in
what
could
be
call
a
modern-dai
Cinderella
fabl
-
``
Never
Been
Kiss
.
''

You
know
it
's
a
fabl
becaus
she
plai
a
copi
editor
at
a
newspap
who
ha
her
own
offic
as
well
as
a
secretari
.

Trust
me
on
thi
on
,
no
copi
editor
ha
seen
the
insid
of
a
privat
offic
sinc
Gutenberg
-LRB-
and
I
do
n't
mean
Steve
-RRB-
invent
the
print
press
.

The
premis
is
simpl
.

Barrymor
's
Josi
Geller
,
atth
youngest
copi
editor
ever
to
be
hire
by
the
Chicago
Sun-Tim
,
is
assign
to
go
undercov
and
return
to
high
school
to
do
an
expos
on
what
todai
's
teen
ar
feel
and
do
.

Josi
-LRB-
she
sai
she
wa
name
after
the
'70
cartoon
charact
-RRB-
wa
a
geek
in
high
school
,
so
she
jump
at
the
opportun
for
a
second
chanc
.

Thi
time
,
she
think
,
she
will
get
it
right
and
be
accept
by
the
in-crowd
.

Now
,
what
kind
of
adult-with
a
good
job
and
a
success
career-would
actual
look
forward
to
reliv
the
hell
that
was-and
is-high
school
and
adolesc
.

These
ar
among
the
mani
problem
that
plagu
``
Never
Been
Kiss
.
''

Screenwrit
Abby
Kohn
and
Marc
Silverstein
can
not
get
a
handl
of
Josi
.

Their
script
ha
her
caprici
switch
from
confid
adult
to
ditzi
,
blubber
woman-child
at
the
least
provoc
.

And
the
fact
that
an
adult
would
put
so
much
stock
into
try
to
becom
tight
with
the
vapid
airhead
who
ar
supposedli
South
Glen
High
School
's
most
popular
girl
lead
you
to
question
her
matur
and
mental
stabil
.

OK
,
so
``
Never
Been
Kiss
''
is
not
a
sociolog
expos
of
todai
's
high
school
scene
.

Howev
,
certain
rule
should
appli
to
film
,
and
on
of
those
is
consist
of
charact
.

Among
the
movi
's
problem
is
the
ill-conceiv
conceit
that
Josi
would
serious
strive
to
climb
the
high
school
food
chain
and
,
in
the
process
,
lose
focu
on
her
assign
.

Any
compet
editor
would
have
toss
her
ass
out
the
door
quicker
than
you
could
sai
``
get
me
rewrit
.
''

To
be
fair
,
Barrymor
is
veri
appeal
,
but
she
is
given
veri
littl
to
work
with
.

She
tri
valiantli
to
get
a
firm
grip
on
her
charact
,
but
the
script
continu
undermin
her
.

Barrymor
come
off
best
in
the
physic
comedi
aspect
of
the
script
in
which
she
tri
to
walk
,
talk
and
act
like
a
cool
,
hip
high
schooler
.

Otherwis
,
she
is
left
founder
on
a
clich
sea
of
teen-ag
stereotyp
and
situat
.

``
Never
Been
Kiss
''
is
entertain
and
funni
in
fit
and
start
.

It
lack
consist
and
a
firm
grasp
on
what
it
want
to
accomplish
.

The
movi
's
main
bright
spot
is
provid
by
Leele
Sobieski
as
Aldy
,
the
outsid
who
befriend
new
student
Josi
.

Josi
see
a
lot
of
her
former
self
in
Aldy
,
yet
still
abandon
her
to
hang
out
with
the
popular
girl
.

Real
matur
.

And
that
is
the
main
defici
with
``
Never
Been
Kiss
.
''

It
's
illog
,
unrealist
,
uneven
and
undemand
.

It
ha
some
warm
and
humor
spot
,
but
not
enough
to
overcom
it
mani
obstacl
.

Bob
Bloom
is
the
film
critic
at
the
Journal
and
Courier
in
Lafayett
,
IN
.

He
can
be
reach
by
e-mail
at
bloom@journal-courier.com
or
at
cbloom@iquest.net

