There
have
been
Merchant-Ivori
costum
drama
with
more
of
a
puls
than
``
The
Mod
Squad
,
''
a
self-consci
``
hip
''
cinemat
render
of
the
old
TV
seri
still
look
upon
fondli
by
so
mani
baby-boom
.

Well
,
said
``
Squad
''
certainli
wo
n't
be
a
pleasant
view
experi
for
them
or
anybodi
els
,
mayb
even
the
teen
target
audienc
the
movi
ha
been
gear
toward
.

A
contemporari
take
on
thi
decidedli
'
70
show
doe
n't
exactli
seem
unwarr
,
but
on
wonder
if
the
mold
it
accumul
while
wait
on
the
shelf
did
n't
transform
into
a
full-blown
case
of
botul
.

How
curiou
that
the
film
begin
by
defin
both
mod
and
squad
,
insist
that
the
latter
is
a
group
of
peopl
work
togeth
and
then
contradict
thi
definit
by
keep
it
titular
trio
apart
for
a
sizeabl
chunk
of
the
run
time
.

Thei
ar
Juli
,
Pete
and
Linc
,
reform
delinqu
work
undercov
for
the
LAPD
-LRB-
exposit
put
out
of
the
wai
so
fast
that
you
're
like
to
be
lost
from
the
open
moment
on
-RRB-
,
and
thei
ar
respect
plai
by
Clair
Dane
,
Giovanni
Ribisi
and
Omar
Epp
,
talent
actor
each
deserv
of
better
than
thi
.

Their
plight
involv
standard
cop-corrupt
stuff
,
as
our
would-b
protagonist
catch
wind
of
an
intern
cover-up
after
their
superior
-LRB-
reliabl
Denni
Farina
,
on
of
the
best
thing
here
and
gone
so
quickli
-RRB-
get
kill
and
frame
for
drug
traffick
.

Thei
pout
a
lot
and
eventu
get
crack
to
expos
thi
convolut
conspiraci
us
surveil
tactic
that
would
impress
the
Hardi
boi
and
Linda
Tripp
but
few
other
.

When
you
're
suppos
to
be
ask
,
``
What
's
go
to
happen
next
?
''

,
you
'll
instead
entertain
thought
like
``
Who
ar
these
peopl
and
why
should
I
care
?
''

or
``
Are
n't
thriller
suppos
to
contain
thrill
?
''

Not
that
Dane
,
Epp
and
Ribisi
do
n't
give
it
a
shot
.

Dane
can
do
the
troubl
teen
thing
in
her
sleep
,
as
evidenc
by
``
My
So-Cal
Life
,
''
but
she
's
saddl
with
a
mysterious-boyfriend
-LRB-
Josh
Brolin
-RRB-
subplot
so
see-through
you
begin
to
serious
question
her
so-cal
intellig
.

Ditto
for
Ribisi
's
-LRB-
``
Save
Privat
Ryan
''
-RRB-
loonei
loos
cannon
,
though
at
least
he
perform
with
a
wild-and-crazi
vigor
that
occasion
demand
attent
.

But
Epps-poor
Epp
.

Epp
-LRB-
``
Higher
Learn
''
-RRB-
is
so
short-chang
he
's
reduc
to
liter
wait
around
for
a
bad
gui
to
chase
him
.

All
thi
sloppi
can
be
attribut
to
the
screenwrit
,
on
of
whom
,
Scott
Silver
,
is
also
the
director
.

Thei
must
think
that
if
thei
dress
up
their
stupid
stori
in
such
spiffi
trap
-LRB-
the
look
of
the
film
is
realli
quit
impress
-RRB-
,
it
'll
somehow
pai
off
,
but
thi
``
Mod
Squad
''
plod
anywai
.

Charact
ar
non-exist
;
present
ar
just
some
good-look
young
thing
model
cool
Levi
and
cooler
attitud
.

Plot
hardli
escap
confus
convent
.

And
the
on
genr
element
you
'd
think
would
be
show
up
in
gener
portions-a
few
nifti
explos
,
some
fight
,
ani
kind
of
action
whatsoever-onli
rare
make
it
to
thi
dull
gabfest
.

All
those
quick
to
put
down
last
month
's
inept
but
servic
``
My
Favorit
Martian
''
updat
need
to
take
a
step
back
.

Here
's
a
small-to-big-screen
translat
that
realli
should
've
stai
in
it
former
incarn
,
``
Mod
''
or
not
.

