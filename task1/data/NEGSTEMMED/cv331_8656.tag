EVENT
HORIZON
Star
:
Laurenc
Fishburn
-LRB-
Captain
Joe
Miller
-RRB-
,
Sam
Neill
-LRB-
Dr.
William
Wier
-RRB-
,
Kathleen
Quinlan
-LRB-
Peter
-RRB-
Direct
by
:
Paul
Anderson
Written
by
:
Phillip
Eisner
The
event
horizon
is
the
boundari
of
a
black
hole
...
and
,
in
the
futur
,
it
's
also
the
name
of
a
spaceship
that
vanish
when
it
tri
to
go
faster
than
the
speed
of
light
by
travers
through
it
own
portabl
black
hole
.

Natur
,
thi
is
the
setup
for
the
other
EVENT
HORIZON
...
the
movi
.

When
the
ship
suddenli
appear
at
the
point
that
it
vanish
at
,
just
outsid
of
Neptun
,
the
crew
of
the
ship
call
the
``
Lewi
and
Clark
''
ha
to
go
to
investig
,
and
Dr.
William
Weir
,
the
creator
of
the
``
Event
Horizon
,
''
get
to
come
along
for
the
ride
.

If
you
've
glanc
at
a
poster
for
thi
movi
,
you
know
that
,
supposedli
,
what
follow
is
``
Infinit
Terror
.
''

Well
,
I
do
n't
know
about
the
``
Terror
''
part
,
but
``
Infinit
''
certainli
seem
to
qualifi
...
EVENT
HORIZON
is
a
mean-spirit
,
gori
,
sick
excus
for
a
film
.

It
start
off
as
an
ALIEN
rip-off
,
and
then
degener
into
a
pointlessli
bloodi
slasher
flick
after
aboutminut
.

At
least
for
those
first
40
minut
it
's
an
interest
rip-off
of
ALIEN
.

That
's
a
lot
more
than
can
be
said
for
the
rest
of
them
movi
,
which
seem
to
go
on
forev
.

It
not
a
scari
monster
movi
,
nor
is
it
a
psycholog
thriller
.

It
is
simpli
a
movi
which
tri
again
and
again
to
make
the
viewer
feel
sick
to
their
stomach
at
the
site
of
extrem
gore
.

You
've
seen
thi
kind
of
movi
befor
.

The
HELLRAISER
seri
had
plenti
of
scene
contain
thi
sensibl
.

Thi
is
a
kind
of
movi
for
which
I
have
no
respect
,
a
kind
of
movi
which
I
had
hope
di
out
year
ago
.

It
is
quit
possibl
for
a
movi
to
frighten
or
shock
it
audienc
without
resort
to
noth
more
than
gore
,
but
EVENT
HORIZON
take
the
low
road
,
and
the
viewer
pai
the
price
.

There
ar
mani
good
,
respect
perform
,
from
Fishburn
and
Neill
most
notic
.

Paul
Anderson
's
direct
is
impress
,
as
it
wa
in
MORTAL
KOMBAT
.

There
ar
mani
nice
special
effect
,
mostli
deal
with
numer
common
object
float
around
in
zero-grav
.

The
set
ar
beauti
to
gaze
upon
.

And
Richard
T.
Jone
'
charact
-LRB-
Cooper
-RRB-
is
hilari
and
extrem
likabl
-LRB-
more
so
than
most
charact
I
've
seen
in
recent
movi
-RRB-
for
the
littl
screen
time
that
he
ha
.

And
none
of
thi
can
save
the
movi
from
be
a
D-grade
level
of
film
.

The
idea
of
a
ship
that
ha
some
sort
of
life
within
it
wall
,
a
life
that
know
the
fear
and
inner
skeleton
of
the
human
passeng
,
is
a
promis
on
.

A
haunt
hous
stori
in
space
is
n't
such
a
bad
idea
.

But
the
final
execut
is
extrem
disappoint
.

That
's
not
to
sai
that
Philip
Eisner
-LRB-
here
make
hi
featur
film-writ
debut
-RRB-
doe
n't
have
talent
.

For
those
firstminut
,
he
manag
to
make
a
tire
premis
interest
,
ad
in
aspect
of
uniqu
flavor
into
a
rip-off
stori
,
and
that
's
someth
that
not
too
mani
writer
can
do
.

He
even
ha
on
scene
that
,
though
it
also
tri
to
sicken
the
audienc
,
ha
a
honestli
tens
feel
to
it
-LRB-
the
scene
in
question
deal
with
on
of
the
crew
be
in
an
airlock
while
other
try
to
save
him
from
float
into
space
-RRB-
.

He
definit
ha
someth
of
a
gift
,
but
it
's
not
fulli
on
displai
here
.

I
certainli
will
look
forward
to
see
more
from
him
,
and
I
hope
that
hi
futur
work
fulli
explor
hi
talent
and
the
stori
possibl
of
the
premis
.

But
becaus
of
what
thi
movi
degener
into
,
I
have
littl
or
no
respect
,
and
got
littl
or
no
enjoy
,
out
of
it
.

If
thi
is
what
we
have
to
look
forward
to
in
the
futur
of
film
,
then
I
agre
with
Laurenc
Fishburn
's
sentiment
-
``
God
help
us
.
''

