Here
's
a
rariti
:
a
children
's
film
that
attempt
to
tackl
a
weighti
subject
,
Is
there
a
God
?

Done
well
,
it
could
have
been
a
gem
among
the
wasteland
of
modern
children
's
cinema
.

Unfortun
,
it
is
n't
.

With
jumbl
messag
,
and
an
unclear
audienc
,
Wide
Awake
wa
better
left
asleep
.

Fifth
grader
Joshua
Beal
-LRB-
Joseph
Cross
-RRB-
is
in
the
middl
of
a
moral
crisi
.

Hi
belov
grandfath
-LRB-
Robert
Loggia
-RRB-
ha
di
,
and
Joshua
ha
begun
a
quest
.

He
want
to
find
God
,
to
discov
why
bad
thing
happen
.

Thi
religi
quest
is
slightli
disturb
for
hi
parent
-LRB-
Dana
Delani
and
Deni
Leari
-RRB-
,
but
thei
do
their
best
to
cope
with
their
son
as
he
explor
differ
religi
faith
.

At
hi
Cathol
school
,
hi
favorit
teacher
,
Sister
Terri
-LRB-
Rosi
O'Donnel
-RRB-
,
tri
to
give
him
guidanc
,
but
thi
is
a
journei
he
must
make
on
hi
own
.

Meanwhil
,
he
is
have
the
most
moment
year
of
hi
life
.

He
ha
sever
adventur
with
hi
daredevil
best
friend
Dave
-LRB-
Timothi
Reifsnyd
-RRB-
,
he
get
hi
first
crush
,
and
begin
to
wake
up
to
the
world
around
him
while
he
is
on
hi
spiritu
journei
.

It
is
somewhat
confus
as
to
what
the
real
audienc
for
Wide
Awake
is
expect
to
be
.

On
it
surfac
,
it
appear
to
be
a
kid
's
film
.

Howev
,
it
deal
with
seriou
issu
,
and
is
like
to
be
bore
for
todai
's
instant-gratif
kid
.

And
while
it
might
seem
hearten
to
see
that
someon
is
try
to
produc
someth
thought
for
the
kidvid
audienc
,
Wide
Awake
ask
seriou
question
,
but
onli
deliv
a
cheap
gimmick
for
an
answer
.

If
there
were
a
bit
more
meat
in
the
stori
,
adult
on
a
nostalg
bent
might
get
a
kick
out
of
the
movi
.

The
actor
who
might
have
creat
a
great
cast
-LRB-
O'Donnel
,
Leari
and
Delani
-RRB-
ar
wast
in
role
that
amount
to
littl
more
than
cameo
.

The
nostalg
element
-LRB-
best
friend
,
favorit
teacher
,
first
crush
,
etc.
-RRB-
have
been
done
much
better
in
other
movi
,
and
actual
seem
more
like
filler
here
.

The
film
's
strongest
scene
ar
some
touch
flashback
depict
Joshua
's
relationship
with
hi
grandfath
.

Thei
show
more
depth
than
is
present
anywher
els
in
the
movi
.

Mayb
the
film
would
have
been
better
if
,
instead
of
plai
the
relationship
through
flashback
,
it
were
set
entir
dure
Joshua
's
last
year
with
hi
grandpa
.

It
certainli
would
have
been
more
entertain
.

Wide
Awake
can
best
be
describ
as
a
fail
experi
.

It
start
out
with
nobl
aspir
,
but
never
deliv
on
it
promis
.

Parent
who
do
take
their
children
to
see
thi
on
ought
to
be
prepar
to
answer
some
tough
question
...
that
is
if
their
kid
ar
n't
bore
to
death
first
.

