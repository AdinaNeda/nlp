The
peopl
who
popul
the
moviear
shallow
,
self-absorb
and
self-indulg
.

In
other
word
,
thei
perfectli
mirror
the
era
as
well
as
the
movi
thi
featur
depict
.

is
the
stori
of
that
well-public
New
York
disco
,
Studio
54
,
the
in-plac
in
the
'70
where
anybodi
who
wa
anybodi
went
to
be
ogl
,
photograph
and
pamper
.

The
difficulti
with
54
,
which
wa
written
and
direct
by
Mark
Christoph
,
is
that
hi
script
take
no
point
of
view
.

Christoph
neither
condemn
nor
glorifi
the
legendari
excess
that
were
Studio
54
's
hallmark
.

He
keep
an
uninvolv
distanc
,
thu
keep
us
from
form
ani
emot
attach
with
ani
of
the
protagonist
.

The
movi
's
on
main
asset
is
the
surpris
perform
by
Mike
Myer
as
Steve
Rubel
,
the
famou
owner
of
the
nightspot
.

He
is
part
rebel
,
part
dreamer
,
part
shrewd
entrepreneur
.

He
's
smart
enough
and
childlik
enough
to
pander
to
the
dream
and
desir
of
hi
clientel
,
yet
stupid
enough
to
brag
on
TV
about
hide
profit
from
the
IRS
.

Myer
,
in
hi
first
straight
charact
part
,
is
in
turn
appeal
and
appal
.

At
on
moment
he
can
try
to
pressur
a
male
employe
into
a
sexual
situat
,
then
at
the
next
moment
apolog
for
hi
bad
behavior
and
offer
the
young
man
a
hand
of
cash
.

The
stori
is
told
by
Shane
O'Shea
-LRB-
Ryan
Phillipp
-RRB-
,
a
New
Jersei
lad
who
dream
of
cross
the
river
to
the
Big
Apple
.

Shade
of
John
Travolta
's
Brooklyn-bound
Toni
in
Saturdai
Night
Fever
.

Eventual
,
Shane
doe
come
to
New
York
,
attract
Rubel
's
ey
and
is
admit
to
the
promis
land
.

Hi
look
get
him
a
job
as
a
busboi
,
and
he
is
later
promot
to
the
prestigi
posit
of
bartend
,
where
he
mix
with
and
make
drink
for
the
rich
and
famou
.

Shane
's
dream
is
to
meet
soap
star
Juli
Black
-LRB-
Neve
Campbel
-RRB-
,
a
fellow
Garden
Stater
.

But
both
charact
ar
so
sketchili
drawn
that
even
when
thei
do
hook
up
,
it
's
no
big
deal
.

The
chemistri
between
Shane
and
Juli
is
nonexist
.

is
a
veri
cold
,
uninvolv
movi
.

It
's
all
strobe
light
and
glitz
,
all
substanc
.

It
's
sort
of
like
the
music
era
it
cover
.

Bob
Bloom
is
the
film
critic
at
the
Journal
and
Courier
in
Lafayett
,
Ind.
.

He
can
be
reach
by
e-mail
at
bloom@journal-courier.com
or
at
cbloom@iquest.net

