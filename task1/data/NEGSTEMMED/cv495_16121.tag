In
the
final
of
Disnei
's
``
Mighti
Joe
Young
,
''
a
15-foot
tall
,
2000-pound
gorilla
hold
a
frighten
young
boi
in
it
clutch
as
it
toppl
from
a
crippl
Ferri
wheel
and
plummet
to
the
ground
.

Upon
impact
,
rug
everyman
and
perenni
do-good
Bill
Paxton
rush
in
,
contain
the
emot
crowd
with
an
earnest
``
move
along
now
folk
,
there
's
noth
to
see
here
.
''

OK
,
so
those
ar
n't
exactli
the
word
he
us
,
but
it
's
probabl
on
of
the
few
clich
not
utter
in
thi
unnecessari
remak
of
an
unnecessari
remak
of
that
mother
of
all
monster
movi
,
``
King
Kong
.
''

1949
's
``
Mighti
Joe
Young
''
wa
an
updat
of
that
classic
creatur
featur
,
also
base
on
Merian
C.
Cooper
's
origin
stori
-LRB-
16
year
after
''
'
Kong
''
it
appear
the
world
wa
readi
for
a
new
take
on
the
beauti
and
the
beast
fabl
-RRB-
.

Now
some
49
year
later
,
at
least
accord
to
Disnei
's
wai
of
think
,
the
world
is
readi
for
on
more
.

Not
so
.

The
onli
--
and
I
mean
onli
--
reason
to
see
the
1998
version
is
for
the
special
effect
.

And
these
,
unfortun
,
run
hot
and
cold
.

Todai
's
Joe
Young
is
design
and
produc
by
special-effect
whiz
Rick
Baker
,
who
ha
been
wow
audienc
with
hi
state-of-the-art
make-up
effect
sinc
1971
's
``
Schlock
''
-LRB-
which
,
incident
,
featur
a
Baker-enhanc
``
gorilla
''
-RRB-
.

Joe
is
a
combin
of
animatron
effect
,
comput
graphic
,
and
that
old
standard
,
a
man
in
a
monkei
suit
.

While
there
ar
occasion
flash
of
brillianc
--
Baker
's
had
a
lot
of
practic
with
simian
effect
,
after
all
,
includ
``
Gorilla
in
the
Mist
,
''
``
Greystok
:
The
Legend
of
Tarzan
,
Lord
of
the
Ape
,
''
and
the
1976
remak
of
``
King
Kong
''
--
there
's
also
some
surpris
cheesi
.

One
of
the
film
's
most
embarrass
moment
is
when
Paxton
's
band
of
African
tracker
first
encount
and
pursu
the
larger-than-lif
primat
-LRB-
in
a
scene
unashamedli
rip
off
from
``
The
Lost
World
:
Jurass
Park
''
-RRB-
.

Paxton
jubilantli
extol
the
beast
's
majest
gait
at
the
same
exact
moment
as
Joe
,
and
the
comput
effect
drive
him
,
stutter
to
a
halt
.

Also
,
if
the
film
's
produc
had
want
us
to
focu
our
attent
on
the
titular
ap
thei
should
n't
have
parad
femal
lead
Charliz
Theron
-LRB-
``
Trial
&
Error
''
-RRB-
around
in
a
seemingly-endless
wardrob
of
spaghetti-strap
top
.

Even
Joe
seem
distract
at
time
.

The
film
pile
on
the
clich
like
there
's
no
tomorrow
,
includ
the
predict
plot
-LRB-
anthro-zoologist
ship
gigant
gorilla
to
L.A.
where
urban
havoc
is
inevit
wrought
-RRB-
,
predict
villain
-LRB-
a
Lithuanian
I
think
I
overheard
someplac
-RRB-
,
predict
love
stori
-LRB-
Bill
and
Charliz
--
surpris
!
-RRB-

,
and
predict
denouement
-LRB-
''
'
twa
box-offic
receipt
that
kill
the
beast
''
-RRB-
.

Kid
rais
on
``
Men
in
Black
''
-LRB-
non-monkei
effect
also
by
Baker
-RRB-
ar
go
to
find
Ron
-LRB-
``
Tremor
''
-RRB-
Underwood
's
out
a
littl
lame
by
comparison
.

While
certainli
better
than
1978
's
``
King
Kong
Live
''
-LRB-
itself
a
lousi
sequel
to
a
not
particularli
good
remak
-RRB-
,
``
Mighti
Joe
Young
''
prove
how
the
mighti
keep
fall
.

