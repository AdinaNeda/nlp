The
work
titl
for
NO
LOOKING
BACK
wa
LONG
TIME
,
NOTHING
NEW
,
and
rare
ha
there
been
a
more
apt
name
for
a
motion
pictur
.

Even
though
thi
movi
clock
in
at
a
rel
skinni
96
minut
,
it
seem
to
run
long
enough
to
engulf
two
TITANIC
.

Writer/director
Edward
Burn
ha
trot
out
a
hacknei
storylin
,
the
trajectori
of
which
will
be
instantli
recogniz
to
anyon
who
ha
n't
spent
their
life
in
seclus
.

Instead
of
tweak
the
formula
a
littl
to
invigor
the
proceed
,
Burn
is
content
to
allow
the
film
to
rambl
aimlessli
toward
it
irritatingli
predict
conclus
,
offer
preciou
few
momentari
pleasur
along
the
wai
.

NO
LOOKING
BACK
is
domin
by
three
veri
dislik
charact
whose
constant
presenc
on
the
screen
is
pain
.

The
most
appropri
end
would
have
been
a
tripl
suicid
,
and
the
sooner
,
the
better
.

Ala
,
that
's
not
the
case
,
and
those
who
stick
with
thi
film
for
it
entir
length
will
be
forc
to
endur
the
prolong
compani
of
thi
wretch
trio
.

And
,
to
further
depress
audienc
,
Burn
ha
shot
the
entir
film
on
cold
,
raini
dai
in
a
grai
New
York
State
beach
town
.

Peek
of
sunshin
ar
few
and
far
between
.

No
wonder
the
charact
ar
all
so
miser
.

First
of
all
,
we
have
Charli
-LRB-
Burn
-RRB-
,
a
Gener
X
slacker
who
abandon
hi
girlfriend
three
year
ago
after
she
had
an
abort
,
then
spent
some
time
bum
around
in
California
befor
decid
to
come
home
.

That
girlfriend
is
Claudia
-LRB-
Lauren
Holli
-RRB-
,
and
,
after
pick
up
the
piec
of
her
life
follow
Charli
's
departur
,
she
ha
move
on
,
shack
up
with
on
of
Charli
's
old
school
buddi
,
Mike
-LRB-
Jon
Bon
Jovi
-RRB-
.

The
two
have
a
comfort
relationship
,
but
it
's
appar
to
even
a
blind
person
that
thei
're
not
right
for
each
other
.

Claudia
year
for
some
spice
in
her
life
;
Mike
want
to
settl
down
and
have
children
.

Then
Charli
re-ent
the
mix
.

So
who
,
if
anyon
,
will
Claudia
end
up
with
?

Who
care
??

NO
LOOKING
BACK
goe
to
extraordinari
length
to
make
sure
that
we
're
not
especi
interest
in
the
outcom
of
the
romant
triangl
.

So
what
if
no
on
find
happi
--
these
charact
do
n't
deserv
it
anywai
,
especi
after
wast
90
minut
of
our
time
.

Thei
ar
n't
real
peopl
--
thei
're
a
writer
's
construct
stumbl
through
a
too-obvi
storylin
.

Thei
should
know
the
end
as
well
as
we
do
.

And
Burn
should
have
given
hi
audienc
more
credit
and
present
them
with
a
plot
that
at
least
offer
a
surpris
or
two
.

Another
frustrat
thing
about
NO
LOOKING
BACK
is
that
Burn
ha
popul
the
film
with
a
group
of
potentially-interest
support
charact
.

Blyth
Danner
is
solid
as
Claudia
's
housebound
mother
,
Conni
Britton
is
suitabl
high-strung
as
Claudia
's
neurot
sister
,
and
Jennif
Esposito
is
eye-catch
as
a
bartend
in
search
of
a
littl
romanc
.

Sadli
,
all
we
get
is
quick
glimps
into
their
live
,
although
a
movi
about
ani
of
them
would
have
been
far
more
intrigu
than
the
stori
Burn
ha
chosen
to
tell
.

None
of
the
lead
perform
ar
go
to
wow
critic
with
their
thespian
attribut
.

Edward
Burn
is
push
the
edg
of
hi
limit
rang
here
.

Jon
Bon
Jovi
show
more
act
abil
than
on
might
reason
expect
from
a
singer
branch
into
a
differ
career
,
but
he
could
still
us
a
littl
polish
.

The
worst
case
is
Lauren
Holli
,
who
present
a
complet
bland
Claudia
.

As
portrai
here
,
she
's
hardli
the
kind
of
woman
who
would
inspir
even
a
moment
's
interest
,
not
to
mention
undi
love
.

Burn
'
ex
,
the
monument
untal
Maxin
Bahn
,
would
have
been
hard-press
to
do
a
less
inspir
job
.

When
he
releas
THE
BROTHERS
MCMULLEN
,
Edward
Burn
wa
rever
as
the
wunderkind
of
the
1995
Sundanc
Film
Festiv
-LRB-
Robert
Redford
ha
appar
stuck
with
him
--
the
ag
actor/director
execut
produc
thi
mess
-RRB-
.

Two
film
and
three
short
year
later
,
the
luster
ha
fade
.

Some
movi
maker
have
onli
on
good
film
in
them
.

With
back-to-back
dud
like
SHE
'S
THE
ONE
and
NO
LOOKING
BACK
to
follow
the
delight
BROTHERS
,
Burn
is
begin
to
look
like
a
member
of
that
undistinguish
club
.

