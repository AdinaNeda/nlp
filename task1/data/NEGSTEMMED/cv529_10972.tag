There
's
no
reason
to
doubt
that
DONNIE
BRASCO
is
base
,
as
it
open
credit
proclaim
,
on
a
True
Stori
.

But
if
it
's
an
accur
pictur
of
life
on
America
's
mean
street
,
than
cinemat
depict
of
organis
crime
have
had
a
much
stronger
basi
in
realiti
than
I
would
have
previous
thought
.

For
from
the
film
's
outset
,
when
a
group
of
hood
jocularli
trade
differ
opinion
on
the
merit
of
variou
automobil
-LRB-
not
unlik
a
number
of
movi
lowlif
who
recent
gave
a
Madonna
song
a
close
read
-RRB-
DONNIE
BRASCO
resembl
more
than
passingli
a
few
gangster
flick
onc
believ
to
be
work
of
fiction
.

Our
eponym
hero
-LRB-
Johnni
Depp
-RRB-
is
not
the
cheap
crook
he
first
appear
to
be
.

Brasco
is
Joseph
Piston
,
an
undercov
FBI
agent
who
ha
the
task
of
chumming-up
to
Lefti
-LRB-
Al
Pacino
-RRB-
,
a
season
hood
who
tutor
Piston
in
the
art
of
be
a
``
wisegui
''
.

Regrett
,
Piston
learn
hi
lesson
all
too
well
.

Hi
concern
for
Lefti
,
who
is
burden
with
a
heroin
addict
son
,
exacerb
hi
own
famili
troubl
:
hi
children
resent
hi
lengthi
absenc
and
hi
wife
fear
that
her
husband
's
gangster
persona
is
becom
a
littl
too
convinc
.

True
,
the
focu
on
Piston
's
famili
life
doe
deviat
from
the
typic
gangster-flick
formula
,
but
thi
subplot
never
rise
abov
it
televis
drama
origin
.

The
unhappi
coupl
trade
predict
line
and
engag
in
drawn-out
domest
squabbl
includ
an
ill-advis
marriag
guidanc
routin
that
slow
the
film
down
unnecessarili
.

The
biggest
problem
with
DONNIE
BRASCO
,
howev
,
is
that
it
wisegui
attitud
and
style
lack
the
flair
of
it
mani
predecessor
.

The
awkward
us
of
an
occasion
disco
tune
momentarili
remind
us
it
the
70
's
,
but
not
with
the
consist
and
blindingli
tacki
style
I
love
those
fluoro
colour
suit
of
Scorc
'
costum
drama
CASINO
.

More
importantli
,
Pacino
's
presenc
trigger
memori
of
hi
earlier
movi
triumph
in
which
similar
territori
wa
cover
far
more
effect
.

DONNIE
BRASCO
's
take
on
American
valu
,
for
instanc
,
is
feebl
and
obviou
.

Lefti
rambl
out
dream
of
materi
better
to
the
accompani
of
grate
inspir
music
.

Inexplic
,
Lefti
's
Horatio
Alger
inspir
gush
is
suppos
to
move
us
.

It
doe
n't
ani
more
than
hi
famili
troubl
do
,
which
ar
dealt
in
the
same
saccharin
and
obviou
manner
as
Piston
's
.

Just
think
a
minut
on
how
these
theme
were
explor
in
the
GODFATHER
movi
and
SCARFACE
.

In
these
pictur
Pacino
's
dedic
to
a
peculiar
and
bloodi
hyper-capit
wa
twist
and
confront
,
while
hi
dog
applic
of
macho
wisegui
procedur
meant
that
hi
relat
to
friend
and
famili
wa
intriguingli
dark
neither
were
spare
bloodi
retribut
for
breach
wisegui
regul
.

DONNIE
BRASCO
's
tawdri
littl
crime
,
therefor
is
n't
so
much
it
repetit
of
familiar
gangster
theme
,
but
the
shameless
wai
in
which
it
sanitis
them
.

-LRB-
Oh
and
by
the
wai
,
I
forgot
to
mention
that
the
script
also
sport
a
startl
subplot
:
Pipston
's
superior
ar
obstruct
incompet
who
ar
infuri
by
hi
constant
insubordin
is
that
a
``
Damn
you
McBain
!
''

I
hear
from
Springfield
wai
?
-RRB-

So
the
movi
is
clich
.

Big
deal
,
what
do
you
expect
from
a
gangster-flick
?

Well
you
expect
qualiti
action
sequenc
and
bravado
perform
in
abund
if
it
go
to
be
a
decent
exampl
of
the
genr
.

DONNIE
BRASCO
fail
on
both
count
.

The
action
scene
never
rise
abov
the
ordinari
and
Depp
's
woeful
perform
tend
to
smother
the
good-work
of
hi
comrad
.

Admittedli
,
Pacino
doe
noth
new
and
Michael
Madsen
-LRB-
Sonni
-RRB-
simpli
smirk
hi
wai
through
the
pictur
like
a
slightli
subdu
Mr.
White
,
but
both
possess
an
unmistak
sly
charm
.

Depp
usual
exhibit
a
bit
of
class
himself
,
but
regrett
,
be
in
the
presenc
of
on
of
hi
better
must
have
left
him
star-struck
,
for
he
is
complet
hell-bent
on
mimick
Pacino
's
well-earn
high
style
.

The
consequ
ar
pain
to
watch
.

It
no
exagger
to
sai
that
the
film
's
credibl
is
serious
strain
by
the
sight
of
the
Piston
famili
's
uncanni
abil
to
keep
a
straight
face
in
front
of
their
breadwinn
's
phoni
Brooklyn
accent
.

Perhap
we
should
take
piti
on
the
maker
of
DONNIE
BRASCO
.

Mayb
a
scrupul
adher
to
pinpoint
accuraci
demand
tier
dialogu
and
scenario
weakli
reminisc
of
classic
gangster
flick
.

If
thi
is
inde
the
case
,
sure
the
maker
of
DONNIE
BRASCO
could
have
explor
the
fascin
possibl
that
America
's
underworld
is
commit
to
emul
,
albeit
in
heavili
attenu
form
,
their
movi
namesak
.

Depp
could
have
poignantli
plai
a
simpl
cop
who
becom
a
hideou
parodi
onc
he
is
forc
to
mimic
big-screen
gangster
great
.

Who
know
what
insight
into
the
crimin
psych
could
have
arisen
?

But
then
again
,
perhap
Donni
Brasco
is
n't
so
true
after
all
.

