What
ar
we
go
to
do
with
Jim
Carrei
?

Viewer
of
televis
's
``
In
Live
Color
''
know
thi
one-man
cartoon
from
such
charact
as
Fire
Marshal
Bill
.

Viewer
also
know
that
``
In
Live
Color
''
is
a
skit-show
and
that
a
littl
of
Jim
Carrei
goe
a
long
wai
.

Unfortun
,
thi
fact
wa
forgotten
by
the
maker
of
the
Carrei
comedi
ACE
VENTURA
:
PET
DETECTIVE
.

Three
writer
,
includ
Carrei
,
work
on
the
slapstick
stori
,
which
send
a
self-styl
``
Pet
Detect
''
on
the
trail
of
a
stolen
dolphin
.

The
miss
mammal
belong
to
the
Miami
Dolphin
,
who
need
their
mascot
for
the
upcom
Superbowl
.

For
plot
porpois
,
thi
stori
work
as
well
as
ani
Three
Stoog
short
.

Carrei
get
to
do
hi
``
offici
''
schtick
as
he
snoop
around
greater
Miami
.

He
leer
and
sneer
,
crane
hi
neck
to
funni
effect
.

He
even
doe
hi
Captain
Kirk
imperson
.

Again
.

All
of
thi
is
pretti
harmless
stuff
up
until
the
point
that
you
realiz
that
the
writer
have
absolut
no
intent
of
focus
on
anyon
other
than
Carrei
.

-LRB-
Suggest
altern
titl
--
JIM
CARREY
:
WILL
DO
ANYTHING
FOR
A
LAUGH
.
-RRB-

Export
it
to
Franc
and
you
mai
have
a
hit
.

As
it
stand
,
ACE
VENTURA
is
n't
even
good
kid
's
stuff
.

The
profan
count
,
alon
,
is
too
high
.

Which
is
iron
,
sinc
children
ar
,
probabl
,
Carrei
's
best
audienc
.

The
film
doe
n't
even
have
the
goofbal
charm
of
Chri
Elliott
's
recent
no-brain
CABIN
BOY
.

Sure
,
Carrei
ha
hi
moment
.

But
what
can
you
sai
about
a
film
whose
high-point
includ
watch
Carrei
slink
around
to
the
theme
from
``
Mission
Impossibl
?
''

ACE
VENTURA
ha
on
glare
incongru
.

Amid
the
butt-jok
and
double-tak
,
the
script
take
great
pain
to
set-up
an
elabor
and
rather
funni
``
Cry
Game
''
gag
.

And
,
for
thi
intend
audienc
,
that
take
-LRB-
ahem
-RRB-
cojon
.

