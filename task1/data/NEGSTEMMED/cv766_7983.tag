Be
forewarn
.

``
Quick
,
Robin
!
''

The
anti-shark
repel
!
''
''

-
Adam
West
in
the
1966
BATMAN
featur
film
,
casual
kick
at
a
pathetic-look
rubber
shark
attach
to
hi
leg
I
had
never
thought
that
an
entri
in
the
modern
incarn
of
the
BATMAN
featur
film
would
approach
thi
level
of
campi
,
but
in
mani
instanc
BATMAN
AND
ROBIN
near
,
and
at
some
point
even
exce
thi
standard
.

Thi
is
a
disaster
bad
film
,
easili
the
worst
in
the
seri
to
date
,
and
fairli
epitom
a
cinemat
definit
of
the
word
excessive-it
's
loud
,
garish
,
and
obnoxi
,
with
pointless
,
gratuit
action
sequenc
and
set
piec
which
clutter
up
the
screen
with
elabor
product
design
to
the
point
of
overkil
.

BATMAN
AND
ROBIN
featur
the
Cape
Crusad
-LRB-
Georg
Cloonei
debut
as
Batman
,
with
Chri
O'Donnel
retur
as
Robin
-RRB-
squar
off
against
anoth
bevi
of
chemically-induc
villains-th
nefari
ice-cold
Mr.
Freez
-LRB-
Arnold
Schwarzenegg
-RRB-
,
arm
with
a
weapon
which
freez
everyth
in
it
sight
,
and
the
slinki
Poison
Ivy
-LRB-
Uma
Thurman
-RRB-
,
who
ha
the
abil
to
blow
power
love
dust
into
the
face
of
men
in
order
so
that
thei
will
fall
helplessli
in
love
with
her
-LRB-
not
that
the
dust
is
realli
necessari
to
accomplish
thi
result
,
but
whatev
-RRB-
,
and
then
dispatch
them
with
a
poison
kiss
.

By
Ivy
's
side
is
the
giant
steroid
monster
Bane
-LRB-
Jeep
Swanson
-RRB-
,
a
grunt
hulk
of
a
beast
.

The
villain
'
goal
ar
nobl
ones-Freez
steal
diamond
to
power
hi
climat
suit
-LRB-
in
order
to
keep
hi
bodi
temperatur
at
zero
degre
-RRB-
,
so
that
he
can
surviv
in
order
to
devis
a
cure
for
hi
belov
wife
-LRB-
Vendela
-RRB-
,
dy
of
a
degen
diseas
and
frozen
in
suspend
anim
,
and
Ivy
's
intent
is
to
restor
the
domin
of
plant
life
on
Earth
,
albeit
by
destroi
all
human
life
.

Meanwhil
,
on
the
homefront
,
life
at
Wayn
Manor
is
thrown
into
upheav
by
the
ill
of
butler
Alfred
Pennyworth
-LRB-
Michael
Gough
-RRB-
,
and
the
arriv
of
hi
niec
Barbara
-LRB-
Alicia
Silverston
-RRB-
.

Akiva
Goldsman
's
screenplai
for
the
film
is
ridicul
and
laughabl
bad
,
with
astonishingli
terribl
dialogu
,
lame
joke
,
and
an
aw
by-the-numb
plot
which
simpli
coast
along
and
fail
to
gener
ani
genuin
excit
.

It
make
Goldsman
's
screenplai
of
BATMAN
FOREVER
,
which
I
thought
wa
dread
,
look
posit
inspir
in
comparison
.

I
am
still
astonish
that
a
cheesi
plot
devic
which
I
'd
seen
us
in-no
joke-an
episod
of
GILLIGAN
'S
ISLAND
somehow
make
it
wai
into
a
multimillion
dollar
blockbust
product
.

Joel
Schumach
's
direct
of
BATMAN
AND
ROBIN
is
horrif
,
with
a
terribl
balanc
of
flashi
over
substanc
.

There
is
a
clear
conceit
toward
neon
in
thi
film
,
even
moreso
than
with
hi
previou
BATMAN
FOREVER
,
with
the
revamp
Batcav
helpfulli
sport
gigant
glow
emblem
for
the
Dynam
Duo
-LRB-
just
in
case
,
I
suppos
,
if
thei
ever
happen
to
forget
that
the
Batcav
is
the
headquart
of
Batman
and
Robin
-RRB-
,
and
with
neon
promin
figur
in
an
utterly-pointless
fight
sequenc
with
Bane
and
a
street
gang
over
Ivy
's
chosen
new
abod
.

Another
action
sequenc
which
fail
to
serv
ani
us
point
other
than
to
chew
up
five
minut
of
screentim
involv
an
incred
uninvolv
late-night
motorcycl
race
with
Barbara
and
some
Gotham
goon
.

Mr.
Schumach
's
focu
for
BATMAN
AND
ROBIN
appear
to
be
to
make
the
film
as
visual
strike
as
possibl
,
to
the
detriment
of
the
story-ther
ar
drastic
shift
in
the
tone
of
the
film
between
all-out
camp
and
heartfelt
drama
,
with
the
latter
complet
unconvinc
and
ineffect
.

It
is
perhap
not
the
most
promis
of
sign
when
the
group
I
wa
with
burst
out
laugh
within
twenti
second
of
the
film
's
open
,
even
befor
a
singl
line
of
dialogu
had
been
utter
.

Is
BATMAN
AND
ROBIN
suppos
to
be
campi
?

I
think
it
is-it
's
hard
to
imagin
that
the
filmmak
could
have
intend
mani
part
of
the
film
to
be
taken
at
all
serious
.

-LRB-
One
of
my
favourit
wa
when
Bane
helpfulli
grunt
``
Bomb
!
''

each
time
he
laid
down
an
explos
devic
in
the
Gotham
Observatori
.
-RRB-

Is
it
suppos
to
be
as
overli
campi
as
it
turn
out
to
be
?

I
somehow
doubt
it-th
subplot
involv
Afred
is
deliv
so
solemnli
and
with
such
grave
that
the
impress
is
made
that
the
film
is
n't
attempt
to
be
the
utter
farc
which
it
is
.

Arnold
Schwarzenegg
is
top-bil
in
the
film
as
the
villain
Mr.
Freez
,
and
is
bland
and
uninterest
,
perhap
the
worst
thing
that
a
villain
can
be
.

Mr.
Schwarzenegg
's
attempt
to
be
menac
ar
laughabl
,
and
hi
attempt
at
convei
patho
ar
laughabl
;
frankli
,
everyth
he
doe
onscreen
is
laughabl
.

By
the
end
of
the
film
,
I
wa
stifl
a
chuckl
everi
time
he
simpli
appear
onscreen
.

The
bulk
of
hi
perform
consist
of
utter
near-unintelligbl
pun
and
one-lin
featur
everi
possibl
permut
of
``
Cool
!
''

in
the
least
invent
wai
.

Georg
Cloonei
ha
been
given
veri
littl
to
do
in
BATMAN
AND
ROBIN
,
be
overshadow
by
the
villain
,
and
consequ
he
look
rather
uncomfort
in
the
film
.

Hi
Batman
is
hardli
an
impos
figur
.

Chri
O'Donnel
is
unimpress
in
a
one-not
perform
,
while
Alicia
Silverston
lackadas
fail
to
make
ani
impress
at
all
.

The
film
's
on
save
grace
?

Undoubtabl
Uma
Thurman
's
entertain
perform
as
sexi
villai
Poison
Ivy
.

Her
work
in
BATMAN
AND
ROBIN
is
certainli
over-the-top
,
but
in
a
control
fashion
which
work
splendidli
within
the
tone
of
the
film
.

Ms.
Thurman
's
comic
time
is
impecc
,
and
remind
us
that
it
take
skill
perform
to
make
campi
work
successfulli
.

-LRB-
I
'm
alreadi
start
to
posit
reassess
Jim
Carrei
's
perform
in
BATMAN
FOREVER
.
-RRB-

Her
amus
Poison
Ivy
is
the
most
entertain
charact
in
the
film
,
and
when
she
's
offscreen
the
film
greatli
suffer
.

I
figur
that
if
on
ha
to
die
,
be
kiss
to
death
by
Uma
Thurman
is
n't
a
half-bad
wai
to
go
.

While
BATMAN
AND
ROBIN
wa
hardli
a
ride
of
pulse-pound
excit
,
I
must
admit
that
I
wa
not
bore
watch
it
,
although
I
did
glanc
at
my
watch
repeatedli
through
the
screening-mi
attent
wa
kept
through
anticip
of
the
utter
of
yet
anoth
terribl
pun
or
one-lin
,
and
by
await
yet
anoth
scene
to
fall
flat
.

It
's
been
a
long
time
sinc
I
've
laugh
so
much
at
a
movi
.

``
At
''
,
of
cours
,
is
the
oper
word
.

