A
few
month
befor
the
releas
of
Star
War
Episod
1
,
The
Phantom
Menac
,
20th
Centuri
Fox
decid
to
releas
anoth
space
film
,
that
is
a
complet
rip
off
of
Star
War
.

What
is
the
point
of
thi
?

I
do
not
know
,
but
I
wish
it
had
n't
been
done
,
consid
Wing
Command
is
definit
the
year
's
worst
film
so
far
.

To
attract
peopl
to
thi
horribl
movi
,
thei
attach
the
full
trailer
for
The
Phantom
Menac
.

Wing
Command
will
draw
larg
crowd
,
becaus
thi
is
the
onli
film
where
you
can
find
The
Phantom
Menac
full
trailer
attach
at
thi
time
.

The
trailer
for
The
Phantom
Menac
wa
certainli
the
best
part
of
the
movi
experi
I
had
tonight
.

Mani
peopl
do
not
know
that
Wing
Command
is
base
on
a
Star
War
comput
game
.

I
found
thi
veri
interest
,
consid
thi
fact
almost
sai
that
Wing
Command
is
a
Star
War
movi
.

It
is
nowher
near
the
level
of
the
Star
War
film
though
.

Freddi
Prinz
Jr.
star
in
the
film
as
Christoph
Blair
.

Wing
Command
wa
a
huge
mistak
for
Freddi
.

Thi
wa
definit
hi
worst
perform
to
date
.

After
Januari
's
She
's
All
That
,
I
actual
saw
a
good
actor
in
Freddi
,
but
now
all
those
vision
have
been
crush
.

-LRB-
For
now
anywai
.
-RRB-

Christoph
Blair
is
a
pilot
who
is
a
part
of
the
battl
to
stop
the
Kilrathi
,
a
group
that
is
try
to
destroi
the
earth
.

Blair
's
parent
also
fought
in
the
space
battl
,
and
also
di
there
.

Hi
parent
were
pilgrim
,
so
mani
peopl
disrespect
him
becaus
of
thi
.

Blair
is
under
the
command
of
``
Angel
''
,
plai
by
Saffron
Burrow
,
and
he
is
friend
with
anoth
pilot
,
Todd
``
Maniac
Marshal
''
,
plai
by
Matthew
Lillard
.

Matthew
Lillard
seem
to
plai
the
same
role
in
everi
movi
,
and
he
doe
in
thi
on
too
.

Hi
charact
as
Stu
in
1996
's
Scream
-LRB-
A
+
-RRB-
,
wa
a
great
on
,
but
now
,
it
seem
like
that
same
role
is
be
plai
again
,
onli
in
a
space
movi
.

The
three
ar
just
a
small
part
of
a
larg
group
try
to
stop
the
Kilrathi
befor
thei
take
over
earth
.

Thi
plot
is
veri
flimsi
,
and
doe
n't
give
a
whole
lot
to
work
with
at
all
.

Thi
explain
why
Wing
Command
drag
on
,
seem
like
there
is
no
point
to
ani
of
the
event
that
ar
go
on
.

The
act
is
horribl
in
Wing
Command
.

Mani
line
seem
out
of
place
,
and
ar
complet
meaningless
.

The
act
and
dialogu
wa
so
terribl
,
I
even
found
myself
laugh
at
it
.

The
other
thing
which
ar
veri
bad
about
Wing
Command
ar
the
special
effect
and
the
music
.

The
special
effect
ar
veri
unrealist
.

When
look
at
two
ship
fly
side
by
side
,
it
is
simpl
to
tell
thei
ar
hang
and
a
blue
screen
is
behind
them
.

The
explos
look
veri
unrealist
as
well
.

A
ship
will
be
blown
up
,
but
there
will
onli
be
fire
present
four
about
on
half
of
a
second
.

Thi
is
veri
unrealist
.

Music
pretti
much
accompani
the
film
the
entir
time
.

The
music
also
felt
like
video
game
music
,
and
it
wa
veri
corni
and
annoi
sound
.

After
we
ar
given
the
plot
,
the
film
repeat
itself
for
anoth
hour
,
without
ani
plot
twist
,
interest
scene
,
or
anyth
import
to
the
film
.

Blair
is
forc
to
make
jump
over
other
planet
,
and
he
must
also
fight
against
the
Kilrathi
aircraft
.

For
the
entir
length
of
the
movi
,
it
take
place
in
space
.

It
is
truli
like
you
ar
in
a
video
game
,
becaus
you
ar
just
watch
peopl
shoot
at
each
other
in
ship
for
a
long
,
extend
period
of
time
.

It
's
not
much
fun
to
watch
at
all
.

If
I
want
to
watch
peopl
shoot
at
each
other
in
ship
,
I
would
go
watch
Star
War
at
home
.

At
least
Star
War
pai
attent
to
peopl
,
myth
,
feel
,
and
ha
some
real
plot
to
it
.

The
Bottom
Line-Br
on
The
Phantom
Menac
20th
Centuri
Fox
!

