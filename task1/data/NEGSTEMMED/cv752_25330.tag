Bad
movi
describ
as
``
a
swift
descent
into
sin
pleasur
,
decai
,
and
debaucheri
''
ar
hard
to
watch
.

Bad
2000
's
movi
that
resembl
bad
1980
film
ar
even
harder
to
watch
.

Shadow
Hour
fall
into
the
latter
categori
,
a
mish-mash
train
wreck
of
B-movi
actor
-LRB-
includ
Michael
Dorn
,
aka
Star
Trek
's
Worf
-RRB-
,
an
uninterest
plot
,
vain
attempt
at
capit
on
the
``
underground
''
scene
of
seedi
Lo
Angele
,
and
realli
,
realli
bad
direct
and
horrend
music
video-esqu
ballist
edit
that
wa
taught
to
me
in
film
school
right
befor
I
decid
to
drop
out
.

The
film
revolv
around
the
life
of
Michael
Hollowai
-LRB-
Balthazar
Getti
-RRB-
who
is
try
to
restart
hi
life
with
hi
one-dimension
wife
Chloe
-LRB-
Rebecca
Gayheart
-RRB-
after
a
nasti
bout
of
drug
and
alcohol
addict
.

Michael
take
a
job
of
work
the
graveyard
shift
at
the
local
ga
station
and
is
bombard
by
the
ugli
and
weird
of
the
nightlif
of
L.A.
.

One
night
,
he
meet
a
strang
gent
name
Stuart
-LRB-
Mr.
Buckaroo
Banzai
,
Peter
Weller
-RRB-
.

He
drive
a
Porsch
,
smoke
French
cigarett
,
and
drone
on
about
life
,
eventu
coax
Mike
into
explor
the
``
underbelli
''
of
L.A.
togeth
,
a
tour
of
punk
bar
,
S&M
club
,
and
bare-knuckl
fight
.

The
film
then
throw
in
a
murder
mysteri
with
a
cop
plai
Peter
Green
,
act
like
he
want
to
get
the
chanc
at
a
repris
of
hi
role
in
The
Mask
2
.

The
film
then
simpli
dissolv
into
a
cheap
rip-off
of
Dant
's
Inferno
mix
with
an
old
Stephen
J.
Cannel
televis
pilot
.

The
pace
of
the
film
is
jar
and
utterli
without
focu
.

Other
horror
includ
the
endless
montag
of
peopl
pump
ga
and
the
Charli
Sheen
,
Johnni
Depp
,
Richard
Grieco-esqu
act
of
Balthazar
Getti
.

Peter
Weller
clearli
know
hi
career
is
complet
gone
and
doe
n't
give
two
shit
about
it
.

And
after
the
cheap
exploit
of
bondag
club
,
danc
club
,
and
brothels-damn
,
is
n't
anyon
safe
ani
more
from
the
ugli
ey
of
Hollywood
!?

Wai
back
in
1984
,
a
great
but
crazi
director
name
Abel
Ferrara
made
hi
wors
film
and
call
it
Fear
Citi
.

Shadow
Hour
remind
me
of
an
almost
perfect
sequel
.

