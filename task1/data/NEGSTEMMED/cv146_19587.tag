THE
HAUNTING
Some
hous
ar
born
bad
,
``
goe
The
Haunt
's
tag
line
,
to
which
I
must
add
,
''
Some
movi
,
too
.
''
''

Noth
short
of
hire
a
new
cast
,
a
more
liter
screenwrit
,
and
a
new
director
could
have
save
thi
tragic
misguid
adapt
of
Jackson
's
meritori
novel
.

The
Haunt
is
late
entri
summer
dreck
too
slick
to
be
creepi
,
and
it
seemingli
endless
stream
of
digit
trickeri
and
spooki
ooki
sound
effect
do
n't
frighten
so
much
as
numb
the
audienc
into
submission-th
film
is
like
a
Rube
Goldberg
contrapt
rig
to
shout
``
boo
.
''

Fragil
Nell
-LRB-
Taylor
-RRB-
,
bisexu
Theo
-LRB-
Zeta-Jon
-RRB-
,
and
smilei
Luke
-LRB-
Wilson
-RRB-
ar
three
insomniac
who
gather
at
the
reputedli
possess
Hill
Hous
for
an
extend
studi
on
sleep
disord
,
host
by
Professor
Marrow
-LRB-
Neeson
-RRB-
.

Marrow
's
secretli
gather
data
on
their
respect
paranoid
respons
to
hi
recount
of
Hill
Hous
's
bleak
histori
.

He
's
not
prepar
for
the
veri
real
apparit
that
terror
the
crew
,
Nell
especi
,
who
ha
some
ancestr
connect
to
the
manor
's
previou
inhabit
.

Taylor
is
thoroughli
insuffer
in
her
first
big-budget
lead
.

For
starter
,
her
consist
dour
express
suck
the
life
out
of
even
the
earli
scene
,
when
we
're
introduc
to
the
mansion
and
all
it
fun-hous
trap
.

Her
charact
is
suppos
to
be
depress
,
have
tend
to
an
unlov
mother
for
too
mani
year
,
but
Taylor
plai
Nell
as
supernatur
lame
,
altern
grouchi
,
mopei
,
wiggi
and
pathet
,
and
I
kept
wonder
why
the
other
charact
did
n't
just
ditch
thi
bitch
.

As
for
the
obscen
photogen
Zeta-Jon
,
she
breez
through
her
scene
with
a
wink
and
a
smile
and
take
the
sceneri
with
her
.

Unfortun
,
she
's
saddl
with
some
of
the
most
unlik
dialogu
the
screenplai
ha
to
offer
.

Theo
's
assess
of
the
hous
?

``
I
love
it
!
''

Sort
of
Charl
Foster
Kane
meet
The
Munster
.
''
''

Who
on
Earth
would
sai
that
in
place
of
``
Citizen
Kane
meet
The
Munster
''
?

-LRB-
Furthermor
,
would
you
gladli
spend
a
singl
night
in
a
hous
befit
that
descript
?
-RRB-

Both
actress
fare
better
than
Neeson
,
who
look
embarrass
to
be
a
part
of
thi
ensembl
-LRB-
and
for
good
reason
-RRB-
,
and
hi
charact
is
the
most
bland
.

As
Luke
put
it
,
Dr.
Marrow
pull
the
old
``
academ
bait
and
switch
''
on
hi
subject
,
but
he
break
down
and
confess
to
thi
the
second
he
's
accus
.

Later
,
he
risk
hi
life
by
climb
a
crumbl
stairwel
to
save
Nell
-LRB-
it
's
amus
to
hear
Neeson
shout
``
Nell
''
repeatedli
,
given
hi
star
role
in
the
1994
Jodi
Foster
vehicl
of
the
same
name
-RRB-
.

Why
wa
thi
nice
,
help
,
and
redempt
research
so
absent
of
ethic
at
the
start
?

The
Haunt
wa
well
design
by
Eugenio
Zanetti
.

Hi
set
ar
obsess
detail
,
and
even
befor
the
CGI
kick
in
,
thei
seem
aliv
,
never
quit
still
.

I
do
have
on
beef
with
thi
aspect
of
the
product
:
the
real
life
mansion
us
in
exterior
shot
,
Nottinghamshir
's
Harlaxton
Manor
,
is
so
vast
that
on
ha
troubl
believ
that
Nell
and
compani
,
no
matter
how
much
run
awai
thei
do
from
ghost
and
goblin
,
alwai
finish
up
in
locat
that
were
establish
in
act
on
,
as
if
all
the
action
ha
been
confin
to
on
wing
of
Hill
Hous
.

Eighti
million
dollar
wa
spent
on
The
Haunt
,
and
despit
a
powerhous
box
offic
debut
,
I
doubt
it
will
recoup
it
cost
-LRB-
includ
market
-RRB-
domest
.

Thank
Jan
De
Bont
for
that
:
prove
for
the
third
time
that
Speed
wa
a
fluke
,
in
that
it
wa
actual
enjoy
,
De
Bont
ha
serv
up
anoth
ride
,
san
thrill
.

Hi
imag
,
well-lit
though
thei
ar
,
have
an
unmistak
been
there-don
that
qualiti
;
in
fact
,
whole
sequenc
,
not
to
mention
the
cloak
,
airi
ghoul
who
own
the
climax
,
feel
lift
from
a
much
smarter
and
infinit
more
enjoy
spectacl
from
three
summer
back
,
Peter
Jackson
's
The
Frighten
.

I
do
n't
mean
to
suggest
De
Bont
is
a
plagiarist
,
I
mean
to
suggest
that
he
's
a
hack
,
have
found
no
new
wai
to
give
us
chill
.

