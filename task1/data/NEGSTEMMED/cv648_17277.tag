``
There
's
noth
new
under
the
sun
''
is
a
phrase
often
us
when
the
speaker
actual
mean
``
Let
's
find
someth
to
copi
.
''

Of
cours
there
ar
veri
few
complet
origin
idea
.

Even
earth-shatt
concept
ar
built
upon
the
vast
bodi
of
human
experi
.

There
is
,
after
all
,
no
need
to
re-inv
the
wheel
time
after
time
.

Recent
it
seem
that
Hollywood
doe
n't
feel
the
need
to
even
re-writ
the
script
.

My
understand
of
the
word
``
sequel
''
is
a
continu
of
the
stori
.

The
film
industri
ha
defin
the
word
to
mean
reshoot
the
origin
with
minor
chang
.

Have
an
overwhelm
desir
to
see
an
inferior
version
of
Brian
DePalma
's
adapt
of
the
Steven
King
novel
?

Thi
is
your
dream
come
true
.

Rachel
-LRB-
Emili
Bergl
-RRB-
,
a
high
school
outcast
,
is
begin
to
notic
weird
thing
happen
around
her
.

Door
slam
shut
by
themselv
.

Glass
globe
blow
up
.

Her
mother
ha
sever
mental
problem
and
her
father
is
absent
.

A
popular
boi
unexpectedli
ask
her
out
.

The
in-crowd
conspir
to
embarrass
her
at
a
public
event
.

Any
of
thi
sound
familiar
?

Once
the
audienc
catch
on
that
thi
is
the
same
stori
as
``
Carri
''
,
there
's
littl
to
do
but
wait
for
the
inevit
end
.

The
effect
ar
a
bit
better
thi
time
around
,
but
the
film
doe
n't
work
nearli
as
well
.

There
ar
a
coupl
of
minor
plot
differ
.

Rachel
live
with
foster
parent
becaus
her
mother
is
institution
.

The
boi
at
her
school
ar
portrai
as
even
more
evil
than
in
the
origin
.

Thei
keep
score
of
their
score
with
point
given
for
each
conquest
.

And
,
uh
,
there
must
be
other
stori
chang
but
none
stand
out
.

One
nice
touch
is
the
cast
of
Amy
Irving
again
as
Sue
Snell
.

Over
20
year
ago
she
wa
the
on
girl
who
tri
to
help
Carri
.

Now
a
high
school
counselor
,
she
befriend
Rachel
,
but
her
charact
's
potenti
is
squander
.

There
's
too
much
about
Sue
that
doe
n't
make
sens
.

After
the
slaughter
when
Carri
kill
most
of
her
classmat
,
Sue
is
driven
mad
and
spend
time
in
the
institut
that
Rachel
's
mother
is
in
.

Still
live
in
the
same
small
town
,
the
woman
with
a
well-known
histori
of
mental
problem
is
hire
as
a
high
school
counselor
?

Sue
tell
Rachel
that
her
telekinesi
is
a
genet
diseas
.

Thi
mai
be
the
on
origin
idea
in
the
film
,
but
the
reason
behind
describ
psychic
power
as
a
``
diseas
''
is
never
explain
.

Sue
's
eventu
fate
is
an
admiss
by
director
Katt
Shea
and
writer
Rafael
Moreu
that
thei
had
an
interest
charact
but
could
n't
figur
out
what
to
do
with
her
.

Blink
and
you
'll
miss
it
.

Some
of
the
event
ar
film
in
black
and
white
,
but
the
rational
for
thi
is
unknown
.

It
doe
n't
add
anyth
and
the
choic
of
scene
appear
somewhat
random
.

``
The
Rage
''
retain
some
of
the
trap
of
``
Carri
''
without
the
mean
.

In
the
first
film
the
color
red
wa
a
motif
connect
to
Carri
's
onset
of
power
at
the
same
time
she
began
menstruat
.

Here
there
's
an
abund
of
red
,
but
for
no
appar
purpos
.

The
cast
of
high
school
jock
and
cheerlead
as
villain
is
begin
to
wear
thin
.

One
might
be
lead
to
suspect
that
most
filmmak
were
unpopular
in
school
and
the
histori
of
teenag
film
is
an
extend
cinemat
reveng
of
the
nerd
.

The
biggest
mistak
the
film
make
is
includ
clip
of
the
origin
.

See
Sissi
Spacek
on
the
screen
onli
point
to
the
qualiti
of
that
film-and
the
flaw
in
thi
on
.

