The
Corruptor
is
a
big
silli
mess
of
an
action
movi
,
complet
with
pointless
plot
turn
and
gratuit
violenc
.

It
's
not
abhorr
,
or
even
blatantli
unlik
,
but
it
doe
n't
make
a
shred
of
sens
.

And
whose
idea
wa
it
to
have
the
director
of
Glengarri
Glen
Ross
direct
an
action
film
?

Jame
Folei
know
a
lot
about
charact
and
act
,
and
those
ar
the
strength
of
The
Corruptor
.

But
the
quiet
scene
clash
with
the
ludicr
action
nonsens
,
and
the
result
is
less
like
a
movi
and
more
like
a
derail
train
.

At
least
we
have
Chow
Yun-Fat
in
the
lead
role
.

Chow
plai
Nick
Chen
,
a
Chinatown
cop
.

He
's
a
good
cop
,
as
the
first
few
scene
establish
,
and
he
's
veri
familiar
with
Chinatown
.

That
's
probabl
why
the
powers-that-b
decid
to
team
him
up
with
a
rooki
name
Danni
Wallac
-LRB-
Mark
Wahlberg
-RRB-
.

Nick
and
Danni
begin
by
step
on
each
other
's
toe
,
but
final
end
up
like
each
other
onc
thei
both
get
a
chanc
to
save
the
other
on
's
life
.

What
happen
after
that
is
kind
of
a
mysteri
to
me
.

I
'm
fairli
certain
that
the
villain
ar
all
part
of
the
Fukienes
Dragon
,
led
by
a
young
Chines
psycho
name
Bobbi
Vu
-LRB-
Byron
Mann
-RRB-
.

I
'm
also
pretti
sure
that
Nick
is
on
the
payrol
of
Henri
Lee
-LRB-
Ric
Young
-RRB-
,
a
gangster
deal
in
prostitut
and
other
neat
stuff
.

I
'm
not
quit
sure
why
Henri
Lee
decid
to
emploi
Danni
,
although
thi
make
for
some
ridicul
-LRB-
albeit
unpredict
-RRB-
plot
twist
along
the
wai
.

There
's
also
an
interest
subplot
revolv
around
Danni
's
father
-LRB-
Brian
Cox
-RRB-
,
but
it
doe
n't
have
much
to
do
with
the
main
stori
.

One
of
the
problem
,
as
I
said
,
is
that
the
movi
doe
n't
make
ani
sens
.

I
do
n't
blame
thi
entir
on
Folei
,
becaus
he
's
obvious
a
good
director
.

I
'm
more
inclin
to
point
my
finger
at
Robert
Pucci
's
script
,
which
doe
n't
seem
to
be
in
tune
to
the
wai
normal
peopl
act
.

One
scene
earli
on
had
me
particularli
annoi
:
Nick
confront
hi
boss
becaus
he
's
angri
about
hi
new
white
partner
.

He
's
shout
and
point
hi
finger
and
step
on
the
furnitur
,
and
I
wa
think
that
I
would
have
fire
him
if
he
had
done
that
to
me
.

-LRB-
In
addit
,
the
film
take
a
bite
into
race-rel
issu
,
and
never
develop
them
at
all
.
-RRB-

There
ar
a
lot
of
scene
like
thi
on
,
and
none
of
them
ar
veri
coher
.

There
's
also
a
consider
languag
barrier
,
given
that
Chow
and
some
of
the
other
actor
have
thick
Chines
accent
.

The
plot
itself
never
straighten
out
.

I
'm
not
sure
who
or
what
the
Fukienes
Dragon
ar
,
or
why
Henri
Lee
is
associ
with
them
,
or
what
thei
do
as
a
group
.

The
Corruptor
is
an
action
movi
,
and
so
all
these
weird
plot
develop
ar
decor
with
loud
and
violent
action
sequenc
.

The
sequenc
ar
n't
bad
,
but
thei
're
not
new
;
how
mani
car
chase
through
Chinatown
have
you
seen
?

On
that
note
,
how
mani
Chinatown
cop
movi
have
you
seen
?

One
too
mani
,
I
'd
imagin
.

Folei
's
strength
is
clearli
in
character
,
and
he
doe
a
pretti
good
job
here
.

The
scene
between
Nick
and
Danni
ar
veri
good
,
and
I
actual
got
a
feel
for
their
charact
;
a
bond
form
between
them
that
hold
part
of
the
film
togeth
.

Chow
and
Wahlberg
ar
both
good
actor
;
Chow
is
a
pro
,
and
can
do
thi
kind
of
stuff
in
hi
sleep
.

Wahlberg
seem
less
at
home
in
thi
atmospher
,
but
he
's
still
fun
to
watch
.

I
also
like
the
subplot
involv
Danni
's
father
;
Brian
Cox
's
perform
is
power
,
and
hi
charact
make
a
compel
moral
compass
for
Danni
.

But
the
film
ultim
fail
,
mostli
at
the
hand
of
insan
incoher
and
overly-familiar
action
scene
.

A
complic
plot
can
be
success
,
but
the
stori
need
to
make
sens
when
it
's
over
.

The
Corruptor
never
manag
to
make
ani
sens
--
it
just
keep
spin
out
of
control
until
,
final
,
there
's
noth
left
to
hold
on
to
.

