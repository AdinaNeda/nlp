Horror
auteur
John
Carpent
-LRB-
``
Halloween
,
''
``
Vampir
''
-RRB-
strike
out
with
thi
sci-fi
eco-f
that
's
so
bad
it
boggl
the
mind
to
imagin
how
the
project
ever
got
green-lit
.

The
script
by
Carpent
and
Larri
Sulki
appear
to
have
been
lift
directli
from
last
year
's
``
Pitch
Black
,
''
involv
a
violent
prison
who
must
be
releas
from
bondag
so
that
he
can
help
a
small
band
of
human
protect
themselv
from
blood-thirsti
,
maraud
alien
.

In
the
year
2176
,
there
ar
640,000
Earthl
on
Mar
,
live
in
a
matriarch
societi
led
by
a
Command
,
plai
by
Pam
Grier
.

Grier
,
pill-poppin
'
Natasha
Henstridg
,
and
some
rooki
Mar
Polic
offic
-LRB-
Clea
Duval
,
Jason
Statham
-RRB-
travel
to
the
remot
mine
town
of
Shine
Canyon
to
fetch
``
Desol
''
Williams-that
's
Ice
Cube-to
bring
him
back
to
Chryse
Citi
to
stand
trial
for
murder
.

But
when
thei
're
besieg
by
dement
,
zombie-lik
,
body-snatch
miner
,
thei
readili
free
the
scowl
Ice
Cube
sinc
thei
need
him
for
protect
.

It
seem
a
red
cloud
wa
releas
from
a
Shine
Canyon
cave
and
,
soon
after
,
most
of
the
miner
went
bonker
as
long-dorm
remnant
of
an
ancient
Martian
civil
took
over
their
mind
and
bodi
,
lop
off
head
as
``
vengeanc
for
anyth
that
tri
to
lai
claim
to
their
planet
,
''
accord
to
a
scientist
-LRB-
Joanna
Cassidi
-RRB-
.

Carpent
us
so
mani
flashback
to
tell
the
``
Night
of
the
Live
Dead
''
-
like
stori
that
the
idiot
plot
get
incomprehens
confus
.

But
you
can
easili
predict
each
of
the
support
charact
who
will
be
kill
,
along
with
the
order
of
their
elimin
.

``
Ghost
of
Mar
''
thud
to
a
labori
,
bottom-of-the-barrel
.

Perhap
,
inde
,
there
is
a
curs
on
Mar
film
,
if
you
recal
two
other
dud
:
``
Mission
to
Mar
''
and
``
Red
Planet
.
''

