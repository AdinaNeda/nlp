The
obviou
reason
for
produc
a
sequel
to
an
immens
popular
movi
is
to
acquir
continu
profit
.

The
rational
is
sound
,
but
in
mani
case
of
thi
and
recent
year
,
the
sequel
is
a
shoddi
product
that
's
expos
for
the
cash-milk
vehicl
it
realli
is
.

Last
year
's
SPEED
2
and
SCREAM
2
,
as
well
as
thi
year
's
SPECIES
II
have
all
been
product
that
have
been
decis
less
than
satisfactori
.

In
some
case
,
a
sequel
can
even
discredit
it
predecessor
,
as
with
the
latest
neo-slash
flick
,
I
STILL
KNOW
WHAT
YOU
DID
LAST
SUMMER
.

Whatev
uniqu
the
origin
might
've
had
now
seem
trite
and
overplai
when
pair
with
thi
abomin
thriller
.

Juli
Jame
-LRB-
Jennif
Love
Hewitt
-RRB-
and
Rai
Bronson
-LRB-
Freddi
Prinz
Jr.
-RRB-
ar
back
from
the
origin
to
star
in
I
STILL
KNOW
,
repris
their
role
in
typic
fashion
.

Juli
and
Rai
experi
a
rather
predict
falling-out
at
the
begin
of
the
movi
,
leav
the
door
wide
open
for
newcom
Will
Benson
-LRB-
Matthew
Settl
-RRB-
.

Will
and
Juli
hit
it
off
,
and
along
with
friend
Karla
-LRB-
Brandi
Norwood
-RRB-
and
Tyrel
-LRB-
Mekhi
Phifer
-RRB-
,
the
foursom
head
off
to
a
radio
station-giveawai
vacat
in
the
tropic
.

Unfortun
,
thing
ar
n't
so
peachi
onc
thei
arriv
,
and
with
a
regular
you
could
set
your
watch
by
,
the
infam
fisherman
-LRB-
Muse
Watson
-RRB-
is
back
with
hi
hook
.

It
's
anoth
bloodi
showdown
,
complet
with
cliffhang
end
.

There
's
not
much
that
's
origin
about
I
STILL
KNOW
,
and
with
the
market
satur
by
Gen-X
thriller
like
thi
on
,
it
's
unlik
that
I
STILL
KNOW
will
get
ani
recognit
other
than
that
of
a
bad
sequel
.

For
die-hard
fan
of
the
genr
,
thi
will
be
requir
view
materi
,
but
the
bottom
line
is
that
the
materi
here
is
just
recycl
from
the
origin
I
KNOW
WHAT
YOU
DID
LAST
SUMMER
.

The
aura
of
thriller
surround
the
plot
is
n't
heighten
by
skill
script
or
camera
work
,
but
rather
a
tens
,
string-bas
score
and
manipul
edit
.

The
man
with
the
hook
end
up
becom
veri
belittl
thi
time
around
when
the
script
give
him
too
mani
line
of
dialogu
--
he
goe
from
be
a
scari
figur
to
a
nutcas
in
a
few
short
and
pain
moment
.

Much
of
the
mysteri
is
drop
for
the
sake
of
get
the
point
.

The
whole
reason
the
plot
exists-Juli
and
Karla
must
guess
the
capit
of
Brazil
in
order
to
win
the
trip
from
the
radio
station-i
a
dead
giveawai
,
lessen
suspens
and
creat
a
hurry-up-and-wait
time
problem
.

And
,
wherea
the
killer
's
ident
might
've
been
a
question
in
the
first
movi
,
it
's
almost
a
given
here
,
which
chang
the
dynam
of
the
movi
drastic
.

Three
or
four
teen
run
automat
from
a
man
in
a
rain
slicker
thei
nonchalantli
refer
to
as
``
the
killer
.
''

Thei
're
almost
us
to
it
,
and
the
perform
show
it-not
Hewitt
,
nor
Prinz
,
nor
Brandi
,
nor
Phifer
give
a
decent
show
.

And
so
,
in
the
end
,
it
's
left
wide
open
for
a
third
movi
and-most
likely-a
brand
new
support
cast
.

God
help
us
.

