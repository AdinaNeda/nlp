A
film
which
introduc
charact
,
situat
,
dilemma
and
develop
that
we
've
seen
befor
in
a
parad
of
other
film
.

A
film
which
can
easili
be
guess
out
by
the
end
of
frame
number
on
.

A
film
which
is
pack
to
the
cap
with
predict
,
lead
to
veri
littl
tension
,
excit
,
suspens
or
interest
on
the
part
of
it
pai
audienc
.

In
short
,
a
clich-ridden
formula
film
.

Welcom
to
my
review
of
THE
GENERAL
'S
DAUGHTER
.

An
undercov
Army
detect
and
a
rape
counselor
find
themselv
lock
insid
an
investig
into
some
bigwig
Gener
daughter
's
rape
,
tortur
and
murder
.

Thei
must
delv
through
all
of
the
unspoken
Army
rule
and
the
hush-hush
,
to
figur
out
the
conspiraci
behind
the
shock
murder
.

By
the
number
-LRB-
see
abov
-RRB-
.

Thi
film
is
just
there
.

It
sit
there
on
the
big
screen
for
a
coupl
of
hour
,
float
around
,
goe
awai
,
hopefulli
never
to
be
heard
from
again
.

It
is
so
predict
that
even
a
blind
man
could
see
it
plot
point
come
a
mile
awai
.

It
's
as
suspens
as
a
leaf
drop
from
a
tree
.

It
's
as
action-pack
as
a
Canadian
curl
tournament
.

Get
the
pictur
?

I
sure
did
...
it
's
too
bad
that
it
took
my
friend
and
I
less
than
two
minut
to
figur
out
the
entir
plot
,
and
to
break
down
each
scene
befor
it
wa
even
complet
.

Easi
as
pie
.

It
's
unfortun
becaus
Jame
Wood
and
John
Travolta
actual
have
on
extrem
enjoy
scene
togeth
near
the
begin
of
the
film
,
but
ala
,
t
`
wa
not
to
be
-LRB-
That
scene
alon
score
two
of
my
four
point
allot
.
-RRB-

Wood
chew
it
up
in
the
few
scene
that
he
's
in
,
Travolta
pass
the
test
,
Cromwel
plai
,
well
,
Cromwel
,
and
Stow
is
window
dress
with
a
smile
-LRB-
MIA
sinc
12
MONKEYS
-LRB-
8/10
-RRB-
it
seem
-RRB-
.

And
thi
predict
is
n't
reserv
onli
to
those
who
have
seen
film
like
COURAGE
UNDER
FIRE
or
A
FEW
GOOD
MEN
,
it
run
deep
insid
everi
on
of
us
who
know
to
suspect
someon
as
soon
as
thei
see
their
obviou
guilti
mug
on
the
big
screen
.

It
's
like
ride
a
bike
.

Other
scenario
which
spong
out
ani
tension
,
suspens
or
interest
from
thi
film
includ
everi
singl
charact
eventu
``
break
down
''
to
the
investig
without
much
reason
given
,
a
ridicul
place
background
relationship
between
two
of
the
lead
charact
,
as
much
action
as
my
grandpar
bedroom
nightli
,
and
a
directori
style
that
can
onli
reward
director
Simon
West
with
a
solid
nomin
for
the
``
Best
Poor
Man
's
Michael
Bai
Do
hi
Best
Poor
Man
's
Impression
of
Toni
Scott
''
-LRB-
Add
two
scene
with
sunlight
shine
through
some
half-open
shade
for
grit
and
integr
,
and
an
all-out
rainfal
for
the
final
for
further
chao
,
and
you
're
a
great
director
.

Yawn
.

Yeah
,
whatev
Toni
...
I
mean
,
Simon
.
-RRB-

And
ar
n't
we
all
sick
of
hear
about
these
Army
``
bad
boi
''
and
their
overdon
``
code
of
silenc
''
?!

Enough
alreadi
!

Next
subject
,
pleas
.

See
it
on
video
if
you
wan
na
fall
asleep
after
see
a
much
better
movi
like
AN
OFFICER
AND
A
GENTLEMAN
-LRB-
8.5
/
10
-RRB-
.

Otherwis
,
save
yourself
the
troubl
and
go
take
a
crap
instead
.

You
'll
feel
much
better
afterward
.

Trust
me
.

Littl
Known
Fact
about
thi
film
and
it
star
:
Ironic
,
John
Travolta
turn
down
the
lead
role
in
AN
OFFICER
AND
A
GENTLEMAN
,
which
eventu
went
to
littl
Dicki
Gere
.

Ironic
on
Jame
Wood
'
part
,
he
complet
on
of
hi
earliest
act
role
on
TV
's
``
Welcom
Back
,
Kotter
''
,
star
none
other
than
John
Travolta
.

Actor
Jame
Wood
recent
confirm
report
of
hi
``
big
dick
''
on
Howard
Stern
's
radio
program
.

Unlike
rocker
Tommi
Lee
,
Wood
is
also
alleg
to
have
an
IQ
of
180
.

He
appar
score
a
perfect
800
on
hi
verbal
SAT
and
a
779
on
the
math
section
.

What
a
man
!

John
Travolta
is
marri
to
actress
Kelli
Preston
,
and
thei
have
a
son
name
Jett
-LRB-
Travolta
love
them
plane
!
-RRB-
.

Word
on
the
street
is
that
the
kid
wa
appar
conceiv
dure
a
weekend
at
Demi
Moor
and
Bruce
Willi
'
home
.

Director
Simon
West
's
first
film
wa
the
Jerri
Bruckheim
produc
CON
AIR
-LRB-
6.5
/
10
-RRB-
.

Befor
that
,
he
direct
TV
commerci
includ
the
Budweis
ad
with
the
danc
ant
.

Yippe
!

Veteran
director
John
Frankenheim
-LRB-
RONIN
-LRB-
7.5
/
10
-RRB-
,
THE
MANCHURIAN
CANDIDATE
-RRB-
portrai
the
charact
of
Gener
Sonnenberg
in
thi
film
.

The
IMDb
report
that
when
Senat
Robert
Kennedi
wa
shot
at
the
Ambassador
Hotel
in
Lo
Angele
on
June
5
,
1968
,
it
wa
hi
good
friend
John
Frankenheim
who
had
person
driven
him
there
that
dai
.

Clarenc
William
III
,
who
plai
Colonel
Fowler
in
thi
film
,
is
known
to
some
from
hi
role
as
``
Linc
''
in
the
origin
``
Mod
Squad
''
TV
seri
.

Younger
folk
mai
rememb
him
as
Princ
's
father
in
PURPLE
RAIN
.

