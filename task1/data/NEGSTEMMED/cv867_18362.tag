Whenev
U.S.
govern
start
meddl
into
other
countri
'
affair
,
under
the
pretext
of
support
human
right
or
prevent
polit
,
religi
or
ethnic
persecut
,
the
other
side
is
readi
to
us
mantra
that
sai
``
Look
who
's
talk
!
''

What
have
you
done
to
the
Indian
?
''
''

Howev
,
even
the
American
themselv
ar
readi
to
us
that
dark
chapter
of
their
own
nation
histori
when
it
suit
their
purpos
.

Hollywood
is
just
anoth
exampl
,
with
it
revisionist
western
,
made
in
earli
1990
.

Those
movi
tri
to
exploit
the
emerg
wave
of
Polit
Correct
,
coincid
with
the
500th
anniversari
of
Columbu
'
discoveri
of
America
.

One
of
such
exampl
is
GERONIMO
:
AN
AMERICAN
LEGEND
,
1993
western
direct
by
Walter
Hill
,
film
that
deal
with
on
of
the
last
conflict
between
American
nativ
and
white
settler
.

The
hero
of
the
film
is
Geronimo
-LRB-
plai
by
We
Studi
-RRB-
,
leader
of
Apach
,
fierc
warrior
tribe
that
us
to
give
hard
time
to
white
settler
dure
the
second
half
of
19th
centuri
.

When
the
movi
begin
,
in
1885
,
Geronimo
and
hi
Apach
made
peac
with
U.S.
govern
and
try
to
live
peacefulli
in
Arizona
reserv
.

Howev
,
broken
promis
,
injustic
and
violenc
against
hi
peopl
would
make
Geronimo
restless
.

With
not
more
30
or
so
of
hi
follow
,
he
escap
reserv
and
begin
guerrilla
campaign
.

Gener
Crook
-LRB-
Gene
Hackman
-RRB-
,
command
of
U.S.
Army
forc
,
respect
Geronimo
and
know
that
even
hi
5,000
forc
is
n't
enough
to
catch
Geronimo
in
the
great
space
of
the
American
Southwest
.

Instead
,
he
turn
to
peopl
who
ar
more
experienc
with
Apaches-Lt
.

Charl
Gatewood
-LRB-
Jason
Patric
-RRB-
and
Indian
hunter
Al
Sieber
-LRB-
Robert
Duval
-RRB-
.

Togeth
with
young
Lt.
Britton
Davi
-LRB-
Matt
Damon
-RRB-
thei
would
begin
mission
aim
at
captur
Geronimo
.

GERONIMO
:
AN
AMERICAN
LEGEND
,
like
mani
movi
made
under
the
shadow
of
Polit
Correct
,
try
to
tell
the
tale
about
oppress
minor
,
but
instead
the
real
subject
is
the
bad
conscienc
of
the
oppressor
.

So
,
the
stori
about
Geronimo
is
told
from
the
perspect
of
hi
enemi
.

Almost
all
of
them
happen
to
be
hi
greatest
admir
and
us
everi
opportun
to
express
how
sorri
thei
feel
for
have
to
fight
him
and
hi
peopl
.

Although
such
element
of
John
Milliu
'
screenplai
do
inde
have
some
basi
in
histori
,
thei
harm
the
stori
of
Geronimo
.

To
be
honest
,
Walter
Hill
doe
try
to
make
Geronimo
the
real
hero
of
the
film
,
but
the
movi
segment
that
deal
with
the
plight
of
Apach
and
the
upris
ar
given
too
littl
time
.

Instead
,
thei
turn
out
to
be
noth
more
than
the
back
stori
for
rather
uninterest
adventur
stori
of
Gatewood
and
hi
band
.

To
make
even
wors
,
Hill
ha
some
real
problem
with
pace
and
style
,
and
in
the
end
we
have
impress
that
we
ar
watch
two
film
badli
edit
into
one-stori
about
Geronimo
and
stori
about
hi
pursuer
.

The
movi
should
have
been
better
if
it
turn
to
Geronimo
's
life
befor
and
after
hi
last
upris
,
in
mani
wai
more
interest
than
the
stori
about
Gatewood
.

The
differ
between
those
segment
could
be
observ
through
the
differ
qualiti
of
act
.

We
Studi
,
Cheroke
actor
who
wa
so
impress
as
Magua
in
THE
LAST
OF
THE
MOHICANS
,
wa
perfect
choic
for
Geronimo
,
not
onli
becaus
he
resembl
Geronimo
,
but
becaus
he
induc
a
lot
of
passion
in
hi
role
.

Contrari
to
him
,
we
have
disinterest
actor
who
sleepwalk
through
the
role
of
hi
white
enemi
.

While
thi
could
be
expect
from
someon
like
Jason
Patric
,
it
is
shame
when
we
have
veteran
like
Gene
Hackman
or
Robert
Duval
.

Even
Hill
's
direct
is
bellow
expectations-battl
scene
ar
too
short
and
,
like
in
mani
of
hi
late
film
,
forc
viewer
to
ask
what
had
happen
to
the
great
action
director
of
1970
.

Even
hi
old
associ
,
music
compos
Ry
Cooder
,
disappoint
,
with
the
score
that
shift
between
Indian
motiv
and
classic
.

On
the
other
hand
,
photographi
by
Lloyd
Ahern
II
,
with
the
us
of
red
lens
,
give
somewhat
dreami
atmospher
,
ideal
for
thi
movi
that
wa
suppos
to
be
melanchol
epic
.

All
in
all
,
compar
with
some
of
the
Hollywood
's
exampl
of
Polit
Correct
,
thi
film
is
n't
so
bad
,
but
we
ar
left
with
the
unpleas
impress
that
it
could
have
been
better
.

