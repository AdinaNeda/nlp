At
the
end
of
the
dai
,
those
reflect
upon
the
debacl
that
is
THE
AVENGERS
would
do
well
to
take
note
that
warn
cloud
loom
on
the
horizon
for
the
project
well
befor
Warner
Bros.
made
the
contenti
decis
to
abandon
preview
press
screen
and
scrap
plan
for
a
gala
premier
.

Thi
highly-anticip
film
rendit
of
the
cult
televis
show
wa
origin
slot
for
an
earli
June
open
,
where
it
would
have
gone
head-to-head
against
the
rival
studio
'
heavi
hitter
;
it
eventu
demot
to
a
less
potent
mid-August
open
wa
an
obviou
earli
indic
of
the
studio
's
lack
of
confid
with
the
pictur
.

And
with
good
reason
.

Thi
is
a
joyless
exercis
of
a
film
,
held
togeth
by
a
bare
coher
plot
and
lack
ani
semblanc
of
excit
,
thrill
or
wit
.

Remark
in
it
banal
and
brutal
uninvolv
,
THE
AVENGERS
is
a
catastroph
mess
which
immedi
invit
comparison
to
last
year
's
case
studi
in
style
over
substanc
,
Joel
Schumach
's
much-loath
BATMAN
&
ROBIN
.

-LRB-
Indeed
,
both
film
even
featur
appal
,
ridicul
sequenc
which
find
central
charact
dress
up
in
fuzzi
overs
costum
.
-RRB-

Uma
Thurman
,
who
take
on
the
salaci
role
of
the
catsuit-clad
,
karate-chop
Emma
Peel
immort
by
Diana
Rigg
,
wa
the
onli
bright
spot
in
the
aforement
Schumach
disast
,
imbu
her
Poison
Ivy
with
a
dose
of
sassi
and
sly
wit
that
gave
audienc
someth
to
smile
at
amidst
the
cinemat
carnag
.

Unfortun
,
the
same
ca
n't
be
said
here
,
where
she
and
cohort
Ralph
Fienn
-LRB-
our
new
John
Steed
,
take
over
for
Patrick
Macne
-RRB-
demonstr
no
appreci
chemistri
whatsoev
,
fatal
crippl
the
pictur
as
thei
vollei
fizzl
reparte
back
and
forth
and
trade
double-entendr
with
all
the
enthusiasm
of
two
actor
painfulli
awar
that
thei
're
on
board
a
sink
ship
.

At
thi
rate
,
usually-splendid
actor
Mr.
Fienn
mai
never
make
the
transit
from
Arthous
Apollo
to
mainstream
lead
man
--
hi
tepid
turn
here
will
make
as
much
of
an
inroad
as
hi
commend
seedi
perform
in
the
regrettably-neglect
Kathyrn
Bigelow
film
STRANGE
DAYS
.

The
duo
*
look
*
the
part
--
and
admittedli
THE
AVENGERS
is
,
more
than
most
,
heavili
depend
upon
style
--
but
thei
're
no
fun
to
watch
,
and
I
found
myself
grow
increasingli
distant
and
annoi
by
the
lack
of
spark
between
the
two
cheekili
iron
charact
as
thei
navig
through
the
pictur
's
caper-esqu
plot
.

When
not
check
my
wristwatch
or
shift
restlessli
in
my
seat
,
I
began
to
allevi
the
boredom
by
consid
how
thi
all
might
have
plai
out
had
the
filmmak
chose
to
go
instead
with
that
erstwhil
EMMA
as
our
Mrs.
Peel
--
no
,
not
Kate
Beckinsal
-LRB-
although
the
notion
now
intrigu
me
-RRB-
,
but
Gwyneth
Paltrow
,
who
wa
origin
in
the
run
for
the
part
and
can
verit
handl
a
spot-on
English
accent
.

If
noth
els
,
it
'd
at
least
be
highli
entertain
for
the
incongru
sight
of
the
vagu
twiggish
young
actress
kick
butt
.

The
stori
,
such
as
it
is
,
involv
the
ever-bemus
tandem
of
Steed
and
Peel
combat
the
malevol
Sir
August
de
Wynter
-LRB-
Sean
Conneri
-RRB-
,
an
eccentr
aristocrat
threaten
the
safeti
of
the
nation
with
hi
climate-control
contrapt
.

-LRB-
Thei
also
sip
a
lot
of
tea
.
-RRB-

Overlook
some
goofi
clone
nonsens
and
quirki
hijinx
involv
our
protagonist
'
superior
,
it
sound
far
better
than
it
plai
,
and
is
render
almost
indecipher
by
blatant
post-product
tinker
;
it
's
clearli
evid
that
the
pictur
ha
been
cut
to
shred
.

THE
AVENGERS
wa
never
about
grip
drama
,
and
our
hero
accordingli
never
take
villain
Sir
August
veri
serious
,
but
given
the
lack
of
cohes
in
the
plot
and
the
lack
of
menac
convei
by
the
buffoonish
maniac
,
it
's
all
decidedli
uncompel
.

Mr.
Conneri
,
who
's
onscreen
bare
long
enough
to
regist
an
impress
,
approach
the
role
like
a
man
fulfil
a
contractu
oblig
,
simultan
chew
the
sceneri
while
unabl
to
hide
hi
disinterest
.

At
least
it
all
look
good
.

Thi
is
a
genuin
handsom
product
,
with
fine
costum
design
by
Anthoni
Powel
and
crispli
shot
by
Roger
Pratt
.

In
particular
,
the
gleam
product
design
by
Stuart
Craig
command
attent
,
adeptli
draw
element
both
old
and
new
in
order
to
depict
thi
Great
Britain
.

There
ar
a
hand
of
strike
visual
moment
in
the
film
,
includ
an
attack
by
a
swarm
of
giant
robot
bee
and
a
nice
shot
of
Steed
and
Peel
find
a
wai
to
walk
on
water
,
but
the
film
is
so
unremittingli
dull
that
even
these
instanc
fail
to
stir
interest
or
rais
puls
rate
.

By
the
time
the
film
's
climax
had
arriv
,
my
interest
wa
not
with
the
sight
of
Steed
and
Sir
August
slug
it
out
amidst
crash
wave
and
thunder
rain
,
but
with
the
quickest
escap
rout
from
the
theatr
.

Not
coincident
,
the
entic
bit
of
visual
bravura
were
the
shot
assembl
into
the
movi
's
remark
trailer
,
a
savvi
piec
of
work
which
iron
is
infinit
more
appeal
that
the
featur
film
itself
;
the
first
promo
which
made
the
round
in
earli
spring
is
probabl
my
favourit
studio
trailer
so
far
thi
year
.

It
's
everyth
that
THE
AVENGERS
is
not
--
sauci
,
clever
,
engag
,
and
entertain
.

A
crush
disappoint
,
the
film
is
on
of
the
worst
out
of
the
year
--
too
drearili
aw
to
be
savour
as
gleefulli
bad
,
too
polish
to
overlook
it
defici
.

There
mai
be
upcom
pictur
that
ar
even
more
lifeless
than
THE
AVENGERS
,
but
I
sure
hope
not
.

