It
's
tough
to
be
an
aspir
superhero
in
Champion
Citi
.

Just
ask
Mr.
Furiou
-LRB-
Ben
Stiller
-RRB-
,
the
Blue
Raja
-LRB-
Hank
Azaria
-RRB-
,
and
The
Shovel
-LRB-
William
H.
Maci
-RRB-
.

Thei
're
try
to
break
into
the
biz
,
but
Captain
Amaze
-LRB-
Greg
Kinnear
-RRB-
,
the
citi
's
hero
numero
uno
,
hog
all
the
action
.

The
good
captain
ha
hi
own
problem
.

Becaus
he
's
all
but
elimin
crime
,
Amaze
's
endors
deal
ar
fall
through
.

The
public
crave
a
major
slugfest
,
so
Amaze
engin
the
parol
of
hi
one-tim
arch
enemi
Casanova
Frankenstein
-LRB-
Geoffrei
Rush
-RRB-
.

Frankenstein
succe
beyond
expect
;
he
captur
Capt.
Amaze
befor
set
out
on
hi
scheme
to
destroi
the
citi
.

So
,
no
on
is
left
to
save
the
dai
but
our
would-b
superhero
,
but
their
abil
ar
more
``
littl
leagu
''
than
``
Justic
Leagu
.
''

The
Raja
hurl
fork
at
foe
,
the
Shovel
hit
them
with
a
shovel
,
and
Mr.
Furiou
just
yell
at
peopl
-LRB-
Stiller
is
do
essenti
the
same
charact
he
plai
in
a
guest
appear
on
``
Friend
''
-RRB-
.

After
a
pain
first
strike
,
the
gui
hold
audit
for
teammat
and
add
The
Bowler
-LRB-
Janean
Garofalo
-RRB-
,
who
carri
her
dead
father
's
skull
in
her
bowl
ball
;
Invisibl
Boi
-LRB-
Kel
Mitchel
-RRB-
,
who
can
onli
us
hi
power
when
no
on
is
look
at
him
;
The
Spleen
-LRB-
Paul
Reuben
-RRB-
,
who
blow
devast
wind
from
hi
rear
;
and
The
Sphinx
-LRB-
We
Studi
-RRB-
,
a
mysteri
figur
who
will
teach
them
to
fight
.

``
Mysteri
Men
''
boast
on
of
the
most
talent
and
eclect
cast
ever
assembl
.

In
addit
to
those
alreadi
mention
,
the
credit
includ
Lena
Olin
as
Frankenstein
's
evil
shrink
,
comedian
Eddie
Izzard
and
the
Fuge
'
Prakazrel
Michel
as
disco-them
thug
,
singer
Tom
Wait
as
an
inventor
of
superhero
weapon
,
Artie
Lang
-LRB-
from
``
MAD
TV
''
-RRB-
as
anoth
thug
,
Clair
Forlani
-LRB-
from
``
Meet
Joe
Black
''
-RRB-
as
a
waitress
Mr.
Furiou
is
pursu
,
Louis
Lasser
-LRB-
from
``
Mari
Hartman
,
Mari
Hartman
''
-RRB-
as
the
Blue
Raja
's
mother
,
and
actor/magician
Ricki
Jai
as
Capt.
Amaze
's
manag
.

The
cast
includ
four
Oscar-nomin
actor
!

When
I
saw
the
list
of
player
earli
last
year
,
I
figur
the
result
would
be
a
ca
n't
-
miss
comedi
.

Yet
,
I
must
report
disappoint
.

The
script
is
probabl
the
worst
of
1999
.

Nearli
everi
joke
misfir
.

The
stab
at
comic
book
ar
too
obviou
to
be
funni
.

-LRB-
For
exampl
,
no
on
can
recogn
Capt.
Amaze
in
hi
secret
ident
becaus
he
's
wear
glass
.
-RRB-

The
fart
joke
surround
the
Spleen
ar
surprisingli
the
onli
gag
that
work
.

The
cast
tri
hard
,
especi
Hank
Azaria
,
whose
charact
adopt
an
effet
British
accent
while
in
costum
,
but
the
script
is
just
too
lame
to
run
.

