A
remak
of
the
1978
Warren
Beatti
vehicl
``
Heaven
Can
Wait
''
-LRB-
which
in
turn
wa
a
remak
of
``
Here
Come
Mr.
Jordan
''
-RRB-
,
``
Down
to
Earth
''
tell
the
tale
of
Lanc
Barton
-LRB-
Chri
Rock
,
``
Lethal
Weapon
IV
''
-RRB-
,
a
young
Black
standup
comic
try
to
win
over
the
audienc
at
Harlem
's
Apollo
Theater
.

When
he
's
taken
to
heaven
prematur
by
bumbl
angel
Key
-LRB-
Eugen
Levi
,
``
Best
in
Show
''
-RRB-
,
hi
onli
recours
is
to
return
in
anoth
bodi
.

He
choos
that
of
Charl
Wellington
,
the
tenth
richest
man
in
the
Unite
State
.

Of
cours
,
Wellington
is
not
onli
rich
,
but
white
andyear
old
,
which
make
Lanc
's
bid
for
close
night
at
the
Apollo
,
as
well
as
hi
desir
to
woo
Sunte
-LRB-
Regina
Taylor
,
``
Jerri
Maguir
''
-RRB-
,
a
tad
tricki
.

The
origin
Elain
May/Warren
Beatti
script
ha
been
rework
by
Chri
Rock
,
Lanc
Crouther
,
Ali
LeRoi
,
and
Loui
C.K.
to
turn
Beatti
's
vehicl
as
a
footbal
player
into
Rock
's
vehicl
as
a
comic
and
add
some
racial
humor
.

As
direct
by
Chri
Weitz
and
Paul
Weitz
-LRB-
codirector
of
``
American
Pie
''
and
costar
of
``
Chuck
&
Buck
''
-RRB-
,
the
whole
affair
come
off
as
amateur
hour
.

Initial
scene
plai
like
film
line
read
rehears
.

Product
valu
ar
shoddi
.

Most
seriou
of
all
is
the
us
of
Rock
when
hi
Wellington
counterpart
would
have
been
more
appropri
.

Thi
problem
is
clearli
attribut
to
the
fact
that
the
old
white
gui
is
never
lip
or
action
synch
when
he
IS
on
screen
.

Rock
doe
get
the
chanc
to
do
some
fun
schtick
,
such
as
hi
turnaround
on
Wellington
's
plan
for
a
poor
neighborhood
hospit
-LRB-
`
Bullet
in
the
head
?

You
got
a
bed
!
'
-RRB-

,
but
he
's
uneven
when
interact
with
the
rest
of
the
cast
.

Regina
Taylor
fare
well
amidst
the
mediocr
as
the
young
activist
who
confusedli
end
up
fall
for
the
man
she
believ
to
be
her
nemesi
.

Also
good
is
Franki
Faison
-LRB-
``
Hannib
''
-RRB-
as
Whitnei
,
Lanc
's
compassion
manager-th
onli
human
who
's
made
privi
to
Lanc
's
bodi
bodi
switch
.

Chaz
Palminteri
and
Levi
ar
pretti
much
wast
as
heaven
's
ambassador
.

Another
``
Best
in
Show
''
alumnu
,
Jennif
Coolidg
,
is
poorli
us
as
Wellington
's
cheat
wife
-LRB-
AND
shown
sever
dai
apart
wear
the
SAME
outfit
?!
-RRB-

and
Greg
Germann
-LRB-
TV
's
``
Ally
McBeal
,
''
``
Sweet
Novemb
''
-RRB-
continu
displai
hi
sitcom
root
as
her
lover
and
husband
's
lawyer
.

Mark
Addy
-LRB-
``
The
Full
Monti
''
-RRB-
is
sore
underutil
as
Cisco
,
Wellington
's
fake
English
butler
.

Steal
the
show
in
everi
scene
she
's
in
is
Wanda
Syke
-LRB-
``
The
Chri
Rock
Show
''
-RRB-
as
Wellington
's
disgruntl
maid
.

``
Down
to
Earth
''
is
for
Chri
Rock
fan
onli
.

All
other
should
go
rent
``
Heaven
Can
Wait
''
or
truli
be
brought
down
to
earth
.

