It
seem
like
the
perfect
concept
.

What
better
for
the
Farrelli
Brother
,
famou
for
write
and
direct
comedi
with
offens
subject
matter
,
than
to
make
a
movi
about
a
gui
with
a
split
person
?

It
's
exactli
the
sort
of
thing
the
Brother
relish
:
poke
fun
at
someth
seriou
-LRB-
in
thi
case
mental
ill
-RRB-
,
throw
all
care
to
the
wind
to
get
a
laugh
.

Jim
Carrei
's
sign
on
too
?

Even
better
.

The
Nation
Allianc
for
the
Mental
Ill
even
help
out
by
levi
complaint
against
the
Brother
'
new
film
befor
it
open
,
claim
it
wa
misrepres
the
condit
of
split
person
,
label
it
incorrectli
as
``
schizophrenia
,
''
and
so
forth
.

Such
a
protest
seem
like
just
the
sort
of
thing
that
would
,
of
cours
,
onli
add
more
fuel
to
the
Farrelli
Brother
'
fire
,
prove
that
some
peopl
just
could
n't
take
a
joke
,
and
that
the
Farrelli
would
be
help
the
more
enlighten
viewer
to
yet
anoth
dose
of
their
brilliantli
subvers
comedi
.

Ye
,
it
all
seem
perfect
,
but
on
thing
went
wrong
:
Their
movi
is
n't
funni
.

It
's
not
for
lack
of
try
.

The
Farrelli
util
in
``
Me
,
Myself
&
Irene
''
their
most
high-concept
premis
ever
:
Carrei
plai
Charli
Baileygait
,
a
man
who
after
be
dump
by
hi
wife
for
a
midget
limo
driver
,
decid
to
buri
all
hi
aggress
feel
deep
down
insid
and
never
releas
them
.

Thi
,
of
cours
,
mean
all
hi
neighbor
exploit
hi
entir
too-forgiv
natur
,
make
hi
job
as
a
Rhode
Island
state
trooper
increasingli
difficult
.

Soon
enough
,
Charli
's
repress
aggress
manifest
itself
into
a
second
,
independ
person
name
Hank
,
a
deep-voic
,
boorish
ogr
unafraid
of
take
the
assert
action
hi
predecessor
had
been
unabl
to
muster
.

Thi
gui
is
n't
abov
crash
a
car
through
the
wall
of
the
barber
shop
in
which
he
's
been
insult
,
or
hold
a
littl
girl
's
head
underwat
becaus
she
refus
to
stop
jump-rop
in
the
street
.

Then
thing
start
get
lost
in
the
most
complic
plot
the
Farrelli
have
ever
attempt
,
and
the
film
run
off
it
track
.

Some
have
suggest
that
thi
is
n't
a
problem
,
becaus
the
Farrelli
Brother
'
brand
of
humor
doe
n't
requir
plot
to
work
.

Thei
're
wrong
,
of
cours
:
Plot
wa
greatli
instrument
in
build
up
the
kind
of
rollick
comic
energi
that
infus
the
Farrelli
'
last
effort
,
1996
's
``
There
's
Someth
About
Mari
.
''

-LRB-
1999
's
``
Outsid
Provid
''
wa
technic
an
earlier
project
.
-RRB-

The
brother
'
earlier
film
wa
n't
funni
mere
becaus
it
contain
outrag
gag
-LRB-
despit
what
some
newsmagazin
articl
would
have
you
believ
-RRB-
,
but
rather
becaus
it
most
outrag
gag
were
entir
unexpect
.

In
``
Mari
,
''
the
Farrelli
manag
sever
time
to
pull
off
a
neat
sleight-of-hand
trick
:
Thei
'd
have
you
think
the
stori
wa
go
on
wai
,
then
reveal
it
real
direct
in
delightfulli
surpris
fashion
.

``
Me
,
Myself
&
Irene
,
''
by
contrast
,
seem
to
have
been
made
by
folk
who
look
at
``
Mari
''
and
saw
onli
the
surfac
gross
,
miss
all
of
the
subtl
machin
that
realli
made
it
work
.

Have
been
produc
by
the
same
gui
who
made
``
Mari
,
''
``
Irene
''
seem
like
an
even
bigger
disappoint
.

The
brother
pile
on
the
offens
humor
,
take
shot
at
race
,
midget
,
albino
,
mental
ill
,
and
all
manner
of
bathroom
joke
.

But
thei
have
n't
come
up
with
a
wai
to
make
ani
of
it
fresh
;
most
of
``
Me
,
Myself
&
Irene
''
come
off
as
rote
,
by-the-numb
,
adolesc
comedi
.

The
plot
,
with
Carrei
forc
to
drive
alleg
fugit
Irene
P.
Water
-LRB-
Rene
Zellweg
-RRB-
,
who
's
in
more
troubl
than
anyon
know
,
back
to
New
York
,
ha
an
end
that
's
entir
predict
from
the
get-go
.

-LRB-
Think
Charli
and
Irene
will
fall
in
love
?

Yeah
,
me
too
.
-RRB-

The
Farrelli
then
introduc
score
of
differ
charact
,
and
none
of
them
ever
manag
to
do
anyth
you
have
n't
alreadi
expect
them
to
do
,
no
matter
how
outrag
their
action
might
be
.

Compar
to
the
curvebal
the
Farrelli
ar
us
to
throw
,
thi
stuff
is
almost
entir
soft-toss
,
present
an
obviou
problem
:
When
gross-out
humor
lose
it
shock
valu
,
it
's
no
longer
funni
,
mere
gross
.

The
joke
that
do
work
ar
milk
over
and
over
until
their
effect
run
dry
.

Take
,
for
exampl
,
the
subplot
involv
Charli
's
three
Black
son
-LRB-
Anthoni
Anderson
,
Mongo
Brownle
,
Jerod
Mixon
-RRB-
.

The
incongru
of
it
all
is
funni
for
a
while
,
with
three
burli
Black
men
discuss
higher
math
in
ghetto
languag
and
white-bread
Carrei
mouth
said
languag
with
an
entir
too-pleas
smile
on
hi
face
.

But
by
the
end
of
the
film
,
thei
're
still
do
the
same
schitck
;
it
ha
n't
been
elev
to
anoth
,
funnier
level
,
and
it
ha
n't
been
drop
either
.

That
's
too
bad
,
becaus
it
ceas
to
be
amus
about
halfwai
through
.

``
Me
,
Myself
&
Irene
''
reek
of
wast
opportun
.

There
ought
to
be
more
focu
on
how
other
peopl
react
to
Charli
's
new
person
,
and
on
how
Charli
deal
with
the
consequ
of
Hank
's
action
.

Thi
doe
n't
realli
happen
;
nearli
everi
support
charact
learn
about
Charli
's
condit
earli
on
,
so
thei
do
n't
have
ani
opportun
to
be
surpris
by
it
.

The
film
throw
what
look
like
a
patent
Farrelli
curv
in
a
scene
toward
the
midwai
point
-LRB-
involv
an
albino
companion
Charli
and
Irene
pick
up
call
,
appropri
,
``
Whitei
''
-RRB-
,
but
the
script
doe
n't
go
anywher
with
it
,
instead
leav
the
thread
twist
in
the
wind
befor
awkwardli
ty
it
up
dure
the
climax
.

Jim
Carrei
is
a
gift
comedian
,
both
physic
and
vocal
,
but
he
's
left
with
noth
much
to
do
here
except
contort
himself
in
a
manner
similar
to
Steve
Martin
in
``
All
of
Me
.
''

It
's
a
great
showcas
of
flexibl
and
split-second
role-shift
,
but
none
of
it
is
terribl
funni
.

Carrei
doe
n't
pull
ani
stunt
we
do
n't
expect
him
to
pull
,
and
the
Farrelli
'
script
doe
n't
give
him
anyth
els
to
pull
:
The
situat
in
which
he
must
perform
the
role-shift
ar
n't
set
up
in
ani
meaning
wai
.

Perhap
Carrei
can
take
solac
in
the
fact
that
hi
support
actor
fare
no
better
.

Zellweg
's
Irene
is
not
a
strong
femal
lead
;
Mari
in
``
Mari
''
mai
have
been
part
adolesc
fantasi
,
but
she
wa
also
intellig
and
strong-wil
.

Irene
is
noth
in
particular
,
as
the
film
never
make
clear
whether
she
's
ditzi
,
clever
,
or
neither
.

As
such
,
she
give
us
noth
to
latch
onto
as
the
onli
``
sane
''
person
in
the
film
.

Chri
Cooper
is
stuck
plai
exactli
on
note
as
a
corrupt
FBI
agent
,
and
hi
charact
is
entir
too
straight-lac
for
a
movi
like
thi
.

He
,
like
the
other
,
doe
absolut
noth
unexpect
.

After
view
the
shapeless
mess
that
``
Me
,
Myself
&
Irene
''
eventu
dissolv
into
,
I
wa
stuck
wonder
whether
or
not
the
Farrelli
had
outsmart
themselv
.

Mayb
their
kind
of
comedi
can
onli
work
for
so
long
until
audienc
get
wise
to
it
and
stop
be
shock
.

But
I
do
n't
believ
it
--
good
filmmak
find
wai
of
surpris
their
audienc
even
after
peopl
have
grown
attun
to
their
style
.

If
the
Farrelli
ar
inde
good
,
smart
filmmak
-LRB-
and
I
still
think
thei
ar
-RRB-
,
thei
'll
rebound
just
fine
.

Even
after
that
happen
,
though
,
I
'll
still
consid
``
Me
,
Myself
&
Irene
''
to
be
a
high-calib
misfir
.

