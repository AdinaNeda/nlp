She
wa
the
femm
in
``
La
Femm
Nikita
.
''

He
wa
the
Baldwin
in
``
Backdraft
,
''
``
Sliver
,
''
and
``
Fair
Game
''
-LRB-
with
Cindi
Crawford
-RRB-
.

Togeth
,
Anne
Parillaud
and
William
Baldwin
conspir
to
make
``
Shatter
Image
''
the
biggest
piec
of
hooei
sinc
the
Stallone/Ston
``
thriller
''
``
The
Specialist
.
''

The
film
pose
the
question
``
what
if
the
life
you
're
live
now
is
realli
a
dream
,
and
your
dream
realiti
?
''

It
's
either
about
a
woman
who
's
haunt
by
a
recur
-LRB-
and
recur
and
recur
-RRB-
nightmar
that
she
's
a
hire
assassin
,
or
it
's
about
a
hire
assassin
who
's
haunt
by
a
recur
-LRB-
and
recur
and
recur
-RRB-
nightmar
that
she
's
honeymoon
with
William
Baldwin
in
Jamaica
.

It
doe
n't
much
matter
and
believ
me
by
the
time
``
Shatter
Image
''
run
it
pain
and
pedestrian
cours
you
wo
n't
care
.

These
two
live
,
with
Parillaud
look
like
Siouxsi
Sioux
with
a
black
wig
,
black
emotionless
ey
,
and
black
leather
cloth
in
the
Seattle-bas
assassin
scene
,
and
mope
around
like
Karen
Carpent
in
the
Jamaica
scene
,
plai
out
endlessli
throughout
the
film
and
the
result
is
it
's
now
twice
as
bore
as
it
might
have
been
.

It
's
not
that
complic
plot
ca
n't
be
entertain
.

Of
cours
it
help
if
you
have
interest
charact
,
crisp
dialogu
,
and
a
modicum
of
style
.

``
Shatter
Image
''
is
n't
complex
,
it
's
just
stupid
.

And
bore
.

Parillaud
and
Baldwin
,
who
ar
n't
exactli
Shakespearean
materi
to
begin
with
,
ar
saddl
with
such
leaden
dialogu
that
their
charact
have
zero
chanc
of
break
free
of
their
cardboard
confin
.

Line
like
:
``
You
're
not
the
reason
I
could
n't
care
less
about
you
.
''

Huh
?

And
thi
wonder
bathroom
interchang
earli
in
the
film
:
Talk
of
pant
,
Parillaud
ha
her
cloth
off
faster
than
you
can
sai
``
Point
of
No
Return
.
''

We
have
come
to
expect
thi
from
Billi
Baldwin
,
but
it
might
have
been
nice
to
have
learn
someth
about
their
charact
first
.

But
there
's
noth
to
learn
.

Karen
is
as
interest
as
a
cereal
box
,
a
someon
's
-
out-to-get-m
crybabi
who
imagin
the
voic
at
the
other
end
of
the
phone
,
the
stranger
who
send
her
flower
,
mayb
even
her
husband
himself
,
is
her
would-b
killer
.

Siouxsi
is
the
chromium
cool
,
tough-as-nail
crack
kill
machin
who
shoot
out
a
coupl
of
mirror
in
order
to
justifi
the
film
's
meaningless
stock
titl
.

Baldwin
seem
more
interest
in
Parillaud
's
nest
egg
-LRB-
so
that
he
can
pave
paradis
and
put
up
a
park
lot
-RRB-
than
he
doe
in
her
.

Each
time
Graham
Green
show
up
he
get
kill
.

Barbet
Schroeder
-LRB-
``
Revers
of
Fortun
''
-RRB-
co-produc
and
should
be
asham
of
himself
.

Everi
now
and
again
it
's
fun
to
watch
a
realli
bad
movi
.

And
everi
now
and
again
,
as
``
Shatter
Image
''
make
agonizingli
clear
,
it
is
n't
.

