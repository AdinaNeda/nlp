One
of
the
contributor
to
the
destruct
of
the
Batman
film
franchis
Chri
O'Donnel
star
in
thi
remak
of
Buster
Keaton
's
1925
silent
film
Seven
Chanc
.

Now
I
've
never
seen
Seven
Chanc
,
as
I
'm
more
of
a
Chaplin
fan
than
a
Keaton
fan
,
but
I
serious
doubt
that
the
classic
version
could
be
as
insipid
as
thi
.

O'Donnel
plai
Jimmi
Shannon
,
the
manag
of
a
pool
tabl
manufactur
compani
.

He
also
fear
commit
,
and
despit
date
Anne
-LRB-
Rene
Zellweg
-RRB-
for
over
three
year
,
he
's
unwil
to
marri
her
.

Hi
reason
?

Well
he
's
a
gui
you
see
,
and
well
,
gui
ar
like
stallion
,
and
thei
appar
do
n't
like
to
be
rope
or
someth
-LRB-
hei
,
that
's
the
movi
's
explan
...
I
'd
marri
Rene
Zellweg
in
a
second
-RRB-
.

Eventual
it
come
time
for
Jimmi
to
propos
,
and
when
he
doe
,
it
's
the
most
absurd
propos
ever
utter
by
a
man
EVER
.

He
essenti
sai
to
her
,
``
You
win
''
and
hand
her
a
ring
.

The
audienc
should
now
hate
thi
charact
.

I
did
.

Then
the
meat
of
the
plot
present
itself
.

Jimmi
's
grandfath
-LRB-
Peter
Ustinov
,
complet
wast
-RRB-
di
and
leav
Jimmi
hi
entir
estat
of
100
million
dollar
as
well
as
ownership
of
the
pool
tabl
busi
.

Howev
,
in
order
to
get
the
monei
and
save
the
job
of
the
factori
worker
,
Jimmi
must
be
marri
befor
hi
next
birthdai
-LRB-
which
is
just
over
24
hour
awai
-RRB-
.

Have
blown
hi
chanc
with
Anne
,
Jimmi
proce
to
track
down
all
hi
prior
girlfriend
and
suggest
a
``
busi
proposit
''
.

The
movi
wa
n't
half
bad
by
thi
time
,
surprisingli
.

It
wa
n't
bore
me
,
and
even
manag
to
read
my
thought
at
on
point
.

Ustinov
's
charact
left
a
video
will
,
and
while
he
's
read
the
ridicul
claus
in
it
,
I
thought
to
myself
``
What
is
thi
,
Brewster
's
Million
?
''
.

As
soon
as
I
thought
it
,
Hal
Holbrook
's
charact
actual
sai
``
What
is
thi
,
Brewster
's
Million
?
''

It
wa
quit
surreal
.

Someth
happen
though
dure
the
film
's
final
act
that
caus
me
to
sharpli
drop
the
rate
it
wa
go
to
receiv
.

Up
to
that
point
,
all
of
Jimmi
's
prior
girlfriend
had
turn
down
hi
``
busi
proposit
''
which
I
felt
wa
good
.

At
least
the
film
wa
n't
paint
women
to
be
cold
heart
gold
digger
.

When
Jimmi
ha
no
other
option
,
hi
goofi
friend
-LRB-
plai
by
Artie
Lang
,
make
a
career
out
of
plai
the
goofi
friend
-RRB-
place
an
ad
in
the
newspap
that
somehow
becom
a
front
page
stori
.

At
thi
point
,
100,000
women
don
wed
gown
and
head
off
to
persuad
Jimmi
to
pick
them
to
be
hi
bride
.

These
women
arriv
at
Jimmi
's
locat
and
IMMEDIATELY
start
bulli
him
about
what
he
's
look
for
in
a
woman
.

When
he
's
had
enough
of
their
unjustifi
attack
,
he
tell
the
angri
mob
that
there
's
been
a
mistak
and
he
wo
n't
be
marri
ani
of
them
.

As
a
result
,
the
women
proce
to
chase
him
around
the
citi
,
in
hope
of
catch
him
and
tear
hi
limb
off
.

Thi
goe
on
FOR
THE
REST
OF
THE
FILM
.

It
's
just
Chri
O'Donnel
run
from
100,000
obnoxi
,
greedi
,
angri
and
stupid
women
.

My
groan
of
disgust
could
be
heard
for
mile
.

The
film
final
end
-LRB-
thankfulli
-RRB-
,
but
to
thi
dai
I
'm
still
groan
.

The
Bachelor
is
avail
on
DVD
from
New
Line
Home
Video
.

It
contain
the
film
in
both
full
frame
and
in
it
origin
theatric
aspect
ratio
of
1.85:1
,
cast
and
crew
info
,
and
special
DVD-ROM
featur
.

The
origin
theatric
trailer
is
also
on
the
disc
,
which
contain
scene
not
in
the
film
.

Apparent
there
wa
even
more
footag
of
the
gang
of
bride
chase
O'Donnel
around
the
citi
,
and
at
on
point
O'Donnel
is
leap
from
the
top
of
buse
.

So
that
wa
bad
enough
to
be
cut
,
but
the
other
30
minut
of
bride
shenanigan
wa
good
?

Ugh
.

