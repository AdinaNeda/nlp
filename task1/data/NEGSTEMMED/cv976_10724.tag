Nostalgia
for
the
70
continu
,
as
we
see
a
reviv
of
on
of
the
decad
's
greatest
achiev
:
the
marijuana
comedi
.

Howev
Half
Bake
doe
n't
quit
run
with
all
it
brain
cell
,
and
will
make
you
appreci
the
question
talent
of
Cheech
and
Chong
all
the
more
.

The
plot
follow
the
misadventur
of
four
ne'er
-
do-wel
stoner
.

There
's
the
group
's
unoffici
leader
,
Thurgood
-LRB-
David
Chappel
-RRB-
,
Scarfac
-LRB-
Guillermo
Diaz
-RRB-
,
Brian
-LRB-
Jim
Breuer
-RRB-
,
and
Kenni
-LRB-
Harland
William
-RRB-
.

Kenni
get
into
troubl
,
when
,
while
on
a
munchi
run
,
feed
hi
snack
food
to
a
diabet
polic
hors
.

When
the
anim
keel
over
,
he
find
himself
accus
of
kill
a
polic
offic
,
and
face
a
$
1,000,000
bail
.

Hi
friend
promis
to
rais
monei
for
a
10
%
bail
bond
,
but
have
no
idea
how
.

That
is
,
until
Thurgood
stumbl
upon
a
stash
of
pharmaceut
marijuana
be
test
at
the
compani
where
he
work
as
a
janitor
.

Soon
the
three
gui
ar
deal
dope
to
rais
fund
,
while
avoid
the
cop
and
rival
dealer
Sampson
Simpson
-LRB-
Clarenc
William
III
-RRB-
.

For
a
comedi
,
the
film
is
pretti
humorless
.

Not
that
it
doe
n't
try
...
it
's
just
that
the
comic
setup
ar
obviou
and
the
payoff
nearli
all
fall
flat
.

The
four
lead
ar
nearli
all
plai
the
same
charact
.

Only
William
stand
out
-LRB-
while
still
perform
on
the
level
of
hi
humor-fre
comedi
Rocket
Man
-RRB-
,
but
that
is
becaus
he
's
imprison
throughout
most
of
the
film
,
give
a
much
need
chang
of
pace
-LRB-
but
mostli
swap
on
set
of
obviou
gag
for
anoth
-RRB-
.

To
help
out
,
the
film
is
pack
full
of
cameo
.

Steven
Wright
,
Tommi
Chong
,
Janean
Garofalo
,
Willi
Nelson
,
Snoop
Doggi
Dogg
,
and
Jon
Stewart
all
make
appear
at
on
point
or
anoth
.

None
of
them
work
,
beyond
the
simpl
``
hei
,
that
's
_____
''
level
.

In
fact
the
funniest
work
in
the
film
come
from
Chappel
.

Not
as
hi
bland
pothead
lead
,
but
in
hi
second
role
,
as
a
pot-obsess
rapper
,
Sir
Smokealot
.

Grant
,
it
's
pretti
much
a
one-jok
role
,
and
there
ar
n't
a
ton
of
laugh
...
but
thi
film
need
everi
on
it
can
scrape
up
.

To
top
it
off
,
and
in
a
move
contrast
with
the
tone
of
the
rest
of
the
film
,
Thurgood
is
given
a
love
interest
,
Mari
Jane
-LRB-
Rachel
True
-RRB-
.

Her
role
is
that
of
the
Public
Servic
Announcement
:
to
inform
us
why
do
drug
-LRB-
includ
pot
-RRB-
is
wrong
.

Her
charact
seem
fabric
mere
as
a
defens
to
the
``
your
film
promot
the
us
of
drug
''
camp
.

The
film
would
have
been
better
off
by
stick
with
the
``
rebel
''
tone
it
so
eagerli
tri
to
claim
.

Yet
,
in
the
end
,
it
doe
n't
realli
matter
.

Watch
the
film
clean
and
sober
,
you
ar
bound
to
recogn
how
truli
aw
it
is
.

