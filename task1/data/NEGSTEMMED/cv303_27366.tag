ANYWHERE
BUT
HERE
-LRB-
director
:
Wayn
Wang
;
screenwrit
:
Alvin
Sargent/bas
on
the
book
by
Mona
Simpson
;
cinematograph
:
Roger
Deakin
;
editor
:
Nichola
C.
Smith
;
cast
:
Susan
Sarandon
-LRB-
Adele
August
-RRB-
,
Natali
Portman
-LRB-
Ann
August
-RRB-
,
Eileen
Ryan
-LRB-
Lillian
-RRB-
,
Rai
Baker
-LRB-
Ted
-RRB-
,
John
Diehl
-LRB-
Jimmi
-RRB-
,
Shawn
Hatosi
-LRB-
Benni
-RRB-
,
Bonni
Bedelia
-LRB-
Carol
-RRB-
,
Michael
Milhoan
-LRB-
Policeman
-RRB-
,
Hart
Bochner
-LRB-
Dr.
Josh
Spritzer
-RRB-
,
Corbin
Allred
-LRB-
Peter
-RRB-
;
Runtim
:
113
;
Fox
2000
Pictur
,
1999
-RRB-
The
titl
is
taken
from
the
write
of
Ralph
Waldo
Emerson
describ
the
life
of
a
travel
as
`
Anywher
but
here
.
'

There
is
n't
on
thing
about
thi
mother-daught
relationship
melodrama
that
felt
honest
.

It
reli
on
a
contriv
minor
charact
to
tell
both
mother
and
daughter
what
wa
obviou
to
do
in
order
to
straighten
out
their
dysfunct
live
,
as
for
some
reason
thei
could
n't
figur
it
out
for
themselv
.

If
the
film
look
familiar
,
that
is
becaus
``
The
Slum
of
Beverli
Hill
-LRB-
98
-RRB-
''
went
over
the
same
ground
but
wa
fortun
to
be
more
comic
and
percept
due
to
Alan
Arkin
and
Natasha
Lyonn
perform
,
in
which
thei
were
fortun
to
have
a
much
sharper
script
to
work
with
.

Alvin
Sargent
's
script
wa
weak
and
annoyingli
claustral
.

Wayn
Wang
's
-LRB-
``
The
Joi
Luck
Club
''
-RRB-
inept
direct
made
a
weak
script
even
weaker
by
hi
inabl
to
tell
the
stori
unfold
without
the
voiceov
relat
what
the
action
could
n't
convei
.

Pushi
mother
Adele
August
-LRB-
Susan
Sarandon
-RRB-
forc
her
14-year-old
daughter
Ann
-LRB-
Natali
Portman
-RRB-
to
leav
a
small
Midwestern
burg
to
go
cross-countri
with
her
to
Beverli
Hill
.

The
daughter
resent
her
twice
divorc
,
unstabl
but
exot
,
schoolteach
mother
,
bulli
her
to
be
an
actress
as
a
mean
of
escap
a
humdrum
life
.

The
daqught
doe
n't
even
like
the
us
gold
Merced
mother
bui
to
drive
to
LaLa
land
in
and
ca
n't
stand
the
Beach
Boi
record
mom
enjoi
hear
on
the
car
radio
.

So
it
becom
a
question
of
who
know
best
,
as
the
ladi
fight
it
out
between
them
,
until
the
predict
sweet
end
come
in
thi
yawner
.

Adele
's
dream
of
greener
pastur
in
Beverli
Hill
soon
doe
n't
look
that
good
,
as
she
land
a
job
in
a
slum
school
,
ha
her
preciou
car
scratch
by
the
unruli
kid
,
and
settl
into
a
job
she
hate
in
order
to
financi
surviv
.

Daughter
pine
for
small
town
Wisconsin
and
her
friend
there
.

Dure
on
of
mani
argument
with
her
free-spirit
mom
,
thei
go
for
ic
cream
and
while
mother
is
be
ticket
,
she
run
awai
,
onli
to
be
chase
down
by
friendli
traffic
cop
-LRB-
Michael
Milhoan
-RRB-
,
who
offer
her
wise
counsel
.

With
the
cop
's
Zen
wisdom
pass
onto
the
daughter
,
she
will
surviv
live
in
mani
differ
address
in
Beverli
Hill
until
she
reachesand
then
she
plan
to
get
wai
from
mom
by
us
her
good
grade
to
go
to
Brown
Univers
instead
of
UCLA
.

Dure
their
stai
in
Beverli
Hill
,
she
grow
from
feel
awkward
in
Beverli
Hill
High
to
immedi
have
mani
high
school
girlfriend
and
even
a
high
school
rich
boi
admir
--
a
T.S.
Eliot
-LRB-
Corbin
Allred
-RRB-
reader
.

But
the
transit
of
her
life
into
Beverli
Hill
had
no
feel
of
realiti
,
as
everyth
seem
stage
and
unemot
.

When
she
learn
her
cousin
Benni
-LRB-
Shawn
Hatosi
-RRB-
,
who
happen
to
be
her
best
friend
di
in
a
traffic
accid
back
in
Wisconsin
,
she
return
for
the
funer
and
in
those
reunion
scene
it
wa
n't
clearli
shown
why
she
want
to
stai
there
,
and
for
that
matter
,
it
wa
never
made
clear
why
the
mother
want
to
leav
so
badli
.

The
film
is
on
big
battl
of
will
between
mother
and
daughter
over
their
dream
.

I
guess
what
the
filmmak
is
try
to
sai
,
is
that
mom
is
a
bad
dreamer
and
the
daughter
is
the
observ
on
,
abl
to
recogn
mother
's
fault
when
not
in
grow
pain
and
in
need
of
parent
herself
.

Mother
ha
a
seri
of
setback
,
like
be
dump
by
her
dream-boat
dentist
-LRB-
Bochner
-RRB-
she
met
on
the
beach
,
and
wit
her
daughter
mimic
her
whini
optimist
sai
when
try
out
for
an
act
part
.

But
mother
learn
that
her
daughter
ha
grown-up
and
is
independ
and
that
she
ha
to
stop
live
her
life
through
her
.

She
learn
thi
when
the
same
wise
cop
who
told
Ann
what
to
do
,
is
about
to
ticket
her
and
thi
time
will
remind
the
mother
of
the
right
thing
to
do
.

Thi
of
cours
result
in
the
corni
end
,
show
that
mom
's
heart
wa
alwai
in
the
right
place
,
onli
she
went
about
it
in
the
wrong
wai
.

Thi
wa
just
on
of
those
film
where
you
want
to
be
anywher
but
in
the
theater
where
thi
film
is
show
.

Denni
Schwartz
:
``
Ozu
'
World
Movi
Review
''
ALL
RIGHTS
RESERVED
DENNIS
SCHWARTZ

