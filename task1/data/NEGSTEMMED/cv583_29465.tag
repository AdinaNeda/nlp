Susan
Granger
's
review
of
``
TWO
CAN
PLAY
THAT
GAME
''
-LRB-
Screen
Gem
-RRB-
Sinc
the
success
of
``
Wait
to
Exhale
,
''
there
have
been
sever
romant
comedi
about
African-American
profession
.

Thi
femal
reveng
fantasi
,
Vivica
A.
Fox
plai
Shant
Smith
,
a
stun
ad
exec
who
seem
to
have
reach
the
pinnacl
of
success
:
a
mansion
,
a
sporti
car
and
an
idyl
relationship
with
a
hunki
,
hot-shot
lawyer
,
Morri
Chestnut
.

She
's
at
a
point
in
her
life
when
she
dispens
advic
to
her
grate
girl-friend
-LRB-
Mo'Niqu
,
Wendi
Racquel
Robinson
,
Tamala
Jone
-RRB-
.

``
When
your
man
mess
up
,
no
matter
how
small
it
is
,
''
she
smugli
decre
,
``
yuh
got
to
punish
him
.
''

So
when
she
catch
her
hot
man
danc
at
a
bar
with
a
smart
and
sexi
rival
,
Gabriel
Union
,
she
devis
her
own
version
of
``
The
Rule
,
''
a
10-dai
``
tough
love
''
emotion
punish
plan
to
get
him
back
.

Unfortun
,
writer/director
Mark
Brown
-LRB-
``
How
To
Be
a
Player
''
-RRB-
break
the
cardin
rule
of
romant
comedi
:
you
have
to
like
the
protagonist-and
shrill
,
self-congratulatori
Shant
Smith
is
a
smirk
,
superfici
,
spite
shrew
who
doe
n't
realiz
that
ration
rule
can
not
alwai
be
appli
to
love
.

Have
her
talk
directli
into
the
camera
get
stale
veri
quickli
and
the
``
Dai
One
,
''
``
Dai
Two
''
title-card
devic
underscor
the
tedium
.

Comic
Anthoni
Anderson
score
as
Chestnut
's
boister
best-friend
,
and
singer
Bobbi
Brown
doe
a
cameo
as
a
scuzzi
mechan
who
's
given
a
smooth
makeov
by
Ms.
Robinson
.

But
the
out-tak
over
the
close
credit
contain
more
humor
than
the
film
itself
.

On
the
Granger
Movi
Gaug
of
1
to
10
,
``
Two
Can
Plai
That
Game
''
is
smarmi
if
slick
4
,
fill
with
misogynist
attitud
and
blatant
product
placement
-LRB-
Coca-Cola
,
Miller
Genuin
Draft
-RRB-
but
littl
els
.

In
thi
R-rate
-LRB-
for
explicit
sexual
languag
-RRB-
,
pseudo-hip
battl
of
the
sex
,
the
audienc
lose
.

