Talk
about
beat
a
dead
hors
!

When
HOME
ALONE
wa
releas
in
1990
,
it
wa
a
breath
of
fresh
air
,
and
the
final
box
offic
talli
indic
how
much
audienc
appreci
a
genuinely-funni
famili
film
.

The
unexpectedli
high
gross
guarante
a
sequel
,
so
,
two
year
later
,
we
were
subject
to
HOME
ALONE
2
,
which
might
as
well
have
been
call
CLONE
ALONE
for
all
of
the
origin
it
exhibit
.

For
John
Hugh
,
two
HOME
ALONE
movi
were
n't
enough
he
began
recycl
the
same
kind
of
villain
and
situat
in
almost
everi
movi
he
wa
involv
with
,
includ
a
pathet
box
offic
bomb
call
BABY
'S
DAY
OUT
,
last
year
's
live-act
101
DALMATIANS
,
and
thi
year
's
FLUBBER
.

Now
,
inexplic
,
Hugh
ha
exhum
not
onli
these
worn-out
plot
element
,
but
the
``
HOME
ALONE
''
name
as
well
.

The
result
1997
's
worst
sequel
-LRB-
edg
out
SPEED
2
and
BATMAN
AND
ROBIN
-RRB-
,
HOME
ALONE
3
.

By
chang
the
charact
,
Hugh
-LRB-
who
wrote
and
co-produc
the
film
-RRB-
,
along
with
hi
co-conspir
,
director
Raja
Gosnel
,
ha
attempt
to
inject
new
life
into
a
seri
that
is
wai
past
the
point
of
cardiac
arrest
.

The
new
kid
,
Alex
Pruitt
,
is
plai
by
Alex
D.
Linz
-LRB-
ONE
FINE
DAY
-RRB-
,
and
the
onli
thing
he
ha
go
for
him
is
termin
cute
.

He
's
not
half
as
interest
as
Macaulai
Culkin
onc
wa
.

The
villain
,
pale
copi
of
Joe
Pesci
and
Daniel
Stern
,
ar
even
less
engag
than
the
pair
of
idiot
in
FLUBBER
.

In
HOME
ALONE
3
,
there
ar
four
of
them
-LRB-
Olek
Krupa
,
David
Thornton
,
Lenni
von
Dohlen
,
and
Rya
Kihlstedt
-RRB-
,
but
all
that
mean
is
an
opportun
for
twice
as
mani
pratfal
.

Thi
time
,
the
kid
is
n't
left
home
alon
becaus
hi
parent
have
gone
on
a
trip
.

Instead
,
he
ha
develop
a
bad
case
of
chicken
pox
,
so
he
ca
n't
go
to
school
.

Hi
dad
-LRB-
Kevin
Kilner
-RRB-
is
awai
on
busi
and
hi
mom
-LRB-
Haviland
Morri
-RRB-
ha
to
run
errand
,
so
,
for
the
most
part
,
he
's
all
by
himself
dure
the
dai
.

Through
a
seri
of
coincid
too
irrit
to
relat
,
a
top
secret
U.S.
Air
Forc
integr
circuit
come
into
hi
possess
.

It
's
want
by
a
gang
of
four
intern
crook
who
intend
to
break
into
Alex
's
home
to
retriev
it
.

The
eight-year
old
,
who
is
wise
beyond
hi
year
,
boobi
trap
the
hous
with
all
sort
of
Rube
Goldberg-typ
devic
design
to
humili
and
incapacit
the
villain
.

The
movi
's
climax
take
place
dure
a
rage
snowstorm
onli
none
of
the
fall
flake
look
remot
believ
.

Previous
,
the
most
counterfeit-look
snow
effect
I
can
rememb
were
in
STAR
TREK
III
.

These
ar
far
wors
.

In
fact
,
the
product
valu
ar
so
shoddi
that
there
ar
some
scene
in
the
midst
of
thi
near-blizzard
when
a
shine
sun
can
be
seen
.

If
IT
'S
A
WONDERFUL
LIFE
could
gener
real-look
snow
back
in
the
1940
,
why
ca
n't
HOME
ALONE
3
,
which
ha
a
significantli
larger
budget
and
'
90
technolog
at
it
dispos
?

There
ha
been
an
ongo
debat
regard
the
appropri
of
live-act
cartoon
violenc
for
young
children
.

HOME
ALONE
3
will
add
fuel
to
the
fire
.

It
's
on
thing
to
see
Wyle
E.
Coyot
flatten
by
a
10
ton
Acme
weight
,
but
quit
anoth
to
watch
a
run
lawn
mower
fall
on
Lenni
von
Dohlen
.

Adult
and
even
older
children
will
recogn
that
thi
is
obvious
fake
and
intend
to
be
humor
,
but
what
about
five
and
six-year
old
?

The
level
of
violenc
in
HOME
ALONE
3
is
extrem
mani
of
Alex
's
scheme
ar
nasti
enough
to
kill
.

But
,
becaus
thi
is
a
``
famili
film
,
''
no
on
di
,
despit
be
electrocut
,
fall
thirti
feet
,
and
get
smack
on
the
head
by
a
barbel
.

Not
onli
is
HOME
ALONE
3
unnecessari
,
but
it
's
offens
.

It
's
an
exercis
in
tedious
,
and
there
is
n't
a
genuin
laugh
to
be
found
from
the
begin
to
the
end
-LRB-
unless
,
by
some
strang
quirk
of
fate
,
you
have
miss
everi
1990
movi
associ
with
John
Hugh
,
and
thu
have
n't
seen
thi
stuff
befor
-RRB-
.

I
ca
n't
imagin
anyon
with
a
reason
attent
span
be
more
than
momentarili
distract
by
thi
pointless
adventur
.

Mayb
that
's
why
the
onli
on
laugh
at
the
screen
I
attend
were
still
in
their
thumb-suck
year
.

