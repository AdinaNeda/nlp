_________________________________________________________
The
recent
onslaught
of
film
noir
that
ha
pop
up
in
multiplex
,
with
everyth
rang
from
``
L.A.
Confidenti
''
to
``
Palmetto
''
to
``
The
Big
Lebowski
,
''
ha
prove
to
be
an
artist
commod
for
veteran
,
talent
director
.

With
thi
particular
genr
,
director
ar
abl
to
collat
the
sleazi
underworld
of
the
lower-class
and
the
glamor
,
opul
upper-class
,
while
maintain
the
noirish
,
ambigu
ambienc
,
the
stapl
of
film
noir
.

``
Wild
Thing
''
could
be
classifi
as
a
black
comedi
,
a
sultri
film
noir
or
a
plain
ol'
erot
thriller
,
but
the
on
categori
it
doe
n't
qualifi
for
is
that
of
a
qualiti
motion
pictur
.

Overact
,
overwrought
and
overlong
,
``
Wild
Thing
''
is
a
confus
mess
,
a
movi
that
want
to
have
it
cake
and
eat
it
too
.

With
it
incalcul
twist
,
turn
,
more
twist
and
more
turn
,
it
soon
becom
a
tediou
exercis
in
pointless
.

In
spite
of
game
effort
by
Kevin
Bacon
and
Matt
Dillon
,
who
manag
to
sustain
straight
face
throughout
the
ordeal
,
``
Wild
Thing
''
not
onc
jell
in
it
erotic
,
unpredict
,
charact
motiv
,
sens
of
the
world
,
plot
,
etc.
.

There
's
not
on
redeem
qualiti
in
the
pictur
that
is
not
technic
.

Direct
by
John
McNaughton
-LRB-
``
Henri
:
Portrait
of
a
Serial
Killer
''
-RRB-
,
``
Wild
Thing
''
follow
Sam
Lombardo
-LRB-
Matt
Dillon
-RRB-
,
a
hunkish
,
suburban
Florida
high
school
teacher
who
's
the
dream
of
everi
spruce
up
femal
in
the
commun
.

Particularli
on
.

Kelli
Van
Ryan
-LRB-
Denis
Richard
-RRB-
ador
him
to
such
extent
that
she
offer
to
wash
hi
Jeep
with
a
partner
.

Soon
,
her
tini
short
ar
soak
and
tell
the
other
girl
to
take
a
hike
,
enabl
her
to
come
into
hi
hous
.

Next
thing
,
she
cri
rape
.

And
the
so-cal
``
roller-coast
ride
''
ensu
.

A
second
accus
come
front
,
Suzi
Toller
-LRB-
Neve
Campbel
-RRB-
,
a
booze-drinkin
'
,
tattoo
piec
of
trailer
trash
that
charg
Lombardo
of
sexual
molest
her
,
virtual
guarante
a
trial
where
preposter
confess
,
asinin
occurr
and
laughabl
courtroom
procedur
ar
bound
to
happen
.

Even
Bill
Murrai
get
into
the
act
as
Lombardo
's
zani
lawyer
.

He
wear
a
fake
neck-brac
,
wave
the
finger
at
the
other
client
and
infus
the
script
with
some
Billi
Murrai
.

You
can
tell
McNaughton
knew
he
had
noth
to
lose
assign
Murrai
to
thi
role
.

As
ridicul
as
it
sound
,
he
's
the
most
sane
charact
in
the
movi
.

The
commun
is
astonish
.

Report
flee
to
the
scene
.

There
's
an-million
settlement
for
a
libel
suit
against
Kelli
's
mother
-LRB-
Theresa
Russel
-RRB-
.

There
's
the
ambigu
cop
-LRB-
Kevin
Bacon
,
who
also
execut
produc
-RRB-
who
sniff
someth
iffi
in
thi
concoct
of
deceit
,
murder
and
lust
.

We
,
the
audienc
,
sniff
someth
not
so
lust
.

Like
so
mani
thriller
these
dai
,
includ
``
The
Usual
Suspect
''
and
``
The
Game
,
''
``
Wild
Thing
''
eschew
charact
for
plot
.

McNaughton
know
how
to
handl
the
camera
,
and
he
keep
the
campi
fill
of
the
script
flow
with
workmanlik
eas
,
as
if
he
know
the
territori
he
's
cover
is
a
harmless
appet
to
accompani
``
Henri
,
''
a
furiou
,
unsettl
film
directori
tour-de-forc
.

Georg
Clinton
's
score
is
a
delight
to
behold
.

But
it
's
the
human
.

The
charact
ar
mere
devic
for
the
``
nasti
''
twist
.

What
writer
Stephen
Peter
doe
not
grasp
is
thi
becom
tedious
uninvolv
,
almost
unbear
,
if
those
who
inhibit
thi
twist
world
ar
not
interest
,
do
n't
feel
real
or
we
do
n't
care
for
them
.

Not
even
the
unintention
funni
moment
work
.

The
peopl
in
``
Wild
Thing
''
ar
not
so
much
peopl
as
thei
ar
caricatur
.

Then
there
's
the
erot
content
,
which
nearli
earn
the
film
an
NC-17
,
for
a
sexless
,
badli
edit
,
poorli
lit
menage-a-troi
.

And
in
what
ha
to
be
on
of
the
most
flabbergastingli
inept
scene
that
ha
grace
thi
countri
,
Kevin
Bacon
's
member
make
a
cameo
appear
with
ten
minut
to
go
.

Ironic
,
the
wildest
element
of
``
Wild
Thing
''
ar
not
wild
at
all
.

