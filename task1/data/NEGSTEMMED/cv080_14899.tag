Befor
the
remak
of
Psycho
appear
,
we
've
got
to
suffer
through
thi
remak
of
an
earlier
Hitchcock
film
,
Dial
M
For
Murder
.

As
usual
,
Hollywood
ha
fill
it
with
glitz
and
big
name
star
,
and
it
all
amount
to
a
loud
sound
noth
.

The
film
open
with
Emili
-LRB-
Paltrow
-RRB-
and
David
-LRB-
Viggo
Mortensen
-RRB-
'
have
fun
'
in
an
loft
apart
.

The
problem
is
,
Paltrow
is
marri
to
Stephen
-LRB-
Dougla
-RRB-
,
who
is
not
too
happi
when
he
discov
thi
affair
.

If
I
wa
Paltrow
,
though
,
I
'd
definit
go
with
Mortensen
.

Less
wrinkl
.

Anyhow
,
Stephen
approach
David
with
an
interest
proposit
:
he
'll
pai
him
to
kill
hi
love
wife
.

Mortensen
agre
,
but
the
murder
goe
awri
,
and
the
twist
keep
on
come
as
the
film
progress
.

Unfortun
,
the
film
dose
n't
progress
veri
fast
.

In
fact
,
it
move
like
a
tortois
with
arthriti
-LRB-
read
:
veri
,
veri
s-l-o-w
.
-RRB-

The
plot
move
nowher
fast
,
and
onli
becom
excit
in
veri
short
burst
.

Not
too
good
for
a
'
thriller
.
'

Also
,
the
perform
,
apart
from
Dougla
,
ar
below
par
.

Paltrow
,
show
immens
talent
in
Se7en
-LRB-
1995
-RRB-
and
Slide
Door
-LRB-
1997
-RRB-
is
strang
stilt
,
even
unconvinc
,
in
thi
movi
.

Mortensen
is
a
littl
bit
better
,
come
across
as
a
younger
Dougla
,
but
hi
role
is
n't
meati
enough
to
show
all
hi
talent
.

Which
leav
Dougla
to
waltz
awai
with
the
film
,
which
is
doe
.

Howev
,
I
doubt
Dougla
find
it
veri
hard
to
plai
a
stogi
smoke
,
drink
woman
with
a
creepi
undersid
.

Also
pop
up
in
the
film
is
David
Suchet
,
plai
a
shifti
look
detect
.

Again
,
it
's
not
a
charact
we
're
see
on
the
screen
,
it
's
David
Suchet
.

The
director
,
who
bought
us
The
Fugit
,
pile
on
all
the
flash
techniqu
,
such
as
zoom-in
's
,
quick
cut
,
etc.
.

The
light
is
also
interest
.

Howev
,
look
ca
n't
save
thi
film
from
the
depth
of
mediocr
,
and
the
film
seem
to
work
better
in
a
singl
set
anywai
.

The
screenplai
is
O.K
,
but
there
's
some
hacknei
sub-plot
about
Dougla
be
a
ruthless
player
on
Wall
Street
-LRB-
again
-RRB-
and
the
end
is
surprisingli
stupid
and
clich
.

The
charact
also
make
incred
dumb
move
,
especi
Paltrow
,
and
Stephen
appear
to
lose
all
intellig
in
the
last
reel
.

With
a
combin
of
no
tension
,
drama
,
or
decent
charact
,
A
Perfect
Murder
is
a
failur
all
round
,
and
an
incred
disappoint
.

There
ar
a
few
bright
moment
,
but
thei
ar
far
and
few
between
.

The
last
thing
a
thriller
should
be
is
bore
,
and
although
the
film
just
bare
manag
to
keep
your
interest
thank
to
Michael
Dougla
,
you
would
n't
be
miss
anyth
if
you
decid
not
to
watch
A
Perfect
Murder
.

