Make
your
first
featur
film
ai
n't
easi
.

Assembl
a
decent
,
if
not
,
strong
cast
,
as
writer/director
Robert
Moresco
ha
done
with
One
Ey
King
,
and
you
're
alreadi
ahead
of
the
game
.

But
rehash
old
plot
line
,
tire
dialogu
,
and
standard
clich
,
and
a
well-intent
effort
such
as
thi
on
could
jeopard
your
chanc
at
a
second
featur
film
.

How
mani
more
movi
do
we
need
about
a
rough
neighborhood
full
of
lifelong
friend
hopelessli
turn
to
crime
or
wors
?

The
enorm
catalog
of
such
movi
might
dissuad
a
filmmak
from
make
yet
anoth
,
but
here
we
have
it
.

Again
.

Five
Irish
kid
in
NYC
's
Hell
's
Kitchen
make
an
overemot
pact
over
some
stolen
ring
on
an
anonym
rooftop
.

With
teari
music
.

And
slow
motion
.

In
the
film
's
first
scene
.

The
kid
grow
up
to
be
fairli
worthless
adult
,
unabl
or
unwil
to
make
their
wai
out
of
the
heat
of
the
Kitchen
.

Lead
the
clueless
pack
is
William
Baldwin
as
a
good-heart
gui
who
watch
out
for
hi
buddi
and
is
in
tight
with
local
mob
head
Armand
Assant
.

I
'd
like
to
sai
that
hi
charact
get
involv
over
hi
head
in
some
sort
of
blah
,
blah
,
blah
,
but
all
we
seem
to
get
ar
littl
tast
of
possibl
plot
point
.

He
's
concern
about
buddi
Jason
Gedrick
's
heroin
abus
.

He
stick
up
for
Jim
Breuer
after
he
impregn
Baldwin
's
charact
's
sister
.

He
look
into
who
might
be
push
the
most
moronic-look
counterfeit
cash
ever
made
-LRB-
a
genuin
funni
touch
-RRB-
.

But
none
of
thi
ever
realli
amount
to
anyth
.

It
seem
that
Moresco
's
greater
concern
is
to
provid
that
intang
``
slice
of
life
''
,
that
flavor
of
the
neighborhood
that
everyon
's
been
try
to
evok
sinc
Scorses
's
earli
work
.

So
,
we
get
the
drunk
gui
,
hug
and
sing
togeth
at
the
local
bar
,
to
prove
to
us
that
thei
realli
love
each
other
.

-LRB-
Do
peopl
actual
do
thi
!?
-RRB-

We
get
a
lot
of
tough
street
talk
--
usual
mumbl
for
effect
--
and
a
whole
lot
of
the
F-word
,
whether
it
sound
like
it
fit
or
not
.

We
also
get
a
hand
of
good
actor
in
small
role
that
seem
to
lack
purpos
.

Bruno
Kirbi
,
Chazz
Palminteri
,
you
know
,
gui
you
've
seen
in
movi
just
like
thi
on
befor
.

Assant
is
intellig
cast
as
the
man
that
everyon
fear
,
and
Baldwin
's
perform
is
adequ
,
but
most
of
the
rest
of
the
cast
jump
into
the
tough
gui
persona
so
thoroughli
that
it
's
almost
funni
.

Moresco
,
a
theater
gui
and
sometim
TV
writer
-LRB-
includ
the
seri
Falcon
-RRB-
,
obvious
labor
over
thi
on
as
anyon
might
a
first
child
,
but
the
content
is
probabl
too
person
.

As
a
result
,
the
movi
's
style
is
heavy-hand
,
in
need
of
a
consider
amount
of
tone
down
.

Nearli
everi
time
an
action
by
the
grown
up
gang
recal
someth
thei
did
as
kid
,
Moresco
remind
us
--
boi
doe
he
remind
us
.

With
slow
dissolv
to
the
earlier
scene
,
run
in
slow
motion
,
complet
with
dialogu
from
the
present
,
just
in
case
we
do
n't
comprehend
the
link
to
the
past
.

Moresco
need
to
either
trust
hi
audienc
's
intellig
,
or
have
more
faith
in
hi
own
present
rather
than
beat
us
over
the
head
with
it
.

Hi
next
project
should
have
a
littl
more
person
distanc
,
and
a
lot
more
subtleti
.

If
he
actual
get
that
chanc
.

