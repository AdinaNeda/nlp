Frank
Detorri
's
-LRB-
Bill
Murrai
-RRB-
a
singl
dad
who
live
on
beer
and
junk
food
with
no
appar
understand
of
sanit
or
hygien
,
much
to
the
dismai
of
hi
preteen
daughter
Shane
-LRB-
Elena
Franklin
-RRB-
.

When
he
us
the
'10
second
rule
'
to
retriev
a
hard
boil
egg
from
a
chimp
's
cage
at
the
zoo
and
down
it
,
he
introduc
a
lethal
bacteria
into
hi
system
.

Inside
hi
skin
,
the
Citi
of
Frank
is
in
turmoil
thank
to
the
vote-pand
of
Mayor
Phlegm
-LRB-
voic
of
William
Shatner
-RRB-
,
so
it
's
up
to
on
Frank
PD
white
blood
cell
-LRB-
voic
of
Chri
Rock
-RRB-
to
save
the
dai
in
Peter
and
Bobbi
Farrelli
's
``
Osmosi
Jone
.
''

The
Citi
of
Frank
is
a
brightli
anim
-LRB-
anim
direct
by
Piet
Kroon
and
Tom
Sito
-RRB-
cellular
municip
where
Osmosi
Jone
is
a
typic
rogu
cop
look
for
anoth
chanc
.

He
's
inadvert
team
up
with
Drix
-LRB-
voic
of
David
Hyde
Pierc
,
TV
's
``
Frasier
''
-RRB-
,
a
cold
capsul
withhour
worth
of
painkil
to
dispens
.

Thi
quarrel
duo
ar
about
to
go
on
a
``
Fantast
Voyag
''
in
order
to
hunt
down
Thrax
-LRB-
voic
of
Laurenc
Fishburn
-RRB-
,
the
viru
intent
on
shut
down
Frank
.

While
the
anim
is
certainli
color
to
look
at
,
Osmosi
Jone
'
stori
is
a
hacknei
on
.

The
stori
cri
out
for
puni
pun
,
but
we
onli
get
occasion
sprinkl
of
wit
or
bodili
humor
-LRB-
Drix
graduat
phi
beta
capsul
,
he
depart
on
a
bu
head
for
bladder
-RRB-
.

Neither
the
hero
or
villain
is
particularli
interest
-LRB-
Thrax
look
like
an
anim
``
Predat
''
-RRB-
,
although
Hyde
Pierc
is
a
delight
sidekick
.

Adult
can
desper
keep
their
ey
peel
for
small
amus
the
anim
dot
along
the
landscap
.

Meanwhil
,
back
in
live
action
land
,
Bill
Murrai
is
reduc
to
noth
more
than
a
walk
gross-out
joke
.

There
's
no
particular
enjoy
to
be
found
watch
him
vomit
on
Molli
Shannon
-LRB-
she
plai
Shane
's
teacher
,
Mrs.
Boyd
-RRB-
or
hoist
hi
ingrown
toenail
onto
a
restaur
tabl
.

One
must
wonder
how
the
climat
flatlin
of
a
child
's
father
will
plai
to
the
famili
audienc
as
well
.

Rest
assur
,
the
whole
enchilada
is
wrap
up
with
a
fart
joke
.

While
far
less
offens
than
the
Farrelli
's
last
effort
``
Me
,
Myself
and
Irene
,
''
that
film
at
least
spike
some
comic
high
with
Jim
Carrei
's
hijinx
.

``
Osmosi
Jone
''
will
probabl
be
OK
for
the
kid
,
but
the
Farrelli
plai
for
the
famili
audienc
is
like
watch
Marilyn
Manson
croon
a
Phil
Collin
tune
.

