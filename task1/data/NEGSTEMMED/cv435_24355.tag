A
coupl
of
crimin
-LRB-
Mario
Van
Peebl
and
Loretta
Devin
-RRB-
move
into
a
rich
famili
's
hous
in
hope
of
con
them
out
of
their
jewel
.

Howev
,
someon
els
steal
the
jewel
befor
thei
ar
abl
to
get
to
them
.

Writer
Mario
Van
Peebl
deliv
a
clever
script
with
sever
unexpect
plot
twist
,
but
director
Mario
Van
Peebl
undermin
hi
own
high
point
with
haphazard
camera
work
,
edit
and
pace
.

It
felt
as
though
the
film
should
have
been
wrap
up
at
the
hour
mark
,
but
ala
there
wa
still
35
more
minut
to
go
.

Daniel
Baldwin
-LRB-
I
ca
n't
believ
I
'm
about
to
type
thi
-RRB-
give
the
best
perform
in
the
film
,
outshin
the
other
talent
member
of
the
cast
.

