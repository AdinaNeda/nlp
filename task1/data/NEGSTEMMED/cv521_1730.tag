Saw
an
advanc
screen
of
the
movi
SNIPER
last
night
,
and
I
have
to
sai
I
wa
n't
too
impress
.

Thi
film
is
about
an
expert
marin
sniper
,
plai
by
Tom
Bereng
,
who
is
team
with
a
hot-shot-young-no-experience-never-killed-a-man
new
partner
to
take
out
some
drug-kingpin
and
militari
strong
men
in
Panama
.

Sound
clich
?

That
's
onli
the
begin
.

Billi
Zane
-LRB-
MEMPHIS
BELLE
-RRB-
plai
the
rooki
,
and
never
seem
to
get
a
handl
on
hi
charact
.

He
wa
so
contrari
and
pig-head
in
the
begin
,
you
just
want
to
smack
him
.

Then
he
goe
``
crazi
''
from
the
pressur
,
but
seem
to
immedi
snap
out
of
it
.

I
'm
not
sure
if
the
blame
should
lie
with
the
direct
,
edit
,
scriptwrit
,
or
act
...
but
it
did
n't
all
come
togeth
.

I
think
Bereng
is
a
pretti
good
actor
,
and
he
look
great
in
the
part
,
cover
in
camo
,
face
paint
,
stalk
through
the
jungl
with
hi
high-pow
rifl
...
but
onc
again
,
hi
charact
wa
given
some
pretti
bad
dialogu
,
and
did
n't
react
too
logic
to
mani
of
the
situat
.

There
wa
veri
littl
logic
develop
in
these
charact
.

My
biggest
problem
with
thi
film
wa
it
's
tendenc
to
put
these
two
sniper
in
as
mani
inches-from-death
situat
as
possibl
.

Thei
began
to
resembl
G.
I.
Joe
,
the
Greatest
American
Hero
.

And
of
cours
their
almost
supernatur
accuraci
with
their
gun
wa
call
upon
to
get
them
out
of
far
too
mani
close
call
.

Now
a
lot
of
movi
ar
stamp
out
of
thi
mold
,
in
fact
that
's
what
made
DIE
HARD
so
much
fun
:
the
super-hero
aveng
type
.

So
some
will
enjoi
the
action
hero
heroic
,
but
I
did
n't
think
it
fit
with
the
nice
tension
that
wa
built
up
in
the
earlier
scene
,
and
the
``
real
life
''
feel
of
covert
oper
in
Panama
.

A
word
must
be
said
on
the
camera
work
,
much
of
it
wa
veri
nice
.

The
jungl
of
Panama
-LRB-
or
their
stand-in
in
thi
case
-RRB-
form
a
veri
picturesqu
background
...
and
the
drama
of
speed
bullet
wa
captur
us
some
nice
trick
photographi
.

Mani
have
seen
the
slow
down
``
bullet-cam
''
follow
the
projectil
to
it
target
.

Thi
wa
us
well
in
the
begin
to
show
the
feverish
nightmar
Bereng
get
when
rememb
the
moment
of
the
kill
.

It
wa
dramat
when
us
in
flashback
...
but
it
seem
the
director
like
the
techniqu
so
much
he
start
insert
these
shot
-LRB-
pardon
the
pun
-RRB-
into
the
real
time
action
...
then
it
just
seem
silli
.

All
in
all
,
not
a
real
dog
:
there
is
some
nice
action
,
good
atmospher
,
and
pretti
photographi
.

But
the
plot
is
pretti
lame
,
the
act
did
n't
form
a
cohes
whole
,
and
far
too
much
thisclosefromdeath
heroic
.

