FIRESTORM
My
first
press
screen
of
1998
and
alreadi
I
've
gotten
a
prime
candid
for
my
worst
ten
of
the
year
list
.

What
an
auspici
begin
!

Welcom
to
the
dog
dai
of
winter
when
the
onli
film
open
of
merit
ar
those
Oscar
contend
that
the
studio
open
in
late
Decemb
in
New
York
and
L.A.
and
which
ar
just
now
begin
to
appear
elsewher
.

FIRESTORM
,
the
directori
debut
of
DANCES
WITH
WOLVES
's
Academi
Award
win
cinematograph
Dean
Semler
,
is
the
first
of
the
new
year
's
crop
of
movi
.

As
our
stori
open
,
the
movi
pretenti
inform
us
that
of
the
ten
of
thousand
of
firefight
onli
400
ar
``
smokejump
.
''

We
then
cut
to
a
plane
load
of
smoke
jump
cowboi
and
on
cowgirl
,
where
on
of
the
gung-ho
gui
is
take
a
romanc
quiz
from
``
Cosmopolitan
.
''

Have
the
time
of
their
live
,
thei
then
jump
into
the
middl
of
a
burn
forest
.

When
,
even
in
the
begin
,
the
director
ca
n't
get
the
small
part
right
,
you
can
sens
the
movi
is
in
troubl
.

With
the
noisi
fire
roar
all
about
them
and
with
the
trap
peopl
huddl
near
their
gasoline-fil
car
,
smokejump
Monica
-LRB-
Christiann
Hirt
-RRB-
tell
them
to
get
awai
from
their
soon-to-explod
vehicl
.

Not
bother
to
shout
nor
even
get
close
to
them
,
she
announc
her
warn
without
rais
her
voic
much
or
approach
the
peopl
.

Miracul
,
thei
manag
to
hear
her
and
move
awai
.

In
a
movi
that
special
in
cheap
shot
,
the
camera
locat
the
proverbi
young
girl
trap
in
a
nearbi
burn
build
.

As
it
doe
throughout
,
overli
dramat
cinematograph
Stephen
F.
Windon
from
THE
POSTMAN
us
extrem
fast
zoom
right
down
to
the
endang
girl
's
face
.

Our
show
's
two
hero
,
the
crew
's
chief
,
Wynt
Perkin
,
plai
lacon
by
Scott
Glenn
,
and
hi
second-in-command
,
Jess
Grave
,
plai
by
Howi
Long
in
a
weak
attempt
to
be
the
next
Steven
Seagal
,
enter
the
burn
hous
look
for
the
littl
girl
.

In
a
panic
thei
have
difficulti
in
locat
her
befor
thei
ar
engulf
in
flame
.

The
manipul
script
ha
her
hidden
in
her
own
dollhous
.

Thi
mawkish
show
cut
back
to
Monica
,
who
ha
a
life-or-death
decis
to
make
.

The
chopper
with
the
fire-retard
chemic
ha
onli
enough
to
save
on
group
.

Will
it
be
the
larg
group
near
the
car
or
the
helpless
littl
girl
and
Monica
's
two
firefight
buddi
?

She
ha
onli
second
to
decid
who
will
be
save
.

Ye
,
she
goe
for
the
major
,
but
,
miracl
of
miracl
,
the
other
three
come
out
aliv
anywai
.

Not
content
with
a
tradit
firefight
stori
,
Chri
Soth
's
screenplai
attempt
to
jazz
it
up
by
have
William
Forsyth
from
PALOOKAVILLE
plai
a
viciou
killer
name
Randi
Earl
Shay
who
set
a
forest
fire
so
that
he
can
join
the
crew
to
put
it
out
and
then
escap
.

-LRB-
``
Hood
in
the
wood
,
''
is
what
the
``
ground-pound
''
yell
out
when
the
convict
ar
buse
in
to
help
them
fight
the
fire
.
-RRB-

Along
the
wai
,
Shay
pick
up
an
ornithologist
hostag
plai
by
Suzi
Ami
,
who
turn
out
to
have
been
train
in
warrior
wai
by
her
father
,
who
wa
a
Marin
drill
instructor
.

Most
of
the
highli
predict
movi
is
a
long
chase
in
which
poor
Howi
Long
is
given
on
ridicul
stunt
after
anoth
to
look
silli
perform
.

He
fling
a
chain
saw
backward
over
hi
head
while
ride
a
speed
motorcycl
so
that
the
saw
can
hit
the
windshield
of
the
pursu
truck
.

Arguabli
the
low
point
is
when
he
escap
from
a
lock
burn
build
by
ride
a
motorcycl
conveni
park
insid
.

Using
a
ramp
,
he
shoot
straight
out
of
the
top
of
the
build
's
attic
,
and
when
he
hit
the
ground
,
he
just
ride
off
in
a
cloud
of
dust
.

When
the
film
is
n't
us
some
stock
footag
of
actual
forest
fire
,
the
simul
on
look
hokei
.

Editor
Jack
Hofstra
cheapen
the
action
even
more
by
hi
us
of
burn
flame
in
scene
transit
.

The
end
,
with
it
sick
twist
,
manag
to
be
even
wors
than
the
rest
of
the
movi
.

Perhap
the
best
that
can
be
said
for
the
pictur
is
the
faint
prais
I
heard
afterward
in
the
lobbi
,
``
It
's
not
as
bad
as
some
of
the
televis
sitcom
.
''

FIRESTORM
run
mercifulli
just
1:29
.

It
is
rate
R
for
violenc
and
languag
and
would
be
accept
for
teenag
.

