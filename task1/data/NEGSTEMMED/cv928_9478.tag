Martial
art
master
Steven
Seagal
-LRB-
not
to
mention
director
!
-RRB-

ha
built
a
career
out
of
plai
an
allegedli
fictiti
martial
art
superman
who
never
get
hurt
in
fight
,
talk
in
a
hush
tone
,
and
squint
at
ani
sign
of
danger
.

He
's
also
the
most
consist
individu
in
Hollywood
todai
,
sinc
all
hi
movi
suck
.

Thei
basic
repres
hi
egotisit
tendenc
about
hi
art
-LRB-
that
is
,
martial
art
-RRB-
.

I
'm
sure
the
gui
's
good
,
and
he
seem
like
a
nice
gui
on
talk
show
,
although
a
tad
haughti
,
but
these
movi
he
make
ar
all
the
same
:
a
gui
who
is
basic
indestruct
,
is
mayb
wound
supposedli
mortal
,
then
come
back
with
a
vengeanc
and
goe
buddha
on
all
the
baddi
ass
-LRB-
although
I
kinda
like
``
Under
Sieg
''
-RRB-
.

Of
cours
,
thi
on
,
as
a
chang
,
ha
a
``
messag
''
that
is
drill
into
our
mind
...
of
cours
,
after
he
blow
up
a
lot
of
stuff
and
kill
a
bunch
of
peopl
.

So
why
do
I
watch
hi
crap
?

I
usual
do
n't
.

I
will
never
,
and
you
can
hold
me
to
thi
,
I
will
NEVER
pai
to
see
thi
man
's
movi
,
unless
,
and
onli
unless
,
he
's
in
a
support
role
-LRB-
i.e.
``
Execut
Decis
''
-RRB-
and
I
'd
definit
pai
if
he
di
-LRB-
i.e.
``
Execut
Decis
''
-RRB-
.

But
thi
on
ha
a
special
place
in
my
heart
.

Thi
doe
n't
mean
it
's
good
or
that
I
even
like
it
.

Thi
wa
the
last
movi
I
watch
with
my
deceas
uncl
,
and
we
had
on
hell
of
a
time
rip
it
apart
a
la
``
Mysteri
Scienc
Theatr
3000
,
''
and
thi
wa
a
coupl
year
befor
I
had
heard
of
``
Mysteri
Scienc
Theatr
3000
.
''

In
thi
on
,
Seagal
plai
a
worker
for
a
mine
factori
set
in
Alaska
and
run
by
the
greased-up
typic
shallow
villain
,
thi
time
plai
by
an
Oscar-winn
to
give
the
movi
some
more
clout-Michael
Cain
.

It
seem
that
Cain
want
to
do
someth
with
hi
oil
factori
that
includ
him
dump
oil
all
over
Inuit
land
.

Around
the
20-30
minut
point
,
Seagal
speak
up
to
him
in
what
seem
to
be
the
typic
speech
to
all
the
vain
entrepeneur
-LRB-
what
with
hi
new
``
Fire
Down
Below
,
''
anoth
``
messag
film
''
-RRB-
,
and
Cain
ha
him
bump
off
...
or
doe
he
?

Seagal
is
rescu
by
some
Inuit
,
and
fall
in
love
with
on
of
them
,
plai
by
Joan
Chen
,
who
CAN
act
,
hypothet
,
but
,
for
some
reason
,
not
here
.

One
of
Cain
's
clich
henchmen
-LRB-
plai
here
with
a
lot
of
overact
by
John
C.
McGinlei
-RRB-
shoot
the
cheif
of
the
Inuit
clan
,
and
Chen
and
Seagal
go
on
a
voyag
to
take
down
the
oil
factori
...
liter
,
of
cours
.

At
on
point
,
Seagal
give
a
wonderfulli
hyster
speech
about
how
he
doe
n't
have
ani
option
but
blow
stuff
up
.

He
even
goe
as
far
as
to
sai
,
``
I
do
n't
want
to
kill
someon
,
''
and
in
the
same
breath
,
he
ask
some
gui
where
the
arsen
is
.

I
have
no
problem
with
violenc
.

I
'm
a
huge
John
Woo
fan
,
but
he
paint
hi
film
with
suspens
,
skill
,
style
,
depth
,
character
,
and
just
plain
cool
violenc
.

In
the
film
of
Seagal
,
the
suspens
mainli
consist
of
the
baddi
attack
him
stupidli
,
and
him
either
wound
or
kill
them
.

At
some
point
,
thei
us
the
clich
of
the
talk
villain
,
where
the
villain
ha
the
advantag
,
can
shoot
Seagal
,
but
begin
talk
by
either
tell
him
hi
big
secret
plan
,
or
sai
a
corni
line
,
to
which
Seagal
sai
someth
hokei
back
,
and
ha
had
enough
time
to
devis
of
a
wai
to
do
awai
with
them
,
and
doe
.

Thi
would
be
okai
if
there
were
ani
suspens
or
if
it
did
n't
take
itself
serious
at
all
,
like
in
the
case
of
thi
summer
's
``
Con
Air
.
''

But
Seagal
is
seriou
about
hi
skill
,
and
of
cours
,
hi
messag
.

I
would
n't
mind
if
thi
wa
a
messag
film
in
the
wai
that
thei
present
it
to
you
with
evid
.

But
Seagal
ha
no
idea
how
to
present
a
film
where
the
messag
is
subtl
,
not
pound
into
the
viewer
's
mind
.

The
villain
is
total
shallow
and
cartoonish
,
thu
we
ca
n't
take
him
and
hi
motiv
serious
,
and
while
Seagal
talk
about
be
kind
to
the
environ
,
he
also
goe
ahead
and
blow
up
a
squar
mile
of
rig
,
and
kill
some
worker
who
were
just
do
hi
job
.

Then
at
the
end
,
he
spend
a
good
10
minut
give
a
speech
,
just
in
case
you
did
n't
get
the
messag
from
the
trailer
.

What
Seagal
doe
n't
realiz
is
that
no
on
take
hi
film
serious
-LRB-
although
mayb
a
coupl
do
-RRB-
and
ani
messag
he
ha
is
no
onli
redund
,
but
doe
n't
comfort
fit
in
hi
film
,
which
is
fill
to
the
brim
with
hokei
violenc
,
crap
suspens
,
stupid
melodrama
,
and
charact
who
have
about
as
much
emot
depth
as
a
petri
dish
.

As
far
as
Seagal
and
hi
act
,
he
's
rather
bore
.

He
squint
,
he
kill
.

Period
.

Noth
els
.

Oh
,
yeah
,
there
's
corni
one-lin
-LRB-
``
I
'm
gon
na
reach
out
and
touch
someon
!
''
-RRB-
.

Of
cours
,
he
's
the
star
,
and
we
're
suppos
to
root
for
him
and
all
,
so
he
make
all
the
villain
unbeliev
stupid
and
a
bunch
of
jerk
.

Michael
Cain
,
who
's
a
great
actor
,
is
just
suppos
to
yell
and
look
cold
.

He
doe
it
well
,
I
guess
,
but
thi
is
no
``
Alfie
.
''

Of
cour
,
no
on
wa
expect
that
calib
of
perform
from
him
.

Hi
big
henchman
,
John
C.
McGinlei
is
kinda
bore
as
well
,
but
is
not
horribl
.

And
we
even
get
a
small
perform
from
that
god
of
drill
sergeant
on
celluloid
,
R.
Lee
Ermei
-LRB-
from
``
Full
Metal
Jacket
''
-RRB-
as
a
hire
assasin
squad
leader
who
get
to
sai
the
obligatori
speech
about
how
danger
Seagal
is
,
just
for
the
movi
trailer
and
for
Seagal
's
ego
.

And
also
,
look
for
Billi
Bob
Thornton
as
on
of
Ermei
's
assasin
.

Anywai
,
to
conclud
thi
all
,
to
judg
on
of
Seagal
's
movi
is
to
judg
all
of
them
-LRB-
except
for
``
Under
Sieg
''
and
``
Execut
Decis
,
''
though
the
latter
is
not
realli
a
``
Seagal
movi
''
-RRB-
.

Thei
all
have
thi
same
formula
,
thei
all
have
the
same
action
,
same
villain
,
same
plot
,
but
thi
on
ha
that
messag
,
which
make
it
more
excrucit
to
watch
.

I
mean
,
if
you
do
rent
it
,
and
I
do
n't
reccomend
you
do
,
make
sure
you
just
skip
the
last
10
minut
.

But
I
have
to
put
it
to
Seagal
for
creat
a
film
so
bad
,
that
the
last
film
I
view
with
my
uncl
wa
a
pleasur
on
.

