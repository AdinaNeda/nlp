THE
ART
OF
WOO
attempt
to
be
on
of
those
film
like
BREAKFAST
AT
TIFFANY
'S
in
which
the
audienc
is
root
for
some
sweet
,
vulner
,
irresist
woman
to
work
out
her
problem
and
to
find
happi
.

The
problem
is
that
Helen
Lee
who
write
and
direct
seem
to
have
written
Alessa
Woo
-LRB-
plai
by
Sook-Yin
Lee
-RRB-
as
neither
sweet
nor
vulner
and
she
is
quit
resist
.

Alessa
is
a
young
woman
who
happen
to
be
a
brilliant
art
dealer
in
the
Toronto
art
scene
.

Thi
is
some
sort
of
altern
world
art
scene
where
peopl
pai
ten
of
thousand
of
dollar
for
paint
by
talent
beginn
and
dealer
in
these
paint
fly
back
and
forth
to
place
like
Switzerland
.

One
of
the
most
knowledg
of
the
art
dealer
is
Alessa
.

She
also
happen
to
be
the
center
of
adul
of
her
friend
and
everi
parti
ha
suitor
camp
outsid
her
window
.

Next
door
to
Alessa
move
struggl
geniu
artist
and
Nativ
American
Ben
Crowchild
-LRB-
Adam
Beach
-RRB-
.

He
see
that
behind
the
facad
that
there
is
realli
a
sad
littl
girl
within
Alessa
who
realli
will
not
be
happi
with
the
rich
art
collector
she
is
date
.

Ben
get
emotion
involv
with
Alessa
.

-LRB-
As
Alessa
so
delic
put
it
,
``
We
were
bosom
buddi
,
now
we
're
fuck
buddi
.
''
-RRB-

But
Alessa
will
have
to
decid
whether
she
want
love
with
Ben
or
wealth
with
her
rich
suitor
.

The
real
problem
with
the
film
seem
to
be
Helen
Lee
's
inabl
to
decid
what
she
want
to
be
sai
.

She
undercut
nearli
everyth
she
want
us
to
believ
about
Alessa
.

Alessa
is
look
for
financi
secur
but
she
make
decis
about
larg
sum
of
monei
for
her
client
.

Thi
appear
to
be
a
high
profil
and
well-paid
job
.

We
ar
suppos
to
care
about
Alessa
's
feel
,
but
she
coldli
refus
to
visit
her
own
ail
father
.

Alessa
can
not
be
portrai
as
sweet
and
vulner
if
at
an
art
auction
she
turn
into
OUR
MAN
FLINT
.

Thi
is
a
charmless
romant
comedi
that
bet
everyth
it
ha
on
the
appeal
of
it
main
charact
and
come
up
double-zero
.

I
rate
it
aon
thetoscal
and
a-1
on
the-4
to
+4
scale
.

