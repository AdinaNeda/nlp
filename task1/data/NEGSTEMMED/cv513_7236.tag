CAPSULE
:
Silli
and
inan
adapt
of
Gibson
's
short
stori
,
which
is
nowher
in
sight
.

Gibson
's
script
onli
add
insult
to
injuri
.

JOHNNY
MNEMONIC
is
an
awesom
bad
movi
.

I
sai
``
awesom
''
becaus
it
's
on
thing
to
fail
,
but
anoth
thing
entir
to
fail
so
complet
that
even
the
chanc
for
camp
valu
ar
sabotag
.

Keanur
Reev
-LRB-
who
is
terribl
-RRB-
star
as
Johnni
,
an
``
inform
courier
''
who
can
carri
dozen
of
gigabyt
of
data
in
hi
head
.

He
is
given
``
on
last
job
''
-LRB-
whenev
you
ar
in
a
movi
and
you
hear
those
word
,
RUN
-RRB-
,
which
involv
him
shove
so
much
data
into
hi
cranium
that
it
could
be
lethal
.

One
of
the
neater
touch
that
the
movi
bring
in
is
that
the
onli
wai
he
could
make
such
an
arrang
work
wa
by
ditch
all
of
hi
childhood
memori
,
but
it
's
onli
follow
up
on
in
a
token
fashion
.

For
hi
troubl
,
Johnni
get
chase
by
the
Yakuza
,
who
seem
to
be
the
new
bad
gui
in
all
the
high-tech
thriller
.

What
's
funni
is
that
if
you
watch
gangster
movi
made
in
Japan
,
there
ar
whole
gob
of
detail
about
genuin
Yakuza
behavior
and
ethic
,
but
of
cours
there
's
no
room
in
thi
movi
for
ani
of
that
.

The
Yakuza
ar
simpli
us
to
point
gun
,
wave
sword
,
flaunt
tattoo
,
and
grimac
menacingli
.

-LRB-
I
could
go
on
about
how
gangster
and
crimin
of
mani
other
ethnic
have
gotten
thought
examin
in
the
movi
--
BOUND
BY
HONOR
,
SUGAR
HILL
and
AMERICAN
ME
come
to
mind
--
when
Asian
remain
perpetu
stereotyp
.

But
that
's
anoth
essai
.
-RRB-

Anywai
,
Johnni
run
and
eventu
wind
up
in
Newark
.

Why
Newark
?

Mayb
becaus
it
wa
cheaper
to
fake
a
futur
Newark
than
a
futur
New
York
,
that
's
why
.

There
,
he
meet
an
assort
of
odd
charact
-LRB-
Ice-T
,
Dolph
Lundgren
and
Henri
Rollin
plai
a
whole
galleri
of
weirdo
-RRB-
.

The
script
deal
with
them
with
all
the
depth
of
piec
of
furnitur
.

It
turn
out
-LRB-
what
els
?
-RRB-

that
the
data
in
Johnni
's
head
could
save
a
lot
of
peopl
,
but
of
cours
Johnni
onli
want
it
out
of
hi
head
so
it
doe
n't
kill
him
.

Handl
right
,
thi
could
have
been
absorb
,
but
the
script
manag
to
mangl
ani
chanc
of
real
sympathi
for
Johnni
at
everi
opportun
.

The
detail
about
the
look
and
feel
of
the
futur
ar
all
phone
in
from
other
,
better
movi
--
BLADE
RUNNER
and
BRAZIL
come
to
mind
.

Everyth
look
run-down
and
scummi
,
everyon
dress
like
thei
're
punk
rocker
,
and
videophon
ar
commonplac
.

Snore
.

The
onli
realli
interest
flourish
is
an
extend
depict
of
the
wai
the
Internet
might
work
in
the
futur
-LRB-
complet
with
VR
goggl
and
feedback
glove
-RRB-
,
but
I
kept
think
that
it
wa
more
like
what
some
rel
un-techn
fellow
would
think
it
would
look
and
behav
like
.

A
hacker
of
Johnni
's
calib
would
be
blast
awai
with
on
command-lin
function
after
anoth
,
instead
of
wast
all
thi
time
twiddl
with
hologram
,
but
of
cours
that
's
not
cinemat
.

Whatev
.

What
went
wrong
with
thi
movi
?

Gibson
wrote
hi
own
screenplai
,
which
I
guess
is
part
of
the
problem
:
what
work
as
a
short
stori
doe
n't
work
in
a
movi
.

Hi
ear
for
dialogu
is
terribl
and
the
plot
doe
n't
advanc
,
it
convuls
.

From
the
script
on
out
,
it
wa
probabl
all
downhil
.

Rent
the
movi
to
make
fun
of
it
is
sort
of
pointless
;
there
's
no
fun
in
kick
a
wound
dog
,
is
there
?

