The
Blue
Brother
wa
a
wonder
film
,
a
hilari
comedi
pack
with
good
music
.

It
cri
out
for
a
sequel
,
but
John
Belushi
's
untim
death
seem
to
elimin
the
idea
.

Howev
,
eighteen
year
have
pass
,
and
the
long
dormant
sequel
ha
final
emerg
.

Unfortun
,
it
's
a
sequel
not
worthi
of
the
origin
.

The
film
start
exactli
eighteen
year
after
the
first
on
end
.

Elwood
Blue
-LRB-
Dan
Aykroyd
-RRB-
is
just
get
out
of
jail
,
hi
brother
Jake
have
recent
di
.

As
in
the
first
film
,
he
first
visit
Mother
Mari
Stigmata
-LRB-
Kathleen
Freeman
-RRB-
and
then
set
about
get
the
band
back
togeth
.

John
Belushi
's
absenc
leav
a
terribl
hole
in
the
film
,
and
although
three
new
charact
ar
creat
to
fill
the
void
,
it
is
still
veri
notic
.

First
,
there
's
Cabel
-LRB-
Joe
Morton
-RRB-
the
illegitim
son
of
Elwood
's
stepfath
-LRB-
plai
by
Cab
Callowai
in
the
first
movi
-RRB-
.

Cabel
is
reluct
to
join
hi
destini
,
and
spend
most
of
the
movi
as
an
Illinoi
sheriff
,
chase
the
Blue
Brother
Band
.

Next
,
there
's
Mighti
Mack
-LRB-
John
Goodman
-RRB-
,
a
bartend
who
becom
the
new
lead
singer
of
the
band
.

Final
,
there
's
Buster
-LRB-
J.
Evan
Bonif
-RRB-
,
a
ten
year
old
orphan
who
tag
along
with
Elwood
and
eventu
join
the
band
.

The
plot
of
the
film
is
hardli
origin
...
it
seem
to
be
almost
a
clone
of
the
origin
.

Elwood
ha
to
go
to
reluctantli
retriev
each
member
of
the
band
,
thei
then
travel
,
while
be
pursu
by
the
polic
,
and
perform
at
sever
odd
stop
until
thei
final
reach
the
big
concert
final
.

The
first
film
had
Neo-Nazi
as
the
random
element
,
thi
time
around
,
the
Russian
Mafia
and
a
militia
group
fill
their
role
.

In
fact
,
the
duplic
of
the
plot
is
so
ridicul
complet
that
certain
scene
ar
practic
ident
to
the
origin
.

Rememb
the
classic
perform
at
Countri
Bob
's
-LRB-
where
thei
like
both
type
of
music
:
Countri
AND
Western
-RRB-
from
the
first
movi
?

Well
,
thi
movi
ha
a
perform
at
a
countri
fair
,
where
the
band
is
expect
to
plai
bluegrass
music
.

There
's
the
massiv
polic
car
pileup
,
although
thi
time
the
gag
fall
complet
flat
.

There
's
even
an
exact
replica
of
the
convers
scene
in
the
church
of
Reverend
Cleophu
-LRB-
Jame
Brown
-RRB-
.

There
ar
plenti
of
recur
charact
too
.

In
addit
to
Mother
Stigmata
and
Reverend
Cleophu
,
Aretha
Franklin
repris
her
role
as
Mrs.
Murphi
.

Frank
Oz
,
a
prison
guard
in
the
first
film
,
make
an
appear
here
as
the
prison
warden
.

As
the
star
,
the
new
Blue
Brother
do
n't
live
up
to
their
legaci
.

Aykroyd
is
more
loquaci
,
yet
much
flatter
as
Elwood
.

John
Goodman
bare
ha
a
charact
as
Mighti
Mack
.

Joe
Morton
ha
the
deepest
charact
,
but
not
a
terribl
interest
on
,
as
Cab
.

And
what
's
the
deal
with
the
orphan
?

It
plai
like
a
desper
gimmick
that
doe
n't
mesh
at
all
with
the
rest
of
the
film
.

At
least
Bonif
is
n't
as
precoci
as
he
could
have
been
in
the
role
.

But
the
true
star
,
and
the
onli
save
grace
,
of
the
film
is
the
music
.

And
the
film
is
pack
with
it
-LRB-
even
dure
and
after
the
end
credit
-RRB-
.

Although
there
ar
no
brilliant
merger
of
comedi
and
song
as
in
the
origin
's
Rawhide/Stand
By
Your
Man
medlei
,
the
music
is
veri
much
enjoy
.

To
top
it
off
,
the
film
is
pack
to
the
gill
with
cameo
musician
appear
.

B.B.
King
,
Blue
Travel
,
Eric
Clapton
,
Travi
Tritt
,
Wilson
Pickett
,
Erykah
Badu
,
Bo
Diddlei
and
Steve
Winwood
ar
just
a
sampl
of
the
multitud
of
star
that
make
an
appear
here
and
there
.

Unfortun
,
the
music
paus
here
and
there
to
allow
in
the
familiar
plot
.

If
simpli
copi
the
origin
Blue
Brother
wa
n't
bad
enough
,
writer
Aykroyd
and
John
Landi
dumb
it
down
,
remov
ani
memor
charact
,
and
replac
them
with
flashi
,
but
unbeliev
,
magic
gimmick
.

It
's
a
shame
.

Bui
the
soundtrack
and
avoid
the
film
.

Better
yet
,
rewatch
the
origin
...
you
'll
have
a
much
better
time
.

