Star
Terrenc
Stamp
,
Peter
Fonda
.

Direct
by
Steven
Soderbergh
.

Rate
R.
Out
of
Sight
director
Steven
Sorderbergh
baffl
the
hell
out
of
us
all
in
The
Limei
,
a
cold
,
uninvolv
,
confus
new
thriller
.

Though
the
plot
descript
mai
at
first
seem
like
it
came
from
the
pen
of
Elmore
Leonard
-LRB-
author
of
Out
of
Sight
,
as
well
as
Jacki
Brown
,
Get
Shorti
and
Pulp
Fiction
-RRB-
,
after
you
watch
it
,
you
realiz
that
it
's
not
nearli
good
enough
.

In
an
aggress
non-linear
fashion
,
The
Limei
-LRB-
limei
,
noun
:
an
English
gentleman
-RRB-
tell
the
stori
of
Wilson
-LRB-
Terrenc
Stamp
-RRB-
,
a
British
ex-con
just
releas
from
ayear
stint
in
prison
for
arm
robberi
.

He
ha
come
to
the
US
to
seek
vengeanc
for
the
death
of
hi
daughter
Jenni
.

He
doe
n't
know
much
about
the
circumst
of
her
demis
,
all
he
ha
is
a
name
:
Terri
Valentin
.

Valentin
wa
Jenni
's
former
boyfriend
,
a
wealthi
and
corrupt
record
execut
.

He
's
plai
by
Peter
Fonda
,
in
hi
first
major
role
sinc
the
terrif
Ulee
's
Gold
in
1997
.

Seek
Valentin
's
reclus
place
of
resid
turn
out
to
be
no
easi
task
for
Wilson
.

He
final
find
the
impress
abod
high
in
the
mountain
and
sneak
in
just
as
Valentin
is
have
a
big
parti
.

He
wind
up
break
hi
cover
eventu
,
set
off
Valentin
's
head
of
secur
and
Valentin
himself
,
who
decid
to
run
for
it
.

What
a
mess
.

I
have
no
problem
when
film
refus
to
be
constrict
by
the
linear
of
time
--
Pulp
Fiction
,
which
twist
time
everi
which
wai
,
wa
a
masterpiec
--
but
I
do
take
except
to
movi
that
decid
to
plai
around
with
it
for
no
reason
other
than
to
confus
the
viewer
.

The
Limei
doe
exactli
that
.

The
plot
is
permeat
with
flashback
,
flash-forward
and
what
can
onli
be
describ
as
random
time-travel
,
without
ani
evid
purpos
.

There
is
no
method
to
thi
movi
's
mad
.

It
us
a
fanci
wai
to
tell
a
stori
that
would
be
better
off
told
more
convention
and
more
comprehend
.

The
plot
is
n't
particularli
interest
in
the
first
place
:
tradit
,
mildli
hacknei
and
not
veri
involv
.

Thi
is
a
sort
of
brood
film
--
our
protagonist
doe
n't
speak
much
and
the
action
sequenc
ar
done
with
an
annoyingli
perfunctori
attitud
.

I
felt
like
the
director
wa
n't
veri
interest
in
the
proceed
himself
,
almost
like
he
made
thi
film
for
a
paycheck
.

Ditto
for
the
edit
,
which
seem
to
be
deliber
sloppi
and
unpleas
.

Sixti
icon
Terrenc
Stamp
manag
to
at
least
be
menac
as
the
ag
crimin
.

He
's
not
much
in
the
wai
of
statur
but
he
ha
a
surprisingli
impos
physic
presenc
that
work
to
hi
advantag
here
.

Peter
Fonda
is
an
unbeliev
underr
actor
:
he
's
shy
,
quiet
but
alwai
effect
.

He
's
adept
at
convei
emot
through
speech
rather
than
express
:
hi
feel
do
n't
alwai
show
on
hi
face
by
you
can
alwai
tell
what
thei
ar
.

Thi
is
basic
a
convent
thriller
told
in
a
pretenti
bizarr
fashion
.

Why
Soderbergh
could
n't
just
parrot
down
and
tell
a
stori
,
I
do
n't
know
,
but
what
he
doe
do
certainli
doe
n't
work
.

The
result
is
a
wild
cornucopia
of
imag
that
amount
to
precis
nil
--
even
the
action
scene
do
n't
work
.

1999
mai
have
signifi
the
death
of
the
tradit
act
one-act
two-act
three
storylin
,
but
obvious
some
movi
have
not
yet
transcend
it
.

Shall
we
go
back
to
basic
?

