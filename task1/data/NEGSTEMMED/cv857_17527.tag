Clair
Dane
,
Giovanni
Ribisi
,
and
Omar
Epp
make
a
likabl
trio
of
protagonist
,
but
thei
're
just
about
the
onli
palat
element
of
The
Mod
Squad
,
a
lame-brain
big-screen
version
of
the
70
TV
show
.

The
stori
ha
all
the
origin
of
a
block
of
wood
-LRB-
well
,
it
would
if
you
could
deciph
it
-RRB-
,
the
charact
ar
all
blank
slate
,
and
Scott
Silver
's
perfunctori
action
sequenc
ar
as
clich
as
thei
come
.

By
sheer
forc
of
talent
,
the
three
actor
wring
margin
enjoy
from
the
proceed
whenev
thei
're
on
screen
,
but
The
Mod
Squad
is
just
a
second-r
action
pictur
with
a
first-rat
cast
.

