Mortal
Kombat
:
Annihil
A
rather
lengthi
movi
review
by
Josh
Hammond
Capsul
:
Not
as
bad
a
sequel
as
Crowor
Batman
&
Robin
,
but
still
horribl
putrid
,
cheesi
and
ill-conceiv
.

Thi
on
belong
in
Saturdai
morn
cartoon
...
Extend
Review
:
You
know
,
about
halfwai
through
thi
movi
,
I
realiz
that
if
you
hack
out
everyth
except
the
fight
scene
,
you
'd
have
a
damn
fineminut
or
so
of
flashi
,
Hong
Kong
style
martial
art
.

Beautifulli
choreograph
by
Robin
Shou
,
who
also
plai
Liu
Kang
,
the
fight
scene
ar
both
mind-blow
and
grace
,
acrobat
enough
to
make
Olympic
gymnast
weep
.

Thi
is
what
made
the
origin
so
fun
,
but
unfortun
for
the
sequel
,
we
ar
without
the
frenet
directori
style
of
Paul
Anderson
.

Instead
,
thei
got
John
R.
Leonetti
,
the
cinematograph
for
the
origin
Mortal
Kombat
.

Not
a
good
choic
.

While
the
fight
scene
ar
brutal
and
eye-pop
,
the
rest
of
the
movi
look
like
standard
made
for
TV
fare
.

The
act
is
sub-par
,
which
I
could
endur
if
it
were
not
for
A.
-RRB-
the
director
's
style
,
B.
-RRB-
the
special
effect
,
&
C.
-RRB-
the
treatment
of
the
stori
and
charact
.

A.
-RRB-
Mr.
Leonetti
should
go
back
to
light
,
in
my
opinion
.

I
could
sai
dozen
of
bad
thing
about
him
:
he
wai
overus
slow-mot
,
he
ha
no
ey
for
action
,
he
ca
n't
get
even
mediocr
perform
out
of
actor
,
and
so
on
.

But
by
far
hi
worst
move
wa
the
wai
he
treat
jump
.

Apparantli
,
everybodi
can
now
fly
.

Hell
,
I
'm
all
for
the
occaision
graviti
defi
flip
kick
and
whatnot
,
but
not
when
it
happen
everi
few
minut
,
and
is
done
so
poorli
.

Better
jump
and
acrobat
ha
been
seen
in
Xena
:
Warrior
Princess
.

In
short
,
thei
should
have
gotten
Paul
Anderson
back
.

Or
at
least
let
Robin
Shou
direct
...
B.
-RRB-
Unlike
the
first
on
,
whose
SFX
were
vibrant
and
somewhat
realist
,
MK
:
A
's
special
effect
ar
bland
,
fake
look
,
and
overal
just
plain
bad
.

I
count
at
least
half
a
dozen
time
that
blue-screen
wa
painfulli
obviou
.

Had
thi
movi
been
made
in
the
80
's
,
it
would
have
been
ground-break
.

But
in
todai
's
industri
,
it
doe
n't
even
look
finish
.

C.
-RRB-
Now
for
the
_
REALLY
_
bad
part
.

I
admit
,
I
'm
an
avid
fan
of
the
Mortal
Kombat
seri
.

The
game
ar
amus
divers
,
an
easi
wai
to
work
off
stress
and
anger
.

The
first
movi
wa
a
mindlessli
fun
thrillrid
.

Thi
could
have
been
a
realli
cool
movi
.

It
is
n't
.

The
writer
apparantli
deem
it
necessari
to
lower
the
target
audienc
from
teen
to
preschool
.

Some
of
the
plot
element
ar
just
plain
stupid
.

How
stupid
?

Take
,
for
instanc
,
how
our
hero
move
around
.

Thei
us
giant
sphere
the
roll
around
underground
,
supposedli
at
thousand
of
mile
per
hour
...
oh
boi
...
Even
wors
is
the
treatment
of
secondari
charact
.

Blink
and
you
'll
miss
'em
.

Most
charact
had
more
depth
in
the
video
game
.

If
you
thought
Batman
&
Robin
wa
bad
about
thi
,
you
ai
n't
seen
nothin
'
yet
.

A
good
75
%
of
the
charact
ar
introduc
,
kick
somebodi
around
a
bit
,
then
either
die
or
ar
forgotten
about
.

There
's
no
explan
at
all
for
thi
.

And
for
the
final
blasphemi
,
the
fight
that
all
the
fan
were
wait
rabidli
for
,
the
fight
hype
to
be
the
most
intric
of
the
movi
,
last
aboutminut
and
then
just
sort
of
...
end
.

It
almost
made
me
weep
.

To
sum
it
all
up
,
rent
it
on
video
,
and
fast-forward
through
everyth
except
the
fight
scene
.

