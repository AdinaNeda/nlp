My
opinion
on
a
film
can
be
easili
swai
by
the
presenc
of
actor
I
love
.

I
love
Ralph
Fienn
.

I
love
Uma
Thurman
.

I
love
Sean
Conneri
.

Hell
,
I
'm
even
a
big
fan
of
Jim
Broadbent
and
Fiona
Shaw
.

I
saw
the
fantast
preview
for
The
Avenger
nearli
eight
month
ago
,
and
I
've
been
eagerli
await
the
film
ever
sinc
.

A
few
month
into
the
summer
,
howev
,
I
notic
that
it
releas
date
had
been
chang
a
few
time
,
and
that
it
had
end
up
in
the
mid-August
dump
ground
.

Then
,
in
thi
final
week
befor
it
offici
releas
,
I
learn
that
it
wa
not
to
be
screen
for
critic
.

And
that
the
actor
had
not
been
plug
the
film
on
late
night
program
.

And
that
it
wa
direct
by
the
same
man
who
brought
us
the
remak
of
Diaboliqu
.

My
expect
fell
to
piec
when
I
learn
all
of
these
thing
.

The
film
I
saw
todai
did
n't
even
meet
those
expect
.

Thi
is
a
lousi
,
incoher
mess
.

I
would
slam
it
harder
,
if
it
were
n't
for
the
nifti
set
and
the
mere
presenc
of
all
these
fine
,
lovabl
actor
.

But
set
ar
ultim
empti
,
and
the
perform
ar
complet
uninspir
.

That
's
the
main
problem
with
The
Avenger
:
for
all
it
's
hip-hop
flash
and
tidal
wave
,
none
of
it
feel
the
least
bit
energet
or
inspir
.

It
's
like
a
chore
,
a
bland
exercis
in
superhero
film
make
.

It
also
feel
like
it
been
cut
to
piec
,
clock
in
at
90
minut
and
forget
to
close
some
of
it
own
subplot
.

Thi
is
just
plain
depress
.

The
Avenger
is
a
film
version
of
the
popular
60
televis
show
.

Frequent
reader
of
mine
will
not
be
surpris
to
learn
that
I
've
never
watch
an
episod
.

I
wa
n't
aliv
then
.

I
do
n't
even
watch
televis
now
.

I
'm
veri
sorri
that
I
do
n't
have
thi
perspect
,
but
,
judg
from
the
review
I
've
alreadi
read
,
know
the
TV
show
just
make
matter
wors
.

I
did
n't
know
and
love
John
Steed
and
Emma
Peel
,
and
therefor
I
wa
not
as
upset
to
see
how
these
Actor
I
Love
have
manag
to
thrash
their
role
.

I
also
doubt
that
familiar
with
the
seri
would
allow
me
to
understand
more
clearli
the
chain
of
event
that
do
take
place
in
thi
film
.

Base
on
what
the
film
told
me
,
I
gather
that
Steed
-LRB-
Fienn
-RRB-
is
some
kind
of
British
super
gui
-LRB-
somewhat
like
Jame
Bond
-RRB-
,
and
that
Dr.
Peel
-LRB-
Thurman
-RRB-
is
just
a
realli
smart
doctor
,
who
also
happen
to
know
a
lot
about
weather
and
about
beat
peopl
up
while
limit
by
tight
leather
suit
.

Thei
ar
to
work
togeth
,
under
Mother
's
order
-LRB-
Mother
is
plai
by
jolli
Jim
Broadbent
,
while
hi
co-conspir
,
Father
,
is
plai
by
the
equally-tal
Fiona
Shaw
-RRB-
.

It
seem
that
there
is
a
man
out
there
control
the
weather
.

Hi
name
is
De
Wynter
-LRB-
Conneri
-RRB-
.

He
is
a
crazi
Scottish
gui
.

Our
hero
had
better
stop
him
,
or
els
...
the
weather
will
keep
get
colder
until
thei
``
have
to
go
to
hell
to
warm
up
''
-LRB-
on
of
the
film
's
few
funni
line
-RRB-
.

Along
the
wai
there
is
subplot
after
subplot
,
hing
sloppili
togeth
by
scene
that
go
nowher
,
feel
perfunctori
,
and
ultim
make
no
sens
.

For
exampl
,
our
hero
ar
eventu
attack
by
a
swarm
of
giant
mechan
insect
with
machinegun
attach
to
their
torso
.

Now
,
killer
bug
do
n't
realli
go
with
the
weather-control
theme
of
the
film
.

The
purpos
of
the
bug
,
other
than
to
annihil
our
hero
,
is
never
establish
.

Thei
're
never
even
directli
connect
to
De
Wynter
-LRB-
thei
're
control
by
hi
crazi
henchman
,
plai
by
Eddie
Izzard
-RRB-
.

The
special
effect
ar
n't
bad
,
but
thei
're
loud
,
obnoxi
,
and
intrus
.

Like
so
mani
scene
in
the
film
,
it
seem
present
onli
to
keep
your
attent
from
wane
.

And
I
guarante
you
,
it
will
wane
.

I
stop
think
about
the
stori
when
I
realiz
it
did
n't
do
ani
good
to
think
.

The
Avenger
ha
clearli
been
chop
up
and
re-assembl
so
mani
time
that
even
the
peopl
involv
could
n't
tell
us
what
happen
.

There
ar
sever
scene
in
the
preview
that
did
n't
make
the
final
cut
.

In
addit
,
mani
of
the
sequenc
have
irrit
,
graini
film
qualiti
,
which
make
it
feel
low-budget
-LRB-
the
last
scene
is
particularli
bad
-RRB-
.

The
scene
that
do
n't
featur
action
should
be
electr
,
thank
to
our
wonder
cast
.

Thei
ar
n't
.

The
action
scene
should
be
electr
,
becaus
thi
is
an
action
film
.

Thei
ar
n't
,
either
.

And
what
about
thi
wast
cast
?

Oh
,
it
make
me
weep
with
disappoint
.

We
're
talk
Ralph
Fienn
here
,
on
of
the
best
actor
work
todai
,
on
of
the
best
actor
*
ever
*
.

I
love
the
gui
.

I
've
love
all
hi
film
,
until
thi
on
.

He
seem
like
he
want
to
understand
the
film
,
but
he
's
as
lost
as
we
ar
.

And
Thurman
,
in
all
of
her
spectacular
beauti
and
talent
,
ca
n't
manag
to
look
at
home
here
.

-LRB-
I
do
n't
blame
her
complet
,
for
there
is
a
strang
subplot
involv
her
evil
twin
that
is
never
explain
in
ani
wai
,
and
it
ca
n't
be
easi
to
deal
with
such
terribl
screenwrit
.
-RRB-

Conneri
,
howev
,
seem
the
most
out
of
it
,
total
lack
ani
kind
of
focu
,
or
interest
,
for
that
matter
.

Who
is
to
blame
?

Is
it
the
screenwrit
,
Don
MacPherson
?

Perhap
,
although
I
can
see
a
shell
of
a
stori
here
that
could
have
been
a
good
superhero
movi
had
it
been
handl
right
.

Is
it
the
actor
?

For
God
's
sake
,
no
--
these
peopl
clearli
lost
interest
when
thei
saw
the
inevit
path
of
destruct
on
which
thi
film
wa
travel
.

I
blame
director
Jeremiah
Chechik
,
who
should
n't
have
been
given
the
task
to
begin
with
.

Hi
best
film
,
Benni
&
Joon
,
is
quirki
in
the
same
wai
that
thi
on
wa
suppos
to
be
,
but
here
it
mostli
seem
like
a
lot
of
fail
attempt
at
wit
and
humor
.

Add
to
that
a
lack
of
experi
in
big-budget
action
,
and
you
have
the
worst
choic
for
director
thi
side
of
Jame
Ivori
.

If
The
Avenger
ha
a
save
grace
,
it
's
the
set
design
.

The
set
-LRB-
by
Stuart
Craig
-RRB-
ar
big
,
color
,
and
often
pleas
to
look
at
.

I
like
the
final
set-piec
,
on
De
Wynter
's
island
,
even
if
none
of
it
make
a
bit
of
sens
.

I
admir
the
overhead
view
of
the
stair
,
show
Dr.
Peel
run
around
in
circl
and
never
get
anywher
.

I
also
enjoi
Michael
Kamen
's
music
score
,
particularli
in
the
open
credit
.

But
thi
stuff
is
routin
--
good
set
and
music
ar
nice
,
but
there
's
a
lot
more
that
need
to
be
here
.

Inspirat
,
for
instanc
,
would
have
been
realli
nice
.

Mayb
a
littl
bit
of
cohes
in
the
stori
.

Or
a
sens
of
purpos
.

Most
of
all
,
though
,
I
would
have
like
to
see
these
actor
relish
in
their
role
.

Damn
thi
movi
for
not
give
them
the
chanc
.

