Arrive
in
a
barrag
of
hype
,
The
Blair
Witch
Project
is
on
of
the
biggest
box
offic
success
of
the
year
.

Howev
,
like
The
Golden
Child
,
although
Blair
Witch
ha
made
a
lot
of
monei
,
it
's
not
veri
good
.

Donahu
,
William
and
Leonard
plai
themselv
as
three
student
who
set
out
to
make
a
documentari
about
the
Blair
Witch
myth
.

The
film
is
made
up
of
the
camcord
footag
thei
record
,
which
mean
graini
footag
and
woozi
camera
angl
.

Although
event
start
of
normal
,
thei
get
weird
pretti
quickli
,
while
the
threesom
argu
more
and
more
as
the
journei
goe
on
.

Although
an
interest
premis
,
The
Blair
Witch
Project
amount
to
noth
more
than
a
miss
opportun
.

The
biggest
mistak
the
film
make
is
to
let
three
mediocr
actor
the
chanc
to
improvis
.

Most
of
the
dialogu
is
ad-lib
,
and
still
manag
to
sound
like
a
poor
B-movi
.

The
'
script
'
eventu
degener
into
shout
match
,
with
the
F-word
includ
a
lot
to
sound
like
student
.

There
ar
break
in
these
argument
where
some
creepi
event
occur
,
but
then
it
's
back
to
the
shout
and
swear
,
which
get
veri
tiresom
veri
quickli
.

If
I
want
to
see
three
peopl
get
lost
in
the
wood
,
shout
a
lot
and
swear
,
I
'd
go
on
scout
camp
.

But
no
,
the
audienc
is
meant
to
get
some
entertain
factor
out
of
thi
,
but
I
'm
not
quit
sure
how
.

The
supernatur
part
of
the
film
ar
actual
interest
,
especi
if
close
attent
is
paid
to
the
stori
develop
first
twenti
minut
.

With
no
music
and
no
budget
to
work
with
,
the
film
ha
to
depend
on
natur
,
psycholog
scare
,
which
ar
sometim
well
deliv
,
sometim
not
.

I
never
realli
felt
truli
scare
dure
ani
part
of
the
film
,
although
there
is
a
small
sens
of
fear
underli
throughout
the
film
.

Howev
,
becaus
the
actor
ar
so
irrit
,
the
scare
ar
lost
when
it
eventu
revert
back
to
Heather
sai
'
What
the
f*ck
is
that
?
'

a
lot
,
and
Mike
giggl
like
a
looni
.

There
's
also
the
niggl
fact
that
these
student
filmmak
do
some
realli
stupid
thing
.

The
main
problem
is
the
fact
that
even
though
these
hapless
bunch
could
be
kill
at
ani
moment
,
and
ar
hopelessli
lost
,
Heather
still
insist
on
film
it
all
.

The
film
give
a
half
heart
reason
why
she
should
want
to
do
thi
,
but
it
is
n't
veri
convinc
.

Also
,
the
student
have
no
idea
how
to
surviv
in
the
wood
,
such
as
follow
a
larg
river
flow
through
the
wood
to
civilis
.

There
ar
also
some
part
where
the
'
amateur
'
camcord
footag
is
obvious
stage
,
Heather
's
apolog
be
a
major
on
.

The
Blair
Witch
Project
,
in
the
end
,
just
fail
to
deliv
.

I
suppos
if
you
've
been
lost
camp
befor
,
the
film
mai
deliv
some
chill
,
but
thi
is
no
us
for
the
other
99
%
of
the
pai
audienc
who
have
n't
been
lost
in
the
wood
.

Apart
from
the
final
minut
,
the
film
is
mind
boggingli
unscari
,
and
the
shout
match
get
hideous
dull
.

The
spook
scene
ar
short
and
far
between
,
and
ani
other
horror
movi
could
probabl
achiev
the
same
amount
of
fear
that
these
scene
provid
.

Although
a
good
idea
,
it
's
not
execut
well
enough
to
be
a
fun
,
scari
cinema
experi
.

It
's
a
worri
fact
when
the
websit
-LRB-
-RRB-
is
better
than
the
film
.

