Ever
sinc
WARGAMES
,
the
first
real
comput
hack
movi
,
Hollywood
ha
attempt
to
produc
more
and
more
film
about
hack
.

These
film
usual
show
the
audienc
a
look
into
comput
that
realli
is
not
imposs
and
is
usual
ridicul
.

If
it
's
not
thi
,
the
exagger
about
what
is
real
is
usual
greater
than
you
would
expect
.

HACKERS
is
guilti
of
all
of
these
crime
.

To
anyon
familiar
with
comput
and/or
the
Internet
,
most
of
the
movi
is
so
incred
stupid
that
you
ca
n't
take
it
anymor
.

Those
of
you
who
frequent
on
of
the
greatest
site
on
the
World
Wide
Web
,
the
Internet
Movi
Databas
,
ar
probabl
awar
of
the
section
for
movi
entitl
``
Goof
''
.

Thi
is
the
section
for
mistak
in
the
movi
.

Well
,
IMDb
sum
up
HACKERS
extrem
well
with
the
first
goof
list
:
``
Gener
ill-inform
and
ridicul
to
the
extrem
regard
the
capabl
of
comput
and
technolog
''
.

And
truthfulli
,
thi
is
one-hundr
percent
correct
.

The
plot
,
obvious
,
center
around
comput
hack
.

More
specif
,
it
follow
the
exploit
of
Dade
Murphi
,
a
young
comput
hacker
plai
by
Jonni
Lee
Miller
.

Miller
should
be
extrem
thank
for
the
film
TRAINSPOTTING
,
becaus
if
it
were
not
for
that
,
who
know
where
hi
career
would
be
now
after
HACKERS
.

The
film
open
with
a
young
Dade
Murphi
be
arrest
for
hack
comput
system
.

He
wa
forbidden
to
us
a
comput
until
he
turn
eighteen
year
old
.

The
film
then
pick
up
at
that
point
,
an
eighteen
year
old
Dade
Murphi
hack
awai
at
the
comput
.

Dade
later
meet
up
with
fellow
hacker
Acid
Burn
-LRB-
Angelina
Joli
-RRB-
,
Cereal
Killer
-LRB-
Matthew
Lillard
-RRB-
,
Lord
Nikon
-LRB-
Laurenc
Mason
-RRB-
,
and
the
Phantom
Phreak
-LRB-
Renoli
Santiago
-RRB-
,
who
ar
basic
try
to
do
what
is
next
to
imposs
:
hack
the
Gibson
comput
and
not
get
caught
by
the
FBI
.

Oh
,
and
as
a
littl
sub-plot
,
Dade
also
tri
to
woo
Acid
Burn
,
the
onli
femal
hacker
in
the
film
.

Realli
,
I
ca
n't
think
of
much
more
to
sai
about
the
film
's
plot
asid
from
what
I
've
said
.

It
's
just
a
bunch
of
comput
geek
-LRB-
no
,
obsess
compuls
comput
geek
a
littl
too
advanc
for
realiti
-RRB-
run
around
hack
comput
and
stai
awai
from
the
FBI
.

Thank
to
thi
movi
,
the
public
unfamiliar
with
comput
now
ha
a
distort
viewpoint
of
comput
and
what
thei
can
do
.

Thi
movi
make
comput
look
a
littl
bit
too
advanc
for
their
time
,
and
the
frivol
suggest
it
make
ar
too
mani
to
list
.

To
sum
it
up
,
HACKERS
is
a
terribl
comput
film
.

If
you
're
look
for
a
film
about
comput
closer
to
what
goe
on
in
the
real
world
,
I
suggest
SNEAKERS
.

Although
at
time
it
too
mai
seem
a
littl
far
fetch
,
it
should
be
much
more
believ
than
HACKERS
to
regular
comput
user
.

Hopefulli
,
thi
film
did
n't
influenc
too
mani
young
comput
user
out
to
think
thei
can
do
what
is
portrai
in
thi
105
minut
wast
of
time
.

