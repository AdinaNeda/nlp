star
Christoph
Walken
,
Jai
Mohr
,
Denni
Leari
,
Sean
Patrick
Flanneri
written
and
direct
by
Peter
O'Fallon
Walken
star
as
a
mobster
who
is
kidnap
and
held
for
ransom
by
four
bratti
rich
kid
.

It
seem
that
a
woman
ha
also
been
kidnap
--
she
is
the
sister
of
on
of
them
-LRB-
E.T.
's
Henri
Thoma
-RRB-
and
the
girlfriend
of
anoth
-LRB-
Flanneri
-RRB-
--
and
the
ask
price
is
$
2
million
,
which
said
snot
ar
unabl
to
cough
up
alon
.

Thei
even
cut
off
Walken
's
finger
to
show
thei
mean
busi
,
becaus
thei
ar
desper
to
save
the
woman
's
life
.

Suicid
King
is
a
terribl
film
.

Walken
asid
,
there
is
n't
a
singl
appeal
cast
member
.

O'Fallon
creat
charact
that
ar
function
type
without
ani
reson
.

In
an
amusingli
uniron
scene
,
Walken
plai
poker
with
the
foursom
and
describ
each
of
their
person
to
a
tee
--
it
's
as
if
he
wa
read
the
summari
sheet
for
a
cast
director
.

The
plot
is
anoth
issu
entir
.

O'Fallon
is
someon
whom
I
'm
bet
ha
seen
Reservoir
Dog
and
The
Usual
Suspect
too
mani
time
,
for
not
onli
doe
hi
stori
veer
off
on
bizarr
tangent
from
whenc
thei
never
return
-LRB-
do
we
realli
need
the
scene
where
Denni
Leari
beat
up
an
abus
father
with
a
toaster
,
which
is
entir
unrel
to
both
the
stori
and
Leari
's
charact
,
or
the
numer
anecdot
sequenc
?
-RRB-

,
but
the
central
plot
itself
is
a
serpentin
mess
,
fill
with
cross
and
doubl
cross
and
tripl
cross
...
By
the
fourth
big
revelation/twist
,
I
had
complet
tune
out
,
wonder
what
on
Earth
attract
these
actor
to
the
materi
.

Recent
a
peer
,
a
fellow
young
filmmak
,
inform
me
that
he
had
an
idea
for
a
movi
about
four
gui
,
the
mob
,
and
the
FBI
.

It
occur
to
me
then
what
's
wrong
with
indi
like
Suicid
King
:
I
suspect
O'Fallon
ha
never
met
a
mobster
,
is
not
a
rich
man
,
doe
n't
deliv
endless
``
clever
''
monologu
to
hi
friend
about
hi
favourit
type
of
boot
...
In
short
,
these
gui
ar
just
rif
on
other
movi
,
and
in
do
that
,
make
the
same
film
over
and
over
and
over
again
.

Tarantino
found
hi
nich
and
now
hundr
of
GenXer
with
movi
camera
ar
try
to
find
Tarantino
's
nich
instead
of
carv
their
own
.

