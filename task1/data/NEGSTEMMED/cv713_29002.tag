America
's
Sweetheart
-LRB-
2001
-RRB-
``
America
's
Sweetheart
''
ha
an
intrigu
premis
and
a
great
cast
,
but
it
is
n't
nearli
as
edgi
or
funni
as
it
should
be
.

Almost
all
the
problem
with
the
project
can
be
trace
back
to
co-script
writer
Billi
Crystal
,
who
show
the
same
lack
of
disciplin
with
the
screenplai
that
he
typic
displai
while
co-host
``
Comic
Relief
''
chariti
show
with
Robin
William
and
Whoopi
Goldberg
-LRB-
two
other
paragon
of
self-indulg
-RRB-
.

Crystal
ignor
a
simpl
,
but
crucial
,
rule
:
For
a
screwbal
comedi
to
work
,
the
charact
must
be
place
into
a
rigid
social
set
,
becaus
onli
in
that
context
will
their
unorthodox
antic
be
humor
.

``
America
's
Sweetheart
''
take
place
at
a
press
junket
,
where
decorum
must
be
maintain
in
front
of
the
report
.

It
's
a
promis
set-up
,
but
the
screenplai
quickli
blow
off
the
rule
,
thu
dissip
the
tension
of
the
situat
.

By
the
end
of
the
film
,
all
the
lead
perform
particip
in
a
huge
fight
with
a
room
full
of
journalist
look
on
,
but
their
outburst
ar
onli
mildli
amus
becaus
the
structur
ha
been
destroi
.

John
Cusack
and
Catherin
Zeta-Jon
plai
Eddie
Thoma
and
Gwen
Harrison
,
a
belov
act
duo
whose
marriag
hit
the
skid
when
Gwen
began
see
Hector
-LRB-
Hank
Azaria
-RRB-
,
a
Spanish
actor
with
an
ego
almost
as
pronounc
as
hi
lisp
.

Of
the
last
nine
film
Eddie
and
Gwen
made
togeth
,
six
cross
the
$
100
million
mark
,
but
the
prospect
for
their
final
effort
,
a
space
opu
titl
``
Time
Over
Time
,
''
ar
far
from
rosi
.

While
Eddie
ha
spent
mani
month
in
a
New
Age
rest
clinic
fret
over
the
breakup
,
Gwen
's
solo
film
have
tank
.

To
make
matter
wors
,
the
director
of
the
movi
-LRB-
Christoph
Walken
-RRB-
,
a
``
visionari
''
who
purchas
the
Unabomb
's
cabin
and
had
it
move
to
hi
backyard
,
is
withhold
the
film
from
the
studio
,
insist
that
the
first
screen
be
held
at
the
junket
.

Desper
to
win
over
the
press
,
the
studio
elect
to
hire
Lee
-LRB-
Billi
Crystal
-RRB-
,
a
recent
fire
publicist
,
to
salvag
the
situat
.

Lee
hope
to
turn
lemon
into
lemonad
by
convinc
Eddie
and
Gwen
to
pretend
to
be
on
the
road
to
reconcili
.

He
enlist
the
help
of
Kiki
-LRB-
Julia
Robert
-RRB-
,
Gwen
's
sister
,
person
assist
and
whip
girl
.

What
Lee
doe
n't
know
is
that
Kiki
is
in
love
with
Eddie
,
a
fact
that
could
temper
her
effect
.

Press
junket
ar
a
haven
for
control
freak
.

Studio
fly
journalist
in
from
around
the
world
and
put
them
up
in
a
plush
hotel
,
with
food
and
drink
alwai
at
hand
.

Gener
,
on
the
even
of
their
arriv
,
writer
ar
buss
to
see
the
featur
film
,
then
ferret
straight
back
to
the
hotel
.

The
next
dai
,
writer
go
to
the
studio
suit
and
assembl
in
group
of
five
or
six
for
roundtabl
interview
.

Everi
30
minut
or
so
,
a
produc
,
director
,
writer
or
actor
is
brought
into
the
room
for
a
few
minut
of
question
,
with
a
publicist
hover
in
the
corner
to
keep
an
ey
on
thing
.

The
atmospher
is
on
of
cordial
oppress
writer
ar
free
to
ask
what
thei
want
,
but
understand
that
if
the
studio
dislik
a
question
,
thei
mai
not
be
invit
to
futur
junket
.

Repres
from
TV
station
face
even
more
restrict
.

Thei
get
roughli
five
minut
to
interview
each
member
of
the
cast
and
crew
,
with
the
studio
film
the
exchang
.

The
``
report
''
ar
notori
for
toss
softbal
question
as
thei
suck
up
to
the
star
,
but
to
plai
it
safe
,
the
studio
stand
readi
to
eras
the
tape
if
anyth
unpleas
occur
.

Place
two
spoil
actor
in
a
set
where
imag
is
everyth
is
inspir
,
but
the
screenplai
undermin
the
conceit
.

The
junket
is
move
from
the
handsom
,
but
highli
confin
,
Four
Season
Hotel
to
a
plush
resort
near
La
Vega
.

For
most
of
the
film
,
the
movi
star
run
around
the
sprawl
ground
,
complet
safe
from
the
ey
of
the
press
.

When
thei
do
deal
with
journalist
,
the
``
it
is
imper
that
you
be
on
your
best
behavior
in
front
of
the
report
''
premis
is
de-claw
.

Gwen
and
Eddie
insult
each
other
while
the
TV
camera
roll
,
thei
scream
at
each
other
in
a
restaur
fill
with
the
media
and
,
at
the
screen
of
the
movi
,
everyon
connect
with
the
film
goe
nut
,
all
without
ani
repercuss
.

Lee
certainli
is
n't
bother
by
ani
of
the
infantil
outburst
;
in
fact
,
he
make
arrang
for
footag
of
even
more
inappropri
behavior
to
be
deliv
to
the
tabloid
.

Is
the
studio
angri
about
hi
handl
of
the
comb
actor
?

Hell
no
thei
feel
Lee
is
a
geniu
for
garner
so
much
public
for
the
movi
.

All
of
which
underscor
how
Billi
Crystal
and
co-writ
Peter
Tolan
screw
up
their
own
premis
:
The
comedi
in
``
America
's
Sweetheart
''
is
base
on
barely-in-control
peopl
try
to
contain
themselv
in
the
presenc
of
report
,
except
that
it
doe
n't
matter
becaus
ani
public
is
good
public
.

And
thu
the
veri
set-up
for
the
film
implod
,
leav
smoke
and
dust
in
place
of
laughter
.

So
what
about
the
cast
?

Julia
Robert
,
at
her
best
plai
the
underdog
,
is
utterli
charm
here
,
although
I
could
have
live
without
flashback
that
exist
sole
as
an
excus
to
show
her
in
a
fat
suit
-LRB-
and
not
a
veri
convinc
on
,
by
the
wai
-RRB-
.

Catherin
Zeta-Jon
make
a
believ
brat
and
John
Cusack
flesh
out
hi
obsess
charact
enough
to
make
him
vagu
sympathet
.

By
cast
himself
as
the
publicist
,
Billi
Crystal
allow
himself
to
do
roughli
the
same
thing
he
doe
on
``
Comic
Relief
''
-
stai
on
the
sidelin
of
the
action
while
toss
off
cornbal
joke
and
snarki
remark
.

In
support
role
,
Hank
Azaria
wear
out
hi
welcom
fast
with
broad
gestur
and
a
Spanish
accent
that
Speedi
Gonzal
would
have
deem
``
too
broad
.
''

Seth
Green
is
amus
as
a
toadi
,
Stanlei
Tucci
is
veri
good
as
a
ruthless
studio
head
and
Christoph
Walken
plai
the
eccentr
director
with
suitabl
flair
,
though
he
ha
littl
to
work
with
.

Come
to
think
of
it
,
``
littl
to
work
with
''
is
the
oper
phrase
for
thi
movi
.

As
a
Hollywood
satir
,
``
America
's
Sweetheart
''
is
toothless
.

As
a
romanc
,
it
is
at
best
a
minor
pleasur
.

Such
a
good
cast
,
such
a
wast
of
their
effort
.

Had
it
not
been
taken
long
ago
,
a
better
titl
for
the
film
would
have
been
``
Much
Ado
about
Noth
.
''

