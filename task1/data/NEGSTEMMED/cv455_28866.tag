Violenc
is
bad
.

Violenc
is
ugli
.

Violenc
breed
yet
more
violenc
.

Kid
,
do
n't
try
thi
at
home
.

Thi
weighti
messag
is
n't
the
onli
barrier
to
enjoi
Brother
,
but
it
's
certainli
on
of
the
largest
.

Written
,
direct
by
,
and
star
the
infam
Takeshi
Kitano
-LRB-
Kikujiro
,
Sonatin
-RRB-
Brother
is
hi
first
film
made
outsid
hi
famili
Japan
,
bring
the
yakuza
tradit
to
Lo
Angele
.

-LRB-
Yakuza
translat
for
the
averag
American
is
the
Japanes
mafia
.
-RRB-

If
you
piss
a
``
famili
''
member
off
,
or
dishonor
yourself
in
ani
wai
,
the
usual
punish
is
public
displai
of
self-mutil
,
usual
result
in
the
loss
of
limb
.

A
definit
of
dishonor
can
be
anyth
from
make
a
stupid
decis
to
leav
on
famili
for
anoth
.

It
would
be
interest
to
know
more
about
where
these
and
other
custom
come
from
.

Unfortun
the
film
doe
n't
give
too
much
of
an
explan
,
assum
it
audienc
is
awar
of
Kitano
's
earlier
work
.

There
ar
sever
shot
that
focu
specif
on
detail
tattoo
that
spread
across
the
entir
back
of
the
yakuza
member
,
lead
on
to
assum
thei
would
be
symbol
of
someth
,
but
you
never
know
what
.

Then
there
's
a
scene
in
which
a
man
kill
himself
in
front
of
a
rival
in
exchang
for
that
rival
join
the
famili
.

Grant
,
thi
is
on
of
the
best
scene
in
the
movi
,
but
it
doe
n't
make
a
lot
of
sens
.

Instead
,
the
two
hour
ar
basic
spent
watch
the
follow
:
peopl
go
out
and
shoot
each
other
,
talk
about
it
for
ten-plu
minut
,
then
go
out
and
do
it
again
.

The
consist
repetit
discuss
of
territori
dure
these
moment
involuntarili
provok
yawn
.

There
ar
also
plot
detail
thrown
in
for
no
identifi
purpos
.

All
of
the
sudden
Yamamoto
-LRB-
Kitano
-RRB-
ha
a
girlfriend
.

He
bare
speak
to
her
,
treat
her
like
crap
,
and
then
send
her
awai
.

Another
miss
opportun
,
consid
it
is
such
a
big
deal
for
Kitano
to
bring
hi
magic
to
the
Unite
State
,
is
the
combin
of
cultur
,
which
reli
too
heavili
on
overus
stereotyp
.

Though
slow
move
,
Brother
doe
have
good
element
.

The
action
scene
ar
well
direct
,
clearli
defin
,
and
interest
to
watch
.

Some
of
the
violenc
is
more
hint
at
than
shown
,
which
produc
the
lusciou
squirm
that
on
goe
to
see
such
film
for
.

The
actor
would
be
more
entic
if
thei
had
more
to
do
.

Shiras
's
-LRB-
Masaya
Kato
-RRB-
loud
,
sarcast
cool
set
against
Yamamoto
's
quietli
threaten
attitud
is
truli
an
entertain
combin
.

Their
moment
togeth
or
apart
steal
the
rest
of
the
show
.

Also
to
it
credit
,
Brother
tackl
the
caus
and
effect
of
crime
with
realism
.

A
life
of
crime
is
easi
to
get
suck
into
with
the
first
reward
of
quick
cash
.

Sure
peopl
get
rich
,
but
thei
can
also
lose
just
as
easili
.

It
's
a
great
moral
,
with
a
great
cast
,
just
not
much
substanc
to
back
it
up
.

