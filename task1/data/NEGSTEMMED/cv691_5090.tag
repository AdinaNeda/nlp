Thi
is
the
movi
that
could
single-handedli
bring
``
Mysteri
Scienc
Theater
3000
''
out
of
cancel
.

It
's
on
of
those
movi
that
's
so
bad
it
's
absolut
hilari
,
due
in
no
small
part
to
it
big
star
,
supermodel
Cindi
Crawford
.

If
you
onli
rememb
on
thing
about
FAIR
GAME
,
it
should
be
that
it
singlehandedli
prove
Crawford
should
stick
to
the
Sport
Illustrat
swimsuit
issu
.

If
you
thought
Kathi
Ireland
wa
laughabl
in
ALIEN
FROM
L.A.
,
you
'll
chang
your
mind
when
you
see
FAIR
GAME
.

Ireland
would
win
hand
of
Oscar
if
thi
wa
her
onli
competit
.

In
a
real
cast
coup
,
Crawford
plai
a
super-intellig
lawyer
-LRB-
in
a
jog
bra
,
of
cours
-RRB-
.

We
all
know
thi
is
a
stretch
,
the
onli
legal
opinion
Crawford
ha
ever
put
forth
be
that
she
favor
the
death
penalti
for
anyon
who
wear
white
after
Labor
Dai
.

Nonetheless
,
someon
out
there
thought
she
'd
make
a
good
lawyer
,
but
we
're
remind
of
her
true
function
in
the
movi
when
she
take
two
shower
in
a
period
of
twenti
minut
.

And
for
you
horni
teenag
boi
out
there
,
you
actual
get
to
see
her
topless
for
two
second
in
the
dark
...
Come
to
think
of
it
,
that
mai
have
been
a
bodi
doubl
.

Cindi
Crawford
is
n't
black
,
is
she
?

Like
I
said
,
Crawford
is
a
lawyer
with
a
bunch
of
Russian
after
her
.

Billi
Baldwin
-LRB-
or
is
it
Stephen
?

Alec
?

Adam
?

Kim
Basing
?
-RRB-

is
the
polic
detect
who
ha
to
save
her
life
,
time
after
time
,
chase
after
chase
,
explos
after
explos
.

There
's
absolut
noth
origin
about
thi
movi
.

It
's
everi
cop
show
of
the
70
's
mix
with
everi
action
thriller
of
the
80
's
and
everi
technology-exploit
movi
of
the
90
's
.

Three
decad
of
crap
all
in
on
place
,
driven
further
into
the
ground
by
Crawford
's
complet
lack
of
act
talent
.

And
it
's
all
complet
predict
.

You
know
the
mistak
Baldwin
and
Crawford
ar
go
to
make
befor
thei
make
them
,
you
know
when
the
``
sexual
tension
''
will
final
end
up
in
them
consumm
the
relationship
,
you
know
when
the
villain
will
captur
Crawford
so
Baldwin
ha
to
rescu
her
in
the
climax
and
you
know
the
movi
's
go
to
suck
from
the
first
scene
.

The
plot
is
n't
realli
explain
until
the
end
.

All
we
know
is
these
Russian
have
everi
detail
about
Crawford
in
their
comput
.

In
the
word
of
on
of
the
villain
,
``
We
even
know
what
size
pantyhos
she
wear
.
''

-LRB-
Yeah
,
you
and
everi
14-year-old
boi
in
America
.
-RRB-

He
goe
on
to
add
,
``
We
know
more
about
her
than
she
doe
.
''

-LRB-
_
That
_
I
find
veri
easi
to
believ
.
-RRB-

I
hope
you
enjoi
those
two
sampl
dialogu
quot
,
becaus
I
wrote
down
plenti
of
other
bad
on
-LRB-
``
If
it
were
n't
for
me
,
you
'd
still
be
pull
banana
out
of
your
ass
in
Cuba
!
''
-RRB-

becaus
,
you
see
,
FAIR
GAME
is
not
onli
a
showcas
for
recycl
action
clich
and
terribl
act
,
but
also
some
serious
bad
dialogu
.

It
all
add
up
to
a
realli
terribl
movi
that
made
me
laugh
in
plenti
of
place
I
wa
n't
suppos
to
and
grimac
in
place
I
wa
suppos
to
laugh
.

One
more
thing
FAIR
GAME
ha
against
it
is
some
aw
comic
relief
.

Would
you
laugh
at
a
scene
where
Crawford
tortur
a
comput
nerd
with
doubl
entendr
like
``
I
'm
veri
interest
in
_
hard_war
''
and
other
crap
about
plai
with
hi
joystick
?

I
would
n't
,
but
not
becaus
I
'm
a
comput
nerd
.

It
's
just
not
funni
.

Crawford
's
onli
contribut
to
the
Informat
Age
ar
a
few
GIF
file
float
around
with
her
head
on
a
nude
Traci
Lord
'
bodi
...
or
wa
it
Jack
Lord
's
bodi
?

I
'll
leav
you
with
the
final
line
of
dialogu
in
the
movi
.

The
boat
with
all
the
Russian
ha
just
blown
up
and
Baldwin
and
Crawford
ar
float
on
a
life
raft
.

Crawford
sai
woodenli
,
``
That
wa
my
client
's
boat
you
just
blew
up
.
''

I
'm
file
a
lawsuit
against
you
.

You
're
in
big
troubl
,
``
or
someth
to
that
effect
,
and
Baldwin
repli
smugli
,
''
What
do
I
have
to
do
to
get
out
of
it
?
''
''

Thei
of
cours
begin
make
out
and
the
credit
roll
.

I
'm
not
go
to
talk
about
how
stupid
the
line
is
or
how
,
if
thei
'd
realli
want
to
go
for
a
bad
close
line
thei
would
have
had
Baldwin
sai
,
``
You
think
I
could
settl
out
of
court
?
''

but
I
will
tell
you
that
,
if
anyon
ever
suggest
you
watch
FAIR
GAME
with
him
or
her
,
you
quot
the
final
line
of
the
movi
to
that
person
:
``
What
do
I
have
to
do
to
get
out
of
it
?
''

