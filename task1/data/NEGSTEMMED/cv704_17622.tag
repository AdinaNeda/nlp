After
1993
's
``
Fall
Down
,
''
I
hope
that
Joel
Schumach
would
matur
into
a
great
director
.

Sinc
then
he
ha
offer
us
two
so-so
adapt
of
John
Grisham
novel
-LRB-
``
The
Client
''
and
``
A
Time
to
Kill
''
-RRB-
and
two
Batman
movi
that
lower
the
standard
of
that
franchis
.

Although
these
disappoint
dampen
my
enthusiasm
for
Schumach
's
potenti
,
the
public
for
hi
latest
releas
,
``
8MM
,
''
rais
new
hope
.

It
promis
to
be
someth
unusu
.

It
wa
n't
.

The
plot
goe
like
thi
:
Tom
Well
-LRB-
Nicola
Cage
-RRB-
is
a
privat
ey
who
is
hire
by
a
wealthi
woman
,
allegor
name
Mrs.
Christian
-LRB-
Myra
Carter
-RRB-
,
to
investig
an
8mm
movi
found
among
her
late
husband
's
belong
.

The
movi
appear
to
be
a
snuff
film
in
which
a
teenag
girl
is
rape
and
murder
by
a
man
in
a
leather
mask
-LRB-
who
remind
me
of
Bane
in
Schumach
's
``
Batman
&
Robin
''
-RRB-
.

Becaus
murder
can
be
realist
simul
in
movi
,
Mrs.
Christian
want
Tom
to
discov
if
the
girl
is
aliv
or
dead
.

By
comb
through
miss
person
report
,
Tom
find
the
girl
's
name
and
track
her
to
Lo
Angele
.

With
the
aid
of
Max
California
-LRB-
Joaquin
Phoenix
-RRB-
,
a
video
store
clerk
-LRB-
and
anoth
symbol
name
-RRB-
,
Tom
wander
through
the
underworld
of
pornographi
in
search
of
snuff
.

The
plot
bear
an
obviou
resembl
to
Paul
Schrader
's
``
Hardcor
,
''
in
which
a
father
goe
deeper
and
deeper
into
the
industri
of
porn
and
sex
to
find
hi
miss
daughter
.

Both
film
ar
model
on
Dant
's
``
The
Inferno
,
''
of
cours
;
``
8MM
''
make
that
connect
overli
obviou
by
cast
Max
as
Virgil
and
have
him
constantli
tell
Tom
that
thei
were
head
toward
a
meet
with
the
devil
.

``
Hardcor
''
is
a
much
more
subtl
and
meaning
film
.

``
8MM
''
had
a
lot
of
promis
.

In
it
better
moment
it
's
an
examin
of
violenc
as
entertain
and
of
the
beast
within
even
a
nice
gui
like
Tom
.

One
moment
of
the
first
sort
:
when
we
first
meet
Max
he
's
read
Truman
Capot
's
``
In
Cold
Blood
''
underneath
the
cover
of
a
porn
novel
.

``
In
Cold
Blood
''
wa
the
father
of
the
true-crim
genr
,
which
is
the
literari
equival
of
snuff
film
.

Max
promis
that
we
will
see
exampl
of
the
second
theme
when
he
tell
Tom
,
``
Danc
with
the
devil
and
the
devil
do
n't
chang
;
the
devil
chang
you
.
''

Tom
doe
change-I
'll
let
you
discov
the
specif
for
yourself-but
not
perman
.

Schumach
and
screenwrit
Andrew
Kevin
Walker
did
n't
have
the
gut
to
take
Tom
so
far
down
the
dark
tunnel
that
he
could
n't
come
back
.

-LRB-
Walker
's
``
Seven
''
did
a
much
better
job
on
that
theme
by
refus
to
compromis
-RRB-
.

Cage
tri
hard
to
pull
it
off
,
but
the
script
doe
n't
give
him
enough
to
work
with
.

Phoenix
walk
awai
with
the
movie-h
's
smart
,
charm
,
and
funni
.

Other
cast
member
includ
``
Fargo
''
's
Peter
Stormar
camp
it
up
as
porn
auteur
Dino
Velvet
and
``
The
Soprano
''
's
Jame
Gandolfini
as
a
soulless
porn
merchant
who
push
Tom
too
far
.

In
the
last
analysi
,
sinc
``
8MM
''
fall
short
of
it
pretens
and
it
promis
,
it
just
anoth
privat
ey
stori
in
the
Raymond
Chandler/Ross
Macdonald
tradit
of
have
the
detect
uncov
the
deprav
behind
the
glossi
facad
of
wealth
and
privileg
.

Howev
,
unlik
other
predict
fare
in
the
genre-last
year
's
``
Twilight
,
''
for
exampl
-
``
8MM
''
is
especi
disappoint
becaus
it
could
have
been
much
more
than
it
is
.

