Movi
base
on
video
game
,
such
as
Street
Fighter
or
Mario
Bros.
,
have
never
gener
much
interest
at
the
box
offic
.

But
when
the
first
Mortal
Kombat
movi
came
out
in
1995
,
it
did
surprisingli
well
.

With
a
simpl
stori
,
a
pulsat
soundtrack
,
and
lot
of
awesom
choreograph
fight
scene
,
the
movi
move
quickli
and
displai
lot
of
energi
.

It
got
my
vote
for
`
the
movi
where
I
expect
the
least
and
got
the
most
'
.

And
,
if
you
have
n't
had
the
opportun
,
I
would
definit
recommend
that
you
see
it
on
video
.

In
the
world
of
Mortal
Kombat
,
base
on
the
popular
arcad
game
,
dark
forc
from
Outworld
try
to
infiltr
Earth
's
realm
with
the
ulim
goal
of
total
conquest
and
the
destruct
of
human
.

The
Mortal
Kombat
refer
to
a
competit
,
fought
by
human
mortal
against
Outworld
's
minion
,
which
if
won
,
will
guarante
Earth
's
safeti
for
anoth
gener
.

MK2
pick
up
almost
immedi
after
the
human
return
victori
from
competit
.

Despit
their
victori
,
howev
,
a
gatewai
ha
somehow
open
,
and
Outworld
continu
in
it
quest
to
conquer
Earth
.

Outworld
's
warrior
includ
an
impress
collect
of
fighter
,
includ
Shao
Kahn
,
Mintoro
-LRB-
a
centaur
-RRB-
,
Sheeva
-LRB-
a
four-arm
ogr
-RRB-
,
and
Sindel
.

Earth
's
mortal
warrior
,
under
the
leadership
of
the
benevol
God
,
Lord
Rayden
,
includ
Liu
Kang
,
Princess
Kitana
,
Sonya
Blade
and
Jax
,
who
must
fight
against
Outworld
's
forc
.

It
's
an
easi
enough
premis
,
but
unfortun
-LRB-
veri
unfortun
-RRB-
,
the
writer
try
to
do
too
much
with
the
movi
.

It
is
onli
a
video
game
,
after
all
,
but
the
on-screen
version
tri
to
go
beyond
it
's
comic
book
boundari
by
ad
depth
,
a
conceptu
of
self-worth
,
and
worst
of
all
,
a
love
stori
.

Thi
becom
veri
awkward
,
becaus
all
of
the
player
ar
fighter
and
noth
more
.

Thei
ar
likabl
and
impress
when
thei
ar
showcas
their
martial
art
and
fight
skill
,
but
look
extrem
awkward
and
uncomfort
when
thei
ar
requir
to
actual
act
.

Thi
becom
embarrassingli
appar
,
for
exampl
,
as
the
movi
tri
to
develop
a
romant
bond
between
Liu
Kang
and
Princess
Kitana
.

The
origin
MK
wa
smart
in
let
the
player
do
what
thei
do
best
.

Thei
spoke
littl
,
but
fought
a
lot
.

Thu
,
the
movi
wa
easi
to
digest
.

Howev
,
MK2
tri
to
give
these
player
a
certain
amount
of
depth
,
an
element
that
wa
lack
in
the
first
movi
,
but
the
attempt
here
fail
miser
.

Thi
uneasi
feel
is
readili
appar
,
for
exampl
,
whenev
you
hear
Lord
Rayden
speak
.

Not
onli
doe
he
talk
in
riddl
and
offer
enigmat
-LRB-
e.g.
useless
-RRB-
advic
,
but
he
speak
so
slowli
as
if
to
simul
infinit
wisdom
.

Try
read
thi
half
as
fast
,
and
you
'll
hear
what
I
mean
.

But
the
movi
is
not
a
complet
fatal
.

Fight
scene
ar
well-choreograph
,
highlight
by
terrif
acrobat
and
agil
.

And
player
of
the
game
will
be
reward
with
the
inclus
of
practic
everi
charact
from
the
video
game
,
although
their
screen
time
amount
to
noth
more
than
a
cameo
.

Even
if
you
're
not
familiar
with
the
video
game
,
each
fighter
ha
their
own
costum
and
uniqu
weaponri
to
easili
differenti
themselv
.

But
it
's
just
too
littl
of
what
movie-go
and
especi
fan
of
the
game
will
expect
.

Oddly
,
the
inclus
of
the
human
element
actual
pollut
the
puriti
of
the
Mortal
Kombat
essenc
.

And
the
poor
act
result
in
a
muddl
movi
that
offer
the
view
audienc
littl
chanc
for
surviv
.

-LRB-
I
am
now
talk
veri
slowli
as
if
to
simul
infinit
wisdom
-RRB-
.

Rent
the
first
on
,
but
the
sequel
is
for
Kombat
fan
onli
.

