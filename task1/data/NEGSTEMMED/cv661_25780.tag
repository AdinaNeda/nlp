I
have
a
confess
.

Even
though
I
am
a
movi
junkie-I
saw
Eye
of
the
Behold
and
yet
I
still
watch
movies-I
've
never
seen
ani
of
the
origin
Godzilla
.

-LRB-
Note
that
Godzilla
2000
is
not
a
sequel
to
the
U.S.
Godzilla
releas
in
1998
;
it
's
the
American
releas
of
a
1999
Godzilla
flick
call
Gojira
ni-sen
mireniamu
.
-RRB-

So
,
part
of
me
wa
excit
as
I
drove
to
the
local
multiplex
to
see
Godzilla
2000-the
latest
entri
in
an
almost
50-year
old
franchis
.

Thi
wa
a
long-await
treat
.

I
wa
expect
a
goofi
good
time
,
complet
with
bad
dub
,
scienc
fair
level
set
and
a
ludicr
plot
line
.

The
last
thing
I
expect
wa
for
the
movi
to
be
sobor
.

I
can
onli
imagin
what
the
theatr
full
of
kid
I
saw
thi
with
felt
.

You
do
get
all
of
the
``
so
bad
thei
're
good
''
trait
so
normal
associ
with
the
franchis
.

But
you
have
to
sit
through
a
snoozer
plot
that
ha
the
organ
of
a
Rorschach
blot
.

It
's
a
deal
I
wa
n't
will
to
accept
.

Hiroshi
Kashiwabara
and
Waturu
Mimura
's
script
cram
in
wai
too
mani
detail
,
and
in
mani
case
,
fail
to
follow
up
on
them
.

The
maneuv
is
not
onli
disconcert
,
but
give
the
movi
a
perman
logi
,
weighti
feel
.

Godzilla
start
off
destroi
power
plant
and
then
just
stop
.

Wa
it
a
whim
?

A
bold
polit
statement
?

I
do
n't
know
.

It
foe
,
an
ancient
meteor
that
look
like
Prudenti
's
logo
,
doe
n't
just
fight
the
Beast
from
the
Far
East
.

No
,
it
's
got
to
have
life-sav
power
,
a
plot
to
eras
``
the
data
''
from
Japan
,
a
desir
to
clone
and
the
abil
to
becom
a
spaceship
and
then
some
kind
of
tentacl
space
creatur
.

There
's
a
battl
between
the
head
of
the
Godzilla
Predict
Unit
-LRB-
Takehiro
Murata
-RRB-
and
a
slimi
govern
offici
-LRB-
Hiroshi
Abe
-RRB-
over
the
handl
of
Godzilla
,
and
person
issu
.

And
then
there
's
the
plucki
new
photograph
and
blah
,
blah
,
blah
.

It
's
like
watch
Magnolia
all
over
again-except
without
the
good
write
,
keen
sociolog
insight
and
Aime
Mann
song
.

What
a
wast
.

When
the
talk
and
the
plot
point
stop
hurtl
at
you
,
Godzilla
2000
doe
the
job-it
's
entertain
and
goe
down
easi
.

The
action
scene
ar
cheesi
in
their
grandeur
though
a
littl
sluggish
.

The
dub
is
nice
and
aw
,
with
Murata
sound
like
he
's
in
constant
need
of
a
cough
drop
.

As
for
the
dialogu
,
on
line
summar
the
goofi
factor
:
``
Did
anyon
see
that
fly
rock
go
by
?
''

Thi
movi
also
mark
the
first
time
sinc
The
Three
Stoog
that
I
've
heard
the
word
``
imbecil
''
us
in
casual
convers
.

Though
the
summer
movi
season
is
dry
up
and
Godzilla
2000
ha
it
moment
,
I
would
n't
take
kid
to
see
it
.

Thei
'll
probabl
end
up
ask
more
question
than
Charli
Rose
.

