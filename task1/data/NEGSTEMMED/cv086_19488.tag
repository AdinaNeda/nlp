Luca
wa
wise
to
start
hi
Star
War
trilogi
with
Episod
4
:
Episod
1
is
a
bore
,
empti
spectacl
that
featur
some
nice
special
effect
.

After
the
familiar
'
A
long
time
ago
...
'
open
,
the
film
start
with
the
open
yellow
crawl
that
featur
in
everi
Star
War
movi
and
comput
game
.

The
plot
is
that
the
trade
confeder
ar
block
off
suppli
to
the
peac
planet
of
Naboo
,
rule
by
Queen
Amidala
-LRB-
Portman
-RRB-
Jedi
Knight
Qui-Gon
-LRB-
Neeson
-RRB-
and
Obi
Wan
-LRB-
McGregor
-RRB-
ar
sent
to
negoti
a
deal
with
the
confeder
to
stop
the
blockad
.

Howev
,
thi
simpl
blockad
is
not
all
it
seem
,
and
the
Jedi
Knight
soon
have
to
deal
with
mani
more
danger
,
includ
face
the
evil
Darth
Maul
-LRB-
Rai
Park
.
-RRB-

Thei
also
meet
the
futur
Darth
Vadar
,
Anakin
Skywalk
-LRB-
Jake
Lloyd
-RRB-
Star
War
is
larg
a
failur
in
all
the
major
area
of
filmmak
:
script
,
direct
and
charact
.

The
script
desper
tri
to
make
an
thin
and
bore
stori
into
a
two
hour
epic
.

The
trade
confeder
plot
is
just
not
as
excit
as
the
death
star
,
and
as
the
film
goe
on
the
drama
becom
more
and
more
non-exist
.

There
's
no
underli
tension
,
and
no
urgent
need
to
see
what
the
outcom
is
.

The
film
also
take
a
leisur
pace
in
tell
the
bore
stori
,
which
doe
n't
help
.

There
's
no
snap
to
make
the
film
work
,
it
move
at
a
plod
.

The
script
is
bore
:
all
the
charact
speak
in
moral
,
especi
Anakin
mother
.

There
's
no
group
spirit
evid
here
,
and
when
all
charact
can
speak
onli
in
profound
statement
or
bark
order
,
it
's
evid
that
not
much
is
go
to
happen
.

The
two
lead
ar
hideous
bore
,
static
charact
given
littl
to
do
and
too
much
time
to
do
it
.

Qui-Gon
and
Obi-Wan
hold
no
presenc
on
film
,
and
give
littl
for
the
audienc
to
root
for
.

The
audienc
will
probabl
warm
more
to
bad
gui
Darth
Maul
,
if
he
had
been
given
more
screen
time
.

Maul
is
on
of
the
most
underus
bad
gui
in
film
histori
,
even
if
he
doe
provid
the
onli
'
straight
'
plot
line
in
the
film
-LRB-
all
the
other
plot
in
thi
film
ar
to
be
conclud
throughout
the
trilogi
.
-RRB-

Anakin
is
annoi
and
unlikeable.
,
instead
of
cute
and
huggabl
as
Luca
no
doubt
intend
.

It
's
probabl
not
surpris
that
hi
hate
littl
boi
,
unfortun
bless
'
Ani
'
by
everyon
he
know
,
join
the
dark
side
and
kill
everyon
as
quickli
as
possibl
.

The
comedi
relief
is
supposedli
provid
by
the
gangli
Jar
Jar
Brink
,
although
in
the
end
none
is
provid
in
the
film
by
him
.

Although
hi
floppi
featur
will
no
doubt
be
great
for
stuf
toi
,
the
line
and
action
he
is
given
ar
painfulli
unfunni
.

The
onli
good
laugh
is
when
hi
tongu
is
burn
by
a
'
pod
'
racer
,
becaus
I
knew
he
would
n't
be
abl
to
talk
for
a
while
.

Queen
Amidala
is
a
noth
in
the
film
,
she
serv
no
real
purpos
apart
from
the
fact
that
she
must
'
serv
her
peopl
,
'
and
go
goo-goo
over
Anakin
.

Again
,
her
role
will
becom
more
import
in
the
later
episod
,
and
is
given
littl
to
do
here
.

The
actor
do
n't
help
the
movi
much
either
.

Liam
Neeson
seem
bore
and
embarrass
,
and
seem
desper
to
leav
the
movi
as
quickli
as
possibl
.

McGregor
come
across
as
a
posh
mother
boi
,
with
littl
charm
.

The
cast
director
must
have
chose
him
becaus
he
's
Scottish
like
Alec
Guin
:
there
's
littl
other
explan
for
it
.

Natali
Portman
is
plagu
by
hideou
costum
and
a
poor
script
,
but
she
deliv
a
spunki
perform
.

And
Jake
Lloyd
look
at
everyon
with
hate
ey
and
deliv
hi
line
in
a
bore
voic
.

There
's
veri
littl
talent
evid
with
him
,
it
's
hard
to
see
why
Luca
chose
him
when
there
ar
better
child
actor
out
there
.

He
's
not
even
veri
'
cute
.
'

The
main
claim
of
the
film
,
the
special
effect
,
ar
okai
.

There
's
noth
impress
about
them
,
yet
thei
complement
the
stori
well
.

The
CGI
take
awai
much
of
the
human
element
,
howev
.

The
battl
droid
for
exampl
,
the
replac
for
the
stormtroop
,
ar
characterless
littl
comput
graphic
,
lack
the
humour
and
human
that
came
from
the
stormtroop
.

Episod
1
is
just
disappoint
.

It
's
got
high
product
valu
,
but
littl
els
.

The
music
is
meander
,
except
near
the
end
.

The
plot
give
us
no-on
to
root
for
,
or
even
care
for
.

The
effect
ar
nice
,
but
dull
.

The
plot
goe
on
for
half
an
hour
longer
than
it
should
.

There
's
an
air
of
manufactur
among
the
whole
sorri
affair
,
and
mani
clich
ar
cater
for
throughout
the
film
.

Apart
from
a
remot
excit
pod
race
sequenc
,
Episod
1
is
a
dry
affair
that
serv
littl
purpos
,
except
to
make
monei
and
to
get
us
to
watch
Episod
2
to
see
what
happen
to
the
mani
unfinish
plot
line
in
the
film
.

Disappoint
is
bare
the
word
.

