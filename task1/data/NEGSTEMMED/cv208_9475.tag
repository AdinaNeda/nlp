One
would
think
that
David
Duchovni
,
star
of
the
cult
favorit
``
X-File
''
would
be
veri
care
in
choos
hi
lead
man
cinema
role
.

At
least
on
would
hope
so
.

One
would
be
serious
incorrect
.

Eugen
Sand
-LRB-
Duchovni
-RRB-
is
a
surgeon
so
dedic
to
hi
craft
that
he
becom
addict
to
amphetamin
to
stai
awak
and
work
more
.

It
turn
out
to
be
a
bad
plan
when
he
lose
a
patient
and
hi
licens
while
under
the
influenc
.

Ten
month
later
,
in
a
seedi
bar
to
score
synthet
heroin
,
the
doctor
get
a
chanc
to
ply
hi
trade
when
assassin
gun
down
a
custom
.

Using
a
plastic
water
bottl
and
bar
tube
,
Sand
oper
and
save
hi
life
.

Smalltim
hoodlum
Raymond
Blossom
-LRB-
Timothi
Hutton
-RRB-
is
impress
with
the
cutleri
skill
on
hi
associ
.

He
kidnap
and
then
offer
down-and-out
Eugen
a
posit
in
hi
organ
.

Blossom
need
a
doctor
to
patch
up
gunshot
victim
that
would
be
problemat
at
a
hospit
.

Sand
's
temptat
is
that
thi
will
give
him
a
chanc
to
practic
medicin
again
albeit
illeg
,
big
chunk
of
cash
to
fuel
hi
habit
and
close
proxim
to
the
gangster
's
womanfriend
Clair
-LRB-
Angelina
Joli
-RRB-
,
thi
film
's
babe
factor
.

The
would-b
big
time
oper
is
desper
in
need
of
help
.

On
the
run
from
Russian
mobster
-LRB-
the
bad
gui
that
seem
to
be
all
the
rage
in
thi
year
's
film
-RRB-
and
try
to
cut
a
pirat
softwar
deal
with
the
Chines
Mafia
,
Blossom
is
surround
by
blood
.

After
a
few
medic
misadventur
,
the
physician
grow
disench
with
hi
employ
.

An
encount
with
a
coupl
of
psycho
surfer
dude
who
threaten
to
blow
him
awai
unless
he
``
fix
''
their
dead
buddi
cement
hi
feel
that
he
is
in
the
wrong
line
of
work
.

When
the
FBI
visit
and
forc
him
to
becom
an
inform
,
there
is
no
question
that
he
need
to
be
somewher
els
.

Oddly
enough
,
these
seem
like
thei
could
be
excit
scene
.

The
film
disprov
that
assumpt
.

Then
a
bunch
of
other
uninterest
thing
happen
.

Write
thi
onli
two
dai
after
see
the
film
,
it
a
struggl
to
rememb
the
event
.

You
can
take
that
as
a
solid
lack
of
recommend
.

For
all
the
cool
that
Duchovni
exhibit
in
``
X-File
''
,
it
's
a
surpris
that
hi
big
screen
presenc
is
so
lack
in
charisma
.

The
doctor
is
particularli
bland
and
dull
.

Even
the
cold
turkei
bit
which
is
rife
with
dramat
possibl
come
across
as
banal
.

A
littl
chocol
and
minor
sweat
get
him
through
heroin
withdraw
.

Like
virtual
everyth
els
in
the
film
,
it
's
a
miss
opportun
.

The
Fox
Mulder
detach
doe
n't
work
here
.

Oscar-winn
Hutton
's
manic
mobster
is
much
more
difficult
to
get
a
handl
on
.

Plai
partial
for
comedi
and
partial
as
craze
killer
,
Blossom
almost
becom
real
,
but
then
sink
into
goofi
.

Most
of
the
time
Hutton
look
as
if
he
's
search
for
hi
charact
and
come
up
empti
.

Joli
also
make
a
few
fals
move
toward
creat
a
three-dimension
human
be
befor
give
up
and
turn
into
sceneri
.

The
most
interest
aspect
of
her
perform
is
watch
her
pouti
lip
threaten
to
take
over
the
screen
.

The
rest
ar
n't
ani
better
.

Michael
Masse
's
eccentr
FBI
agent
never
rev
up
.

One
of
Blossom
's
henchmen
steal
a
few
moment
of
the
show
as
a
quirki
gunman
reminisc
of
Val
Kilmer
's
Doc
Hollidai
in
``
Tombston
''
.

Hi
two-gun
blast
as
he
doe
a
bizarr
danc
is
the
high
point
of
the
film
.

But
thatsecond
is
n't
worth
sit
through
the
otherminut
.

First
time
film
director
Andy
Wilson
-LRB-
known
for
hi
work
on
the
British
televis
seri
``
Cracker
''
-RRB-
appear
to
be
attempt
to
jump
on
the
``
Pulp
Fiction
''
bandwagon
.

The
best
of
these
movi
combin
a
sens
of
style
and
flash
with
signific
substanc
.

The
second-r
on
concentr
on
just
on
of
these
aspect
.

``
Plai
God
''
doe
neither
well
.

The
basic
idea
of
the
film
is
solid
.

And
then
it
goe
nowher
.

Even
wors
,
it
plod
back
and
forth
,
up
and
down
a
long
and
wind
road
befor
it
end
up
nowher
.

Fox
fail
yet
again
in
hi
search
for
intellig
life
.

-LRB-
After
overyear
of
write
thi
column
,
Michael
Redman
is
still
shock
when
good
actor
and
a
sound
premis
combin
to
creat
a
wretch
film
.

You
'd
think
he
'd
learn
.

Lesson
plan
can
be
sent
to
mredman@bvoice.com
-RRB-
-LSB-
Thi
appear
in
the
/
97
``
Bloomington
Voic
''
,
Bloomington
,
Indiana
.

Michael
Redman
can
be
reach
at
mredman@bvoice.com
-RSB-

