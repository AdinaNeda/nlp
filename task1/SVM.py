import svmlight
from task1 import NaiveBayes

current_dir = "../task1/data/"

def read_document_text(path_to_file):
    with open(path_to_file, 'r') as f:
        # extract the text for the file
        content = [token.strip() for token in f.readlines()]
    return content


def train_svm(trainingPOS, trainingNEG, unigrams=True, bigrams=False, presence=False):
    dictionary_unigrams = {}
    dictionary_bigrams = {}

    # Assign a unique id to each word
    id = 0
    for file in trainingPOS:
        path_to_file = current_dir + 'POS/' + file
        content = read_document_text(path_to_file)
        # Create dictionary for negative unigrams
        positives = NaiveBayes.count_dups(content)

        for key in positives.keys():
            if key not in dictionary_unigrams.keys():
                dictionary_unigrams[key] = id
                id += 1

        for bigram in zip(content[:-1], content[1:]):
            if bigram not in dictionary_bigrams.keys():
                dictionary_bigrams[bigram] = id
                id += 1

    for file in trainingNEG:
        path_to_file = current_dir + 'NEG/' + file
        content = read_document_text(path_to_file)
        # Create dictionary for negative unigrams
        negatives = NaiveBayes.count_dups(content)
        for key in negatives.keys():
            if key not in dictionary_unigrams.keys():
                dictionary_unigrams[key] = id
                id += 1

        for bigram in zip(content[:-1], content[1:]):
            if bigram not in dictionary_bigrams.keys():
                dictionary_bigrams[bigram] = id
                id += 1

    unigrams_and_bigrams_dictionaries = (dictionary_unigrams, dictionary_bigrams)

    # with open('./unigrams_and_bigrams_dictionaries.txt', 'w') as f:
    #     print('unigrams_and_bigrams_dictionaries \n', unigrams_and_bigrams_dictionaries, file=f)

    # print("SVM length unigrams dictionary:", len(dictionary_unigrams), "and length dictionary bigrams:", len(dictionary_bigrams))

    training_data = []
    for file in trainingPOS:
        path_to_file = current_dir + 'POS/' + file
        content = read_document_text(path_to_file)

        docVec = make_document_vector(content, unigrams_and_bigrams_dictionaries, unigrams, bigrams, presence)
        # print("docVec from SVM", docVec)

        training_data.append((1, docVec))

    for file in trainingNEG:
        path_to_file = current_dir + 'NEG/' + file
        content = read_document_text(path_to_file)
        docVec = make_document_vector(content, unigrams_and_bigrams_dictionaries, unigrams, bigrams, presence)
        training_data.append((-1, docVec))

    model = svmlight.learn(training_data, type='classification')

    # with open('./training_data.txt', 'w') as f:
    #     print("training_data\n", training_data, file=f)

    return model, unigrams_and_bigrams_dictionaries


def predict_svm(model, testSet, unigrams_and_bigrams_dictionaries, sentiment, unigrams=True, bigrams=False, presence=False):
    results = {}

    test_data = []
    for file in testSet:
        path_to_file = current_dir + sentiment+ '/' + file
        content = read_document_text(path_to_file)
        document_vector = make_document_vector(content, unigrams_and_bigrams_dictionaries, unigrams, bigrams, presence)
        test_data.append((0, document_vector))


    predictions = svmlight.classify(model, test_data)

    # Classify the document as positive or negative
    for i in range(len(predictions)):
        results[testSet[i]] = "POS" if predictions[i] > 0 else "NEG"

    return results

def make_document_vector(content, unigrams_and_bigrams_dictionaries, unigrams=True, bigrams=False, presence=False):
    dictionary_unigrams, dictionary_bigrams = unigrams_and_bigrams_dictionaries

    document_vector = []
    count_word = {}

    if (unigrams):
        for unigram in content:
            if not unigram in dictionary_unigrams: continue
            id = dictionary_unigrams[unigram]
            if(presence): count_word[id] = 1
            else:
                if(id in count_word): count_word[id] += 1
                else: count_word[id] = 1
    if (bigrams):
        for bigram in zip(content[:-1], content[1:]):
            if not bigram in dictionary_bigrams: continue
            id = dictionary_bigrams[bigram]
            if(presence): count_word[id] = 1
            else:
                if (id in count_word): count_word[id] += 1
                else: count_word[id] = 1


    for id, count in count_word.items():
        # if(count > 1): # add for feature cutoff
        document_vector.append((id, count))

    list.sort(document_vector)

    return document_vector


