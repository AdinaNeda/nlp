import glob, os
from task1 import PorterStemmerAlgorithm

ps = PorterStemmerAlgorithm.PorterStemmer()

current_dir = "./data/"

def stem_files():
    pos_files_list = os.listdir(os.path.abspath(current_dir) + "/POS")
    neg_files_list = os.listdir(os.path.abspath(current_dir) + "/NEG")

    if not os.path.exists("./data/POSSTEMMED"): os.makedir("./data/POSSTEMMED")
    if not os.path.exists("./data/NEGSTEMMED"): os.makedir("./data/NEGSTEMMED")

    # Stem positive files
    for pos_file in pos_files_list:
        #print(pos_file)
        with open(current_dir + "POS/" + pos_file, 'r') as file_read, open(current_dir + "POSSTEMMED/" + pos_file, 'w') as file_written:
            # extract the text for the file
            content = [token.strip() for token in file_read.readlines()]

            for token in content:
                # stem each word and write it to the new file
                stem =  ps.stem(token, 0, len(token)-1)
                file_written.writelines(stem + "\n")

    # Stem positive files
    for neg_file in neg_files_list:
        # print(pos_file)
        with open(current_dir + "NEG/" + neg_file, 'r') as file_read, open(current_dir + "NEGSTEMMED/" + neg_file, 'w') as file_written:
            # extract the text for the file
            content = [token.strip() for token in file_read.readlines()]

            for token in content:
                # stem each word and write it to the new file
                stem = ps.stem(token, 0, len(token) - 1)
                file_written.writelines(stem + "\n")

    return;

stem_files()


# To compare the difference between text in 2 files:

# with open(current_dir + "POS/cv615_14182.tag", 'r') as file_read:
#     text = file_read.readlines()
#     print(text)
# print("---------------------------")
#
# with open(current_dir + "POSSTEMMED/cv615_14182.tag", 'r') as file_read:
#     text = file_read.readlines()
#     print(text)