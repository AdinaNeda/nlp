import os, re

current_dir = "./data/"

def transform(content):
    transformed = re.sub(r'(?:not|never|no|hadn\'t|nothing|nowhere|aren\'t|ain\'t'
                         r'noone|hasn\'t|can\'t|couldn\'t|shouldn\'t|won\'t|wouldn\'t|don\'t|doesn\'t|didn\'t|isn\'t'
                         r')[\w\s]+[^\w\s]',
                         lambda match: re.sub(r'(\s+)(\w+)', r'\1NEG_\2', match.group(0)),
                         content,
                         flags=re.IGNORECASE)
    return transformed

def tag_words():
    pos_files_list = os.listdir(os.path.abspath(current_dir) + "/POS")
    neg_files_list = os.listdir(os.path.abspath(current_dir) + "/NEG")

    if not os.path.exists("./data/POSTAGGED"): os.mkdir("./data/POSTAGGED")
    if not os.path.exists("./data/NEGTAGGED"): os.mkdir("./data/NEGTAGGED")

    # positive files
    for pos_file in pos_files_list:
        with open(current_dir + "POS/" + pos_file, 'r') as file_read, open(current_dir + "POSTAGGED/" + pos_file, 'w') as file_written:
            # extract the text for the file
            content = " ".join(token.strip() for token in file_read.readlines())

            # for token in content:
            transformed = transform(content)
            file_written.writelines(transformed + "\n")

    # positive files
    for neg_file in neg_files_list:
        with open(current_dir + "NEG/" + neg_file, 'r') as file_read, open(current_dir + "NEGTAGGED/" + neg_file, 'w') as file_written:
            # extract the text for the file
            content = " ".join(token.strip() for token in file_read.readlines())

            transformed = transform(content)
            file_written.writelines(transformed + "\n")

    return;

tag_words()