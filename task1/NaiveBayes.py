# from __future__ import division
import math, collections

current_dir = "./data/"

def count_dups(list_of_words):
    val = collections.Counter(list_of_words)
    return val

def smoothed_log_probs(trainingPOS, trainingNEG, unigrams=True, bigrams=False):
    dictionary_unigrams = {}
    dictionary_bigrams = {}


    # Create a dictionary for all the words and how manny occurences they had in positive and negative reviews
    # Do a dictionary for unigrams and one for bigrams
    for file in trainingPOS:
        path_to_file = current_dir + 'POSSTEMMED/' + file
        with open(path_to_file, 'r') as f:
            # extract the text for the file

            content = [token.strip() for token in f.readlines()]

            # Create dictionary for positive unigrams
            positives = count_dups(content)

            for key in positives.keys():
                value = positives[key]

                if key in dictionary_unigrams.keys(): dictionary_unigrams[key]["POS"] += value
                else:
                    dictionary_unigrams[key] = {"POS": 1, "NEG": 1}
                    dictionary_unigrams[key]["POS"] += value

            # Create dictionary for positive bigrams
            for bigram in zip(content[:-1], content[1:]):
                if bigram in dictionary_bigrams.keys(): dictionary_bigrams[bigram]["POS"] += value
                else:
                    dictionary_bigrams[bigram] = {"POS": 1, "NEG": 1}
                    dictionary_bigrams[bigram]["POS"] += value

    for file in trainingNEG:
        path_to_file = current_dir + 'NEGSTEMMED/' + file
        with open(path_to_file, 'r') as f:
            # extract the text for the file
            content = [token.strip() for token in f.readlines()]

            # Create dictionary for negative unigrams
            negatives = count_dups(content)

            for key in negatives.keys():
                value = negatives[key]

                if key in dictionary_unigrams.keys(): dictionary_unigrams[key]["NEG"] += value
                else:
                    dictionary_unigrams[key] = {"POS": 1, "NEG": 1}
                    dictionary_unigrams[key]["NEG"] += value

            # Create dictionary for positive bigrams
            for bigram in zip(content[:-1], content[1:]):
                # print(bigram)
                if bigram in dictionary_bigrams.keys(): dictionary_bigrams[bigram]["NEG"] += value
                else:
                    dictionary_bigrams[bigram] = {"POS": 1, "NEG": 1}
                    dictionary_bigrams[bigram]["NEG"] += value


    # print("NB length unigrams dictionary:", len(dictionary_unigrams), "and length dictionary bigrams:", len(dictionary_bigrams))

    total_pos_words = 0
    total_neg_words = 0
    word_probabilities = {}

    if(unigrams):
        # Calculate total number of positive and negative words in unigrams in test set
        for word in dictionary_unigrams.keys():
            total_pos_words += dictionary_unigrams[word]["POS"]
            total_neg_words += dictionary_unigrams[word]["NEG"]

        # Calculate probability of positive and negative words in unigrams in test set
        for word in dictionary_unigrams:
            word_prob_pos = math.log(dictionary_unigrams[word]["POS"] / (total_pos_words + len(dictionary_unigrams)))
            word_prob_neg = math.log(dictionary_unigrams[word]["NEG"] / (total_neg_words + len(dictionary_unigrams)))

            word_probabilities[word] = {"POS": word_prob_pos, "NEG": word_prob_neg}

    if(bigrams):
        # Calculate total number of positive and negative words in bigrams in test set
        for bigram in dictionary_bigrams.keys():
            total_pos_words += dictionary_bigrams[bigram]["POS"]
            total_neg_words += dictionary_bigrams[bigram]["NEG"]

        # Calculate probability of positive and negative words in bigrams in test set
        for bigram in dictionary_bigrams:
            word_prob_pos = math.log(dictionary_bigrams[bigram]["POS"] / (total_pos_words + len(dictionary_bigrams)))
            word_prob_neg = math.log(dictionary_bigrams[bigram]["NEG"] / (total_neg_words + len(dictionary_bigrams)))

            word_probabilities[bigram] = {"POS": word_prob_pos, "NEG": word_prob_neg}

    return word_probabilities


def naive_bayes(testSet, tokenLogProbs, classProbabilities, sentiment, unigrams=True, bigrams=False, presence=False):
    file_classification = {}

    for file in testSet:
        path_to_file = current_dir + sentiment + 'STEMMED/' + file

        with open(path_to_file, 'r') as f:
            # extract the text for the file
            content = [token.strip() for token in f.readlines()]

            sum_pos_prob = 0
            sum_neg_prob = 0

            present_features_unigrams = []
            present_features_bigrams = []

            # Sum of probablities and list of features unigrams
            if(unigrams):
                for word in content:
                    if (presence and (word in present_features_unigrams)): continue
                    if (presence and not(word in present_features_unigrams)): present_features_unigrams.append(word)
                    if word in tokenLogProbs:
                        sum_pos_prob += tokenLogProbs[word]["POS"]
                        sum_neg_prob += tokenLogProbs[word]["NEG"]

            # Sum of probablities and list of features bigrams
            if(bigrams):
                for bigram in zip(content[:-1], content[1:]):
                    if (presence and (bigram in present_features_bigrams)): continue
                    if (presence and not (bigram in present_features_bigrams)): present_features_bigrams.append(bigram)
                    if bigram in tokenLogProbs:
                        sum_pos_prob += tokenLogProbs[bigram]["POS"]
                        sum_neg_prob += tokenLogProbs[bigram]["NEG"]


            # Classify the document as being positive or negative
            if (math.log(classProbabilities["POS"]) + float(sum_pos_prob) > math.log(classProbabilities["NEG"]) + float(sum_neg_prob)):
                file_classification[file] = "POS"
            else:
                file_classification[file] = "NEG"

    return file_classification