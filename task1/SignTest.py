import math, scipy.stats,random

current_dir = "./data/"


def monte_carlo_permutation_test(allTrueSentiments, allPredictionsA, allPredictionsB, R=5000):
    s = 0
    orig_mean_diff = abs(accuracy(allTrueSentiments, allPredictionsA) - accuracy(allTrueSentiments, allPredictionsB))

    # print("original mean diff:", orig_mean_diff)
    # print("random int:", random.randint(0,1))

    for i in range(R):
        new_predictions_modelA = dict(allPredictionsA)
        new_predictions_modelB = dict(allPredictionsB)

        # print("new_predictions_modelA", new_predictions_modelA)
        # print("new_predictions_modelB", new_predictions_modelB)

        for j in new_predictions_modelA:
            swap = True if random.randint(0, 1) == 1 else False
            if swap:
                temp = new_predictions_modelA[j]
                new_predictions_modelA[j] = new_predictions_modelB[j]
                new_predictions_modelB[j] = temp
                # new_predictions_modelA[j], new_predictions_modelB[j] = new_predictions_modelB[j], new_predictions_modelA[j]

        new_mean_diff = abs(accuracy(allTrueSentiments, new_predictions_modelA) - accuracy(allTrueSentiments, new_predictions_modelB))
        # print("new mean diff:", new_mean_diff)

        if (new_mean_diff >= orig_mean_diff): s += 1

    return (s + 1) / (R + 1)


# Calculates the accuracy of a system
def accuracy(trueSentiments, predictions):
    correct = 0
    for file, sentiment in trueSentiments.items():
        if (predictions[file] == sentiment): correct += 1
    return correct / len(trueSentiments)

# Sign test with q = 0.5
# 2 sided sign test
def sign_test(actual_sentiments, classificationA, classificationB):
    plus = 0
    minus = 0
    null = 0

    print('actual_sentiments', actual_sentiments)
    print('classificationA', classificationA)
    print('classificationB', classificationB)

    for file_name, true_sentiment in actual_sentiments.items():
        # Both systems got the correct result
        if (classificationA[file_name] == classificationB[file_name] and classificationA[file_name] == true_sentiment):
            null += 1
        # System A outperforms System B
        elif (classificationA[file_name] == true_sentiment):
            plus += 1
        # System B outperforms System A
        elif (classificationB[file_name] == true_sentiment):
            minus += 1

    n = 2 * math.ceil (null / 2) + plus + minus;
    k = math.ceil (null / 2) + min(plus, minus)
    q = 0.5

    return scipy.stats.binom_test(k, n)


def compare_systems_task1(folds, systemA, systemB, systemAName, systemBName):
    scoresA = []
    scoresB = []
    allTrueSentiments = {}
    allPredictionsA = {}
    allPredictionsB = {}


    for i in range(len(folds)):
        train_set_POS = []
        train_set_NEG = []

        test_set_POS = []
        test_set_NEG = []
        for j in range(len(folds)):
            if(i == j):
                test_set_POS.extend(folds[i]["POS"])
                test_set_NEG.extend(folds[i]["NEG"])
            else:
                train_set_POS.extend(folds[j]["POS"])
                train_set_NEG.extend(folds[j]["NEG"])

        # Assign true sentiments
        trueSentiments = {}

        for file in test_set_POS: trueSentiments[file] = "POS"
        for file in test_set_NEG: trueSentiments[file] = "NEG"


        predictionA = systemA(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)
        predictionB = systemB(train_set_POS, train_set_NEG, test_set_POS, test_set_NEG)

        allTrueSentiments.update(trueSentiments)
        allPredictionsA.update(predictionA)
        allPredictionsB.update(predictionB)

        # Compute accuracies
        accA = accuracy(trueSentiments, predictionA)
        accB = accuracy(trueSentiments, predictionB)
        scoresA.append(accA)
        scoresB.append(accB)

    p_permutation = monte_carlo_permutation_test(allTrueSentiments, allPredictionsA, allPredictionsB)
    p_sign = sign_test(allTrueSentiments, allPredictionsA, allPredictionsB)

    print("RESULTS SIGN TEST")
    print("System A name", systemAName, "Accuracy:", sum(scoresA) / len(folds))
    print("System B name", systemBName, "Accuracy:", sum(scoresB) / len(folds))
    print("p-sign value:", p_sign)
    print("p-permutation value:", p_permutation)